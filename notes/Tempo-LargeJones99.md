# The auditory dynamic attending theory




> Large, E. W., & Jones, M. R.  
> The dynamics of attending: how people track time-varying events
> Psychological review, 106(1), 119–159, 1999
> https://doi.org/10.1037/0033-295X.106.1.119

---
**abstract**

A theory of attentional dynamics is proposed and aimed at explaining how listeners respond to systematic change in everyday events while retaining a general sense of their rhythmic structure. 

The approach describes attending as the behavior of internal oscillations, called attending rhythms, that are capable of entraining to external events and targeting attentional energy to expected points in time. A mathematical formulation of the theory describes internal oscillations that focus pulses of attending energy and interact in various ways to enable attentional tracking of events with complex rhythms. 

This approach provides reliable predictions about the role of attending to event time structure in rhythmical events that modulate in rate, as demonstrated in 3 listening experiments.

---
**equations**

Extended Kalman Filter predictive/corrective oscilator for tempo adaptation

A formal model of the expectations of a music listener in term of rhythm / beat tracking

It is based on the notion of *entrainment* in coupled oscillators: 

- 2 oscillators tend to synchronise when there is a possible energy transfer between both. (see image and movies below);
- In our case, 
  - one oscillator is the listener's beat expectation function (*internal or attentional beat / rhythm*) 
  - and the other oscillator is the musician (playing with temporal fluctuation) (*external beat / rhythm*).


First we consider the case of a listener expecting 1 event every beat.

Let $t_x$ be the time at which an event is expected to occur.

The phase $\phi(t)$ is the delay between the onset $t$ and its expectation $t_x$ :

$$ \phi(t) = \frac{t - t_x}{p} \qquad    t_x - \frac{p}{2} ≤ t < t_x + \frac{p}{2} \qquad (1)$$

where $p$ is the period (inverse of tempo, *i.e.* number of seconds per beat).

Hence the phase is negative when onset at $t$ arrives early to expectation $t_x$ and positive when it arrives late.

[TBC] about the definition interval $[t_x - \frac{p}{2}, t_x + \frac{p}{2}[$ : should the events outside this time window be discarded (**parse error**)?



Expressing the eternal events as a sequence of onset times $t_0, t_1,...$  at (expected) period $p$, we can determine the phase  $\phi_{n+1}$ from next event from the phase $\phi_n$ for event $n$ :

$$ \phi_{n+1} = \phi_n + \frac{t_{n+1} - t_n}{p} \qquad (\mod_{-\frac{1}{2},\frac{1}{2}}1) \qquad (2) $$

where $(\mod_{-\frac{1}{2},\frac{1}{2}}1)$ is the decimal part of the value, centered  at 0 (*i.e.* $[\frac{1}{2}, 1]$ is remapped to  $[-\frac{1}{2}, 0]$).

$$ z \mapsto \frac{\delta_{z>0}}{2} . ( | 2 z | -2 . (\lceil \frac{| 2z |-1}{2} \rceil )$$

In the next equation (called *phase attractive circle map*) , a coupling term is added in order to model the force exerted by the external rhythm to the attentional rhythm.

$$ \phi_{n+1} = \phi_n + \frac{t_{n+1} - t_n}{p} - \eta_\phi . \frac{1}{2\pi} \sin(2\pi\phi_n) \qquad (\mod_{-\frac{1}{2},\frac{1}{2}}1) \qquad (3) $$

where the parameter $\eta_\phi$ is a coupling strength ($0 < \eta_\phi ≤ 2$).

The period is also adjusted at each event:

$$ p_{n+1} = p_n \bigl(1 + \eta_p . \frac{1}{2\pi} \sin(2\pi\phi_n)\bigr) \qquad (4)$$



Introduction of a new state variable $\kappa$ which represents *attentional focus* : the bigger $\kappa$ is, the narrower is the listener's temporal expectation, and small $\kappa$ reflects greater uncertainty about external happenings. 

It is used in the following equation which models of a pulse of attentional energy, replacing the above parameter $\eta_\phi$:

$$ f(\phi, \kappa) = \frac{1}{I_0(\kappa)} . \exp(\kappa . \cos(2 \pi . \phi))  \qquad (5)$$

$I_0$ is the modified Bessel function (first kind) of order 0.

An asymptotic development shows that for a large $x$, it holds that $I_0(x) \sim \frac{\exp(x)}{\sqrt{2\pi x}}$.

The adaptation of $\kappa$ to events is designed similarly to a maximal likelihood estimate, given by the resolution of

$$ A(\kappa) = r  \qquad (6) $$

where $0 ≤ r ≤ 1$  , called *mean vector length* is a statistic that measure the degree of synchronisation, as variability in relative phase ) and $A(\kappa) = \frac{I_1(\kappa)}{I_0(\kappa)}$.

An incremental approximation of $r$ is computed as the following *synchronised strength* value $s_n$:

$$ s_{n+1} = s_n - \eta_s (s_n - \cos(2\pi . \phi_n)) \qquad (7a) $$  

where parameter  $0 ≤ \eta_s ≤ 1$, determines how quickly $r$ is approximated, *i.e.* how quickly attention rate changes.

and the realtime adaptation of attentional focus is then described by:

$$ \kappa_{n+1} = A^{-1} \bigl[ H(b, s_{n+1}) \bigr]   \qquad (7b)$$

there $H$ is a hard limit function ($A^{-1}(1) = +\infty$) and parameter $0 < b < 1$ is a maximal value for $s_n$, fixing a upper limit $A^{-1}(b)$ on the attention focus $\kappa$  ($H(b, s) = s$ when $s < b$ and $H(b, s) = b$ otherwise).

The monotonic function $A$ cannot be inverted analytically, and hence $\kappa_{n+1}$ is estimated with (7b) numerically or by table lookup ($A$  varies from in $[0, 1]$ when $\kappa$ goes from $0$ to $+\infty$).



To summarise, we have altogether ($t_n$ and $t_{n+1}$ are two successive RTU onsets of events):

$$ \phi_{n+1} = \phi_n + \frac{t_{n+1} - t_n}{p_n} - \frac{\exp(\kappa_{n+1} . \cos(2 \pi . \phi_n))}{I_0(\kappa_{n+1})} .  \frac{1}{2\pi} \sin(2\pi.\phi_n) \qquad (\mod_{-\frac{1}{2},\frac{1}{2}}1)$$

$$ p_{n+1} = p_n . \bigl(1 + \frac{\exp(\kappa_{n+1} . \cos(2 \pi . \phi_n))}{I_0(\kappa_{n+1})} .\frac{1}{2\pi} \sin(2\pi . \phi_n)\bigr)$$

$$ \kappa_{n+1} = A^{-1} \bigl[ H(b, s_{n+1}) \bigr]$$

$$ s_{n+1} = s_n - \eta_s . (s_n - \cos(2\pi . \phi_n))$$



For a rhythm value inside-the-beat: when the onset of the next expected event is not 1 beat after the previous event but $q$ beats (with $q$ rational), we replace $p_n$ by $\frac{p_n}{q}$ in the first above equation.

Some parameter values (and approx.) taken from an implementation of the model:

- $b = 0.94$
- $\eta_s = 0.9$
- $I_0(\kappa) \sim \exp(\kappa)$
- a table for $A$ for $\kappa ∈ [1.0, 10.0 ]$ (step = $0.001$) - lookup with stl [`lower_bound`](http://www.cplusplus.com/reference/algorithm/lower_bound/).



---

**window of expectation**

In CFG model, the new values of tempo *etc* are estimated  in leaves (terminal productions), using the MTU time interval $[x, x'[$ associated to non-terminals ($q = x' - x$) and the real-time onset $t_{n+1}$ in the input timeline.

In order to discard meaningless parse trees, and reduce search space, we restrict this $t_{n+1}$ to a window of expectation:

$$ t_{n} - \frac{p}{2q} ≤ t_{n+1} < t_n + \frac{p}{2q} $$   

When $t_{n+1}$ is out of this window, the application of the terminal production is discarded.

Q: narrower window?

[Large 94], pages 14-15, defines a region of temporal expectancy, using an output pulse 

$$ o(t) = 1 + \tanh(\gamma.a(t) )$$  

where parameter $\gamma$ is the output gain, and $a(t) = cos(2\pi. \phi(t)) - 1$ is an activation function (the phase $\phi(t) = \frac{t - t_0}{p}$). The more $\gamma$ increases, the more the temporal receptive field shrinks. 

Q2: shall we consider the attentional focus $\kappa_{n+1}$ to define the window? 



---
**listening experiments**

for validation of these equations and the coupled oscillators model of listener / player.



> Jones, M.R., Moynihan, H., MacKenzie, N., Puente, J.
> Temporal aspects of stimulus-driven attending in dynamic arrays 
> Psychol. Sci. 13 (4), 313–319. 2002.
https://doi.org/10.1111/1467-9280.00458



> Anna-Katharina R. Bauer, Manuela Jaeger, Jeremy D. Thorne, Alexandra Bendixen, Stefan Debener,
> The auditory dynamic attending theory revisited: A closer look at the pitch comparison task
> Brain Research, Volume 1626, 2015, Pages 198-210, ISSN 0006-8993
https://doi.org/10.1016/j.brainres.2015.04.032





---
**appendix: coupled oscillators**

see *e.g.* http://www.math.pitt.edu/~bard/classes/mth3380/syncpapers/metronome.pdf

> Synchronization of metronomes
> James Pantaleone
>  Am. J. Phys **70**(10), 992-1000. 2002.


![Synchronization of metronomes](data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUTEhMWFRUXFxcXFRcVGBUXFRUXFxcWFxcXFxcYHSggGBolHRUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAJ0BQAMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAAAQIDBAYFB//EAD8QAAEEAAMEBgcGBQQDAQAAAAEAAgMRBBIhBRMxQSJRYXGBkQYyU6Gx0fAUI0JSosEVYnKC4UNzkvEzVLQk/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AO2TSCdIGmlSYQATBSCaBoQhBU71h3H9lNI+sPH9lNAlEppFAWgJBP65oGAmkCpBAUmEIpAJoH1wpMBAD660kyikB9BIp+SRKBKNKSigGHRWKEQ0VoQOkgfJAP11oKBEpG0yOxPVBEoS8U0EfrqTan2KJtA6Qi0UgiUk6SKCBCaKQgaEICBlNRtNAwmohCCLvWHj8FIqDzqPrkpg9SBJOCZP0VCWQNBJOgGtoC0wV4zNvteXCKGeQNc4FwjeGW05XU4inUdNFXgvSUTWYsPO+uNMkFeDmC+HJB0DCmF4+ydutnkMYjexzbDhICxzSG5qLXAHUEL17QNMJWmEDRVpD6+aaB39c0EpJ2gLUXFPMoSyhoJPLxQNRteXhtrOlsxYaaQAubmaGhpLTlIBJ4g8lXhNuumLhFhsQ8trNbSyuPOQNDuHK0Hswixy81cT1LBsrEl7Xh0b2PjcA5jwAekMzefMLdaAtK00OKBUhLxTKACTig9/yTaaQCVoBSJCBlKkJlAj5IpCVIIJlCLQRTTpFIC0rUqRSBEotCSCEh1Hf+ykVCXiP6lIoHaz7RP3bu7mtBVGOHQd3ckHP7FFRaaZnYvN0JTq2eSjnByM05HU0tpvK/W/uyKLZHjUi+jGcx06lk2KPux/u4wcZvauPBv3Z4/i16ltc3ovH8p0uRtcDVxdP/igybLZW0JgOFRVxGn2Rn5teXPVey7HgPbGGucXOo5a6I5uPYvFikyY2Z/8sB5ka4YD8WvnqvT2FASDKfxer3dfig9MFMFOkyECUiikyO5BFBPYU6QUEVmx/qO7lrpZseOge5B4vo6yoG0KzOxYd93K7hPLVSA5GcBoRqvRaXa9ImozQLXyAaD8DTmPcCsHo+37ln+5jOU3t5fy/d/8tepbuThd9EivvOoaVH0/+OqA2UzLJOBpX2bk8f6AJ0eS4ceB1V8uN1ytYXm2g0QAL7+rismGmDH4k9Rw4A6Wp+ztr1+l56rTgYjV3Z4955nw4eaDZmTzJFBQSvT90kqUg3tQRBTKeX/CVIFYQnlRlQAKdpUpIEQik7USgrpMJJhA6RlQE0BSK7/NCaCshKvq1JyigrkHDvCmT9aqE3LvCkUDAVWKHQd3KwlVYn1Hdx/67UHibIw5LQ7eyNDJ8WDG0gRy5yKzjmRxC2SRFzJGh7ocwIEjNHMugHA8AfHmsuzX1G+uWJl97Yz+6q9K3ncAZc7S8bziG5QCRmrWs1eSCGKwjnY10GYkvZhg57uJDYem49pAXXBoFAaAaAcgBwXzvZG3ywwkxNuO2PdmkdLKKOVxJuiAKrt8F3Gy9pDEBxa0ty0CHEEm7/Kg2j6pFdSYTpAtfoIKEIBCLQgD9aLPjh0HfXP/AKWlZsb6hQeFsKAmNj97I0MkxjTE0jdyF08mrgeJHJb3QlzXsDnRlwIDmaObYABHIFefseQiBtaffYrr/wDYk6uK17VstaL0J6XIEacexBlEDvtM0RLibw9udxNQMBee35r3w0AUOXJeRsRh3oYXAOf0Q5xJDqBdqTqKDQAB1L28THkcW2DR4jgdAUEK7/rqUrStIIHSmG9qgrAUALQEadSYCCKCFIpEIEhBCCgRSTJ6kighSKRfYhA00gU77CgEFHgi0EXBQKsce9VlyCubh4j4qZCrnOniPiFMoHfaqsS3oO7irLVc5truehQeJseNzmzgAmsS/r5wwFParTM9mHa3kZJa1ys0DW6dZXLbR9G5Z34nENxLoWMe1uUbzpERRm7a4AWXVw5FZ8N6I4h5NYsh7Q3ODvgRmGZozZukKQddg2Bm0S1oDRusM4tAoZnDEBxrtyt8l1LRWgAHcAPguD9G9mOwuN3bpDI4x4d5cb4l2IbXSJJADRqV3n12IJppBMIApWmkgaEkIBZ8b6hV7VRjfUKDwdiwuMAoEjfYquV//ok61oxcbpCGBvAHNWo48L4LmW7EncySZmJdG0zTFjAZAB944UNaFmzw5p4XYc77LMU7ouLSCZW07iaBOo4IOt2ez7zEgjRskYaDwaDh43Gu8uJ8VvHBeJ6KYR8X2pj5N44TtJcc2odBEQOkSdBovbB7EB3JqKfggkrVS0K4dSCSTkwkECSKklaCITKdJIEgoKEFaaVoKBnRNYGYgOJu6B0H7laPtLe1Bei1T9pb2+X+U/tLe1BNyrP1oj7SP5vJP7Q3+by/ygoxA04Hly7VZfXfgpCdp5O8h81IOvgD7kEMovUu9wHmvH23vDYY0hvca4da9p19qrLn8ig42LGyNjIlYXtdpkfXXRLb1DgK1XoZRTGRW4NIZhqaScMdzZfiQH2Wkk11rDt7Z2LMxdDFBJGQCGvcGlrgOlXYaB0K0QnFMGZmz4myODd68TRjeZRQutT2ILmgjaEd0XfZ8NZAoEiWcGgeWq6olczg9n4h8/2icRMqNjGxxuc4gtc91ucQAT0zoF7zAeaDW1ykSsra6z5KYcEF19oRarDh1ozdqCaLULSQWLydpyFzshNNINcgTyHcvRLHclW9knZ7kGHETvyhs4c4SHKy6BF2CWuq7AFjiqMS6XpxxluYk7qR0ZLI2MqmSU71tTwq9FCFmOjcBuIJ2h0hD5HgupziWNAtpblFN4m1uZoRu8FIzM7NKXSwuzmgL6Ml8gO5Atm/+TF/70f/AM0PWto71jweDmDpnkNYJXh4ZeYsDY2MAcRoT0b061ubA7n7kCtAVwgKluCgpaQrRSYhP0E90foIFakSEt2VEtKBvKSWUpZSgdoUTpxI7VIkoApWnaSCnOjMuR+1y+0d5j5Ibi5R/qv8/wDCDqhEzqClu29XxXKHHTe1f5j5IGOm9s/3fJB1e7b1fFG6b1fFcoMbN7V/6fkgY6b2z/0/JB00sbb/AMleXtDFNHQaaP4nWdOwdq8jEbSmDTczq/t+SzbJidISS7otFve48AOOp59vag6TZzWSGy+dxGgZCHljR1vcaDj4L32bDfu82+05A5mnxc12hXFSek72DJh29Ac38+3L9dyswPp29vRnjBZwO76Lh3cvNBDa2ExTJWhuJxABdoHSOcxwvUWTr9aLU7EPGhcfM6LZtWWN7BJG7NDJq0j8MjddQfUePevHklc/T8XBx5ADmg6PBFkkbX11g6niDR5rR9nZ1e8lclHjyNInOa1un9R5upSGOl9q7wr5IOsEDeQrxKlum/RPzXJHHyn/AFX+75JjHy+1f+n5IOtETe3zKZjHb5lcp/EZvbP/AE18EO2nN7Q+Tfkg6ox9/mUt2O3zK5f+Jze0/S35IO1Jvae5vyQdSGDrPmUV2nzXK/xSb2nub58FH+Lzfn/SEHWgdp806/mPmuUG2Jvz/pCf8Xmr1x35W38EHV/3HzSkkrXMfArjpcfiDwmI7ms/cFebjHYogk4ybhwqMD3MQdK3ashhjcJCXPGYno6NzEdVcgsL9q46RwZA8l7j+Vmg5k2KA7SvJ2NiXMgfvCCGMiZEK19Z1gnn1r1fR6F0uZ5IbG2t5I/Rn9P8x7EHVbJwmKNNfiWuJ4kCIgEcqY0D3p7dwWLaAIsU1rjwzRx5T35hp5rJifSuJguGEyBugcbY3wa39wqovSzDTuDcTCWXp61s7+sckEYdoYlramNSAU4U0dIcSO9epBKXta4OOoB5c/Bc7tB74n5SS5tHJZsFhJ4HmNfBTO13RhrWBpbkaQTmvUa8Cg6OnfmPu+SDf5vcFzY2/Lfqs/V81E7em5tZ+r5oOkt35vcEW78w8guZG3Zb9Vn6lIbel/LH+pB0L481gnQijQAVhK5o7el/Iz9SY29J7Nnm5B0eZBcud/j7/Zt7NSo/x+T2bPNyDyk7StCDPI4gqvelaXttUmNBHfFG+Klu0t2gwbUxBy9518LP7LoMLh2/ZsPAXhm9a2WQ16xcA8DuBff9o6l4G1objJHKj4c/iuo9HMXFJhcO98LZQxu4l1LZI3xigQWnTMzI6iNbQexD6FNfFmZI13Vy95XB7c2W+IkEHQkHwX17Aw4Qs+7JYDx6ZBHeCaXIek2wpCCYXido1IYQ5w72j4hBy3olMTvoD6sjC4dQezVru+6HcSrNoYrK1uU3nAuvh8E9jQGFsuIe2msaWMvTO8jgB2HJ5rynO1aOTWjz+gg0wyEDirN+VkzJhyDa2YqW9Kog1WgtQMSlG9KQapUgW8KN8VLIjdoI74qOdWbtAjQQ3iN+pbtUSikFwxCrlxBpUlyreUFEuI6Ib1a+PAfuul2/92Y8Gw1HEwOd/PI7Uk9fX4jqXIbRBbTgLNWO9pul2/pJhw7ERTsNxzRsLSOBIFfDKfFB7PopsVs0bw7gW0OXS4g/Fclt3ZxjJBGtka9hpfS/Q6PLG4c9Fzvpxhmh7q56+epQc9sycy4KZrtX4Yh7DzLCLLT/AGtf/wAWrDDiOI7vf/0tmy27rBY6U6CQNiZ2mns0/ul/SepeVgGWCe2vL/soN4nKDMqwxSLED3yN8VExpbtBfDLattUxClZaCSRSQgjadqkSdinaBkpWglIlAWouKCQkUFbieoLNsnHyYGRzmND4ngCSNxprgD0SHfheL0d4FaysG0ZQ1p1QdzhPSHCSAEzbo1wlqNw7C5wMcneHBZ9r47ZxbmfLHI8cBhmAPPe6NxA9y+XZ5b6LnAdXFXPwc7h67h4C/NB7HpB6Rh4aCMkbP/FFdn+p3b8L58Vz2Exry6yeJulmk2ZR6TiT1nVacLhgEHrxSLQ1Z4WaK5oQa8NxWuismF4ra0oG0KVJhOkCpFJpIDKik0kAVmxDVpVE/BBgcVWSrHBV5UE5Yw5lHStQvT9F9tMEYw+IswgkskFl0BJunAallnQjUXzC8jFE5F4Mxex1scQezn4IPv2yWig+J+8bXRkiqRrv6g06FZfS/CwuaHzyiEGtHHpO01Abxcb5AL4jhsXNebK2zzBc0nvIU5MTMDYa1t8XC3O8yg6rb21GzZIogWQs9RrqzONVvHgcNLocrPMpRNAAA4BcxgruybN6krqIRpxQWBNCECTQhA2qVqITtA0kWlaCnKgv5JpICkUhRJQTpBVZcglASuoWvJjuRxtbsaaaVnwXAdqD1MFgG8aWvGzxhhGTXrC9XYuED26rw/SGLIXAckHNzwAkkKuBivwLunXI6KDR0kF4KkCo0ptagvgK2xuWGE6razggtBUrVYKdoLLSUbRaCVotRKVoJkqmY6KZKpnOiDG4qCTykEFWOfopbI2ZvbLuA4kqrF6kL02PydEcEG7A7OZWjbrmVqfsqJzDXRdfgt/o5AHkNPMrz9vvyPcG8iR5FByuMwxjfw09y9vCOtoWGR2eM3+HUHvWjZp6KDaEJIJQNAStNAIQEwgEkIQf/9k=)



There are many youtube videos presenting similar experiments.

*e.g.* https://sciencedemonstrations.fas.harvard.edu/presentations/synchronization-metronomes			 	




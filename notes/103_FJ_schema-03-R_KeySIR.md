result of parsing input segment from `103_FJ.mid` 
with base automata `schema-03-R.wta` and class `KeySIR`.




## annotated content of input segment
with parsing annotations
E1 = ●,   R = ○

input segment : 107 events

```
measure 00  3̅(●, 2̅(●, ○), ●) |
 E1 point[00]: 0.05625  NOTE_ON  G3 rdur=0.991667s
 E1 point[01]: 1.04792  NOTE_OFF R  rdur=0.00416667s
 |  point[02]: 1.05208  NOTE_ON  C4 rdur=0.397917s
 R  point[03]: 1.45     NOTE_OFF R  rdur=0.533333s
 E1 point[04]: 1.98333  NOTE_ON  C4 rdur=0.516667s

measure 01  3̅(●, 2̅(●, ●), 2̅(2̅(●, 2̅(○, ●)), ⏑)) |
 E1 point[05]: 2.5      NOTE_OFF R  rdur=0.485417s
 |  point[06]: 2.98542  NOTE_ON  C4 rdur=0.697917s
 E1 point[07]: 3.68333  NOTE_OFF R  rdur=0.3625s
 |  point[08]: 4.04583  NOTE_ON  D4 rdur=0.404167s
 E1 point[09]: 4.45     NOTE_OFF R  rdur=0.0229167s
 |  point[10]: 4.47292  NOTE_ON  C4 rdur=0.472917s
 E1 point[11]: 4.94583  NOTE_ON  B3 rdur=0.258333s
 R  point[12]: 5.20417  NOTE_OFF R  rdur=0.15625s
 E1 point[13]: 5.36042  NOTE_ON  C4 rdur=0.5s
 C0 

measure 02  3̅(●, 2̅(●, ○), 2̅(●, ○)) |
 E1 point[14]: 5.86042  NOTE_ON  G3 rdur=0.970833s
 E1 point[15]: 6.83125  NOTE_OFF R  rdur=0.0958333s
 |  point[16]: 6.92708  NOTE_ON  D4 rdur=0.470833s
 R  point[17]: 7.39792  NOTE_OFF R  rdur=0.583333s
 E1 point[18]: 7.98125  NOTE_ON  D4 rdur=0.466667s
 R  point[19]: 8.44792  NOTE_OFF R  rdur=0.614583s

measure 03  3̅(●, ⏑, ●) |
 E1 point[20]: 9.0625   NOTE_ON  D4 rdur=1.54792s
 C0
 E1 point[21]: 10.6104  NOTE_OFF R  rdur=0.420833s
 |  point[22]: 11.0312  NOTE_ON  F4 rdur=0.589583s

measure 04  3̅(2̅(2̅(●, ○), ●), ●, ●) |
 E1 point[23]: 11.6208  NOTE_OFF R  rdur=0.41875s
 |  point[24]: 12.0396  NOTE_ON  E4 rdur=0.191667s
 R  point[25]: 12.2312  NOTE_OFF R  rdur=0.28125s
 E1 point[26]: 12.5125  NOTE_ON  F4 rdur=0.302083s
 E1 point[27]: 12.8146  NOTE_OFF R  rdur=0.208333s
 |  point[28]: 13.0229  NOTE_ON  G4 rdur=0.879167s
 E1 point[29]: 13.9021  NOTE_ON  E4 rdur=0.972917s

measure 05  3̅(2̅(●, 2̅(●, 2̅(○, ●))), ⏑, ●) |
 E1 point[30]: 14.875   NOTE_ON  C4 rdur=0.422917s
 E1 point[31]: 15.2979  NOTE_OFF R  rdur=0.0604167s
 |  point[32]: 15.3583  NOTE_ON  D4 rdur=0.354167s
 R  point[33]: 15.7125  NOTE_OFF R  rdur=0.166667s
 E1 point[34]: 15.8792  NOTE_ON  E4 rdur=1.08333s
 C0
 E1 point[35]: 16.9625  NOTE_OFF R  rdur=0.00208333s
 |  point[36]: 16.9646  NOTE_ON  C4 rdur=0.55625s

measure 06  3̅(2̅(●, ●), ●, ●) |
 E1 point[37]: 17.5208  NOTE_OFF R  rdur=0.452083s
 |  point[38]: 17.9729  NOTE_ON  A3 rdur=0.364583s
 E1 point[39]: 18.3375  NOTE_OFF R  rdur=0.06875s
 |  point[40]: 18.4062  NOTE_ON  B3 rdur=0.397917s
 E1 point[41]: 18.8042  NOTE_OFF R  rdur=0.13125s
 |  point[42]: 18.9354  NOTE_ON  C4 rdur=0.7125s
 E1 point[43]: 19.6479  NOTE_OFF R  rdur=0.3125s
 |  point[44]: 19.9604  NOTE_ON  D4 rdur=0.59375s

measure 07  3̅(2̅(●, 2̅(●, 2̅(⏑, ●))), 2̅(⏑, 2̅(●, ○)), ●) |
 E1 point[45]: 20.5542  NOTE_OFF R  rdur=0.44375s
 |  point[46]: 20.9979  NOTE_ON  D4 rdur=0.43125s
 E1 point[47]: 21.4292  NOTE_ON  C4 rdur=0.414583s
 C0
 E1 point[48]: 21.8438  NOTE_OFF R  rdur=0.0333333s
 |  point[49]: 21.8771  NOTE_ON  B3 rdur=0.533333s
 C0
 E1 point[50]: 22.4104  NOTE_ON  A3 rdur=0.291667s
 R  point[51]: 22.7021  NOTE_OFF R  rdur=0.279167s
 E1 point[52]: 22.9812  NOTE_ON  G3 rdur=0.610417s

measure 08  3̅(●, ●, 2̅(●, ○)) |
 E1 point[53]: 23.5917  NOTE_OFF R  rdur=0.410417s
 |  point[54]: 24.0021  NOTE_ON  G3 rdur=0.90625s
 E1 point[55]: 24.9083  NOTE_OFF R  rdur=0.0729167s
 |  point[56]: 24.9812  NOTE_ON  C4 rdur=0.522917s
 E1 point[57]: 25.5042  NOTE_OFF R  rdur=0.433333s
 |  point[58]: 25.9375  NOTE_ON  C4 rdur=0.533333s
 R  point[59]: 26.4708  NOTE_OFF R  rdur=0.452083s

measure 09  3̅(●, 2̅(●, 2̅(●, 2̅(⏑, ●))), 2̅(⏑, ●)) |
 E1 point[60]: 26.9229  NOTE_ON  C4 rdur=0.766667s
 E1 point[61]: 27.6896  NOTE_OFF R  rdur=0.289583s
 |  point[62]: 27.9792  NOTE_ON  D4 rdur=0.314583s
 E1 point[63]: 28.2937  NOTE_OFF R  rdur=0.120833s
 |  point[64]: 28.4146  NOTE_ON  C4 rdur=0.41875s
 C0 
 E1 point[65]: 28.8333  NOTE_OFF R  rdur=0.0458333s
 |  point[66]: 28.8792  NOTE_ON  B3 rdur=0.427083s
 C0
 E1 point[67]: 29.3062  NOTE_OFF R  rdur=0.08125s
 |  point[68]: 29.3875  NOTE_ON  C4 rdur=0.579167s

measure 10  3̅(●, 2̅(●, ○), ●) |
 E1 point[69]: 29.9667  NOTE_OFF R  rdur=0.00208333s
 |  point[70]: 29.9688  NOTE_ON  G3 rdur=0.93125s
 E1 point[71]: 30.9     NOTE_OFF R  rdur=0.1s
 |  point[72]: 31       NOTE_ON  D4 rdur=0.49375s
 R  point[73]: 31.4937  NOTE_OFF R  rdur=0.49375s
 E1 point[74]: 31.9875  NOTE_ON  D4 rdur=0.5125s

measure 11  3̅(●, ⏑, ●) |
 E1 point[75]: 32.5     NOTE_OFF R  rdur=0.475s
 |  point[76]: 32.975   NOTE_ON  D4 rdur=1.72917s
 C0 
 E1 point[77]: 34.7042  NOTE_OFF R  rdur=0.2125s
 |  point[78]: 34.9167  NOTE_ON  C4 rdur=0.65625s

measure 12  3̅(●, 2̅(2̅(●, 2̅(○, ●)), 2̅(⏑, 2̅(○, ●))), 2̅(2̅(⏑, ○), ●)) |
 E1 point[79]: 35.5729  NOTE_OFF R  rdur=0.366667s
 |  point[80]: 35.9396  NOTE_ON  B3 rdur=0.597917s
 E1 point[81]: 36.5375  NOTE_OFF R  rdur=0.408333s
 |  point[82]: 36.9458  NOTE_ON  B4 rdur=0.310417s
 R  point[83]: 37.2563  NOTE_OFF R  rdur=0.133333s
 E1 point[84]: 37.3896  NOTE_ON  A4 rdur=0.2875s
 C0
 R  point[85]: 37.6771  NOTE_OFF R  rdur=0.166667s
 E1 point[86]: 37.8438  NOTE_ON  G4 rdur=0.366667s
 C0 
 R  point[87]: 38.2104  NOTE_OFF R  rdur=0.21875s
 E1 point[88]: 38.4292  NOTE_ON  F4 rdur=0.327083s

measure 13  3̅(●, 2̅(●, 2̅(●, ○)), 2̅(●, ●)) |
 E1 point[89]: 38.7563  NOTE_OFF R  rdur=0.2s
 |  point[90]: 38.9562  NOTE_ON  E4 rdur=0.54375s
 E1 point[91]: 39.5     NOTE_OFF R  rdur=0.460417s
 |  point[92]: 39.9604  NOTE_ON  E4 rdur=0.404167s
 E1 point[93]: 40.3646  NOTE_OFF R  rdur=0.0666667s
 |  point[94]: 40.4312  NOTE_ON  D4 rdur=0.275s
 R  point[95]: 40.7062  NOTE_OFF R  rdur=0.222917s
 E1 point[96]: 40.9292  NOTE_ON  C4 rdur=0.35625s
 E1 point[97]: 41.2854  NOTE_OFF R  rdur=0.1s
 R  point[98]: 41.3854  NOTE_ON  B3 rdur=0.375s

measure 14  3̅(●, ●, ●) |
 E1 point[99]: 41.7604  NOTE_OFF R  rdur=0.152083s
 |  point[100]: 41.9125 NOTE_ON  A3 rdur=1.01458s
 E1 point[101]: 42.9271 NOTE_ON  F3 rdur=1.13125s
 E1 point[102]: 44.0583 NOTE_OFF R  rdur=0.0229167s
 |  point[103]: 44.0812 NOTE_ON  B3 rdur=0.8375s

measure 15   ●  ||
 E1 point[104]: 44.9188 NOTE_OFF R  rdur=0.120833s
 |  point[105]: 45.0396 NOTE_ON  E3 rdur=1.30625s
 |  point[106]: 46.3458 NOTE_OFF R 
```
note that the last note in measure 15 is made of
- a grace-rest
- a note
- a rest (final event, with no duration).


## content of input segment
without parsing annotations

input segment : 107 events

```
point[00]: 0.05625   NOTE_ON  G3 rdur=0.991667s
point[01]: 1.04792   NOTE_OFF R  rdur=0.00416667s
point[02]: 1.05208   NOTE_ON  C4 rdur=0.397917s
point[03]: 1.45      NOTE_OFF R  rdur=0.533333s
point[04]: 1.98333   NOTE_ON  C4 rdur=0.516667s
point[05]: 2.5       NOTE_OFF R  rdur=0.485417s
point[06]: 2.98542   NOTE_ON  C4 rdur=0.697917s
point[07]: 3.68333   NOTE_OFF R  rdur=0.3625s
point[08]: 4.04583   NOTE_ON  D4 rdur=0.404167s
point[09]: 4.45      NOTE_OFF R  rdur=0.0229167s
point[10]: 4.47292   NOTE_ON  C4 rdur=0.472917s
point[11]: 4.94583   NOTE_ON  B3 rdur=0.258333s
point[12]: 5.20417   NOTE_OFF R  rdur=0.15625s
point[13]: 5.36042   NOTE_ON  C4 rdur=0.5s
point[14]: 5.86042   NOTE_ON  G3 rdur=0.970833s
point[15]: 6.83125   NOTE_OFF R  rdur=0.0958333s
point[16]: 6.92708   NOTE_ON  D4 rdur=0.470833s
point[17]: 7.39792   NOTE_OFF R  rdur=0.583333s
point[18]: 7.98125   NOTE_ON  D4 rdur=0.466667s
point[19]: 8.44792   NOTE_OFF R  rdur=0.614583s
point[20]: 9.0625    NOTE_ON  D4 rdur=1.54792s
point[21]: 10.6104   NOTE_OFF R  rdur=0.420833s
point[22]: 11.0312   NOTE_ON  F4 rdur=0.589583s
point[23]: 11.6208   NOTE_OFF R  rdur=0.41875s
point[24]: 12.0396   NOTE_ON  E4 rdur=0.191667s
point[25]: 12.2312   NOTE_OFF R  rdur=0.28125s
point[26]: 12.5125   NOTE_ON  F4 rdur=0.302083s
point[27]: 12.8146   NOTE_OFF R  rdur=0.208333s
point[28]: 13.0229   NOTE_ON  G4 rdur=0.879167s
point[29]: 13.9021   NOTE_ON  E4 rdur=0.972917s
point[30]: 14.875    NOTE_ON  C4 rdur=0.422917s
point[31]: 15.2979   NOTE_OFF R  rdur=0.0604167s
point[32]: 15.3583   NOTE_ON  D4 rdur=0.354167s
point[33]: 15.7125   NOTE_OFF R  rdur=0.166667s
point[34]: 15.8792   NOTE_ON  E4 rdur=1.08333s
point[35]: 16.9625   NOTE_OFF R  rdur=0.00208333s
point[36]: 16.9646   NOTE_ON  C4 rdur=0.55625s
point[37]: 17.5208   NOTE_OFF R  rdur=0.452083s
point[38]: 17.9729   NOTE_ON  A3 rdur=0.364583s
point[39]: 18.3375   NOTE_OFF R  rdur=0.06875s
point[40]: 18.4062   NOTE_ON  B3 rdur=0.397917s
point[41]: 18.8042   NOTE_OFF R  rdur=0.13125s
point[42]: 18.9354   NOTE_ON  C4 rdur=0.7125s
point[43]: 19.6479   NOTE_OFF R  rdur=0.3125s
point[44]: 19.9604   NOTE_ON  D4 rdur=0.59375s
point[45]: 20.5542   NOTE_OFF R  rdur=0.44375s
point[46]: 20.9979   NOTE_ON  D4 rdur=0.43125s
point[47]: 21.4292   NOTE_ON  C4 rdur=0.414583s
point[48]: 21.8438   NOTE_OFF R  rdur=0.0333333s
point[49]: 21.8771   NOTE_ON  B3 rdur=0.533333s
point[50]: 22.4104   NOTE_ON  A3 rdur=0.291667s
point[51]: 22.7021   NOTE_OFF R  rdur=0.279167s
point[52]: 22.9812   NOTE_ON  G3 rdur=0.610417s
point[53]: 23.5917   NOTE_OFF R  rdur=0.410417s
point[54]: 24.0021   NOTE_ON  G3 rdur=0.90625s
point[55]: 24.9083   NOTE_OFF R  rdur=0.0729167s
point[56]: 24.9812   NOTE_ON  C4 rdur=0.522917s
point[57]: 25.5042   NOTE_OFF R  rdur=0.433333s
point[58]: 25.9375   NOTE_ON  C4 rdur=0.533333s
point[59]: 26.4708   NOTE_OFF R  rdur=0.452083s
point[60]: 26.9229   NOTE_ON  C4 rdur=0.766667s
point[61]: 27.6896   NOTE_OFF R  rdur=0.289583s
point[62]: 27.9792   NOTE_ON  D4 rdur=0.314583s
point[63]: 28.2937   NOTE_OFF R  rdur=0.120833s
point[64]: 28.4146   NOTE_ON  C4 rdur=0.41875s
point[65]: 28.8333   NOTE_OFF R  rdur=0.0458333s
point[66]: 28.8792   NOTE_ON  B3 rdur=0.427083s
point[67]: 29.3062   NOTE_OFF R  rdur=0.08125s
point[68]: 29.3875   NOTE_ON  C4 rdur=0.579167s
point[69]: 29.9667   NOTE_OFF R  rdur=0.00208333s
point[70]: 29.9688   NOTE_ON  G3 rdur=0.93125s
point[71]: 30.9      NOTE_OFF R  rdur=0.1s
point[72]: 31        NOTE_ON  D4 rdur=0.49375s
point[73]: 31.4937   NOTE_OFF R  rdur=0.49375s
point[74]: 31.9875   NOTE_ON  D4 rdur=0.5125s
point[75]: 32.5      NOTE_OFF R  rdur=0.475s
point[76]: 32.975    NOTE_ON  D4 rdur=1.72917s
point[77]: 34.7042   NOTE_OFF R  rdur=0.2125s
point[78]: 34.9167   NOTE_ON  C4 rdur=0.65625s
point[79]: 35.5729   NOTE_OFF R  rdur=0.366667s
point[80]: 35.9396   NOTE_ON  B3 rdur=0.597917s
point[81]: 36.5375   NOTE_OFF R  rdur=0.408333s
point[82]: 36.9458   NOTE_ON  B4 rdur=0.310417s
point[83]: 37.2563   NOTE_OFF R  rdur=0.133333s
point[84]: 37.3896   NOTE_ON  A4 rdur=0.2875s
point[85]: 37.6771   NOTE_OFF R  rdur=0.166667s
point[86]: 37.8438   NOTE_ON  G4 rdur=0.366667s
point[87]: 38.2104   NOTE_OFF R  rdur=0.21875s
point[88]: 38.4292   NOTE_ON  F4 rdur=0.327083s
point[89]: 38.7563   NOTE_OFF R  rdur=0.2s
point[90]: 38.9562   NOTE_ON  E4 rdur=0.54375s
point[91]: 39.5      NOTE_OFF R  rdur=0.460417s
point[92]: 39.9604   NOTE_ON  E4 rdur=0.404167s
point[93]: 40.3646   NOTE_OFF R  rdur=0.0666667s
point[94]: 40.4312   NOTE_ON  D4 rdur=0.275s
point[95]: 40.7062   NOTE_OFF R  rdur=0.222917s
point[96]: 40.9292   NOTE_ON  C4 rdur=0.35625s
point[97]: 41.2854   NOTE_OFF R  rdur=0.1s
point[98]: 41.3854   NOTE_ON  B3 rdur=0.375s
point[99]: 41.7604   NOTE_OFF R  rdur=0.152083s
point[100]: 41.9125  NOTE_ON  A3 rdur=1.01458s
point[101]: 42.9271  NOTE_ON  F3 rdur=1.13125s
point[102]: 44.0583  NOTE_OFF R  rdur=0.0229167s
point[103]: 44.0812  NOTE_ON  B3 rdur=0.8375s
point[104]: 44.9188  NOTE_OFF R  rdur=0.120833s
point[105]: 45.0396  NOTE_ON  E3 rdur=1.30625s
point[106]: 46.3458  NOTE_OFF R 
```
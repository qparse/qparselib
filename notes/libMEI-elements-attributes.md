# libMEI classes

- definitions of classes:
  - [github.com/DDMAL/libmei/src/modules/shared.h](https://github.com/DDMAL/libmei/blob/d7a4385adf01697509b0019f011e6624c29121ab/src/modules/shared.h)
  - [github.com/DDMAL/libmei/src/modules/header.h](https://github.com/DDMAL/libmei/blob/d7a4385adf01697509b0019f011e6624c29121ab/src/modules/header.h)
  - [github.com/DDMAL/libmei/src/modules/cmn.h](https://github.com/DDMAL/libmei/blob/d7a4385adf01697509b0019f011e6624c29121ab/src/modules/cmn.h)
  - [github.com/DDMAL/libmei/src/modules/cmnornaments.h](https://github.com/DDMAL/libmei/blob/d7a4385adf01697509b0019f011e6624c29121ab/src/modules/cmnornaments.h)
- definitions of MixIns:
  - [github.com/DDMAL/libmei/src/modules/sharedmixins.h](https://github.com/DDMAL/libmei/blob/d7a4385adf01697509b0019f011e6624c29121ab/src/modules/sharedmixins.h)
  - [github.com/DDMAL/libmei/src/modules/cmnmixins.h](https://github.com/DDMAL/libmei/blob/d7a4385adf01697509b0019f011e6624c29121ab/src/modules/cmnmixins.h)
  - [github.com/DDMAL/libmei/src/modules/cmnornamentsmixins.h](https://github.com/DDMAL/libmei/blob/d7a4385adf01697509b0019f011e6624c29121ab/src/modules/cmnornamentsmixins.h)




# MixIns
general form, for a given MEI `Attribute` :

```C++
class AttributeMixIn 
{
    public:
        explicit AttributeMixIn(MeiElement *b);
        virtual ~AttributeMixIn();

		MeiAttribute* getAttribute();
        void setAttribute(std::string val);
        bool hasAttribute();
        void removeAttribute();
    private:
        MeiElement *b;
};
```

## common MixIns




- [ ] `MeasurenumbersMixIn`
- Indicates whether measure numbers should be displayed.
Attribute = `MnumVisible`



# Events

## Beam

```C++
/** \brief A container for a series of explicitly beamed events that begins and ends
 *  entirely within a measure.
 */
class MEI_EXPORT Beam : public MeiElement 
{
    public:
        Beam();
        Beam(const Beam& other);
        virtual ~Beam();

/* include <beam> */

        CommonMixIn    m_Common;
        CommonPartMixIn    m_CommonPart;
        FacsimileMixIn    m_Facsimile;
        TimestampMusicalMixIn    m_TimestampMusical;
        TimestampPerformedMixIn    m_TimestampPerformed;
        StaffidentMixIn    m_Staffident;
        LayeridentMixIn    m_Layerident;
        BeamedwithMixIn    m_Beamedwith;
        ColorMixIn    m_Color;
        BeamrendMixIn    m_Beamrend;
        CommonAnlMixIn    m_CommonAnl;
        AlignmentMixIn    m_Alignment;

    private:
        REGISTER_DECLARATION(Beam);
};
```
- [ ] `CommonMixIn`
- Provides a number-like designation for an element
Attribute = `N`

- [ ] `CommonPartMixIn`
- Provides a name or label for an element.
Attribute = `Label`
- ???
Attribute = `Base`

- [ ] `BeamedwithMixIn` `m_Beamedwith`
In the case of cross-staff beams, the beam.with attribute is used to indicate which staff the beam is connected to; that is, the staff above or the staff below.
Attribute = `BeamWith`
Value: see https://music-encoding.org/guidelines/v4/data-types/data.otherstaff.html
  - `above` (the staff immediately above) 
  - `below` (the staff immediately below)

- [ ] `BeamrendMixIn` `m_Beamrend`
- Records the appearance and usually the function of the bar line.
  (Captures whether a beam is "feathered" and in which direction)
Attribute = `form` [opt]
Value: see https://music-encoding.org/guidelines/v4/attribute-classes/att.beamrend.html
  - `acc` (*accelerando*) indicates that the secondary beams get progressively closer together toward the end of the beam, 
  - `mixed` (*mixed acc and rit*) for beams that are "feathered" in both directions, 
  - `rit` (*ritardando*) means that the secondary beams become progressively more distant toward the end of the beam, 
  - `norm` (*normal*) indicates that the secondary beams are equidistant along the course of the beam.
- Records the placement of the beam relative to the events it affects
Attribute = `place`  [opt]
Value: see https://music-encoding.org/guidelines/v4/data-types/data.beamplace.html
  - `above` (the beam is above the material it affects)
  - `below` (the beam is below the material it affects)
  - `mixed` (the below is above and below the material it affects)
- Indicates presence of slash through the beam
Attribute = `slash`  [opt]
Value: boolean `true` | `false` see https://music-encoding.org/guidelines/v4/data-types/data.boolean.html
- Records the slope of the beam.
Attribute = `slope` [opt]
Value: decimal

- [ ] `CommonAnlMixIn` cf. [github.com/DDMAL/libmei/src/modules/analysismixins.h](https://github.com/DDMAL/libmei/blob/d7a4385adf01697509b0019f011e6624c29121ab/src/modules/analysismixins.h)


## Beam Span

```C++
/** \brief (beam span) – Alternative element for explicitly encoding beams, particularly
 *  those which extend across bar lines.
 */
class MEI_EXPORT BeamSpan : public MeiElement {
    public:
        BeamSpan();
        BeamSpan(const BeamSpan& other);
        virtual ~BeamSpan();

/* include <beamSpan> */

        CommonMixIn    m_Common;
        CommonPartMixIn    m_CommonPart;
        FacsimileMixIn    m_Facsimile;
        PlistMixIn    m_Plist;
        TargetevalMixIn    m_Targeteval;
        TimestampMusicalMixIn    m_TimestampMusical;
        TimestampPerformedMixIn    m_TimestampPerformed;
        StaffidentMixIn    m_Staffident;
        LayeridentMixIn    m_Layerident;
        AugmentdotsMixIn    m_Augmentdots;
        BeamedwithMixIn    m_Beamedwith;
        DurationAdditiveMixIn    m_DurationAdditive;
        StartendidMixIn    m_Startendid;
        StartidMixIn    m_Startid;
        Timestamp2MusicalMixIn    m_Timestamp2Musical;
        ColorMixIn    m_Color;
        BeamrendMixIn    m_Beamrend;
        DurationPerformedMixIn    m_DurationPerformed;
        CommonAnlMixIn    m_CommonAnl;
        AlignmentMixIn    m_Alignment;
        TypedMixIn    m_Typed;

    private:
        REGISTER_DECLARATION(BeamSpan);
};
```

- [ ] `PlistMixIn` `m_Plist`
- Contains a space separated list of references that identify active participants in a collection/relationship, such as notes under a phrase mark; that is, the entities pointed "from".
Attribute = `Plist`
		
		

- [ ] `DurationAdditiveMixIn` `m_DurationAdditive`
- Records duration using optionally dotted, relative durational values provided by the data.DURATION datatype.
When the duration is "irrational", as is sometimes the case with tuplets, multiple space-separated values that add up to the total duration may be used. 
When dotted values are present, the dots attribute must be ignored.
Attribute = `getDur`


​		
## Tuplet

```C++
/** \brief A group of notes with "irregular" (sometimes called "irrational") rhythmic
 *  values, for example, three notes in the time normally occupied by two or nine in
 *  the time of five.
 */
class MEI_EXPORT Tuplet : public MeiElement 
{
    public:
        Tuplet();
        Tuplet(const Tuplet& other);
        virtual ~Tuplet();

/* include <tuplet> */

        CommonMixIn    m_Common;
        CommonPartMixIn    m_CommonPart;
        FacsimileMixIn    m_Facsimile;
        TimestampMusicalMixIn    m_TimestampMusical;
        TimestampPerformedMixIn    m_TimestampPerformed;
        StaffidentMixIn    m_Staffident;
        LayeridentMixIn    m_Layerident;
        BeamedwithMixIn    m_Beamedwith;
        AugmentdotsMixIn    m_Augmentdots;
        DurationAdditiveMixIn    m_DurationAdditive;
        DurationRatioMixIn    m_DurationRatio;
        StartendidMixIn    m_Startendid;
        StartidMixIn    m_Startid;
        TupletVisMixIn    m_TupletVis;
        ColorMixIn    m_Color;
        NumberplacementMixIn    m_Numberplacement;
        DurationPerformedMixIn    m_DurationPerformed;
        CommonAnlMixIn    m_CommonAnl;
        AlignmentMixIn    m_Alignment;

    private:
        REGISTER_DECLARATION(Tuplet);
};
```

- [ ] `StartendidMixIn` `m_Startendid`
- Indicates the final element in a sequence of events to which the feature applies.
Attribute = `Endid`


- [ ] `TupletVisMixIn`  `m_TupletVis`
- Used to state where a tuplet bracket will be placed in relation to the note heads.
Attribute = `BracketPlace`
Value: `above`| `below` see https://music-encoding.org/guidelines/v4/data-types/data.staffrel.basic.html
- States whether a bracket should be rendered with a tuplet.
Attribute = `BracketVisible`
Value: Bool `true` | `false` see https://music-encoding.org/guidelines/v4/data-types/data.boolean.html
- Determines if the tuplet duration is visible.
Attribute = `DurVisible`
Value: Bool `true` | `false` see https://music-encoding.org/guidelines/v4/data-types/data.boolean.html
- Controls how the num:numbase ratio is to be displayed.
Attribute `NumFormat`
Values: see https://music-encoding.org/guidelines/v4/elements/tuplet.html
  - `count` (only the num attribute is displayed, e.g., '7'.)
  - `ratio` (both the num and numbase attributes are displayed, e.g., '7:4'.)

- [ ] `DurationRatioMixIn` `m_DurationRatio`
see Note
Used for the num:numbase ratio label?
In https://music-encoding.org/guidelines/v4/elements/tuplet.html, 
`num` nd `numbase` are explicit attributes of `Tuplet`, with value type positiveInteger.

## Tuplet Span

```C++
/** \brief (tuplet span) – Alternative element for encoding tuplets, especially useful
 *  for tuplets that extend across bar lines.
 */
class MEI_EXPORT TupletSpan : public MeiElement 
{
    public:
        TupletSpan();
        TupletSpan(const TupletSpan& other);
        virtual ~TupletSpan();

/* include <tupletSpan> */

        CommonMixIn    m_Common;
        CommonPartMixIn    m_CommonPart;
        FacsimileMixIn    m_Facsimile;
        PlistMixIn    m_Plist;
        TargetevalMixIn    m_Targeteval;
        TimestampMusicalMixIn    m_TimestampMusical;
        TimestampPerformedMixIn    m_TimestampPerformed;
        StaffidentMixIn    m_Staffident;
        LayeridentMixIn    m_Layerident;
        BeamedwithMixIn    m_Beamedwith;
        AugmentdotsMixIn    m_Augmentdots;
        DurationAdditiveMixIn    m_DurationAdditive;
        DurationRatioMixIn    m_DurationRatio;
        StartendidMixIn    m_Startendid;
        StartidMixIn    m_Startid;
        Timestamp2MusicalMixIn    m_Timestamp2Musical;
        TupletVisMixIn    m_TupletVis;
        ColorMixIn    m_Color;
        NumberplacementMixIn    m_Numberplacement;
        DurationPerformedMixIn    m_DurationPerformed;
        CommonAnlMixIn    m_CommonAnl;
        AlignmentMixIn    m_Alignment;
        TypedMixIn    m_Typed;

    private:
        REGISTER_DECLARATION(TupletSpan);
};
```


## Note

```C++
/** \brief A single pitched event.
 */
class MEI_EXPORT Note : public MeiElement 
{
    public:
        Note();
        Note(const Note& other);
        virtual ~Note();

/* include <note> */

        CommonMixIn    m_Common;
        CommonPartMixIn    m_CommonPart;
        FacsimileMixIn    m_Facsimile;
        TimestampMusicalMixIn    m_TimestampMusical;
        TimestampPerformedMixIn    m_TimestampPerformed;
        StaffidentMixIn    m_Staffident;
        LayeridentMixIn    m_Layerident;
        AccidentalMixIn    m_Accidental;
        ArticulationMixIn    m_Articulation;
        AugmentdotsMixIn    m_Augmentdots;
        DurationMusicalMixIn    m_DurationMusical;
        FermatapresentMixIn    m_Fermatapresent;
        PitchMixIn    m_Pitch;
        OctaveMixIn    m_Octave;
        SyltextMixIn    m_Syltext;
        SlurpresentMixIn    m_Slurpresent;
        TiepresentMixIn    m_Tiepresent;
        TupletpresentMixIn    m_Tupletpresent;
        BeamedMixIn    m_Beamed;
        LvpresentMixIn    m_Lvpresent;
        OrnamMixIn    m_Ornam;
        NoteLogMensuralMixIn    m_NoteLogMensural;
        AltsymMixIn    m_Altsym;
        ColorMixIn    m_Color;
        ColorationMixIn    m_Coloration;
        EnclosingcharsMixIn    m_Enclosingchars;
        ExtsymMixIn    m_Extsym;
        NoteheadsMixIn    m_Noteheads;
        RelativesizeMixIn    m_Relativesize;
        StafflocMixIn    m_Staffloc;
        StemsMixIn    m_Stems;
        StemsCmnMixIn    m_StemsCmn;
        TypographyMixIn    m_Typography;
        VisibilityMixIn    m_Visibility;
        VisualoffsetHoMixIn    m_VisualoffsetHo;
        VisualoffsetToMixIn    m_VisualoffsetTo;
        XyMixIn    m_Xy;
        BeamsecondaryMixIn    m_Beamsecondary;
        NoteGesMixIn    m_NoteGes;
        AccidentalPerformedMixIn    m_AccidentalPerformed;
        ArticulationPerformedMixIn    m_ArticulationPerformed;
        DurationPerformedMixIn    m_DurationPerformed;
        InstrumentidentMixIn    m_Instrumentident;
        MidivelocityMixIn    m_Midivelocity;
        NoteGesCmnMixIn    m_NoteGesCmn;
        GracedMixIn    m_Graced;
        DurationRatioMixIn    m_DurationRatio;
        NoteGesTablatureMixIn    m_NoteGesTablature;
        CommonAnlMixIn    m_CommonAnl;
        AlignmentMixIn    m_Alignment;
        HarmonicfunctionMixIn    m_Harmonicfunction;
        IntervalmelodicMixIn    m_Intervalmelodic;
        MelodicfunctionMixIn    m_Melodicfunction;
        PitchclassMixIn    m_Pitchclass;
        SolfaMixIn    m_Solfa;

    private:
        REGISTER_DECLARATION(Note);
};
```

- [ ] `TimestampMusicalMixIn `m_TimestampMusical`
- Encodes the onset time in terms of musical time, i.e., beats[.fractional_beat_part].
Attribute = `Tstamp`

- [ ] `StaffidentMixIn` `m_Staffident`
- Signifies the staff on which a notated event occurs or to which a control event applies.
Mandatory when applicable.
Attribute = `Staff`

- [ ] `LayeridentMixIn` `m_Layerident`
- Identifies the layer to which a feature applies.
Attribute = `Layer`

- [ ] `AccidentalMixIn` `m_Accidental`
- Captures a written accidental
Attribute = `Accid`
Values: https://music-encoding.org/guidelines/v4/data-types/data.accidental.written.html
  
- [ ] `ArticulationMixIn` `m_Articulation`
- Encodes the written articulation(s).
Articulations are normally encoded in order from the note head outward; that is, away from the stem. See additional notes at `att.vis.note`. 
Only articulations should be encoded in the artic attribute; for example, fingerings should be encoded using the `<fingering>` element.
Attribute = `Artic`
Value: https://music-encoding.org/guidelines/v4/data-types/data.articulation.html
Element `<artic>`: https://music-encoding.org/guidelines/v4/elements/artic.html

- [ ] `AugmentdotsMixIn` `m_Augmentdots`
- Records the number of augmentation dots required by a dotted duration.
Attribute = `Dots`
Value: 1-4 see https://music-encoding.org/guidelines/v4/data-types/data.augmentdot.html

- [ ] `DurationMusicalMixIn` `m_DurationMusical`
- Records duration using optionally dotted, relative durational values provided by the data.DURATION datatype.
When the duration is "irrational", as is sometimes the case with tuplets, multiple space-separated values that add up to the total duration may be used. 
When dotted values are present, the dots attribute must be ignored.
Attribute = `Dur`
Values = `long`, `breve`, 1 (whole note), 2 (half note), 4 (quarter note), 8, ..., 2048. see https://music-encoding.org/guidelines/v4/attribute-classes/att.duration.additive.html

see also `DurationAdditiveMixIn`.
In the doc of [libmei/src/modules/sharedmixins.h](https://github.com/DDMAL/libmei/blob/d7a4385adf01697509b0019f011e6624c29121ab/src/modules/sharedmixins.h)
the definitions of `DurationMusicalMixIn` and `DurationAdditiveMixIn` are the same (copy paste?).
But in MEI doc, there is a difference between the definitions of the following:
- [duration.logical](https://music-encoding.org/guidelines/v4/attribute-classes/att.duration.logical.html)
  for elements `<note>`, `<rest>`, `<chord>`, `<space>`
  single value (power of 2) = 2^(nb of beams + 2)
- [duration.additive](https://music-encoding.org/guidelines/v4/attribute-classes/att.duration.additive.html)
  for elements like `<beamSpan>`, `<dir>`, `<dynam>`, `<glissando>`, `<hairpin>`, `<ornam>`, `<tuplet>`, `<tupletSpan>`... but none of the above.
  sequence of single values (to represent a sum)

we assume in MEI.export that 
- `DurationMusicalMixIn` = duration.logical
- `DurationAdditiveMixIn` = duration.additive
- [ ] post issue in libMEI 

- [ ] `FermatapresentMixIn` `m_Fermatapresent`
-  Indicates the attachment of a fermata to this element.
If visual information about the fermata needs to be recorded, then a `<fermata>` element should be employed instead.
Attribute = `Fermata`

- [ ] `PitchMixIn` `m_Pitch`
- Contains a written pitch name.
Attribute = Pname
Values: `a`-`g`, see https://music-encoding.org/guidelines/v4/data-types/data.pitchname.html

- [ ] `OctaveMixIn` `m_Octave`
- Captures written octave information.
Attribute = `Oct`
Value: 0-9 see https://music-encoding.org/guidelines/v4/data-types/data.octave.html

- [ ] `TiepresentMixIn` `m_Tiepresent`
- Indicates that this element participates in a tie.
If visual information about the tie needs to be recorded, then a `<tie>` element should be employed.
Attribute = `Tie`
Values: `i` | `m` | `t`  see https://music-encoding.org/guidelines/v4/data-types/data.tie.html

- [ ] `TupletpresentMixIn` `m_Tupletpresent`
- Indicates that this feature participates in a tuplet.
If visual information about the tuplet needs to be recorded, then a `<tuplet>` element should be employed.
Attribute = `Tuplet`
Values: [`i` | `m` | `t`] [1-6] see https://music-encoding.org/guidelines/v4/data-types/data.tuplet.html
see also `<tuplet>` element https://music-encoding.org/guidelines/v4/elements/tuplet.html

- [ ] `BeamedMixIn` `m_Beamed` 
  not found
  = `BeamPresent` ?
- Indicates that this event is "under a beam".
Attribute = `Beam`
Value: [`i` | `m` | `t`] [1-6] see https://music-encoding.org/guidelines/v4/data-types/data.beam.html

- [ ] `OrnamMixIn` `m_Ornam`
- Indicates that this element has an attached ornament.
If visual information about the ornament is needed, then one of the elements that represents an ornament (mordent, trill, or turn) should be employed.
Attribute = `Ornam`
Value: [`A`|`a`|`b`|`I`|`i`|`K`|`k`|`M`|`m`|`N`|`n`|`S`|`s`|`T`|`t`|`O`]|(`A`|`a`|`S`|`s`|`K`|`k`)?(`T`|`t`|`M`|`m`)(`I`|`i`|`S`|`s`)? see https://music-encoding.org/guidelines/v4/data-types/data.ornam.cmn.html

- [ ] `NoteheadsMixIn  m_Noteheads`
- Captures the overall color of a notehead.
Attribute = `HeadColor`
- Describes how/if the notehead is filled.
Attribute = `HeadFill`
- Captures the fill color of a notehead if different from the overall note color.
Attribute = `HeadFillcolor`
- Records any additional symbols applied to the notehead.
Attribute = `HeadMod`
- Describes rotation applied to the basic notehead shape.
A positive value rotates the notehead in a counter-clockwise fashion, while negative values produce clockwise rotation.
Attribute = `HeadRotation`
- Used to override the head shape normally used for the given duration.
Attribute = `HeadShape`
- Indicates if a feature should be rendered when the notation is presented graphically or sounded when it is presented in an aural form.
Attribute = `HeadVisible`

- [ ] `StafflocMixIn m_Staffloc`
- Holds the staff location of the feature.
Attribute = `Loc`
Value: int see https://music-encoding.org/guidelines/v4/data-types/data.staffloc.html

- [ ] `StemsMixIn` `m_Stems`
- Describes the direction of a stem.
Attribute = `StemDir`
- Encodes the stem length.
Attribute = `StemLen`
- Encodes any stem "modifiers"; that is, symbols rendered on the stem, such as tremolo or Sprechstimme indicators.
Attribute = `StemMod`
- Records the position of the stem in relation to the note head(s).
Attribute = `StemPos`
- Records the output x coordinate of the stem's attachment point.
Attribute = `StemX`
- Records the output y coordinate of the stem's attachment point.
Attribute = `StemY`

- [ ] `StemsCmnMixIn` `m_StemsCmn`
- Contains an indication of which staff a note or chord that logically belongs to the current staff should be visually placed on; that is, the one above or the one below.
Attribute = `StemWith`

- [ ] [`BeamsecondaryMixIn`](https://music-encoding.org/guidelines/v4/attribute-classes/att.beamsecondary.html) `m_Beamsecondary`
- Presence of this attribute indicates that the secondary beam should be broken following this note/chord.
Attribute `Breaksec`
Value: positive Integer. The value of the attribute records the number of beams which should remain unbroken.

- [ ] `DurationPerformedMixIn` `m_DurationPerformed`
-  Records performed duration information that differs from the written duration.
Its value may be expressed in several forms; that is, ppq (MIDI clicks and MusicXML 'divisions'), Humdrum `**recip` values, beats, seconds, or mensural duration values.
Attribute `DurGes`
Value: same as duration.logical, see https://music-encoding.org/guidelines/v4/data-types/data.duration.gestural.html
see https://music-encoding.org/guidelines/v4/attribute-classes/att.duration.gestural.html


- [ ] `GracedMixIn` `m_Graced`
- Marks a note or chord as a "grace" (without a definitive written duration) and records from which other note/chord it should "steal" time.
Attribute = `Grace`
- Records the amount of time to be "stolen" from a non-grace note/chord.
Attribute = `GraceTime`

- [ ] `DurationRatioMixIn` `m_DurationRatio`
- Along with numbase, describes duration as a ratio.
   num is the first value in the ratio, while numbase is the second.
Attribute = `Num`
- Along with num, describes duration as a ratio.
num is the first value in the ratio, while numbase is the second.
Attribute = `Numbase`

Attributes that describe duration as a ratio.
- [ ] ratio of what?



## Accidental
see element `<accid>` https://music-encoding.org/guidelines/v4/elements/accid.html
can be contained in `<note>`


```C++
/** \brief (accidental) – Records a temporary alteration to the pitch of a note.
 */
class MEI_EXPORT Accid : public MeiElement 
{
    public:
        Accid();
        Accid(const Accid& other);
        virtual ~Accid();

/* include <accid> */

        CommonMixIn    m_Common;
        CommonPartMixIn    m_CommonPart;
        FacsimileMixIn    m_Facsimile;
        AccidLogMixIn    m_AccidLog;
        AccidentalMixIn    m_Accidental;
        PlistMixIn    m_Plist;
        TargetevalMixIn    m_Targeteval;
        TimestampMusicalMixIn    m_TimestampMusical;
        TimestampPerformedMixIn    m_TimestampPerformed;
        StaffidentMixIn    m_Staffident;
        LayeridentMixIn    m_Layerident;
        AltsymMixIn    m_Altsym;
        ColorMixIn    m_Color;
        EnclosingcharsMixIn    m_Enclosingchars;
        ExtsymMixIn    m_Extsym;
        PlacementMixIn    m_Placement;
        StafflocMixIn    m_Staffloc;
        TypographyMixIn    m_Typography;
        VisualoffsetHoMixIn    m_VisualoffsetHo;
        VisualoffsetVoMixIn    m_VisualoffsetVo;
        XyMixIn    m_Xy;
        StafflocPitchedMixIn    m_StafflocPitched;
        AccidentalPerformedMixIn    m_AccidentalPerformed;
        CommonAnlMixIn    m_CommonAnl;
        AlignmentMixIn    m_Alignment;

    private:
        REGISTER_DECLARATION(Accid);
};
```

- [ ] `TargetevalMixIn` `m_Targeteval`
- Specifies the intended meaning when a participant in a relationship is itself a pointer.
Attribute = `Evaluate`


```C++
/** \brief Dot of augmentation or division.
 */
class MEI_EXPORT Dot : public MeiElement 
{
    public:
        Dot();
        Dot(const Dot& other);
        virtual ~Dot();

/* include <dot> */

        CommonMixIn    m_Common;
        CommonPartMixIn    m_CommonPart;
        FacsimileMixIn    m_Facsimile;
        DotLogMixIn    m_DotLog;
        PlistMixIn    m_Plist;
        TargetevalMixIn    m_Targeteval;
        TimestampMusicalMixIn    m_TimestampMusical;
        TimestampPerformedMixIn    m_TimestampPerformed;
        StaffidentMixIn    m_Staffident;
        LayeridentMixIn    m_Layerident;
        AltsymMixIn    m_Altsym;
        ColorMixIn    m_Color;
        ExtsymMixIn    m_Extsym;
        StafflocMixIn    m_Staffloc;
        TypographyMixIn    m_Typography;
        VisualoffsetHoMixIn    m_VisualoffsetHo;
        VisualoffsetVoMixIn    m_VisualoffsetVo;
        XyMixIn    m_Xy;
        StafflocPitchedMixIn    m_StafflocPitched;
        CommonAnlMixIn    m_CommonAnl;
        AlignmentMixIn    m_Alignment;

    private:
        REGISTER_DECLARATION(Dot);
};	
```

- [ ] `DotLogMixIn` `m_DotLog`
Records the appearance and usually the function of the bar line.
Attribute = `Form`


## Rest

```C++
/** \brief A non-sounding event found in the source being transcribed.
 */
class MEI_EXPORT Rest : public MeiElement 
{
    public:
        Rest();
        Rest(const Rest& other);
        virtual ~Rest();

/* include <rest> */

        CommonMixIn    m_Common;
        CommonPartMixIn    m_CommonPart;
        FacsimileMixIn    m_Facsimile;
        AugmentdotsMixIn    m_Augmentdots;
        TimestampMusicalMixIn    m_TimestampMusical;
        TimestampPerformedMixIn    m_TimestampPerformed;
        StaffidentMixIn    m_Staffident;
        LayeridentMixIn    m_Layerident;
        DurationMusicalMixIn    m_DurationMusical;
        FermatapresentMixIn    m_Fermatapresent;
        TupletpresentMixIn    m_Tupletpresent;
        BeamedMixIn    m_Beamed;
        AltsymMixIn    m_Altsym;
        ColorMixIn    m_Color;
        EnclosingcharsMixIn    m_Enclosingchars;
        RelativesizeMixIn    m_Relativesize;
        RestVisMensuralMixIn    m_RestVisMensural;
        StafflocMixIn    m_Staffloc;
        StafflocPitchedMixIn    m_StafflocPitched;
        TypographyMixIn    m_Typography;
        VisualoffsetHoMixIn    m_VisualoffsetHo;
        VisualoffsetToMixIn    m_VisualoffsetTo;
        VisualoffsetVoMixIn    m_VisualoffsetVo;
        XyMixIn    m_Xy;
        DurationPerformedMixIn    m_DurationPerformed;
        InstrumentidentMixIn    m_Instrumentident;
        DurationRatioMixIn    m_DurationRatio;
        CommonAnlMixIn    m_CommonAnl;
        AlignmentMixIn    m_Alignment;

    private:
        REGISTER_DECLARATION(Rest);
};
```


- [ ] `DurationMusicalMixIn` `m_DurationMusical`

- [ ] `FermatapresentMixIn` `m_Fermatapresent`
see `Note`

- [ ] `TupletpresentMixIn` `m_Tupletpresent`
see `Note`

- [ ] `BeamedMixIn` `m_Beamed`
see `Note`

- [ ] `OrnamMixIn` `m_Ornam`
see `Note`

- [ ] `NoteheadsMixIn  m_Noteheads`
see `Note`

- [ ] `RelativesizeMixIn m_Relativesize`

- [ ] `StafflocMixIn m_Staffloc`
see `Note`

- [ ] `StafflocPitchedMixIn` `m_StafflocPitched`

- [ ] `DurationRatioMixIn` `m_DurationRatio`
see `Note`



- [ ] `MultinummeasuresMixIn`
- Indicates whether programmatically calculated counts of multiple measures of rest (mRest) and whole measure repeats (mRpt) in parts should be rendered.
Attribute = `MultiNumber`



## Chord


```C++
/** \brief A simultaneous sounding of two or more notes in the same layer *with the same
 *  duration*.
 */
class MEI_EXPORT Chord : public MeiElement {
    public:
        Chord();
        Chord(const Chord& other);
        virtual ~Chord();

/* include <chord> */

        CommonMixIn    m_Common;
        CommonPartMixIn    m_CommonPart;
        FacsimileMixIn    m_Facsimile;
        TimestampMusicalMixIn    m_TimestampMusical;
        TimestampPerformedMixIn    m_TimestampPerformed;
        StaffidentMixIn    m_Staffident;
        LayeridentMixIn    m_Layerident;
        ArticulationMixIn    m_Articulation;
        AugmentdotsMixIn    m_Augmentdots;
        DurationMusicalMixIn    m_DurationMusical;
        FermatapresentMixIn    m_Fermatapresent;
        SyltextMixIn    m_Syltext;
        SlurpresentMixIn    m_Slurpresent;
        TiepresentMixIn    m_Tiepresent;
        TupletpresentMixIn    m_Tupletpresent;
        BeamedMixIn    m_Beamed;
        LvpresentMixIn    m_Lvpresent;
        OrnamMixIn    m_Ornam;
        ChordVisMixIn    m_ChordVis;
        AltsymMixIn    m_Altsym;
        ColorMixIn    m_Color;
        EnclosingcharsMixIn    m_Enclosingchars;
        ExtsymMixIn    m_Extsym;
        RelativesizeMixIn    m_Relativesize;
        StemsMixIn    m_Stems;
        StemsCmnMixIn    m_StemsCmn;
        TypographyMixIn    m_Typography;
        VisibilityMixIn    m_Visibility;
        VisualoffsetHoMixIn    m_VisualoffsetHo;
        VisualoffsetToMixIn    m_VisualoffsetTo;
        XyMixIn    m_Xy;
        BeamsecondaryMixIn    m_Beamsecondary;
        ArticulationPerformedMixIn    m_ArticulationPerformed;
        DurationPerformedMixIn    m_DurationPerformed;
        InstrumentidentMixIn    m_Instrumentident;
        GracedMixIn    m_Graced;
        CommonAnlMixIn    m_CommonAnl;
        AlignmentMixIn    m_Alignment;

    private:
        REGISTER_DECLARATION(Chord);
};
```


- [ ] `FermatapresentMixIn` `m_Fermatapresent`
see `Space`


## Space


```C++
/** \brief A placeholder used to fill an incomplete measure, layer, etc.
 * 
 *  most often so that the combined duration of the events equals the number of
 *  beats in the measure.
 */
class MEI_EXPORT Space : public MeiElement 
{
    public:
        Space();
        Space(const Space& other);
        virtual ~Space();

/* include <space> */

        CommonMixIn    m_Common;
        CommonPartMixIn    m_CommonPart;
        FacsimileMixIn    m_Facsimile;
        AugmentdotsMixIn    m_Augmentdots;
        TimestampMusicalMixIn    m_TimestampMusical;
        TimestampPerformedMixIn    m_TimestampPerformed;
        StaffidentMixIn    m_Staffident;
        LayeridentMixIn    m_Layerident;
        DurationMusicalMixIn    m_DurationMusical;
        FermatapresentMixIn    m_Fermatapresent;
        TupletpresentMixIn    m_Tupletpresent;
        BeamedMixIn    m_Beamed;
        SpaceVisMixIn    m_SpaceVis;
        DurationPerformedMixIn    m_DurationPerformed;
        CommonAnlMixIn    m_CommonAnl;
        AlignmentMixIn    m_Alignment;

    private:
        REGISTER_DECLARATION(Space);
};
```

- [ ] `AugmentdotsMixIn` `m_Augmentdots`
see `Note`

- [ ] `DurationMusicalMixIn` `m_DurationMusical`
see `Note`

- [ ] `FermatapresentMixIn` `m_Fermatapresent`
see `Note`

- [ ] `TupletpresentMixIn` `m_Tupletpresent`
see `Note`

- [ ] `BeamedMixIn` `m_Beamed`
see `Note`


## Ornament

```C++
/** \brief An element indicating an ornament that is not a mordent, turn, or trill.
 */
class MEI_EXPORT Ornam : public MeiElement 
{
    public:
        Ornam();
        Ornam(const Ornam& other);
        virtual ~Ornam();

/* include <ornam> */

        CommonMixIn    m_Common;
        CommonPartMixIn    m_CommonPart;
        FacsimileMixIn    m_Facsimile;
        PlistMixIn    m_Plist;
        TargetevalMixIn    m_Targeteval;
        TimestampMusicalMixIn    m_TimestampMusical;
        TimestampPerformedMixIn    m_TimestampPerformed;
        StaffidentMixIn    m_Staffident;
        LayeridentMixIn    m_Layerident;
        DurationAdditiveMixIn    m_DurationAdditive;
        OrnamentaccidMixIn    m_Ornamentaccid;
        StartendidMixIn    m_Startendid;
        StartidMixIn    m_Startid;
        Timestamp2MusicalMixIn    m_Timestamp2Musical;
        ColorMixIn    m_Color;
        PlacementMixIn    m_Placement;
        VisualoffsetHoMixIn    m_VisualoffsetHo;
        VisualoffsetToMixIn    m_VisualoffsetTo;
        VisualoffsetVoMixIn    m_VisualoffsetVo;
        Visualoffset2HoMixIn    m_Visualoffset2Ho;
        Visualoffset2ToMixIn    m_Visualoffset2To;
        XyMixIn    m_Xy;
        DurationPerformedMixIn    m_DurationPerformed;
        CommonAnlMixIn    m_CommonAnl;
        AlignmentMixIn    m_Alignment;
        TypedMixIn    m_Typed;

    private:
        REGISTER_DECLARATION(Ornam);
};
```

- [ ] `DurationAdditiveMixIn` `m_DurationAdditive`
- Records duration using optionally dotted, relative durational values provided by the `data.DURATION` datatype.
When a duration cannot be represented as a single power-of-two value (so called "irrational"; this happens for tuplets or spanning elements), multiple space-separated values that add up to the total duration may be used.
When dotted values are present, the dots attribute must be ignored.
see https://music-encoding.org/guidelines/v4/attribute-classes/att.duration.additive.html
Attribute = `Dur`
Values =  see https://music-encoding.org/guidelines/v4/attribute-classes/att.duration.additive.html
see also https://music-encoding.org/guidelines/v4/attribute-classes/att.duration.logical.html


# Markups

## Clef

```C++
/** \brief Indication of the exact location of a particular note on the staff and,
 *  therefore, the other notes as well.
 */
class MEI_EXPORT Clef : public MeiElement 
{
    public:
        Clef();
        Clef(const Clef& other);
        virtual ~Clef();

/* include <clef> */

        CommonMixIn    m_Common;
        CommonPartMixIn    m_CommonPart;
        TimestampMusicalMixIn    m_TimestampMusical;
        TimestampPerformedMixIn    m_TimestampPerformed;
        StaffidentMixIn    m_Staffident;
        LayeridentMixIn    m_Layerident;
        FacsimileMixIn    m_Facsimile;
        CommonAnlMixIn    m_CommonAnl;
        AlignmentMixIn    m_Alignment;
        ClefLogMixIn    m_ClefLog;
        ClefshapeMixIn    m_Clefshape;
        LinelocMixIn    m_Lineloc;
        OctaveMixIn    m_Octave;
        OctavedisplacementMixIn    m_Octavedisplacement;
        AltsymMixIn    m_Altsym;
        ColorMixIn    m_Color;
        ExtsymMixIn    m_Extsym;
        TypographyMixIn    m_Typography;

    private:
        REGISTER_DECLARATION(Clef);
};
```

- [ ] `ClefshapeMixIn` `m_Clefshape`
- Describes a clef's shape.
Attribute = `Shape`
- [ ] `OctaveMixIn` `m_Octave`
see `Note`
- [ ] `OctavedisplacementMixIn` `m_Octavedisplacement`
- Records the amount of octave displacement.
Attribute = `Dis`
- Records the direction of octave displacement.
  Attribute `DisPlace`

- [ ] `CleffingLogMixIn` ???
- Encodes a value for the clef symbol.
   Attribute = `ClefShape`
- Contains a default value for the position of the clef.
The value must be in the range between 1 and the number of lines on the staff.
The numbering of lines starts with the lowest line of the staff.
Attribute = `ClefLine`
- Records the amount of octave displacement to be applied to the clef.
Attribute = `ClefDis`
- Records the direction of octave displacement to be applied to the clef.
Attribute = `ClefDisPlace`



## Key Signature

```C++
/** \brief (key signature) – Written key signature.
 */
class MEI_EXPORT KeySig : public MeiElement 
{
    public:
        KeySig();
        KeySig(const KeySig& other);
        virtual ~KeySig();

/* include <keySig> */

        CommonMixIn    m_Common;
        CommonPartMixIn    m_CommonPart;
        FacsimileMixIn    m_Facsimile;
        CommonAnlMixIn    m_CommonAnl;
        AlignmentMixIn    m_Alignment;
        KeySigLogMixIn    m_KeySigLog;
        AccidentalMixIn    m_Accidental;
        PitchMixIn    m_Pitch;
        KeySigVisMixIn    m_KeySigVis;
        VisibilityMixIn    m_Visibility;

    private:
        REGISTER_DECLARATION(KeySig);
};
```

```C++
/** \brief (key accidental) – Accidental in a key signature.
 */
class MEI_EXPORT KeyAccid : public MeiElement {
    public:
        KeyAccid();
        KeyAccid(const KeyAccid& other);
        virtual ~KeyAccid();
        /** \brief Records the appearance and usually the function of the bar line.
         */
        MeiAttribute* getForm();
        void setForm(std::string _form);
        bool hasForm();
        void removeForm();

/* include <keyAccid> */

        CommonMixIn    m_Common;
        CommonPartMixIn    m_CommonPart;
        FacsimileMixIn    m_Facsimile;
        CommonAnlMixIn    m_CommonAnl;
        AlignmentMixIn    m_Alignment;
        AccidentalMixIn    m_Accidental;
        PitchMixIn    m_Pitch;
        OctaveMixIn    m_Octave;
        AltsymMixIn    m_Altsym;
        EnclosingcharsMixIn    m_Enclosingchars;
        ExtsymMixIn    m_Extsym;
        StafflocMixIn    m_Staffloc;
        TypographyMixIn    m_Typography;
        XyMixIn    m_Xy;

    private:
        REGISTER_DECLARATION(KeyAccid);
};
```


## Meter Signature

- [ ] `MeterSigLogMixIn`
- Captures the number of beats in a measure, that is, the top number of the meter signature.
It must contain a decimal number or an additive expression that evaluates to a decimal number, such as 2+3.
Attribute = `Count`
- Indicates the use of a meter symbol instead of a numeric meter signature, that is, 'C' for common time or 'C' with a slash for cut time.
Attribute = `Sym`
- Indicates the unit of measurement.
Attribute = `Unit`





## Barline

```C++
/** \brief Vertical line drawn through one or more staves that divides musical notation
 *  into metrical units.
 */
class MEI_EXPORT BarLine : public MeiElement {
    public:
        BarLine();
        BarLine(const BarLine& other);
        virtual ~BarLine();

/* include <barLine> */

        CommonMixIn    m_Common;
        CommonPartMixIn    m_CommonPart;
        FacsimileMixIn    m_Facsimile;
        PointingMixIn    m_Pointing;
        TargetevalMixIn    m_Targeteval;
        BarLineLogMixIn    m_BarLineLog;
        MeterconformanceBarMixIn    m_MeterconformanceBar;
        BarplacementMixIn    m_Barplacement;
        ColorMixIn    m_Color;
        MeasurementMixIn    m_Measurement;
        WidthMixIn    m_Width;
        TimestampMusicalMixIn    m_TimestampMusical;
        CommonAnlMixIn    m_CommonAnl;
        AlignmentMixIn    m_Alignment;

    private:
        REGISTER_DECLARATION(BarLine);
};
```

- [ ] `BarLineLogMixIn` `m_BarLineLog`
Records the appearance and usually the function of the bar line.
```
        MeiAttribute* getForm();
        void setForm(std::string _form);
        bool hasForm();
        void removeForm();
```


## Tempo

```C++
/** \brief Text and symbols descriptive of tempo, mood, or style, e.g., "allarg.", "a
 *  tempo", "cantabile", "Moderato", "♩=60", "Moderato ♩ =60").
 */
class MEI_EXPORT Tempo : public MeiElement 
{
    public:
        Tempo();
        Tempo(const Tempo& other);
        virtual ~Tempo();

/* include <tempo> */

        CommonMixIn    m_Common;
        CommonPartMixIn    m_CommonPart;
        BiblMixIn    m_Bibl;
        FacsimileMixIn    m_Facsimile;
        LangMixIn    m_Lang;
        TempoLogMixIn    m_TempoLog;
        PlistMixIn    m_Plist;
        TargetevalMixIn    m_Targeteval;
        TimestampMusicalMixIn    m_TimestampMusical;
        TimestampPerformedMixIn    m_TimestampPerformed;
        StaffidentMixIn    m_Staffident;
        LayeridentMixIn    m_Layerident;
        StartidMixIn    m_Startid;
        PlacementMixIn    m_Placement;
        VisualoffsetHoMixIn    m_VisualoffsetHo;
        VisualoffsetToMixIn    m_VisualoffsetTo;
        VisualoffsetVoMixIn    m_VisualoffsetVo;
        Visualoffset2HoMixIn    m_Visualoffset2Ho;
        Visualoffset2ToMixIn    m_Visualoffset2To;
        XyMixIn    m_Xy;
        MiditempoMixIn    m_Miditempo;
        MmtempoMixIn    m_Mmtempo;
        CommonAnlMixIn    m_CommonAnl;
        AlignmentMixIn    m_Alignment;
        TypedMixIn    m_Typed;

    private:
        REGISTER_DECLARATION(Tempo);
};
```

- [ ] `MmtempoMixIn`
Used to describe tempo in terms of beats (often the meter signature denominator) per minute, ala M.M.
- (Maezel's Metronome). Do not confuse this attribute with midi.bpm or midi.mspb.
In MIDI, a beat is always defined as a quarter note, *not the numerator of the time signature or the metronomic indication*.
Attribute = `Mm`
- Captures the metronomic unit.
Attribute = `MmUnit`
- Records the number of augmentation dots required by a dotted metronome unit.
Attribute = `MmDots`





# Annotations (Locals and Spanning)


## Articulation

```C++
/** \brief (articulation) – An indication of how to play a note or chord.
 */
class MEI_EXPORT Artic : public MeiElement 
{
    public:
        Artic();
        Artic(const Artic& other);
        virtual ~Artic();

/* include <artic> */

        CommonMixIn    m_Common;
        CommonPartMixIn    m_CommonPart;
        FacsimileMixIn    m_Facsimile;
        ArticulationMixIn    m_Articulation;
        PlistMixIn    m_Plist;
        TargetevalMixIn    m_Targeteval;
        TimestampMusicalMixIn    m_TimestampMusical;
        TimestampPerformedMixIn    m_TimestampPerformed;
        StaffidentMixIn    m_Staffident;
        LayeridentMixIn    m_Layerident;
        AltsymMixIn    m_Altsym;
        ColorMixIn    m_Color;
        EnclosingcharsMixIn    m_Enclosingchars;
        ExtsymMixIn    m_Extsym;
        PlacementMixIn    m_Placement;
        StafflocMixIn    m_Staffloc;
        TypographyMixIn    m_Typography;
        VisualoffsetHoMixIn    m_VisualoffsetHo;
        VisualoffsetToMixIn    m_VisualoffsetTo;
        VisualoffsetVoMixIn    m_VisualoffsetVo;
        XyMixIn    m_Xy;
        ArticulationPerformedMixIn    m_ArticulationPerformed;
        CommonAnlMixIn    m_CommonAnl;
        AlignmentMixIn    m_Alignment;

    private:
        REGISTER_DECLARATION(Artic);
};
```


## Dynamic

```C++
/** \brief (dynamic) – Indication of the volume of a note, phrase, or section of music.
 */
class MEI_EXPORT Dynam : public MeiElement 
{
    public:
        Dynam();
        Dynam(const Dynam& other);
        virtual ~Dynam();

/* include <dynam> */

        CommonMixIn    m_Common;
        CommonPartMixIn    m_CommonPart;
        PlistMixIn    m_Plist;
        TargetevalMixIn    m_Targeteval;
        TimestampMusicalMixIn    m_TimestampMusical;
        TimestampPerformedMixIn    m_TimestampPerformed;
        StaffidentMixIn    m_Staffident;
        LayeridentMixIn    m_Layerident;
        AugmentdotsMixIn    m_Augmentdots;
        DurationAdditiveMixIn    m_DurationAdditive;
        StartendidMixIn    m_Startendid;
        StartidMixIn    m_Startid;
        Timestamp2MusicalMixIn    m_Timestamp2Musical;
        PlacementMixIn    m_Placement;
        VisualoffsetHoMixIn    m_VisualoffsetHo;
        VisualoffsetToMixIn    m_VisualoffsetTo;
        VisualoffsetVoMixIn    m_VisualoffsetVo;
        Visualoffset2HoMixIn    m_Visualoffset2Ho;
        Visualoffset2ToMixIn    m_Visualoffset2To;
        XyMixIn    m_Xy;
        DurationPerformedMixIn    m_DurationPerformed;
        MidivalueMixIn    m_Midivalue;
        Midivalue2MixIn    m_Midivalue2;
        CommonAnlMixIn    m_CommonAnl;
        AlignmentMixIn    m_Alignment;
        FacsimileMixIn    m_Facsimile;
        LangMixIn    m_Lang;
        TypedMixIn    m_Typed;

    private:
        REGISTER_DECLARATION(Dynam);
};
```

- [ ] `DurationAdditiveMixIn` `m_DurationAdditive`
cf. Ornament
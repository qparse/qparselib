# MEI doc structure & content

## MEI.shared module
= declarations that are common to two or more other modules.
cf. https://music-encoding.org/guidelines/v4/content/shared.html

### shared structural elements

- [`<meiCorpus>`](https://music-encoding.org/guidelines/v4/elements/meicorpus.html) = `<meiHead>` `<mei>`*

- `<mei>` = `<meiHead>` `<music>`
  - attribute: `meiversion`

`<meiHead>` = metadata
cf. 
https://music-encoding.org/guidelines/v4/content/metadata.html
https://music-encoding.org/guidelines/v4/content/metadata.html#headerstructure
https://music-encoding.org/guidelines/v4/content/metadata.html#corpus

for a single musical text :
- `<music>` = `<front>` `<body>` `<back>`

for a composite musical text :
- `<music>` = `<front>` `<group>` `<back>`
where
- `<group>` = `<music>`*

musical content:
- `<body>` = `<mdiv>`*
- `<mdiv>` = `<mdiv>`* | `<score>` `<parts>` 
  attributes:
  - `n`
  - `type` (movement, act, scene...)

- `<score>` : full score with all the parts together

- `<parts>` = `<part>`*
individually notated parts for each performer or group

- `<score>` or `<part>` = [ `<extension>` ] (`<section>` | `<ending>`)*

`<extension>` is a list of section or ending names to be expanded.

the content below `<section>` differs according to notation e.g. CNM,  Mensural or Neume. see below for CMN.


### other shared musical elements

meta-information:
- `<scoreDef>` = `<staffGrp>`
  attributes:
  - `meter.count` (numerator of key signature, int or additice expression)
  - `meter.unit` (denominator of key signature, int)
  - `meter.sym` (symbol e.g. `C`, `c` slash)
  - `key.sig` 

- `<staffGrp>` = `<staffDef>`*  

- `<staffDef>` = `<clef>` `<clefGrp>` `<keySig>` `<keyAccid>` `<label>` `<meterSig>` `<meterSigGrp>` `<layerDef>`*
  attributes:
  - `n` (int, mandatory)
  - `label`
  - `clef.line`  (int)
  - `clef.shape` 
  - `clef.dis`  (octave displacement, innt)
  - `clef.dis.place` (direction of octave displacement)

- `<layerDef>`  
  - `n` (int, mandatory)
  - `label`


organization:
- `<staffGrp>`  (bracketed or braced staves)
- `<grpSym>`    (brace or bracket symbol)
- `<label>`

score or staff parameters:
- `<clef>`
- `<clefGrp>`  (set of simultaneously-occurring clefs)
- `<keySig>`
- `<keyAccid>` (accidental in a key signature)
- `<meterSig>` (meter signature). 
- `<meterSigGrp>`

for the logical organization of musical notation

- `<staff>`

- `<layer>` (layer = voice = monophonic stream of events)


### basic musical elements

- `<note>`  attributes
  - `pname` (pitch name, A - G)
  - `oct` (octave, 0 - 9)
  - `dur` (duration, 1, 2, 4, 8, 16, ..., 2048)
  - `grace` (`unacc`= played before onset of the foll. note or `acc`= played at onset of foll. note)
  - `grace.time` (% of time stolen by grace note)

- `<graceGrp>` =  `<note>`*  
  attributes:
  - `grace` (`unacc`= played before onset of the foll. note or `acc`= played at onset of foll. note)
  - `attach` to the following event or ce preceding one.


- `<chord>`
- `<rest>`

several `stem.X` attributes for note and chord.

separators:
- `<barLine>`
- `<custos>`   (sign)

augmentation symbols (can be independent in mensural or neume notation):
- `<accid>` (accidental)
- `<artic>` (articulation) 
- `<dot>`

placeholder:
- `<space>` used to fill missing notes in layer / staff
with attribute 
  - `dur`


marks
- `<tempo>`
- `<dynam>` (dynamic) 
- `<phrase>` (curved lines placed over or under notes)
- `<slur>`

ornaments:
- `<ornam>` (generic ornament = not of one of the particular kind below)

for particular ornaments, see
https://music-encoding.org/guidelines/v4/content/cmn.html#cmnOrnaments

- `<mordent>`
  attributes: 
  - `form` (upper or lower)
  - `long` (bool)
  - `accidlower`
  - `accidupper`

- `<trill>`
  attributes: 
  - `accidlower`
  - `accidupper`

- `<turn>`  (4 notes = gruppetto)
  attributes: 
  - form (upper or lower)
  - delayed (bool)
  - accidlower
  - accidupper

## Common Music Notation
specific organisation of the content of `<section>` for CMN.
organization for Mensural and Neume notation may differ.

- `<section>` and `<ending>` = [`<staffDef>`] `<measure>`* 
  attribute: 
  - `label`

- `<measure>` =  [`<staffDef>`] `<staff>`* 
 
- `<staff>` = [`<layerDef>`] `<layer>`*
  attribute (for all) 
  - `n` (int)
used to link `<staff>` to `<staffDef>`, `<layer>` to `<layerDef>`...

- `<layer>` = ( `<chord>` | `<note>` | `<rest>` | `<clef>` | `<clefGrp>` |`<keySig>` | `<keyAccid>` | `<meterSig>` | `<meterSigGrp>` )* | `<mSpace>`
 
`<mSpace>` (measure space) is for an empty measure  


- `<chord>` = [`<artic>`] (`<note>` [`<accid>`])*
 
- `<dot>`





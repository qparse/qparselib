# Proposal for a (small) reorganisation of qparse sources

Every section corresponds to a (proposed) directory name.





## targets
Each of the following `targets` produces a command line utility, either for testing the quantization procedure or for the construction and management of grammars or transcription input.

Every `target` is defined in a .cpp file with a main function.
The commandline options are handled using the GNU [getopt](https://www.gnu.org/software/libc/manual/html_node/Getopt.html) function.

* quant:
Transcription of an input given by a text or a MIDI file wrt a given grammmar.
Various output possible, including MEI score file and quantized MIDI file.

* mei:
is a target to produce an MEI score from the quantization of an input segment wrt a given grammmar.
It has more or less the same options as quant.

* schema:
Utilities for the construction of schemas and computation of weights.

* MIDI utils:
Utilities for reading MIDI files and conversion to text format.

* equiv:
Enumeration of rhythm trees in a given grammar's language equivalent to a given sequence of (quantized) IOI.


## weight

Several semirings definining weight values.
Every semiring is defined by 

* a weight domain, 
* a binary operator plus (associative, commutative), 
* a neutral element `zero` for `plus`, 
* a binary operator `mult` (associative)
* a neutral element `one` for mult

such that `zero` is an absorbing element for `mult`
and `mult` distributes over `plus`.




## schema / wta
was `schemata`

Classes related to rhythm `grammars` used in input for quantization.
A `grammar` (or weighted tree automaton, `wta`) associates to every tree with `labeled` leaves a unique weight value, in one of the domain defined in directory `weight` (a unique `weight` domain is fixed for a `grammar`).

A `grammar` is defined by a list of `transition` rules, were each transition rule is defined by a target `state`, a body (sequence of states) and a `weight`.
An initial `state` is distinguised in every `grammar`.

A `grammar` can be read from a text file, describing transition rules and options…



## segment / interval
was in `input`

Classes for the abstract representation of data in input (performance) processed by parsing.

The first category of classes are used for the representation of performances in input: sequences of input events in input.


* Pitch
* MusEvent
musical events (without timestamped)
* Point
musical event with real-time onset and duration
* MusPoint
musical event with real-time and musical-time onsets and durations
* InputSegment
sequence of events with
    * real-time onset and duration
    * musical-time onset and duration
        an InputSegment is called 

The second category of classes represent time intervals, 
and tools for the alignement of input events to these intervals (for quantization).
Every interval has real-time and musical-time bound.

* Interval
* AlignedInterval
* IntervalTree
* IntervalHeap


## table / parsetable
was `parsing`

A table stores the result of parsing wrt a given grammar.
It defines the associations to `keys` of `records`, where every `records` contains some `runs`, and can be requested for a k-best `run`.

A `run` is either a tuple of `keys` (inner `run`) or a `label` (terminal `run`).
Therefore, a table associates best trees (with `labels` in leaves) to `keys`.


partial / complete `keys` : symbolic constraint solving techniques.



## parsers
A `parser` class defines a scenario for the construction of a table and its use to compute a tree from a `grammar` and some `input`.
This use can correspond to a workflow  of transcription of the given `input`.

Each `parser` clas may contain a demo() fonction to be called in a target.



## scoremodel
mv to `output`?

An abstract model used to produce scores in various format from parsing results (*i.e.* from trees).



## intput
Different formats for sequences of musical events given in input to quantization.
Facilities to import these formats into InputSegment.

- text format

- MIDI file input. based on the library [Midifile](http://midifile.sapp.org) of Craig Stuart Sapp.


## output


## general
Misc classes:


* ini : project constants and reading parameters in a .ini file, based on the C++ [header only version](https://github.com/jtilly/inih) of the library [inih](https://github.com/benhoyt/inih).

* QPconfig : 
verification of compile variables

* Rational

* trace : based on the library [spdlog](https://github.com/gabime/spdlog) of Gabi Melman.

V. 6 oct. 2017

we need to fix (unique?) format for :
- scores (quantised durations)
- performances (unquantised durations)
- comparison functions.

name to be fixed:
event traces (currently class Segment in src), timelines, IOI sequences...


* current implementation 

see file input/Date.hpp
dates contain
- unquantified value, absolute (in seconds) and normalised
- quantified value (in # of samples, an internal time unit).
it may contain both, with different values (before and after quantisation).
maybe this is better suited to internal processing than external storage.

class Point in Segment.hpp is composed of an event (pitch, velocity) and a Date.

moreover, output/DurationList.hpp define sequences of rational numbers (quantised durations).
they are computed together with rhythm trees during k-best parsing.


* time units

milliseconds in D. Temperley’s Notelists.
see "An Evaluation Systemfor Metrical Models” CMJ 28:3, 2004.

in current implementation, values can be
- normalised values in [0..1]  (fraction of input segment)
- integer values relative to a sample period (see MIDI).

the idea for sample period is that the given WTA for quantization implicitly defines a minimal duration
(see WTA.resolution() in schemata/ ).
expressing durations smaller than this value is useless in our context.


* MIDI Time code

the notion MIDI ticks (as time unit) can be interesting to us.

https://stackoverflow.com/questions/34166367/how-to-correctly-convert-midi-ticks-to-milliseconds
https://stackoverflow.com/questions/2038313/midi-ticks-to-actual-playback-seconds-midi-music


* values

we need to consider values for
- files
- internal representation
they may differ.

can be
- integer (quantised)
- rational (quantised) see output/Rational.hpp in source
- floating point double (unquantified)
or a mix?

rational is more appropriate for quantised (to avoid imprecision)
floating point is more appropriate for performance (to avoid having to fix a sample length).


* file format

one entry per line

two possibilities:

1. absolute dates
- first entry = begin date (i.e. date of beginning of recording)
- next lines = absolute dates of event’s start
- last line = end date (necessary only if previous are absolute dates)

2. durations
- first entry = duration between begin date and beginning of first event (mandatory? can be 0)
- next lines = events durations (IOIs)
- last line = duration between end of last event and end pf recording (mandatory?)

rem: 
the first and last line may be useful in the case of unquantified data (performance recording)
but make not much sense in the case of quantized values.

proposal: 
- 2 for scores (quantised) with rational values
- 1 for performances (with floating point values)













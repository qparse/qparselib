//
//  PSConfig.hpp
//  squant2
//
//  Created by Florent Jacquemard on 15/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#ifndef PSConfig_hpp
#define PSConfig_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>
#include <assert.h>
#include <array>
#include <vector>

#include "trace.hpp"
#include "Pitch.hpp"
#include "KeySig.hpp"
#include "AccidentState.hpp"


namespace ScoreModel {

class PSConfig;


/// Initial configuration for a pitch spelling algorithm of scope 1 bar
class PSConfig
{
public:

    /// initial config for a given tonality.
    /// @param ton a tonality given in number of flats (negative int)
    /// or number of sharps (positive int). must be in -7..7.
    PSConfig(int ton, size_t init=0);

    /// initial config for a given key signature.
    PSConfig(const KeySig& ks, size_t init=0);

    /// initial config with a given
    PSConfig(const AccidState& s, size_t init=0);

    /// copy constructor
    PSConfig(const PSConfig& s);

    virtual ~PSConfig();
    
    /// target configs reached by transition from this config,
    /// when receiving the given pitch.
    std::vector<PSConfig*> successors(const Pitch& p) const;

    virtual bool initial() const { return true; }
    
    /// alteration for the given note name recorded in this config.
    int alteration(int name) const;

    /// index of last note read to reach this configuration
    inline size_t id() const { return _id; }

    /// number of accidents in the minimal path to this config
    inline size_t cost() const { return _cost; }

    /// previous config in min path to this config
    inline const PSConfig* previous() const { return _pred; }
    
protected:

    /// description of accidents for each note name
    AccidState _state;

    /// index of last note read to reach this configuration
    size_t _id;

    /// previous config in min path to this config
    const PSConfig* _pred;

    /// number of accidents in the minimal path to this config
    size_t _cost;
    
private:
    
    /// for each MIDI pitch modulo 12, there are 2 or 3 synonyms,
    /// described in the following table:
    /// | pitch  |       |    |       |
    /// |:------:|:------|:---|:------|
    /// |   0    |  Dbb  | C  |  B#   |
    /// |   1    |  Db   | C# | [B##] |
    /// |   2    |  Ebb  | D  |  C##  |
    /// |   3    | [Fbb] | Eb |  D#   |
    /// |   4    |  Fb   | E  |  D##  |
    /// |   5    |  Gbb  | F  |  E#   |
    /// |   6    |  Gb   | F# | [E##] |
    /// |   7    |  Abb  | G  |  F##  |
    /// |   8    |  Ab   | G# |       |
    /// |   9    |  Bbb  | A  |  G##  |
    /// |  10    | [Cbb] | Bb |  A#   |
    /// |  11    |  Cb   | B  |  A##  |
    ///
    /// The table SYNONYMS contains for each MIDI pitch in 0..11
    /// three synonyms in 0..2 presented with 2 values :
    /// - name in 0..6 for C, D, E, F, G, A, B (see table NAME) and
    /// - alteration in -2..2
    /// In the cases where there are only 2 synonyms (e.g. pitch = 8),
    /// both components are set to SYNONYM_UNDEF.
    static const int SYNONYM[12][3][2];
    static const int SYNONYM_UNDEF;
    
    /// short list for SYNONYM's
    static const int SYNONYMSHORT[12][3][2];

    // same as SYNONYMS with flats first
    // static const int SYNONYMF[12][3][2];

};



/// target configuration for a pitch spelling algorithm of scope 1 bar
/// it extends the PSConfig with members for the pitch name and print flag.
class PSConfigMark : public PSConfig
{
public:

    
    /// copy
    PSConfigMark(const PSConfigMark& c);

    /// target config for a transition from given (previous) config,
    /// when receiving the given pitch.
    /// copy and update with given accident for given name.
    /// @param c previous config (origin), to be updated with the pitch
    // @param p pitch to be named, used to update the config.
    /// @param name chosen name for the given pitch
    /// @param accid chosen alteration for the given pitch
    PSConfigMark(const PSConfig& c, int name, int accid);
    
    virtual ~PSConfigMark();
    
    virtual bool initial() const { return false; }
    
    /// this config's name formatted for a Pitch object.
    char name() const;
    
    /// this config's alteration formatted for a Pitch object.
    float alteration() const;
    
    /// whether this config's alteration must be printed or not.
    bool printed() const;
    
private:
    
    // pitch used to build this config. It can be modified.
    // @todo TBR. useless
    // Pitch& _pitch;
    
    /// a chosen pitch name, in 0..6 (0 is 'C', 6 is 'B').
    int _name;
    
    // a chosen number of accidents, in -2..2.
    // @todo TBR: it is _state[_name] by construction
    // int _accid;
    
    /// whether the accident must be printed.
    bool _print;

private:
    
    /// conversion of pitch names from 0 to 6 into char 'A'..'G' (0 is 'C').
    static const char NAME[7];

};


/// orderings for ordered container of PSConfigMark.
using PSCCompare = std::function<bool(const PSConfig*, const PSConfig*)>;


} // namespace ScoreModel

/// @}

#endif /* PSConfig_hpp */

//
//  PS0.hpp
//  squant2
//
//  Created by Florent Jacquemard on 19/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#ifndef PS0_hpp
#define PS0_hpp
/// @addtogroup scoremodel
/// @{


#include <stdio.h>
#include <assert.h>
#include <array>
#include <vector>

#include "trace.hpp"
#include "MTU.hpp"
#include "Pitch.hpp"
#include "Part.hpp"
#include "AEVisitor.hpp"
#include "KeySig.hpp"
#include "PitchEnum.hpp"
#include "PSshortest.hpp"

namespace ScoreModel {

/// pitch spelling algorithm by minimization of the number of accident displayed.
class PS0
{
    
public:

    /// @param p a part in a score model. cannot be changed.
    /// all the Notes in this part will be renamed.
    PS0(Part& p);

    /// copy constructor
    PS0(const PS0& p);
    
    ~PS0();

    /// rename the notes in registered part, assuming same tonality for
    /// all voices and all measures.
    void spell1();
    
private:
    
    Part& _part;
    
    PitchEnum _enum;
    
    /// one shortest path for each ton in each measure of the registered part.
    /// or NULL (for each ton) in case of empty measure (rest).
    std::vector<std::array<PSshortest*, 15>> _bars;

    /// total cost (all measures) for each ton
    size_t _cost[15];
    
    
    
    
    
    
    /// fill the _bars structure with one shortest path per measure per tonality.
    /// @return whether _bars was effectively initialized.
    bool init_barshortest();

    /// compute the _cost structure.
    /// @return whether _cost was effectively initialized.
    bool init_costs1();

    /// @param bar measure number (first is 0)
    /// @param i0 index of first note of the measure (wrt the enumerator)
    /// @return the index of the first note of the next bar, or the total
    /// number of note (index of last note + 1) if end of part is reached.
    size_t bound_measure(size_t bar, size_t i0);

};


} // namespace ScoreModel

/// @}

#endif /* PS0_hpp */

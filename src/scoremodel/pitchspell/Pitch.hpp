//
//  Pitch.hpp
//  qparse
//
//  Created by Florent Jacquemard on 10/05/17.
//  Copyright © 2017 Florent Jacquemard. All rights reserved.
//


#ifndef Pitch_hpp
#define Pitch_hpp
/// @addtogroup scoremodel
/// @{


#include <stdio.h>
#include <assert.h>
#include <vector>

#include "trace.hpp"


namespace ScoreModel {



/// @brief internal representation of a pitch value.
/// it can have:
/// - a MIDI or MIDIcent value
/// - a MIDI value and a name/alteration/octave
/// - node of them (undef pitch)
/// can be unknown value
/// @todo extend conversions to MIDIcent (import OM)
class Pitch
{
public:
    enum PitchUnit { MIDI, MIDICENT };
    
    static const unsigned int UNDEF_MIDICENT;
    static const char         UNDEF_NOTE_NAME;
    static const int          UNDEF_NOTE_OCTAVE;
    static const float        UNDEF_NOTE_ALTERATION;

    /// @brief undef pitch value.
    Pitch();
    
    /// @brief construct pitch from name+alteration+octave.
    /// @param name see table Pitch::NAME
    /// @param alt in [-2, 2] where 1.0 is half tone
    /// @param oct in -10..10
    Pitch(char name,
          float alt = 0.0,
          int oct = 0);
    
    /// @brief construct note from MIDI pitch
    /// @param pitch in 0..127
    /// @param unit for pitch : MIDI or MIDICENT
    /// @param named whether a default name must be computed for this pitch.
    Pitch(unsigned int pitch, PitchUnit unit = MIDI, bool named = true);
    
    /// @brief copy constructor.
    Pitch(const Pitch& p);
    
    virtual ~Pitch() {}
    
    Pitch& operator= (const Pitch&);
    
    bool equal(const Pitch&) const;

    bool less(const Pitch&) const;

    /// the MIDI value is undefined.
    bool undef() const;
    bool unpitched() const { return undef(); }

    /// the name/alteration/octave values are set.
    bool named() const;
    
    void rename(char n, float alt, int oct, bool altprint);
        
    /// @brief value in MIDIcent.
    unsigned int midicent() const { return _midi; };
    
    /// @brief value in MIDI.
    unsigned int midi() const { return (_midi/100); };
    
    virtual void print(std::ostream& o) const;

    friend std::ostream& operator<<(std::ostream&, const Pitch&);

    
public:
    
    /// @brief note name betwen 'A' and 'G'.
    /// @see MusicXML step https://usermanuals.musicxml.com/MusicXML/Content/EL-MusicXML-step.htm
    char name;

    /// @brief alteration in [-2, 2] where 1.0 is half tone.
    /// e.g. -1 for flat, 1 for sharp
    /// decimal values are used for microtones
    /// e.g. 0.5 for quarter tone sharp
    /// @see MusicXML alter https://usermanuals.musicxml.com/MusicXML/Content/EL-MusicXML-alter.htm
    float alteration;
    
    /// @brief octave in -10..10.
    int octave;

    /// @brief whether the alteration must be printed or not
    /// (engraving info)
    /// @warning this value can be set by a pitch spelling algorithm
    bool altprint;
    
    
private:
    /// @brief in MIDI cent.
    unsigned int _midi;

    /// @brief tables for conversion from MIDI values into note names.
    static const char NAMEFLAT[12];
    static const char NAMESHARP[12];
    static const int  FLAT[12];
    static const int  SHARP[12];

private: // non unique
    
    static char midi_to_name(unsigned int);
    static float midi_to_alt(unsigned int);

public:  // unique
    
    static int midi_to_octave(unsigned int);
    static int midicent_to_octave(unsigned int);
    static unsigned int name_to_midi(char, float, int);
    static unsigned int name_to_int(char);

}; // end class Pitch


// boolean operators
inline bool operator==(const Pitch& lhs, const Pitch& rhs)
{
    return lhs.equal(rhs);
}

inline bool operator!=(const Pitch& lhs, const Pitch& rhs)
{
    return !operator==(lhs,rhs);
}

inline bool operator<(const Pitch& lhs, const Pitch& rhs)
{
    return (lhs.less(rhs));
}

inline bool operator>(const Pitch& lhs, const Pitch& rhs)
{
    return  operator<(rhs,lhs);
}

inline bool operator<=(const Pitch& lhs, const Pitch& rhs)
{
    return !operator> (lhs, rhs);
}

inline bool operator>=(const Pitch& lhs, const Pitch& rhs)
{
    return !operator< (lhs,rhs);    
}

} // namespace ScoreModel

/// @}

#endif /* Pitch_hpp */



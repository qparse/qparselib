//
//  AEVisitor.cpp
//  squant2
//
//  Created by Florent Jacquemard on 18/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#include "AEVisitor.hpp"


namespace ScoreModel {

// dispatch Atomic Events in Musical Score Model

void AEVisitor::visitNote(Note*){ }

void AEVisitor::visitGraceNote(GraceNote*) { }

void AEVisitor::visitRest(Rest*) { }

void AEVisitor::visitChord(Chord*) { }

void AEVisitor::visitGraceChord(GraceChord*) { }

void AEVisitor::visitTrill(Trill*) { }

} // endamespace ScoreModel

//
//  Pitch.cpp
//  qparse
//
//  Created by Florent Jacquemard on 10/05/17.
//  Copyright © 2017 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{


#include "Pitch.hpp"


namespace ScoreModel {

const unsigned int Pitch::UNDEF_MIDICENT        = 12800;
const char         Pitch::UNDEF_NOTE_NAME       = 'X';
const int          Pitch::UNDEF_NOTE_OCTAVE     = 128;
const float        Pitch::UNDEF_NOTE_ALTERATION = 11;



// names for the 12 semi-tones with flat accidents. private.
const char Pitch::NAMEFLAT[12] =
{'C', 'D', 'D', 'E', 'E', 'F', 'G', 'G', 'A', 'A', 'B', 'B'};

// flat alterations to add to names indexes. private.
const int  Pitch::FLAT[12] =
{ 0 ,  1 ,  0 ,  1 ,  0 ,  0 ,  1 ,  0 ,  1 ,  0 ,  1 ,  0 };

// names for the 12 semi-tones with sharp accidents. private.
const char Pitch::NAMESHARP[12] =
{'C', 'C', 'D', 'D', 'E', 'F', 'F', 'G', 'G', 'A', 'A', 'B'};

// sharp alterations to add to names indexes. private.
const int  Pitch::SHARP[12] =
{ 0 ,  1 ,  0 ,  1 ,  0 ,  0 ,  1 ,  0 ,  1 ,  0 ,  1 ,  0 };


Pitch::Pitch():
_midi(UNDEF_MIDICENT),
name(UNDEF_NOTE_NAME),
octave(UNDEF_NOTE_OCTAVE),
alteration(UNDEF_NOTE_ALTERATION),
altprint(false)
{ };


Pitch::Pitch(char n,
             float alt,
             int oct):
name(n),
alteration(alt),
octave(oct),
altprint(true)
{
    assert('A' <= n);
    assert(n <= 'G');
    assert(-10 <= oct);
    assert(oct <= 10);
    assert(-2.0 <= alt);
    assert(alt <= 2.0);
    assert(named());
    
    _midi = name_to_midi(n, alt, oct)*100;
    assert(_midi != UNDEF_MIDICENT);
}


Pitch::Pitch(unsigned int k, PitchUnit unit, bool named):
_midi((unit==MIDI)?k*100:k),
altprint(true)
{
    assert((unit == MIDI) || (unit == MIDICENT));
    assert(0 <= k);
    assert(k < 12800);
    assert(_midi != UNDEF_MIDICENT);

    // compute default name (can be not relevant)
    if (named)
    {
        name = midi_to_name(_midi);
        alteration = midi_to_alt(_midi);
        assert((-2 <= alteration) && (alteration <= 2));
        octave = midicent_to_octave(_midi);
        assert((-1 <= octave) && (octave <= 9));
    }
    // do not name before pitchspelling : set to undef name/alt/octave
    else
    {
        name = UNDEF_NOTE_NAME;
        alteration = UNDEF_NOTE_ALTERATION;
        octave = UNDEF_NOTE_OCTAVE;
    }
}


Pitch::Pitch(const Pitch& p):
name(p.name),
alteration(p.alteration),
octave(p.octave),
altprint(p.altprint),
_midi(p._midi)
{
    // @todo sanity check?
}


Pitch& Pitch::operator= (const Pitch& p)
{
    if(this != &p)
    {
        name = p.name;
        alteration = p.alteration;
        octave = p.octave;
        altprint = p.altprint;
        _midi = p._midi;
    };
    return *this;
}


bool Pitch::equal(const Pitch& p) const
{
    return (_midi == p._midi);
}


bool Pitch::less(const Pitch& p) const
{
    assert(_midi != UNDEF_MIDICENT);
    assert(p._midi != UNDEF_MIDICENT);
    return (_midi < p._midi);
}


bool Pitch::undef() const
{
    return (_midi == UNDEF_MIDICENT);
}


bool Pitch::named() const
{
    return ((name != UNDEF_NOTE_NAME) &&
            (alteration != UNDEF_NOTE_ALTERATION) &&
            (octave != UNDEF_NOTE_OCTAVE));
}


void Pitch::rename(char n, float alt, int oct, bool altpr)
{
    if (name != UNDEF_NOTE_NAME)
    {
        TRACE("Pitch rename: aleady named, replace name.");
        TRACE("former pitch name : (MIDI {}%{}) {}", midi(), midi()%12,  *this);
    }
    assert('A' <= n);
    assert(n <= 'G');
    name = n;
    assert(-2.0 <= alt);
    assert(alt <= 2.0);
    alteration = alt;
    assert(-10 <= oct);
    assert(oct <= 10);
    octave = oct;
    altprint = altpr;
    DEBUGU("new pitch name : (MIDI {}%{}) {}", midi(), midi()%12,  *this);
    // assert(name_to_midi(name, alteration, octave) == midi());
}

    
// revise to MIDIcent
char Pitch::midi_to_name(unsigned int c)
{
    unsigned int p = int(floor(c / 100));
    assert ((0 <= p) && (p <= 127));
    return NAMESHARP[p%12];
}


float Pitch::midi_to_alt(unsigned int c)
{
    unsigned int p = int(floor(c / 100));
    assert(0 <= p);
    assert(p <= 127);
    
    return (float(SHARP[p%12]) + ( float(c-(p*100))/100));
}


// C4 is MIDI key 60
int Pitch::midicent_to_octave(unsigned int c)
{
    int oct = int(floor(c/1200)) - 1;
    assert(-1 <= oct);
    assert(oct <= 9);
    return oct;
}


int Pitch::midi_to_octave(unsigned int c)
{
    int oct = int(floor(c/12)) - 1;
    assert(-1 <= oct);
    assert(oct <= 9);
    return oct;
}


unsigned int Pitch::name_to_int(char n)
{
    char c = toupper(n);
    for (unsigned int i = 0; i < 12; i++)
    {
        if (NAMESHARP[i] == c)
            return i;
    }
    throw std::runtime_error("wrong note name\n");
}

// TBC
unsigned int Pitch::name_to_midi(char n, float alt, int oct)
{
    assert(-2.0 <= alt);
    assert(alt <= 2.0);
    assert(-1 <= oct);
    assert(oct <= 9);
    
    char i = name_to_int(n);
    int falt = int(floor(alt));
    int r = (i + falt)%12;
    assert(0 <= r);
    assert(r <= 11);
    unsigned int p = ((oct+1) * 12) + r;
    assert(0 <= p);
    assert(p <= 127);
    
    return ((p*100)+ int((alt-falt)*100));
}


void Pitch::print(std::ostream& o) const
{
    if (undef()) o << "Unpitched";
    else if (named())
    {
        o << name;
        if ( alteration == Pitch::UNDEF_NOTE_ALTERATION) o << "?";
        else if ( alteration == 0.0) o << "";
        else if ( alteration == 1.0) o << "#";
        else if ( alteration == 2.0) o << "##";
        else if ( alteration == -1.0) o << "b";
        else if ( alteration == -2.0) o << "bb";
        else o << " ALT=" << alteration << " ";
        if ( octave == Pitch::UNDEF_NOTE_OCTAVE) o << "?";
        else o << octave;
        if (altprint)
            o << "(p)";
    }
    else
        o << "(" << midi() << ")";  // MIDI key
}

std::ostream& operator<<(std::ostream& o, const Pitch& p)
{
    p.print(o);
    return o;
}


} // end namespace ScoreModel


/// @}

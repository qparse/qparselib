//
//  PSShortest.hpp
//  squant2
//
//  Created by Florent Jacquemard on 16/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#ifndef PSshortest_hpp
#define PSshortest_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>
#include <assert.h>
#include <array>
#include <vector>
#include <queue>   // std::priority_queue

#include "trace.hpp"
#include "Pitch.hpp"
#include "KeySig.hpp"
#include "AccidentState.hpp"
#include "PSConfig.hpp"
#include "PitchEnum.hpp"

  
namespace ScoreModel {


/// Shortest path algorithm to computate a pitch spelling for one measure.
/// The events in the mesure are given by a part enumerator (see PitchEnum)
/// and the index (wrt the enumerator) of first and last event of measure.
class PSshortest
{
public:
    
    /// @param ton a tonality given in number of flats (negative int)
    /// or number of sharps (positive int). must be in -7..7.
    /// @param e an enumerator of notes for transitions of configs.
    /// @param n0 index of the first note to read in enumerator.
    /// @param n1 index of the last note to read in enumerator.
    /// @warning the values of parameters cannot be changed once the object created.
    PSshortest(int ton, PitchEnum& e, size_t n0, size_t n1);

    ~PSshortest();

    /// name for pitch of note of given index in the best path.
    /// @param i index of note in enumerator, must be between first and last.
    char name(size_t i);
    
    /// alteration for pitch of note of given index in the best path.
    /// @param i index of note in enumerator, must be between first and last.
    float alteration(size_t i);
    
    /// print flag for pitch of note of given index in the best path.
    /// @param i index of note in enumerator, must be between first and last.
    bool printed(size_t i);

    /// cost of best path from first to last note.
    size_t cost();
    
    inline size_t first() const { return _first; }
    
    inline size_t last() const { return _last; }
    
private:
    
    /// tonality of the best path.
    int _ton;
    
    /// note enumerator where the input is read.
    PitchEnum& _enum;
    
    /// index of the first note to read in enumerator.
    size_t _first;
    
    /// index of the last note to read in enumerator.
    size_t _last;
    
    // PSConfig* _final;

    /// heap of candidate configs for the best path search
    std::priority_queue<PSConfig*, std::vector<PSConfig*>, PSCCompare> _queue;
    
    /// backup of visited nodes
    std::vector<PSConfig*> _visited;

    /// names on best path from n0 to n1
    std::vector<char> _names;
    std::vector<float> _accids;
    std::vector<bool> _prints;

    /// number of accidents in the best path from n0 to n1
    size_t _cost;

    void complete();
    
    void record_path(const PSConfig* c);

};


} // namespace ScoreModel

/// @}

#endif /* PSshortest_hpp */

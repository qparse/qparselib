//
//  PitchEnum.cpp
//  squant2
//
//  Created by Florent Jacquemard on 17/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#include "PitchEnum.hpp"

namespace ScoreModel {


PitchEnum::esiterator::esiterator(EventSeq::iterator i, Voice* v):
aeiter(i),
voice(v)
{
    assert(v);
}


PitchEnum::esiterator::esiterator(const esiterator& e):
aeiter(e.aeiter),
voice(e.voice)
{
    assert(e.voice);
}


PitchEnum::esiterator& PitchEnum::esiterator::operator=(const esiterator& rhs)
{
    if (this != &rhs)
    {
        aeiter = rhs.aeiter;
        assert(rhs.voice);
        voice = rhs.voice;
    }
    return *this;
}



PitchEnum::PitchEnum(Part& p):
_part(p),
_backup(), // empty
_queue(esmin)
{
    // we initialize the queue with the first event of each voice in part p.
    for (Wozzeck::iterator iv = p.begin(); iv != p.end(); ++iv)
    {
        int nb = iv->first;
#ifdef SM_MEASURE_STATIC
        Voice& v = iv->second;
        assert(nb == v.number());
        if (v.begin() != v.end()) // not empty
            _queue.emplace(v.begin(), &v);
            //_queue.push(std::make_pair(v.begin(), v));
#else
        Voice* v = iv->second;
        assert(v); // Voice*
        assert(nb == v->number());
        EventSeq::iterator vit = v->begin();
        if (vit != v->end())
            _queue.emplace(v->begin(), v);
//            _queue.push(std::pair<EventSeq::iterator, Voice&>(vit, *v));
#endif
    }
}


PitchEnum::PitchEnum(const PitchEnum& pe):
_part(pe._part),
_backup(pe._backup), // no clones
_queue(pe._queue)    // copies of esiterators
{ }


PitchEnum::~PitchEnum()
{
    // the notes contained will be deallocated elsewhere.
    _backup.clear();
    // clear priority queue
    _queue = std::priority_queue<esiterator, std::vector<esiterator>,
                                 esCompare>();
}


PitchEnum::esCompare PitchEnum::esmin =
[](const PitchEnum::esiterator lhs, const PitchEnum::esiterator rhs)
{
    Voice* lvoice = lhs.voice;
    assert(lvoice);
    assert(lhs.aeiter != lvoice->end());
    AtomicEvent* el = *(lhs.aeiter);
    assert(el);
    Voice* rvoice = rhs.voice;
    assert(rvoice);
    assert(rhs.aeiter != rvoice->end());
    AtomicEvent* er = *(rhs.aeiter);
    assert(er);

    if (el->date() == er->date())
    {
        assert(lvoice->number() != rvoice->number());
        return (lvoice->number() < rvoice->number());
    }
    else
    {
        return (el->date() < er->date());
    }
};
    
    
Note* PitchEnum::note(size_t i)
{
    if (i < _backup.size())
    {
        return _backup[i];
    }

    // on demand computation
    // for (size_t c = i - _backup.size(); c <= i; ++c)
    while (_backup.size() <= i)
    {
        if (! next())
            break;
    }
    if (i < _backup.size())
        return _backup[i];
    else // we could not read until i
        return NULL;
}


bool PitchEnum::next()
{
    if (_queue.empty())
        return false;

    // first event by PitchEnum ordering
    esiterator esi = _queue.top();
    EventSeq::iterator it = esi.aeiter;
    Voice* v = esi.voice;
    assert(v);

    // dispatch *it to fill the _backup sequence
    AtomicEvent* e = *it;
    assert(e);
    e->accept(this);
    
    // replace top by next in event in the same voice v
    _queue.pop();
    ++it;
    if (it != v->end())
    {
        _queue.emplace(it, v);
        return true;
    }
    return (! _queue.empty()); // still some events to enumerate
}


void PitchEnum::visitNote(Note* n)
{
    assert(n);
    _backup.push_back(n);
}

void PitchEnum::visitGraceNote(GraceNote* gn)
{
    assert(gn);
    _backup.push_back(gn);
}

void PitchEnum::visitRest(Rest* r)
{
    assert(r);
    // do nothing (no spelling for rests!)
}


void PitchEnum::visitChord(Chord* c)
{
    assert(c);
    for (NoteBag::iterator i = c->begin(); i != c->end(); ++i)
    {
        Note* n = *i;
        assert(n);
        _backup.push_back(n);
    }
}


void PitchEnum::visitGraceChord(GraceChord* gc)
{
    assert(gc);
    for (NoteBag::iterator i = gc->begin(); i != gc->end(); ++i)
    {
        Note* n = *i;
        assert(n);
        _backup.push_back(n);
    }
}


void PitchEnum::visitTrill(Trill* t)
{
    assert(t);
    _backup.push_back(t); // is a Note
    _backup.push_back(t->second()); 
}





} // end namespace ScoreModel

/// @}


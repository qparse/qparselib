//
//  PS0.cpp
//  squant2
//
//  Created by Florent Jacquemard on 19/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{


#include "PS0.hpp"


namespace ScoreModel {

PS0::PS0(Part& p):
_part(p),
_enum(p),
_bars(),
_cost{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} // array initialization
{ }


PS0::PS0(const PS0& p):
_part(p._part),
_enum(p._enum)
{ }


PS0::~PS0()
{
    // delete all the configs
    for (std::array<PSshortest*, 15> bar : _bars)
    {
        for (size_t i = 0; i < 15; ++i)
        {
            if (bar[i])
                delete bar[i];  // PSshortest*
        }
    }
}


void PS0::spell1()
{
    if (! init_barshortest())
    {
        ERROR("PS0.1: fail to compute spelling of part {} (shortest).",
              _part.id());
        return;
    }
    if (! init_costs1())
    {
        ERROR("PS0.1: fail to compute spelling of part {} (costs).",
              _part.id());
        return;
    }
    for (int ton = -7; ton <= 7; ++ton)
    {
        DEBUGU("PS0.1: cost of part:{} for tonality {} : {}",
              _part.id(), ton, _cost[ton+7]);
    }

    size_t imin = 0;

    for (size_t i = 1; i < 15; ++i)
    {
        if (_cost[i] < _cost[imin])
            imin = i;
    }
    INFO("PS0.1: best tonality : {}", imin -7);
    assert(0 <= imin);
    assert(imin <= 14);
    
    // do the renaming of pitchs
    for (std::array<PSshortest*, 15> bar : _bars)
    {
        PSshortest* ps = bar[imin];
        // when bar is empty (rest) : nothing to do
        if (ps == NULL) continue;

        // otherwise, rename every note in the bar
        for (size_t n = ps->first(); n <= ps->last(); ++n)
        {
            Note* note = _enum.note(n);
            assert(note);
            note->namePitch(ps->name(n),
                            ps->alteration(n),
                            Pitch::midi_to_octave(note->pitch().midi()),
                            ps->printed(n));
            TRACE("PS0.1: part {} note {} : {}", _part.id(), n, note->pitch());
        }
    }
}


bool PS0::init_barshortest()
{
    assert(_bars.empty());     // do not recompute
    size_t i0 = 0; // first note of current bar
    size_t i1 = 0; // first note of next bar
    Note* n = _enum.note(i0);
    
    // empty part
    if (n == NULL)
        return false;
    
    // number of current bar
    size_t b = 0; // MTU::measure(n->date());

    while (n != NULL)
    {
        assert(MTU::measure(n->date()) >= b);
        
        // current bar b empty (i0 not in b)
        if (MTU::measure(n->date()) > b)
        {
            std::array<PSshortest*, 15> emptybar;
            emptybar.fill(NULL);
            _bars.push_back(emptybar);
            ++b;
            continue;
        }
            
        // current bar not empty (i0 in b)
        i1 = bound_measure(b, i0); // first note after current measure b
        assert(_enum.note(i1 - 1) != NULL);
        std::array<PSshortest*, 15> bar;
        for (int ton = -7; ton <= 7; ++ton)
        {
            DEBUGU("compute the best spelling for measure {} (notes {}-{}) part:{} ton={}",
                  b, i0, i1-1, _part.id(), ton);
            PSshortest* ps = new PSshortest(ton, _enum, i0, i1 - 1);
            DEBUGU("cost={}  the best spelling for measure {} (notes {}-{}) part:{} ton={}",
                  ps->cost(), b, i0, i1-1, _part.id(), ton);
            bar[ton+7] = ps;
        }
        // add current bar b
        _bars.push_back(bar);
        assert(_bars.size() == b+1);

        // next bar
        i0 = i1;
        n = _enum.note(i0); // first note of next bar or NULL
        ++b;
    }
    return true;
}


size_t PS0::bound_measure(size_t bar, size_t i0)
{
    size_t i = i0;
    Note* n = _enum.note(i);
    assert(n);
    assert(MTU::measure(n->date()) == bar);
    while ((n != NULL) && (MTU::measure(n->date()) == bar))
    {
        ++i;
        n = _enum.note(i);
    }
    return i;
}


bool PS0::init_costs1()
{
    if (_bars.empty())
        return false;
    size_t b = 0;
    for (std::array<PSshortest*, 15>& bar : _bars)
    {
        // ignore empty bar
        if (bar[0] == NULL)
        {
            DEBUGU("bar {} empty", b);
            continue;
        }

        for (int ton = -7; ton <= 7; ++ton)
        {
            if (bar[ton+7] != NULL) // shortest path
            {
                assert(bar[ton+7]);
                DEBUGU("Ton {} bar {}: {}", ton, b, bar[ton+7]->cost());
                _cost[ton+7] += bar[ton+7]->cost();
            }
        }
        ++b;
    }
    return true;
}






} // end namespace ScoreModel

/// @}


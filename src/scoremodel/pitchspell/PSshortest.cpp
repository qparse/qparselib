//
//  PSshortest.cpp
//  squant2
//
//  Created by Florent Jacquemard on 16/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#include "PSshortest.hpp"

namespace ScoreModel {


/// ordering for PSConfig based on cost.
PSCCompare PSCmin =
[](const PSConfig* lhs, const PSConfig* rhs)
{
    assert (lhs);
    assert (rhs);
    if (lhs->cost() == rhs->cost())
    {
        return (lhs->id() <= rhs->id()); // priority to longer path
    }
    
    return (lhs->cost() > rhs->cost());  // smallest cost
};


PSshortest::PSshortest(int ton, PitchEnum& e, size_t n0, size_t n1):
_ton(ton),
_enum(e),
_first(n0),
_last(n1),
// _final(NULL), // unset initialy
_queue(PSCmin),  // empty config queue
_visited(),      // empty
_names(),
_accids(),
_prints(),
_cost(0)
{
    assert(-7 <= ton);
    assert(ton <= 7);
    assert(n0 <= n1); // at least one pitch to read
    _queue.push(new PSConfig(ton, n0)); // initial configuration
}


PSshortest::~PSshortest()
{
    // deallocate all configs in the priority queue
    TRACE("delete PSshortest candidate queue (size={})",  _queue.size());
  
    // clear priority queue
    while(! _queue.empty())
    {
        PSConfig* c = _queue.top();
        if(c) delete c;
        _queue.pop();
    }
    _queue = std::priority_queue<PSConfig*, std::vector<PSConfig*>, PSCCompare>();
    
    // free other configs
    for (PSConfig* c : _visited)
    {
        if(c) delete c;
    }
    _visited.clear();

}


char PSshortest::name(size_t i)
{
    if (_names.empty())
    {
        complete();
    }
    assert(_first <= i);
    assert(i <= _last);
    assert((i - _first) < _names.size());
    return _names[i - _first];
}


float PSshortest::alteration(size_t i)
{
    if (_accids.empty())
    {
        complete();
    }
    assert(_first <= i);
    assert(i <= _last);
    assert(i - _first < _accids.size());
    return _accids[i - _first];
}


bool PSshortest::printed(size_t i)
{
    if (_prints.empty())
    {
        complete();
    }
    assert(_first <= i);
    assert(i <= _last);
    assert(i - _first < _prints.size());
    return _prints[i - _first];
}


size_t PSshortest::cost()
{
    if (_names.empty())
    {
        complete();
    }
    return _cost;
}


void PSshortest::complete()
{
    while (_names.empty())
    {
        assert(! _queue.empty());
        PSConfig* c = _queue.top(); // smallest
        assert(c);
        assert(c->id() >= _first);
        assert(c->id() <= _last+1);
        if (c->id() == _last+1)
        {
            record_path(c);
            return;
        }
        
        Note* n = _enum.note(c->id());
        assert(n);
        std::vector<PSConfig*> cv = c->successors(n->pitch());
        _visited.push_back(c); // keep c for deletion
        _queue.pop(); // remove
        while(! cv.empty())
        {
            assert(cv.back());
            _queue.push(cv.back());
            cv.pop_back();
        }
    }
}


void PSshortest::record_path(const PSConfig* c)
{
    assert(c);
    assert(c->id() == _last+1);
    _cost = c->cost();
    const PSConfig* co = c;
    while (co != NULL)
    {
        assert((! co->initial()) || (co->previous() == NULL));
        if (! co->initial())
        {
            const PSConfigMark* com = dynamic_cast<const PSConfigMark*>(co);
            assert(com);
            _names.insert(_names.begin(), com->name()); // push_front
            _accids.insert(_accids.begin(), com->alteration());
            _prints.insert(_prints.begin(), com->printed());
        }
        co = co->previous(); // NULL if co is initial
    }
    assert(_names.size() == _last - _first + 1);
    assert(_accids.size() == _last - _first + 1);
    assert(_prints.size() == _last - _first + 1);
}


} // end namespace ScoreModel

/// @}


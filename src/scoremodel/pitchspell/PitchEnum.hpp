//
//  PitchEnum.hpp
//  squant2
//
//  Created by Florent Jacquemard on 17/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#ifndef PitchEnum_hpp
#define PitchEnum_hpp
/// @addtogroup scoremodel
/// @{


#include <stdio.h>
#include <assert.h>
#include <vector>
#include <queue>          // std::priority_queue

#include "trace.hpp"
#include "MTU.hpp"
#include "Pitch.hpp"
#include "Part.hpp"
#include "Voice.hpp"
#include "AtomicEvent.hpp"
#include "Note.hpp"
#include "GraceNote.hpp"
#include "Chord.hpp"
#include "GraceChord.hpp"
#include "Trill.hpp"
#include "AEVisitor.hpp"
#include "KeySig.hpp"
#include "AccidentState.hpp"

namespace ScoreModel {


/// Enumerator of the Notes of a Part by chronological order.
/// Traverse all the pitches in all voices in the :
/// - notes,
/// - grace notes,
/// - notes in chords, notes in grace chords,
/// - 2 notes in trills.
/// Enumeration order (lexicographically):
/// - event date (chronological ordering),
/// - voice number for simultaneous events in different voices,
/// - for lower to higher pitch for notes in a chord or grace chord
///   (same voice, same date)
/// - grace notes (simultaneous):
///   same order as in their voice, when all in same voice, and
///   voice number in case of simultaneous grace notes in different voices,
/// - trill : main pitch then second pitch.
class PitchEnum : public AEVisitor
{
    
public:

    /// @param p registered part we want to enumerate the notes.
    /// cannot be changed.
    PitchEnum(Part& p);

    PitchEnum(const PitchEnum& pe);

    ~PitchEnum();
    
    /// note by index.
    /// The index is defined by the ordering defined in the description of
    /// class PitchEnum.
    /// @param i number of the note, between 0 and the total number of notes
    /// in the registered part.
    /// @return ith note in the registered part,
    /// or NULL if there are less than i notes.
    Note* note(size_t i);
    
    inline Part& part() const { return _part; }

    // @return ith pitch in the part in enumeration order.
    // Pitch& pitch(size_t i);

    // @return date of the event containing the ith pitch.
    // mtu_t date(size_t i);
    
    /// dispatch of AtomicEvent
    void visitNote(Note* n);
    void visitGraceNote(GraceNote* gn);
    void visitRest(Rest* r);
    void visitChord(Chord* c);
    void visitGraceChord(GraceChord* gc);
    void visitTrill(Trill* t);

private:

    struct esiterator
    {
    public:
        esiterator(EventSeq::iterator i, Voice* v);
        esiterator(const esiterator&);
        esiterator& operator=(const esiterator& rhs);
        
        EventSeq::iterator aeiter;
        Voice* voice;
    };
    
    using esCompare =
        std::function<bool(const esiterator, const esiterator)>;

    /// comparator of elements in the queue
    // static bool esmin(const esiterator, const esiterator);
    static esCompare esmin;

    Part& _part;
    
    /// record all the notes read in the Part.
    std::vector<Note*> _backup;

    /// heap of candidate configs for the best path search
    std::priority_queue<esiterator, std::vector<esiterator>, esCompare> _queue;
    
    /// compute the next notes in part.
    /// @return whether there are still notes to enumerate in the part.
    bool next();
       
};






} // namespace ScoreModel

/// @}

#endif /* PitchEnum_hpp */

//
//  AccidentState.hpp
//  squant2
//
//  Created by Florent Jacquemard on 14/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#ifndef AccidentState_hpp
#define AccidentState_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>
#include <assert.h>
#include <array>

#include "trace.hpp"
#include "Pitch.hpp"
#include "KeySig.hpp"

namespace ScoreModel {


/// a Accident State represent one accident (in -2..2) recorded
/// for each pitch name (in 0..6) in a measure.
/// - accident number -2 is a double flat
/// - accident number -1 is a flat
/// - accident number  0 is a neutral 
/// - accident number  1 is a sharp
/// - accident number  2 is a double sharp
/// - pitch names correspond to  chars 'A'..'G', where 0 is 'C' and 6 is 'B'.
class AccidState
{
public:

    /// initial state for a given tonality.
    /// @param ton a tonality given in number of flats (negative int)
    /// or number of sharps (positive int). must be in -7..7.
    AccidState(int ton);
    AccidState(const KeySig& ks);
    
    /// copy constructor
    AccidState(const AccidState& as);

    /// copy and update given name with given accident
    AccidState(const AccidState& as, int name, int accid);

    ~AccidState();
    
    /// accident for given pitch name.
    /// @param n a pitch name, in 0..6 (0 is 'C', 6 is 'B').
    /// @return the number of accidents, in state, for n, in -2..2.
    int accid(int n) const;
    
    /// update this state with the given accident for the given name.
    /// @param n a pitch name, in 0..6 (0 is 'C', 6 is 'B').
    /// @param a a number of accidents, for n, in -2..2.
    /// @return whether this state was effectively modified.
    bool update(int n, int a);
    
private:

    /// association to each pitch name (in 0..6)
    /// of a number of accidents (in -2..2).
    std::array<int, 7> _state; // _state[7]
       
    static const std::array<std::array<int, 7>, 15> TON;
    // static const int TON[15][7];
        
};


} // namespace ScoreModel

/// @}

#endif /* AccidentState_hpp */

//
//  PSConfig.cpp
//  squant2
//
//  Created by Florent Jacquemard on 15/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#include "PSConfig.hpp"
#include <cmath>        // std::abs


namespace ScoreModel {


PSConfig::PSConfig(int ton, size_t i):
_state(ton),
_id(i),
_pred(NULL),
_cost(0)
{
    assert(-7 <= ton);
    assert(ton <= 7);
}


PSConfig::PSConfig(const KeySig& ks, size_t i):
_state(ks),
_id(i),
_pred(NULL),
_cost(0)
{ }


PSConfig::PSConfig(const AccidState& s, size_t i):
_state(s), // copy
_id(i),
_pred(NULL),
_cost(0)
{ }


PSConfig::PSConfig(const PSConfig& c):
_state(c._state),
_id(c._id),
_pred(c._pred),
_cost(c._cost)
{ }


PSConfig::~PSConfig()
{
//    if (_pred)
//        delete _pred;
}


int PSConfig::alteration(int name) const
{
    assert(0 <= name);
    assert(name <= 6);
    return _state.accid(name);
}


const int PSConfig::SYNONYM_UNDEF = 9;


const int PSConfig::SYNONYM[12][3][2] =
{
    { {1, -2}, {0,  0}, {6, 1} }, //  0
    { {1, -1}, {0,  1}, {6, 2} }, //  1
    { {2, -2}, {1,  0}, {0, 2} }, //  2
    { {3, -2}, {2, -1}, {1, 1} }, //  3
    { {3, -1}, {2,  0}, {1, 2} }, //  4
    { {4, -2}, {3,  0}, {2, 1} }, //  5
    { {4, -1}, {3,  1}, {2, 2} }, //  6
    { {5, -2}, {4,  0}, {3, 2} }, //  7
    { {5, -1}, {4,  1}, {9, 9} }, //  8
    { {6, -2}, {5,  0}, {4, 2} }, //  9
    { {0, -2}, {6, -1}, {5, 1} }, // 10
    { {0, -1}, {6,  0}, {5, 2} }  // 11
};


// short list without B##, Fbb, E##, Cbb
const int PSConfig::SYNONYMSHORT[12][3][2] =
{
    { {1, -2}, {0,  0}, {6, 1} }, //  0
    { {1, -1}, {0,  1}, {9, 9} }, //  1
    { {2, -2}, {1,  0}, {0, 2} }, //  2
    { {9,  9}, {2, -1}, {1, 1} }, //  3
    { {3, -1}, {2,  0}, {1, 2} }, //  4
    { {4, -2}, {3,  0}, {2, 1} }, //  5
    { {4, -1}, {3,  1}, {9, 9} }, //  6
    { {5, -2}, {4,  0}, {3, 2} }, //  7
    { {5, -1}, {4,  1}, {9, 9} }, //  8
    { {6, -2}, {5,  0}, {4, 2} }, //  9
    { {9,  9}, {6, -1}, {5, 1} }, // 10
    { {0, -1}, {6,  0}, {5, 2} }  // 11
};


// second version with different order (useless)
//const int PSConfig::SYNONYMF[12][3][2] =
//{
//    { {6, 1}, {0,  0}, { 1, -2} }, //  0
//    { {0, 1}, {1, -1}, { 6,  2} }, //  1
//    { {0, 2}, {1,  0}, { 2, -2} }, //  2
//    { {1, 1}, {2, -1}, { 3, -2} }, //  3
//    { {1, 2}, {2,  0}, { 3, -1} }, //  4
//    { {2, 1}, {3,  0}, { 4, -2} }, //  5
//    { {2, 2}, {3,  1}, { 4, -1} }, //  6
//    { {3, 2}, {4,  0}, { 5, -2} }, //  7
//    { {4, 1}, {5, -1}, {-1,  0} }, //  8
//    { {4, 2}, {5,  0}, { 6, -2} }, //  9
//    { {5, 1}, {6, -1}, { 0, -2} }, // 10
//    { {5, 2}, {6,  0}, { 0, -1} }  // 11
//};


std::vector<PSConfig*> PSConfig::successors(const Pitch& p) const
{
    unsigned int pm = p.midi();
    assert(0 <= pm);
    assert(pm <= 127);
    size_t m = pm%12;
    std::vector<PSConfig*> v = std::vector<PSConfig*>();

    for (size_t i = 0; i < 3; ++i)
    {
        int name = SYNONYM[m][i][0];
        int accid = SYNONYM[m][i][1];
        // case of 8 and (short list) 1, 3, 6, 10
        if ((name != SYNONYM_UNDEF) && (accid != SYNONYM_UNDEF))
            v.push_back(new PSConfigMark(*this, name, accid));
    }
    
    return v;
}


PSConfigMark::PSConfigMark(const PSConfigMark& c):
PSConfig(c),
_name(c._name),
_print(c._print)
{ }


PSConfigMark::PSConfigMark(const PSConfig& c, int n, int a):
PSConfig(c),
_name(n),
_print(_state.update(n, a)) 
{
    assert(0 <= n);
    assert(n <= 6);
    assert(-2 <= a);
    assert(a <= 2);
    _id = c.id() + 1;
    _pred = &c;
    // update cost when accident for the name was updated
    if (_print)
    {
        if (std::abs(a) == 2) // number of printed accident
            _cost += 2;
        else
            _cost += 1;
    }
}


PSConfigMark::~PSConfigMark()
{ }


// not name for each index
const char PSConfigMark::NAME[7] =
    {'C', 'D', 'E', 'F', 'G', 'A', 'B'};


char PSConfigMark::name() const
{
    assert(0 <= _name);
    assert(_name <= 6);
    return NAME[_name];
}


float PSConfigMark::alteration() const
{
    int accid = _state.accid(_name);
    assert(-2 <= accid);
    assert(accid <= 2);
    return ((float) accid); // cast float format for Pitch
}


bool PSConfigMark::printed() const
{
    return _print;
}


} // end namespace ScoreModel

/// @}

//
//  AccidentState.cpp
//  squant2
//
//  Created by Florent Jacquemard on 14/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#include "AccidentState.hpp"


namespace ScoreModel {

// const int AccidState::TON[15][7] =
const std::array<std::array<int, 7>, 15> AccidState::TON =
{{
    { -1, -1, -1, -1, -1, -1, -1 }, // -7
    { -1, -1, -1,  0, -1, -1, -1 }, // -6
    {  0, -1, -1,  0, -1, -1, -1 }, // -5
    {  0, -1, -1,  0,  0, -1, -1 }, // -4
    {  0,  0, -1,  0,  0, -1, -1 }, // -3
    {  0,  0, -1,  0,  0,  0, -1 }, // -2
    {  0,  0,  0,  0,  0,  0, -1 }, // -1
    {  0,  0,  0,  0,  0,  0,  0 }, //  0
    {  0,  0,  0,  1,  0,  0,  0 }, //  1
    {  1,  0,  0,  1,  0,  0,  0 }, //  2
    {  1,  0,  0,  1,  1,  0,  0 }, //  3
    {  1,  1,  0,  1,  1,  0,  0 }, //  4
    {  1,  1,  0,  1,  1,  1,  0 }, //  5
    {  1,  1,  1,  1,  1,  1,  0 }, //  6
    {  1,  1,  1,  1,  1,  1,  1 }  //  7
}};


AccidState::AccidState(int ton):
_state(TON[ton+7])
{
    assert(-7 <= ton);
    assert(ton <= 7);
}


AccidState::AccidState(const KeySig& ks)
{
    if (ks.flats() > 0)
    {
        assert(ks.flats()  <= 7);
        _state = TON[7 - ks.flats()];
    }
    else
    {
        assert(ks.sharps() <= 7);
        _state = TON[ks.sharps() + 7];
    }
}


AccidState::~AccidState()
{ }


AccidState::AccidState(const AccidState& as):
_state(as._state)
{ }


AccidState::AccidState(const AccidState& as, int name, int accid):
_state(as._state)
{
    assert(0 <= name);
    assert(name <= 6);
    assert(-2 <= accid);
    assert(accid <= 2);
    _state[name] = accid;
}


int AccidState::accid(int n) const
{
    assert(0 <= n);
    assert(n <= 6);
    return _state[n];
}


bool AccidState::update(int n, int a)
{
    assert(0 <= n);
    assert(n <= 6);
    assert(-2 <= a);
    assert(a <= 2);
    if (_state[n] == a)
        return false;
    else // real update
    {
        _state[n] = a;
        return true;
    }
}


} // end namespace ScoreModel


/// @}

//
//  AEVisitor.hpp
//  squant2
//
//  Created by Florent Jacquemard on 18/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup output
/// @{

#ifndef AEVisitor_hpp
#define AEVisitor_hpp

#include <stdio.h>
#include <assert.h>
//#include <exception>

class AtomicEvent;
class Note;
class GraceNote;
class Rest;
class Chord;
class GraceChord;
class Trill;


namespace ScoreModel {

/// non-const Visitor for the AtomicEvent's of the musical part of score model.
class AEVisitor
{
public:

    virtual void visitNote(Note*);
    virtual void visitGraceNote(GraceNote*);
    virtual void visitRest(Rest*);
    virtual void visitChord(Chord*);
    virtual void visitGraceChord(GraceChord*);
    virtual void visitTrill(Trill*);
    
};

} // endamespace ScoreModel

#endif /* AEVisitor_hpp */

/// @} // end group scoremodel





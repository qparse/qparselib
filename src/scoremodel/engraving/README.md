# src/scoremodel/engraving

An *engraving* model is a part of a score model containing engraving details about the events contained in a companion *musical* part of the score model. 
The engraving information is stored in one tree for every measure and every monophonic voice, plus some floating annotations called `Direction`.

The engraving model is presented into the following two diagrams  which are actually 2 parts of the same graph separated for presentation purpose. The parts describe respectively:

- a tree-structured representation of the content of a measure, displayed on a staff
  this representation defines also the durations of the events in the musical model (Engraving Tree).
- performance directives floating above or below the staff (`Directions` = `Local` and `Spanning`)

The diagrams contain:

- the inheritance relationships (plain bottom-up arrows),
- the contents relationships (arrows with a label `1` or `*`, top-down in general).



#### Engraving Tree:


```mermaid
classDiagram

class Node{
  int indegree
  int outdegree
  int nbChildren
  StaffRange staves
  container() Sequence
  measure() int
  parent() Inner
  prevSibling() Node
  replace(Node)
}
  Node <|-- Inner
  Node <|-- Leaf

  class Inner
  Inner <|-- Root
  Inner <|-- Decorated
  Inner <|-- Tuplet
  Inner: sub(int) Node
  Inner: add(Node) Node
  Inner: complete() bool 
  Inner: delete_tree()

  class Root
  Root -->"1" Node
  Root: int measure_nb_display
  Root: Barline type
  Root: tempo() Tempo
  Root: clef() Clef
  Root: timesig() MeterSig
  Root: key() KeySig
  Root: sign() RepeatMesure
  Root: last() Leaf 

  class Decorated
  Decorated -->"1" Ornament
  Decorated -->"1" LeafEvent
  Decorated: Ornament decoration 
  Decorated: LeafEvent decorated
  Decorated: add(Ornament)
%%  Decorated: add(Node) 
  Decorated: ornamentFirst() bool

  class Tuplet
  Tuplet -->"*" Node
  Tuplet: BeamInfo beaming
  Tuplet: TupletLabel label  
  
  class Leaf
  Leaf <|-- Continuation
  Leaf <|-- LeafEvent
  Leaf -->"*" Local
  Leaf -->"1" AtomicEvent
  Leaf: int voice_nb // Layer
  Leaf: previous() Leaf
  Leaf: next() Leaf

   class AtomicEvent
   <<musical>> AtomicEvent

  class Continuation
  Continuation <|-- Tie
  Continuation <|-- Dot
  
  class Tie

  class Dot
  
  class LeafEvent
  LeafEvent <|-- LeafNote
  LeafEvent <|-- LeafRest
  LeafEvent <|-- LeafChord
  LeafEvent <|-- LeafSpace
  
  class SingleNote
  SingleNote <|-- PitchedNote
  SingleNote <|-- SingleGraceNote
  SingleNote: Notehead head
  SingleNote: int accidental
  SingleNote: bool harmonic
%%  SingleNote: Stem s
%%  SingleNote: Articulation a
%%  SingleNote: int tremolo

  class PitchedNote
  PitchedNote <|-- LeafNote
  PitchedNote: Pitch pitch
  PitchedNote: int staff
  PitchedNote: equal()
  PitchedNote: smaller()

  class SingleGraceNote
  SingleGraceNote -->"1" GraceNote
  PitchedNote: Pitch pitch
  PitchedNote: int staff

   class GraceNote
   <<musical>> GraceNote

  class LeafNote
  LeafNote: Stem stem
  LeafNote: int tremolo

  class LeafChord
  LeafChord -->"*" PitchedNote
  LeafChord: Stem stem
  LeafChord: int tremolo
  LeafChord: Arpeggio type
  LeafChord: empty() bool
  LeafChord: bass() PitchedNote
  LeafChord: treble() PitchedNote

  class Ornament
  Ornament <|-- Appogiatura
  Ornament <|-- Mordent
  Ornament <|-- Gruppetto
  Ornament -->"*" SingleGraceNote
  Ornament: bool play_before
  Ornament: StaffRange staves
  Ornament: upMordent(Pitch) bool 
  Ornament: lowMordent(Pitch) bool 
  Ornament: gruppetto(Pitch) bool 
  Ornament: invGruppetto(Pitch) bool 

  class Appogiatura
  Appogiatura -->"*" SingleGraceNote
  Appogiatura: bool acciaccatura
  Appogiatura: mtu printed_duration
  Appogiatura: addGraceNote(Pitch)

  class Gruppetto
  Gruppetto --|> Local
  Gruppetto: bool inverted

  class Mordent
  Mordent --|> Local
  Mordent: bool up

```

Note on Ornaments: this diagram contains 4 nodes of the following Direction diagram (`Mordent`, `Gruppetto`, `Trill`, `Prall`). These correspond to the ornaments denoted above (or below) the decorated note. Another kind of Ornament is the `Appogiatora` = sequence of grace notes (denoted on the staff). All these 5 cases of Ornaments contain links to the corresponding musical events (grace notes) in a `Multi`.

The above nodes also contain pointers to elements of the model of musical content:

- the `AtomicEvent` container of a  `Leaf`
  one `AtomicEvent` may correspond to several `Leaf`
  *e.g.* note + tied notes or dotted note

- it is inherited by `Single`, contained in `Multi`'s  classes  `ChordNode`  (companion `Chord`) 
  and `Appogiatura`  (the vector of `GraceNote`).

  

#### Directions:

```mermaid
classDiagram

  class Local{
  Local: Vposition vpos
  Local: Hposition hpos
  Local: link() Leaf
}
  Local -->"1" HPosition
  Local -->"1" VPosition
  Local <|-- Mordent
  Local <|-- Gruppetto
  Local <|-- LocalTrill
  Local <|-- Articulation
  Local <|-- Dynamic
  Local <|-- Jump
  Local <|-- Fermata
  Local <|-- Breath
  Local <|-- ClefChange
  Local <|-- TempoChange
  Local <|-- Spanning


  class HPosition
  HPosition: Leaf link
  HPosition: mtu time
  HPosition: Part container
  HPosition: measure() Measure
  HPosition: date() mtu

  class VPosition
	VPosition: int staff
  VPosition: Orientation o  
  VPosition: bool bistaff 
  VPosition -->"1" Orientation

class Orientation{
    <<enumeration>>
    above
    below
}

  class Mordent
  Mordent --|> Ornament
  Mordent: bool up
  Mordent: float accident

  class Gruppetto
  Gruppetto --|> Ornament
  Gruppetto: bool inverted
  Gruppetto: float accident

  class LocalTrill
  LocalTrill: float interval
  LocalTrill: float accident

  class Articulation{
  <<enumeration>>
  Accent
  DetachedLegato
  Spiccato
  Staccato
  Staccatissimo
  Stress
  StrongAccent
  Tenuto
  Unstress
  UpBow
  DownBow
}

  class Dynamic{
  <<enumeration>>
  p
  ...
  pppppp
  f
  ...
  ffffff
  mp
  mf
  sf
  sfp
  sfpp
  fp
  rf
  rfz
  sfz
  sffz
  fz
  n
  pf
  sfzp
}

  class Jump{
  <<enumeration>>
  None
  Segno
  Coda
  CodaSquare
  DaCapo
  DaCapoAlFine
  DaCapoAlCoda
  DaCapoAlSegno
  DalSegno
  DalSegnoAlCoda
  DalSegnoAlFine
  Fine
}

  class Fermata{
  <<enumeration>>
  Normal
  Angled
  DoubleAngled
  Square
  DoubleSquare
  Henze
}

  class Breath{
  <<enumeration>>
  Comma
  Tick
  Saltzed
  UpBow
  Caesura
  CurvedCaesura
  ShortCaesura
  ThickCaesura
}

  class ClefChange
  ClefChange: Clef c
  
  class TempoChange
  TempoChange: Tempo t
  
  class Spanning
  Spanning: Hposition hpos2
  Spanning <|-- Line
  Spanning <|-- Slur
  Spanning <|-- Volta
  Spanning <|-- Octava
  Spanning <|-- VolChange
  Spanning <|-- Pedal
  Spanning <|-- Vibrato
  Spanning <|-- Glissando

  class Line
  Line: LineType type
  Line -->"1" LineType


  class Slur
  Slur: LineType type
  Slur -->"1" LineType

  class LineType{
  <<enumeration>>
  None
  Solid
  Dashed
  Dotted
  Wavy
}

  class Volta
  Volta: int nb
  Volta: bool open
  Volta: LineType type
  Volta -->"1" LineType

  class Octava
  Octava: int shift
  Octava: LineType type
  Octava --> LineType

  class VolChange
  VolChange: Shape shape
  VolChange: bool cresc | dim
  VolChange: LineType type
  VolChange -->"1" LineType
  VolChange -->"1" Shape
  
  class Shape{
  <<enumeration>>
  Hairpin
  Text
}

  class Pedal
  Pedal: PedalType type
  Pedal: bool line
  Pedal -->"1" PedalType

  class PedalType{
  <<enumeration>>
  Start
  Stop
  Change
  Continue 
}

  class Vibrato
  Vibrato: OscKind osc
  Vibrato -->"1" OscKind

  class OscKind{
  <<enumeration>>
  Trill
  Prall
  Upprall
  Downprall
  Vibrato  
}

  class Glissando

```


//
//  VPosition.hpp
//  squanty
//
//  Created by Florent Jacquemard on 30/07/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{


#ifndef VPosition_hpp
#define VPosition_hpp

#include <stdio.h>

#include "trace.hpp"
#include "MTU.hpp"
#include "StaffRange.hpp"


namespace ScoreModel {

class VPosition
{
public:
    
    /// vertical position relative to staff
    /// see MNX attribue [`@orient`](https://w3c.github.io/mnx/specification/common/#orientation)
    enum Orientation {
        Above,
        Below
    };

    /// Position relative to one or several staves.
    /// @param staff staff number.
    /// @param orient vertical position relative to staff.
    /// @param bis apply to two successive staves.
    VPosition(int staff, Orientation orient, bool bis = false);

    VPosition(const VPosition& p);
    
    inline int staff() const { return _staff; }

    inline Orientation orientation() const { return _orient; }

    inline bool twostaves() const { return _bistaff; }
    
    inline const StaffRange& staves() const { return _staves; }

    virtual void print(std::ostream& o) const;

    
    private:
    
    /// staff number for vertical position.
    /// @todo inherit Staffed ?
    int _staff;

    /// vertical position relative to staff
    Orientation _orient;
    
    /// apply to this staff and prev (if  orientation is `above`) or next (if `below`)
    bool _bistaff;
    
    StaffRange _staves;
    
    void init_staves();

};


std::ostream& operator<<(std::ostream&, const VPosition&);


} // namespace ScoreModel

#endif /* VPosition_hpp */

/// @} // group scoremodel

//
//  LeafNote.hpp
//  squanty
//
//  Created by Florent Jacquemard on 03/07/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef LeafNote_hpp
#define LeafNote_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>

#include "LeafEvent.hpp"
#include "SingleNote.hpp"
#include "Stem.hpp"

namespace ScoreModel {

/// a single note.
class LeafNote : public LeafEvent,
                 public SingleNote
{
public:

    /// single note tree, this note is the root, embedded in given wrapper.
    /// @param accident displayed accident or courtesy
    /// @param h shape of the note head.
    /// @param s direction of the stem.
    /// @param harm whether the note is an natural harmonic
    /// @todo harmonic redundant with note-head?
    /// @param tremolo integer from 0 to 8 indicating the number
    /// of tremolo marks over the stem (~ beams) for single note tremolo.
    LeafNote(Root& wrapper,
             int staff,
             float accident = Pitch::UNDEF_NOTE_ALTERATION,
             const Notehead& h = Notehead::Shape::Oval,
             const Stem& s = Stem::Type::Unspecified,
             bool harm=false,
             int tremolo=0,
             Leaf* previous=NULL, Leaf* next=NULL,
             AtomicEvent* e=NULL);
    
    /// note leaf with a parent.
    /// @param accident displayed accident or courtesy
    /// @param h shape of the note head.
    /// @param s direction of the stem.
    /// @param harm whether the note is an natural harmonic
    /// @todo harmonic redundant with note-head?
    /// @param tremolo integer from 0 to 8 indicating the number
    /// of tremolo marks over the stem (~ beams) for single note tremolo.
    LeafNote(size_t mult,
             Inner* parent,
             int staff,
             float accident = Pitch::UNDEF_NOTE_ALTERATION,
             const Notehead& h = Notehead::Shape::Oval,
             const Stem& s = Stem::Type::Unspecified,
             bool harm=false,
             int tremolo=0,
             Leaf* previous=NULL, Leaf* next=NULL,
             AtomicEvent* e=NULL);

    /// copy
    LeafNote(const LeafNote& n);

    virtual bool isNote() const { return true; }
    
    virtual void accept(SMVisitor*) const;

    virtual void print(std::ostream& o) const;

private:

    // const Notehead _head;

    /// direction of stem
    const Stem _stem;

    // @see Pitch.alteration
    // float _accidental;

    // natural harmonic
    // bool _harmonic;

    /// integer from 0 to 8 indicating the number
    /// of tremolo marks over the stem (~ beams) for single note tremolo.
    int _tremolo;
};

std::ostream& operator<<(std::ostream&, const LeafNote&);

} // namespace ScoreModel


/// @}
#endif /* LeafNote_hpp */


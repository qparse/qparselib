//
//  Articulation.cpp
//  squanty
//
//  Created by Florent Jacquemard on 05/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Articulation.hpp"



namespace ScoreModel {


Articulation::Articulation(const VPosition& vpos, Leaf* link, Kind kind):
Local(vpos, link),
_kind(kind)
{ }


Articulation& Articulation::operator=(const Articulation& rhs)
{
    if(this != &rhs)
    {
        _kind = rhs._kind;
    };

    return *this;
}


bool Articulation::operator==(const Articulation& rhs) const
{
    return _kind == rhs._kind;
}


char Articulation::sign() const
{
    switch (_kind)
    {
        case Articulation::None:
            return 0;

        case Articulation::Accent:
            return '>';

        case Articulation::DetachedLegato:
            return ';';

        case Articulation::Spiccato:
            return '\'';

        case Articulation::Staccatissimo:
            return '^';

        case Articulation::Staccato:
            return '.';

        case Articulation::Stress:
            return '`';

        case Articulation::StrongAccent:
            return 'v';

        case Articulation::Tenuto:
            return '-';

        case Articulation::Unstress:
            return 'U';

        default:
        {
            ERROR("unknown articulation kind {}", _kind);
            return 0;
        }
    }
}


void Articulation::print(std::ostream& o) const
{
    o << "Articulation: ";
    o << "\"" << sign() << "\"";
}


std::ostream& operator<<(std::ostream& o, const Articulation& a)
{
    a.print(o);
    return o;
}


} // end namespace ScoreModel

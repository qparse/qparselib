//
//  StaffRange.cpp
//  squant2
//
//  Created by Florent Jacquemard on 29/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "StaffRange.hpp"

namespace ScoreModel {


StaffRange::StaffRange():
_staff_min(UNDEF_STAFF),
_staff_max(UNDEF_STAFF)
{ }


StaffRange::StaffRange(int s):
_staff_min(s),
_staff_max(s)
{
    assert(s >= 0);
}


StaffRange::StaffRange(int min, int max):
_staff_min(min),
_staff_max(max)
{
    assert(0 <= min);
    assert(0 <= max);
    assert(min <= max);
}


StaffRange::StaffRange(const StaffRange& r):
_staff_min(r._staff_min),
_staff_max(r._staff_max)
{
    // invariant

    // domain
    assert((_staff_min == UNDEF_STAFF) || (_staff_min >= 0));
    assert((_staff_max == UNDEF_STAFF) || (_staff_max >= 0));

    // both undef or none undef
    assert((_staff_min != UNDEF_STAFF) || (_staff_max == UNDEF_STAFF));
    assert((_staff_max != UNDEF_STAFF) || (_staff_min == UNDEF_STAFF));

    // interval
    assert(((_staff_min == UNDEF_STAFF) || (_staff_max == UNDEF_STAFF)) ||
           (_staff_min <= _staff_max));
}


size_t StaffRange::size() const
{
    if ((_staff_min == UNDEF_STAFF) || (_staff_max == UNDEF_STAFF))
        return 0;
    else
    {
        assert(_staff_min >= 0);
        assert(_staff_max >= 0);
        assert(_staff_min <= _staff_max);
        return (_staff_max - _staff_min + 1);
    }
}


bool StaffRange::contains(int n) const
{
    assert(n >= 0);
    // empty
    if ((_staff_min == UNDEF_STAFF) && (_staff_max == UNDEF_STAFF))
        return false;
//    else if (_staff_min == UNDEF_STAFF)
//        return (n <= _staff_max);
//    else if (_staff_max == UNDEF_STAFF)
//        return (_staff_min <= n);
    else // both defined by invariant
    {
        assert(_staff_min >= 0);
        assert(_staff_max >= 0);
        assert(_staff_min <= _staff_max);
        return ((_staff_min <= n) && (n <= _staff_max));
    }
}


int StaffRange::staff() const
{
    if (singleton())
    {
        assert(_staff_min >= 0);
        assert(_staff_max == _staff_min);
        return _staff_min;
    }
    else
        return UNDEF_STAFF;
}


void StaffRange::add(int n)
{
//    assert(n != UNDEF_STAFF);
    assert(n >= 0);
    
    if (_staff_min == UNDEF_STAFF)
    {
        if (_staff_max == UNDEF_STAFF)
        {
            // singleton
            _staff_min = n;
            _staff_max = n;
        }
        // should not happen
        else if (_staff_max < n)
        {
            _staff_min = _staff_max;
            _staff_max = n;
        }
        else
        {
            _staff_min = n;
        }
    }
    else if (_staff_max == UNDEF_STAFF)
    {
        if (n < _staff_min)
        {
            _staff_max = _staff_min;
            _staff_min = n;
        }
        else
        {
            _staff_max = n;
        }
    }
    // _staff_max and _staff_min are defined
    else if (n < _staff_min)
    {
        assert(_staff_min <= _staff_max);
        _staff_min = n;
    }
    else if (_staff_max < n)
    {
        assert(_staff_min <= _staff_max);
        _staff_max = n;
    }
    else // ignore
    {  }

    // both defined
    assert(_staff_min >= 0);
    assert(_staff_max >= 0);
    assert(_staff_min <= n);
    assert(n <= _staff_max);
}


void StaffRange::merge(const StaffRange& r)
{
    // ignore empty range
    if (r.empty())
        return;
    assert(r._staff_min != UNDEF_STAFF);
    add(r._staff_min);
    assert(r._staff_max != UNDEF_STAFF);
    if (r._staff_max != r._staff_min) // not singleton
        add(r._staff_max);
}


} // namespace ScoreModel


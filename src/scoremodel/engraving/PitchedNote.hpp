//
//  PitchedNote.hpp
//  squant2
//
//  Created by Florent Jacquemard on 07/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef PitchedNote_hpp
#define PitchedNote_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>

#include "SingleNote.hpp"
#include "Staffed.hpp"
#include "Pitch.hpp"
#include "SMVisitor.hpp"


namespace ScoreModel {


/// note in a chord.
/// extension of SingleNote with a pitch value and a staff location,
/// replacing the pitch of a scoremodel embedding.
class PitchedNote : public SingleNote,
                    public Staffed
{
    
public:

    /// pitched note elements
    /// @param p pitch value (copied)
    /// @param staff number of staff where the note is located
    /// @param accident displayed accident or courtesy
    /// @param h shape of the note head.
    /// @param harm whether the note is an natural harmonic
    /// @todo harmonic redundant with note-head?
    PitchedNote(const Pitch& p,
                int staff,
                float accident = Pitch::UNDEF_NOTE_ALTERATION,
                const Notehead& h = Notehead::Shape::Oval,
                bool harm = false);

    /// copy
    PitchedNote(const PitchedNote& pn);
    
    virtual ~PitchedNote() {}
    
    inline const Pitch& pitch() const { return _pitch; }
    
    bool equal(const PitchedNote&) const;

    bool smaller(const PitchedNote&) const;
        
    virtual void accept(SMVisitor*) const;

    virtual void print(std::ostream& o) const;

protected:
    
    Pitch _pitch;
  
};

// boolean operators

inline bool operator==(const PitchedNote& lhs, const PitchedNote& rhs)
{
    return lhs.equal(rhs);
}

inline bool operator!=(const PitchedNote& lhs, const PitchedNote& rhs)
{
    return !operator==(lhs, rhs);
}

inline bool operator<(const PitchedNote& lhs, const PitchedNote& rhs)
{
    return (lhs.smaller(rhs));
}

inline bool operator>(const PitchedNote& lhs, const PitchedNote& rhs)
{
    return  operator<(rhs,lhs);
}

inline bool operator<=(const PitchedNote& lhs, const PitchedNote& rhs)
{
    return !operator>(lhs, rhs);
}

inline bool operator>=(const PitchedNote& lhs, const PitchedNote& rhs)
{
    return !operator<(lhs,rhs);
}

std::ostream& operator<<(std::ostream&, const PitchedNote&);


} // namespace ScoreModel


/// @}

#endif /* PitchedNote_hpp */

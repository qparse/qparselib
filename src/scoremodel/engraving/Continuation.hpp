//
//  Continuation.hpp
//  squanty
//
//  Created by Florent Jacquemard on 20/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef SM_Continuation_hpp
#define SM_Continuation_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>

#include "Leaf.hpp"

namespace ScoreModel {

/// leaf node in a tree representation of engraving information,
/// containing an event continuating a previous event.
/// It corresponds to notational elements such as ties and dots.
/// This event's will have same pitch.s than the previous.
/// This pitch is contained in the embedding score event.
/// @todo TBR?
class Continuation : public Leaf
{
public:
    
    /// construct a single node tree: the whole measure is a continuation.
    /// @see Leaf
    /// @param e pointer to the embedding score model element,
    /// i.e. the event this continuation is part of; must be an allocated.
    /// @warning previous must be allocated.
    Continuation(Root& wrapper,
                 AtomicEvent* e,
                 Leaf* previous, Leaf* next=NULL);
   
    /// construct a continuation node below of another node.
    /// @see Leaf
    /// @param e pointer to the embedding score model element,
    /// i.e. the event this continuation is part of; must be an allocated.
    /// @warning previous must be allocated.
    Continuation(size_t mult,
                 Inner* parent,
                 AtomicEvent* e,
                 Leaf* previous, Leaf* next=NULL);
    
    /// construct a continuation root node without wrapper.
    /// @see Leaf
    /// @todo TBR deprecated
    Continuation(mtu_t start, mtu_t dur,
                 Sequence& container,
                 AtomicEvent* e,
                 Leaf* previous, Leaf* next=NULL);
    
    /// copy
    Continuation(const Continuation& c);   

    virtual ~Continuation() {}
   
    /// @brief leaf event continuated by this leaf
    /// or NULL if there are none.
    //Leaf* const previous;
    //Leaf* const continuated;

    /// @brief leaf event continuating this leaf
    /// or NULL if there are none.
    //Leaf* const continuation;
    //void setContinuation(Leaf*);

    virtual bool isContinuation() const { return true; }
    
    virtual void print(std::ostream& o) const;    

};


std::ostream& operator<<(std::ostream&, const Continuation&);


} // namespace ScoreModel

/// @}
#endif /* SM_Continuation_hpp */


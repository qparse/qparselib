//
//  CMND.hpp
//  squant2
//
//  Created by Florent Jacquemard on 05/01/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#ifndef CMNDuration_hpp
#define CMNDuration_hpp

#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include <math.h>  // pow

#include "trace.hpp"

namespace ScoreModel {

/// @brief representation of durations in CMN.
/// Written duration values for the CMN repertoire.
/// see https://music-encoding.org/guidelines/v4/data-types/data.duration.cmn.html
typedef unsigned long cmnd_t;

/// Allowed Values:
/// - `maxima` Octuple whole note (or duplex longa or larga or large).
/// - `long` Quadruple whole note (or longe or longae or quadruple note).
/// - `breve` Double whole note (or double note).
/// - `1` Whole note (or semibreve).
/// - `2` Half note (or minim).
/// - `4` Quarter note (or crotchet).
/// - `8` 8th note (or quaver).
/// - `16` 16th note (or semiquaver).
/// - `32` 32nd note (or demisemiquaver).
/// - `64` 64th note (or hemidemisemiquaver or semidemisemiquaver).
/// - `128` 128th note (or semihemidemisemiquaver or quasihemidemisemiquaver).
/// - `256` 256th note (or demisemihemidemisemiquaver).
/// - `512` 512th note.
/// - `1024` 1024th note.
/// - `2048` 2048th note.
namespace CMN
{

    /// unknown CMN duration value
    const cmnd_t UNDEF  =   UINT_MAX;

    /// maximal number of flags or beams
    const unsigned long MAX_FLAGS = 11;

    /// inverse of shortest note duration (in nb of whole notes) = 2048th note
    const unsigned long SHORTEST = (1<<MAX_FLAGS);

    const cmnd_t MAXIMA =   9998;
    const cmnd_t LONG   =   9997;
    const cmnd_t BREVE  =   0;
    const cmnd_t WHOLE  =   1;

    /// literal CMN encodings
    const cmnd_t HALF   =   2;
    const cmnd_t QUARTER =  4;
    const cmnd_t QUAVER  =  8;

    /// the given time value is one of the allowed values.
    bool defined(cmnd_t d);

    /// the given time value is UNDEF
    bool undefined(cmnd_t d);

    /// @todo divide (for dur of child in tuplets)

    cmnd_t stocmnd(const std::string& str);

    /// number of flags of beams for the CMN notation of the given CMN duration.
    /// @param d CMN duration. must be defined.
    unsigned long to_flags(cmnd_t d);

} // namespace CMN

} // namespace ScoreModel



#endif /* CMNDuration_hpp */

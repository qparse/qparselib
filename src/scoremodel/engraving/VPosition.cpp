//
//  VPosition.cpp
//  squanty
//
//  Created by Florent Jacquemard on 30/07/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "VPosition.hpp"


namespace ScoreModel {


VPosition::VPosition(int staff,
                     Orientation orient,
                     bool bis):
_staff(staff),
_orient(orient),
_bistaff(bis)
// empty, initialized below
{
    assert(_staff >= 0);
    assert((_orient == Above) || (_orient == Below));
    //init_staves();
    if (_bistaff && (_orient == Above))
    {
        assert(_staff > 0);
        _staves = StaffRange(_staff - 1, _staff);
    }
    else if (_bistaff && (_orient == Below))
        _staves = StaffRange(_staff, _staff + 1);
    else
    {
        assert(! _bistaff);
        _staves = StaffRange(_staff);
    }
}


VPosition::VPosition(const VPosition& p):
_staff(p._staff),
_orient(p._orient),
_bistaff(p._bistaff),
_staves(p._staves)
{
    assert(_staff >= 0);
    assert((_orient == Above) || (_orient == Below));
}


void VPosition::init_staves()
{
    _staves.add(_staff);
    if (_bistaff)
    {
        if (_orient == VPosition::Above)
        {
            assert(_staff > 0);
            _staves.add(_staff - 1);
        }
        else if (_orient == VPosition::Below)
        {
            _staves.add(_staff + 1);
        }
        else
        {
            ERROR("VPosition: unexpected Orientation value {}", _orient);
        }
    }
}


void VPosition::print(std::ostream& o) const
{
    o << "VPOS: staff=" << _staff;
    switch (_orient)
    {
        case Above:
            if (_bistaff) o << "+" << (_staff-1);
            else o << " above";
            break;
        case Below:
            if (_bistaff) o << "+" << (_staff+1);
            else o << " below";
            break;
        default:
            ERROR("VPosition: unexpected orientation value {}", _orient);
            break;
    }
}


std::ostream& operator<<(std::ostream& o, const VPosition& p)
{
    p.print(o); // dispatch to descendant classes
    return o;
}




} // namespace ScoreModel

//
//  LocalTrill.hpp
//  squanty
//
//  Created by Florent Jacquemard on 27/08/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{


#ifndef LocalTrill_hpp
#define LocalTrill_hpp

#include <stdio.h>

#include "Ornament.hpp"
#include "Local.hpp"

namespace ScoreModel {


/// annotation of note to indicate to play a trill
/// @todo make it an Ornament
class LocalTrill : public Local
{
public:
    /// @param width distance in semi-tone between the main and
    /// second note of trill.
    /// @see Pitch.alteration for the type.
    LocalTrill(const VPosition& vpos,
               Leaf* link,
               float width=-1);
    
    ~LocalTrill() {}

    /// mordent is upper or lower
    /// @return true if the this mordent is an upper mordent
    /// and false if it is a lower mordent.
    float second() const { return _second; }

    virtual bool isTrill() const { return true; }

    virtual void print(std::ostream& o) const;

protected:

    /// distance in semi-tone from the main and second note of trill.
    /// @see Pitch.alteration
    /// @todo or alteration?
    float _second;

};

std::ostream& operator<<(std::ostream&, const LocalTrill&);

} // namespace ScoreModel

#endif /* LocalTrill_hpp */

/// @}

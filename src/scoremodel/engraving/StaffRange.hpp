//
//  StaffRange.hpp
//  squant2
//
//  Created by Florent Jacquemard on 29/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef StaffRange_hpp
#define StaffRange_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>

#include "trace.hpp"
#include "Staffed.hpp" // for UNDEF_STAFF

namespace ScoreModel {


/// @brief interval of staff number for elements that can be printed over multiple successive staves.
/// an interval of size one can also be used for elements printed on a unique staff.
/// The lowwer and upper bounds of the interval  are positive or null integers or
/// they are both UNDEF_STAFF (empty range).
class StaffRange
{
public:

    /// empty staff rangle.
    StaffRange();

    /// singleton staff rangle.
    /// @param s staff number. must not be positive or null and not be UNDEF_STAFF.
    StaffRange(int s);
    
    /// @param min smallest staff number in this range. must not be UNDEF_STAFF.
    /// @param max highest staff number in this range. must not be UNDEF_STAFF must be larger than min.
    StaffRange(int min, int max);

    StaffRange(const StaffRange& r);

    ~StaffRange() { }

    /// @return min staff number in this range, UNDEF_STAFF if there is none.
    inline int min() const { return _staff_min; }
    
    /// @return max staff number in this range, UNDEF_STAFF if there is none.
    inline int max() const { return _staff_min; }

    /// @return the number of staves in this range or 0 if the min or max of the range is empty.
    size_t size() const;
    
    bool empty() const { return size() == 0; }
    bool singleton() const { return size() == 1; }

    /// the given staff number is inside this range.
    /// @param n a staff number. must be defined.
    bool contains(int n) const;
    
    /// for a singleton staff, the unique staff in rage, or UNDEF_STAFF otherwise.
    int staff() const;

    /// @param n staff number. must be defined.
    void add(int n);
    
    /// add all staves of the given staff range to this staff range.
    /// @param r another staff range. ignored if one of the bounds is empty.
    void merge(const StaffRange& r);
    
private:
    
    /// lower bound (staff number) of this range
    int _staff_min;

    /// upper bound (staff number) of this range
    int _staff_max;

};

} // namespace ScoreModel

/// @}

#endif /* StaffRange_hpp */

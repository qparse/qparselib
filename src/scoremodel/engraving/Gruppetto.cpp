//
//  Gruppetto.cpp
//  qparse
//
//  Created by Florent Jacquemard on 18/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Gruppetto.hpp"


namespace ScoreModel {


Gruppetto::Gruppetto(const VPosition& vpos, Leaf* link, bool before,
                     bool invert):
Ornament(before),
Local(vpos, link),
_inverted(invert)
{ }


Gruppetto::Gruppetto(const Ornament& o,
                     const VPosition& vpos, Leaf* link,
                     bool invert):
Ornament(o),
Local(vpos, link),
_inverted(invert)
{ }


void Gruppetto::accept(SMVisitor* v) const
{
    v->visitGruppetto(this);
}


void Gruppetto::print(std::ostream& o) const
{
    if (_inverted)
        o << "Gruppetto ";
    else
        o << "i-Gruppetto ";
}


std::ostream& operator<<(std::ostream& o, const Gruppetto& g)
{
    g.print(o);
    return o;
}

} // namespace ScoreModel

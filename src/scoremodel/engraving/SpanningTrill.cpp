//
//  SpanningTrill.cpp
//  squanty
//
//  Created by Florent Jacquemard on 02/09/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "SpanningTrill.hpp"


namespace ScoreModel {


SpanningTrill::SpanningTrill(const VPosition& vpos,
                             const HPosition& hp1, const HPosition& hp2,
                             Kind k):
Spanning(vpos, hp1, hp2),
_kind(k)
{ }


void SpanningTrill::print(std::ostream& o) const
{
    o << "SpanningTrill: ";
    o << _kind;
}


} // namespace ScoreModel

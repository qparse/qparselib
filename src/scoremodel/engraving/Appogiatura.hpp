//
//  Appogiatura.hpp
//  qparse
//
//  Created by Florent Jacquemard on 18/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{


#ifndef SM_Appogiatura_hpp
#define SM_Appogiatura_hpp

#include <stdio.h>
#include <vector>

#include "util.hpp"
#include "MTU.hpp"
#include "Ornament.hpp"
#include "Root.hpp"
#include "SMVisitor.hpp"


namespace ScoreModel {

/// 2 cases of ornaments:
/// - Appogiatura: added note that is important melodically,
///   and suspends the principal note by a portion of its
///   time-value
/// - Acciaccatura: shorter, performed before the beat and
///   the emphasis is on the main note, not the grace note
///   (classical period).
class Appogiatura : public Ornament
{
public:

    /// empty appogiatura (not linked to score grace-notes)
    /// @param before flag see Ornament
    /// @param d inverse of printed duration, in fraction of  beat.
    /// @param stroke if diagonal stroke will be printed. content can be added with Ornament.add
    /// @warning the printed duration  `d` is only for rendering,
    /// the theoretical duration of an appogiatura is always 0.
    /// Unlike the other note durations, it is NOT expressed in fraction of one measure
    /// but in fraction of one beat.
    Appogiatura(bool before=true,
                mtu_t d = MTU::make(0,1,2),
                bool stroke = false);

    /// appogiatura with copy of content of given ornament
    /// (linked to score grace-notes)
    /// @param d inverse of printed duration, in fraction of
    /// beat.
    /// @param stroke if diagonal stroke will be printed.
    /// @warning the printed duration `d` is only for rendering,
    /// the theoretical duration of an appogiatura is always 0.
    /// Unlike the other note durations, it is NOT expressed in
    /// fraction
    /// of one measure but in fraction of one beat.
    Appogiatura(const Ornament& o,
                mtu_t d = MTU::make(0,1,2),
                bool stroke = false);

    virtual ~Appogiatura();

    /// add the given pitch as a grace note.
    /// @param p pitch to add. it is copied in this appogiatura.
    /// @todo TBR. replaced by Ornament.add(GreaceNote*)
    void add(const Pitch& p);
      
    /// the duration of each grace note in this appogiatura, for printing.
    cmnd_t printedDuration() const;
        
    /// add printed content.
    /// @param t must be created with the printed duration of this appogiatura.
    /// @warning can only be called once.
    void addPrinted(Root* t);
    
    /// An acciaccatura is a short appoggiatura,
    /// denoted as 8th note with oblique stroke through the stem.
    virtual bool isAcciaccatura() const { return _stroke; }

    /// An appoggiatura is denoted without oblique stroke.
    virtual bool isAppogiatura() const { return (! _stroke); }

    /// a grace note is an accaciatura with a single note,
    /// denoted as 8th note with oblique stroke through the stem.
    /// @warning the printed content must be set.
    bool isGraceNote() const;

    virtual void accept(SMVisitor*) const;

    virtual void print(std::ostream& o) const;

    //friend std::ostream& operator<<(std::ostream&, const Note&);

protected:
    
    /// printed diagonal stroke 
    /// characteristic of acciaccatura.
    bool _stroke;
    
    /// the total rendered duration of the appogiatura, expressed in fraction of one quarter note.
    /// @warning unlike the other note durations, it is NOT expressed in fraction of one measure.
    mtu_t _printed_dur;
    
    Root* _printed_content;
    
};


std::ostream& operator<<(std::ostream& o, const Appogiatura& a);


} // namespace ScoreModel

#endif /* SM_Appogiatura_hpp */

/// @}

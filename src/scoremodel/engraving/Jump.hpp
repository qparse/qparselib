//
//  Jump.hpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#ifndef Jump_hpp
#define Jump_hpp

#include <stdio.h>

#include "Local.hpp"

namespace ScoreModel {

/// @brief Navigation markers
class Jump : public Local
{
public:

    enum Kind {
        None,
        Segno,
        Coda,
        CodaSquare,
        DaCapo,
        DaCapoAlFine,
        DaCapoAlCoda,
        DaCapoAlSegno,
        DalSegno,
        DalSegnoAlCoda,
        DalSegnoAlFine,
        Fine
    };

    Jump(const VPosition& vpos, Leaf* link, Kind m);
    Jump(const VPosition& vpos, mtu_t date, const Part& p, Kind m);

    virtual bool isJump() const { return true; }
    
    Kind mark() const { return _marker; }
    
    virtual void print(std::ostream& o) const;
       
private:

    Kind _marker;
    
};

std::ostream& operator<<(std::ostream&, const Jump&);

} // namespace ScoreModel

#endif /* Jump_hpp */

/// @} // end group scoremodel

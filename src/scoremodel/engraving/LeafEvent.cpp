//
//  LeafEvent.cpp
//  squanty
//
//  Created by Florent Jacquemard on 20/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "LeafEvent.hpp"

namespace ScoreModel {


LeafEvent::LeafEvent(Root& wrapper,
                     //int staff,
                     Leaf* previous, Leaf* next,
                     AtomicEvent* e):
Leaf(wrapper, e, previous, next)
{
    assert((e == NULL) || (e->date() == start()));
}


LeafEvent::LeafEvent(size_t mult,
                     Inner* parent,
                     //int staff,
                     Leaf* previous, Leaf* next,
                     AtomicEvent* e):
Leaf(mult, parent, e, previous, next)
{
    assert((e == NULL) || (e->date() == start()));
}


LeafEvent::LeafEvent(const LeafEvent& n):
Leaf(n)
{ }


void LeafEvent::setEvent(AtomicEvent* e)
{
    assert(e);
    assert(e->date() == start());
    _embedding = e;
}



//void LeafEvent::print(std::ostream& o) const
//{
//    assert(embedding);
//    embedding->print(o);
//    if (staccato)
//        o << "(stacc.)";
//}


std::ostream& operator<<(std::ostream& o, const LeafEvent& e)
{
    e.print(o);
    return o;
}


} // namespace ScoreModel

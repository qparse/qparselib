//
//  Pedal.hpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#ifndef Pedal_hpp
#define Pedal_hpp

#include <stdio.h>

#include "Spanning.hpp"

namespace ScoreModel {

/// @brief Pedal markers
class Pedal : public Spanning
{
public:

    /// types of pedal directions
    enum Type {
        Start,
        Stop,
        Change,
        Continue
    };
   
    
    /// @param vpos vertical position.
    /// @param hp1 starting horizontal position.
    /// @param hp2 ending horizontal position.
    /// @param type start-stop-change-continue type of pedal direction
    /// @param line if true, notation is Ped followed by a line,
    /// otherwise, notation is Ped      *
    Pedal(const VPosition& vpos, const HPosition& hp1, const HPosition& hp2,
          Type type, bool line);

    virtual bool isPedal() const { return true; }

    inline Type type() const { return _type; }

    inline bool line() const { return _line; }
       
    virtual void print(std::ostream& o) const;

private:
    
    /// start-stop-change-continue type
    Type _type;
    
    /// if true, notation is Ped followed by a line
    /// if false, notation is Ped      *
    bool _line;

};

} // namespace ScoreModel

#endif /* Pedal_hpp */

/// @} // end group scoremodel

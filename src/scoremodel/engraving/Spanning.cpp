//
//  Spanning.cpp
//  squanty
//
//  Created by Florent Jacquemard on 24/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Spanning.hpp"


namespace ScoreModel {

Spanning::Spanning(const VPosition& vpos,
                   const HPosition& hp1, const HPosition& hp2):
Local(vpos, hp1),
_hposend(hp2)
{
    if (hp2.linked())
    {
        assert(hp2.leaf());
        hp2.leaf()->addLink(this);
    }
}


Spanning::Spanning(const VPosition& vpos, Leaf* hp1, Leaf* hp2):
Local(vpos, hp1),
_hposend(hp2)
{
    assert(hp2);
    hp2->addLink(this);
}


Spanning::Spanning(const VPosition& vpos, Leaf* hp1,
                   mtu_t hp2, const Part& p):
Local(vpos, hp1),
_hposend(hp2, p)
{ }


Spanning::Spanning(const VPosition& vpos, mtu_t hp1, const Part& p,
                   Leaf* hp2):
Local(vpos, hp1, p),
_hposend(hp2)
{
    assert(hp2);
    hp2->addLink(this);
}


Spanning::Spanning(const VPosition& vpos,
                   mtu_t hp1, const Part& p,
                   mtu_t hp2):
Local(vpos, hp1, p),
_hposend(hp2, p)
{ }


std::ostream& operator<<(std::ostream& o, const Spanning& p)
{
    p.print(o); // dispatch to descendant classes
    o << " from " << p.hposition();
    o << " to " << p.hposition2();
    o << " vpos= " << p.vposition();
    return o;
}


} // namespace ScoreModel

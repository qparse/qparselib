//
//  CMND.cpp
//  squant2
//
//  Created by Florent Jacquemard on 05/01/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#include "CMND.hpp"
#include "util.hpp"

namespace ScoreModel {

bool CMN::defined(cmnd_t d)
{
    for (unsigned long val = 1; val <= 2048; val <<= 1)
    {
        if (d == val)
            return true;
    }

    return ((d == MAXIMA) || (d == LONG) || (d == BREVE));
}


bool CMN::undefined(cmnd_t d)
{
    return (d == CMN::UNDEF);
}


cmnd_t CMN::stocmnd(const std::string& str)
{
    
    if (str.compare("long") == 0)
        return CMN::LONG;
    else if (str.compare("breve") == 0)
        return CMN::BREVE;
    else if (str.compare("whole") == 0)
        return CMN::WHOLE;
    else
    {
        long ret = std::stoi(str);
        if (defined(ret))
            return (cmnd_t) ret;
        else
        {
            ERROR("fail casting {} to CMN duration", str);
            return CMN::UNDEF;
        }
    }
}


unsigned long CMN::to_flags(cmnd_t d)
{
    assert(defined(d));
    
    if ((d == MAXIMA) ||
        (d == LONG) ||
        (d == BREVE) ||
        (d == WHOLE) ||
        (d == HALF) ||
        (d == QUARTER))
    {
        return 0;
    }

    // log2(d) - 2;
    if (d > 0)
    {
        // int val = d;
        // int ret = 0;
        // while (val >>= 1) ++ret;
        // alt: GCC __builtin_clz
        // (emulate BSR instruction on x86 or x86-64 and CLZ on ARM)
        // alt2: with de Bruijn sequences
        unsigned long ret = util::trunclog2(d);
        assert(3 <= ret);
        assert(ret <= 11);
        return ret - 2;
    }
    else // should not happen
    {
        ERROR("CMN.to_flags: non-admissible value {}", d);
        return 99;
    }
}


} // namespace ScoreModel

//
//  Dynamic.hpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#ifndef Dynamic_hpp
#define Dynamic_hpp

#include <stdio.h>

#include "Local.hpp"

namespace ScoreModel {

/// @brief Dynamic marking
class Dynamic : public Local
{
public:

    enum Kind {
       p,
       pp,
       ppp,
       pppp,
       ppppp,
       pppppp,
       f,
       ff,
       fff,
       ffff,
       fffff,
       ffffff,
       mp,
       mf,
       sf,
       sfp,
       sfpp,
       fp,
       rf,
       rfz,
       sfz,
       sffz,
       fz,
       n,
       pf,
       sfzp
    };
    
    Dynamic(const VPosition& vpos, Leaf* link, Kind mark);
    Dynamic(const VPosition& vpos, mtu_t date, const Part& p, Kind mark);
    
    virtual bool isDynamic() const { return true; }
        
    Kind mark() const { return _marker; }

    virtual void print(std::ostream& o) const;

private:

    Kind _marker;

};

std::ostream& operator<<(std::ostream&, const Dynamic&);

} // namespace ScoreModel

#endif /* Dynamic_hpp */

/// @} // end group scoremodel

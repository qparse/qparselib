//
//  Volta.cpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Volta.hpp"

namespace ScoreModel {

Volta::Volta(const VPosition& vpos,
             const HPosition& hp1, const HPosition& hp2,
             int n, bool open):
Spanning(vpos, hp1, hp2),
_nb(n),
_open(open)
{
    assert((n == 1) || (n == 2) || (n == 3));
}


Volta::Volta(const VPosition& vpos,
             const HPosition& hp1, const HPosition& hp2,
             int n):
Spanning(vpos, hp1, hp2),
_nb(n),
_open(((n == 1) || (n == 3))?false:true)
{
    assert((n == 1) || (n == 2) || (n == 3));
}


void Volta::print(std::ostream& o) const
{
    o << "Volta: ";
    o << _nb;
    if (_open)
        o << "(open)";
    else
        o << "(close)";
}


} // namespace ScoreModel

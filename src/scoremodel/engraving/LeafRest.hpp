//
//  LeafRest.hpp
//  squanty
//
//  Created by Florent Jacquemard on 03/07/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef LeafRest_hpp
#define LeafRest_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>

#include "LeafEvent.hpp"


namespace ScoreModel {

/// a notated rest
/// http://usermanuals.musicxml.com/MusicXML/MusicXML.htm#CT-MusicXML-rest.htm
class LeafRest : public LeafEvent
{
public:

    /// single rest tree, this rest is the root, embedded in given wrapper.
    LeafRest(Root& wrapper,
             int staff,
             Leaf* previous=NULL, Leaf* next=NULL,
             AtomicEvent* e=NULL);
    
    /// rest leaf with a parent.
    LeafRest(size_t mult,
             Inner* parent,
             int staff,
             Leaf* previous=NULL, Leaf* next=NULL,
             AtomicEvent* e=NULL);

    virtual bool isRest() const { return true; }
    
    virtual void accept(SMVisitor*) const;

    virtual void print(std::ostream& o) const;

/// @todo add rest type (quarter etc)?
    
};

std::ostream& operator<<(std::ostream&, const LeafRest&);


} // namespace ScoreModel

/// @}
#endif /* LeafRest_hpp */


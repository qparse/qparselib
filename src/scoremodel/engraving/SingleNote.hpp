//
//  SingleNote.hpp
//  qparse
//
//  Created by Florent Jacquemard on 19/11/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef SingleNote_hpp
#define SingleNote_hpp
/// @addtogroup scoremodel
/// @{


#include <stdio.h>

#include "Notehead.hpp"
#include "Pitch.hpp"

namespace ScoreModel {

/// standalone class containing some information common to
/// a single note and a note in a chord.
///
/// In contrary to LeafNote and LeafChord,
/// no scoremodel embedding is associated to a SingleNote.
/// A SingleNote is not located in a staff.
class SingleNote
{
public:

    /// single note elements
    /// @param accident displayed accident or courtesy
    /// @param h shape of the note head.
    /// @param harm whether the note is an natural harmonic
    /// @todo harmonic redundant with note-head?
    SingleNote(float accident = Pitch::UNDEF_NOTE_ALTERATION,
               const Notehead& h = Notehead::Shape::Oval,
               bool harm = false);

    /// copy constructor
    SingleNote(const SingleNote& n);

    virtual ~SingleNote();
    
    bool equal(const SingleNote& n) const;

    // inline int singleStaff() const { return _single_staff; }
    inline const Notehead& head() const { return _head; }

    inline float accidental() const { return _accidental; }

    inline bool harmonic() const { return _harmonic; }

    virtual void print(std::ostream& o) const;

protected:

    // individual staff number
    // @todo is redundant with LeafEvent.staff
    //int _single_staff;
    
    /// shape of the note head.
    const Notehead _head;
    
    /// @see Pitch.alteration
    /// @todo use bool = show accidental, for cautionary accident
    float _accidental;

    /// note is a natural harmonic
    bool _harmonic;
 
};

inline bool operator==(const SingleNote& lhs, const SingleNote& rhs)
{
    return lhs.equal(rhs);
}

inline bool operator!=(const SingleNote& lhs, const SingleNote& rhs)
{
    return !operator==(lhs, rhs);
}

std::ostream& operator<<(std::ostream&, const SingleNote&);


} // namespace ScoreModel


/// @}
#endif /* SingleNote_hpp */

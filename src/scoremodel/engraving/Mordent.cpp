//
//  Mordent.cpp
//  qparse
//
//  Created by Florent Jacquemard on 18/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Mordent.hpp"

namespace ScoreModel {

Mordent::Mordent(const VPosition& vpos,
                 Leaf* link,
                 bool before,
                 bool up):
Ornament(before),
Local(vpos, link),
_up(up)
{ }


Mordent::Mordent(const Ornament& o,
                 const VPosition& vpos,
                 Leaf* link,
                 bool up):
Ornament(o),
Local(vpos, link),
_up(up)
{ }


void Mordent::accept(SMVisitor* v) const
{
    v->visitMordent(this);
}


void Mordent::print(std::ostream& o) const
{
    if (_up)
        o << "Mordent ";
    else
        o << "LMordent ";
}


std::ostream& operator<<(std::ostream& o, const Mordent& m)
{
    m.print(o);
    return o;
}


} // namespace ScoreModel

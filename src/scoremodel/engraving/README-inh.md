# `src/scoremodel/engraving`

An engraving model contains engraving details about the events contained in a score model. 
The engraving information is stored in one tree for every measure and every monophonic voice.


### content:

```mermaid
graph TD;
  Tuplet-->|vec| Node;
  Leaf-->|ptr| AtomicEvent;
  Decorated-->Ornament;
  Decorated-->LeafEvent;
  Ornament-->|vec| GraceNote
  
```


### inheritance:

```mermaid
graph TD;
  Node-->Leaf;
  Leaf-->LeafEvent;
  Leaf-->LeafCont;
  LeafCont-->Tie;
  LeafCont-->Dot;
  Node-->Tuplet;
  Node-->Decorated;
  Node-->Ornament;
  Ornament-->Appogiatura;
  Ornament-->Mordent;
  Ornament-->Gruppetto;
```

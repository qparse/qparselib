//
//  Local.hpp
//  squanty
//
//  Created by Florent Jacquemard on 24/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#ifndef Local_hpp
#define Local_hpp

#include <stdio.h>

#include "HPosition.hpp"
#include "VPosition.hpp"
#include "Leaf.hpp"


namespace ScoreModel {


/// Annotation giving directive on performances to be written above or
/// below a staff, with horizontal location defined by a musical date value.
/// called `Notation` in [MusicXML](https://usermanuals.musicxml.com/MusicXML/Content/EL-MusicXML-notations.htm)
class Local
{
public:

    /// annotation localized
    /// @param vpos vertical position of the annotation.
    /// @param hpos horizontal position of the annotation.
    Local(const VPosition& vpos, const HPosition&  hpos);
    
    /// annotation localized at the given event.
    /// @param vpos vertical position of the annotation.
    /// @param hpos event defining the horizontal position of the annotation.
    Local(const VPosition& vpos, Leaf* hpos);

    /// annotation localized at the given date
    /// @param vpos vertical position of the annotation.
    /// @param hpos musical time defining the horizontal position
    /// of the annotation.
    /// @param p the part the annotation belongs to.
    Local(const VPosition& vpos, mtu_t hpos, const Part& p);

    virtual ~Local();
    
    inline const HPosition& hposition() const { return _hpos; }
    inline const VPosition& vposition() const { return _vpos; }

    // event based
    virtual bool isArticulation() const { return false; }
    virtual bool isMordent() const { return false; }
    virtual bool UpMordent(const Pitch&) const { return false; }
    virtual bool LowMordent(const Pitch&) const { return false; }
    virtual bool isGruppetto(const Pitch&) const { return false; }
    virtual bool isInvGruppetto(const Pitch&) const { return false; }
    virtual bool isTrill() const { return false; }
    virtual bool isPrall() const { return false; }
    // floating
    virtual bool isDynamic() const { return false; }
    virtual bool isJump() const { return false; }
    virtual bool isFermata() const { return false; }
    virtual bool isBreath() const { return false; }
    virtual bool isClefChange() const { return false; }
    // spanning
    virtual bool isSlur() const { return false; }
    virtual bool isVolta() const { return false; }
    virtual bool isOctava() const { return false; }
    virtual bool isDynChange() const { return false; }
    virtual bool isPedal() const { return false; }
    virtual bool isOsc() const { return false; }

    virtual void print(std::ostream& o) const { }

private:

    HPosition _hpos;
    
    VPosition _vpos;
   
};


std::ostream& operator<<(std::ostream&, const Local&);



} // namespace ScoreModel

#endif /* Local_hpp */

/// @} // group scoremodel


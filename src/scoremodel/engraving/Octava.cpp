//
//  Octava.cpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Octava.hpp"


namespace ScoreModel {


Octava::Octava(const VPosition& vpos,
               const HPosition& hp1, const HPosition& hp2,
               int nb):
Spanning(vpos, hp1, hp2),
_nb(nb)
{
    assert(nb != 0);
}


void Octava::print(std::ostream& o) const
{
    o << "Octava: ";
    o << (_nb * 7 + 1);
}


} // namespace ScoreModel

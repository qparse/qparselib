//
//  Continuation.cpp
//  squanty
//
//  Created by Florent Jacquemard on 20/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Continuation.hpp"

namespace ScoreModel {


Continuation::Continuation(Root& wrapper,
                           AtomicEvent* e,
                           Leaf* previous, Leaf* next):
Leaf(wrapper, e, previous, next)
{
    assert(e);
    assert(e->date() <= start());
    assert(previous);
}


Continuation::Continuation(size_t mult,
                           Inner* parent,
                           AtomicEvent* e,
                           Leaf* previous, Leaf* next):
Leaf(mult, parent, e, previous, next)
{
    assert(e);
    assert(e->date() <= start());
    assert(previous);
}


/// @todo TBR deprecated
Continuation::Continuation(mtu_t start, mtu_t dur,
                           Sequence& container,
                           AtomicEvent* e,
                           Leaf* previous, Leaf* next):
Leaf(start, dur, container, e, previous, next)
{
    assert(e);
    assert(e->date() <= this->start());
    assert(previous);
}


Continuation::Continuation(const Continuation& c):
Leaf(c)
{ }


void Continuation::print(std::ostream& o) const
{ } // skip (only for descendant classes)



std::ostream& operator<<(std::ostream& o, const Continuation& e)
{
    e.print(o);
    return o;
}


} // namespace ScoreModel

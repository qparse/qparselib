//
//  Tied.cpp
//  squanty
//
//  Created by Florent Jacquemard on 20/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Tied.hpp"

namespace ScoreModel {

Tied::Tied(Root& wrapper,
           int staff,
           AtomicEvent* e,
           Leaf* previous, Leaf* next):
Continuation(wrapper, e, previous, next)
{
    assert(staff >= 0);
    assert(_staves.empty());
    _staves.add(staff);
}


Tied::Tied(size_t mult,
           Inner* parent,
           int staff,
           AtomicEvent* e,
           Leaf* previous, Leaf* next):
Continuation(mult, parent, e, previous, next)
{
    assert(staff >= 0);
    assert(_staves.empty());
    _staves.add(staff);
}


/// @todo TBR deprecated
Tied::Tied(mtu_t start, mtu_t dur,
           Sequence& container,
           int staff,
           AtomicEvent* e,
           Leaf* previous, Leaf* next):
Continuation(start, dur, container, e, previous, next)
{
    assert(staff >= 0);
    assert(_staves.empty());
    _staves.add(staff);
}


Tied::Tied(const Continuation& t):
Continuation(t)
{ }


void Tied::accept(SMVisitor* v) const
{
    v->visitTied(this);
}


void Tied::print(std::ostream& o) const
{
    //assert(_embedding);
    //_embedding->print(o);
    o << " tie ";
//    if (_staccato)
//        o << "(stacc.)";
}


std::ostream& operator<<(std::ostream& o, const Tied& e)
{
    e.print(o);
    return o;
}


} // namespace ScoreModel

//
//  SingleGraceNote.cpp
//  squant2
//
//  Created by Florent Jacquemard on 29/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "SingleGraceNote.hpp"

namespace ScoreModel {


SingleGraceNote::SingleGraceNote(const GraceNote* n,
                                 int staff,
                                 float accident,
                                 const Notehead& h,
                                 bool harm):
SingleNote(accident, h, harm),
Staffed(staff),
_embedding(n)
{
    assert(staff >= 0);
    assert(n);
}


SingleGraceNote::SingleGraceNote(const SingleGraceNote& n):
SingleNote(n),
Staffed(n),
_embedding(n._embedding)
{ }


SingleGraceNote::~SingleGraceNote()
{
    // GraceNotes are deleted with Score/Part/Voice
}



const Pitch& SingleGraceNote::pitch() const
{
    assert(_embedding);
    return _embedding->pitch();
}


void SingleGraceNote::print(std::ostream& o) const
{
    pitch().print(o);
    ((SingleNote*) this)->print(o);
}


void SingleGraceNote::accept(SMVisitor* v) const
{
    v->visitSingleGraceNote(this);
}


std::ostream& operator<<(std::ostream& o, const SingleGraceNote& n)
{
    n.print(o);
    return o;
}


} // namespace ScoreModel

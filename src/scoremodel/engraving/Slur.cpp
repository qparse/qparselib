//
//  Slur.cpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Slur.hpp"


namespace ScoreModel {

Slur::Slur(const VPosition& vpos,
           const HPosition& hp1, const HPosition& hp2,
           Line::Type type):
Spanning(vpos, hp1, hp2),
_linetype(type)
{ }


void Slur::print(std::ostream& o) const
{
    o << "Slur " << "(" << _linetype << ")";
}


} // namespace ScoreModel

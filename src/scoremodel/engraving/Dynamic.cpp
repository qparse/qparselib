//
//  Dynamic.cpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Dynamic.hpp"


namespace ScoreModel {


Dynamic::Dynamic(const VPosition& vpos, Leaf* link, Kind k):
Local(vpos, link),
_marker(k)
{ }


Dynamic::Dynamic(const VPosition& vpos,
                 mtu_t date, const Part& p,
                 Kind k):
Local(vpos, date, p),
_marker(k)
{ }


void Dynamic::print(std::ostream& o) const
{
    o << "Dynamic: ";
    switch (_marker)
    {
        case p:
            o << "p";
            break;
        case pp:
            o << "pp";
            break;
        case ppp:
            o << "ppp";
            break;
        case pppp:
            o << "pppp";
            break;
        case ppppp:
            o << "ppppp";
            break;
        case pppppp:
            o << "pppppp";
            break;
        case f:
            o << "f";
            break;
        case ff:
            o << "ff";
            break;
        case fff:
            o << "fff";
            break;
        case ffff:
            o << "ffff";
            break;
        case fffff:
            o << "fffff";
            break;
        case ffffff:
            o << "ffffff";
            break;
        case mp:
            o << "mp";
            break;
        case mf:
            o << "mf";
            break;
        case pf:
            o << "pf";
            break;
        case sf:
            o << "sf";
            break;
        case sfpp:
            o << "sfpp";
            break;
        case fp:
            o << "fp";
            break;
        case rf:
            o << "rf";
            break;
        case sfz:
            o << "sfz";
            break;
        case sffz:
            o << "sffz";
            break;
        case sfzp:
            o << "sfzp";
            break;
        case fz:
            o << "fz";
            break;
        case n:
            o << "n";
            break;
        default:
            ERROR("Dynamic: unknown marker {}", _marker);
            break;
    }
}


std::ostream& operator<<(std::ostream& o, const Dynamic& g)
{
    g.print(o);
    return o;
}


} // namespace ScoreModel

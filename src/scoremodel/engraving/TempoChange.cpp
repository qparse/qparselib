//
//  TempoChange.cpp
//  squanty
//
//  Created by Florent Jacquemard on 01/09/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "TempoChange.hpp"


namespace ScoreModel {


TempoChange::TempoChange(const VPosition& vpos, Leaf* link, const Tempo& t):
Local(vpos, link),
_tempo(t)
{ }


TempoChange::TempoChange(const VPosition& vpos,
                       mtu_t date, const Part& p,
                       const Tempo& t):
Local(vpos, date, p),
_tempo(t)
{ }


/// @todo complete
void TempoChange::print(std::ostream& o) const
{
    o << "Tempo change: ";
    // o << _tempo;
}


std::ostream& operator<<(std::ostream& o, const TempoChange& cc)
{
    cc.print(o);
    return o;
}

} // namespace ScoreModel

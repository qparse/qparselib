//
//  Notehead.cpp
//  squanty
//
//  Created by Florent Jacquemard on 03/07/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Notehead.hpp"






namespace ScoreModel {


Notehead::Notehead(bool filled, Shape s, bool par, std::string text):
_shape(s),
_filled(filled),
_parenthesized(par),
_text(text)
{ }


Notehead::Notehead(const Notehead& h):
_shape(h._shape),
_filled(h._filled),
_parenthesized(h._parenthesized),
_text(h._text)
{ }


bool Notehead::equal(const Notehead& n) const
{
    return ((_shape == n._shape) &&
            (_filled == n._filled) &&
            (_parenthesized == n._parenthesized));
}


void Notehead::print(std::ostream& o) const
{
    if (_parenthesized)
        o << "(";

    switch (_shape)
    {
        case None:
            return;
        case Oval:
            o << "Oval"; break;
        case Slash:
            o << "slash"; break;
        case Triangle:
            o << "triangle"; break;
        case Diamond:
            o << "diamond"; break;
        case Square:
            o << "square"; break;
        case Cross:
            o << "cross"; break;
        case X:
            o << "x"; break;
        case Circle_x:
            o << "circle-x"; break;
        case Inverted_triangle:
            o << "inverted triangle"; break;
        case Arrow_down:
            o << "arrow down"; break;
        case Arrow_up:
            o << "arrow up"; break;
        case Slashed:
            o << "slashed"; break;
        case Back_slashed:
            o << "back slashed"; break;
        case Cluster:
            o << "cluster"; break;
        case Circle_dot:
            o << "circle dot"; break;
        case Left_triangle:
            o << "left triangle"; break;
        case Rectangle:
            o << "rectangle"; break;
        case Do:
            o << "do"; break;
        case Re:
            o << "re"; break;
        case Mi:
            o << "mi"; break;
        case Fa:
            o << "fa"; break;
        case Fa_up:
            o << "fa up"; break;
        case So:
            o << "so"; break;
        case La:
            o << "la"; break;
        case Ti:
            o << "ti"; break;
        default:
            ERROR("unexpected note head shape {}", _shape); break;
    }
    if (_filled)
        o << "-filled";
    if (_parenthesized)
        o << ")";
    
    if (! _text.empty())
        o << "text=" << _text;
}


std::ostream& operator<<(std::ostream& o, const Notehead& h)
{
    h.print(o);
    return o;
}

} // namespace ScoreModel




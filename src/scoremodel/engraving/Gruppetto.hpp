//
//  Gruppetto.hpp
//  qparse
//
//  Created by Florent Jacquemard on 18/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{


#ifndef SM_Gruppetto_hpp
#define SM_Gruppetto_hpp

#include <stdio.h>

#include "Ornament.hpp"
#include "Local.hpp"


namespace ScoreModel {


/// short figure made of:
/// - the note above the main note,
/// - the main note,
/// - the note below the main note,
/// - and the main note again.
/// If is inverted, the note below is played first.
class Gruppetto : public Ornament, Local
{
public:

    /// empty ornament (no links to score grace-notes)
    /// @param invert true if the this gruppetto is counterclockwise
    Gruppetto(const VPosition& vpos,
              Leaf* link,
              bool before=true,
              bool invert=false);

    /// new gruppetto copying the ornament content.
    /// @param invert true if the this gruppetto is counterclockwise
    Gruppetto(const Ornament& o,
              const VPosition& vpos,
              Leaf* link,
              bool invert=false);

    
    ~Gruppetto() {}

    /// gruppetto is clockwise or counter
    /// @return true if the this gruppetto is counterclockwise
    bool inverted() const { return _inverted; }
    
    virtual bool isGruppetto() const { return (! _inverted); }
    virtual bool isInvGruppetto() const { return _inverted; };

    virtual void accept(SMVisitor*) const;
    
    virtual void print(std::ostream& o) const;

protected:
    bool _inverted;
};


std::ostream& operator<<(std::ostream&, const Gruppetto&);


} // namespace ScoreModel

#endif /* SM_Gruppetto_hpp */

/// @}

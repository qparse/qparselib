// Implementation of Beam functions

#include "Beam.hpp"
#include <algorithm>    // std::min

namespace ScoreModel {

// Main constructor
// @todo STUB TBR
//Beam::Beam(Sequence events)
//{
    // Copy the references to the set of events
    //this->concatenate(events) ;
    // Inform the first and last event that the beam starts/ends
    //assert(events.nbEvents() == 2);
//    events.getFirstEvent()->setStartBeam(this);
//    events.getLastEvent()->setEndBeam(this);
//}


Beam::Beam(Info i):
_info(i)
{ }


Beam::Beam(const Beam& b):
_info(b._info)
{ }


unsigned long Beam::interEventBeams(unsigned long nf1,
                                    unsigned long nf2,
                                    Info pol,
                                    unsigned long depth)
{
    assert(0 <= nf1);
    assert(nf1 <= CMN::MAX_FLAGS);
    assert(0 <= nf2);
    assert(nf2 <= CMN::MAX_FLAGS);

    // switch case
    if (pol == None)
    {
        return 0; // no beam
    }
    else if (pol == Full)
    {
        return std::min(nf1, nf2); // all beams possible
    }
    else if (pol == Minus)
    {
        unsigned long m = std::min(nf1, nf2);
        // all beams possible - 1
        if (m == 0)
            return 0;
        else if (m == 1)
            return 1;
        else
            return (m - 1);
    }
    else if (pol == One)
    {
        return 1; // one beam
    }
    else if (pol == Depth)
    {
        assert(0 <= depth);
        assert(depth <= CMN::MAX_FLAGS);
        return depth;
    }
    else
    {
        Beam b = Beam(pol);
        ERROR("unexpected beaming info code {}", b);
        return 99;
    }
}


void Beam::print(std::ostream& o) const
{
    switch (_info)
    {
        case None:
            o << "NONE"; break;
        case Full:
            o << "FULL"; break;
        case Minus:
            o << "MINUS"; break;
        case One:
            o << "ONE"; break;
        case Depth:
            o << "DEPTH"; break;
        case Undef:
            o << "UNDEF"; break;
        default:
            o << "Unknown:" << _info; break;
            //ERROR("unexpected beam type {}", _info); break;
    }
}


std::ostream& operator<<(std::ostream& o, const Beam& b)
{
    b.print(o);
    return o;
}



} // namespace ScoreModel

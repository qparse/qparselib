//
//  Fermata.cpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Fermata.hpp"


namespace ScoreModel {


Fermata::Fermata(const VPosition& vpos, Leaf* link, Shape shape):
Local(vpos, link),
_shape(shape)
{ }


Fermata::Fermata(const VPosition& vpos,
                 mtu_t date, const Part& p,
                 Shape shape):
Local(vpos, date, p),
_shape(shape)
{ }


void Fermata::print(std::ostream& o) const
{
    o << "Fermata: ";
    switch (_shape)
    {
        case Normal:
            o << "Normal";
            break;
        case Angled:
            o << "Angled";
            break;
        case DoubleAngled:
            o << "DoubleAngled";
            break;
        case Square:
            o << "Square";
            break;
        case DoubleSquare:
            o << "DoubleSquare";
            break;
        case Henze:
            o << "Henze";
            break;
        default:
            ERROR("Fermata: unknown shape {}", _shape);
            break;
    }
}



std::ostream& operator<<(std::ostream& o, const Fermata& f)
{
    f.print(o);
    return o;
}


} // namespace ScoreModel

//
//  Local.cpp
//  squanty
//
//  Created by Florent Jacquemard on 24/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Local.hpp"


namespace ScoreModel {

Local::Local(const VPosition& vpos, const HPosition& hpos):
_hpos(hpos),
_vpos(vpos)
{
    if (hpos.linked())
    {
        assert(hpos.leaf());
        hpos.leaf()->addLink(this);
        // hpos.leaf()->sequence().addStaff(vpos.staff());
    }
    else
    {
        hpos.measure().addLink(this);
    }
    _hpos.measure().addStaff(vpos.staff());
}


Local::Local(const VPosition& vpos, Leaf* hpos):
_hpos(hpos),
_vpos(vpos)
{
    assert(hpos);
    hpos->addLink(this);
    // hpos->sequence().staves().add(_vpos.staff());
    _hpos.measure().addStaff(_vpos.staff());
}


Local::Local(const VPosition& vpos, mtu_t hpos, const Part& p):
_hpos(hpos, p),
_vpos(vpos)
{
    _hpos.measure().addLink(this);
    _hpos.measure().addStaff(_vpos.staff());
}


Local::~Local()
{ }


std::ostream& operator<<(std::ostream& o, const Local& p)
{
    p.print(o); // dispatch to descendant classes
    o << " at " << p.hposition() << "; " << p.vposition();
    return o;
}


} // namespace ScoreModel

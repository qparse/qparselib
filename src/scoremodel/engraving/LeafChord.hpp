//
//  LeafChord.hpp
//  squanty
//
//  Created by Florent Jacquemard on 02/07/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef LeafChord_hpp
#define LeafChord_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>
//#include <vector>
#include <set>
#include <initializer_list>

#include "trace.hpp"
#include "LeafEvent.hpp"
#include "LeafNote.hpp"
#include "PitchedNote.hpp"
#include "StaffRange.hpp"
#include "Arpeggio.hpp"
#include "Stem.hpp"


namespace ScoreModel {


struct PitchedNoteComp
{
  bool operator() (const PitchedNote& lhs, const PitchedNote& rhs) const
  { return lhs < rhs; }
};

typedef std::set<PitchedNote, PitchedNoteComp> PitchedNoteBag;


/// @todo complete
class LeafChord : public LeafEvent
{
public:

    /// single chord, this chord is the root, embedded in given wrapper.
    // @param staff the staff number of the fundamental of this chord.
    /// @param arp kind of arpegio mark (default None)
    LeafChord(Root& wrapper,
              const Arpeggio& arp = Arpeggio(),
              const Stem& s = Stem::Type::Unspecified,
              int tremolo = 0,
              Leaf* previous = NULL, Leaf* next = NULL,
              AtomicEvent* e = NULL);
    
    /// chord leaf with a parent.
    // @param staff the staff number  of the fundamental of this chord.
    /// @param arp kind of arpegio mark.
    LeafChord(size_t mult,
              Inner* parent,
              const Arpeggio& arp = Arpeggio(),
              const Stem& s = Stem::Type::Unspecified,
              int tremolo=0,
              Leaf* previous=NULL, Leaf* next=NULL,
              AtomicEvent* e=NULL);

    /// copy constructor
    LeafChord(const LeafChord& lc);
    
    virtual bool isChord() const { return true; }
    
    inline bool empty() const { return _content.empty(); }
    
    /// lowest note in this chord
    /// @warning this chord must not be empty.
    const PitchedNote& bass() const;

    /// highest note in this chord
    /// @warning this chord must not be empty.
    const PitchedNote& treble() const;
    
    /// create a new PitchedNote and add it to this Chord.
    void addNote(const Pitch& p,
                 int staff,
                 float accident = Pitch::UNDEF_NOTE_ALTERATION,
                 const Notehead& h = Notehead::Shape::Oval,
                 bool harm = false);
    
    virtual void accept(SMVisitor*) const;

    virtual void print(std::ostream& o) const;
    
private:
    
    /// stem direction is global, for the whole chord.
    const Stem _stem;

    /// the tremolo value is global, for the whole chord.
    /// It is an integer from 0 to 8 indicating the number
    /// of tremolo marks over the stem (~ beams) for single note tremolo.
    int _tremolo;

    /// kind of arpegio mark (default is None)
    const Arpeggio _arp;
    
    ///   the elements
    ///   - accident
    ///   - note head
    ///   - harmonic flag
    ///   - individual staff number
    ///   are for each single note of chord
    // std::vector<PitchedNote> _content;
    PitchedNoteBag _content;
};

std::ostream& operator<<(std::ostream&, const LeafChord&);

} // namespace ScoreModel

/// @}

#endif /* LeafChord_hpp */


//
//  Jump.cpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Jump.hpp"


namespace ScoreModel {


Jump::Jump(const VPosition& vpos, Leaf* link, Kind k):
Local(vpos, link),
_marker(k)
{ }


Jump::Jump(const VPosition& vpos, mtu_t date, const Part& p, Kind k):
Local(vpos, date, p),
_marker(k)
{ }


void Jump::print(std::ostream& o) const
{
    o << "Jump: ";
    switch (_marker)
    {
        case None:
            o << "None";
            break;
        case Segno:
            o << "Segno";
            break;
        case Coda:
            o << "Coda";
            break;
        case CodaSquare:
            o << "CodaSq";
            break;
        case DaCapo:
            o << "DC";
            break;
        case DaCapoAlFine:
            o << "DCaF";
            break;
        case DaCapoAlCoda:
            o << "DCaC";
            break;
        case DaCapoAlSegno:
            o << "DCaS";
            break;
        case DalSegno:
            o << "DS";
            break;
        case DalSegnoAlCoda:
            o << "DSaC";
            break;
        case DalSegnoAlFine:
            o << "DSaF";
            break;
        case Fine:
            o << "Fine";
            break;
        default:
            ERROR("Jump: unknown marker {}", _marker);
            break;
    }
}


std::ostream& operator<<(std::ostream& o, const Jump& j)
{
    j.print(o);
    return o;
}

} // namespace ScoreModel

//
//  Decorated.cpp
//  squanty
//
//  Created by Florent Jacquemard on 20/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Decorated.hpp"

namespace ScoreModel {

//Inner(2, wrapper, init_added(o != NULL, e != NULL)),
Decorated::Decorated(Root& wrapper, Ornament* const o, LeafEvent* const e):
Inner(2, wrapper),
_ornament(o),
_leaf(e),
_ordering(init_ordering(o, e))
{
    assert(e == NULL || o != NULL);
    assert(e == NULL || e->isLeaf());
    assert(e == NULL || e->event());
    assert(e == NULL || e->multiplicity() == 1);
    assert(_staves.empty());
    updateStaves();
}


Decorated::Decorated(Root& wrapper, LeafEvent* const e, Ornament* const o):
Inner(2, wrapper),
_ornament(o),
_leaf(e),
_ordering(1) // Leaf event first
{
    assert(e != NULL);
    assert(e->isLeaf());
    assert(e->event());
    assert(e->multiplicity() == 1);
    assert(_staves.empty());
    updateStaves();
}


//Inner(2, mult, parent, init_added(o != NULL, e != NULL)),
Decorated::Decorated(size_t mult,
                     Inner* parent,
                     Ornament* const o, LeafEvent* const e):
Inner(2, mult, parent),
_ornament(o),
_leaf(e),
_ordering(init_ordering(o, e))
{
    assert(e == NULL || o != NULL);
    assert(e == NULL || e->isLeaf());
    assert(e == NULL || e->event());
    assert(e == NULL || e->multiplicity() == 1);
    assert(mult > 0);
    assert(parent);
    assert(_staves.empty());
    updateStaves();
}


Decorated::Decorated(size_t mult,
                     Inner* parent,
                     LeafEvent* const e, Ornament* const o):
Inner(2, mult, parent),
_ornament(o),
_leaf(e),
_ordering(1) // Leaf event first
{
    assert(e != NULL);
    assert(e->event());
    assert(mult > 0);
    assert(parent);
    assert(_staves.empty());
    updateStaves();
}


Decorated::~Decorated()
{
    TRACE("delete Decorated with{} ornament, with{} leaf",
          (_ornament == NULL)?"out":"", (_leaf == NULL)?"out":"");
}


void Decorated::delete_tree()
{
    TRACE("delete recursively the content of Decorated  node: {} ornament, {} leaf",
          (_ornament == NULL)?"no":"one", (_leaf == NULL)?"no":"one");

    if (_ornament)
        delete _ornament;

    if (_leaf)
    {
        _leaf->delete_tree();
        delete _leaf;
    }
}


size_t Decorated::init_added(bool ornament, bool event)
{
    if (ornament == true && event == true)
        return 2;
    else if (ornament == true && event == false)
        return 1;
    else
    {
        assert(event == false);
        assert(ornament == false);
        return 0;
    }
}


int Decorated::init_ordering(Ornament* const o, Leaf* const e)
{
    if (o == NULL)
    {
        assert(e == NULL);
        return 2; // unknown
    }
   else
        return 0; // ornament first
}


//bool Decorated::complete() const
//{
//    return ((_ornament != NULL) && (_leaf != NULL));
//}


void Decorated::add(Ornament* o)
{
    assert(o);
    if (_ornament != NULL)
    {
        WARN("Decorated: replace Ornament");
        delete _ornament;
    }
    if (_leaf == NULL)
        _ordering = 0; // ornament first, event second
    _ornament = o;
    _staves.merge(o->staves());
    assert((_ordering == 0) || (_ordering == 1)); // ordering is known
}
 
bool Decorated::add(Node* t)
{
    assert(t);
    assert(t->isLeafEvent());
    LeafNote* n = dynamic_cast<LeafNote*>(t);
    if (n != NULL)
    {
        addNote(n);
        return true;
    }
    
    LeafChord* c = dynamic_cast<LeafChord*>(t);
    if (c != NULL)
    {
        addChord(c);
        return true;
    }

    ERROR("Decorated.add: the node must be a LeafNote or a LeafChord");
    return false;
}


void Decorated::addNote(LeafNote* n)
{
    assert(n);
    assert(n->staff() != UNDEF_STAFF);
    assert(_leaf == NULL);
    _staves.add(n->staff());
    _leaf = (LeafEvent*) n;
    if (_ornament == NULL)
        _ordering = 1; // event first, ornament second
    assert((_ordering == 0) || (_ordering == 1)); // ordering is known
}


void Decorated::addChord(LeafChord* c)
{
    assert(c);
    assert(_leaf == NULL);
    _staves.merge(c->staves());
     _leaf = (LeafEvent*) c;
    if (_ornament == NULL)
        _ordering = 1; // event first, ornament second
    assert((_ordering == 0) || (_ordering == 1)); // ordering is known
}


bool Decorated::replace(size_t i, Node* n)
{
    if (i != 1)
    {
        ERROR("replacement in Decorated can be applied only to leaf node.");
        return false;
    }
    add(n);
    return true;
}


size_t Decorated::size() const
{
    int ornament = (_ornament == NULL)?0:1;
    int leaf = (_leaf == NULL)?0:1;
    return (ornament+leaf);
}


size_t Decorated::completed() const
{
    if ((_ornament != NULL) && (_leaf != NULL))
        return _leaf->multiplicity(); // always 1
    else
        return 0;
}


bool Decorated::complete() const
{
    return ((_ornament != NULL) && (_leaf != NULL));
}


Node* Decorated::sub(size_t i) const
{
    assert(i < size());
    if (i != 0)
    {
        ERROR("Decorated.sub: cannot access sub {} of a Decorated", i);
        return NULL;
    }
    else
    {
        assert(_leaf != NULL);
        return _leaf;
    }
}


// the only real child (i.e. Node) is the Leaf event _leaf.
// start date of the ornament and Leaf event coincide
mtu_t Decorated::start_child() const
{
    assert(! complete());
    if (_leaf == NULL)
        return this->date();
    else
    {
        ERROR("Decorated : start_child called for adding Ornament");
        return (this->date() + this->duration());
    }
}


// the only real child (i.e. Node) is the Leaf event _leaf.
// Its duration is the duration of this Decorated.
mtu_t Decorated::dur_child() const
{
    assert(! complete());
    if (_leaf == NULL)
        return this->duration();
    else
    {
        ERROR("Decorated : dur_child called for adding Ornament");
        return MTU::ZERO;
    }
}


bool Decorated::ornamentFirst() const
{
    assert((_ordering == 0) || (_ordering == 1));
    if (_ordering == 0)
        return true;
    else if (_ordering == 1)
        return false;
    else
    {
        ERROR("Decorated:ornamentFirst : children must be set before calling");
        return true;
    }
}


void Decorated::updateStaves()
{
    if (_ornament != NULL)
    {
        _staves.merge(_ornament->staves());
    }

    if (_leaf != NULL)
    {
        LeafNote* n = dynamic_cast<LeafNote*>(_leaf);
        if (n != NULL)
        {
            _staves.add(n->staff());
            return;
        }
        LeafChord* c = dynamic_cast<LeafChord*>(_leaf);
        if (c != NULL)
        {
            _staves.merge(c->staves());
            return;
        }
        ERROR("Decorated: the leaf event must be a Note or a Chord");
    }
}


void Decorated::accept(SMVisitor* v) const
{
    v->visitDecorated(this);
}


void Decorated::print(std::ostream& o) const
{
    assert(_ornament);
    _ornament->print(o);
    assert(_leaf);
    o << " ";
    _leaf->print(o);
}


std::ostream& operator<<(std::ostream& o, const Decorated& d)
{
    d.print(o);
    return o;
}


} // namespace ScoreModel

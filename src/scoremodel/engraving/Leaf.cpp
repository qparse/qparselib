//
//  Leaf.cpp
//  squanty
//
//  Created by Florent Jacquemard on 20/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Leaf.hpp"


namespace ScoreModel {


Leaf::Leaf(Root& wrapper,
           // int staff,
           AtomicEvent* e,
           Leaf* previous, Leaf* next):
Node(0, wrapper),
_embedding(e),
//_staff(staff),
_previous(previous),
_next(next),
_backlinks()
{
    assert((previous == NULL) || previous->isLeaf());
    assert((next == NULL) || next->isLeaf());
//    assert(_staff >= 0);
//    measure().addStaff(_staff);
}


Leaf::Leaf(size_t mult,
           Inner* parent,
           //int staff,
           AtomicEvent* e,
           Leaf* previous, Leaf* next):
Node(0, mult, parent),
_embedding(e),
//_staff(staff),
_previous(previous),
_next(next),
_backlinks()
{
    assert((previous == NULL) || previous->isLeaf());
    assert((next == NULL) || next->isLeaf());
}


/// @todo TBR deprecated
Leaf::Leaf(mtu_t start, mtu_t dur,
           Sequence& container,
           //int staff,
           AtomicEvent* e,
           Leaf* previous, Leaf* next):
Node(start, dur, 0, container),
_embedding(e),
//_staff(staff),
_previous(previous),
_next(next),
_backlinks()
{
    ERROR("Leaf constructor should not be used");
}


Leaf::Leaf(const Leaf& l):
Node(l),
_embedding(l._embedding),
_previous(l._previous),
_next(l._next),
_backlinks(l._backlinks) // copy
{ }


Leaf::~Leaf()
{
    TRACE("delete Leaf");
}


void Leaf::delete_tree()
{
    WARN("no subtree to delete for a Leaf");
}


bool Leaf::add(Node* t)
{
    ERROR("add should not be called on a Leaf node");
    return false;
}


Node* Leaf::sub(size_t i) const
{
    ERROR("sub({}) should not be called on a Leaf node", i);
    // throw tree_error;
    return NULL;
}


//bool Leaf::replace(size_t i, Node* n)
//{
//    ERROR("replace({}) should not be called on a Leaf node", i);
//    return false;
//}


/// @todo TBC
Voice& Leaf::voice() const
{
    assert(_embedding);
    return _embedding->voice();
}


void Leaf::setNext(Leaf* n)
{
    assert(n != NULL);
    assert(_next == NULL);
    _next = n;
}


void Leaf::setPrevious(Leaf* p)
{
    assert(p != NULL);
    assert(_previous == NULL);
    _previous = p;
}


Backlinks::iterator Leaf::beginBacklinks()
{
    return _backlinks.begin();
}


Backlinks::iterator Leaf::endBacklinks()
{
    return _backlinks.end();
}


Backlinks::const_iterator Leaf::cbeginBacklinks() const
{
    return _backlinks.cbegin();
}


Backlinks::const_iterator Leaf::cendBacklinks() const
{ return _backlinks.cend(); }


const Local* Leaf::backlink(size_t i) const
{
    assert(i < _backlinks.size());
    return _backlinks[i];
}


void Leaf::addLink(const Local* d)
{
    assert(d);
    _backlinks.push_back(d);
}


void Leaf::print(std::ostream& o) const
{
    assert(_embedding);
    _embedding->print(o);
}


std::ostream& operator<<(std::ostream& o, const Leaf& e)
{
    e.print(o);
    return o;
}


} // namespace ScoreModel

//
//  Staffed.cpp
//  squant2
//
//  Created by Florent Jacquemard on 28/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Staffed.hpp"



namespace ScoreModel {


Staffed::Staffed(int nb):
_staff(nb)
{
    assert((_staff == UNDEF_STAFF) || (_staff >= 0));
}

Staffed::Staffed(const Staffed& s):
_staff(s._staff)
{ }


} // namespace ScoreModel


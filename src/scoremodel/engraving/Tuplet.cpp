// Implementation of Tuplet functions

#include "Tuplet.hpp"
#include "Score.hpp"

namespace ScoreModel {


Tuplet::Tuplet(size_t ar, Root& wrapper,
               const Beam::Info beam, const TupletLabel& tlab):
Inner(ar, wrapper),
_children(), // initially empty
_beaming(beam),
_label(tlab), /// @todo complete default
_completed(0)
{
    assert(ar > 1);
}


Tuplet::Tuplet(size_t ar, size_t mult, Inner* parent,
               const Beam::Info beam, const TupletLabel& tlab):
Inner(ar, mult, parent),
_children(), // initially empty
_beaming(beam),
_label(tlab), /// @todo complete default
_completed(0)
{
    assert(parent);
    assert(parent->isInner());
    assert(ar > 1);
}


;/// @todo TRB deprecated
Tuplet::Tuplet(mtu_t start, mtu_t dur, size_t ar, Sequence& container):
Inner(start, dur, ar, container),
_children(), // initially empty
_beaming(Beam::None),
_label(),
_completed(0)
{
    assert(ar > 1);
}


Tuplet::~Tuplet()
{
    TRACE("delete Tuplet node measure {} voice {} part:{}",
          _container.measureNb(),
          _container.voiceNb(),
          _container.measure().part().id());
}


void Tuplet::delete_tree()
{
    TRACE("delete the content of Tuplet node measure {} voice {} part:{}",
          _container.measureNb(),
          _container.voiceNb(),
          _container.measure().part().id());
    for (Node* t : _children)
    {
        assert(t);
        t->delete_tree();
        delete(t);
    }
}


bool Tuplet::add(Node* t)
{
    assert(t);
    assert(t->multiplicity() > 0);
    assert(completed() + t->multiplicity() <= arity());
    _children.push_back(t);
    _completed += t->multiplicity();
    _staves.merge(t->staves());  // _container.addStaff(staff);
    return true;
}


bool Tuplet::replace(size_t i, Node* n)
{
    assert(n);
    if (i >= size()) return false;
    assert(_children[i]);
    assert(_children[i]->multiplicity() <= _completed);
    _completed -= _children[i]->multiplicity();
    _children[i] = n;
    _completed += n->multiplicity();
    return true;
}


//std::vector<Event*> Tuplet::getEvents() const {
//	return _events.getEvents();
//}

size_t Tuplet::size() const
{
    return _children.size();
}


size_t Tuplet::completed() const
{
    return _completed;
//    size_t res = 0;
//    for (Node* c : _children)
//    {
//        assert(c);
//        //assert(c->complete());
//        res += c->multiplicity();
//    }
//
//    return res;
}


Node* Tuplet::sub(size_t i) const
{
    assert(i < size());
    return _children.at(i);
}


//size_t Tuplet::getArity() const
//{
//return _arity;
//}

mtu_t Tuplet::getBaseDuration() const
{
	mtu_t dur = MTU::of_int(1);

	for (Node* event : _children)
    {
		if (dur > event->duration())
        {
			dur = event->duration();
		}
	}
	return dur;
}


long Tuplet::getNumBase(MeterSig& ts) const
{
    //const MeterSig& ts = this->score().getTS();
    return ts.MTUtoCMN(getBaseDuration()) / ts.MTUtoCMN(this->duration());
}


/**
 * First event
 */
//Event* Tuplet::getFirstEvent() const
//{
//	return _events.getFirstEvent();
//}


/**
 * Last event
 */
//Event* Tuplet::getLastEvent() const
//{
//	return _events.getLastEvent();
//}


mtu_t Tuplet::start_child() const
{
    assert(! complete());
    return (this->date() + completed() * dur_child());
}


mtu_t Tuplet::dur_child() const
{
    assert(! complete());
    assert(arity() > 0);
    return duration() / arity();
}


void Tuplet::accept(SMVisitor* v) const
{
    v->visitTuplet(this);
}


/// @todo STUB
std::string Tuplet::to_string() const
{
//    label_t a = label();
//    std::string res = "";
//
//    if (SymbLabel::inside(a))
//    {
//        size_t ar = this->arity();
//        res = LabeledRhythmTree::label_to_string(a);
//        if (ar > 1) // inner
//        {
//
//            res += "(";
//            for (size_t i = 0; i < ar; i++)
//            {
//                if (_children[i] == NULL)
//                    res += "NULL";
//                else
//                    res += _children[i]->to_string();
//
//                if (i < ar-1)
//                    res += ", ";
//            }
//            res += ")";
//        }
//    }
//    else if (SymbLabel::bar(a))
//    {
//        res += std::string("0");  // bar_to_string(0);
//    }

    std::ostringstream os;
    this->print(os); // os << *(this);
    return os.str();
}


void Tuplet::print(std::ostream& o) const
{
    o << "Tuplet-" << this->arity();
}


std::ostream& operator<<(std::ostream& o, const Tuplet& d)
{
    d.print(o);
    return o;
}


} // namespace ScoreModel

//
//  LeafHarmonic.cpp
//  squanty
//
//  Created by Florent Jacquemard on 03/07/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "LeafHarmonic.hpp"

namespace ScoreModel {

LeafHarmonic::LeafHarmonic(Root& wrapper,
                           int staff,
                           const Pitch& pitch_touching,
                           float accident_touching,
                           float accident_sounding,
                           const Stem& stem,
                           int tremolo,
                           Leaf* previous, Leaf* next,
                           AtomicEvent* e):
LeafNote(wrapper, staff,  accident_sounding, Notehead(), stem),
pitchTouching(pitch_touching),
accidentTouching(accident_touching)
{ }


LeafHarmonic::LeafHarmonic(size_t mult,
                           Inner* parent,
                           int staff,
                           const Pitch& pitch_touching,
                           float accident_touching,
                           float accident_sounding,
                           const Stem& stem,
                           int tremolo,
                           Leaf* previous, Leaf* next,
                           AtomicEvent* e):
LeafNote(mult, parent, staff, accident_sounding, Notehead(), stem),
pitchTouching(pitch_touching),
accidentTouching(accident_touching)
{ }


void LeafHarmonic::accept(SMVisitor* v) const
{
    v->visitLeafHarmonic(this);
}


/// @todo STUB complete
void LeafHarmonic::print(std::ostream& o) const
{
    o << "Harmonic";
}

std::ostream& operator<<(std::ostream& o, const LeafHarmonic& e)
{
    e.print(o);
    return o;
}


} // namespace ScoreModel




//
//  LeafSpace.hpp
//  squant2
//
//  Created by Florent Jacquemard on 21/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef LeafSpace_hpp
#define LeafSpace_hpp
/// @addtogroup scoremodel
/// @{


#include <stdio.h>

#include "LeafEvent.hpp"


namespace ScoreModel {

/// an whitespace in a sequence, with a duration.
/// can be used to fill gaps in incomplete sequences,
/// in order to meaningfully align multiple voices in a multi-voice texture.
/// also useful in case of parse error.
/// @see https://music-encoding.org/guidelines/v4/elements/space.html
/// The associated musical event is a rest
class LeafSpace : public LeafEvent 
{
public:

    /// single whitespace tree: this space is the root, embedded in given wrapper.
    /// the associated atomic event must be a Rest.
    LeafSpace(Root& wrapper,
              int staff,
              Leaf* previous=NULL, Leaf* next=NULL,
              AtomicEvent* e=NULL);
    
    /// spzce leaf with a parent.
    /// the associated atomic event must be a Rest.
    LeafSpace(size_t mult,
              Inner* parent,
              int staff,
              Leaf* previous=NULL, Leaf* next=NULL,
              AtomicEvent* e=NULL);

    virtual bool isSpace() const { return true; }
    
    virtual void accept(SMVisitor*) const;

    virtual void print(std::ostream& o) const;

};

std::ostream& operator<<(std::ostream&, const LeafRest&);


} // namespace ScoreModel

/// @}


#endif /* LeafSpace_hpp */

//
//  Tied.hpp
//  squanty
//
//  Created by Florent Jacquemard on 20/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef SM_Tied_hpp
#define SM_Tied_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>

#include "Continuation.hpp"

namespace ScoreModel {

/// leaf node in a tree representation of engraving information,
/// containing an event tied to its previous event.
class Tied : public Continuation 
{
public:
    
    /// root tie: the whole measure is a continuation of the previous one.
    /// @see Continuation
    Tied(Root& wrapper,
         int staff,
         AtomicEvent* e,
         Leaf* previous, Leaf* next=NULL);

    /// non-root tie.
    /// @see Continuation
    Tied(size_t mult,
         Inner* parent,
         int staff,
         AtomicEvent* e,
         Leaf* previous, Leaf* next=NULL);

    /// root tie without wrapper.
    /// @see Continuation
    /// @todo TBR deprecated
    Tied(mtu_t start, mtu_t dur,
         Sequence& container,
         int staff,
         AtomicEvent* e,
         Leaf* previous, Leaf* next=NULL);

    /// copy
    Tied(const Continuation& t);
    
    virtual ~Tied() {}
    
    virtual bool isTie() const { return true; }

    virtual void accept(SMVisitor*) const;

    virtual void print(std::ostream& o) const;

};


std::ostream& operator<<(std::ostream&, const Tied&);
    

} // namespace ScoreModel

/// @}
#endif /* SM_Tied_hpp */

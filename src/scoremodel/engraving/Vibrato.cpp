//
//  Vibrato.cpp
//  squanty
//
//  Created by Florent Jacquemard on 02/09/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Vibrato.hpp"


namespace ScoreModel {


Vibrato::Vibrato(const VPosition& vpos,
                             const HPosition& hp1, const HPosition& hp2,
                             Kind k):
Spanning(vpos, hp1, hp2),
_kind(k)
{ }


void Vibrato::print(std::ostream& o) const
{
    o << "Vibrato: ";
    o << _kind;
}


} // namespace ScoreModel

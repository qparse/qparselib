//
//  Staffed.hpp
//  squant2
//
//  Created by Florent Jacquemard on 28/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef Staffed_hpp
#define Staffed_hpp
/// @addtogroup scoremodel
/// @{


#include <stdio.h>

#include "trace.hpp"

namespace ScoreModel {


const int UNDEF_STAFF = -1;


/// @brief an object that must be positionned in a staff.
class Staffed
{
public:
    
    Staffed(int nb = UNDEF_STAFF);

    Staffed(const Staffed& s);
    
    ~Staffed() { }

    /// @return number of staff where the object is located
    inline int staff() const { return _staff; }

    /// @param n number of staff where the object is located
    inline void setStaff(int n) { assert(n >= 0); _staff = n; }

private:
    
    int _staff;

};


} // namespace ScoreModel

/// @}

#endif /* Staffed_hpp */

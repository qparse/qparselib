//
//  TempoChange.hpp
//  squanty
//
//  Created by Florent Jacquemard on 01/09/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#ifndef TempoChange_hpp
#define TempoChange_hpp

#include <stdio.h>

//#include "Tempo.hpp"
#include "Local.hpp"


namespace ScoreModel {


/// @todo STUB define class in musical model
//class Tempo;
typedef int Tempo;


/// @brief Tempo Indication
/// @todo complete
class TempoChange : public Local
{
public:

    /// change before an event.
    TempoChange(const VPosition& vpos, Leaf* link, const Tempo& t);

    /// change at a date.
    TempoChange(const VPosition& vpos,
                mtu_t date, const Part& p,
                const Tempo& t);

    Tempo tempo() const { return _tempo; }
   
    virtual bool isTempoChange() const { return true; }

    virtual void print(std::ostream& o) const;
 
private:
    
    Tempo _tempo;

};

std::ostream& operator<<(std::ostream&, const TempoChange&);

} // namespace ScoreModel

#endif /* TempoChange_hpp */

/// @} // end group scoremodel



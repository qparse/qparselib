//
//  HPosition.cpp
//  squanty
//
//  Created by Florent Jacquemard on 24/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "HPosition.hpp"
#include "Score.hpp"
#include "Part.hpp"
#include "Measure.hpp"
#include "Leaf.hpp"


namespace ScoreModel {


HPosition::HPosition(Leaf* n):
_link(n),
_time(MTU::UNDEF),  // unused
_part(NULL)         // unused
{
    assert(n);
}


HPosition::HPosition(mtu_t t, const Part& p):
_link(NULL),
_time(t),
_part(&p)
{
    assert(MTU::defined(t));
    assert(MTU::finite(t));
}


HPosition::HPosition(const HPosition& p):
_link(p._link),
_time(p._time),
_part(p._part)
{ }


mtu_t HPosition::date() const
{
    if (this->linked())
    {
        assert(_link);
        return _link->date();
    }
    else
        return _time;
}


//Part& HPosition::part() const
//{
//    if (this->linked())
//    {
//        assert(_link);
//        return _link->sequence().parent();
//    }
//    else
//    {
//        return _time;
//    }
//}

Measure& HPosition::measure() const
{
    if (this->linked())
    {
        assert(_link);
        return _link->sequence().measure();
    }
    else
    {
        assert(_part);
        const Part& part = *_part;
        Score& score = part.score();
        assert(MTU::defined(_time));
        assert(MTU::finite(_time));
        long n = MTU::measure(_time);
        assert(n < score.nbMeasures());
        MeasureStack& stack = score.stack(n);
        Measure& m = stack.get(part.id()); /// @todo use ofPart ?
        return m;
    }
}


void HPosition::print(std::ostream& o) const
{
    if (_link)
    {
        _link->print(o);
    }
    else
    {
        o << MTU::to_string(_time) << "MTU";
    }
}


std::ostream& operator<<(std::ostream& o, const HPosition& p)
{
    p.print(o); // dispatch to descendant classes
    return o;
}


} // namespace ScoreModel

//
//  LeafEvent.hpp
//  squanty
//
//  Created by Florent Jacquemard on 20/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//


#ifndef SM_LeafEvent_hpp
#define SM_LeafEvent_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>
#include <vector>

#include "Leaf.hpp"
#include "Articulation.hpp"

namespace ScoreModel {

/// leaf node in a tree representation of engraving information,
/// containing a musical event.
///
/// The musical event's duration may be longer than the leaf event,
/// in case it is made from the leaf event plus some continuations.
/// @todo TBR
class LeafEvent : public Leaf
{
public:
    
    /// root node of event, in a wrapper.
    /// @see Leaf
    LeafEvent(Root& wrapper,
              //int staff,
              Leaf* previous=NULL, Leaf* next=NULL,
              AtomicEvent* e=NULL);


    /// non-root node of event.
    /// @see Leaf
    LeafEvent(size_t mult,
              Inner* parent,
              //int staff,
              Leaf* previous=NULL, Leaf* next=NULL,
              AtomicEvent* e=NULL);

    /// copy
    LeafEvent(const LeafEvent& n);

    virtual ~LeafEvent() { }

    virtual bool isLeafEvent() const { return true; }

    virtual void setEvent(AtomicEvent* e); //{ return _embedding; }

    //virtual void print(std::ostream& o) const;
    
protected:
    
    // @todo TBR (are in backlinks of leafs)
    // std::vector<Articulation> _articulations;
    
};

std::ostream& operator<<(std::ostream&, const LeafEvent&);


} // namespace ScoreModel

/// @}
#endif /* SM_LeafEvent_hpp */


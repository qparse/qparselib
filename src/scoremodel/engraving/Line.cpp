//
//  Line.cpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Line.hpp"


namespace ScoreModel {


std::ostream& operator<<(std::ostream& o, const Line::Type& t)
{
    o << "Line ";
    switch (t)
    {
        case Line::Solid:
            o << "Solid";
            break;
        case Line::Dashed:
            o << "Dashed";
            break;
        case Line::Dotted:
            o << "Dotted";
            break;
        case Line::Wavy:
            o << "Wavy";
            break;
        default:
            ERROR("Unknown line type {}", t);
            break;
    }
    return o;
}



} // namespace ScoreModel

//
//  HPosition.hpp
//  squanty
//
//  Created by Florent Jacquemard on 24/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{


#ifndef HPosition_hpp
#define HPosition_hpp

#include <stdio.h>

#include "trace.hpp"
#include "MTU.hpp"
//#include "Leaf.hpp"

namespace ScoreModel {

class Leaf;
class Score;
class Part;
class Measure;


/// An horizontal position is either a link to an event (node) or a date in musical time (union).
class HPosition
{
public:

    /// position relative to an event
    /// @param link event defining horizontal position. must not be NULL.
    HPosition(Leaf* link);

    /// position relative to an date in musical time
    /// @param time musical time value defining horizontal position.
    HPosition(mtu_t time, const Part& p);

    /// copy constructor
    HPosition(const HPosition& p);

    /// the horizontal position is defined by an event
    inline bool linked() const { return (_link != NULL); }
 
    inline Leaf* leaf() const { return _link; }

    /// if the h_position is a link, return the date of the linked event,
    /// otherwise, return the date of the h_position.
    mtu_t date() const;

    /// part containing this horizontal position.
    //Part& part() const;

    /// measure containing this horizontal position.
    Measure& measure() const;

    virtual void print(std::ostream& o) const;
    
private:
    /// event defining horizontal position.
    /// if NULL the horizontal position is defined by time.
    Leaf* _link;

    /// horizontal position defined by musical time value.
    /// defined only if the link is NULL
    mtu_t _time;

    /// part containing this HPosition.
    /// NULL if the link is non-NULL.
    const Part* _part;
};


std::ostream& operator<<(std::ostream&, const HPosition&);



} // namespace ScoreModel

#endif /* HPosition_hpp */

/// @} // group scoremodel

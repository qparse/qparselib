//
//  LeafHarmonic.hpp
//  squanty
//
//  Created by Florent Jacquemard on 03/07/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef LeafHarmonic_hpp
#define LeafHarmonic_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>

#include "LeafNote.hpp"
#include "Stem.hpp"


namespace ScoreModel {


/// @see http://usermanuals.musicxml.com/MusicXML/MusicXML.htm#CT-MusicXML-harmonic.htm
/// @see http://usermanuals.musicxml.com/MusicXML/MusicXML.htm#EL-MusicXML-artificial.htm
/// artificial harmonic (notation with 2 notes), with
/// - touching-pitch (diamond head)
/// - sounding-pitch (normal head)
/// a single note.
/// @todo OBSOLETE, particular case of chord.
class LeafHarmonic : public LeafNote
{
public:

    /// single note tree, this note is the root, embedded in given wrapper.
    /// @param stem stem direction
    /// @param tremolo integer from 0 to 8 indicating number
    /// of tremolo marks (~ beams) for single note tremolo.
    /// @param pitch_touching pitch of the sounding note.
    /// @param accident_touching accident for touching note
    /// @param accident_sounding accident for sounding note
    /// @param e score model element for the sounding note.
    LeafHarmonic(Root& wrapper,
                 int staff,
                 const Pitch& pitch_touching,
                 float accident_touching,
                 float accident_sounding,
                 const Stem& stem,
                 int tremolo=0,
                 Leaf* previous=NULL, Leaf* next=NULL,
                 AtomicEvent* e=NULL);
    
    /// note leaf with a parent.
    LeafHarmonic(size_t mult,
                 Inner* parent,
                 int staff,
                 const Pitch& pitch_touching,
                 float accident_touching,
                 float accident_sounding,
                 const Stem& stem,
                 int tremolo=0,
                 Leaf* previous=NULL, Leaf* next=NULL,
                 AtomicEvent* e=NULL);

    virtual bool isHarmonic() const { return true; }
    
    /// @todo complete with accessors
    Pitch pitchTouching;
    
    float accidentTouching;

    virtual void accept(SMVisitor*) const;

    virtual void print(std::ostream& o) const;

};

std::ostream& operator<<(std::ostream&, const LeafHarmonic&);


} // namespace ScoreModel

/// @}
#endif /* LeafHarmonic_hpp */


//
//  StaffDef.hpp
//  squant2
//
//  Created by Florent Jacquemard on 11/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef StaffDef_hpp
#define StaffDef_hpp
/// @addtogroup scoremodel
/// @{


#include <stdio.h>
#include <iostream>

#include "trace.hpp"
#include "Ambitus.hpp"
//#include "Part.hpp"
#include "Clef.hpp"
#include "MeterSig.hpp"
#include "KeySig.hpp"
#include "SMVisitor.hpp"



namespace ScoreModel {

class AtomicEvent;
class Note;
class GraceNote;
class Rest;
class Chord;
class Trill;


/// @brief Some information about the engraving content of a staff;
/// but does not contain the actual engraving content of the staff.
/// The class inherits SMVisitor for dispatch on AtomicElements engraved in staff.
/// @see https://music-encoding.org/guidelines/v4/elements/staffdef.html
class StaffDef
{
public:
   
    /// @brief new staff definition, with default meter signature and key signature of the score.
    /// @param part container.
    /// @param n staff number. must be positive or null.
    /// @param clef the clef at beginning of staff
    /// The created voice is initialy empty and its duration is set to 0.
    StaffDef(Part& part, int n,
             const Clef& clef = Clef(Clef::GTreble));
    
    /// @brief new staff definition, with given meter signature and key signature.
    /// @param part container.
    /// @param n staff number. must be positive or null.
    /// @param clef the clef at beginning of staff
    /// The created voice is initialy empty and its duration is set to 0.
    StaffDef(Part& part, int n,
             const Clef& clef, const MeterSig& ts, const KeySig& ks);

    /// copy
    StaffDef(const StaffDef& s);
    
    virtual ~StaffDef();
           
    inline int nb() const { return _nb; }
    
    inline const Part& part() const { return _part; }
    
    inline std::string id() const { return _id; };
    
    virtual std::string name() const { return std::string("staff_"); }

    /// clef at the beginning of the staff. can be modified.
    /// default is the clef of the embedding score.
    inline Clef& clef() { return _clef; }
    void setClef(const Clef&); // redundant
    
    /// key signature at the beginning of the staff. can be modified.
    /// default is the clef of the embedding score.
    inline KeySig& keySig() { return _ks; }
    void setKeySig(const KeySig&); // redundant

    /// time signature at the beginning of the staff. can be modified.
    /// default is the clef of the embedding score.
    inline MeterSig& meterSig() { return _ts; }
    void setMeterSig(const MeterSig&); // redundant

    /// lowest pitch in the staff.
    /// @return the lowest pitch in the staff. can be undef if staff is empty.
    inline const Pitch& lowest() const { return _ambitus.low(); }

    /// highest pitch in the staff.
    /// @return the highest pitch in the staff. can be undef if staff is empty.
    inline const Pitch& highest() const { return _ambitus.high(); }

    inline const Ambitus& ambitus() const { return _ambitus; }
    
    bool empty() const;
    
    /// update this staff def with the given event.
    /// @param e an atomic event.
    void add(const AtomicEvent* e);

    virtual void print(std::ostream& o) const;
    
private:
    /// the part to which this staff belong
    const Part& _part;

    /// staff number
    int _nb;
    
    /// @brief id as a string for XML output
    std::string _id;
  
    /// initial clef
    Clef _clef;

    /// initial key signature
    KeySig _ks;
    
    /// initial time signature
    MeterSig _ts;

    Ambitus _ambitus;
    
    /// @todo instrDef
  
   
};


} // namespace ScoreModel

/// @}

#endif /* StaffDef_hpp */

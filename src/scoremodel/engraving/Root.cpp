//
//  Root.cpp
//  squanty
//
//  Created by Florent Jacquemard on 03/07/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Root.hpp"
#include "Leaf.hpp"


namespace ScoreModel {


Root::Root(mtu_t start, mtu_t dur, Sequence& container):
Inner(start, dur, 1, container),
_voice(container.voiceNb()),
_content(NULL),
_lastLeaf(NULL)
{    }


Root::Root(mtu_t start, mtu_t dur, Sequence& container, Node* n):
Inner(start, dur, 1, container),
_voice(container.voiceNb()),
_content(n),
_lastLeaf(NULL)
{
    assert(n);
    assert(n->multiplicity() == 1);
    assert(_staves.empty());
    _staves.merge(n->staves());
}


Root::~Root()
{
    TRACE("delete Root node, measure {} voice {} part:{}",
          _container.measureNb(), _voice, _container.measure().part().id());
}


void Root::delete_tree()
{
    TRACE("delete the content of Root node, measure {} voice {} part:{}",
          _container.measureNb(), _voice, _container.measure().part().id());

    assert(_content);
    _content->delete_tree();
    delete _content;
}


//Part& Root::part() const
//{
//    return voice().part();
//}


// 1 unique child
mtu_t Root::start_child() const
{
    assert(arity() == 1);
    assert(! complete());
    return this->date();
}


// 1 unique child
mtu_t Root::dur_child() const
{
    assert(arity() == 1);
    assert(! complete());
    return this->duration();
}


bool Root::replace(size_t i, Node* n)
{
    TRACE("Root Node: replacement of wrapped node by {}", *n);
    assert(i == 0);
    assert(n);
    assert(n->multiplicity() == 1);
//    if (n->multiplicity() != 1)
//    {
//        ERROR("Root: the multiplicity of wrapper node must be 1");
//        return false;
//    }
    _content = n;
    _staves.merge(n->staves());
    return true;
}


bool Root::add(Node* n)
{
    TRACE("Root Node: initialize wrapped node {}", *n);
    if (complete())
    {
        assert(_content != NULL); // shoul be equivalent to complete() == true
        ERROR("Root.add: can be called only once.");
        return false;
    }

    return replace(0, n);
}


size_t Root::size() const
{
    if (_content == NULL)
        return 0;
    else
        return 1;
}


size_t Root::completed() const
{
    if (_content == NULL)
        return 0;
    else
    {
        assert(_content->multiplicity() == 1);
        return 1;
    }
}

Node* Root::sub(size_t i) const
{
    assert(i == 0);
    assert(_content != NULL);
    return _content;
}


void Root::setLastLeaf(Leaf* n)
{
    assert(n);
    assert(_lastLeaf == NULL);
    _lastLeaf = n;
}


void Root::accept(SMVisitor* v) const
{
    v->visitRoot(this); 
}


void Root::print(std::ostream& o) const
{
    o << "<";
    ((Node*) this)->print(o);
    o << ", voice=" << _voice;
    o << ">";
}


std::ostream& operator<<(std::ostream& o, const Root& n)
{
    n.print(o);
    return o;
}


} // namespace ScoreModel

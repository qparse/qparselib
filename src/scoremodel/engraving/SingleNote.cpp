//
//  SingleNote.cpp
//  qparse
//
//  Created by Florent Jacquemard on 19/11/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "SingleNote.hpp"


namespace ScoreModel {

SingleNote::SingleNote(float accident,
                       const Notehead& h,
                       bool harm):
_head(h),
_accidental(accident),
_harmonic(harm)
{ }

SingleNote::SingleNote(const SingleNote& n):
_head(n._head),
_accidental(n._accidental),
_harmonic(n._harmonic)
{ }

SingleNote::~SingleNote()
{ }


bool SingleNote::equal(const SingleNote& n) const
{
    return ((_head == n._head) &&
            (_accidental == n._accidental) &&
            (_harmonic == n._harmonic));
}


void SingleNote::print(std::ostream& o) const
{
    o << "head="; _head.print(o);
    if (_accidental != Pitch::UNDEF_NOTE_ALTERATION)
        o << "accident=" << _accidental;
}


std::ostream& operator<<(std::ostream& o, const SingleNote& n)
{
    n.print(o);
    return o;
}


} // namespace ScoreModel


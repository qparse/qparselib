//
//  Osc.hpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#ifndef Osc_hpp
#define Osc_hpp

#include <stdio.h>

#include "Spanning.hpp"

namespace ScoreModel {

/// @brief performance directive about various kind of oscillations
class Osc : public Spanning
{
public:
    
    enum Kind {
        Trill,
        Prall,
        Upprall,
        Downprall,
        Vibrato
    };
    
    
    Osc(const VPosition& vpos,
        const HPosition& hp1, const HPosition& hp2,
        Kind k);

    virtual bool isOsc() const { return true; }
    
    inline Kind kind() const { return _kind; }

    virtual void print(std::ostream& o) const;

private:
    
    Kind _kind;

};

} // namespace ScoreModel

#endif /* Osc_hpp */

/// @} // end group scoremodel

//
//  Dot.hpp
//  squanty
//
//  Created by Florent Jacquemard on 20/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#ifndef SM_Dot_hpp
#define SM_Dot_hpp

#include <stdio.h>

#include "Continuation.hpp"

namespace ScoreModel {

/// leaf node in a tree representation of engraving information,
/// containing a dot prolongating the previous event (by half its duration).
class Dot : public Continuation
{
public:
    
    /// dot node (non-root).
    /// @see Continuation
    Dot(size_t mult,
        Inner* parent,
        AtomicEvent* e,
        Leaf* previous, Leaf* next=NULL);
          
    /// copy
    Dot(const Continuation& c);   
    
    virtual ~Dot() {}

    virtual bool isDot() const { return true; }
    
    virtual void accept(SMVisitor*) const;

    virtual void print(std::ostream& o) const;

};


std::ostream& operator<<(std::ostream&, const Dot&);
   

} // namespace ScoreModel

#endif /* SM_Dot_hpp */

/// @}

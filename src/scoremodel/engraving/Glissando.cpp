//
//  Glissando.cpp
//  squanty
//
//  Created by Florent Jacquemard on 02/09/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Glissando.hpp"


namespace ScoreModel {


Glissando::Glissando(const VPosition& vpos,
                     const HPosition& hp1, const HPosition& hp2,
                     Line::Type type):
Spanning(vpos, hp1, hp2),
_linetype(type)
{ }


void Glissando::print(std::ostream& o) const
{
    o << "Glissando: ";
    o << _linetype;
}


} // namespace ScoreModel

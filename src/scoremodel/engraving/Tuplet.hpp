//
//  Tuplet.hpp
//  qparse
//
//  Created by Florent Jacquemard on 19/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef SM_Tuplet_hpp
#define SM_Tuplet_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>
#include <string>

//#include "Duration.hpp"
#include "MTU.hpp"
#include "SymbLabel.hpp"
#include "TupletLabel.hpp"
//#include "LRT.hpp"
//#include "Event.hpp"
//#include "Sequence.hpp"
#include "Inner.hpp"
#include "MeterSig.hpp"
#include "Beam.hpp"


namespace ScoreModel {

/// Non-leaf node with division of a time interval in equal sub-segments.
/// It includes every grouping, with ot without labels,
/// including "irrational" tuplets and normal binary divisions.
class Tuplet : public Inner
{

public:

    
    /// construct a new root tuplet node embedded in the given wrapper.
    /// @param ar arity of the new node.
    /// @param wrapper wrapper node. This new node will be added to it.
    Tuplet(size_t ar, Root& wrapper,
           const Beam::Info beam, const TupletLabel& tlab);
    
    
    /// construct new non-root tuplet node.
    /// @param ar arity (see Node).
    /// @param mult multiplicity (see Node).
    /// @param parent (see Node). must not be NULL.
    /// @param beam beaming policy for children nodes.
    /// @param tlab optional tuplet label
    Tuplet(size_t ar, size_t mult, Inner* parent,
           const Beam::Info beam, const TupletLabel& tlab);


    
    /// construct a new root tuplet node.
    /// @todo TBR deprecated
    /// @param start date (MTU) of the leftmost leaf under this node.
    /// @param dur cumulated duration (MTU) of the leaves under this node.
    /// @param ar arity of the node.
    /// @param container musical score model element (voice in a measure)
    ///        containing this tree.
    Tuplet(mtu_t start, mtu_t dur,
          size_t ar,
          Sequence& container);
    
    /// delete this node but not its content (sub-nodes).
    virtual ~Tuplet();

    /// recursively delete all nodes in the sub-trees of this tuplet node.
    virtual void delete_tree();
    
    /// subtree of given index.
    /// @param i the index of subtree requested.
    /// must be smaller than size().
    /// @return the ith subtree
    virtual Node* sub(size_t i) const;
    
    /// add the given tree at the end of subtree list.
    /// @param t pointer to a subtree. must be allocated.
    /// @return whether the node was effectively added.
    virtual bool add(Node* t);
    
	/**
	 * Duration of the tuplet
     * @todo TBR
	 */
	//Duration getDuration() const;

    /// Number of children of this node
    /// without counting multiplicities.
    virtual size_t size() const;
    
    inline const TupletLabel& label() const { return _label; }

    inline const Beam::Info& beaming() const { return _beaming; }
    
    /// Current number of edges from this node.
    /// i.e. number of children that have been added times
    /// their respective multiplicities.
    virtual size_t completed() const;

	/**
	 * Base duration = duration of regular tuplet events,
     * before applying the ratoi
	 *
	 * Example: a triplet of eigthth, the base duration is the eighth
	 */
	mtu_t getBaseDuration() const;

	/**
	 * Arity = the number of duration-equal timespans the tuplet is decomposed in
	 */
	// size_t getArity() const;

	/**
	 * Numbase = the regular number of  base duration in the tuplet
	 *
	 * Example: a triplet of eigthth correspond to 2 (two) regular eigthth
	 */
	long getNumBase(MeterSig& ts) const;


	/**
     * Get the first event of the tuplet sequence
     */
	//Event* getFirstEvent() const;

	/**
	 * Last event
	 */
	//Event* getLastEvent() const;

	/**
	 * Get events
	 */
	//std::vector<Event*> getEvents() const;
       
    virtual bool isTuplet() const { return true; }
        
    virtual void accept(SMVisitor*) const;

    virtual void print(std::ostream& o) const;
    virtual std::string to_string() const;

protected:
       
    /// starting musical date of the next child to be added
    /// @warning this node must not be complete.
    virtual mtu_t start_child() const;

    /// duration of the next child to be added
    /// @warning this node must not be complete.
    virtual mtu_t dur_child() const;
    
    /// replace the node at given index by the given node.
    /// @param i the index of a subnode. must be smaller than size().
    /// @param n a new node. must be allocated.
    /// @return whether the replacement was effectively done.
    /// @warning the former sub-node at position i is not deleted.
    virtual bool replace(size_t i, Node* n);
    
private:

    std::vector<Node*> _children;

    /// beaming label
    Beam::Info _beaming;

    /// tuplet label
    TupletLabel _label;
    
    /// Current number of edges from this node = number of children
    /// that have been added times their respective multiplicities.
    size_t _completed;
    
    mtu_t init_start(Node* parent)
    {
        assert(parent);
        return parent->start();
    }

    mtu_t init_dur(Node* parent, size_t ar)
    {
        assert(parent);
        assert(ar > 0);
        return (parent->duration() / ar);
    }

};


std::ostream& operator<<(std::ostream& o, const Tuplet& d);


} // namespace ScoreModel

/// @}
#endif /* SM_Tuplet_hpp */

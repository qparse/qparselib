//
//  LeafChord.cpp
//  squanty
//
//  Created by Florent Jacquemard on 02/07/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "LeafChord.hpp"

namespace ScoreModel {


LeafChord::LeafChord(Root& wrapper,
                     const Arpeggio& a,
                     const Stem& s,
                     int tremolo,
                     Leaf* previous, Leaf* next,
                     AtomicEvent* e):
LeafEvent(wrapper, previous, next, e),
_stem(s),
_tremolo(tremolo),
_arp(a),
_content()
{
    assert(_staves.empty());
}


LeafChord::LeafChord(size_t mult,
                     Inner* parent,
                     const Arpeggio& a,
                     const Stem& s,
                     int tremolo,
                     Leaf* previous, Leaf* next,
                     AtomicEvent* e):
LeafEvent(mult, parent, previous, next, e),
_stem(s),
_tremolo(tremolo),
_arp(a),
_content()
{
    assert(_staves.empty());
}


LeafChord::LeafChord(const LeafChord& lc):
LeafEvent(lc),
_stem(lc._stem),
_tremolo(lc._tremolo),
_arp(lc._arp),
_content(lc._content) // copy of notes
{ }


const PitchedNote& LeafChord::bass() const
{
    assert(! empty());
    PitchedNoteBag::iterator it = _content.begin();
    assert(it != _content.end());
    return (*it);
}


const PitchedNote& LeafChord::treble() const
{
    assert(! empty());
    PitchedNoteBag::reverse_iterator it = _content.rbegin();
    assert(it != _content.rend());
    return (*it);
}


void LeafChord::addNote(const Pitch& p,
                        int staff,
                        float accident,
                        const Notehead& h,
                        bool harm)
{
    _content.emplace(p, staff, accident, h, harm);
    assert(staff >= 0);
    _staves.add(staff);
    // sequence().addStaff(staff); // also add to measure
    TRACE("add single Note {} to Chord (staff={})", p, staff);
}


void LeafChord::accept(SMVisitor* v) const
{
    v->visitLeafChord(this);
}


/// @todo STUB complete
void LeafChord::print(std::ostream& o) const
{
    o << _content.size() << "-Chord ";
    if (_stem.type() != Stem::Type::Unspecified)
        _stem.print(o); // { o << "stem="; _stem.print(o); }
    if (_tremolo > 0)
        o << "tremolo=" << _tremolo;
    if (_arp.arpegiate())
        _arp.print(o);
    o << " { ";
    for (const PitchedNote& n : _content)
    {
        n.print(o);
        o << ", ";
    }
    o << " }";
}

std::ostream& operator<<(std::ostream& o, const LeafChord& e)
{
    e.print(o);
    return o;
}



} // namespace ScoreModel

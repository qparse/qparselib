//
//  Arpeggio.cpp
//  squanty
//
//  Created by Florent Jacquemard on 02/07/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Arpeggio.hpp"



namespace ScoreModel {


Arpeggio::Arpeggio(Line::Type linetype,
                   Arrow arrowtype,
                   bool bracket):
_linetype(linetype),
_arrowtype(arrowtype),
_bracket(bracket)
{
    assert( (_linetype == Line::Wavy) || (_linetype == Line::Solid));
    assert((_bracket == false) || (_arrowtype == None));
}


bool Arpeggio::operator==(const Arpeggio& rhs) const
{
    return((_linetype == rhs._linetype) &&
           (_arrowtype == rhs._arrowtype) &&
           (_bracket == rhs._bracket));
}


Arpeggio& Arpeggio::operator=(const Arpeggio& rhs)
{
    if(this != &rhs)
    {
        _linetype = rhs._linetype;
        _arrowtype = rhs._arrowtype;
        _bracket = rhs._bracket;
    };

    return *this;
}


void Arpeggio::print(std::ostream& o) const
{
    if (_bracket || (_linetype == Line::None))
    {
        o << "non-arpeggiate";
    }
    else
    {
        o << "arpeggiate(";
        if (_linetype == Line::Wavy)
            o << "wavy";
        else if (_linetype == Line::Solid)
            o << "solid";
        else
            ERROR("Arpegio: unexpected line type {}", _linetype);
        if (_arrowtype == Up)
            o << ", up";
        else if (_arrowtype == Down)
            o << ", down";
        o << ")";
    }
}


std::ostream& operator<<(std::ostream& o, const Arpeggio& a)
{
    a.print(o);
    return o;
}



} // end namespace ScoreModel

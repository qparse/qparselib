//
//  Glissando.hpp
//  squanty
//
//  Created by Florent Jacquemard on 02/09/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#ifndef Glissando_hpp
#define Glissando_hpp

#include <stdio.h>

#include "Line.hpp"
#include "Spanning.hpp"

namespace ScoreModel {

/// @brief performance directive about various kind of oscillations
class Glissando : public Spanning
{
public:
    
        
    /// @param vpos vertical position.
    /// @param hp1 starting horizontal position.
    /// @param hp2 ending horizontal position.
    Glissando(const VPosition& vpos,
              const HPosition& hp1, const HPosition& hp2,
              Line::Type type = Line::Wavy);

    virtual bool isGlissando() const { return true; }
    
    Line::Type linetype() const { return _linetype; }

    virtual void print(std::ostream& o) const;

private:
    
    Line::Type _linetype;


};

} // namespace ScoreModel

#endif /* Glissando_hpp */

/// @} // end group scoremodel

//
//  TupletLabel.cpp
//  squanty
//
//  Created by Florent Jacquemard on 23/09/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "TupletLabel.hpp"
#include "util.hpp"


namespace ScoreModel {


TupletLabel::TupletLabel(int n, int p):
_num(n),
_intimeof(p)
{
    assert(n >= 0);
    assert(p >= 0);
    assert((n != 0) || (p == 0));
}


/// @todo a revoir
TupletLabel::TupletLabel(size_t a, size_t m,
                         mtu_t dur, const MeterSig& ts):
_num(0),
_intimeof(0)
{
    assert(a > 0);
    assert(m > 0);
    assert(! ts.undef());
    int nb = ts.beats(); // number of beats in bar.
    assert(nb > 0);
    mtu_t beatlen = MTU::invert(nb); // duration of 1 beat

    assert(dur <= 1);

    // bar level
    if (dur == 1)
    {
        if ((a <= nb) && ((nb % a) == 0))
            _num = 0; // no number displayed
        else if ((a > nb) && ((a % nb) == 0))
            _num = 0; /// @todo TBC
        else
            _num = a;
    }
    // inside the bar, above the beat level
    else if ((beatlen < dur ) && (dur < 1))
    {
        // number of beats in this portion of bar
        Rational nbb = Rational(nb) * dur;
        if (nbb.integral() &&
            (((trunc(nbb) % a) == 0) ||
             ((a % trunc(nbb)) == 0)))
            _num = 0; /// @todo TBC
        else
            _num = a;
    }
    // beat level, compound measure
    else if ((dur == beatlen) && ts.compound())
    {
        if ((a % 3) == 0)
            _num = 0;
        else
            _num = a;
    }
    // beat level, simple measure
    else if ((dur == beatlen) && ts.simple())
    {
        if (util::ispow2(a))
            _num = 0;
        else
            _num = a;
    }
    // inside the beat level
    /// @todo TBC (same as above)
    else
    {
        if (util::ispow2(a))
            _num = 0;
        else
            _num = a;
    }

    /// compute the in_time_of part
    // no label
    if (_num == 0)
    {
        _intimeof = 0;
    }
    // compound at beat level
    else if ((dur == beatlen) && ts.compound())
    {
        _intimeof = 0; // duolets etc: in_time_of 3 skip
    }
    // simple or compound not at beat level
    else if (ts.simple())
    {
        long loga = util::trunclog2(a); // number of additional beams
        _intimeof = loga * m;
        if (_intimeof == 1) _intimeof = 0;
    }
}


TupletLabel::TupletLabel(const TupletLabel& tl):
_num(tl._num),
_intimeof(tl._intimeof)
{ }



/// @todo STUB
std::string TupletLabel::to_string() const
{
    std::ostringstream os;
    this->print(os); // os << *(this);
    return os.str();
}


void TupletLabel::print(std::ostream& o) const
{
    o << _num << ":" << _intimeof;
}


std::ostream& operator<<(std::ostream& o, const TupletLabel& d)
{
    d.print(o);
    return o;
}



} // namespace ScoreModel

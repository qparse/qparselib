//
//  Fermata.hpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#ifndef Fermata_hpp
#define Fermata_hpp

#include <stdio.h>

#include "Local.hpp"

namespace ScoreModel {

/// @brief Fermata sign
class Fermata : public Local
{
public:

    enum Shape {
        Normal,
        Angled,       // short fermata
        DoubleAngled, // very short fermata
        Square,       // long fermata
        DoubleSquare, // very long fermata
        Henze         // long fermata
    };
    
    Fermata(const VPosition& vpos, Leaf* link, Shape shape);
    Fermata(const VPosition& vpos, mtu_t date, const Part& p, Shape shape);

    Shape shape() const { return _shape; }

    virtual bool isFermata() const { return true; }

    virtual void print(std::ostream& o) const;

private:

    Shape _shape;
    
};

std::ostream& operator<<(std::ostream&, const Fermata&);

} // namespace ScoreModel

#endif /* Fermata_hpp */

/// @} // end group scoremodel

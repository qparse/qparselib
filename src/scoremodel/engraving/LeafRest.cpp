//
//  LeafRest.cpp
//  squanty
//
//  Created by Florent Jacquemard on 03/07/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "LeafRest.hpp"

namespace ScoreModel {

LeafRest::LeafRest(Root& wrapper,
                   int staff,
                   Leaf* previous, Leaf* next,
                   AtomicEvent* e):
LeafEvent(wrapper, previous, next, e)
{
    assert(staff >= 0);
    assert(_staves.empty());
    _staves.add(staff);  // _container.addStaff(staff);
}


LeafRest::LeafRest(size_t mult,
                   Inner* parent,
                   int staff,
                   Leaf* previous, Leaf* next,
                   AtomicEvent* e):
LeafEvent(mult, parent, previous, next, e)
{
    assert(staff >= 0);
    assert(_staves.empty());
    _staves.add(staff);  // _container.addStaff(staff);
}


void LeafRest::accept(SMVisitor* v) const
{
    v->visitLeafRest(this);
}


void LeafRest::print(std::ostream& o) const
{
    o << "Rest" << duration();
    ((Leaf*) this)->print(o);
}

std::ostream& operator<<(std::ostream& o, const LeafRest& e)
{
    e.print(o);
    return o;
}

} // namespace ScoreModel


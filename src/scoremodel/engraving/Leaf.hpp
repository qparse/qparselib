//
//  Leaf.hpp
//  squanty
//
//  Created by Florent Jacquemard on 20/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//


#ifndef SM_Leaf_hpp
#define SM_Leaf_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>

#include "Node.hpp"
#include "AtomicEvent.hpp"
//#include "Articulation.hpp"

namespace ScoreModel {

class Local;

/// polymorphic list of annotations (Local, Floating, Spanning)
typedef std::vector<const Local*> Backlinks;


/// leaf node in a tree representation of engraving information, with a non-zero duration.
/// It is associated to exactly one  atomic event embedded in the musical representation of the score model.
/// several Leave events can be associated to the same atomic event,
/// e.g. a note or rest leaf and one or several  Dot, or a note leaf and one or several  Tied.
/// @see Ornament for another kind of leaf, with duration zero.
class Leaf : public Node
{
public:

    /// construct a single node tree: leaf is root, in given wrapper.
    /// @param wrapper wrapper node. This new node will be added to it.
    // @param staff number of the staff where this event is displayed.
    /// @param e pointer to the embedding score model element.
    /// @param previous previous leaf in left-depth-first-traversal ordering.
    /// @param next next leaf in left-depth-first-traversal ordering.
    /// @warning e, previous, next can be NULL if unknown.
    Leaf(Root& wrapper,
         // int staff,   /// @todo TBR
         AtomicEvent* e,
         Leaf* previous=NULL, Leaf* next=NULL);
    
    /// construct a leaf node below of another node.
    /// @param mult number of (multiple) edges between this node
    /// and its parent. must be strictly positive.
    /// @param parent the parent node. must be an allocated inner node.
    // @param staff number of the staff where this event is displayed.
    ///        must be positive or null.
    /// @param e pointer to the embedding score model element.
    /// @param previous previous leaf in left-depth-first-traversal ordering.
    /// @param next next leaf in left-depth-first-traversal ordering.
    /// @warning e, previous, next can be NULL if unknown.
    Leaf(size_t mult,
         Inner* parent,
         //int staff,   /// @todo TBR
         AtomicEvent* e,
         Leaf* previous=NULL, Leaf* next=NULL);

    /// construct a single node tree (leaf is root).
    /// @todo TBR deprecated
    /// @param start onset (in MTU) of the leaf event.
    /// May be later than the onset of the embedding score model element.
    /// @param dur duration (in MTU) of the leaf event.
    /// May be shorter than the onset of the embedding score model element.
    /// @param container musical score model element (voice in a measure)
    ///        containing this tree.
    // @param staff number of the staff where this event is displayed
    /// @param e pointer to the embedding score model element.
    /// @param previous previous leaf in left-depth-first-traversal ordering.
    /// @param next next leaf in left-depth-first-traversal ordering.
    /// @warning e, previous, next can be NULL if unknown.
    Leaf(mtu_t start, mtu_t dur,
         Sequence& container,
         //int staff,   /// @todo TBR
         AtomicEvent* e,
         Leaf* previous=NULL, Leaf* next=NULL);

    /// copy
    Leaf(const Leaf& l);

    // embedding score event is assumed deallocated elsewhere
    virtual ~Leaf();
    
    /// do nothing - there are no subtrees to delete for a leaf.
    /// @warning should not be called for a Leaf.
    virtual void delete_tree();

    /// number of edges from this node to its children
    /// redefinition of Node.
    virtual size_t arity() const { return 0; }
    
    /// number of children nodes.
    /// redefinition of Node.
    virtual size_t size() const { return 0; }

    /// a Leaf is always complete (zero subtrees).
    /// redefinition of Node.
    virtual bool complete() const { return true; }

    /// do nothing.
    /// @warning should not be called for a Leaf.
    /// redefinition of Node.
    virtual bool add(Node* t);
    
    /// @return NULL
    /// @warning should not be called for a Leaf.
    /// redefinition of Node.
    virtual Node* sub(size_t i) const;

    /// voice containing the associated (unique) atomic event.
    virtual Voice& voice() const;

    /// @todo TBR (undef for chord)
    // inline int staff() const { return _staff; }
    
    /// Score Model atomic event that contains the event in this leaf as a part.
    inline AtomicEvent* event() const { return _embedding; }
    
    /// next Leaf in tree
    inline Leaf* getNext() const { return _next; }

    /// set next leaf in left-depth-first-traversal ordering
    /// @param n pointer to next leaf. must be allocated.
    /// @warning can be called at most once.
    void setNext(Leaf* n);

    /// previous Leaf in tree
    inline Leaf* getPrevious() { return _previous; }

    /// set previous leaf in left-depth-first-traversal ordering
    /// @param p pointer to previous leaf. must not be NULL.
    /// @warning can be called at most once.
    void setPrevious(Leaf* p);

    /// annotations (Local, Floating, Spanning) located at this leaf.
    /// @todo TBR
    //std::vector<const Local*> backlinks() const { return _backlinks; }
    
    /// scan back-links = annotations (Local, Floating, Spanning) located at this leaf.
    Backlinks::iterator beginBacklinks();
    Backlinks::iterator endBacklinks();
    Backlinks::const_iterator cbeginBacklinks() const;
    Backlinks::const_iterator cendBacklinks() const;
    
    inline size_t nbBacklinks() const
    { return _backlinks.size(); }
    
    const Local* backlink(size_t i) const;

    /// Register an annotation located at this leaf.
    /// @param d annotation to register. must be allocated.
    void addLink(const Local* d);
    
    virtual bool isLeaf() const { return true; }

    virtual void print(std::ostream& o) const;
    
    
protected:
    
    /// Score model element that contains this leaf as a part.
    /// const pointer to non-const value.
    AtomicEvent* _embedding;
   
    /// number of the staff where this event is displayed
    // int _staff;     /// @todo TBR (undef for chord)
    
    /// the leafs in the tree are linked following a left-depth-first-traversal ordering
    Leaf* _previous;
    Leaf* _next;
    
    /// list of Local, Floating, Spanning, located at this leaf.
    std::vector<const Local*> _backlinks;

    // do nothing.
    // @warning should not be called for a Leaf.
    // virtual bool replace(size_t i, Node* n);

};


std::ostream& operator<<(std::ostream& o, const Leaf& l);


} // namespace ScoreModel

/// @}
#endif /* SM_Leaf_hpp */



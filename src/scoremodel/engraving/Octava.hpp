//
//  Octava.hpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#ifndef Octava_hpp
#define Octava_hpp

#include <stdio.h>

#include "Spanning.hpp"

namespace ScoreModel {

/// @brief delimited octave shift
class Octava : public Spanning
{
public:

    /// @param vpos vertical position.
    /// @param hp1 starting horizontal position.
    /// @param hp2 ending horizontal position.
    /// @param nb number of octava shift, upper (>0) or lower (<0).
    ///        +/-1 for 8, +/-2 for 15, +/-3 for 22. must not be 0.
    Octava(const VPosition& vpos, const HPosition& hp1, const HPosition& hp2,
           int nb);

    virtual bool isOctava() const { return true; }
    
    int number() const { return _nb; }

    virtual void print(std::ostream& o) const;
       
private:

   /// number of octava shift, upper (>0) or lower (<0)
    int _nb;
    
};


} // namespace ScoreModel

#endif /* Octava_hpp */

/// @} // end group scoremodel

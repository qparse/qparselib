//
//  LeafNote.cpp
//  squanty
//
//  Created by Florent Jacquemard on 03/07/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "LeafNote.hpp"

namespace ScoreModel {

LeafNote::LeafNote(Root& wrapper,
                   int staff,
                   float accident,
                   const Notehead& h,
                   const Stem& s,
                   bool harm,
                   int tremolo,
                   Leaf* previous, Leaf* next,
                   AtomicEvent* e):
LeafEvent(wrapper, previous, next, e),
SingleNote(accident, h, harm),
//_head(h),
_stem(s),
//_accidental(accident),
//_harmonic(harm),
_tremolo(tremolo)
{
    assert(staff >= 0);
    assert(_staves.empty());
    _staves.add(staff); // _container.addStaff(staff);
}


LeafNote::LeafNote(size_t mult,
                   Inner* parent,
                   int staff,
                   float accident,
                   const Notehead& h,
                   const Stem& s,
                   bool harm,
                   int tremolo,
                   Leaf* previous, Leaf* next,
                   AtomicEvent* e):
LeafEvent(mult, parent, previous, next, e),
SingleNote(accident, h, harm), 
//_head(h),
_stem(s),
//_accidental(accident),
//_harmonic(harm),
_tremolo(tremolo)
{
    assert(staff >= 0);
    assert(_staves.empty());
    _staves.add(staff); // _container.addStaff(staff);
}


LeafNote::LeafNote(const LeafNote& n):
LeafEvent(n),
SingleNote(n),
_stem(n._stem),
_tremolo(n._tremolo)
{ }


void LeafNote::accept(SMVisitor* v) const
{
    v->visitLeafNote(this);
}


void LeafNote::print(std::ostream& o) const
{
    ((Leaf*) this)->print(o);
    o << " ";
    ((SingleNote*) this)->print(o);
    o << " ";
    if (_stem.type() != Stem::Type::Unspecified)
        _stem.print(o); // { o << "stem="; _stem.print(o); }
    if (_tremolo > 0)
        o << "tremolo=" << _tremolo;
}


std::ostream& operator<<(std::ostream& o, const LeafNote& n)
{
    n.print(o);
    return o;
}


} // namespace ScoreModel

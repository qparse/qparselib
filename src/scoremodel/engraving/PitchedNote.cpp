//
//  PItchedNote.cpp
//  squant2
//
//  Created by Florent Jacquemard on 07/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "PitchedNote.hpp"


namespace ScoreModel {


PitchedNote::PitchedNote(const Pitch& p,
                         int staff,
                         float accident,
                         const Notehead& h,
                         bool harm):
SingleNote(accident, h, harm),
Staffed(staff),
_pitch(p)
{
    assert(staff >= 0);
    
}


PitchedNote::PitchedNote(const PitchedNote& pn):
SingleNote(pn),
Staffed(pn),
_pitch(pn._pitch)
{ }


bool PitchedNote::equal(const PitchedNote& p) const
{
    return (p.SingleNote::equal(p) &&
            (staff() == p.staff()) &&
            (_pitch == p._pitch));
}


bool PitchedNote::smaller(const PitchedNote& p) const
{
    return (_pitch < p.pitch());
}


void PitchedNote::accept(SMVisitor* v) const
{
    v->visitPitchedNote(this);
}


void PitchedNote::print(std::ostream& o) const
{
    _pitch.print(o);
    ((SingleNote*) this)->print(o);
}


std::ostream& operator<<(std::ostream& o, const PitchedNote& n)
{
    n.print(o);
    return o;
}


} // namespace ScoreModel

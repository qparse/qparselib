//
//  Breath.cpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Breath.hpp"

namespace ScoreModel {

Breath::Breath(const VPosition& vpos, Leaf* link, Kind m):
Local(vpos, link),
_mark(m)
{ }

Breath::Breath(const VPosition& vpos,
               mtu_t date, const Part& p,
               Kind m):
Local(vpos, date, p),
_mark(m)
{ }

void Breath::print(std::ostream& o) const
{
    o << "Breath: ";
    switch (_mark)
    {
        case Comma:
            o << "Comma";
            break;
        case Tick:
            o << "Tick";
            break;
        case Saltzed:
            o << "Saltzed";
            break;
        case UpBow:
            o << "UpBow";
            break;
        case Caesura:
            o << "Caesura";
            break;
        case CurvedCaesura:
            o << "CurvedCaesura";
            break;
        case ShortCaesura:
            o << "ShortCaesura";
            break;
        case ThickCaesura:
            o << "ThickCaesura";
            break;
        default:
            ERROR("Breath: unknown marker {}", _mark);
            break;
    }
}


std::ostream& operator<<(std::ostream& o, const Breath& b)
{
    b.print(o);
    return o;
}

} // namespace ScoreModel

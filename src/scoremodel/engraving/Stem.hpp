//
//  Stem.hpp
//  squanty
//
//  Created by Florent Jacquemard on 03/07/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//


#ifndef Stem_hpp
#define Stem_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>
#include <iostream>

#include "trace.hpp"

namespace ScoreModel {

/// @brief Direction of a stem
class Stem
{
public:

    /// see http://usermanuals.musicxml.com/MusicXML/MusicXML.htm#EL-MusicXML-stem.htm
    enum Type {
        /// Renderer default.
        Unspecified,
        /// No stem.
        None,
        /// Up stem.
        Up,
        /// Down stem.
        Down,
        /// Up and Down (twice the same note).
        Double
    };

    /// main constructor
    Stem(Type t);
    
    /// copy constructor
    Stem(const Stem&);

    inline Type type() const { return _type; }

    virtual void print(std::ostream& o) const;
       
private:

    Type _type;

};

std::ostream& operator<<(std::ostream& o, const Stem& s);

} // namespace ScoreModel

/// @}
#endif /* Stem_hpp */


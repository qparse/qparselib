//
//  Multi.hpp
//  squanty
//
//  Created by Florent Jacquemard on 02/07/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef Multi_hpp
#define Multi_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>
#include <vector>

#include "LeafNote.hpp"


namespace ScoreModel {


/// container for sequence of leaves
class Multi
{
    
public:
    /// Multi of size zero
    Multi();

    /// every leaf which is a tree (no parent) is deleted.
    ~Multi();
        
    /// number of events in this multi.
    size_t size() const;
        
    /// return the ith event in this multi
    /// @param i event index in multi. first is 0.
    const LeafNote* event(size_t i) const;

    /// add the given event to this multi.
    /// @param e event to add. must not be null.
    void add(const LeafNote* e);
        
    virtual void print(std::ostream& o) const;

protected:
    
    std::vector<const LeafNote*> _content;

};

    std::ostream& operator<<(std::ostream&, const Multi&);

} // namespace ScoreModel

/// @}
#endif /* Multi_hpp */


//
//  Osc.cpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Osc.hpp"


namespace ScoreModel {

Osc::Osc(const VPosition& vpos,
         const HPosition& hp1, const HPosition& hp2,
         Kind k):
Spanning(vpos, hp1, hp2),
_kind(k)
{ }


void Osc::print(std::ostream& o) const
{
    switch (_kind)
    {
        case Trill:
            o << "Trill";
            break;
        case Prall:
            o << "Prall";
            break;
        case Upprall:
            o << "Upprall";
            break;
        case Downprall:
            o << "Downprall";
            break;
        case Vibrato:
            o << "Vibrato";
            break;
        default:
            ERROR("Osc: unexpected value {}", _kind);
            break;
    }
}


} // namespace ScoreModel

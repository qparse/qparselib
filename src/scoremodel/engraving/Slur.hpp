//
//  Slur.hpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#ifndef Slur_hpp
#define Slur_hpp

#include <stdio.h>

#include "Line.hpp"
#include "Spanning.hpp"

namespace ScoreModel {

/// @brief curved line indicating a legato articulation
class Slur : public Spanning
{
public:

    Slur(const VPosition& vpos, const HPosition& hp1, const HPosition& hp2,
         Line::Type type = Line::Solid);

    virtual bool isSlur() const { return true; }

    Line::Type linetype() const { return _linetype; }

    virtual void print(std::ostream& o) const;

private:
    
    Line::Type _linetype;
    
};

} // namespace ScoreModel

#endif /* Slur_hpp */

/// @} // end group scoremodel

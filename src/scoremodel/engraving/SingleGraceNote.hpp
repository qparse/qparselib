//
//  SingleGraceNote.hpp
//  squant2
//
//  Created by Florent Jacquemard on 29/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef SingleGraceNote_hpp
#define SingleGraceNote_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>

#include "GraceNote.hpp"
#include "Staffed.hpp"
#include "SingleNote.hpp"
#include "SMVisitor.hpp"


namespace ScoreModel {

/// grace note in an ornament.
/// particular case of  PitchedNote associated to a grace note.
class SingleGraceNote : public SingleNote,
                        public Staffed
{
    
public:
    
    /// single note elements
    /// @param n associated grace note. must be allocated.
    /// @param staff number of staff where the grace note is located
    /// @param accident displayed accident or courtesy
    /// @param h shape of the note head.
    /// @param harm whether the note is an natural harmonic
    /// @todo harmonic redundant with note-head?
    SingleGraceNote(const GraceNote* n,
                    int staff,
                    float accident = Pitch::UNDEF_NOTE_ALTERATION,
                    const Notehead& h = Notehead::Shape::Oval,
                    bool harm = false);

    /// copy constructor
    SingleGraceNote(const SingleGraceNote& n);

    virtual ~SingleGraceNote();

    const Pitch& pitch() const;
    
    /// Score Model atomic event associated to this  element.
    inline const GraceNote* gracenote() const { return _embedding; }

    virtual void accept(SMVisitor*) const;

    virtual void print(std::ostream& o) const;

protected:
    
    /// associated grace note, score model element.
    const GraceNote* _embedding;

};

std::ostream& operator<<(std::ostream&, const SingleGraceNote&);

} // namespace ScoreModel

/// @}

#endif /* PitchedGraceNote_hpp */

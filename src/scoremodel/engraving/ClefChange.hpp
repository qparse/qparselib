//
//  ClefChange.hpp
//  squanty
//
//  Created by Florent Jacquemard on 01/09/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#ifndef ClefChange_hpp
#define ClefChange_hpp

#include <stdio.h>

#include "Clef.hpp"
#include "Local.hpp"

namespace ScoreModel {

/// @brief Change of Clef
class ClefChange : public Local
{
public:

    /// change before an event.
    ClefChange(const VPosition& vpos, Leaf* link, const Clef& c);

    /// change at a date.
    ClefChange(const VPosition& vpos,
               mtu_t date, const Part& p,
               const Clef& c);

    Clef clef() const { return _clef; }
   
    virtual bool isClefChange() const { return true; }

    virtual void print(std::ostream& o) const;
 
private:
    
    Clef _clef;

};

std::ostream& operator<<(std::ostream&, const ClefChange&);

} // namespace ScoreModel

#endif /* ClefChange_hpp */

/// @} // end group scoremodel





//
//  Mordent.hpp
//  qparse
//
//  Created by Florent Jacquemard on 18/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#ifndef SM_Mordent_hpp
#define SM_Mordent_hpp

#include <stdio.h>

#include "Ornament.hpp"
#include "Local.hpp"


namespace ScoreModel {



/// rapid alternation between a main note
/// and the note above (upper mordent)
/// or the note below (lower mordent).
class Mordent : public Ornament, Local
{
    
public:
 
    /// empty ornament (no links to score grace-notes)
    /// @param up upper or lower mordent
    Mordent(const VPosition& vpos,
            Leaf* link,
            bool before=true,
            bool up=true);

    /// new mordent copying the ornament content.
    /// @param up upper or lower mordent
    Mordent(const Ornament& o,
            const VPosition& vpos,
            Leaf* link,
            bool up=true);

    ~Mordent() {}

    /// mordent is upper or lower
    /// @return true if the this mordent is an upper mordent
    /// and false if it is a lower mordent.
    bool upper() const { return _up; }
    
    virtual bool isMordent() const    { return true; }
    virtual bool isUpMordent() const  { return _up; }
    virtual bool isLowMordent() const { return (! _up); }

    virtual void accept(SMVisitor*) const;

    virtual void print(std::ostream& o) const;
    
protected:
    bool _up;

};


std::ostream& operator<<(std::ostream&, const Mordent&);


} // namespace ScoreModel

#endif /* SM_Mordent_hpp */

/// @}

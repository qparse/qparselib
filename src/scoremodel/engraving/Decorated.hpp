//
//  Decorated.hpp
//  squanty
//
//  Created by Florent Jacquemard on 20/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{


#ifndef SM_Decorated_hpp
#define SM_Decorated_hpp

#include <stdio.h>

#include "trace.hpp"
#include "Ornament.hpp"
#include "LeafEvent.hpp"
#include "LeafNote.hpp"
#include "LeafChord.hpp"
#include "Inner.hpp"


namespace ScoreModel {


/// An Decorated event is made of
/// - an Ornament containing one or several grace notes,
/// - a single atomic event (Leaf) decorated by the ornament,
///   supposed to be relatively longer-lasting.
/// Its arity is always 2,
/// i.e. the multiplicity of the Leaf event must be 1.
/// The time semantics is that all the grace notes in Ornament
/// have duration 0 and the duration of the Leaf event (resp.
/// start and end dates) is the duration (resp. start and end
/// dates) of this node.
/// The ordering, i.e. the start date of the grace notes in
/// Ornament is defined according to the order of addition of
/// components, as described below in constructor and add
/// functions.
class Decorated : public Inner
{
public:

    /// Pitched event decorated by an ornament,
    /// case of root embedded in the given wrapper.
    /// If the Ornament or the Leaf event are unset,
    /// they must be added later with add().
    /// If only the Ornament or both the Ornament and the Leaf
    /// event are given, it means that the Ornament decoration
    /// will be played first.
    /// @param wrapper wrapper node. This new node will be added
    /// to it.
    /// @param o the decoration. if present, must be allocated.
    /// @param e the decorated event. if present, must be
    /// allocated.
    Decorated(Root& wrapper,
              Ornament* const o=NULL, LeafEvent* const e=NULL);

    /// Pitched event decorated by an ornament,
    /// case of root embedded in the given wrapper.
    /// If the Ornament is unset, it must be added later with
    /// add().
    /// If only the Leaf event or both the Leaf event and the
    /// Ornament are given, it means that the Leaf event
    /// decoration will be played first.
    /// @param wrapper wrapper node. This new node will be added
    /// to it.
    /// @param e the decorated event.
    /// @param o the decoration. if present, must be allocated.
    Decorated(Root& wrapper,
              LeafEvent* const e, Ornament* const o=NULL);

    /// Pitched event decorated by an ornament, non-root case.
    /// If the Ornament or the Leaf event are unset,
    /// they must be added later with add().
    /// If only the Ornament or both the Ornament and the Leaf event
    /// are given, it means that the Ornament decoration will be played first.
    /// @param mult number of (multiple) edges between this node
    /// and its parent. must be strictly positive.
    /// @param parent the parent node. NULL or inner node.
    /// @param o the decoration. if present, must be allocated.
    /// @param e the decorated event. if present, must be allocated.
    Decorated(size_t mult, Inner* parent,
              Ornament* const o=NULL, LeafEvent* const e=NULL);

    /// Pitched event decorated by an ornament, non-root case.
    /// If the Ornament or the Leaf event are unset,
    /// they must be added later with add().
    /// If the Ornament is unset, it must be added later with
    /// add().
    /// If only the Leaf event or both the Leaf event and the
    /// Ornament are given, it means that the Leaf event
    /// decoration will be played first.
    /// @param mult number of (multiple) edges between this node
    /// and its parent. must be strictly positive.
    /// @param parent the parent node. NULL or inner node.
    /// @param e the decorated event.
    /// @param o the decoration. if present, must be allocated.
    Decorated(size_t mult, Inner* parent,
              LeafEvent* const e, Ornament* const o=NULL);

    /// delete this node but not its content (sub-nodes).
    ~Decorated();
        
    /// recursively delete all nodes in the 2 sub-trees of this tuplet node.
    virtual void delete_tree();
    
    virtual size_t arity() const { return 2; }   
    
    /// one or several grace notes (ornament)
    Ornament* const decoration() const { return _ornament; }

    /// decorated event following the ornament
    LeafEvent* const decorated() const { return _leaf; }
    
    /// number of staff of the decoration and event.
    /// @return number of staff where the decorated event (leaf) is located.
    /// @warning the event must have been added.
    int staff() const;
    
    /// add the decoration (Ornament).
    /// If the Leaf event is unset,
    /// it means that the Ornament decoration will be played first,
    /// otherwise is must be played second (after Leaf event).
    /// @param o the decoration. Must be allocated.
    /// @warning if one ornament is already set, it will be deallocated.
    void add(Ornament* o);

    /// add decorated (Leaf) event.
    /// If the Ornament decoration is not set,
    /// it means that the Leaf event will be played first,
    /// otherwise is must be played second (after Ornament).
    /// @param e the decorated event.
    /// Must be allocated, and must be a LeafNote or a LeafChord.
    /// @return whether the node was effectively added.
    /// @warning the Leaf event must be unset at calling.
    virtual bool add(Node* e);

    virtual void addNote(LeafNote* n);
    virtual void addChord(LeafChord* c);

    /// subtree of given index (for compatibility).
    /// warning : works only for i = 1 (event)
    /// because the decoration is not a Node (it is an Ornament).
    /// @param i the index of subtree requested. must be 1.
    /// @return the ith subtree or NULL if i == 0;
    virtual Node* sub(size_t i) const;
    
    /// Current number of edges from this node.
    /// i.e. number of children that have been added times
    /// their respective multiplicities.
    virtual size_t completed() const;
    
    virtual bool complete() const;
    
    virtual size_t size() const;

    /// wether the ornament must be played before the note.
    /// @return true if the Ornament must be played before the Leaf event,
    /// false otherwise.
    /// @warning at least one of the child Ornament or Leaf event
    /// must be set.
    bool ornamentFirst() const;
    
    virtual bool isDecorated() const { return true; }

    virtual void accept(SMVisitor*) const;

    virtual void print(std::ostream& o) const;
    
protected:
    
    /// starting musical date of the next child to be added
    /// @warning this node must not be complete.
    /// @warning the leaf (decorated child) must be set,
    /// i.e. the next child is the ornament (decoration).
    virtual mtu_t start_child() const;

    /// duration of the next child to be added
    /// @warning this node must not be complete.
    /// @warning the leaf (decorated child) must be set,
    /// i.e. the next child is the ornament (decoration).
    virtual mtu_t dur_child() const;
    
    /// replace the node at given index by the given node.
    /// @param i the index of a subnode. must 1 (index of leaf node).
    /// @param n a new node. must be allocated.
    /// must be a LeafNote or a LeafChord.
    /// @return whether the replacement was effectively done.
    /// @warning the former sub-node at position i is not deleted.
    virtual bool replace(size_t i, Node* n);
    
    
private:

    /// one or several grace notes (ornament)
    Ornament* _ornament;

    /// decorated event following the ornament
    LeafEvent* _leaf;
    
    /// whether the ornament is played before the leaf event or after.
    /// This flag decides the date of the notes in nornament:
    /// - 0 start date of leaf event
    /// - 1 leaf date of lead event
    /// - 2 unknown
    /// @warning it is different from the flag Ornament._before
    int _ordering;

    /// @param ornament flag: the ornament subtree is defined
    /// @param event flag: the event subtree is defined
    size_t init_added(bool ornament, bool event);
    
    /// initialization of ordering in first kind of constructors
    int init_ordering(Ornament* const o, Leaf* const e);
    
    /// update the staves member according to the ornament and leaf.
    /// also checks that the leaf is a LeafNote or a LeafChord.
    /// @todo ugly. trye to replace.
    void updateStaves();
};

std::ostream& operator<<(std::ostream& o, const Decorated& d);


} // namespace ScoreModel

#endif /* SM_Decorated_hpp */

/// @}

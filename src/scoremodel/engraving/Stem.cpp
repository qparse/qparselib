//
//  Stem.cpp
//  squanty
//
//  Created by Florent Jacquemard on 03/07/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Stem.hpp"

namespace ScoreModel {


Stem::Stem(Type t):
_type(t)
{ }


Stem::Stem(const Stem& s):
_type(s._type)
{ }


void Stem::print(std::ostream& o) const
{
    switch (_type)
    {
        case Unspecified:
            break;
        case None:
            o << "no stem"; break;
        case Up:
            o << "up stem"; break;
        case Down:
            o << "down stem"; break;
        case Double:
            o << "double stem"; break;
        default:
            ERROR("unexpected stem type {}", _type); break;
    }
}


std::ostream& operator<<(std::ostream& o, const Stem& s)
{
    s.print(o);
    return o;
}


} // namespace ScoreModel




//
//  Ornament.hpp
//  qparse
//
//  Created by Florent Jacquemard on 18/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{


#ifndef SM_Ornament_hpp
#define SM_Ornament_hpp

#include <stdio.h>

#include "SMVisitor.hpp"
#include "GraceNote.hpp"
#include "StaffRange.hpp"
#include "SingleGraceNote.hpp"


namespace ScoreModel {


/// An Ornament is made of one or several grace notes.
/// preeceding or following a main note.
/// Its duration is always 0.
/// This abstract class factorizes several types of ornaments
/// correspondings to different CWM notations.
class Ornament
{
public:

    /// @param staff default staff number for the events in this ornament.
    /// @param before flag true (resp. false) if the ornament is supposed
    /// to be played before (resp. at) the date start.
    Ornament(int staff, bool before=true);

    Ornament(const GraceNote* g1, bool before=true);

    Ornament(const GraceNote* g1, const GraceNote* g2, bool before=true);

    Ornament(const GraceNote* g1, const GraceNote* g2,
             const GraceNote* g3,
             bool before=true);

    Ornament(const GraceNote* g1, const GraceNote* g2,
             const GraceNote* g3, const GraceNote* g4,
             bool before=true);

    Ornament(const Ornament& o);

    /// delete this leaf node
    virtual ~Ornament();

    
    
    // An ornament is treated as a leaf
    //virtual size_t arity() const { return 0; }
    
    /// @return true if the ornament is supposed to occur
    /// before the date start
    /// the sounding of the main note.
    /// Otherwise it occurs on the beat.
    inline bool playBefore() const { return _before; }
    inline bool playAfter() const { return (! _before); }

    /// number of grace notes in this ornament.
    size_t size() const;

    /// no grace notes in this ornament.
    bool empty() const;
    
    const SingleGraceNote* at(size_t i) const;
    
    /// range of staves to which this ornament spans
    inline const StaffRange& staves() const { return _staves; }

    /// recognize particular case of appogiatura
    /// before or after a note with given pitch
    bool isUpMordent(const Pitch& p) const;
    bool isLowMordent(const Pitch& p) const;
    bool isGruppetto(const Pitch& p) const;
    bool isInvGruppetto(const Pitch& p) const;

    /// add a event to this ornament.
    /// @param n associated grace note. must be allocated
    /// @param staff number of staff where the grace note is located.
    ///        if UNDEF_STAFF, the defailt staff number is applied.
    /// @param accident displayed accident or courtesy
    /// @param h shape of the note head.
    /// @param harm whether the note is an natural harmonic
    /// @todo harmonic redundant with note-head?
   void add(const GraceNote* n,
            int staff = UNDEF_STAFF,
            float accident = Pitch::UNDEF_NOTE_ALTERATION,
            const Notehead& h = Notehead::Shape::Oval,
            bool harm = false);

    // not a code
    virtual bool isOrnament() const      { return true; }
    virtual bool isMordent() const       { return false; }
    virtual bool isUpMordent() const     { return false; }
    virtual bool isLowMordent() const    { return false; }
    virtual bool isGruppetto() const     { return false; }
    virtual bool isInvGruppetto() const  { return false; }
    virtual bool isAppogiatura() const   { return false; }
    virtual bool isAcciaccatura() const  { return false; }
    virtual bool isTrill() const         { return false; }

    virtual void accept(SMVisitor*) const;

    void print(std::ostream& o) const;
       
protected:
    /// the ornament is supposed to be played before the date start.
    bool const _before;

    /// default staff number for the events in this ornament.
    int _staff;
    
    /// range of staves to which this ornament spans
    StaffRange _staves;
    
    /// Grace notes of score model making of this ornament.
    std::vector<const SingleGraceNote*> _embedding;
            
    bool isMordent(const Pitch& p, int shift) const;
    bool isGruppetto(const Pitch& p, int shift) const;
    
};

std::ostream& operator<<(std::ostream&, const Ornament&);


} // namespace ScoreModel

#endif /* SM_Ornament_hpp */

/// @}


//
//  Multi.cpp
//  squanty
//
//  Created by Florent Jacquemard on 02/07/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Multi.hpp"

namespace ScoreModel {



Multi::Multi():
_content()    // empty
{ }


Multi::~Multi()
{
    for (const LeafNote* e :_content)
    {
        assert(e);
        if (e->parent() == NULL)
            delete e;
    }
}


size_t Multi::size() const
{
    return _content.size();
}


const LeafNote* Multi::event(size_t i) const
{
    assert(i < size());
    return _content.at(i);
}

void Multi::add(const LeafNote* e)
{
    assert(e);
    _content.push_back(e);
}


void Multi::print(std::ostream& o) const
{
    size_t last = size();
    size_t i = 1;
    o << "[";
    for (const LeafNote* e :_content)
    {
        assert(e);
        o << *e;
        if (i < last) o << ", ";
        i++;
    }
    o << "]";
}


std::ostream& operator<<(std::ostream& o, const Multi& m)
{
    m.print(o);
    return o;
}



} // namespace ScoreModel

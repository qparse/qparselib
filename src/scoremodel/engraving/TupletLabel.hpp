//
//  TupletLabel.hpp
//  squanty
//
//  Created by Florent Jacquemard on 23/09/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{


#ifndef TupletLabel_hpp
#define TupletLabel_hpp


#include <stdio.h>

#include <string>

//#include "Duration.hpp"
#include "MTU.hpp"
#include "SymbLabel.hpp"
#include "MeterSig.hpp"


namespace ScoreModel {


/// @brief label for tuplets
class TupletLabel
{
public:

    /// manual explicit construction of tuplet label n:p (n in the time of p).
    /// @param n value (number of units in n-uplet).
    /// @param p value (number of sibling joined to make the tuplet).
    /// default (empty connnstructor) is undef tuplet label.
    TupletLabel(int n=0, int p=0);
    
    /// automatic construction of tuplet label a:m (a in the time of m)
    /// according to the time signature.
    /// @param a tuplet arity.
    /// @param m tuplet multiplicity.
    /// @param dur the total duration of the tuplet.
    /// @param ts embedding time signature.
    /// @todo TBC
    TupletLabel(size_t a, size_t m,
                mtu_t dur, const MeterSig& ts);

    TupletLabel(const TupletLabel& tl);

    inline size_t arity() const { return _num; }
    inline size_t multiplicity() const { return _intimeof; }

    virtual void print(std::ostream& o) const;
    virtual std::string to_string() const;
    
protected:
    
    /// notation 'num in time of p'
    /// p = 0 and n = 0 are ignored.
    /// if n = 0 then p = 0.
    size_t _num;
    size_t _intimeof;
  
};

std::ostream& operator<<(std::ostream& o, const TupletLabel& d);


} // namespace ScoreModel

#endif /* TupletLabel_hpp */

/// @}

//
//  Line.hpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{


#ifndef Line_hpp
#define Line_hpp

#include <stdio.h>
#include <iostream>

#include "trace.hpp"


namespace ScoreModel {

/// @brief kinds of lines
class Line
{
public:

    enum Type {
        None,
        Solid,
        Dashed,
        Dotted,
        Wavy
    };
};

std::ostream& operator<<(std::ostream&, const Line::Type&);


} // namespace ScoreModel

#endif /* Line_hpp */

/// @} // end group scoremodel

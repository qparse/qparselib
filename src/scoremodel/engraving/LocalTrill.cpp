//
//  LocalTrill.cpp
//  squanty
//
//  Created by Florent Jacquemard on 27/08/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "LocalTrill.hpp"

namespace ScoreModel {

LocalTrill::LocalTrill(const VPosition& vpos, Leaf* link, float width):
Local(vpos, link),
_second(width)
{ }


void LocalTrill::print(std::ostream& o) const
{
    o << "Trill";
    o << "(" << _second << ")";
}


std::ostream& operator<<(std::ostream& o, const LocalTrill& g)
{
    g.print(o);
    return o;
}


} // namespace ScoreModel

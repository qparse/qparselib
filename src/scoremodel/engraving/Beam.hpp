/**
 * Representation and management of beams
 *
 * @author Philippe Rigaux
 */

#ifndef Beam_hpp
#define Beam_hpp

#include <stdio.h>

#include "CMND.hpp"
#include "Sequence.hpp"

namespace ScoreModel {

/// @brief information on beaming for labeling Tuplet inner nodes
class Beam
{

public:
  
    /// label for the Tuplet inner nodes.
    /// The label of the innermost common ancestor of two successive leaves (notes or chords)
    /// determines the number of beams between them, according to the rules in the definition of Beam::Info.
    /// @todo revise for unicity definition
    enum Info {
        /// no beam. must be propagated upward .
        None,
        /// as much beams as possible: min(leaf1, leaf2).
        Full,
        /// one less than Full: min (min(leaf1, leaf2) - 1, 1)
        Minus,
        /// one beam exactly
        One,
        /// depth of least-common-ancestor + 1.
        /// @todo revise according to [Gould 2011]
        Depth,
        /// Unknown (error)
        Undef
    };
    
    Beam(Info i);
    Beam(const Beam& b);

    inline Info info() const { return _info; }

    void print(std::ostream& o) const;
    
    /// number of beams between 2 events for a given beaming policy
    /// @param nf1 number of flags or beams of the first event
    /// @param nf2 number of flags or beams of the second event
    /// @param pol beaming policy. must not be Undef.
    /// @param depth the depth (number of flags corresponding to the duration) for the
    /// inner node labeled by `pol`.
    static unsigned long interEventBeams(unsigned long nf1,
                                         unsigned long nf2,
                                         Info pol = Full,
                                         unsigned long depth = 0);
        
    /// @todo STUB TBR
    // Beam(Sequence events);
    
private:

    Info _info;
    
};

std::ostream& operator<<(std::ostream& o, const Beam& s);


} // namespace ScoreModel

#endif /* Beam_hpp */

//
//  Root.hpp
//  squanty
//
//  Created by Florent Jacquemard on 03/07/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//


#ifndef Root_hpp
#define Root_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>

#include "Tuplet.hpp"
#include "MeterSig.hpp"
#include "KeySig.hpp"
#include "Clef.hpp"
#include "Voice.hpp"



namespace ScoreModel {

class Leaf;

/// Wrapper for the root node of an engraving tree.
/// It contains information about the embedding measure (Sequence)
/// and its start time and duration.
class Root : public Inner
{
public:
    
    /// root wrapper without content (wrapped root node unset).
    /// @param start date (MTU) of the leftmost leaf of the tree.
    /// @param dur cumulated duration (MTU) of the leaves in the tree.
    /// @param container musical score model element (voice in a measure)
    ///        containing this tree.
    Root(mtu_t start, mtu_t dur, Sequence& container);
    
    /// root wrapper with given wrapped root node.
    /// @param start date (MTU) of the leftmost leaf of the tree.
    /// @param dur cumulated duration (MTU) of the leaves in the tree.
    /// @param container musical score model element (voice in a measure)
    ///        containing this tree.
    /// @param n wrapped root node. must be allocated.
    Root(mtu_t start, mtu_t dur, Sequence& container, Node* n);

    /// delete this root node but not its content (sub-nodes).
    virtual ~Root();
    
    /// recursively delete all nodes in the sub-tree of this root node.
    virtual void delete_tree();
    
    // useful ?
    // virtual Voice& voice() const;
    // virtual Part& part() const;
    
    /// voice number for the data in the tree rooted by this node
    /// @todo useful?
    inline int voice() const { return _voice; }

    /// wrapped root node
    inline Node* node() const { return _content; }
    
    /// number of children added, at most 1;
    virtual size_t size() const;
    
    /// subtree of given index.
    /// @param i index of subtree. must be 0.
    /// @return the ith (and only) subtree.
    virtual Node* sub(size_t i) const;

    /// set wrapped root node
    /// @param t wrapped node. Its multiplicity must be 1.
    /// @return whether the node was effectively added.
    /// @warning can be called only once.
    virtual bool add(Node* t);
        
    /// Current number of edges from this node.
    /// i.e. number of children that have been added times
    /// their respective multiplicities.
    virtual size_t completed() const;

    /// @param n rightmost innermost leaf of the subtree (content).
    /// Must be allocated.
    void setLastLeaf(Leaf* n);
    
    /// @return the node set by setLastLeaf
    inline Leaf* getLastLeaf() const { return _lastLeaf; }

    virtual bool isRoot() const { return true; }

    
    // const MeterSig& meterSignature() const;
    // const KeySig& keySignature() const;
    // clef at the beginning of the measure corresponding to this root.
    // const Clef& clef() const;

    virtual void accept(SMVisitor*) const;
    
    virtual void print(std::ostream& o) const;

protected:
    
    /// starting musical date of the next child to be added
    /// @warning the real root node must not be set in this wrapper.
    virtual mtu_t start_child() const;

    /// duration of the next child to be added
    /// @warning the real root node must not be set in this wrapper.
    virtual mtu_t dur_child() const;
    
    /// replace the node at given index by the given node.
    /// @param i the index of a subnode. must be 0.
    /// @param n a new node. must be allocated.
    /// @return whether the replacement was effectively done.
    /// @warning the former sub-node at position i is not deleted.
    virtual bool replace(size_t i, Node* n);

private:
    
    /// voice number for the data in the tree rooted by this node
    int _voice;
    
    /// key signature for the measure corresponding to this root.
    
    /// encapsulated real root node of the tree
    Node* _content;

    /// rightmost innermost leaf. for convenience.
    Leaf* _lastLeaf;
    
    // Sequence containing this tree is in Node

};

std::ostream& operator<<(std::ostream&, const Root&);

} // namespace ScoreModel
/// @}
#endif /* Root_hpp */


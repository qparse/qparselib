//
//  Node.hpp
//  qparse
//
//  Created by Florent Jacquemard on 19/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef SM_Node_hpp
#define SM_Node_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>
#include <exception>

#include "MTU.hpp"
#include "CMND.hpp"
#include "Measure.hpp"
#include "Sequence.hpp"
#include "Event.hpp"
#include "SMVisitor.hpp"

namespace ScoreModel {

class tree_exception: public std::exception
{
public:
  virtual const char* what() const throw()
  {
      return "Structure error in engraving tree";
  }
};

class Root;
class Inner;
class MeterSig;

/// abstract class for a generic type of node
/// in a tree representation of engraving information (engraving tree).
class Node : public Event
{
public:
    
    /// construct a new root node, embedded in the given wrapper.
    /// @param ar arity of the new node.
    /// @param wrapper wrapper node. This new node will be added to it.
    /// @warning the wrapper must not already contain a node.
    /// The new node will be aded to it.
    Node(size_t ar, Root& wrapper);

    /// construct a node with a parent.
    /// @param ar arity of the node.
    /// @param mult number of (multiple) edges between this node
    /// and its parent. must be strictly positive.
    /// @param parent the parent node. must be an allocated inner node.
    Node(size_t ar, size_t mult, Inner* parent);
    
    Node(const Node& n);

    /// @warning the content of the node (subtrees) is not deleted,
    /// Neither is its parent node.
    /// To recursively delete the sub-trees, use delete_tree.
    virtual ~Node();

    /// delete all the content of this node,
    /// i.e. recursively delete all nodes in its sub-trees.
    /// by default does nothing.
    virtual void delete_tree() = 0;
    
    /// not used.
    virtual std::string name() const { return std::string("_node_"); }

    /// the arity of this node is the number of edges from it
    /// to its children (out-degree, counting multiplicities).
    virtual size_t arity() const { return _outdegree; }
    
    /// the number of children nodes,
    /// without counting multiplicities.
    /// @warning can be partial in case of uncomplete Inner node.
    virtual size_t size() const = 0;
    
    /// if the arity is > 0, all the expected children have been added.
    /// @warning a child of multiplicity n counts for n children.
    virtual bool complete() const = 0;
    
    /// subtree of given index.
    /// @param i the index of subtree requested.
    /// must be smaller than size().
    /// @return the ith subtree.
    /// @throw tree_error when i is an index of subnode of this node.
    virtual Node* sub(size_t i) const = 0;

    /// add s subtree under this node.
    /// @param t root of the subtree to add.
    /// @return whether the node was effectively added.
    /// @warning this node must not be complete
    virtual bool add(Node* t) = 0;

    /// the multiplicity of this node is the number of edges from
    /// its parent to it (in-degree).
    inline size_t multiplicity() const { return _indegree; }
    void incrMultiplicity(size_t m = 1);
    void setMultiplicity(size_t m);
    
    /// number of the measure to which the node belongs
    long measureNb() const;
            
    // duration of the note in CMN.
    // It is computed from the MTU duration and the meter of the measure.
    // cmnd_t CMNduration() const;

    // Number of flags, or beams, associated to this node in CMN.
    // For a leaf node, it is the number of flags, or beams, on the stem of the written element (note etc).
    // For an inner note,  it is the number of beams between two events (following the beatween policy).
    // virtual unsigned long nbFlags() const = 0;
    
    /// Container in the musical score model.
    inline Sequence& sequence() const { return _container; }

    /// Container of the container in the musical score model.
    inline Measure& measure() const { return _container.measure(); }

    /// @todo useful?
    // virtual Voice& voice() const; // or voice nb?
    // virtual Part& part() const;

    /// parent of this node (backlink)
    /// @return the parent node in tree or NULL if this node is the root node.
    inline Inner* parent() const { return _parent; }
    inline void setParent(Inner* p) { assert(p); _parent = p; }
    
    /// previous sibling of this node
    /// @return the previous sibling node in tree or
    /// NULL if this node is a first sibling (including the case of root).
    /// @warning this node and its parent must be complete.
    /// @throw tree_error when this node is not a subnode of its parent.
    Node* prevSibling() const;

    /// index of this node in its parent node.
    /// @warning this node must have a non NULL parent.
    /// @throw tree_error when this node is not a subnode of its parent.
    size_t index() const;

    /// Root node of the tree this node belongs to.
    /// @return the root (following the parent links).
    const Node* root() const;

    /// replace this node by the given node in the embedding tree.
    /// @param n a new node. must be allocated.
    /// @return whether the replacement was effectively done.
    /// This node can be deleted in this case.
    /// @warning this node should not be a Root.
    // @throw tree_error when i is an index of subnode of this node.
    virtual bool replace(Node* n) const;
    
    /// Time signature for the container measure.
    inline const MeterSig* meterSignature() const
    { return _container.measure().meterSignature();  }
    
    /// Key signature for the container measure.
    inline const KeySig* keySignature() const
    { return _container.measure().keySignature();  }
    
    inline int staff() const { assert(_staves.singleton()); return _staves.staff(); }
    inline bool singleStaff() const { return _staves.singleton(); }
    inline bool nbStaves() const { return _staves.size(); }
    inline int minStaff() const { return _staves.min(); }
    inline int maxStaff() const { return _staves.max(); }
    inline const StaffRange& staves() const { return _staves; }

    
    virtual bool isNode() const         { return true; }
    virtual bool isInner() const        { return false; }
    virtual bool isLeaf() const         { return false; }
    virtual bool isRoot() const         { return false; }
    virtual bool isTuplet() const       { return false; }
    virtual bool isContinuation() const { return false; }
    virtual bool isTie() const          { return false; }
    virtual bool isDot() const          { return false; }
    virtual bool isDecorated() const    { return false; }
    virtual bool isLeafEvent() const    { return false; }
    virtual bool isNote() const         { return false; }
    virtual bool isRest() const         { return false; }
    virtual bool isHarmonic() const     { return false; }
    virtual bool isSpace() const        { return false; }
    virtual bool isChord() const        { return false; }
  //virtual bool isOrnament() const     { return false; }
    virtual bool isAppogiatura() const  { return false; }
  
    virtual void accept(SMVisitor*) const = 0;
    
    virtual void print(std::ostream& o) const;
    virtual std::string to_string() const;
   
protected:
      
    /// number of outgoing edges, counting multiple edges
    /// (arity of this node)
    size_t _outdegree;
        
    /// number of incoming edges from parent
    /// (multiplicity of this node)
    size_t _indegree;
    
    /// range of staves where the content of this node will be printed.
    StaffRange _staves;
    
    Inner* _parent;
    
    Sequence& _container;

    // std::string path;

    /// construct a root node, contained in the given measure.
    /// @param start date (MTU) of the leftmost leaf under this node.
    /// @param dur cumulated duration (MTU) of the leaves under this node.
    /// @param ar arity of the node.
    /// @param container musical score model element (voice in a measure)
    ///        containing this tree.
    Node(mtu_t start, mtu_t dur,
         size_t ar,
         Sequence& container);
   
};


std::ostream& operator<<(std::ostream&, const Node&);


} // namespace ScoreModel

/// @}
#endif /* SM_Node_hpp */

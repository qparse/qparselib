//
//  Union.cpp
//  squanty
//
//  Created by Florent Jacquemard on 28/08/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Union.hpp"


namespace ScoreModel {


Union::Union(const Sequence* seq, mtu_t st, mtu_t dur):
container(seq),
start(st),
duration(dur),
parent(NULL)
{ assert(seq); }


Union::Union(const Inner* p):
container(NULL),
start(0),
duration(0),
parent(p)
{ assert(p); }


bool Union::root() const
{
    assert((container != NULL) || (parent == NULL));
    assert((parent != NULL) || (container == NULL));
    return (parent == NULL);
}



} // namespace ScoreModel




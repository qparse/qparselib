//
//  Vibrato.hpp
//  squanty
//
//  Created by Florent Jacquemard on 02/09/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#ifndef Vibrato_hpp
#define Vibrato_hpp

#include <stdio.h>

#include "Spanning.hpp"

namespace ScoreModel {

/// @brief performance directive about various kind of oscillations
/// including Vibrato and Spanning Trills
class Vibrato : public Spanning
{
public:
    
    enum Kind {
        Trill,
        Prall,
        Upprall,
        Downprall,
        Vibr
    };
    
    
    /// @param vpos vertical position.
    /// @param hp1 starting horizontal position.
    /// @param hp2 ending horizontal position.
    Vibrato(const VPosition& vpos,
            const HPosition& hp1, const HPosition& hp2,
            Kind k);

    virtual bool isVibrato() const { return true; }
    
    inline Kind kind() const { return _kind; }

    virtual void print(std::ostream& o) const;

private:
    
    Kind _kind;

};

} // namespace ScoreModel

#endif /* Vibrato_hpp */

/// @} // end group scoremodel

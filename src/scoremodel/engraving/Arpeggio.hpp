//
//  Arpeggio.hpp
//  squanty
//
//  Created by Florent Jacquemard on 02/07/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{


#ifndef Arpeggio_hpp
#define Arpeggio_hpp

#include <stdio.h>

#include "trace.hpp"
#include "Line.hpp"
#include "Local.hpp"


namespace ScoreModel {

/// @brief Representation of arpegio marks for chords.
class Arpeggio
{

public:
    
enum Arrow {
    None,
    Up,
    Down
};
    
    Arpeggio(Line::Type linetype=Line::None,
             Arrow arrowtype=None,
             bool bracket=false);
    
    Arpeggio& operator=(const Arpeggio& rhs);

    bool operator==(const Arpeggio& rhs) const;
    
    bool arpegiate() const { return (_bracket ||
                                     (_linetype == Line::Type::None)); }

    void print(std::ostream& o) const;
    
private:
    
    /// only None, Wavy or Solid.
    Line::Type _linetype;
    
    /// arrow to indicate bottom-up or top-down arpeggio.
    Arrow _arrowtype;

    /// brackets on chord to indicate explicitely to not arpeggiate.
    bool _bracket;
        
};

std::ostream& operator<<(std::ostream&, const Arpeggio&);




} // end namespace ScoreModel

#endif /* Arpeggio_hpp */

/// @} // end group scoremodel

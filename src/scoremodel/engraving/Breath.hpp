//
//  Breath.hpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#ifndef Breath_hpp
#define Breath_hpp

#include <stdio.h>

#include "Local.hpp"

namespace ScoreModel {

/// @brief Breath marks
class Breath : public Local
{
public:

    enum Kind {
        Comma,
        Tick,
        Saltzed,
        UpBow,
        Caesura,
        CurvedCaesura,
        ShortCaesura,
        ThickCaesura,
    };
        
    Breath(const VPosition& vpos, Leaf* link, Kind mark);
    Breath(const VPosition& vpos, mtu_t date, const Part& p, Kind mark);

    Kind mark() const { return _mark; }
   
    virtual bool isBreath() const { return true; }

    virtual void print(std::ostream& o) const;
 
private:
    
    Kind _mark;

};

std::ostream& operator<<(std::ostream&, const Breath&);

} // namespace ScoreModel

#endif /* Breath_hpp */

/// @} // end group scoremodel

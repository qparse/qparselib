//
//  Notehead.hpp
//  squanty
//
//  Created by Florent Jacquemard on 03/07/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{


#ifndef Notehead_hpp
#define Notehead_hpp

#include <stdio.h>
#include <iostream>
#include <string>

#include "trace.hpp"

namespace ScoreModel {

// @brief Symbols for note heads
class Notehead
{
public:

    /// see http://usermanuals.musicxml.com/MusicXML/MusicXML.htm#ST-MusicXML-notehead-value.htm
    enum Shape {
        None,
        Oval,        /// default (Normal)
        Slash,
        Triangle,    /// tip of the triangle pointing up
        Diamond,
        Square,
        Cross,
        X,
        Circle_x,
        Inverted_triangle, /// tip of the triangle pointing down
        Arrow_down,  /// centered on the stem
        Arrow_up,    /// centered on the stem
        Slashed,
        Back_slashed,
        Cluster,
        Circle_dot,
        Left_triangle,  ///  right triangle with the hypotenuse facing up and to the left
        Rectangle,
        Do,       /// Aikin's 7-shape system
        Re,
        Mi,
        Fa,       /// used with downstems or no stems
        Fa_up,    /// used with upstems
        So,
        La,
        Ti,
    };

    Notehead(bool filled=true,
             Shape s=Oval,
             bool par=false,
             std::string text="");
    
    Notehead(const Notehead&);

    inline Shape shape() const { return _shape; }
    inline bool filled() const { return _filled; }
    inline bool parenthesized() const { return _parenthesized; }

    bool equal(const Notehead&) const;

    virtual void print(std::ostream& o) const;
       
private:

    Shape _shape;
    
    /// can be set to change the hollow/filled appearance of a notehead.
    bool _filled;
    
    /// the notehead is parenthesized.
    bool _parenthesized;
    
    std::string _text;  /// text that is displayed inside a notehead
    
};

inline bool operator==(const Notehead& lhs, const Notehead& rhs)
{
    return lhs.equal(rhs);
}

inline bool operator!=(const Notehead& lhs, const Notehead& rhs)
{
    return !operator==(lhs, rhs);
}

std::ostream& operator<<(std::ostream& o, const Notehead& h);


} // namespace ScoreModel

#endif /* Notehead_hpp */

/// @}


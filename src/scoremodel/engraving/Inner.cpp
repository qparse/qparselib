//
//  Inner.cpp
//  squanty
//
//  Created by Florent Jacquemard on 28/08/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Inner.hpp"




namespace ScoreModel {


Inner::Inner(size_t ar, Root& wrapper):
Node(ar, wrapper)
{ }


Inner::Inner(size_t ar, size_t mult,
             Inner* parent):
Node(ar, mult, parent)
{ }

 
Inner::Inner(mtu_t start, mtu_t dur,
             size_t ar,
             Sequence& container):
Node(start, dur, ar, container)
{ }

Inner::Inner(const Inner& n):
Node(n)
{ }

Inner::~Inner()
{
    TRACE("delete Inner");
}


bool Inner::complete() const
{
    assert(completed() <= arity());
    return (completed() == arity()); ;
}


//unsigned long Inner::nbFlags() const
//{
//    const MeterSig* ts = meterSignature();
//    assert(ts);
//    cmnd_t d = ts->MTUtoCMN(dur_child());     // CMNduration
//    assert(CMN::defined(d));
//
//    unsigned long nb = CMN::to_flags(d);
//    assert(0 <= nb);
//    assert(nb <= CMN::MAX_FLAGS);
//    return nb;
//}


void Inner::print(std::ostream& o) const
{
    ((Node*) this)->print(o);
    if (!complete())
    {
        o << "uncomplete (" << completed() << "/" << arity() << ")";
    }    
}


std::ostream& operator<<(std::ostream& o, const Inner& n)
{
    n.print(o);
    return o;
}


} // namespace ScoreModel

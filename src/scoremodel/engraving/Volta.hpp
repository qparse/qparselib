//
//  Volta.hpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#ifndef Volta_hpp
#define Volta_hpp

#include <stdio.h>
#include <assert.h>

#include "Spanning.hpp"

namespace ScoreModel {

/// @brief delimiter of different endings for repeat
class Volta : public Spanning
{
public:

    /// @param vpos vertical position.
    /// @param hp1 starting horizontal position.
    /// @param hp2 ending horizontal position.
    /// @param number 1 or 2 or 3
    /// @param open can be true only for 2
    Volta(const VPosition& vpos, const HPosition& hp1, const HPosition& hp2,
          int number, bool open);

    /// @param vpos vertical position.
    /// @param hp1 starting horizontal position.
    /// @param hp2 ending horizontal position.
    /// @param number 1 prima volta, closed
    ///               2 seconda volta, open
    ///               3 terza volta, closed
    Volta(const VPosition& vpos, const HPosition& hp1, const HPosition& hp2,
          int number);

    virtual bool isVolta() const { return false; }
    
    int number() const { return _nb; }

    bool open() const { return _open; }
    bool closed() const { return !open(); }

    virtual void print(std::ostream& o) const;

private:
    
    /// 1 or 2 or 3
    int _nb;

    /// always false for 1 and 3.
    bool _open;

};


} // namespace ScoreModel

#endif /* Volta_hpp */

/// @} // end group scoremodel

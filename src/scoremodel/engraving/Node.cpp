//
//  Node.cpp
//  qparse
//
//  Created by Florent Jacquemard on 19/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Node.hpp"
#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream

#include "Voice.hpp"
#include "Inner.hpp"
#include "Root.hpp"

namespace ScoreModel {

tree_exception tree_error;

/// = Node(ar, 1, Root), useless
Node::Node(size_t ar, Root& wrapper):
Event(wrapper.start(), wrapper.duration()),
_outdegree(ar),
_indegree(1),
_staves(), // empty
_parent(&wrapper),
_container(wrapper.sequence())
{
    assert(! wrapper.complete());
    wrapper.add(this);
}


Node::Node(size_t ar, size_t mult, Inner* parent):
Event(parent->start_child(), (parent->dur_child()) * mult),
_outdegree(ar),
_indegree(mult),
_staves(),
_parent(parent),
_container(parent->sequence())
{
    // TRACE("new Node mult={}, ar={}, dur={}", multiplicity(), arity(), duration());
    assert(parent);
    assert(parent->isInner());
    assert(! parent->complete());
    assert((! parent->isRoot()) || (mult == 1));
    assert(mult > 0);
    assert(parent->completed() + mult <= parent->arity());
}


/// @todo deprecated
Node::Node(mtu_t start, mtu_t dur,
           size_t ar,
           Sequence& container):
Event(start, dur),
_outdegree(ar),
_indegree(0),
_staves(),
_parent(NULL),
_container(container)
{ }


Node::Node(const Node& n):
Event(n),
_outdegree(n._outdegree),
_indegree(n._indegree),
_staves(n.staves()),
_parent(n._parent), // no clone
_container(n._container) // ref
{ }

Node::~Node()
{
    // TRACE("delete Node");
}


void Node::incrMultiplicity(size_t m)
{
    _indegree += m;
}


void Node::setMultiplicity(size_t m)
{
    assert(m > 0);
    _indegree = m;
}


long Node::measureNb() const
{
    long m = MTU::measure(this->date());
    assert(m == _container.measureNb());
    return m;
}


// cmnd_t Node::CMNduration() const
// {
//     const MeterSig* ts = meterSignature();
//     assert(ts);
//     return ts->MTUtoCMN(duration());
// }


Node* Node::prevSibling() const
{
    // root is a first sibling
    if (_parent == NULL)
        return NULL;

//    size_t len = _parent->size();
//    assert(len > 0);
//    for (size_t i = 0; i < len; ++i)
//    {
//        if (_parent->sub(i) == this)
//        {
//            if (i == 0)
//                return NULL;
//            else
//                return _parent->sub(i-1);
//        }
//    }
//    ERROR("Node not found it its parent's subs: {}", *this);
//    throw tree_error;

    try
    {
         size_t i = this->index();
        if (i == 0)
            return NULL;
        else
            return _parent->sub(i-1);
    }
    catch(tree_exception& e)
    {
        // not found: should not happen
        ERROR("prevSibling: {}: Node not found it its parent's subs: {}",
              e.what(), *this);
        return NULL;
    }
}


size_t Node::index() const
{
    if (_parent == NULL)
    {
        ERROR("Node.index called on root node");
        throw tree_error;
    }

    for (size_t i = 0; i < _parent->size(); ++i)
    {
        // pointer comparison
        if (_parent->sub(i) == this)
            return i;
    }
    // not found. should not happen.
    ERROR("Node not found it its parent's subs: {}", *this);
    throw tree_error;
    // return size()+9;
    /// @todo exception
}


bool Node::replace(Node* n) const
{
    // should not happen (one should not replace Root nodes)
    if (_parent == NULL)
    {
        WARN("Node.replace: replacement at root");
        return true; // n is the replacement in this case
    }

    try
    {
        size_t i = this->index();
        if (_parent->replace(i, n) == false)
        {
            ERROR("replacement fail.");
            return false;
        }
        else
            return true;
    }
    catch(tree_exception& e) // thrown by index()
    {
        // not found: should not happen
        ERROR("replace fail: {}: Node not found it its parent's subs: {}",
              e.what(), *this);
        return false;
    }
}


const Node* Node::root() const
{
    const Node* p0 = this;
    const Node* p1 = _parent;
    while (p1 != NULL)
    {
        p0 = p1;
        assert(p0);
        p1 = p0->parent();
    }
    // assert(p0->isRoot());
    // const Root* p = dynamic_cast<const Root*>(p0);
    // assert(p);
    return p0;
    // return (Root*) p0;
}


void Node::print(std::ostream& o) const
{
    o << "Node: " << id();
    o << " arity=" << _outdegree;
    o << " multiplicity=" << _indegree;
    o << " onset=" << MTU::to_string(_onset);
    o << " dur=" << _duration;
}


std::string Node::to_string() const
{
    std::stringstream s;
    this->print(s); // s << *(this);
    return s.str();
}


std::ostream& operator<<(std::ostream& o, const Node& n)
{
    n.print(o);
    return o;
}


} // namespace ScoreModel

//
//  VolChange.hpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#ifndef VolChange_hpp
#define VolChange_hpp

#include <stdio.h>

#include "trace.hpp"
#include "Spanning.hpp"
#include "Line.hpp"

namespace ScoreModel {

/// @brief Change in volume 
class VolChange : public Spanning
{
public:
    
    enum Shape {
        Hairpin,   // angle lines
        Text       // cresc. or dim.
    };

    /// @param vpos vertical position.
    /// @param hp1 starting horizontal position.
    /// @param hp2 ending horizontal position.
    /// @param shape hairpin or text.
    /// @param incr true for cresc.; false for dim.
    /// @param type for hairpin lines or after text
    VolChange(const VPosition& vpos,
              const HPosition& hp1, const HPosition& hp2,
              Shape shape, bool incr, Line::Type type);

    /// line type is solid for hairpin
    /// and dotted for text
    VolChange(const VPosition& vpos,
              const HPosition& hp1, const HPosition& hp2,
              Shape shape, bool incr);

    virtual bool isVolChange() const { return true; }

    bool increase() const { return _cresc; }
    
    Shape shape() const { return _shape; }

    Line::Type linetype() const { return _linetype; }
    
    virtual void print(std::ostream& o) const;

private:
    Shape _shape;

    /// true for cresc.; false for dim.
    bool _cresc;
    
    Line::Type _linetype;

    virtual void print_cresc(std::ostream& o) const;

};

} // namespace ScoreModel

#endif /* VolChange_hpp */

/// @} // end group scoremodel

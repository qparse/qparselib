//
//  Spanning.hpp
//  squanty
//
//  Created by Florent Jacquemard on 24/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{


#ifndef Spanning_hpp
#define Spanning_hpp

#include <stdio.h>

#include "Local.hpp"


namespace ScoreModel {

/// Annotation giving directive on performances spanning between two positions.
class Spanning : public Local
{
public:

    /// annotation localized between two positions.
    /// @param vpos vertical position of the annotation.
    /// @param hp1 starting horizontal position for the annotation.
    /// @param hp2 ending horizontal position for the annotation.
    Spanning(const VPosition& vpos,
             const HPosition& hp1, const HPosition& hp2);

    
    /// annotation localized between two events.
    /// @param vpos vertical position of the annotation.
    /// @param hp1 event defining the starting horizontal position for the annotation.
    /// @param hp2 event defining the ending horizontal position for the annotation.
    Spanning(const VPosition& vpos, Leaf* hp1, Leaf* hp2);

    /// annotation localized between one event and one date.
    /// @param vpos vertical position of the annotation.
    /// @param hp1 event defining the starting horizontal position for the annotation.
    /// @param hp2 musical date defining the ending horizontal position for the annotation.
    Spanning(const VPosition& vpos, Leaf* hp1, mtu_t hp2, const Part& p);

    /// annotation localized between one date and one event.
    /// @param vpos vertical position of the annotation.
    /// @param hp1 musical date defining the starting horizontal position for the annotation.
    /// @param hp2 event defining the ending horizontal position for the annotation.
    Spanning(const VPosition& vpos, mtu_t hp1, const Part& p, Leaf* hp2);

    /// annotation localized between two dates.
    /// @param vpos vertical position of the annotation.
    /// @param hp1 musical date defining the starting horizontal position for the annotation.
    /// @param hp2 musical date defining the ending horizontal position for the annotation.
    Spanning(const VPosition& vpos, mtu_t hp1, const Part& p, mtu_t hp2);

    
    inline const HPosition& hposition2() const { return _hposend; }
       
    virtual void print(std::ostream& o) const { }

private:

    /// end of spanning direction
    HPosition _hposend;
    
};


std::ostream& operator<<(std::ostream&, const Spanning&);


} // namespace ScoreModel

#endif /* Spanning_hpp */

/// @}

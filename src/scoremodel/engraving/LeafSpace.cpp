//
//  LeafSpace.cpp
//  squant2
//
//  Created by Florent Jacquemard on 21/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "LeafSpace.hpp"


namespace ScoreModel {

LeafSpace::LeafSpace(Root& wrapper,
                     int staff,
                     Leaf* previous, Leaf* next,
                     AtomicEvent* e):
LeafEvent(wrapper, previous, next, e) 
{
    assert((e == NULL) || (e->isRest()));
    assert(staff >= 0);
    assert(_staves.empty());
    _staves.add(staff);  // _container.addStaff(staff);
}


LeafSpace::LeafSpace(size_t mult,
                     Inner* parent,
                     int staff,
                     Leaf* previous, Leaf* next,
                     AtomicEvent* e):
LeafEvent(mult, parent, previous, next, e) 
{
    assert((e == NULL) || (e->isRest()));
    assert(staff >= 0);
    assert(_staves.empty());
    _staves.add(staff);  // _container.addStaff(staff);
}


void LeafSpace::accept(SMVisitor* v) const
{
    v->visitLeafSpace(this);
}


void LeafSpace::print(std::ostream& o) const
{
    o << "Space" << duration();
    ((Leaf*) this)->print(o);
}

std::ostream& operator<<(std::ostream& o, const LeafSpace& e)
{
    e.print(o);
    return o;
}

} // namespace ScoreModel


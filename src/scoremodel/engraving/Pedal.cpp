//
//  Pedal.cpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Pedal.hpp"


namespace ScoreModel {


Pedal::Pedal(const VPosition& vpos,
             const HPosition& hp1, const HPosition& hp2,
             Type t, bool line):
Spanning(vpos, hp1, hp2),
_type(t),
_line(line)
{ }


void Pedal::print(std::ostream& o) const
{
    o << "Ped ";
    switch (_type)
    {
        case Start:
            o << "Start";
            break;
        case Stop:
            o << "Stop";
            break;
        case Change:
            o << "Change";
            break;
        case Continue:
            o << "Continue";
            break;
        default:
            ERROR("Ped: unexpected value {}", _type);
            break;
    }
    if (! _line)
        o << "...*";
}


} // namespace ScoreModel

//
//  Articulation.hpp
//  squanty
//
//  Created by Florent Jacquemard on 05/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{


#ifndef Articulation_hpp
#define Articulation_hpp

#include <stdio.h>

#include "trace.hpp"
#include "VPosition.hpp"
#include "Local.hpp"

namespace ScoreModel {

class Leaf;


/// @brief Representation of articulations and accents
/// @see https://usermanuals.musicxml.com/MusicXML/Content/EL-MusicXML-articulations.htm
class Articulation : public Local
{

public:
    
enum Kind {
    None,
    Accent,          // regular horizontal accent mark
    DetachedLegato,  // combination of a tenuto line and staccato dot symbol.
    Spiccato,        // stroke articulation
    Staccatissimo,   // wedge articulation
    Staccato,        // dot articulation
    Stress,
    StrongAccent,    // vertical accent mark
    Tenuto,          // tenuto line
    Unstress,
    UpBow,
    DownBow
};
    
    Articulation(const VPosition& vpos, Leaf* link, Kind kind);
    
    inline Kind kind() const { return _kind; } 
    
    Articulation& operator=(const Articulation& rhs);

    bool operator==(const Articulation& rhs) const;
    
    virtual bool isArticulation() const { return true; }

    void print(std::ostream& o) const;
    
private:

    Kind _kind;
    
    /// @return the character for this articulation symbol
    char sign() const;
    
};

std::ostream& operator<<(std::ostream&, const Articulation&);


//bool operator==(const Clef& lhs, const Clef& rhs);


} // end namespace ScoreModel

#endif /* Articulation_hpp */

/// @} // end group scoremodel

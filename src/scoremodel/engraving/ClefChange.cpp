//
//  ClefChange.cpp
//  squanty
//
//  Created by Florent Jacquemard on 01/09/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "ClefChange.hpp"

namespace ScoreModel {


ClefChange::ClefChange(const VPosition& vpos, Leaf* link, const Clef& c):
Local(vpos, link),
_clef(c)
{ }


ClefChange::ClefChange(const VPosition& vpos,
                       mtu_t date, const Part& p,
                       const Clef& c):
Local(vpos, date, p),
_clef(c)
{ }


void ClefChange::print(std::ostream& o) const
{
    o << "Clef change: ";
    o << _clef;
}


std::ostream& operator<<(std::ostream& o, const ClefChange& cc)
{
    cc.print(o);
    return o;
}

} // namespace ScoreModel

//
//  Union.hpp
//  squanty
//
//  Created by Florent Jacquemard on 28/08/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef Union_hpp
#define Union_hpp

#include <assert.h>

#include "MTU.hpp"


namespace ScoreModel {

class Sequence;
class Inner;


/// @brief parent of a node in a tree.
/// It can be either the perent node for a non-root node
/// or the container of the tree for the root node.
class Union
{
public:
  
    Union(const Sequence* seq, mtu_t st, mtu_t dur);

    Union(const Inner* p);

    const Sequence* container;
    mtu_t start;
    mtu_t duration;
    
    const Inner* parent;

    bool root() const;
    
};

} // namespace ScoreModel



#endif /* Union_hpp */

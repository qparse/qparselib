//
//  Appogiatura.cpp
//  qparse
//
//  Created by Florent Jacquemard on 18/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Appogiatura.hpp"

namespace ScoreModel {

Appogiatura::Appogiatura(bool b, mtu_t d, bool stroke):
Ornament(b),
_stroke(stroke),
_printed_dur(d),
_printed_content(NULL)
{ }


Appogiatura::Appogiatura(const Ornament& o, mtu_t d, bool stroke):
Ornament(o),
_stroke(stroke),
_printed_dur(d),
_printed_content(NULL)
{ }


Appogiatura::~Appogiatura()
{
    TRACE("delete Appogiatura (size={})", size());
    if (_printed_content)
        delete _printed_content;
}


bool Appogiatura::isGraceNote() const
{
    assert(_printed_content);
    assert(_printed_content->node());
    return
        (_printed_content->node()->isNote()
         && _printed_dur == MTU::make(0,1,2)
         && isAcciaccatura());
}


cmnd_t Appogiatura::printedDuration() const
{
    // mtu_t _printed_dur is the duration in fraction of quarter notes
    // convert into duration in number of whole notes
    // see MeterSig::MTUtoWhole
    assert(_printed_dur > 0);
    Rational dw = MTU::to_Rational(_printed_dur) * Rational(1, 4);
    // see MeterSig::MTUtoCMN
    // must be shorter than whole note!
    assert(Rational(0) <= dw);
    assert(dw < Rational(1));
    long idw = dw.denominator() /  dw.numerator();
    assert(idw > 1);
    return util::trunclog2(idw);
}


void Appogiatura::addPrinted(Root* t)
{
    assert(_printed_content == NULL);
    assert(t);
    _printed_content = t;
}


void Appogiatura::accept(SMVisitor* v) const
{
    v->visitAppogiatura(this);
}


/// @todo prinnt
void Appogiatura::print(std::ostream& o) const
{
}


std::ostream& operator<<(std::ostream& o, const Appogiatura& a)
{
    a.print(o);
    return o;
}


} // namespace ScoreModel

//
//  Ornament.cpp
//  qparse
//
//  Created by Florent Jacquemard on 18/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Ornament.hpp"

namespace ScoreModel {

// duration = 0
// arity = 0 (leaf)
Ornament::Ornament(int staff, bool b):
_before(b),
_staff(staff),
_staves(),
_embedding()  // empty
{
    assert(staff >= 0);
}

Ornament::Ornament(const GraceNote* g1, bool before):
Ornament(before)
{
    this->add(g1);
}

Ornament::Ornament(const GraceNote* g1, const GraceNote* g2, bool before):
Ornament(before)
{
    this->add(g1);
    this->add(g2);
}


Ornament::Ornament(const GraceNote* g1, const GraceNote* g2,
                   const GraceNote* g3,
                   bool before):
Ornament(before)
{
    this->add(g1);
    this->add(g2);
    this->add(g3);
}


Ornament::Ornament(const GraceNote* g1, const GraceNote* g2,
                   const GraceNote* g3, const GraceNote* g4,
                   bool before):
Ornament(before)
{
    this->add(g1);
    this->add(g2);
    this->add(g3);
    this->add(g4);
}


Ornament::Ornament(const Ornament& o):
_before(o._before),
_staff(o._staff),
_staves(o._staves),
_embedding() // copy, the content are const
{
    for (const SingleGraceNote* g : o._embedding)
        _embedding.push_back(g);
}


Ornament::~Ornament()
{
    TRACE("delete Ornament (size={})", _embedding.size());
    for (const SingleGraceNote* g : _embedding)
    {
        if (g) delete g;
    }
    
    _embedding.clear();
}


size_t Ornament::size() const
{
    return _embedding.size();
}


bool Ornament::empty() const
{
    return _embedding.empty();
}


const SingleGraceNote* Ornament::at(size_t i) const
{
    assert(i < size());
    return _embedding.at(i);
}


/// @todo make something more precise when you known the tonality
bool Ornament::isUpMordent(const Pitch& p) const
{
    return isMordent(p, +1);
}


bool Ornament::isLowMordent(const Pitch& p) const
{
    return isMordent(p, -1);
}


bool Ornament::isMordent(const Pitch& p, int shift) const
{
    assert((shift == 1) || (shift == -1));
    assert(! p.undef());

    if (_embedding.size() != 2)
        return false;
    
    // all grace notes on the same staff
    if (_staves.size() != 1)
        return false;
    
    assert(! _embedding[0]->pitch().undef());
    if (_embedding[0]->pitch() != p)
        return false;
    
        // 1/2 tone or 1 tone difference
        /// @todo be more precise, using tonality.
    assert(! _embedding[1]->pitch().undef());
    return ((_embedding[1]->pitch().midi() == p.midi() + shift) ||
            (_embedding[1]->pitch().midi() == p.midi() + 2*shift));
}


bool Ornament::isGruppetto(const Pitch& p) const
{
    return isGruppetto(p, +1);
}


bool Ornament::isInvGruppetto(const Pitch& p) const
{
    return isGruppetto(p, -1);
}


bool Ornament::isGruppetto(const Pitch& p, int shift) const
{
    assert((shift == 1) || (shift == -1));
    assert(! p.undef());

    if (_embedding.size() != 4)
        return false;

    // all grace notes on the same staff
    if (_staves.size() != 1)
        return false;

    // reference pitch
    assert(! _embedding[1]->pitch().undef());
    assert(! _embedding[3]->pitch().undef());

    if ((_embedding[1]->pitch() != p) || (_embedding[3]->pitch() != p))
        return false;

    // 1/2 tone or 1 tone difference
    /// @todo check accidents according to KeySig
    assert(! _embedding[0]->pitch().undef());
    assert(! _embedding[2]->pitch().undef());
    return (((_embedding[0]->pitch().midi() == p.midi() + shift) ||
             (_embedding[0]->pitch().midi() == p.midi() + 2*shift)) &&
            ((_embedding[2]->pitch().midi() == p.midi() - shift) ||
             (_embedding[2]->pitch().midi() == p.midi() - 2*shift)));
}


void Ornament::add(const GraceNote* n,
                   int staff,
                   float accident,
                   const Notehead& h,
                   bool harm)
{
    assert(n);
    int staffnb = (staff == UNDEF_STAFF)?_staff:staff;
    assert(staffnb >= 0);
    _staves.add(staff);
    _embedding.push_back(new SingleGraceNote(n, staffnb,
                                             accident, h, harm));
    TRACE("add Grace Note {} to Ornament", *(_embedding.back()));

}


void Ornament::accept(SMVisitor* v) const
{
    v->visitOrnament(this);
}


void Ornament::print(std::ostream& o) const
{
    for (const SingleGraceNote* g : _embedding)
    {
        assert(g);
        o << " " << g->pitch() << "'";
    }
    if (_before)
        o << "(before)";
}


std::ostream& operator<<(std::ostream& o, const Ornament& a)
{
    a.print(o);
    return o;
}



} // namespace ScoreModel

//
//  Dot.cpp
//  squanty
//
//  Created by Florent Jacquemard on 20/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Dot.hpp"

namespace ScoreModel {


/// @todo _staves is left empty (not used)
Dot::Dot(size_t mult,
         Inner* parent,
         AtomicEvent* e,
         Leaf* previous, Leaf* next):
Continuation(mult, parent, e, previous, next)
{ }


Dot::Dot(const Continuation& c):
Continuation(c)
{ }


void Dot::accept(SMVisitor* v) const
{
    v->visitDot(this);
}


void Dot::print(std::ostream& o) const
{
    //assert(_embedding);
    //_embedding->print(o);
    o << " dot ";
}


std::ostream& operator<<(std::ostream& o, const Dot& e)
{
    e.print(o);
    return o;
}


} // namespace ScoreModel

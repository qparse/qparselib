//
//  Inner.hpp
//  squanty
//
//  Created by Florent Jacquemard on 28/08/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef Inner_hpp
#define Inner_hpp

#include <stdio.h>

#include "Node.hpp"


namespace ScoreModel {

/// @brief non-leaf node of engraving tree
class Inner : public Node
{
public:
   
    /// construct a new root node, embedded in the given wrapper.
    /// @param ar arity of the new node.
    /// @param wrapper wrapper node. This new node will be added to it.
    Inner(size_t ar,
          Root& wrapper);
    
    /// construct a non-leaf node with a parent.
    /// @param ar arity of the node.
    /// @param mult number of (multiple) edges between this node
    /// and its parent. must be strictly positive.
    /// @param parent the parent node. must be non-NULL.
    Inner(size_t ar, size_t mult, Inner* parent);
       
    /// construct a non-leaf root node.

    /// @param start date (MTU) of the leftmost leaf under this node.
    /// @todo TBR deprecated
    /// @param dur cumulated duration (MTU) of the leaves under this node.
    /// @param ar arity of the node.
    /// @param container musical score model element (voice in a measure)
    ///        containing this tree.
    Inner(mtu_t start, mtu_t dur,
          size_t ar,
          Sequence& container);

    /// copy constructor
    Inner(const Inner& n);
    
    virtual ~Inner();

    virtual bool complete() const;
    
    /// Current number of edges from this node.
    /// i.e. number of children that have been added times
    /// their respective multiplicities.
    virtual size_t completed() const = 0;
           
    /// starting musical date of the next child to be added
    /// @warning this node must not be complete.
    virtual mtu_t start_child() const = 0;

    /// duration of the next child to be added
    /// @warning this node must not be complete.
    virtual mtu_t dur_child() const = 0;
    
    /// Number of flags, or beams, associated to this node in CMN.
    /// For an inner note,  it is the number of beams between two events (following the beatween policy).
    // virtual unsigned long nbFlags() const;
    
    virtual bool isInner() const { return true; }

    virtual void print(std::ostream& o) const;

protected:
    
    friend class Node;
    
    // How many children have been added
    // (counting multiplicities).
    // @todo TBR
    // size_t _added;
    
    /// replace the node at given index by the given node.
    /// @param i the index of a subnode. must be smaller than size().
    /// @param n a new node. must be allocated.
    /// @return whether the replacement was effectively done.
    /// @warning the former sub-node at position i is not deleted.
    // @throw tree_error when i is an index of subnode of this node.
    virtual bool replace(size_t i, Node* n) = 0;
            
};

std::ostream& operator<<(std::ostream&, const Inner&);

} // namespace ScoreModel

#endif /* Inner_hpp */

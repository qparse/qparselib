//
//  VolChange.cpp
//  squanty
//
//  Created by Florent Jacquemard on 25/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "VolChange.hpp"


namespace ScoreModel {


VolChange::VolChange(const VPosition& vpos,
                     const HPosition& hp1, const HPosition& hp2,
                     Shape shape, bool incr, Line::Type type):
Spanning(vpos, hp1, hp2),
_shape(shape),
_cresc(incr),
_linetype(type)
{ }


VolChange::VolChange(const VPosition& vpos,
                     const HPosition& hp1, const HPosition& hp2,
                     Shape shape, bool incr):
Spanning(vpos, hp1, hp2),
_shape(shape),
_cresc(incr)
{
    switch (shape) {
        case Hairpin:
            _linetype = Line::Solid;
            break;
        case Text:
            _linetype = Line::Dotted;
            break;
        default:
            ERROR("VOlChange: unexpected shape value {}", shape);
            break;
    }
}


void VolChange::print_cresc(std::ostream& o) const
{
    if (_cresc)
        o << "cresc";
    else
        o << "dim";
}


void VolChange::print(std::ostream& o) const
{
    switch (_shape)
    {
        case Hairpin:
            o << "hairpin_";
            print_cresc(o);
            o << "(" << _linetype << ")";
            break;
        case Text:
            print_cresc(o);
            o << "(" << _linetype << ")";
            break;
        default:
            ERROR("VolChange: unexpected shape value {}", _shape);
            break;
    }
}


} // namespace ScoreModel


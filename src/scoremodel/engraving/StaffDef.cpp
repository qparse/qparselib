//
//  StaffDef.cpp
//  squant2
//
//  Created by Florent Jacquemard on 11/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Part.hpp"
//#include "Pitch.hpp"
#include "StaffDef.hpp"
#include "Score.hpp"
#include "AtomicEvent.hpp"
#include "Note.hpp"
#include "GraceNote.hpp"
#include "Rest.hpp"
#include "Chord.hpp"
#include "Trill.hpp"


namespace ScoreModel {


StaffDef::StaffDef(Part& part, int n, const Clef& clef):
_part(part),
_nb(n),
_id(part.id() + name() + std::to_string(n)),
_clef(clef),
_ks(part.score().keySignature()),
_ts(part.score().meterSignature()),
_ambitus() // undef pitches
{
    assert(n >= 0);
}


StaffDef::StaffDef(Part& part, int n,
             const Clef& clef, const MeterSig& ts, const KeySig& ks):
_part(part),
_nb(n),
_id(part.id() + name() + std::to_string(n)),
_clef(clef),
_ks(ks),
_ts(ts),
_ambitus() // unndef pitches
{ }


StaffDef::StaffDef(const StaffDef& s):
_part(s._part),
_nb(),
_id(),
_clef(s._clef),
_ks(s._ks),
_ts(s._ts),
_ambitus(s._ambitus)
{ }


StaffDef::~StaffDef()
{ }


// copy
void StaffDef::setClef(const Clef& c)
{
    TRACE("StaffDef {}: change clef to {}", nb(), c);
    _clef = c;
}


void StaffDef::setKeySig(const KeySig& ks)
{
    TRACE("StaffDef {}: change key sig. to {}", nb(), ks);
    _ks = ks;
}


void StaffDef::setMeterSig(const MeterSig& ts)
{
    TRACE("StaffDef {}: change meter sig. to {}", nb(), ts);
    _ts = ts;
}


bool StaffDef::empty() const
{
    return _ambitus.undef();
}


void StaffDef::add(const AtomicEvent* e)
{
    assert(e);
    _ambitus.add(e);
}


void StaffDef::print(std::ostream& o) const
{
    o << "staff " << _nb;
    o << " part:" << _part.id();
    o << " clef=" << _clef;
    o << " ks=" << _ks;
    o << " ts=" << _ts;
    o << " ambitus=<" << _ambitus << ">";
}


std::ostream& operator<<(std::ostream& o, const StaffDef& s)
{
    s.print(o);
    return o;
}


} // namespace ScoreModel

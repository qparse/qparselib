//
//  ScoreBuilder.hpp
//  squanty
//
//  Created by Florent Jacquemard on 24/05/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{


#ifndef ScoreBuilder_hpp
#define ScoreBuilder_hpp

#include <stdio.h>
#include <string>
#include <vector>
#include <stack>


#include "trace.hpp"
#include "InputSegment.hpp"
#include "Score.hpp" // in ScoreModel
#include "Part.hpp" // in ScoreModel
//#include "MeasureStack.hpp"
#include "Measure.hpp"
#include "Sequence.hpp"
#include "Clef.hpp"
#include "MeterSig.hpp"
#include "KeySig.hpp"

#include "Node.hpp"
#include "Inner.hpp"
#include "Root.hpp"
#include "Tuplet.hpp"
#include "Decorated.hpp"
#include "Tied.hpp"
#include "Dot.hpp"
#include "LeafNote.hpp"
#include "LeafRest.hpp"
#include "LeafSpace.hpp"
#include "LeafChord.hpp"


namespace ScoreModel {

class Chord;
//class Sequence;

/// @brief a Score Builder helps to the simultaneous
/// construction of a score model and an engraving model.
class ScoreBuilder
{
public:

    /// empty score builder
    ScoreBuilder();
    
    /// @param seg an input segment. Can be NULL when not needed.
    //ScoreBuilder(InputSegment* seg = NULL);

    /// @brief delete the intermediate structures in the builder.
    /// @warning the built elements of the score and engraving
    /// must be deleted externaly.
    ~ScoreBuilder();

    /// @brief open a new score.
    /// @param name id for the score. must not be empty.
    /// @param meter the default time signature. must not be NULL.
    /// deallocated with the score.
    /// @param key the default key signature. must not be NULL.
    /// deallocated with the score.
    /// @warning no score must be currently open.
    /// @return true iff a new score was successfully open.
    bool openScore(const std::string& name,
                   const MeterSig& meter,
                   const KeySig& key);

    /// if a measure is open, set the time signature for this measure.
    /// otherwise, set the default time signature for the score
    /// currently open.
    /// @warning a score must be open.
    /// @param ts new time signature. must not bu NULL.
    /// @return true iff a new time signature was successfully set.
    bool addMeterSignature(const MeterSig& ts);

    /// if a measure is open, set the key signature for this measure.
    /// otherwise, set the default key signature for the score
    /// currently open.
    /// @warning a score must be open.
    /// @param ks new key signature. must not bu NULL.
    /// @return true iff a new key signature was successfully set.
    bool addKeySignature(const KeySig& ks);
    
    /// get the score currently open
    /// @return the score currently open or NULL if there is none.
    inline Score* getScore() const { return currentScore; }
    
    /// @brief close the score currently open.
    /// @return the score that was created or null if there was none.
    /// @warning the returned score must be deallocated.
    Score* closeScore();
    
    // @brief open a current part (new or existing).
    // a new measure will be open for that part (`partwise` mode).
    // @param name part's name. if empty, a new name is created.
    // @return true iff a new part was successfully open.
    // @warning a score (`currentScore`) must be currently open.
    // @warning no part (`currentPart`) must be currently open.
    // bool openPart(const std::string& name = std::string());

    // @brief the part currently open.
    // @return the part currently open or NULL if there is none.
    // inline Part* getPart() const { return currentPart; }

    // @brief close the part currently open.
    // @return the part that was created or null if there was none.
    // @warning the returned part must be deallocated.
    // Part* closePart();
  
      
    /// @brief create a new measure in partwise mode.
    /// The new measure is after the last measure created, in the same part.
    /// The last measure created is closed.
    /// @param ts time signature taking effect at the beginning of the new
    /// measure; NULL to keep current time signature.
    /// @param ks key signature taking effect at the beginning of the new
    /// measure; NULL to keep current key signature.
    /// @return the measure newly created or NULL if there was an error.
    /// @warning a score must be open.
    /// @warning a measure must have been open before.
    /// @warning no sequence can be open.
    Measure* nextMeasure(const MeterSig* ts = NULL,
                         const KeySig* ks = NULL);

    /// @brief create a measure in timewise mode.
    /// The new measure is in the given part and in the same time window as the
    /// last measure created, or at time 0 if no measure has been created yet.
    /// The part partname is added to the score if it does not exist.
    /// @param partname name of a part (existing or new).
    /// @param ts time signature taking effect at the beginning of the new
    /// measure; NULL to keep current time signature.
    /// @param ks key signature taking effect at the beginning of the new
    /// measure; NULL to keep current key signature.
    /// @return the measure newly created or NULL if there was an error.
    /// @warning a score must be open.
    /// @warning no sequence can be open.
    Measure* newMeasure(const std::string& partname,
                        const MeterSig* ts = NULL,
                        const KeySig* ks = NULL);
    
    /// @brief create a measure in timewise mode.
    /// The new measure is in the given part and in the same time window as the
    /// last measure created, or at time 0 if no measure has been created yet.
    /// @param part a part that already exist in the current score.
    /// @param ts time signature taking effect at the beginning of the new
    /// measure; NULL to keep current time signature.
    /// @param ks key signature taking effect at the beginning of the new
    /// measure; NULL to keep current key signature.
    /// @return the measure newly created or NULL if there was an error.
    /// @warning a score must be open.
    /// @warning no sequence can be open.
    Measure* newMeasure(Part& part,
                        const MeterSig* ts = NULL,
                        const KeySig* ks = NULL);

    /// set the clef for the measure currently open.
    /// @warning a measure must be open.
    /// @param clef new time signature
    /// @return true iff a new clef was successfully set.
    /// @todo clef change at a score position other than beggining.
    bool addClef(const Clef& clef);
    
    // @brief close the measure currently open.
    // Measure* closeMeasure();
    

    // @brief open a voice without corresponding measure.
    // @param v a voice number.
    // @return true iff opening was successful.
    // @warning a score and a part must be open. Voice v must not be open.
    // Open a voice of number v and do not open a measureVoice.
    // Use for building a score model without the companion engraving model.
    // bool openVoice(int v);
    // Voice* closeVoice();
    
    /// @brief open a sequence (voice) in the current open measure.
    /// @param v a voice number.
    /// @param n last leaf before the sequence to open.
    /// it is useful to create a tie between measures.
    /// If present, it does the same thing as setPrevLeaf(n).
    /// @return true iff a voice was successfully open
    /// in the current open measure.
    /// @warning a score, a part and a measure must be open.
    /// @warning Voice v must not be open.
    /// In the measure currently open, open a Sequence for voice number v,
    /// and open the tree that will contain the Sequence.
    /// The tree will be closed by closeSequence.
    bool openSequence(int v, Leaf* n=NULL);
    
    /// get the sequence currently open
    /// @return the sequence currently open or NULL if there is none.
    /// @todo rm if not useful
    inline const Sequence* getSequence() const { return currentSequence; }

    /// @brief close the sequence currently open.
    Sequence* closeSequence();
    
    /// create a new Tuplet node.
    /// @param ar arity of the node. @see Node.
    /// @param mult multiplicity of the node. @see Node.
    /// @param beam beaming policy for children nodes.
    /// @param lab optional tuplet label
    /// @return the tree created.
    /// @warning a sequence must be open.
    Tuplet* pushTuplet(size_t ar, size_t mult,
                       const Beam::Info beam,
                       const TupletLabel& lab);

    // bool setOutDegree(size_t a);
    // bool setInDegree(size_t a);
    // bool setBeaming(size_t b);

    // create a new empty node for an event decorated,
    // and push it to the tree stack.
    // Once the Decorated node is open, there are 2 ways to complete it:
    // - 1. for an ornament played after the event,
    //      - add every element in the Ornament with using pushGraceNote.
    //      - add the event Leaf with pushNote or open/closeChord.
    //   In this case, the Decorated node is closed by the addition of
    //   the event and closeDecorated must NOT be called.
    //   pushGraceNote cannot be called after the event was added.
    //   The Decorated node can be recovered as parent of the event Node
    //   e.g. to cast its Ornement to a specific subtype,
    //  see Decorated.add(Ornament*).
    // - 2. for an event played after the ornament,
    //      - add the event Leaf with pushNote or open/closeChord.
    //      - add every element in the Ornament with using pushGraceNote.
    //      - close Decorated node with closeDecorate().
    //   pushGraceNote cannot be called after the Decorated node was closed.
    // @warning the event must be a note or a chord Leaf.
    
    /// @brief create a new empty Decorated node and push it to the tree stack.
    /// @param mult multiplicity of the node. @see Node.
    /// @return the node created.
    /// @warning a sequence must be open.
    /// The tree headed by this Decorated node will be made of
    /// an Ornament and a Leaf event. It can be completed as follows:
    /// - openOrnament, pushGraceNote (iterated), closeOrnament
    ///   in order to to add the Ornament child.
    /// - pushNote or open/closeChord in order to to add the Leaf event child.
    /// The order in which the 2 above steps are completed determines
    /// if the ornament is played before or after the event, see Decorated.
    /// The ornament child can be casted after addition (get, cast and re-add)
    /// see Decorated.add(Ornament*).
    Decorated* pushDecorated(size_t mult);

    /// create a new empty Ornament to be included in a Decorated node.
    /// Once open, the Ornament can be completed using pushGraceNote.
    /// @param staff default staff number for the events in this ornament.
    /// @param before flag true (resp. false) if the ornament is supposed
    /// to be played before (resp. at) its start date.
    /// @warning only pushGraceNote and closeOrnament can be called as long as
    /// the ornament is open.
    bool openOrnament(int staff, bool before=true);

    // bool isopenOrnament = top.isOrnament()
    // is terminated (closed) when one Leaf is pushed (must be note or Chord)
    
    /// add a new grace note to the currently open Ornament.
    /// A new GraceNote is created in the companion musical score model.
    /// @param staff number of the staff where this event will be located.
    /// @param p pitch of the grace note to add.
    /// @param accident displayed accident or courtesy
    /// @param h shape of the note head.
    /// @param harm whether the note is an natural harmonic
    /// @return the GraceNote created or NULL if construction failed.
    /// @warning one Ornament must be open.
    GraceNote* pushGraceNote(int staff,
                             const Pitch& p,
                             float accident = Pitch::UNDEF_NOTE_ALTERATION,
                             const Notehead& h = Notehead::Shape::Oval,
                             bool harm = false);
    
//    void add(const GraceNote* n,
//             int staff = UNDEF_STAFF,
//             float accident = Pitch::UNDEF_NOTE_ALTERATION,
//             const Notehead& h = Notehead::Shape::Oval,
//             bool harm = false);


    /// Close the currently open Ornament,
    /// push it to the tree stack (its top must be a Decorated node),
    /// and return it.
    /// @return the Ornament node created or NULL in case of error,
    /// in particular if the Ornament is empty.
    /// @warning a Ornament node must be open and not empty.
    /// @warning the Ornament returned can be casted to a particular subtype
    /// (Appogiatura, Mordent....) after closing.
    Ornament* closeOrnament();
    
    inline bool checkPrevLeaf() const { return (prevLeaf != NULL); }

    /// set the state variable containing the previous leaf to the given leaf.
    /// It is useful to create a tie between measures.
    /// @param n the new prev leaf. must be allocated.
    /// @warning the prev leaf must be unset.
    void setPrevLeaf(Leaf* n);

    /// create a new Tied node.
    /// @param mult multiplicity of the node. @see Node.
    /// @param staff number of the staff where this event will be displayed.
    /// @return the node created.
    /// @warning a sequence must be open.
    /// @warning The variable prevLeaf must be set. If the Tie is the first leaf
    /// in the current measure, set it to the previous leaf with setFirstLeaf.
    /// If the Tie is the first leaf in the first measure, use pushSpace instead.
    Tied* pushTie(size_t mult, int staff);

    /// create a new node representing a dot..
    /// @param mult multiplicity of the node. @see Node.
    /// @param staff number of the staff where this event will be displayed.
    /// @return the node created.
    /// @warning a sequence must be open.
    /// @warning it cannot be the first leaf node in the current measure.
    /// @todo staff is useless.
    Dot* pushDot(size_t mult, int staff);
    
    /// create a new node representing a Note.
    /// @param mult multiplicity of the node. @see Node.
    /// @param staff number of the staff where this event will be displayed.
    /// @param p pitch of the note
    /// @param accident displayed accident or courtesy
    ///        (may be different from the accident in the pitch).
    /// @param h shape of the note head
    /// @param s direction of the stem
    /// @param harm whether the note is an natural harmonic
    /// @param tremolo number of tremolo marked over the stem
    /// @return the node created.
    /// @warning a sequence must be open.
    LeafNote* pushNote(size_t mult,
                       int staff,
                       const Pitch& p,
                       float accident = Pitch::UNDEF_NOTE_ALTERATION,
                       const Notehead& h = Notehead::Shape::Oval,
                       const Stem& s = Stem::Type::Unspecified,
                       bool harm=false,
                       int tremolo=0);
    
    /// create a new node representinng a Rest.
    /// @param mult multiplicity of the node. @see Node.
    /// @param staff number of the staff where this event will be displayed.
    /// @return the node created.
    /// @warning a sequence must be open.
    LeafRest* pushRest(size_t mult, int staff);

    LeafSpace* pushSpace(size_t mult, int staff);

    /// create a new node representinng a Chord.
    /// @param mult multiplicity of the node. @see Node.
    /// @param staff number of the staff where this event will be displayed.
    /// @param arp kind of arpegio mark (default None)
    /// @return wether the chord creation succeeded.
    /// @warning a sequence must be open.
    /// @warning only addChord and closeChord can be called as long as
    /// the chord is open.

    bool openChord(size_t mult, int staff,
                   const Arpeggio& arp = Arpeggio(),
                   const Stem& s = Stem::Type::Unspecified,
                   int tremolo=0);

    /// add a note to the chord
    /// @return whether the note addition succeeded.
    /// @warning a chord must be open.
    bool addChord(const Pitch& p,
                  int staff,
                  float accident = Pitch::UNDEF_NOTE_ALTERATION,
                  const Notehead& h = Notehead::Shape::Oval,
                  bool harm = false);

    /// @return the chord node created or NULL in case of error.
    LeafChord* closeChord();

    /// parent node of the current node on the tree stack,,
    /// or NULL if the current node is root.
    Inner* parent() const;
    
    Measure* getMeasure() const { return currentMeasure; }

    /// the current node is the leftmost node the tree stack.
    bool leftmost() const;
    
    /// last sibling before the current node,
    /// or NULL if the corrent node is a first sibling.
    Node* prevSibling() const;
    
protected:
    
    /// points to Score currently open,
    /// NULL if there is none.
    Score*        currentScore;

    /// Part associated to Measure currently open.
    /// @warning a Measure must be open.
    Part&         currentPart() const;

    // Stack containing the Measure currently open.
    // @warning a Measure must be open.
    // MeasureStack& currentMeasureStack() const;

    /// points to Measure currently open,
    /// NULL if there is none.
    Measure*      currentMeasure;

    /// NULL if a Measure is currently open,
    /// otherwise points to the previous Measure that has been open
    Measure*      previousMeasure;

    /// points to Sequence currently open,
    /// NULL if there is none.
    Sequence*     currentSequence;

    /// Number of Voice associated to Sequence currently open.
    /// @warning a Sequence must be open.
    int           currentVoiceNb() const;

    /// Voice associated to Sequence currently open.
    /// @warning a Measure and a Sequence must be open.
    Voice&        currentVoice() const;

    /// points to Ornament currently open,
    /// NULL if there is none.
    Ornament*     currentOrnament;
    mtu_t         currentOrnamentDate;

    /// points to Chord currently open,
    /// NULL if there is none.
    LeafChord*    currentChord;

    /// stack for the depth-first construction of trees (automated)
    //std::stack<Inner*, std::vector<Inner*>> treeStack;
    std::stack<Inner*> treeStack;
    /// other auxiliary states
    
    /// @todo revise for polyphony.
    /// last leaf for the current sequence (NULL if no sequence is open)
    Leaf*         prevLeaf;
    //AtomicEvent*  currentEvent;

private:
        
    /// @warning a sequence must be open.
    /// @warning the tree stack must be empty.
    /// @return the tree created.
    Root* pushRoot();
    
    /// read parent at stack top and sanity check
    /// @param m multiplicity (for checking)
    /// @warning stack must not be empty
    Inner* topParent(size_t m) const;

    /// complete the push of a node:
    /// close the corresponding tree (tree at the given node)
    /// and add it the top tree of the stack,
    /// or push it to the tree-stack.
    void pushNode(Node* t);

    /// @brief open a tree that will be a subtree
    /// of the last currently open tree.
    // bool openSubTree();

    /// @brief close last open subtree and
    /// add it as subtree to its parent tree.
    // Node* closeSubTree();

    /// the given leaf is set as next leaf of the current prevLeaf
    /// and the current prevLeaf is set to the given leaf.
    /// @param n allocated Leaf.
    void linkPrevLeaf(Leaf* n);
    
    void unsetPrevLeaf();
    
    /// check that a leaf can be constructed.
    bool failLeaf(const std::string& fun) const;
    
    /// @return the sibling number, in 0..n, of the current node.
    size_t currentSiblingNb() const;
   
};




} // end namespace ScoreModel


#endif /* ScoreBuilder_hpp */

/// @}

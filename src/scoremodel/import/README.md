# src/scoremodel/import

This group contains procedures for the building one score model containing one musical and one engraving part, from another model.

A generic score model builder (`ScoreBuilder`) is provided to ease the construction. It provide in particular facilities for constructing the hierarchical structures (trees) representing the sequences (voices) in the engraving model: the tree is built by pushing the labelling symbols, one by one, following a depth-first traversal. 

The musical par of the score model is built automatically, during the construction of the engraving part.


The classs `TableMonoImporter`,  based on this builder, builds a score model from a parse table (compact representation of parse trees) obtained by parsing a MIDI input.


//
//  TableMonoImporter.hpp
//  squanty
//
//  Created by Florent Jacquemard on 05/04/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
// This version propagate links for parents and siblings in the tree
// It is useless because it is done automatically by the Score Builder.


/// @addtogroup scoremodel
/// @{


#ifndef TableMonoImporter_hpp
#define TableMonoImporter_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <assert.h>
#include <utility> // pair

#include "trace.hpp"
#include "InputSegment.hpp"
#include "TableA.hpp"
#include "ScoringEnv.hpp"
#include "ScoreBuilder.hpp"


namespace ScoreModel {


/// score and engraving model builder
/// that can read several monophonic parts from parse tables.
class TableMonoImporter
{
public:

    /// @brief start a builder and open a new empty score in this builder.
    /// @param scorename id for the score.
    /// @param meter the default time signature. deallocated with the score.
    /// @param key the default key signature. deallocated with the score.
    TableMonoImporter(const std::string scorename,
                      const MeterSig& meter = MeterSig(4, 4),
                      const KeySig& key = KeySig(0));
    
    ~TableMonoImporter();

    /// get the score built
    /// @return the score constructed by the builder, in its current state.
    Score* getScore() const;
    
    /// create a part of the given name in current score
    /// and feed it with the k-best run in the given table.
    /// @param partname id of the new part.
    /// @param env scoring environement, contains an input segment and
    /// a voicing (description of the organization of the part into voices).
    /// @param tab associated parse table containing engraving info.
    /// @param k target state or run r
    /// @param n (as in n-best) is the rank of the run with target the key
    ///        of the record. must be larger or equal to 1 (default).
    /// @return the next position to read in input segment (can be out
    /// of bounds).
    /// @warning content can be added to the table (for best computation).
    template<class K>
    size_t readPart(const std::string partname,
                    ScoringEnv& env,
                    parsing::TableA<K>& tab,
                    const K& k,
                    size_t n=1);
        
private:
    /// score builder constructing the score and engraving models
    ScoreBuilder _builder;

    /// name of the one and only part
    std::string _partname;

    /// counter of measures
    // size_t _measure_cpt;
    
    /// used to get the last leaf of previous sequence in case of
    /// bar crossing tie.
    Sequence* _prev_sequence;
    
    /// flag defining the tree construction in case of parse fail for a subtree.
    /// - if unfailing is false, return a NULL tree
    ///   iff there is a NULL subrun in the n-best run for k in tab
    ///   (bottom-up NULL propagation).
    /// - otherwise the tree may contain NULL subtrees.
    /// The NULL subtrees can be constructed by readFail.
    /// @warning even when the parse tree is not NULL,
    ///          it may contain NULL subtrees,
    ///          when parsing partially failed e.g. for 1 bar (NULL subrun).
    bool _unfailing = true;
    
    // score model in construction
    // Score& _score;
    
    // name of the part currently in construction (global variable).
    // Part& _part;

    // table used for the construction of the current part imported
    // (global variable).
    
    /// parse one run and import it to the current part.
    /// @param env scoring environement, contains an input segment and
    /// a voicing (description of the organization of the part into voices).
    /// @param tab associated parse table containing engraving info.
    /// @param k target state or run r
    /// @param r a run (body of transition rule of the augmented automaton)
    /// @param b position of first point to read in input segment.
    /// @param m number of current measure (i.e. its MTU starting date).
    /// @return the next position to read in input segment.
    /// @warning content can be added to the table (for best computation).
    template<class K>
    size_t readRun(ScoringEnv& env,
                   parsing::TableA<K>& tab,
                   const K& k,
                   std::shared_ptr<const parsing::Run> r,
                   size_t b,
                   size_t m);

    /// read and import one Measure in the current part
    /// see readRun for parameters and output
    template<class K>
    size_t readMeasure(ScoringEnv& env,
                       parsing::TableA<K>& tab,
                       const K& k,
                       std::shared_ptr<const parsing::Run> r,
                       size_t b,
                       size_t m);

    /// read and import one Sequence in the current Measure.
    /// @param env scoring environement, contains an input segment and
    /// a voicing (description of the organization of the part into voices).
    /// @param tab associated parse table containing engraving info.
    /// @param k target state or run r
    /// @param r a run (body of transition rule of the augmented automaton)
    /// @param b position of first point to read in input segment.
    /// @param m number of current measure (i.e. its MTU starting date).
    /// @param v number of the voice corresponding to the sequence.
    /// @return the next position to read in input segment.
    template<class K>
    size_t readSequence(ScoringEnv& env,
                        parsing::TableA<K>& tab,
                        const K& k,
                        std::shared_ptr<const parsing::Run> r,
                        size_t b,
                        size_t m,
                        int v);
    
    /// add one tree of the current Sequence.
    /// @param env scoring environement, contains an input segment and
    /// a voicing (description of the organization of the part into voices).
    /// @param tab associated parse table containing engraving info.
    /// @param k target state or run r
    /// @param r a run (body of transition rule of the augmented automaton)
    /// @param mult multiplicity of the top node of the tree.
    /// @param dur total duration (in MTU) of the tree.
    /// @return the next position to read in input segment.
    /// @todo TBC parent, prev useless?
    template<class K>
    size_t readTree(ScoringEnv& env,
                    parsing::TableA<K>& tab,
                    const K& k,
                    std::shared_ptr<const parsing::Run> r,
                    size_t mult,
                    size_t b,
                    int voiceNb,
                    mtu_t dur);
    
    /// particular case of readTree
    template<class K>
    size_t readTreeInner(ScoringEnv& env,
                         parsing::TableA<K>& tab,
                         const K& k,
                         std::shared_ptr<const parsing::RunInner<K>> r,
                         size_t mult,
                         size_t b,
                         int voiceNb,
                         mtu_t dur);
    
    /// particular case of readTree
    template<class K>
    size_t readTreeTerminal(ScoringEnv& env,
                            parsing::TableA<K>& tab,
                            const K& k,
                            std::shared_ptr<const parsing::RunTerm> r,
                            size_t mult,
                            size_t b,
                            int voiceNb,
                            mtu_t dur);
   
    /// add one Node in the tree in the current Sequence.
    /// @param env scoring environement, contains an input segment and
    /// a voicing (description of the organization of the part into voices).
    /// @param a label of the node.
    /// @param arity arity of the node.
    /// @param multiplicity multiplicity of the node.
    /// @param b position of first point to read in input segment.
    /// @param n number of points to read in input segment for the symbol.
    /// @param voiceNb current voice nb.
    /// @param dur duration (in MTU) of the node.
    /// @return the next position to read in input segment.
    size_t readSymbol(ScoringEnv& env,
                      label_t a,
                      size_t arity,
                      size_t multiplicity,
                      size_t b,
                      size_t n,
                      int voiceNb,
                      mtu_t dur);

    /// subcase of readSymbol
    size_t readContinuation(size_t multiplicity,
                            size_t b, size_t n,
                            int staff);

    /// subcase of readSymbol
    /// @todo TBR
    size_t readMulti(size_t multiplicity,
                     size_t b, size_t n,
                     int staff);

    /// subcase of readSymbol : one dot symbol
    size_t readTie(size_t multiplicity,
                   size_t b, size_t n,
                   int staff);

    /// subcase of readSymbol : one dot symbol
    size_t readDot(size_t multiplicity,
                   size_t b, size_t n,
                   int staff);

    /// subcase of readSymbol
    size_t readRest(size_t multiplicity,
                    size_t b, size_t n,
                    int staff);

    /// subcase of readSymbol : single note
    std::pair<size_t, Node*> readNote(ScoringEnv& env,
                                      size_t multiplicity,
                                      size_t b, size_t n,
                                      int staff);

    /// part of rhythm corresponding to missing information
    /// (e.g. in case of transcription failure.
    /// It is represented by a Rest.
    size_t readFail(size_t multiplicity,
                    size_t b, size_t n,
                    int staff);

    /// subcase of readSymbol : several grace notes and one note
    /// @param nb number of grace notes to add.
    /// It might be different from n e.g. in case of grace-rests ignored.
    /// @todo distinnguish the case of grace notes after note
    size_t readDecorated(ScoringEnv& env,
                         size_t multiplicity,
                         size_t b,
                         size_t n,
                         size_t nb,
                         int staff);

    /// subcase of readSymbol : tuple symbol
    size_t readTuple(label_t a,
                     size_t arity, size_t multiplicity,
                     size_t b,
                     mtu_t dur);

    /// cast given ornament into appropriate subtype
    /// @param o ornament to cast
    /// @param p pitch of the decorated event, for detection of ornament subtype
    /// @param link Leaf event decorated by ornament
    /// @param staff staff number for the event
    static Ornament* castOrnament(const Ornament& o, const Pitch& p,
                                  LeafNote* link, int staff);
    
    /// format the positions to read in input segment for tracing
    static std::string loc(size_t b, size_t n);

};

} // end namespace ScoreModel


// separated definition of template class
#include "TableMonoImporter.tpp"


#endif /* TableMonoImporter_hpp */

/// @}



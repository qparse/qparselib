//
//  ScoreBuilder.cpp
//  squanty
//
//  Created by Florent Jacquemard on 24/05/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "ScoreBuilder.hpp"
#include "MTU.hpp"
#include "Root.hpp"
#include "Tied.hpp"
#include "Note.hpp"
#include "Rest.hpp"
#include "Chord.hpp"


namespace ScoreModel {


ScoreBuilder::ScoreBuilder():
currentScore(NULL),
currentMeasure(NULL),
previousMeasure(NULL),
currentSequence(NULL),
currentOrnament(NULL),
currentOrnamentDate(MTU::ZERO),
currentChord(NULL),
treeStack(),           // empty
prevLeaf(NULL)
{ }


ScoreBuilder::~ScoreBuilder()
{
    TRACE("delete ScoreBuilder");
    /// @todo checks everything is closed ?
    // clear the tree stack
    //std::stack<Inner*, std::vector<Inner*>>().swap(treeStack);
    std::stack<Inner*>().swap(treeStack);
    // do not delete the built elements.
}


Part& ScoreBuilder::currentPart() const
{
    assert(currentMeasure != NULL);
    return currentMeasure->part();
}


//MeasureStack& ScoreBuilder::currentMeasureStack() const
//{
//    assert(currentMeasure != NULL);
//    return currentMeasure->stack();
//}


int ScoreBuilder::currentVoiceNb() const
{
    assert(currentMeasure != NULL);
    assert(currentSequence != NULL);
    return currentSequence->voiceNb();
}


Voice& ScoreBuilder::currentVoice() const
{
    assert(currentMeasure != NULL);
    Part& p = currentMeasure->part();
    return p.voice(currentVoiceNb());
    //return currentPart().voice(currentVoiceNb());
}


bool ScoreBuilder::openScore(const std::string& name,
                             const MeterSig& meter,
                             const KeySig& key)
{
    if (currentScore != NULL)
    {
        ERROR("ScoreBuilder: open score {}: one score {} already open",
              name, currentScore->id());
        return false;
    }
    
    if (name.empty())
    {
        ERROR("ScoreBuilder: open score: empty name");
        return false;
    }

    TRACE("ScoreBuilder: opening score {}", name);
    currentScore = new Score(name, meter, key);
    return true;
}


Score* ScoreBuilder::closeScore()
{
    if (currentScore == NULL)
    {
        ERROR("ScoreBuilder: close score : no score open");
        return NULL;
    }

    if (currentSequence != NULL)
    {
        ERROR("ScoreBuilder: close score : voice {} in measure {} not closed",
              currentSequence->voiceNb(), currentSequence->measureNb());
        return NULL;
    }
    
    // should both be closed if new Sequence is open.
    assert(currentOrnament == NULL);
    assert(currentChord == NULL);

    TRACE("ScoreBuilder: closing score {}", currentScore->id());
    Score* ret = currentScore;
    currentScore = NULL;
    return ret;
}


bool ScoreBuilder::addClef(const Clef& c)
{
    if (currentMeasure == NULL)
    {
        ERROR("ScoreBuilder: add clef : no measure open");
        return false;
    }
    
    TRACE("ScoreBuilder: set clef {} for measure {}",
          c, currentMeasure->nb());
    /// @todo add Floating
    //currentMeasure->setClef(c);
    return true;
}


bool ScoreBuilder::addMeterSignature(const MeterSig& ts)
{
    //assert(ts);
    if (currentScore == NULL)
    {
        ERROR("ScoreBuilder: add time sig : no score open");
        return false;
    }
    
    if (currentMeasure == NULL)
    {
        TRACE("ScoreBuilder: assign time sig {} to score:{}",
              ts, currentScore->id());
        currentScore->setMeterSignature(ts);
        return true;
    }

    TRACE("ScoreBuilder: assign time sig {} to measure {}",
            ts, currentMeasure->nb());
    /// @todo complete Measure or remove function
    // currentMeasure->setMeterSignature(ts);
    return true;
}


bool ScoreBuilder::addKeySignature(const KeySig& ks)
{
    //assert(ks);
    if (currentScore == NULL)
    {
        ERROR("ScoreBuilder: add key sig: no score open");
        return false;
    }
    
    if (currentMeasure == NULL)
    {
        TRACE("ScoreBuilder: assign key sig {} to score:{}",
              ks, currentScore->id());
        currentScore->setKeySignature(ks);
        return true;
    }
    
    TRACE("ScoreBuilder: assign key sig {} to measure {}",
          ks, currentMeasure->nb());
    /// @todo complete Measure or remove function
    // currentMeasure->setKeySignature(ks);
    return true;
}


//bool ScoreBuilder::openPart(const std::string& name)
//{
//    if (currentScore == NULL)
//    {
//        ERROR("ScoreBuilder: open part : no score open");
//        return false;
//    }
//
//    if (currentPart != NULL)
//    {
//        ERROR("ScoreBuilder: open part:{}: one part:{} already open",
//              name, currentPart->id());
//        return false;
//    }
//
//    TRACE("ScoreBuilder: opening part:{}", name);
//    currentPart = &(currentScore->part(name));
//    return true;
//}


//Part* ScoreBuilder::closePart()
//{
//    Part* ret;
//    if (currentPart == NULL)
//    {
//        ERROR("ScoreBuilder: close part : no part open");
//        ret = NULL;
//    }
//
//    TRACE("ScoreBuilder: closing part:{}", currentPart->id());
//    ret = currentPart;
//    currentPart = NULL;
//    return ret;
//}


Measure* ScoreBuilder::nextMeasure(const MeterSig* ts,
                                   const KeySig* ks)
{
    if (currentScore == NULL)
    {
        ERROR("ScoreBuilder: next measure : no score open");
        return NULL;
    }

    if (currentSequence != NULL)
    {
        ERROR("ScoreBuilder: next measure : voice {} in measure {} not closed",
              currentSequence->voiceNb(), currentSequence->measureNb());
        return NULL;
    }
    
    if (currentMeasure == NULL)
    {
        ERROR("ScoreBuilder: next measure should not be called for first measure");
        return NULL;
    }

    Part& p = currentMeasure->part();
    size_t n = currentMeasure->nb() + 1; // next measure number
    //MeasureStack& s = currentScore->stack(n);
    TRACE("ScoreBuilder: next measure {} in part:{}", n, p.id());
    // measure m may already exist or is created
    Measure& m = p.measure(n, ts, ks);
    currentMeasure = &m;
    return &m;
}


Measure* ScoreBuilder::newMeasure(Part& part,
                                  const MeterSig* ts,
                                  const KeySig* ks)
{
    if (currentScore == NULL)
    {
        ERROR("ScoreBuilder: new measure : no score open");
        return NULL;
    }

    if (currentSequence != NULL)
    {
        ERROR("ScoreBuilder: new measure : voice {} in measure {} not closed",
              currentSequence->voiceNb(), currentSequence->measureNb());
        return NULL;
    }

    if (currentMeasure != NULL)
    {
        const Part& p = currentMeasure->part();
        if (p.id() == part.id())
        {
            WARN("ScoreBuilder: current measure {} already in part:{}.\
                  Do not create a new one.",
                 currentMeasure->nb(), p.id());
            return currentMeasure;
        }
        // MeasureStack& s = currentMeasure->stack();
        size_t n = currentMeasure->nb();
        TRACE("ScoreBuilder: new measure {} in part:{}",
              n, part.id());
        // Measure& m = s.ofPart(part);   /// @todo Part.getMeasure(n);
        Measure& m = part.measure(n, ts, ks);
        currentMeasure = &m;
        return &m;
    }
    // first measure
    // MeasureStack& s = currentScore->stack(0);
    TRACE("ScoreBuilder: new measure 0 in part:{}", part.id());
    //Measure& m = s.ofPart(part);
    Measure& m = part.newMeasure(ts, ks);
    currentMeasure = &m;
    return &m;
}


Measure* ScoreBuilder::newMeasure(const std::string& partname,
                                  const MeterSig* ts,
                                  const KeySig* ks)
{
    if (currentScore == NULL)
    {
        ERROR("ScoreBuilder: new measure : no score open");
        return NULL;
    }

    std::string rename = partname;

    if (! partname.empty())
    {
        rename = std::string("part")+std::to_string(currentScore->nbParts());
        WARN("ScoreBuilder: new measure : empty part name, rename to {}",
             rename);
    }
    Part& p = currentScore->part(rename);
    return newMeasure(p, ts, ks);
}


// n = last leaf before the sequence.
bool ScoreBuilder::openSequence(int v, Leaf* n)
{
    if (currentMeasure == NULL)
    {
        ERROR("ScoreBuilder: open sequence {}: no measure open", v);
        return false;
    }

    if (currentSequence != NULL)
    {
        ERROR("ScoreBuilder: open sequence {}: sequence {} already open",
              v, currentSequence->voiceNb());
        return false;
    }
    
    assert(currentScore != NULL); // should not be NULL if exists measure open
    assert(prevLeaf == NULL);
    assert((n == NULL) || n->isLeaf());
    prevLeaf = n;
    Measure& m = *currentMeasure;
    // Voice& voice = p.voice(v);
    TRACE("ScoreBuilder: open sequence: opening voice {} in measure {} part:{}",
          v, m.nb(), currentPart().id());
    // can be re-open
    Sequence& s = m.voice(v);
    currentSequence = &s;
    Root* content = pushRoot(); // new Root(m.start(), m.duration(), s);
    currentSequence->add(content);
    return true;
}


// internal
Root* ScoreBuilder::pushRoot()
{
    assert(currentSequence != NULL);
    //ERROR("ScoreBuilder: push Root : no sequence open");

    assert(treeStack.empty());
    // ERROR("ScoreBuilder: open Root : tree stack not empty");
    Sequence& s = *currentSequence;
    Measure& m = s.measure();
    TRACE("ScoreBuilder: measure {}: push root to the tree stack", m.nb());
    Root* t = new Root(m.start(), m.duration(), s);
    treeStack.push(t);
    //assert(treeStack.top())->isRoot());
    return t;
}


Sequence* ScoreBuilder::closeSequence()
{
    if (currentSequence == NULL)
    {
        ERROR("ScoreBuilder: close sequence: no sequence open");
        return NULL;
    }
    
    if (! treeStack.empty())
    {
        ERROR("ScoreBuilder: close sequence: tree not complete");
        return NULL;
    }

    // should not be NULL if currentSequence is not
    assert(currentMeasure != NULL);
    TRACE("ScoreBuilder: close sequence: closing voice {} in measure {} part:{}",
          currentVoiceNb(), currentMeasure->nb(), currentPart().id());
    assert(currentSequence->isSequence()); /// @todo TBR - for old dealloc bug.
    Root* t = currentSequence->tree();
    assert(t);
    assert(prevLeaf); // the tree must contain at least one leaf.
    assert(prevLeaf->isLeaf());
    t->setLastLeaf(prevLeaf);
    unsetPrevLeaf();
    Sequence* ret = currentSequence;
    currentSequence = NULL;
    return ret;
}


void ScoreBuilder::setPrevLeaf(Leaf* n)
{
    // ERROR("ScoreBuilder: previous leaf already set");
    assert(prevLeaf == NULL);
    assert(n);
    prevLeaf = n;
}


void ScoreBuilder::unsetPrevLeaf()
{
    assert(prevLeaf != NULL);
    assert(prevLeaf->isLeaf());
    prevLeaf = NULL;
}


// we only push parent nodes on the stack
Inner* ScoreBuilder::topParent(size_t mult) const
{
    assert(currentSequence != NULL);
    assert(! treeStack.empty());
    Inner* parent = treeStack.top();
    assert(parent);
    assert(parent->isInner());
    // case parent = root of tree
    assert((treeStack.size() > 1) || (parent->isRoot()));
    assert((! parent->isRoot()) || (mult == 1));
    return parent;
}


Tuplet* ScoreBuilder::pushTuplet(size_t ar, size_t mult,
                                 const Beam::Info beam,
                                 const TupletLabel& lab)
{
    assert(currentMeasure);
    TRACE("ScoreBuilder: measure {}: push {}-uplet, mult={}, to the tree stack",
          currentMeasure->nb(), ar, mult);
    Tuplet* ret = new Tuplet(ar, mult, topParent(mult), beam, lab); // empty
    pushNode(ret);
    assert(! treeStack.empty());
    assert(treeStack.top()); // is Inner
    assert(treeStack.top()->isTuplet());
    return ret;
}


// @todo revise
Decorated* ScoreBuilder::pushDecorated(size_t mult)
{
    assert(currentMeasure);
    TRACE("ScoreBuilder: measure {}: push decorated to the tree stack",
          currentMeasure->nb());
    Decorated* ret = new Decorated(mult, topParent(mult)); // empty
    pushNode(ret);
    assert(! treeStack.empty());
    assert(treeStack.top()); // is Inner
    assert(treeStack.top()->isDecorated());
    return ret;
}


bool ScoreBuilder::openOrnament(int staff, bool before)
{
    if (currentOrnament != NULL)
    {
        ERROR("ScoreBuilder: open ornament : ornament already open: {}",
              *currentOrnament);
        return false;
    }

    if (currentSequence == NULL)
    {
        ERROR("ScoreBuilder: open ornament: no sequence open");
        return false;
    }
    // should be set if current sequence is set
    assert(currentScore != NULL);
    assert(currentMeasure != NULL);
    TRACE("ScoreBuilder: measure {}: open ornament", currentMeasure->nb());
    currentOrnament = new Ornament(staff, before);
    assert(! treeStack.empty());
    Inner* parent = treeStack.top();
    assert(parent);
    assert(! parent->complete());
    assert(parent->isDecorated());
    assert((currentOrnamentDate == MTU::ZERO) ||
           (currentOrnamentDate < parent->date())); // on avance
    currentOrnamentDate = parent->date();
    return true;
}


GraceNote* ScoreBuilder::pushGraceNote(int staff,
                                       const Pitch& pitch,
                                       float accident,
                                       const Notehead& head,
                                       bool harm)
{
    assert(staff >= 0);
    if (currentOrnament == NULL)
    {
        ERROR("ScoreBuilder: push grace note {} : no ornament open", pitch);
        return NULL;
    }
    assert(currentScore != NULL);
    assert(currentMeasure != NULL);
    assert(currentSequence != NULL);
    Voice& voice = currentVoice();
    Part& part = currentPart();
    TRACE("ScoreBuilder: measure {}: push grace note {} to voice {} part:{}",
          currentMeasure->nb(), pitch, currentVoiceNb(), part.id());
    GraceNote* event = new GraceNote(currentOrnamentDate, voice, pitch);
    part.add(currentVoiceNb(), staff, event);  // voice.add(event);
    currentOrnament->add(event, staff, accident, head, harm);
    return event;
}


Ornament* ScoreBuilder::closeOrnament()
{
    if (currentOrnament == NULL)
    {
        ERROR("ScoreBuilder: close ornament : no ornament open");
        return NULL;
    }
    
    TRACE("ScoreBuilder: measure {}: close ornament", currentMeasure->nb());
    Ornament* o = currentOrnament;
    assert(currentScore != NULL);
    assert(currentMeasure != NULL);
    assert(currentSequence != NULL);
    assert(! treeStack.empty());
    Inner* parent = treeStack.top();
    assert(parent);
    assert(parent->isDecorated());
    Decorated* deco = dynamic_cast<Decorated*>(parent);
    assert(deco);
    assert(! o->empty());
    deco->add(o);
    
    if (deco->complete())
    {
        treeStack.pop();
        pushNode(deco); // recursively add to grand-parent
    }
    // if not complete, keep it on the stack. the Leaf even must be pushed.
    
    currentOrnament = NULL; // close
    return o;
}


void ScoreBuilder::linkPrevLeaf(Leaf* n)
{
    assert(n);
    assert(n->getPrevious() == prevLeaf);
    assert((prevLeaf == NULL) || prevLeaf->isLeaf());
    if (prevLeaf != NULL)
        prevLeaf->setNext(n);
    prevLeaf = n;
}


bool ScoreBuilder::failLeaf(const std::string& fun) const
{
    if (currentScore == NULL)
    {
        ERROR("ScoreBuilder: {}: no score open", fun);
        return true;
    }
    if (currentMeasure == NULL)
    {
        ERROR("ScoreBuilder: {}: no measure open", fun);
        return true;
    }
    if (currentSequence == NULL)
    {
        ERROR("ScoreBuilder: {}: no sequence open", fun);
        return true;
    }
    if (currentOrnament != NULL)
    {
        ERROR("ScoreBuilder: {}: one ornament open", fun);
        return true;
    }
    if (currentChord != NULL)
    {
        ERROR("ScoreBuilder: {}: one chord open", fun);
        return true;
    }
    return false;
}

Tied* ScoreBuilder::pushTie(size_t mult, int staff)
{
    assert(staff >= 0);
    if (failLeaf("push tie")) return NULL;

    // special case of beginning of score: use pushSpace
    assert(prevLeaf != NULL);
//    if (prevLeaf == NULL)
//    {
//        WARN("ScoreBuilder: tie at beginning ot first measure (voice {} part:{}), cast to space.",
//             currentSequence->voiceNb(), currentPart().id());
//        return pushSpace(mult, staff);
//    }

    AtomicEvent* event = prevLeaf->event();
    assert(event);
    assert(currentMeasure);
    assert(currentSequence);

    TRACE("ScoreBuilder: measure {}: push tie to stack, voice {} part:{}",
          currentMeasure->nb(), currentSequence->voiceNb(), currentPart().id());
    Tied* ret = new Tied(mult, topParent(mult), staff, event, prevLeaf);
    event->extendDuration(ret->duration());
    linkPrevLeaf(ret);
    pushNode(ret);

    return ret;
}


Dot* ScoreBuilder::pushDot(size_t mult, int staff)
{
    assert(staff >= 0);
    if (failLeaf("push dot")) return NULL;
    assert(prevLeaf); // a measure cannot start with a dot.
    assert(currentMeasure != NULL);
    assert(currentSequence != NULL);
    TRACE("ScoreBuilder: measure {}: push dot to stack, voice {} part:{}",
          currentMeasure->nb(), currentSequence->voiceNb(), currentPart().id());
    AtomicEvent* event = prevLeaf->event();
    assert(event);
    Dot* ret = new Dot(mult, topParent(mult), event, prevLeaf);
    event->extendDuration(ret->duration());
    linkPrevLeaf(ret);
    pushNode(ret);
    return ret;
}


LeafNote* ScoreBuilder::pushNote(size_t mult,
                                 int staff,
                                 const Pitch& pitch,
                                 float accident,
                                 const Notehead& h,
                                 const Stem& s,
                                 bool harm,
                                 int tremolo)
{
    assert(staff >= 0);
    if (failLeaf("push note")) return NULL;
    
    LeafNote* ret = new LeafNote(mult,
                                 topParent(mult),
                                 staff,
                                 accident, h, s, harm, tremolo,
                                 prevLeaf);
    // assert(currentMeasure != NULL);  // in failLeaf
    // assert(currentSequence != NULL); // in failLeaf
    Voice& voice = currentVoice();
    Part& part = currentPart();
    assert(voice.number() == currentVoiceNb());
    TRACE("ScoreBuilder: measure {} push note {} in voice {} part:{}",
          currentMeasure->nb(), currentVoiceNb(), part.id());
    Note* event = new Note(ret->start(), ret->duration(), voice, pitch);
    part.add(currentVoiceNb(), staff, event);     // voice.add(event);
    ret->setEvent(event);
    linkPrevLeaf(ret);
    pushNode(ret);
    return ret;
}


LeafRest* ScoreBuilder::pushRest(size_t mult, int staff)
{
    assert(staff >= 0);
    if (failLeaf("push rest")) return NULL;
    
    LeafRest* ret = new LeafRest(mult,
                                 topParent(mult),
                                 staff,
                                 prevLeaf);
    // assert(currentMeasure != NULL);  // in failLeaf
    Part& part = currentPart();
    Voice& voice = currentVoice();
    TRACE("ScoreBuilder: measure {}: push rest in voice {}  part:{}",
          currentMeasure->nb(), voice.number(), part.id());
    Rest* event = new Rest(ret->start(), ret->duration(), voice);
    part.add(currentVoiceNb(), staff, event);  // does voice.add(event);
    ret->setEvent(event);
    linkPrevLeaf(ret);
    pushNode(ret);
    return ret;
}


LeafSpace* ScoreBuilder::pushSpace(size_t mult, int staff)
{
    assert(staff >= 0);
    if (failLeaf("push space")) return NULL;
    
    LeafSpace* ret = new LeafSpace(mult,
                                   topParent(mult),
                                   staff,
                                   prevLeaf);
    Part& part = currentPart();
    Voice& voice = currentVoice();
    TRACE("ScoreBuilder: measure {}: push space in voice {} part:{}",
          currentMeasure->nb(), voice.number(), part.id());
    // The associated musical event is a rest
    Rest* event = new Rest(ret->start(), ret->duration(), voice);
    part.add(currentVoiceNb(), staff, event);  // does voice.add(event);
    ret->setEvent(event);
    linkPrevLeaf(ret);
    pushNode(ret);
    return ret;
}


bool ScoreBuilder::openChord(size_t mult,
                             int staff,
                             const Arpeggio& a,
                             const Stem& s,
                             int tremolo)
{
    assert(staff >= 0);
    if (failLeaf("open chord")) return false;
    currentChord = new LeafChord(mult, topParent(mult),
                                 a, s, tremolo, prevLeaf);
    assert(currentMeasure != NULL); // in failLeaf
    Voice& voice = currentVoice();
    Part& part = currentPart();
    TRACE("ScoreBuilder: measure {}: open chord in voice {} part:{}",
          currentMeasure->nb(), voice.number(), currentPart().id());
    Chord* event = new Chord(currentChord->start(),
                             currentChord->duration(), voice);
    part.add(currentVoiceNb(), staff, event);  // does voice.add(event);
    currentChord->setEvent(event);
    return true;
}


bool ScoreBuilder::addChord(const Pitch& p,
                            int staff,
                            float accident,
                            const Notehead& h,
                            bool harm)
{
    assert(staff >= 0);
    if (currentSequence == NULL)
    {
        ERROR("ScoreBuilder: add chord: no sequence open");
        return false;
    }
    if (currentChord == NULL)
    {
        ERROR("ScoreBuilder: add chord: no chord open");
        return false;
    }
    assert(currentChord->isChord());
    AtomicEvent* event = currentChord->event();
    Chord* chord = dynamic_cast<Chord*>(event);
    assert(chord);
    assert(currentMeasure != NULL);
    TRACE("ScoreBuilder: measure {}: adding {} to chord",
          currentMeasure->nb(), p);
    chord->add(p);
    currentChord->addNote(p, staff, accident, h, harm);
    return true;
}


LeafChord* ScoreBuilder::closeChord()
{
    if (currentSequence == NULL)
    {
        ERROR("ScoreBuilder: close chord: no sequence open");
        return NULL;
    }
    if (currentChord == NULL)
    {
        ERROR("ScoreBuilder: close chord: no chord open");
        return NULL;
    }
    assert(currentMeasure != NULL);
    TRACE("ScoreBuilder: measure {}: close chord in voice {} part:{}",
          currentMeasure->nb(), currentVoiceNb(), currentPart().id());
    LeafChord* lc = currentChord;
    linkPrevLeaf(lc);
    pushNode(lc);
    currentChord = NULL;
    return lc;
}


void ScoreBuilder::pushNode(Node* t)
{
    assert(t);
    assert((! t->isLeaf()) || (t->complete()));

    // add the tree as a child of the current stack top
    if (t->complete())
    {
        assert(! treeStack.empty());
        //ERROR("ScoreBuilder: close Tree : tree stack empty");

        Inner* parent = treeStack.top();
        assert(parent);
        assert(! parent->complete());
        parent->add(t);
        
        if (parent->complete())
        {
            treeStack.pop();
            if (! parent->isRoot())
                pushNode(parent); // recursively add to grand-parent
        }
        // if not complete, it stays in place.
        // no need to pop and repush.
    }
    // push to stack
    else
    {
        assert(t->isInner());
        Inner* ti = dynamic_cast<Inner*>(t);
        assert(ti);
        treeStack.push(ti);
    }
}


Inner* ScoreBuilder::parent() const
{
    if (treeStack.empty())
        return NULL;
    else
        return treeStack.top();
}


bool ScoreBuilder::leftmost() const
{
    Node* parent = this->parent(); // downcast
    if (parent == NULL)
        return true;
    else
    {
        // current node is not first sibling
        if (this->prevSibling() != NULL)
            return false;

        // check all ancesters
        while (parent)
        {
            if (parent->prevSibling() != NULL) // parent not leftmost
                return false;
            parent = parent->parent();
        }
        return true;
    }
}


Node* ScoreBuilder::prevSibling() const
{
    Inner *parent = this->parent();
    if (parent == NULL)
        return NULL;
    else
    {
        size_t i = parent->size();
        if (i == 0)
            return NULL;
        else
            return parent->sub(i - 1);
    }
}


size_t ScoreBuilder::currentSiblingNb() const
{
    Inner* parent = this->parent();
    if (parent == NULL)
    {
        WARN("ScoreBuilder.currentSiblingNb: root node");
        return 0;
    }
    else
    {
        size_t i = parent->completed();
        assert(i <= parent->arity());
        return i;
    }
}


} // end namespace ScoreModel

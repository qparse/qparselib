//
//  ScoringEnv.cpp
//  squanty
//
//  Created by Florent Jacquemard on 30/03/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "ScoringEnv.hpp"


namespace ScoreModel {

ScoringEnv::ScoringEnv(SWTA* a, InputSegment* s, Voicing* v):
ParsingEnv(a, s),
voicing(v)
{
    if (v == NULL)
        WARN("new ScoringEnv with null voicing");
}


ScoringEnv::ScoringEnv(const ScoringEnv& env):
ParsingEnv(env),
voicing(env.voicing)
{ }


/// upgrade
ScoringEnv::ScoringEnv(const ParsingEnv& env, Voicing* v):
ParsingEnv(env),
voicing(v)
{
    if (v == NULL)
        TRACE("ScoringEnv: no voicing");
}


ScoringEnv::~ScoringEnv()
{
    TRACE("delete Scoring Env");
}


} // end namespace ScoreModel




//
//  ScoringEnv.hpp
//  squanty
//
//  Created by Florent Jacquemard on 30/03/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{


#ifndef ScoringEnv_hpp
#define ScoringEnv_hpp

#include <stdio.h>

#include "trace.hpp"
#include "ParsingEnv.hpp"
#include "Voicing.hpp"


namespace ScoreModel {

/// @brief a Scoring Environment is an extention of a Parsing Environment
/// with a Voicing of the embedded InputSegment.
class ScoringEnv : public parsing::ParsingEnv
{
public:
    
    /// base SWTA
    Voicing* voicing;
    
    /// main constructor.
    /// @param wta base SWTA. Can be NULL when not needed.
    /// @param seg an input segment. Can be NULL when not needed.
    /// @param v a voicing of bars. Optional in monophonic case.
    ScoringEnv(SWTA* wta = NULL,
               InputSegment* seg = NULL,
               Voicing* v = NULL);
    
    /// copy constructor.
    ScoringEnv(const ScoringEnv&);

    /// upgrade parsing environment
    /// @param env parsing environment to cast to scoring environment
    /// @param v a voicing of bars. optional in monophonic case.
    ScoringEnv(const ParsingEnv& env, Voicing* v=NULL);
    
    /// @brief delete the pool.
    /// @warning the base SWTA and segment must be delete externaly.
    virtual ~ScoringEnv();

        
};


} // end namespace ScoreModel

#endif /* ScoringEnv_hpp */

/// @}

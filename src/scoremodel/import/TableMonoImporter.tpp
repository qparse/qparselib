//
//  TableMonoImporter.cpp
//  squanty
//
//  Created by Florent Jacquemard on 05/04/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
// This version propagate links for parents and siblings in the tree
// It is useless because it is done automatically by the Score Builder.



//#include "TableMonoImporter.hpp"
#include "Beam.hpp"
#include "Appogiatura.hpp"
#include "Mordent.hpp"
#include "Gruppetto.hpp"
#include "VPosition.hpp"

namespace ScoreModel {


TableMonoImporter::TableMonoImporter(const std::string scorename,
                                     const MeterSig& meter,
                                     const KeySig& key):
_builder(),
_partname("solo"), // default name
_prev_sequence(NULL),
_unfailing(true)
{
    TRACE("TableMonoImporter: Start score:{}", scorename);
    const std::string name = (scorename.empty())?"score":scorename;
    bool res = _builder.openScore(name, meter, key);
    assert(res);
}


TableMonoImporter::~TableMonoImporter()
{
    _builder.closeScore();
}


Score* TableMonoImporter::getScore() const
{
    return _builder.getScore();
}


template<class K>
size_t TableMonoImporter::readPart(const std::string partname,
                                   ScoringEnv& env,
                                   parsing::TableA<K>& tab,
                                   const K& k,
                                   size_t n)
{
    TRACE("TableImporter: read part {} k={}", partname, k);
    //Part& part = _score.part(name);
    //_builder.openPart(partname);
    if (! partname.empty())
        _partname = std::string(partname);
    assert(n >= 1);
    std::shared_ptr<const parsing::Run> r = tab.best(k, n);
    assert(r); // no NULL Run even in case of FAIL
    if (r->fail())
    {
        ERROR("TableMonoImporter: parsing failed. Cannot produce score.");
        return 0;
    }
    
    // inside-the-bar symbol:
    // no bar separator (only one bar)
    if (SymbLabel::inside(r->label()))
    {
        return readMeasure(env, tab, k, r, 0, 0);
    }
    // bar separator symbol (several bars)
    else
    {
        return readRun(env, tab, k, r, 0, 0);
    }
    
    //return _builder.closePart();
}


// cut down bars
template<class K>
size_t TableMonoImporter::readRun(ScoringEnv& env,
                                  parsing::TableA<K>& tab,
                                  const K& k,
                                  std::shared_ptr<const parsing::Run> r,
                                  size_t b,
                                  size_t m)
{
    assert(r);
    assert(! r->fail());
    TRACE("TableImporter[{}]: read run {}", b, *r);
    size_t j = b;
    //size_t ar = r->arity();
    label_t a = r->label();
    assert(! SymbLabel::inside(a)); // not in a measure boundary
    
    // single bar mark; parse 1 measure and the rest of the run (tail recusion)
     if (SymbLabel::bar(a) && (SymbLabel::arity(a) == 2))
     {
         std::shared_ptr<const parsing::RunInner<K>> ir =
         dynamic_pointer_cast<const parsing::RunInner<K>, const parsing::Run>(r);
         assert(ir);
         // first child is a measure
         std::shared_ptr<const parsing::Run> r0 = ir->subrun(0);
         assert(r0); // no NULL Run even in case of FAIL
         if (r0->fail()) // parse fail
         {
             WARN("TableImporter[{}]: parse fail for measure {}", m);
             // we shall read a tree representing an empty bar (failed transcript)
         }
         const K& k0 = ir->key(0);
         j = readMeasure(env, tab, k0, r0, j, m);
         
         // second child is the rest
         std::shared_ptr<const parsing::Run> r1 = ir->subrun(1);
         const K& k1 = ir->key(1);
         assert(r1);
         if (r1->fail())
         {
             ERROR("TableMonoImporter: parsing fail after measure {}. Abort score production.", m);
             return 0;
         }
         return readRun(env, tab, k1, r1, j, m+1);
     }
     // double bar mark = end of run
     else if (SymbLabel::bar(a) && (SymbLabel::arity(a) == 0))
     {
         return j;
     }
     // grace notes
     else if (SymbLabel::grace(a))
     {
         WARN("TableImporter[{}]: grace note binary symbol, not expected", j);
         return j;
     }
     // voice separation
     else if (SymbLabel::fork(a))
     {
         WARN("TableImporter[{}]: voice fork symbol, not expected", j);
         return j;
     }
     else if (SymbLabel::dummy(a))
     {
         WARN("TableImporter[{}]: unexpected dummy symbol", j);
         return j;
     }
     else
     {
         ERROR("TableImporter[{}]: unexpected symbol {}", j, a);
         return j;
     }
    return j;
}


// read one bar
template<class K>
size_t TableMonoImporter::readMeasure(ScoringEnv& env,
                                      parsing::TableA<K>& tab,
                                      const K& k,
                                      std::shared_ptr<const parsing::Run> r,
                                      size_t b,
                                      size_t m)
{
    assert(r);
    INFO("TableImporter[{}]: read measure {} {}",
         b, m, ((r->fail())?"(FAILED)":""));
    size_t j = b;
    label_t a = r->label();
    assert(SymbLabel::inside(a));
    
    /// @todo add possibility of time or key change in new measure
    const MeterSig* ts = NULL;
    const KeySig*  ks = NULL;
    Measure* measure = NULL;
    if (m == 0) // first measure
        measure = _builder.newMeasure(_partname, ts, ks);
    else
        measure = _builder.nextMeasure(ts, ks);
    assert(measure);
    assert(measure->nb() == m);
    
    // find or create the measure for the current part
    // MeasureStack& m = _score.measure(m);
    // Measure& mp = m.ofPart(part);

    // monophonic voicing: only one voice of number 0. no need of external voicing.
    // assert(env.voicing);
    // std::vector<int>& voices = env.voicing->voices(m);
    //assert(voices.size() == 1);
    int v = 0; // voices[0];
    j = readSequence(env, tab, k, r, b, m, v);
    
    return j;

    /// @todo poly : read one tree (sequence) for each voice
    /// for (int v : voices)
    /// we read several time the same tree (starting at the same b)
    /// and make a projection to each voice v
    /// @warning check that j is the same for all voices
}


template<class K>
size_t TableMonoImporter::readSequence(ScoringEnv& env,
                                       parsing::TableA<K>& tab,
                                       const K& k,
                                       std::shared_ptr<const parsing::Run> r,
                                       size_t b,
                                       size_t m,
                                       int v)
{
    assert(r);
    assert(r->fail() || r->complete());
    TRACE("TableImporter[{}]: read sequence: {}", b, *r);
    size_t j = b;
    
    // find previous leaf for the case of tie crossing bar (monophonic case)
    Leaf* pl = NULL;

    // first bar
    if (m == 0)
    {
        assert(_prev_sequence == NULL);
    }
    else
    {
        assert(_prev_sequence); // set after creation of each sequence
        assert(_prev_sequence->isSequence());
        // Measure& prev_measure = _prev_sequence->parent();
        // assert(prev_measure.nb() == m-1);
        // monophonic case: there is exactly one sequence in each measure
        // assert(prev_measure.nbVoices() == 1);
        // SequenceList::const_iterator i = prev_measure.cbegin();
        // assert(i != prev_measure.cend());
        // const Sequence& prev_seq = *i;
        Root* t = _prev_sequence->tree();
        assert(t);
        pl = t->getLastLeaf();
        assert(pl);
        assert(pl->isLeaf());
    }

    bool seqopening = _builder.openSequence(v, pl);
    assert(seqopening);
    // size_t j = b;
    // label_t a = r->label();
    
    // compute multiplicity from a
    // label_t a = r->origin().label();
    // size_t mult = SymbLabel::inmultiplicity(a);
    // not needed for top node (mult fixed to 1)
    size_t mult = 1;
    j  = readTree(env, tab, k, r, mult, j, v, MTU::ONE);
    // mp.addVoice(v, p.second);         // NULL if parsing failed

    _prev_sequence = _builder.closeSequence();
    assert(_prev_sequence);  // current sequence is closed
    assert(_prev_sequence->isSequence());
    return j; /// @todo complete (with return of readNode)
}


template<class K>
size_t TableMonoImporter::readTreeTerminal(ScoringEnv& env,
                                           parsing::TableA<K>& tab,
                                           const K& k,
                                           std::shared_ptr<const parsing::RunTerm> r,
                                           size_t mult,
                                           size_t b,
                                           int voiceNb,
                                           mtu_t dur)
{
    assert(r);
    size_t j = b;
    label_t a = r->label();
    label_t ar = r->arity();
    assert(ar == 0);
    assert(SymbLabel::fail(a) || SymbLabel::inside(a));
    // incoming multiplicity (number of edges incoming to top node)
    // assert(SymbLabel::fail(a) || (SymbLabel::inmultiplicity(a) == mult));
    
    /// @todo IMPORTANT
    /// the nb of input points may be > symbol size
    /// e.g. symbol size = 1 for note but 2 points = 1 grace-rest  (OFF) + 1 note (ON)
    /// we need to formalize this

    size_t n = SymbLabel::nbEvents(a); // was k.size(); k.pointsTerminalLabel(a, env);
 
    if (SymbLabel::fail(a))
    {
        assert(ar == 0); // we do not go in the case inner
        INFO("TableImporter[({}) {}]: read FAIL symbol", k.pre(), loc(j, n));
    }
    else if (SymbLabel::terminal(a))
    {
        assert(ar == 0);
        INFO("TableImporter[({}) {}]: read terminal symbol {}",
             k.pre(), loc(j, n), SymbLabel::to_string(a));
    }
    else
    {
        assert(ar > 0);
        ERROR("TableImporter[({}) {}]: unexpected symbol {} of arity {}",
              k.pre(), loc(j, n), SymbLabel::to_string(a), ar);
    }

    // push top symbol to the tree stack of builder
    return readSymbol(env, a, ar, mult, j, n, voiceNb, dur);
}
    
    
template<class K>
size_t TableMonoImporter::readTreeInner(ScoringEnv& env,
                                        parsing::TableA<K>& tab,
                                        const K& k,
                                        std::shared_ptr<const parsing::RunInner<K>> r,
                                        size_t mult,
                                        size_t b,
                                        int voiceNb,
                                        mtu_t dur)
{
    size_t j = b;
    size_t ar = r->arity();
    assert(ar > 0);
    label_t a = r->label();
    assert(SymbLabel::fail(a) || SymbLabel::inside(a));
    // incoming multiplicity (number of edges incoming to top node)
    assert(SymbLabel::fail(a) || (SymbLabel::inmultiplicity(a) == mult));
    size_t n = 0; // only for terminal runs

    if (SymbLabel::inner(a))
    {
        INFO("TableImporter[({}) {}]: read symbol {} of arity {}",
             k.pre(), loc(j, n), SymbLabel::to_string(a), ar);
    }
    else
    {
        ERROR("TableImporter[({}) {}]: unexpected symbol {} of arity {}",
              k.pre(), loc(j, n), SymbLabel::to_string(a), ar);
    }

    // Preorder Traversal
    
    // push top symbol to the tree stack of builder
    j = readSymbol(env, a, ar, mult, j, n, voiceNb, dur);

    // case of inner symbol
    // recurse in every subrun of r   // cf. LRTFactory and LRT
    mtu_t duri = dur / MTU::of_int(ar);
    Inner* tree = _builder.parent(); // top node of the tree in construction
    assert(tree);
    //size_t i = 0; // number of current sub-run
    // while (! tree->complete())
    for (size_t i = 0; i < r->size(); ++i)
    {
        //DEBUGU("TableImporter[{}]: read tree {} from subrun {}/{}", j, *tree, i, ar);
        // outgoing multiplicity = number of edges outgoing to child i
        
        // multi of origin transition of run (not symbol)
        // size_t multi = SymbLabel::outmultiplicity(a, i);
        size_t multi = r->multiplicity(i);
        const K& ki = r->key(i);
        std::shared_ptr<const parsing::Run> ri = r->subrun(i);
        assert(ri);

        if (! ri->fail())
        {
            j = readTree(env, tab, ki, ri, multi, j, voiceNb, duri);
        }
        // parse fail
        else if (_unfailing) // skip this subrun and continue
        {
            label_t ai = ri->label();
            
            /// @todo j should advance! wee need a run with a special symbol FAIL
            /// containing number of events to skip
            WARN("TableImporter[{}]: FAILED subrun - unfailing mode", j);
            /// @todo TBC only 1 staff for mono case?
            int staff = 0;
            j = readFail(multi, j, SymbLabel::nbEvents(ai), staff);
        }
        else                // fail when NULL subrun
        {
            ERROR("TableImporter[{}]: FAILED subrun - failing mode", j);
            /// @todo behaviour undefined if we close the tree now
            return j;
        }
    }
    INFO("Tree completed:{}, Tree.arity:{}, Run.size:{}",
         tree->completed(), tree->arity(), r->size());
    assert(tree->complete());
    return j;
}


/// @todo suppr. parent, prev (useless ?)
template<class K>
size_t TableMonoImporter::readTree(ScoringEnv& env,
                                   parsing::TableA<K>& tab,
                                   const K& k,
                                   std::shared_ptr<const parsing::Run> r,
                                   size_t mult,
                                   size_t b,
                                   int voiceNb,
                                   mtu_t dur)
{
    assert(r);
    assert(r->fail() || r->complete());
    TRACE("TableImporter[{}]: read tree: {}", b, *r);
    
    if (r->terminal())
    {
        // dynamic_cast has stricter checking than regular cast
        // https://stackoverflow.com/questions/28002/regular-cast-vs-static-cast-vs-dynamic-cast
        std::shared_ptr<const parsing::RunTerm> tr =
        std::dynamic_pointer_cast<const parsing::RunTerm, const parsing::Run>(r);
        assert(tr);
        return readTreeTerminal(env, tab, k, tr, mult, b, voiceNb, dur);
//        return readTreeTerminal(env, tab, k,
//                                (const parsing::RunTerm*) r,
//                                mult, b, voiceNb, dur);
    }
    else
    {
        assert(r->inner());
        std::shared_ptr<const parsing::RunInner<K>> ir =
        std::dynamic_pointer_cast<const parsing::RunInner<K>, const parsing::Run>(r);
        assert(ir);
        size_t cu = readTreeInner(env, tab, k, ir, mult, b, voiceNb, dur);
        return cu;
//        return readTreeTerminal(env, tab, k,
//                                (const parsing::RunInner<K>*) r,
//                                mult, b, voiceNb, dur);
    }
}


size_t TableMonoImporter::readSymbol(ScoringEnv& env,
                                     label_t a,
                                     size_t arity,
                                     size_t mult,
                                     size_t b,
                                     size_t n,   // nb of input symbols read
                                     int voiceNb,
                                     mtu_t dur)
{
    INFO("TableImporter[{}]: read symbol: {}",
          loc(b, n), SymbLabel::to_string(a));

    /// @todo TBC only 1 staff for mono case?
    int staff = 0;
    assert(SymbLabel::inside(a));
    //size_t j = b;
    
    // tie or dot or multiplicity
    if (SymbLabel::dot(a))
    {
        /// @todo check that the dot is in the right place ?
        return readDot(mult, b, n, staff);
    }
    else if (SymbLabel::multi(a))
    {
        return readMulti(mult, b, n, staff);
    }
    else if (SymbLabel::continuation(a)) // other cases: tie or casted tie
    {
        // DEBUGU("TableMonoImporter::readSymbol: Tie");
        return readContinuation(mult, b, n, staff);
    }
    else if (SymbLabel::rest(a))
    {
        return readRest(mult, b, n, staff);
    }
    else if (SymbLabel::note(a))
    {
        assert(! SymbLabel::undef(a));
        assert(! SymbLabel::rest(a));

        // aussi grace rests
        size_t ng = SymbLabel::nbGraceNotes(a);
        if (ng == 0)
        {
            assert(SymbLabel::nbEvents(a) == 1); // 1 note, 0 g.n.
            std::pair<size_t, Node*> p = readNote(env, mult, b, n, staff);
            // assert(p.second);
            return p.first;
        }
        else
        {
            assert(SymbLabel::nbEvents(a) == ng+1); // 1 note, ng g.n.
            return readDecorated(env, mult, b, n, ng, staff);
        }
    }
    else if (SymbLabel::tuple(a))
    {
        return readTuple(a, arity, mult, b, dur);
    }
    else if (SymbLabel::fail(a)) // parse fail
    {
        return readFail(mult, b, n, staff);
    }
    else
    {
        ERROR("TableImporter[{}]: unexpected symbol {}", loc(b, n), a);
        return b+n;
    }
}


size_t TableMonoImporter::readContinuation(size_t mult,
                                           size_t b, size_t n,
                                           int staff)
{
    TRACE("TableImporter[{}]: read continuation mult={} staff={}",
          loc(b, n), mult, staff);
    // this can cast tie to dot or consolidate previous leaf
    _builder.pushTie(mult, staff);
    // return can be NULL in case of consolidation
    return b+n;
}


/// multi node is deleted (merged with previous sibling)
/// @todo push a multi node and reduce it by rewriting ?
size_t TableMonoImporter::readMulti(size_t mult,
                                    size_t b, size_t n,
                                    int staff)
{
    TRACE("TableImporter[{}]: read multi mult={} staff={}",
          loc(b, n), mult, staff);
    Node* ps = _builder.prevSibling();
    assert(ps);
    assert(ps->complete());
    TRACE("TableImporter[{}]: consolidate tie (explicit multi)", loc(b, n));
    ps->incrMultiplicity(mult);
    TRACE("TableImporter[{}]: read multi {}={} mtu",
          loc(b, n), mult, ps->duration());
    return b+n;
}


size_t TableMonoImporter::readTie(size_t mult,
                                  size_t b, size_t n,
                                  int staff)
{
    // case of first leaf in first bar
    // there cannot be a tie at begining of first bar: cast to space
    if ((_prev_sequence == NULL) && _builder.leftmost())
//         ((_builder.parent() == NULL) && (_builder.prevSibling() == NULL))))
    {
        LeafSpace* ln = _builder.pushSpace(mult, staff);
        assert(ln);
        WARN("TableImporter[{}]: read 1 leftmost tie {} mtu mult={} staff={}. Cast to Space",
             loc(b, n), ln->duration(), mult, staff);
        return b+n;
    }
    
    // otherwise create a tie
    Leaf* ln = _builder.pushTie(mult, staff);
    assert(ln);
    TRACE("TableImporter[{}]: read 1 tie {} mtu mult={} staff={}",
          loc(b, n), ln->duration(), mult, staff);
    return b+n;
}


size_t TableMonoImporter::readDot(size_t mult,
                                  size_t b, size_t n,
                                  int staff)
{

    Dot* ln = _builder.pushDot(mult, staff);
    assert(ln);
    TRACE("TableImporter[{}]: read 1 dot {} mtu mult={} staff={}",
          loc(b, n), ln->duration(), mult, staff);
    return b+n;
}


size_t TableMonoImporter::readRest(size_t mult,
                                   size_t b, size_t n,
                                   int staff)
{
    LeafRest* r = _builder.pushRest(mult, staff);
    assert(r);
    TRACE("TableImporter[{}]: read rest {} mtu staff={}",
          loc(b, n), r->duration(), staff);
    return b+n;
}


/// same as readSpace  but do not advance in input stream
size_t TableMonoImporter::readFail(size_t mult,
                                   size_t b, size_t n,
                                   int staff)
{
    TRACE("TableImporter[{}]: parse FAIL, pad with a whitespace staff={}",
          loc(b, n), staff);
    LeafSpace* lr = _builder.pushSpace(mult, staff);
    // failure even when unfailing = true
    // if (lt == NULL) WARN("TableImporter[{}]: readFail fails in unfailing mode", b);
    assert(lr);
    TRACE("TableImporter[{}]: read FAIL {} mtu staff={}",
          loc(b, n), lr->duration(), staff);

    return b+n;
}


std::pair<size_t, Node*>
TableMonoImporter::readNote(ScoringEnv& env,
                            size_t mult,
                            size_t b,
                            size_t n,
                            int staff)
{
    assert(env.segment);
    assert(env.segment->point(b).event());
    MusEvent* evt = nullptr;
    //DEBUGU("event is note: {}", env.segment->point(b).event()->isNote());
    //DEBUGU("event is rest: {}", env.segment->point(b).event()->isRest());

    // find the first note in events b..b+n
    /// @todo the note should be the last event
    for (size_t j = b; j < b+n; ++j)
    {
        DEBUGU("...read event {}/{}", j, n);
        if (env.segment->point(j).event()->isNote())
        {
            DEBUGU("...{}/{} = note {}", j, n, env.segment->point(j).event()->pitch());
            evt = env.segment->point(j).event();
            break;
        }
        else
        {
            DEBUGU("...{}/{} = not note", j, n);
        }
    }
    
    if (evt == nullptr)
    {
        ERROR("TableImporter[{}]: failed to read note", loc(b, n));
//        INFO("segment : {} events", env.segment->size());
//        env.segment->print(std::cout);
        return std::make_pair(b+n, (LeafNote*) nullptr);
    }
    assert(evt->isNote());
    Pitch& p = evt->pitch();  // pitch is copied
    /// @todo no courtesy accident
    /// @todo default Notehead and Stem
    /// @todo no harmonic
    /// @todo no tremolo mark
    LeafNote* ln = _builder.pushNote(mult, staff, p);
    assert(ln);
    TRACE("TableImporter[{}]: read 1 note {} {} mtu, mult={} staff={}",
          loc(b, n), p, ln->duration(), mult, staff);
    return std::make_pair(b+n, ln);
}


size_t TableMonoImporter::readDecorated(ScoringEnv& env,
                                        size_t mult,
                                        size_t b, size_t n,
                                        size_t nb,
                                        int staff)
{
    assert(nb > 0); // at least 1 grace note
    assert(n > nb); // ...and a note (to read)
    Decorated* deco = _builder.pushDecorated(mult);
    assert(deco);
    assert(deco->isDecorated());
    /// here, the ornament is always played before the note.
    /// @todo add a new symbol for ornament after note
    bool of = _builder.openOrnament(false);
    assert(of);
    size_t j = b;
    size_t c = 0; // number of grace note effectively read
    for (; j < b+n-1; ++j) // n > 0
    {
        assert(env.segment);
        assert(j < env.segment->size());
        assert(env.segment->point(j).event());
        
        if (env.segment->point(j).event()->isRest())
            TRACE("TableImporter[{}]: ignore grace rest", j);
        else if (env.segment->point(j).event()->isNote())
        {
            Pitch& p = env.segment->point(j).event()->pitch();
            TRACE("TableImporter[{}]: read grace note {}", j, p);
            GraceNote* g = _builder.pushGraceNote(staff, p);
            assert(g);
            c++;
        }
        else
            ERROR("TableImporter[{}]: unexpected ornament", j);
    }
    if (c != nb)
    {
        ERROR("TableImporter[{}]: read {} grace notes for {} expected",
              loc(b, n), c, nb);
    }
    Ornament* o = _builder.closeOrnament();
    assert(o);
    assert(o->size() == nb);
    TRACE("TableImporter[{}]: read ornament of size {}, mult={} staff={}",
          loc(b, n), nb, mult, staff);

    // the last point should be an onset (note)
    INFO("j={}, b+n={}", j, b+n);
    assert(j == b+n-1); // position of the decorated note
    std::pair<size_t, Node*> p = readNote(env, 1, j, 1, staff);
    j = p.first;
    assert(j == b+n);
    assert(p.second == deco->decorated()); // was returning a pair
    LeafNote* ln = dynamic_cast<LeafNote*>(p.second);
    assert(ln);
    
    // cast ornament to appropriate subtype
    assert(deco->decoration());
    assert(deco->decoration() == o);
    // pitch of the decorated event, for detection of ornament subtype
    const Pitch& pitch = env.segment->point(b+n).event()->pitch();
    Ornament* newo = castOrnament(*o, pitch, ln, staff);
    assert(newo);
    assert(newo->size() == o->size());
    deco->add(newo); // that deletes o
    
    return b+n;  // was a pair with second comp. deco);
}


Ornament* TableMonoImporter::castOrnament(const Ornament& o,
                                          const Pitch& p,
                                          LeafNote* ln,
                                          int staff)
{
    assert(ln);
    assert(o.size() > 0);
    const VPosition vp = VPosition(staff, VPosition::Above);
    //Ornament* newo = NULL;
    if (o.size() == 1)
    {
        TRACE("TableImporter: cast singleton ornament to Accaciatura");
        // Acciaccatura : shape of eigth note, stroke
        return new Appogiatura(o, MTU::make(0,1,2), true);
    }
    else if (o.isUpMordent(p))
    {
        TRACE("TableImporter: cast ornament to up Mordent");
        return new Mordent(o, vp, ln, true);
    }
    else if (o.isLowMordent(p))
    {
        TRACE("TableImporter: cast ornament to low Mordent");
        return new Mordent(o, vp, ln, false);
    }
    else if (o.isGruppetto(p))
    {
        TRACE("TableImporter: cast ornament to Gruppetto");
        return new Gruppetto(o, vp, ln, false);
    }
    else if (o.isInvGruppetto(p))
    {
        TRACE("TableImporter: cast ornament to inverse Gruppetto");
        return new Gruppetto(o, vp, ln, true);
    }
    else
    {
        mtu_t d;
        if (o.size() <= 2)
            d = MTU::make(0,1,2); // eigth note
        else if (o.size() <= 4)
            d = MTU::make(0,1,4); // 16th note
        else
            d = MTU::make(0,1,8); // 32th note

        TRACE("TableImporter: cast ornament to appogiatura (size={})",
              o.size());
        return new Appogiatura(o, d, false);
    }
}


size_t
TableMonoImporter::readTuple(label_t a,
                             size_t arity, size_t mult,
                             size_t b,
                             mtu_t dur)
{
    assert(SymbLabel::inner(a));
    assert(SymbLabel::tuple(a));
    Beam::Info beam;
    /// @todo 2 other possible cases Full and Minus
    if (SymbLabel::beamed(a))
    {
        beam = Beam::Info::Full;
    }
    else
    {
        beam = Beam::Info::None;
    }
    // measure currently open
    Measure* cm = _builder.getMeasure();
    assert(cm);
    const MeterSig* ts = cm->meterSignature();
    assert(ts);
    TupletLabel lab(arity, mult, dur, *ts);
    Tuplet* tn = _builder.pushTuplet(arity, mult, beam, lab);
    assert(tn);
    TRACE("TableImporter[{}]: read {}-uplet (push {} to Builder tree stack)",
          b, arity, SymbLabel::to_string(a));
    return b;
}


std::string TableMonoImporter::loc(size_t b, size_t n)
{
    std::string s1 = std::to_string(b);
    std::string s2 = (n > 1)?std::string("-"):std::string(" ");
    std::string se = std::to_string(b+n-1);
    std::string s3 = (n > 1)?se:std::string(' ', se.size());
    return s1+s2+s3;
}


} // end namespace ScoreModel

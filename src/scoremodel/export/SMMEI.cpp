//
//  SMMEI.cpp
//  squant2
//
//  Created by Florent Jacquemard on 06/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "SMMEI.hpp"


namespace ScoreModel {


using mei::documentToFile;
using mei::MeiDocument;
using mei::MeiElement;
using mei::MeiAttribute;
// using mei::Note;
// using mei::Rest;
using mei::Tie;
// using mei::Beam;


sme_exception sm_error;

MEIExporter::MEIExporter():
_mei_document(new mei::MeiDocument()),
_mei_mdiv(new mei::Mdiv()), // empty, to be completed
//_sm_prevleaf(NULL),
_state(new State(*this)) // initial state
{
    // Create the first levels of the document, down to the `Mdiv` element
    mei::Mei* root = new mei::Mei();
    assert(_mei_document);
    _mei_document->setRootElement(root);
    mei::Music* music = new mei::Music();  // or mei::MeiElement*
    root->addChild(music);
    mei::Body* body = new mei::Body();     // or mei::MeiElement*
    music->addChild(body);
    assert(_mei_mdiv);
    body->addChild(_mei_mdiv);
}


MEIExporter::~MEIExporter()
{
    delete _state; // not for shared_ptr
    /// @todo check doc about deallocation of  elements embedded in _mei_document
}


mei::MeiDocument* MEIExporter::getDocument() const
{
    assert(_mei_document);
    return _mei_document;
}

void MEIExporter::writeInFile(const std::string fname)
{
    TRACE("MEIExport: write to file {}", fname);
    assert(_mei_document);
    documentToFile(_mei_document, fname);
}


void MEIExporter::addParts(const Score& score)
{
    // Now create a new parts, and add it to the current div element
    mei::Parts* mei_parts = exportParts(score);
    assert(mei_parts);
    _mei_mdiv->addChild(mei_parts);
}


void MEIExporter::addScore(const Part& part)
{
    // Create the Score element, as a child of the current Mdiv.
    mei::Score* mei_score = exportScore(part);
    assert(mei_score);
    _mei_mdiv->addChild(mei_score);
}


mei::Parts* MEIExporter::exportParts(const Score& score)
{
    TRACE("MEIExport: convert score {} into MEI <parts> = <part>*",
          score.id());
    mei::Parts* mei_parts = new mei::Parts();
    
    // time-wise exploration of the measures.
    for (PartSeq::const_iterator ip = score.cbegin(); ip != score.cend(); ++ip)
    {
#ifdef SM_MEASURE_STATIC
        Part& part = *ip;
#else
        assert(*ip);
        Part& part = **ip;
#endif
        mei::Part* mei_part = exportPart(part);
        mei_parts->addChild(mei_part);   //ms->accept(this);
    }
        
    return mei_parts;
}


// <Score> = <ScoreDef> <section>
mei::Score* MEIExporter::exportScore(const Part& part)
{
    const Score& score = part.score();
    TRACE("MEIExport: convert part {} of score {} into a MEI <score> = <ScoreDef> <section>",
          part.id(), score.id());

    mei::Score* mei_score = new mei::Score();
    // create the MEI score definition element
    mei::ScoreDef*  mei_scoreDef = exportScoreDef(part);
    // add the score definition as a child of the MEI score element
    assert(mei_scoreDef);
    mei_score->addChild(mei_scoreDef);

    // Now create a new MEI section, and add it to the score element
    mei::Section* mei_section = exportSection(part);
    assert(mei_section);
    mei_score->addChild(mei_section);
    
    return mei_score;
}


mei::Score* MEIExporter::exportScore(const Score& score,
                                     const std::string partname)
{
    assert(! partname.empty());
    const Part& part = score.getPart(partname);
    return exportScore(part);
}


mei::ScoreDef* MEIExporter::exportScoreDef(const Part& part)
{
    TRACE("MEIExport: convert part {} of score {} into a MEI <scoreDef>",
          part.id(), part.score().id());
          
    mei::MeiElement* staffGrp = new mei::StaffGrp();

    for (int iStaff = 0; iStaff < part.nbStaves(); ++iStaff)
    {
        const StaffDef& staff = part.getStaff(iStaff);
        assert(staff.nb() == iStaff);
        mei::StaffDef* staffDef = exportStaffDef(staff);
        staffGrp->addChild(staffDef);
    }

    mei::ScoreDef* scoreDef = new mei::ScoreDef();

    // The meter
    const MeterSig& meter = part.score().meterSignature();
    MeiAttribute *mCount =
        new MeiAttribute("meter.count", std::to_string(meter.getCount()));
    scoreDef->addAttribute(mCount);
    MeiAttribute *mUnit =
        new MeiAttribute("meter.unit", std::to_string(meter.getUnit()));
    scoreDef->addAttribute(mUnit);
    
    scoreDef->addChild(staffGrp);

    return scoreDef;
}


// <staffDef> = @clef.*
/// @todo add meter ?
mei::StaffDef* MEIExporter::exportStaffDef(const StaffDef& staff)
{
    TRACE("MEIExport: extract info from staff {} of part {} into MEI <staffDef>",
          staff.nb(), staff.part().id());
    mei::StaffDef* staffDef = new mei::StaffDef();
    const Ambitus& ambitus = staff.ambitus();

    staffDef->setN(std::to_string(toMEIStaffNb(staff.nb())));

    MeiAttribute* staffLines = new MeiAttribute("lines", "5");
    staffDef->addAttribute(staffLines);
    
    // Clef
    Clef clef = Clef(ambitus.low(), ambitus.high());

    // Clef shape & line
    // see https://music-encoding.org/guidelines/v3/data-types/data.clefshape.html
    if (clef.sign() == 'N')
    {
        MeiAttribute* cShape = new MeiAttribute("clef.shape", "perc");
        staffDef->addAttribute(cShape);
        MeiAttribute* cLine = new MeiAttribute("clef.line", "3");
        staffDef->addAttribute(cLine);
    }
    else
    {
        char s = clef.sign();
        assert((s == 'G') || (s == 'F') || (s == 'C')); // no 0 = None
        MeiAttribute* cShape =
            new MeiAttribute("clef.shape", std::string(1, s));
        staffDef->addAttribute(cShape);

        /// @todo TBC   line 0 = no clef or perc or error.
        assert(1 <= clef.line());
        assert(clef.line() <= 5);
        MeiAttribute* cLine =
            new MeiAttribute("clef.line", std::to_string(clef.line()));
        staffDef->addAttribute(cLine);
    }
    assert(staffDef);
    return staffDef;
}


mei::Part* MEIExporter::exportPart(const Part& part)
{
    TRACE("MEIExport: convert part {} of score {} into a MEI <part> = <ScoreDef> <section>",
          part.id(), part.score().id());

    mei::Part* mei_part = new mei::Part();
    mei::ScoreDef*  mei_scoreDef = exportScoreDef(part);
    assert(mei_scoreDef);
    mei_part->addChild(mei_scoreDef);
    mei::Section* mei_section = exportSection(part);
    assert(mei_section);
    mei_part->addChild(mei_section);
    
    return mei_part;
}


mei::Section* MEIExporter::exportSection(const Part& part)
{
    TRACE("MEIExport: convert part {} of score {} into a MEI <section> = <measure>*",
          part.id(), part.score().id());

    mei::Section* mei_section = new mei::Section();

    for (size_t i = 0; i < part.nbMeasures(); ++i)
    {
        const Measure& measure = part.getMeasure(i);
        assert(measure.nb() == i);
        mei::Measure* mei_measure = exportMeasure(measure);
        assert(mei_measure);
        mei_section->addChild(mei_measure);
    }
    assert(mei_section);
    return mei_section;
}


long MEIExporter::toMEIMeasureNb(long n)
{
    assert(n >= 0);
    return (n + 1);
}


mei::Measure* MEIExporter::exportMeasure(const Measure& measure)
{
    TRACE("MEIExport: convert measure {} of part {} in score {}\
          ({} staves {} voices) into a MEI <measure> = <staff>*",
          measure.nb(), measure.part().id(), measure.part().score().id(),
          measure.nbStaves(), measure.nbVoices());

    mei::Measure* mei_measure = new mei::Measure();
    mei_measure->m_Common.setN(std::to_string(toMEIMeasureNb(measure.nb())));
    
    if (measure.staves.empty())
    {
        WARN("MEIExport: 0 staff in measure {}", measure.nb());
        return mei_measure; // empty measure
    }
    assert(measure.staves.min() != UNDEF_STAFF);
    assert(measure.staves.max() != UNDEF_STAFF);
    assert(_state);
    
    // there will be one layer (voice)
    // for each staff i
    // and each sequence of the measure containing elements in staff i.
    for (int staffnb = measure.staves.min();
         staffnb <=  measure.staves.min(); ++staffnb)
    {
        // use the transducer to visit score model measure and
        // construct the MEI staff
        mei::Staff* mei_staff = _state->visitStaff(measure,
                                                   mei_measure,
                                                   staffnb);
        assert(mei_staff);
        mei_measure->addChild(mei_staff);
        /// @todo the backlinks to Locals
    }

    /// @todo clef or key or ts change
    
    return mei_measure;
}


int MEIExporter::toMEIStaffNb(int n)
{
    assert(n >= 0);
    return (n + 1);
}


int MEIExporter::ofMEIStaffNb(int n)
{
    assert(n > 0);
    return (n - 1);
}


int MEIExporter::staffDiff(int staff, const StaffRange& range)
{
    assert(staff >= 0); // staff number of score model
    assert(range.contains(staff));
    int s0 = toMEIStaffNb(staff); // current MEI staff number
    assert(range.min() >= 0);
    int s1 = toMEIStaffNb(range.min());
    assert(range.max() >= 0);
    int s2 = toMEIStaffNb(range.max());
    
    if (s2 == s1)
    {
        assert(s0 == s1);
        return 0;
    }
    else if (s0 == s1)
    {
        assert(s2 == s1 + 1);
        return 1;
    }
    else
    {
        assert(s0 == s2);
        assert(s2 == s1 + 1);
        return -1;
    }
}


mei::Staff* MEIExporter::exportStaff(int staffnb)
{
    assert(staffnb >= 0);
    mei::Staff* mei_staff = new mei::Staff();
    mei_staff->setN(std::to_string(toMEIStaffNb(staffnb)));
    return mei_staff;
}


int MEIExporter::toMEILayerNb(int n)
{
    assert(n >= 0);
    return (n + 1);
}


int MEIExporter::ofMEILayerNb(int n)
{
    assert(n > 0);
    return (n - 1);
}


mei::Layer* MEIExporter::exportLayer(const Sequence& sequence)
{
    mei::Layer* mei_layer = new mei::Layer();
    assert(sequence.voiceNb() >= 0);
    mei_layer->setN(std::to_string(toMEILayerNb(sequence.voiceNb())));

    return mei_layer;
}


mei::Tuplet* MEIExporter::exportTuplet(const Tuplet& n, int currentStaff)
{
    const TupletLabel& label = n.label();
    const Beam::Info& beaminfo = n.beaming();
    size_t outdegree = n.arity();
    assert(outdegree > 1);
    size_t indegree = n.multiplicity();
    assert(indegree >= 1);

    TRACE("MEIExporter: {}-uplet label={}", n.arity(), label);

    // at least arity or arity:multiplicity will be displayed as MEI tuplet label
    assert((label.multiplicity() == 0) || (label.arity() != 0));
    // assert(label.arity() != 0); // alway true
    mei::Tuplet* mei_tuplet = new mei::Tuplet();
    const StaffRange& range = n.staves();

    if (range.singleton())
    {
        assert(range.contains(currentStaff));
        // no m_Beamedwith to set
    }
    else
    {
        // beams spans over 2 successive staves
        assert(range.size() <= 2);
        int diff = staffDiff(currentStaff, range);
        assert((diff == -1) || (diff == 1));
        // if (diff == 0) // should not happen (case singleStaff)
        /// values for `m_Beamedwith` :
        /// @see https://music-encoding.org/guidelines/v4/data-types/data.otherstaff.html
        if (diff == -1)
            mei_tuplet->m_Beamedwith.setBeamWith("below");
        else
        {
            assert(diff == 1);
            mei_tuplet->m_Beamedwith.setBeamWith("above");
        }
    }

    // when unbeamed, always add a beacket
    assert(beaminfo != Beam::Info::Undef);
    if (beaminfo == Beam::Info::None)
        mei_tuplet->m_TupletVis.setBracketVisible("true");
    else
        mei_tuplet->m_TupletVis.setBracketVisible("false");

    // label visible, otherwise it is not worth creating a tuplet element?
    mei_tuplet->m_TupletVis.setDurVisible("true");

    assert((label.arity() == 0) || (label.arity() == outdegree));
    assert((label.multiplicity() == 0) || (label.multiplicity() == indegree));

    /// @todo check that this mixin is used for defining the a:m (a in the time of m) label.
    mei_tuplet->m_DurationRatio.setNum(std::to_string(outdegree));
    mei_tuplet->m_DurationRatio.setNumbase(std::to_string(indegree));

    // denominator (numbase) invisible
    if (label.multiplicity() == 0)
        mei_tuplet->m_TupletVis.setNumFormat("count");
    else  // denominator (numbase) visible
        mei_tuplet->m_TupletVis.setNumFormat("ratio");

    return mei_tuplet;
}


mei::Beam* MEIExporter::exportBeam()
{
    mei::Beam* mei_beam = new mei::Beam();
    return mei_beam;
}


mei::Beam* MEIExporter::exportBeam(int staff, const StaffRange& range)
{
    TRACE("MEIExporter: new MEI Beam");
    mei::Beam* mei_beam = new mei::Beam();
    
    if (range.singleton())
    {
        assert(range.contains(staff));
        return mei_beam;
    }
       
    // beams spans over 2 successive staves
    assert(range.size() <= 2);
    int diff = staffDiff(staff, range);
    // assert((diff == -1) || (diff == 1));
    if (diff == 0) // should not happen (case singleStaff)
        return mei_beam;
    /// values for `m_Beamedwith` :
    /// @see https://music-encoding.org/guidelines/v4/data-types/data.otherstaff.html
    else if (diff == -1)
        mei_beam->m_Beamedwith.setBeamWith("below");
    else
    {
        assert(diff == 1);
        mei_beam->m_Beamedwith.setBeamWith("above");
    }
    return mei_beam;
}


mei::Beam* MEIExporter::exportBeam(const Tuplet& n, int currentStaff)
{
    assert(n.beaming() != Beam::Info::Undef);
    assert(n.beaming() != Beam::Info::None);

    const StaffRange& range = n.staves();
    assert(range.size() <= 2);
    return exportBeam(currentStaff, range);
}


mei::Beam* MEIExporter::exportBeam(const Appogiatura& n, int currentStaff)
{
    assert(n.size() > 1);
    mei::Beam* mei_beam = exportBeam(currentStaff, n.staves());
    assert(mei_beam);

    if (n.isAcciaccatura())
    {
        // mei_beam->m_Beamrend.setSlash("true");
       ///@todo no Slash attribute for beams in libMEI ?
        WARN("MEIExporter: no Slash attribute for beams in libMEI");
    }

    return mei_beam;
}


cmnd_t MEIExporter::exportDuration(const Node& node)
{
    // meter signature of the embedding measure (every node belongs to a measure)
    const MeterSig* ts = node.meterSignature();
    assert(ts);
    // convertion of duration from MTU (nb of bars) into CMN
    mtu_t d = node.duration();
    assert(d != MTU::ZERO);
    cmnd_t CMN_dur = ts->MTUtoCMN(d);
    return CMN_dur;
}


unsigned long MEIExporter::exportInterFlags(const Tuplet& n)
{
    // total duration of the tuplet in MTU
    mtu_t d = n.duration();
    // total duration of the tuplet in CMD
    // cmnd_t dur = exportDuration(n);
    // duration of a child
    // cmnd_t cdur = dur / n.arity();
    const MeterSig* ts = n.meterSignature();
    cmnd_t cd = ts->MTUtoCMN(d / n.arity());
    assert(CMN::defined(cd));
    // number of flags or beams for each child
    unsigned long nb = CMN::to_flags(cd);
    assert(0 <= nb);
    assert(nb <= CMN::MAX_FLAGS);
    return nb;
}


/// @todo case of courtesy accidental : element accidental?
/// @see https://music-encoding.org/guidelines/v4/elements/accid.html
mei::Note* MEIExporter::exportPitch(mei::Note* mei_note, const Pitch& pitch)
{
    assert(mei_note);
    
    std::string name;
    name += tolower(pitch.name);
    mei_note->m_Pitch.setPname(name);
    mei_note->m_Octave.setOct(std::to_string(pitch.octave));

    // do not print alteration
    if ((! pitch.altprint) ||
        (pitch.alteration == Pitch::UNDEF_NOTE_ALTERATION))
        return mei_note;
    // do print alteration
    else if (pitch.alteration ==  3.0)
        mei_note->m_Accidental.setAccid("xs"); // = x# alt "sx"=#x, "ts"=###
    else if (pitch.alteration ==  2.0)
        mei_note->m_Accidental.setAccid("x");  // alt "ss"=##
    else if (pitch.alteration ==  1.5)
        mei_note->m_Accidental.setAccid("su"); // alt "3qs"
    else if (pitch.alteration ==  1.0)
        mei_note->m_Accidental.setAccid("s");  // = #
    else if (pitch.alteration ==  0.5)
        mei_note->m_Accidental.setAccid("sd"); // alt "nu", "1qs"
    else if (pitch.alteration ==  0.0)
        mei_note->m_Accidental.setAccid("n");  // natural
    else if (pitch.alteration == -0.5)
        mei_note->m_Accidental.setAccid("fu"); // alt "nd", "1qf"
    else if (pitch.alteration == -1.0)
        mei_note->m_Accidental.setAccid("f");  // = b
    else if (pitch.alteration == -1.5)
        mei_note->m_Accidental.setAccid("fd"); // alt "3qf"
    else if (pitch.alteration == -2.0)
        mei_note->m_Accidental.setAccid("ff"); // = bb
    else if (pitch.alteration == -3.0)
        mei_note->m_Accidental.setAccid("tf"); // = bbb
    else
        ERROR("MEIExporter: unexpected alteration {}", pitch.alteration);
    
    return mei_note;
}


/// @todo delete
mei::Note* MEIExporter::exportNote(const Note& note, const MeterSig& ts)
{
    TRACE("MEIExport: convert note {} at {} into a MEI <note>",
          note, note.date());
    assert(note.isNote());
    //note.accept(this); // visit the atomic event

    const Pitch& pitch = note.pitch();

    mei::Note* mei_note = new mei::Note();
    exportPitch(mei_note, pitch);
    
    // duration: inappropriate in case of dotted or tied note
    cmnd_t CMN_dur = ts.MTUtoCMN(note.duration());
    mei_note->m_DurationMusical.setDur(std::to_string(CMN_dur));

    return mei_note;
}


mei::Note* MEIExporter::exportNote(const LeafNote& node)
{
    TRACE("MEIExport: convert note at {} into a MEI <note>", node.date());
    
    AtomicEvent* ae = node.event();
    assert(ae);
    assert(ae->isNote());
    //aen->accept(this); // visit the atomic event

    Note* aen = dynamic_cast<Note*>(ae);
    assert(aen);
    const Pitch& pitch = aen->pitch();
    
    mei::Note* mei_note = new mei::Note();
    exportPitch(mei_note, pitch);

    cmnd_t CMN_dur = exportDuration(node);
    mei_note->m_DurationMusical.setDur(std::to_string(CMN_dur));
    /// @todo notehead m_Noteheads
    /// @todo complete m_Tupletpresent->setTuplet() ?
    /// @todo complete m_Beamed->setBeam() ?

    return mei_note;
}


// pitch of given mei_pnote and duration of tie
mei::Note* MEIExporter::exportTiedNote(mei::Note* mei_pnote, const Tied& tie)
{
    assert(mei_pnote);
    mei::Note* mei_note = new mei::Note();
    
    mei_note->m_Pitch.setPname(mei_pnote->m_Pitch.getPname()->getValue());
    mei_note->m_Octave.setOct(mei_pnote->m_Octave.getOct()->getValue());
    // no accident for tied note
    //mei_note->m_Accidental.setAccid(mei_pnote->m_Accidental.getAccid()->getValue());

    long CMN_dur = exportDuration(tie);
    mei_note->m_DurationMusical.setDur(std::to_string(CMN_dur));

    return mei_note;
}


mei::Rest* MEIExporter::exportRest(const LeafRest& node)
{
    mei::Rest* mei_rest = new mei::Rest();
    mei_rest->m_DurationMusical.setDur(std::to_string(exportDuration(node)));

    return mei_rest;
}


// rest param (previous rest) is useless
mei::Rest* MEIExporter::exportRest(mei::Rest* rest, const Tied& tie)
{
    mei::Rest* mei_rest = new mei::Rest();
    mei_rest->m_DurationMusical.setDur(std::to_string(exportDuration(tie)));

    return mei_rest;
}


mei::Chord* MEIExporter::exportTiedChord(mei::Chord* mei_pchord, const Tied& tie)
{
    assert(mei_pchord);

    mei::Chord* mei_chord = new mei::Chord();
    
    WARN("MEIExporter: exportTied of Chord not implemented");
    // copy of every note of mei_pchord with exportTied
    mei_chord->m_DurationMusical.setDur(std::to_string(exportDuration(tie)));

    return mei_chord;
}


// need specific exportSpace for LeafNote, LeafRest etc?
mei::Space* MEIExporter::exportSpace(const Node& node)
{
    TRACE("MEIExport: convert node at {} into a MEI <space>", node.date());
    
    mei::Space* mei_space = new mei::Space();
    long CMN_dur = exportDuration(node);
    mei_space->m_DurationMusical.setDur(std::to_string(CMN_dur));
    /// @todo complete m_Tupletpresent->setTuplet() ?
    /// @todo complete m_Beamed->setBeam() ?
    return mei_space;
}


mei::Space* MEIExporter::exportTiedSpace(const Tied& tie)
{
    mei::Space* mei_space = new mei::Space();
    mei_space->m_DurationMusical.setDur(std::to_string(exportDuration(tie)));

    return mei_space;
}


mei::Note* MEIExporter::exportNote(const PitchedNote& n)
{
    TRACE("MEIExport: convert pitched note {} into a MEI <note>", n);

    mei::Note* mei_note = new mei::Note();
    exportPitch(mei_note, n.pitch());

    return mei_note;
}


mei::Note* MEIExporter::exportGraceNote(const SingleGraceNote& node,
                                        bool before, cmnd_t dur, bool stroke)
{
    TRACE("MEIExport: convert grace note {} into a MEI <note>");
    
    mei::Note* mei_note = new mei::Note();
    exportPitch(mei_note, node.pitch());

    if (before)
        mei_note->m_Graced.setGrace("unacc"); // Time "stolen" from previous note.
    else
        mei_note->m_Graced.setGrace("acc"); // Time "stolen" from following note.
    // other option  mei_note->m_Graced.setGrace("unknown");

    assert(dur >= 4); // should be shorter than quarter note
    mei_note->m_DurationMusical.setDur(std::to_string(dur));

    if (stroke)
        mei_note->m_Stems.setStemMod("1slash");

    return mei_note;
}


mei::Note* MEIExporter::exportGraceNote(const Appogiatura& app)
{
    TRACE("MEIExport: convert grace note {} into a MEI <note>");
    assert(app.size() == 1);
    const SingleGraceNote* n = app.at(0);
    assert(n);
    return exportGraceNote(*n, app.playBefore(), app.printedDuration(),
                           app.isAcciaccatura());
}


mei::Beam* MEIExporter::exportGraceGrp(const Appogiatura& app)
{
    TRACE("MEIExport: convert group of {} grace notes into a MEI <beam>",
          app.size());
    WARN("MEIExport: no lib object for MEI element `<GraceGrp>`?");
    size_t nb = app.size();
    assert(nb > 1);
    bool before = app.playBefore();
    bool stroke = app.isAcciaccatura();
    cmnd_t dur = app.printedDuration();
    
    mei::Beam* mei_beam = new mei::Beam();
    
    for (size_t i = 0; i < nb; ++i)
    {
        const SingleGraceNote* n = app.at(0);
        assert(n);
        // stroke only for the last grace note
        mei::Note* mei_gn = exportGraceNote(*n, before, dur,
                                            (stroke && (i == nb - 1)));
        assert(mei_gn);
        mei_beam->addChild(mei_gn);
    }
    
    return mei_beam;
}


mei::Mordent* MEIExporter::exportMordent(const Mordent& mordent,
                                         const std::string id)
{
    TRACE("MEIExport: convert mordent {} into a MEI <mordent>", mordent);
    mei::Mordent* mei_mordent = new mei::Mordent();
    assert(! id.empty());
    mei_mordent->m_Startid.setStartid(id);

    
    if (mordent.isUpMordent())
        mei_mordent->m_MordentLog.setForm("upper");
    else if (mordent.isLowMordent())
        mei_mordent->m_MordentLog.setForm("lower");
    else
        WARN("MEIExporter.exportMordent: unknown kind of mordent");
    
    /// @todo Attibute Long (bool) of m_MordentLog
    /// @todo Attibute Accidupper of m_Ornamentaccid
    /// @todo Attibute Accidlower of m_Ornamentaccid
    
    return mei_mordent;
}


mei::Turn* MEIExporter::exportTurn(const Gruppetto& n, const std::string id)
{
    TRACE("MEIExport: convert gruppetto into a MEI <turn>");
    mei::Turn* mei_turn = new mei::Turn();
    assert(! id.empty());
    mei_turn->m_Startid.setStartid(id);

    if (n.isGruppetto())
        mei_turn->m_TurnLog.setForm("upper");
    else if (n.isGruppetto())
        mei_turn->m_TurnLog.setForm("lower");
    else
        WARN("MEIExporter.exportTurn: unknown kind of turn");

    /// @todo Attibute Delayed (bool) of m_TurnLog
    /// @todo Attibute Accidupper of m_Ornamentaccid
    /// @todo Attibute Accidlower of m_Ornamentaccid
    
    return mei_turn;
}


mei::Trill* MEIExporter::exportTrill(const LocalTrill& n, const std::string id)
{
    TRACE("MEIExport: convert gruppetto into a MEI <turn>");
    WARN("exportTrill NOT COMPLETE");
    mei::Trill* mei_trill = new mei::Trill();

    /// @todo complete

    return mei_trill;

}


//mtu_t MEIExporter::currentDate() const
//{
//    if (_sm_prevleaf == NULL)
//    {
//        return MTU::ZERO;
//    }
//    else
//    {
//        return (_sm_prevleaf->date() + _sm_prevleaf->duration());
//    }
//}


//bool MEIExporter::checkStaff(const Leaf& node) const
//{
//    if (_current_staff < 0)
//    {
//        WARN("MEIExport: no staff currently proceeded");
//        return false;
//    }
//
//    return ((node.staff() == _current_staff));
//}



















/**
 *
 * Transducer from Score Model into MEI
 *
 */


MEIExporter::State::State(MEIExporter& e):
_exporter(e),
_staffNb(UNDEF_STAFF), // initially undef
_hierorchy(),   // empty satck
_mei_pnote(NULL),
_mei_ppnote(NULL),
_mei_prest(NULL),
_mei_pchord(NULL),
_mei_ppchord(NULL),
_mei_pspace(NULL),
_dots(0),
_ptied(false),
_pbeaming()
{ }


// the MEI objects are deallocated alsewhere
MEIExporter::State::~State()
{
    while (! _hierorchy.empty())
        _hierorchy.pop(); // calls destructor of removed element
}


MEIExporter::State::BeamParam::BeamParam(Beam::Info i, unsigned long d):
info(i),
depth(d)
{
    assert(i != Beam::Info::Undef);
}


/**
 *
 * Visitor functions
 *
 */


// Open a MEI score and a MEI section from the given score object.
// The given Score is stored as the current Score.
// If there was already a current score, it is replaced by the given one.
void MEIExporter::State::visitScore(const Score* score)
{
    assert(score);
    ERROR("MEIExport: visitScore should not be called");
}


// Access the first measure of the given part
// in the SM score currently open.
// A SM Score must have been open by visit(Score).
void MEIExporter::State::visitPart(const Part* p)
{
    assert(p);
    ERROR("MEIExport: visitPart should not be called");
}


/// @todo complete
void MEIExporter::State::visitVoice(const Voice* v)
{
    assert(v);
    ERROR("MEIExport: visitVoice should not be called");
}


void MEIExporter::State::visitNote(const Note* n)
{
    assert(n);
    ERROR("MEIExport: visitNote should not be called");
}


void MEIExporter::State::visitGraceNote(const GraceNote* gn)
{
    assert(gn);
    ERROR("MEIExport: visitGraceNote should not be called");
}


void MEIExporter::State::visitRest(const Rest* r)
{
    assert(r);
    ERROR("MEIExport: visitRest should not be called");
}


void MEIExporter::State::visitChord(const Chord* n)
{
    assert(n);
    ERROR("MEIExport: visitChord should not be called");
}


void MEIExporter::State::visitGraceChord(const GraceChord* n)
{
    assert(n);
    ERROR("MEIExport: visitGraceChord should not be called");
}


void MEIExporter::State::visitTrill(const Trill* t)
{
    assert(t);
    ERROR("MEIExport: visitTrill should not be called");
}


void MEIExporter::State::visitMeasureStack(const MeasureStack* ms)
{
    assert(ms);
    ERROR("MEIExport: visitMeasureStack should not be called");
}


void MEIExporter::State::visitMeasureStacked(const MeasureStacked* m)
{
    assert(m);
    visitMeasure(m);
}


void MEIExporter::State::visitMeasure(const Measure* m)
{
    assert(m);
    ERROR("MEIExport: visitMeasure should not be called");
}


// not a Visitor function
mei::Staff* MEIExporter::State::visitStaff(const Measure& measure,
                                           mei::Measure* mei_measure,
                                           int staffnb)
{
    TRACE("MEIExport[in]: Staaff {}, measure {} part {}",
          staffnb, measure.nb(), measure.part().id());
    assert(mei_measure);
    assert(staffnb >= 0);
    assert(measure.staves.contains(staffnb));
    
    mei::Staff* mei_staff = openStaff(mei_measure, staffnb);
    
    // process all voices in measure and staff
    for (SequenceSeq::const_iterator is = measure.cbegin();
         is != measure.cend(); ++is)
    {
#ifdef SM_MEASURE_STATIC
        const Sequence& sequence = *is;
#else
        assert(*is);
        const Sequence& sequence = **is;
#endif
        // sequence contains the staff
        if (sequence.staves().contains(staffnb))
            sequence.accept(this);
        // otherwise do nothing (no voice for the sequence in the staff)
    }
    
    closeStaff();
    return mei_staff;
}


MEIExporter::State::Env::Env(MEIContainerType t, mei::MeiElement* c, size_t s):
type(t),
container(c),
step(s)
{
    assert(c);
}


/// do not deallocate the MEI container
MEIExporter::State::Env::~Env()
{ }


MEIExporter::State::EnvStaff::EnvStaff(mei::Staff* c, size_t n, mei::Measure* m):
Env(STAFF_TYPE, c, n),
mei_measure(m)
{
    assert(c);
    assert(m);
}


MEIExporter::State::EnvStaff::~EnvStaff()
{ }



mei::Staff* MEIExporter::State::openStaff(mei::Measure* mei_measure,
                                          int n)
{
    // update state
    
    assert(mei_measure);
    assert(n >= 0);
    assert(_staffNb == UNDEF_STAFF); // no staff currently open
    _staffNb = n;
    
    mei::Staff* mei_staff = _exporter.exportStaff(n);
    assert(mei_staff);
    // do not write mei_staff (it is returned to the exporter)
 
    // staff is always the top level of the stack
    assert(_hierorchy.empty());
    _hierorchy.emplace(std::make_unique<Env>(STAFF_TYPE, mei_staff, n));
 
    return mei_staff;
}


int MEIExporter::State::currentStaffNb() const
{
    return _staffNb;
}


void MEIExporter::State::closeStaff()
{
    assert(_staffNb >= 0);
    _staffNb = UNDEF_STAFF;
    
    assert(! _hierorchy.empty());
    assert(_hierorchy.top().get());
    assert(_hierorchy.top().get()->type == STAFF_TYPE);

    // add the annotations stored in buffer to the current measure,
    // after the MEI staff element.
    assert(_hierorchy.top().get()->type == STAFF_TYPE);
    EnvStaff* es = static_cast<EnvStaff*>(_hierorchy.top().get());
    assert(es);
    mei::Measure* mei_measure = es->mei_measure;
    assert(mei_measure);
    while (! _buffer.empty())
    {
        mei::MeiElement* e = _buffer.front(); // oldest element
        assert(e);
        mei_measure->addChild(e);
        _buffer.pop();
    }
    _hierorchy.pop();
    assert(_hierorchy.empty());
}


void MEIExporter::State::visitSequence(const Sequence* s)
{
    assert(s);
    assert(s->isSequence());
    // ERROR("MEIExport: visitSequence should not be called");
    const Sequence& sequence = *s;

    TRACE("MEIExport: convert sequence {}/{}, staff {}/{} of measure {},\
          part {} in score {} into a MEI <layer>",
          sequence.voiceNb(), sequence.measure().nbVoices(), // layer nb
          _staffNb,
          sequence.measure().nbStaves(),
          sequence.measure().nb(),
          sequence.measure().part().id(),
          sequence.measure().part().score().id());

    Root* t = sequence.tree();
    assert(t);
    assert(t->isRoot());
    
    // record layer in current state
    openLayer(sequence);
    // visit and export all the content (tree) of the sequence
    // dispatch (it should always be a Root)
    t->accept(this);
    closeLayer();
}


void MEIExporter::State::openLayer(const Sequence& sequence)
{
    mei::Layer* mei_layer = _exporter.exportLayer(sequence);
    write(mei_layer);
    _hierorchy.emplace(std::make_unique<Env>(LAYER_TYPE, mei_layer,
                                             sequence.voiceNb()));
}


void MEIExporter::State::closeLayer()
{
    assert(! _hierorchy.empty());
    assert(_hierorchy.top().get());
    assert(_hierorchy.top().get()->type == LAYER_TYPE);
    _hierorchy.pop();
}


void MEIExporter::State::visitRoot(const Root* r)
{
    assert(r);
    TRACE("MEIExport[in]: Root of sequence {} measure {} part {}",
          r->sequence().voiceNb(),
          r->measure().nb(),
          r->measure().part().id());
    assert(r->complete());
    assert(r->node());
    
    // proceed directly with the encapsulated tree
    r->node()->accept(this); // dispatch
}


void MEIExporter::State::visitTuplet(const Tuplet* n)
{
    assert(n);
    TRACE("MEIExport[in]: {}-uplet, sequence {} at {}",
          n->arity(), n->sequence().voiceNb(), n->date());
    assert(n->complete());

    // no beam: do not construct a MEI beam element
     /// @todo mv to visitTuplet    const Beam::Info& beaminfo = n.beaming();
    const Beam::Info& beaminfo = n->beaming();
    assert(beaminfo != Beam::Info::Undef);
    
    // we may start an new MEI beam or MEI tuplet or both or none.
    // possible combinations in MEI:
    // - for a beam without tuplet number : <beam>
    // - for a beam with tuplet number : <beam> <tuplet>
    // - for a bracketed tuplet number without beam : <tuplet>
    // - for none of them : the notes are added to the open layer.
    bool bo = openBeam(*n);
    bool to = openTuplet(*n);

    // descend into the subtrees
    size_t nbc = n->size();
    assert(nbc > 1);
    for (size_t i = 0; i < nbc; ++i)
    {
        assert(n->sub(i));
        n->sub(i)->accept(this);
    }
    
    // if to and bo : a beamed tuplet was open
    // if to and not bo : an unbeamed tuplet was open
    if (to) { closeTuplet(bo); }
    if (bo) { closeBeam();   }
}


MEIExporter::State::EnvBeam::EnvBeam(MEIContainerType t,
                                     mei::MeiElement* c,
                                     size_t s,
                                     Beam::Info bi,
                                     unsigned long d):
Env(t, c, s),
beaminfo(bi),
depth(d)
{
    assert(c);
}


MEIExporter::State::EnvBeam::~EnvBeam()
{}


bool MEIExporter::State::openBeam(const Tuplet& n)
{
    // tuplet on another staff
    if (! n.staves().contains(_staffNb))
        return false;

    const Beam::Info& beaminfo = n.beaming();
    assert(beaminfo != Beam::Info::Undef);
   
    // no beam: do not construct a MEI beam element
    if (beaminfo == Beam::Info::None)
    {
        // check that the beaming policy propagates bottom-up
        assert(_hierorchy.empty() ||
               (_hierorchy.top().get()->type == LAYER_TYPE) ||
               (_hierorchy.top().get()->type == TUPLETU_TYPE));
        return false;
    }

    if (n.staves().size() > 2)
    {
        ERROR("MEIExporter: beam spanning over more than 2 staves not supported");
        throw sm_error;
    }

    // there should be at least a layer in the stack
    assert(! _hierorchy.empty());
    Env* c = _hierorchy.top().get();
    assert(c);
    assert(c->container);
    assert(_staffNb >= 0);

    // container is top layer or unbeamed tuplet
    // create new MEI beam element, write it to MEI doc
    // and update state with it and its info
    if ((c->type == LAYER_TYPE) || (c->type == TUPLETU_TYPE))
    {
        TRACE("MEIExporter: top beam");
        mei::Beam* mei_beam = _exporter.exportBeam(n, _staffNb);
        write(mei_beam);
        unsigned long depth = _exporter.exportInterFlags(n);
        _hierorchy.emplace(std::make_unique<EnvBeam>(BEAMED_TYPE,
                                                     mei_beam,
                                                     0, // step
                                                     beaminfo,
                                                     depth));
        // we do not update _pbeaming after push
        return true;
    }

    // nested beam : repush a copy of current top container,
    // with new beam info and depth value
    // we do not write a new MEI beam element: no nested beams in MEI!
    // @see https://music-encoding.org/guidelines/v4/content/cmn.html#cmnBeams
    // @see https://lists.uni-paderborn.de/pipermail/mei-l/2014/001339.html
    else if ((c->type == BEAMED_TYPE) || (c->type ==  TUPLETB_TYPE))
    {
        TRACE("MEIExporter: nested beam (not exported)");
        unsigned long depth = _exporter.exportInterFlags(n);
        _hierorchy.emplace(std::make_unique<EnvBeam>(BEAMED_TYPE,
                                                     c->container,
                                                     0, // step,
                                                     beaminfo,
                                                     depth));
        // we do not update _pbeaming after push
        return true;
    }
    else
    {
        ERROR("MEIExporter: open Beam: unexpected state {}.{}",
              c->type, c->step);
        throw sm_error;
    }
}

/// @todo add TUPLET_TYPE  (beamed tuplet)
// close beam or unbeamed tuplet
void MEIExporter::State::closeBT(MEIContainerType t)
{
    assert((t == BEAMED_TYPE) ||
           (t == TUPLETU_TYPE) ||
           (t == TUPLETB_TYPE));
    assert(! _hierorchy.empty());
    assert(_hierorchy.top());
    assert(_hierorchy.top()->type == t);
    _hierorchy.pop(); // does not delete the embedded MeiElement

    // update _pbeaming after pop
    assert(! _hierorchy.empty()); // there should be at least a layer
    Env* c = _hierorchy.top().get();
    assert(c);
    
    if ((c->type == LAYER_TYPE) || (TUPLETU_TYPE))
    {
        _pbeaming = BeamParam(Beam::Info::None, 0);
    }
    /// @todo add case TUPLET_TYPE  (beamed tuplet)
    else
    {
        assert(c->type == BEAMED_TYPE);
        EnvBeam* eb = static_cast<EnvBeam*>(c);
        assert(eb);
        assert(eb->beaminfo != Beam::Info::Undef);
        _pbeaming = BeamParam(eb->beaminfo, eb->depth);
    }
}


void MEIExporter::State::closeBeam()
{
    closeBT(BEAMED_TYPE);
}


bool MEIExporter::State::openTuplet(const Tuplet& n)
{
    // tuplet on another staff
    if (! n.staves().contains(_staffNb))
        return false;

    const TupletLabel& label = n.label();

    // no label to display, it is not worth building a MEI tuplet!
    if ((label.arity() == 0) && (label.multiplicity() == 0))
        return false;
    
    
    if (n.staves().size() > 2)
    {
        ERROR("MEIExporter: tuplet spanning over more than 2 staves not supported");
        throw sm_error;
    }

    // there should be at least a layer in the stack
    assert(! _hierorchy.empty());
    Env* c = _hierorchy.top().get();
    assert(c);
    assert(_staffNb >= 0);

    // create new mei tuplet
    // add it to current MEI container
    // and update state with it
    mei::Tuplet* mei_tuplet = _exporter.exportTuplet(n, _staffNb);
    write(mei_tuplet);

    // container is top layer or unbeamed tuplet
    // create new MEI tuplet element (unbeamed) and update state
    if ((c->type == LAYER_TYPE) || (c->type == TUPLETU_TYPE))
    {
        TRACE("MEIExporter: unbeamed tuplet");
        _hierorchy.emplace(std::make_unique<Env>(TUPLETU_TYPE, mei_tuplet, 0));
        return true;
    }
    // MEI beamed tuplet = MEI tuplet nested in MEI beam
    else if (c->type == BEAMED_TYPE)
    {
        TRACE("MEIExporter: beamed tuplet");
        EnvBeam* eb = static_cast<EnvBeam*>(c);
        assert(eb);
        /// @todo change to TUPLET_TYPE  (beamed tuplet)
        _hierorchy.emplace(std::make_unique<EnvBeam>(TUPLETB_TYPE,
                                                     mei_tuplet,
                                                     0, // step,
                                                     eb->beaminfo,
                                                     eb->depth));
        // we do not update _pbeaming after push
        return true;
    }
    else
    {
        ERROR("MEIExporter: open Tuplet: unexpected state {}.{}",
              c->type, c->step);
        throw sm_error;
    }
}

/// @todo case TUPLET_TYPE  (beamed tuplet)
void MEIExporter::State::closeTuplet(bool beamed)
{
    if (beamed)
        closeBT(TUPLETB_TYPE);
    else
        closeBT(TUPLETU_TYPE);
}


/// @todo complete
void MEIExporter::State::visitClef(const Clef* c)
{
    assert(c);
    ERROR("MEIExport: visitClef should not be called");
}


/// @todo complete
void MEIExporter::State::visitMeterSig(const MeterSig* ts)
{
    assert(ts);
    ERROR("MEIExport: visitMeterSig should not be called");
}


/// @todo complete
void MEIExporter::State::visitKeySig(const KeySig* ks)
{
    assert(ks);
    ERROR("MEIExport: visitKeySig should not be called");
}


// read a tied SM object
void MEIExporter::State::visitTied(const Tied* n)
{
    assert(n);
    TRACE("MEIExporter[in]: Tied at {} in sequence {}",
          n->date(), n->sequence().voiceNb());
    const Tied& tie = *n;
    assert(! _hierorchy.empty());
    Env* e = _hierorchy.top().get();
    assert(e);
    assert ((e->type == LAYER_TYPE) ||
            (e->type == BEAMED_TYPE) ||
            (e->type == TUPLETU_TYPE) ||
            (e->type == TUPLETB_TYPE) ||
            // grace note.s after note or chord
            // ((e->type == GN_TYPE) && (e->step == 0)) ||
            // mordent etc
            ((e->type == ORNAMENT_TYPE) && (e->step == 0)));
                       
    // write the tied MEI element
    // tied event not on current staff : pad with space
    if (tie.staff() != _staffNb)
    {
        // the previous note was on current staff
        if ((_mei_pnote) || (_mei_pchord))
        {
            ERROR("MEIExporter: tie between nodes in different staves: ignored.");
        }
        else if (_mei_prest)
        {
            ERROR("MEIExporter: tie to a rest.");
            ERROR("MEIExporter: tie between different staves: ignored.");
        }

        // _mei_pspace is OK :
        // the previous element was also on a different staff
        
        // WARNING: _mei_pspace may be NULL
        mei::Space* mei_space = _exporter.exportTiedSpace(tie);
        write(mei_space);
        assert(_mei_pnote  == NULL); // check that update of previous elt correct
        assert(_mei_prest  == NULL);
        assert(_mei_pchord == NULL);
        assert(_mei_pspace == mei_space);
    }
    // tie staff = current staff number _staffNb
    // create new MEI element
    // dispatch previous MEI element (ugly type case analysis)
    // and update previous element pointer
    //// @todo non ca dépend si tie est dans le bon staff
    else if (_mei_pnote)
    {
        TRACE("MEIExporter[in]: tied notes.");
        mei::Note* mei_note = _exporter.exportTiedNote(_mei_pnote, tie);
        write(mei_note, true);
        assert(_mei_pnote == mei_note);
        assert(_mei_prest  == NULL);
        assert(_mei_pchord == NULL);
        assert(_mei_pspace == NULL);
    }
    else if (_mei_prest)
    {
        ERROR("MEIExporter: tie between two rests: ignored.");
        mei::Rest* mei_rest = _exporter.exportRest(_mei_prest, tie);
        write(mei_rest);
        assert(_mei_pnote  == NULL);
        assert(_mei_prest == mei_rest);
        assert(_mei_pchord == NULL);
        assert(_mei_pspace == NULL);
    }
    else if (_mei_pchord)
    {
        TRACE("MEIExporter[in]: tied chords.");
        mei::Chord* mei_chord = _exporter.exportTiedChord(_mei_pchord, tie);
        /// @todo add copies of the notes of _mei_pchord (w. exportTiedNote) to this new chord
        write(mei_chord, true);
        assert(_mei_pnote  == NULL);
        assert(_mei_prest  == NULL);
        assert(_mei_pchord == mei_chord);
        assert(_mei_pspace == NULL);
    }
    else if (_mei_pspace)
    {
        ERROR("MEIExporter: tie between two spaces. ignored.");
        mei::Space* mei_space = _exporter.exportTiedSpace(tie);
        write(mei_space);
        assert(_mei_pnote  == NULL);
        assert(_mei_prest  == NULL);
        assert(_mei_pchord == NULL);
        assert(_mei_pspace == mei_space);
    }
    
    // change state
    if (e->type == LAYER_TYPE) { } // no change
    else if (e->type == BEAMED_TYPE) { }
    else if (e->type == TUPLETU_TYPE) { }
    else if (e->type == TUPLETB_TYPE) { }
    // grace note.s after note or chord
    else if (e->type == GN_TYPE) // && (e->step == 10))
    {
        // e->step = 11; // now wait for grace note
        ERROR("MEIExporter: read tie: grace note was expected state ({}.{})",
              e->type, e->step);
        throw sm_error;
    }
    // mordent etc
    else if ((e->type == ORNAMENT_TYPE) && (e->step == 0))
    {
        // e->step = 1; // now wait for ornament
        ERROR("MEIExporter: read tie: tied note cannot have ornament ({}.{})",
              e->type, e->step);
        throw sm_error;

    }
    else
    {
        ERROR("MEIExporter: read tie: unexpected state {}.{}", e->type, e->step);
        throw sm_error;
    }
}


// read a SM dot object
void MEIExporter::State::visitDot(const Dot* n)
{
    assert(n);
    TRACE("MEIExporter[in]: Dot at {} in sequence {}",
          n->date(), n->sequence().voiceNb());
   
    // change state (save the dot to add as MEI attribute later)
    _dots++;
    
    // check state : parsing a note or chord or rest or space.
    assert(! _hierorchy.empty());
    Env* e = _hierorchy.top().get();
    assert(e);
    if ((e->type == LAYER_TYPE) ||
        (e->type == BEAMED_TYPE) ||
        (e->type == TUPLETU_TYPE) ||
        (e->type == TUPLETB_TYPE) ||
        (e->type == CHORD_TYPE))
    { } // OK, keep in same state
    else if ((e->type == GN_TYPE) || (e->type == ORNAMENT_TYPE))
    {
        ERROR("MEIExporter: ornament on dot! (state: {}.{})", e->type, e->step);
        throw sm_error;
    }
    else
    {
        ERROR("MEIExporter: read dot: unexpected state {}.{}", e->type, e->step);
        throw sm_error;
    }
}


// read a SM note not tied to the previous
void MEIExporter::State::visitLeafNote(const LeafNote* n)
{
    assert(n);
    TRACE("MEIExporter[in]: LeafNote at {} sequence {}",
          n->date(), n->sequence().voiceNb());
    const LeafNote& leaf = *n;
    std::string id = std::string(); // empty initially
    
    // write a MEI note or space
    // note node is in the current staff
    if (leaf.staff() == _staffNb)
    {
        mei::Note* mei_note = _exporter.exportNote(leaf);
        assert(mei_note);
        id = mei_note->getId();
        write(mei_note, false); // new note, not tied
    }
    else  // note node is in another staff, replace by space
    {
        mei::Space* mei_space = _exporter.exportSpace(leaf);
        assert(mei_space);
        write(mei_space);
    }
    
    // change state
    assert(! _hierorchy.empty());
    Env* e = _hierorchy.top().get();
    assert(e);
    if (e->type == LAYER_TYPE) { } // no change
    else if (e->type == BEAMED_TYPE) { }
    else if (e->type == TUPLETU_TYPE) { }
    else if (e->type == TUPLETB_TYPE) { }
    // grace note.s after note or chord
    else if (e->type == GN_TYPE)
    {
        // e->step = 11; // now wait for grace note
        ERROR("MEIExporter: read untied note, expected grace note (state {}.{})",
              e->type, e->step);
        throw sm_error;
    }
    // mordent etc
    else if ((e->type == ORNAMENT_TYPE) && (e->step == 0))
    {
        // save id in stack (used for constr. MEI element)
        EnvOrnament* eo = static_cast<EnvOrnament*>(e);
        assert(eo);
        assert(eo->step == 1);
        assert(eo->id.empty()); // should be unset at this point
        eo->id = id;            // can be empty if no note was added
        e->step = 1;            // update state: now wait for ornament
    }
    else
    {
        ERROR("MEIExporter: read untied note: unexpected state {}.{}",
              e->type, e->step);
        throw sm_error;
    }
}


// read a SM rest object
void MEIExporter::State::visitLeafRest(const LeafRest* n)
{
    assert(n);
    TRACE("MEIExporter[in]: LeafRest, sequence {} at {}",
          n->sequence().voiceNb(), n->date());
    const LeafRest& leaf = *n;
    
    // write a MEI rest or space
    // rest node is in the current staff
    if (leaf.staff() == _staffNb)
    {
        mei::Rest* mei_rest = _exporter.exportRest(leaf);
        assert(mei_rest);
        write(mei_rest); // new note, not tied
    }
    else  // rest node is in another staff, replace by space
    {
        mei::Space* mei_space = _exporter.exportSpace(leaf);
        assert(mei_space);
        write(mei_space);
    }
    
    // change state
    assert(! _hierorchy.empty());
    Env* e = _hierorchy.top().get();
    assert(e);
    if (e->type == LAYER_TYPE) { } // no change
    else if (e->type == BEAMED_TYPE) { }
    else if (e->type == TUPLETU_TYPE) { }
    else if (e->type == TUPLETB_TYPE) { }
    else if ((e->type == GN_TYPE) || (e->type == ORNAMENT_TYPE))
    {
        ERROR("MEIExporter: ornament over rest (state {}.{})",
              e->type, e->step);
        throw sm_error;
    }
    else
    {
        ERROR("MEIExporter: read rest: unexpected state {}.{}",
              e->type, e->step);
        throw sm_error;
    }
}


// read a ScoreModel space object
void MEIExporter::State::visitLeafSpace(const LeafSpace* n)
{
    assert(n);
    TRACE("MEIExporter[in]: LeafSpace, sequence {} at {}",
          n->sequence().voiceNb(), n->date());
    const LeafSpace& leaf = *n;

    mei::Space* mei_space = _exporter.exportSpace(leaf);
    assert(mei_space);
    write(mei_space);
    
    // change state : space is treated like a note or chord
    /// @todo check spec of spaces in SM
    assert(! _hierorchy.empty());
    Env* e = _hierorchy.top().get();
    assert(e);
    if (e->type == LAYER_TYPE) { } // no change
    else if (e->type == BEAMED_TYPE) { }
    else if (e->type == TUPLETU_TYPE) { }
    else if (e->type == TUPLETB_TYPE) { }
    // grace note.s after note or chord
    else if ((e->type == GN_TYPE) && (e->step > 0))
    {
        e->step -= 1;
    }
    // mordent etc
    else if ((e->type == ORNAMENT_TYPE) && (e->step == 0))
    {
        e->step = 1; // now wait for ornament
    }
    else
    {
        ERROR("MEIExporter: read space: unexpected state {}.{}", e->type, e->step);
        throw sm_error;
    }
}


// read an untied sm chord object
/// @todo complete
void MEIExporter::State::visitLeafChord(const LeafChord* n)
{
    assert(n);
    TRACE("MEIExporter[in]: LeafChord, sequence {} at {}",
          n->sequence().voiceNb(), n->date());
    const LeafChord& leaf = *n;
    std::string id = std::string(); // empty initially

    // note node is in the current staff
    if (leaf.staves().contains(_staffNb))
    {
        WARN("MEIExporter[in]: MEI chord not implemented");
        /// @todo complete
        // mei::Chord* mei_chord = _exporter.exportChord(leaf);
        // assert(mei_chord);
        // extract and add leafnotes on the staffNb
        // id = mei_chord->getId();
        // write(mei_chord, false); // new note, not tied
    }
    else  // all notes of chord in other staves, replace by space
    {
        mei::Space* mei_space = _exporter.exportSpace(leaf);
        assert(mei_space);
        write(mei_space);
    }
    
    // change state : same as for note
    assert(! _hierorchy.empty());
    Env* e = _hierorchy.top().get();
    assert(e);
    if (e->type == LAYER_TYPE) { } // no change
    else if (e->type == BEAMED_TYPE) { }
    else if (e->type == TUPLETU_TYPE) { }
    else if (e->type == TUPLETB_TYPE) { }
    // grace note.s after note or chord
    else if (e->type == GN_TYPE)
    {
        ERROR("MEIExporter: read chord, expected grace note (state {}.{})",
              e->type, e->step);
        throw sm_error;
    }
    // mordent etc
    else if ((e->type == ORNAMENT_TYPE) && (e->step == 0))
    {
        // save id in stack (used for constr. MEI element)
        EnvOrnament* eo = static_cast<EnvOrnament*>(e);
        assert(eo);
        assert(eo->step == 1);
        assert(eo->id.empty()); // should be unset at this point
        eo->id = id;            // can be empty if no note was added
        e->step = 1; // now wait for ornament
    }
    else
    {
        ERROR("MEIExporter: read untied chord: unexpected state {}.{}",
              e->type, e->step);
        throw sm_error;
    }

}


/// @todo complete : add to chord
// component of a chord
void MEIExporter::State::visitPitchedNote(const PitchedNote* n)
{
    assert(n);
    WARN("MEIExporter[in]: read PitchedNote (in chord) NOT IMPLEMENTED");

}


/// @todo STUB complete
void MEIExporter::State::visitLeafHarmonic(const LeafHarmonic* n)
{
    assert(n);
    TRACE("MEIExporter[in]: LeafHarmonic, sequence {} at {}",
          n->sequence().voiceNb(), n->date());
    ERROR("MEIExporter: LeafHarmonic TBR (ignored)");
}


void MEIExporter::State::visitDecorated(const Decorated* n)
{
    assert(n);
    assert(n->complete());
    TRACE("MEIExport[in]: decorated, sequence {} at {}",
          n->sequence().voiceNb(), n->date());
    
    // state will wait receiving a dispatched subtype of ornament object
    openDeco(*n);
    
    // dispatch ornament subtype to change the top of hierarchy
    Ornament* orn = n->decoration();
    assert(orn);
    assert(orn->size() > 0);
    orn->accept(this);

    closeDeco();
}


MEIExporter::State::EnvDeco::EnvDeco(mei::MeiElement* c,
                                     size_t s,
                                     const LeafEvent& d,
                                     bool bf):
Env(DECO_TYPE, c, s),
decorated(d),
before(bf)
{ }


MEIExporter::State::EnvDeco::~EnvDeco()
{ }


void MEIExporter::State::openDeco(const Decorated& n)
{
    LeafEvent* le = n.decorated();
    assert(le);
    assert(! _hierorchy.empty());
    Env* e = _hierorchy.top().get();
    assert(e);
    assert(e->container);

    
    // change type
    // push a deco type in state 0 with copy of current top container to the stack
    // decorated event and flag wether the ornament must be played before the note.
    // size_t s = (n.ornamentFirst())?0:1;
    // check type
    if ((e->type == LAYER_TYPE) ||
        (e->type == BEAMED_TYPE) ||
        (e->type == TUPLETU_TYPE) ||
        (e->type == TUPLETB_TYPE))
    {
        _hierorchy.emplace(std::make_unique<EnvDeco>(e->container, 0,
                                                 *le, n.ornamentFirst()));
    }
    else
    {
        ERROR("MEIExporter: read deco: unexpected state {}.{}",
              e->type, e->step);
        throw sm_error;

    }
}


void MEIExporter::State::closeDeco()
{
    assert(! _hierorchy.empty());
    assert(_hierorchy.top().get()->type == DECO_TYPE);
    assert(_hierorchy.top().get()->step == 1); // final state
    _hierorchy.pop(); // does not delete the embedded MeiElement
}



// treat Ornament as abstract
void MEIExporter::State::visitOrnament(const Ornament* a)
{
    ERROR("MEIExporter: visit Ornament should not be called");
}


/// @todo STUB complete
void MEIExporter::State::visitAppogiatura(const Appogiatura* app)
{
    assert(app);
    TRACE("MEIExporter[in]: Appogiatura");
    assert(app->size() > 0);
       
    assert(! _hierorchy.empty());
    Env* e = _hierorchy.top().get();
    assert(e);
    assert(e->type == DECO_TYPE);
    EnvDeco* ed = static_cast<EnvDeco*>(e);
    assert(ed);
    const LeafEvent& event = ed->decorated;

    // decoration is after the decorated leaf
    // dispatch the decorated leaf (first case)
    if (! ed->before)
    {
        event.accept(this);
    }
        
    // dispatch all the grace notes.
    bool go = openGNGrp(*app, ed->before);
    for (size_t i = 0; i < app->size(); ++i)
    {
        const SingleGraceNote* sgn = app->at(i);
        assert(sgn);
        sgn->accept(this);
    }
    if (go) { closeGNGrp(); }
    // ALT: when there is only 1 type og g.n. (no dispatch needed)
    // mei::Beam* = _exporter.exportGraceGrp(*app);

    // dispatch the decorated leaf (second case)
    if (ed->before)
    {
        ed->decorated.accept(this);
    }
        
    // update state
    if ((e->type == DECO_TYPE) && (e->step == 0))
    {
        // change state in DECO env to final (can be closed)
        ed->step = 1;
    }
    else
    {
        ERROR("MEIExporter: read appogiatura: unexpected state {}.{}",
              e->type, e->step);
        throw sm_error;
    }
}


MEIExporter::State::EnvGNGrp::EnvGNGrp(mei::MeiElement* c, size_t nb,
                                       cmnd_t dur, bool bf, bool st):
Env(GN_TYPE, c, nb),
duration(dur),
before(bf),
stroke(st)
{
    assert(nb > 0);   // 1 or several grace notes
    assert(dur >= 8); // eight note or shorter
}


MEIExporter::State::EnvGNGrp::~EnvGNGrp()
{ }


bool MEIExporter::State::openGNGrp(const Appogiatura& app, bool before)
{
    // appogiatura is on other staves
    if (! app.staves().contains(_staffNb))
        return false;
    
    // the appogiatura may span over several staves but not more than 2.
    if (app.staves().size() > 2)
    {
        ERROR("MEIExporter: appogiatura spanning over more than 2 staves, not supported");
        throw sm_error;
    }
    
    size_t n =  app.size();
    assert(n > 0);
    
    // single grace note : use the current container
    if (n == 1)
    {
        assert(! _hierorchy.empty());
        Env* e = _hierorchy.top().get();
        assert(e);
        assert(e->container);
        // GN_TYPE andcontainer = copy of current top container to the stack
        _hierorchy.emplace(std::make_unique<EnvGNGrp>(e->container,
                                                      1, // nb g.n.
                                                      app.printedDuration(),
                                                      before,
                                                      app.isAcciaccatura()));
        return true;
    }
    // multiple grace notes : make a group container
    // see https://music-encoding.org/guidelines/v4/elements/gracegrp.html
    else
    {
        /// @todo no element `<graceGrp>`  in libMEI ? we use a `<beam>` instead
        mei::Beam* mei_beam = _exporter.exportBeam(_staffNb, app.staves());
        write(mei_beam);
        _hierorchy.emplace(std::make_unique<EnvGNGrp>(mei_beam,   // GN_TYPE
                                                      app.size(), // size_t
                                                      app.printedDuration(),  // cmnd_t
                                                      before,
                                                      app.isAcciaccatura())); // stroke
        return true;
    }
}


// stafety checks and pop stack
void MEIExporter::State::closeGNGrp()
{
    assert(! _hierorchy.empty());
    assert(_hierorchy.top().get()->type == GN_TYPE);
    assert(_hierorchy.top().get()->step == 0); // final state
    _hierorchy.pop(); // does not delete the embedded MeiElement
}


/// @todo complete
void MEIExporter::State::visitSingleGraceNote(const SingleGraceNote* n)
{
    assert(n);
    TRACE("MEIExporter[in]: SingleGraceNote");

    assert(! _hierorchy.empty());
    Env* e = _hierorchy.top().get();
    assert(e);
    assert(e->type == GN_TYPE);
    EnvGNGrp* eg = static_cast<EnvGNGrp*>(e);
    assert(eg);
    
    // write a MEI grace note on the current staff
    if (n->staff() == _staffNb)
    {
        bool stroke = (eg->stroke && (eg->step == 1)); // last g.n. of group
        mei::Note* mei_note = _exporter.exportGraceNote(*n, eg->before,
                                                        eg->duration,
                                                        stroke);
        assert(mei_note);
        assert(eg->container);
        eg->container->addChild(mei_note); // write(mei_note)
    }
        
    // change state
    if ((eg->type == GN_TYPE) && (eg->step > 0)) // final state
    {
        eg->step -= 1;
    }
    else
    {
        ERROR("MEIExporter: read grace note: unexpected state {}.{}",
              e->type, e->step);
        throw sm_error;
    }
}


void MEIExporter::State::openOrnament(const Ornament& ornament, bool before)
{
    assert(! _hierorchy.empty());
    Env* e = _hierorchy.top().get();
    assert(e);
    assert(e->container);
    // ORNAMENT_TYPE and container = copy of current top container to the stack
    // and copy of father if it is a Beam
    // initial state = 0
    if (e->type == BEAMED_TYPE)
    {
        EnvBeam* eb = static_cast<EnvBeam*>(e);
        assert(eb);
        _hierorchy.emplace(std::make_unique<EnvOrnament>(*eb));
    }
    else
    {
        _hierorchy.emplace(std::make_unique<EnvOrnament>(e->container, 0));
    }
}


MEIExporter::State::EnvOrnament::EnvOrnament(mei::MeiElement* c, size_t step,
                                             Beam::Info bi, unsigned long d):
EnvBeam(ORNAMENT_TYPE, c, step, bi, d),
id() // initialy empty
{ }

MEIExporter::State::EnvOrnament::EnvOrnament(const EnvBeam& e):
EnvBeam(e), // default copy constructor
id() // initialy empty
{ }

MEIExporter::State::EnvOrnament::~EnvOrnament()
{ }


// stafety checks and pop stack
void MEIExporter::State::closeOrnament()
{
    assert(! _hierorchy.empty());
    assert(_hierorchy.top().get()->type == ORNAMENT_TYPE);
    assert(_hierorchy.top().get()->step == 2); // final state
    _hierorchy.pop(); // does not delete the embedded MeiElement
}


/// @todo complete
void MEIExporter::State::visitMordent(const Mordent* m)
{
    assert(m);
    TRACE("MEIExporter[in]: Mordent");
    //WARN("MEIExporter[in]: Mordent NOT FINISHED");

    assert(! _hierorchy.empty());
    Env* e = _hierorchy.top().get();
    assert(e);

    // dispatch on type of ornament, update stack over deco
    if ((e->type == DECO_TYPE) && (e->step == 0))
    {
        EnvDeco* ed = static_cast<EnvDeco*>(e);
        assert(ed);
                
        openOrnament(*m, ed->before);
        // dispatch event (note or chord, not tied)
        const LeafEvent& event = ed->decorated;
        event.accept(this);
        // rec call : jump to next case
        m->accept(this);
        closeOrnament();
    }
    // note/chord event was read, wait for ornament
    else if ((e->type == ORNAMENT_TYPE) && (e->step == 1))
    {
        EnvOrnament* eo = static_cast<EnvOrnament*>(e);
        assert(eo);
        assert(eo->step == 1);    // state after read/write event, wait for orn.
        if (eo->id.empty())
        {
            // note or chord was not written (not on the current staff)
            // do not write ornament.
            eo->step = 2;
        }
        else
        {
            mei::Mordent* mei_mordent = _exporter.exportMordent(*m, eo->id);
            assert(mei_mordent);
            _buffer.push(mei_mordent); // will be to the current measure latera
            eo->step = 2;
        }
        // ALT
        // retrieve the MEI element inserted and its id
        // assert(eo->container);
        // const std::vector<MeiElement*>& elts = e->container->getChildren();
        // assert(! elts.empty());
        // MeiElement* mei_evt = elts.back();
        // assert(mei_evt);
        // const std::string id = mei_evt->getId();
        // ALT : use _mei_pnote / _mei_pchord
    }
    else
    {
        ERROR("MEIExporter: read mordent: unexpected state {}.{}",
              e->type, e->step);
        throw sm_error;
    }
}


/// @todo complete
void MEIExporter::State::visitGruppetto(const Gruppetto* g)
{
    assert(g);
    TRACE("MEIExporter[in]: Gruppetto (Turn)");
    
    assert(! _hierorchy.empty());
    Env* e = _hierorchy.top().get();
    assert(e);

    // dispatch on type of ornament, update stack over deco
    if ((e->type == DECO_TYPE) && (e->step == 0))
    {
        EnvDeco* ed = static_cast<EnvDeco*>(e);
        assert(ed);
                
        openOrnament(*g, ed->before);
        // dispatch event (note or chord, not tied)
        const LeafEvent& event = ed->decorated;
        event.accept(this);
        // rec call : jump to next case
        g->accept(this);
        closeOrnament();
    }
    // note/chord event was read, wait for ornament
    else if ((e->type == ORNAMENT_TYPE) && (e->step == 1))
    {
        EnvOrnament* eo = static_cast<EnvOrnament*>(e);
        assert(eo);
        assert(eo->step == 1);    // state after read/write event, wait for orn.
        if (eo->id.empty())
        {
            // note or chord was not written (not on the current staff),
            // do not write ornament.
            eo->step = 2;
        }
        else
        {
            mei::Turn* mei_turn = _exporter.exportTurn(*g, eo->id);
            assert(mei_turn);
            _buffer.push(mei_turn); // will be to the current measure latera
            eo->step = 2;
        }
    }
    else
    {
        ERROR("MEIExporter: read gruppetto (turn): unexpected state {}.{}",
              e->type, e->step);
        throw sm_error;
    }
}


/// @todo complete
void MEIExporter::State::visitArticulation(const Articulation* a)
{
    assert(a);
    TRACE("MEIExporter[in]: Articulation");
    WARN("MEIExporter[in]: Not Implemented");
}

/// @todo complete
void MEIExporter::State::visitLocalTrill(const LocalTrill* lt)
{
    assert(lt);
    TRACE("MEIExporter[in]: LocalTrill");
    WARN("MEIExporter[in]: Not Implemented");
}

/// @todo complete
void MEIExporter::State::visitDynamic(const Dynamic* d)
{
    assert(d);
    TRACE("MEIExporter[in]: Dynamic");
    WARN("MEIExporter[in]: Not Implemented");
}

/// @todo complete
void MEIExporter::State::visitJump(const Jump* j)
{
    assert(j);
    TRACE("MEIExporter[in]: Jump");
    WARN("MEIExporter[in]: Not Implemented");
}

/// @todo complete
void MEIExporter::State::visitFermata(const Fermata* f)
{
    assert(f);
    TRACE("MEIExporter[in]: Fermata");
    WARN("MEIExporter[in]: Not Implemented");
}

/// @todo complete
void MEIExporter::State::visitBreath(const Breath* b)
{
    assert(b);
    TRACE("MEIExporter[in]: Breath");
    WARN("MEIExporter[in]: Not Implemented");
}

/// @todo complete
void MEIExporter::State::visitClefChange(const ClefChange* cc)
{
    assert(cc);
    TRACE("MEIExporter[in]: ClefChange");
    WARN("MEIExporter[in]: Not Implemented");
}

/// @todo complete
void MEIExporter::State::visitTempoChange(const TempoChange* tc)
{
    assert(tc);
    TRACE("MEIExporter[in]: TempoChange");
    WARN("MEIExporter[in]: Not Implemented");
}


/// @todo complete
void MEIExporter::State::visitSlur(const Slur* s)
{
    assert(s);
    TRACE("MEIExporter[in]: Slur");
    WARN("MEIExporter[in]: Not Implemented");
}

/// @todo complete
void MEIExporter::State::visitVolta(const Volta* v)
{
    assert(v);
    TRACE("MEIExporter[in]: Volta");
    WARN("MEIExporter[in]: Not Implemented");
}
   
/// @todo complete
void MEIExporter::State::visitOctava(const Octava* o)
{
    assert(o);
    TRACE("MEIExporter[in]: Octava");
    WARN("MEIExporter[in]: Not Implemented");
}

/// @todo complete
void MEIExporter::State::visitVolChange(const VolChange* vc)
{
    assert(vc);
    TRACE("MEIExporter[in]: VolChange");
    WARN("MEIExporter[in]: Not Implemented");
}

/// @todo complete
void MEIExporter::State::visitPedal(const Pedal* p)
{
    assert(p);
    TRACE("MEIExporter[in]: Pedal");
    WARN("MEIExporter[in]: Not Implemented");
}

/// @todo complete
void MEIExporter::State::visitSpanningTrill(const SpanningTrill* st)
{
    assert(st);
    TRACE("MEIExporter[in]: SpanningTrill");
    WARN("MEIExporter[in]: Not Implemented");
}

/// @todo complete
void MEIExporter::State::visitVibrato(const Vibrato* v)
{
    assert(v);
    TRACE("MEIExporter[in]: Vibrato");
    WARN("MEIExporter[in]: Not Implemented");
}

/// @todo complete
void MEIExporter::State::visitGlissando(const Glissando* g)
{
    assert(g);
    TRACE("MEIExporter[in]: Glissando");
    WARN("MEIExporter[in]: Not Implemented");
}

/// @todo complete
void MEIExporter::State::visitOsc(const Osc* o)
{
    assert(o);
    TRACE("MEIExporter[in]: Osc");
    WARN("MEIExporter[in]: Not Implemented");
}








mei::MeiElement* MEIExporter::State::topContainer() const
{
    if (_hierorchy.empty())
        return NULL;
    else
    {
        Env* e = _hierorchy.top().get();
        assert(e);
        return e->container;
    }
}


MEIExporter::State::MEIContainerType MEIExporter::State::topType() const
{
    assert(! _hierorchy.empty());
    Env* e = _hierorchy.top().get();
    assert(e);
     return e->type;
}


void MEIExporter::State::write(mei::Layer* mei_layer)
{
    assert(mei_layer);
    assert(! _hierorchy.empty()); // there should be a staff layer
    Env* e = _hierorchy.top().get();
    assert(e);
    assert(e->type == STAFF_TYPE);
    assert(e->container); // castable to MEI::Staff*
    e->container->addChild(mei_layer);
}


void MEIExporter::State::write(mei::Beam* mei_beam)
{
    assert(mei_beam);
    assert(! _hierorchy.empty());
    Env* c = _hierorchy.top().get();
    assert(c);
    assert(c->container);
    (c->container)->addChild(mei_beam);
}


void MEIExporter::State::write(mei::Tuplet* mei_tuplet)
{
    assert(mei_tuplet);
    assert(! _hierorchy.empty());
    Env* c = _hierorchy.top().get();
    assert(c);
    assert(c->container);
    (c->container)->addChild(mei_tuplet);
}


void MEIExporter::State::write(mei::Note* mei_note, bool tied)
{
    assert(mei_note);
    assert(invariant()); // pre: invariant
    
    
    // is current note mei_note really tied to previous element?
    bool newtie = tied;
    // ugly dispatch on previous element:
    // case analysis for saving tie of to previous note.
    if (_mei_pnote && tied) // by invariant, the other _mei_pX are NULL
    {
        TRACE("MEIExporter[out]: registered tied notes");
    }
    else if (_mei_prest && tied) // by invariant, the other _mei_pX are NULL
    {   // should not happen
        ERROR("MEIExporter: tie between a rest and a note. ignored.");
        newtie = false;
    }
    else if (_mei_pchord && tied) // by invariant, the other _mei_pX are NULL
    {   // should not happen
        ERROR("MEIExporter: tie between a chord and a note. ignored.");
        newtie = false;
    }
    else if (_mei_pspace && tied) // by invariant, the other _mei_pX are NULL
    {
        ERROR("MEIExporter: tie between a space and a note. ignored.");
        newtie = false;
    }

    // change state
    TRACE("MEIExporter[out]: add MEI note");
    // update dots in previous element
    update_dots();
    // update tie in previous element and flag _ptied (previous is tied)
    update_tie(newtie);
    // update bream separation attribute (inter-notes beam) in previous element
    update_breaksec(mei_note); 
    // add note to top container
    assert(topContainer());
    topContainer()->addChild(mei_note);

    // update variable for stored previous element
    _mei_pnote = mei_note;
    _mei_ppnote = mei_note;
    _mei_prest = NULL;
    _mei_pchord = NULL;
    _mei_ppchord = NULL;
    _mei_pspace = NULL;
    // post: invariant: exactly one previous pointer non-null
}


void MEIExporter::State::write(mei::Rest* mei_rest)
{
    assert(mei_rest);
    assert(invariant()); // pre: invariant

    TRACE("MEIExporter: add MEI rest");
    update_dots();
    update_tie(false); // update _ptied (previous is tied) to false
    // no update of breaksec for rest
    assert(topContainer());
    topContainer()->addChild(mei_rest);

    // update previous element
    _mei_pnote = NULL;  // do not update _mei_ppnote
    _mei_prest = mei_rest;
    _mei_pchord = NULL; // do not update _mei_ppchord
    _mei_pspace = NULL;
    // post: invariant: exactly one previous pointer non-null
}


void MEIExporter::State::write(mei::Chord* mei_chord, bool tied)
{
    assert(mei_chord);
    assert(invariant());

    // invariant: exactly one previous pointer non-null
    assert((_mei_pnote == NULL) || ((_mei_prest == NULL) &&
                                    (_mei_pchord == NULL) &&
                                    (_mei_pspace == NULL)));
    assert((_mei_prest == NULL) || ((_mei_pnote == NULL) &&
                                    (_mei_pchord == NULL) &&
                                    (_mei_pspace == NULL)));
    assert((_mei_pchord == NULL) || ((_mei_pnote == NULL) &&
                                     (_mei_prest == NULL) &&
                                     (_mei_pspace == NULL)));
    assert((_mei_pspace == NULL) || ((_mei_pnote == NULL) &&
                                     (_mei_prest == NULL) &&
                                     (_mei_pchord == NULL)));

    WARN("MEIExporter[in]: MEI chord not implemented yet");
    /// @todo complete
    // TRACE("MEIExporter: add MEI chord");
    update_dots();
    update_tie(false); // update _ptied (previous is tied) to false
    // update of breaksec
    assert(topContainer());
    //topContainer()->addChild(mei_chord);

    // update variable for stored previous element
    _mei_pnote = NULL;
    _mei_ppnote = NULL;
    _mei_prest = NULL;
    _mei_pchord = mei_chord;
    _mei_ppchord = mei_chord;
    _mei_pspace = NULL;
    // post: invariant: exactly one previous pointer non-null
}


void MEIExporter::State::write(mei::Space* mei_space)
{
    assert(mei_space); // new MEI element
    assert(invariant()); // pre: invariant

    TRACE("MEIExporter: add MEI space");
    update_dots();
    update_tie(false); // update _ptied (previous is tied) to false
    // no update of breaksec for space
    assert(topContainer());
    topContainer()->addChild(mei_space);

    // update previous element
    _mei_pnote = NULL;  // do not update _mei_ppnote
    _mei_prest = NULL;
    _mei_pchord = NULL; // do not update _mei_ppchord
    _mei_pspace = mei_space;
    // post: invariant: exactly one previous pointer non-null
}



/**
 *
 * Internal state modeifications (updates)
 *
 */


void MEIExporter::State::update_dots()
{
    if (_dots == 0)
        return;
    
    // set attribute nb of dots in the previous MEI element
    std::string sdots = std::to_string(_dots);
    if (_mei_pnote)
    {
        TRACE("MEIExporter: MEI note has {} dots", sdots);
        _mei_pnote->m_Augmentdots.setDots(sdots);
    }
    else if (_mei_prest)
    {
        TRACE("MEIExporter: MEI rest has {} dots", sdots);
        _mei_prest->m_Augmentdots.setDots(sdots);
    }
    else if (_mei_pchord)
    {
        TRACE("MEIExporter: MEI chord has {} dots", sdots);
        _mei_pchord->m_Augmentdots.setDots(sdots);
    }
    else if (_mei_pspace)
    {
        TRACE("MEIExporter: MEI space has {} dots", sdots);
        _mei_pspace->m_Augmentdots.setDots(sdots);
    }

    // reset number of dots
    _dots = 0;
}


void MEIExporter::State::update_tie(bool tied)
{
    assert(invariant()); // pre: invariant

    // set the attribute tie in the previous MEI element
    // and reset the `tied` flag in this state.
    /// @see https://music-encoding.org/guidelines/v4/data-types/data.tie.html
    if (_mei_pnote && _ptied && tied)
    {
        TRACE("MEIExporter: MEI attribute: medial tie between notes");
        _mei_pnote->m_Tiepresent.setTie("m"); // medial
    }
    else if (_mei_pnote && _ptied) // not tied
    {
        TRACE("MEIExporter: MEI attribute: terminal tie between notes");
        _mei_pnote->m_Tiepresent.setTie("t"); // final
    }
    else if (_mei_pnote && tied)   // not _ptied
    {
        TRACE("MEIExporter: MEI attribute: initial tie between notes");
        _mei_pnote->m_Tiepresent.setTie("i"); // initial
    }
    else if (_mei_prest && tied)
    {
        ERROR("MEIExporter: element tied to rest, ignore tie.");
        _ptied = false;
        return;
    }
    else if (_mei_prest && _ptied)
    {
        ERROR("MEIExporter: tied rest, ignore tie.");
    }
    else if (_mei_pchord && _ptied && tied)
    {
        TRACE("MEIExporter: MEI attribute: medial tie between chords");
        _mei_pchord->m_Tiepresent.setTie("m");
    }
    else if (_mei_pchord && _ptied)
    {
        TRACE("MEIExporter: MEI attribute: terminal tie between chords");
        _mei_pchord->m_Tiepresent.setTie("t");
    }
    else if (_mei_pchord && tied)
    {
        TRACE("MEIExporter: MEI attribute: initial tie between chords");
        _mei_pchord->m_Tiepresent.setTie("i");
    }
    else if (_mei_pspace && tied)
    {
        ERROR("MEIExporter: element tied to space, ignore tie.");
        _ptied = false;
        return;
    }
    else if (_mei_pspace && _ptied)
    {
        ERROR("MEIExporter: tied space, ignore tie.");
    }

    // reset flag "tied to previous"
    _ptied = tied;
}


// Beam Policies:
// Beam::Info::Undef
// Beam::Info::None
// Beam::Info::Depth:
// Beam::Info::Full:
// Beam::Info::Minus:


void MEIExporter::State::update_breaksec(mei::Note* mei_note)
{
    assert(mei_note);
    update_breaksec(nbFlags(mei_note));
}


void MEIExporter::State::update_breaksec(mei::Chord* mei_chord)
{
    assert(mei_chord);
    update_breaksec(nbFlags(mei_chord));
}


void MEIExporter::State::update_breaksec(unsigned long nf)
{
    assert(! _hierorchy.empty());
    Env* e = _hierorchy.top().get();
    assert(e);
    
    if ((e->type == LAYER_TYPE) || (e->type == TUPLETU_TYPE))
    {
        return; // unbeamed, nothing to update.
    }
    // should not happen
    else if ((e->type == STAFF_TYPE) ||
             (e->type == CHORD_TYPE) ||
             (e->type == DECO_TYPE) ||
             (e->type == GN_TYPE))
    {
        ERROR("MEIExporter: update_breaksec: unexpected unbeamed Env");
        return;
    }

    // 2 cases of environments of beamed events, with beaming info
    assert((e->type == BEAMED_TYPE) ||
           (e->type == TUPLETB_TYPE) ||
           (e->type == ORNAMENT_TYPE));
    
    // number of beams of new element
    unsigned long nf2 = nf;
    assert(0 <= nf2);
    assert(nf2 <= CMN::MAX_FLAGS);
    EnvBeam* eb = static_cast<EnvBeam*>(e);
    assert(eb);

    // number of beams of previous note or chord element added
    // (rest and space elements are ignored)
    // unsigned long nf1 = CMN::MAX_FLAGS + 99;
    if (_mei_ppnote != NULL)
    {
        unsigned long nf1 = nbFlags(_mei_ppnote);
        // compute and add the breaksec to _mei_ppnote
        unsigned long nf3 = Beam::interEventBeams(nf1, nf2,
                                                  eb->beaminfo, eb->depth);
        if (nf3 != nf1)
            _mei_ppnote->m_Beamsecondary.setBreaksec(std::to_string(nf3));
    }
    else if (_mei_ppchord != NULL)
    {
        unsigned long nf1 = nbFlags(_mei_ppchord);
        // compute and add the breaksec to _mei_ppchord
        unsigned long nf3 = Beam::interEventBeams(nf1, nf2,
                                                  eb->beaminfo, eb->depth);
        if (nf3 != nf1)
            _mei_ppchord->m_Beamsecondary.setBreaksec(std::to_string(nf3));
    }
    // no former note or chord element:
    // no need to set inter-element beam
    // just initialize _pbeaming
    else
    {
        // initialize inter-elements beaming policy
        _pbeaming = BeamParam(eb->beaminfo, eb->depth);  // copy
        assert(_pbeaming.info != Beam::Info::Undef);
        // do not add a breaksec now
        return;
    }
}


/// @todo remove (useless & redundant w. Beam::interEventBeams)
unsigned long MEIExporter::State::breaksec(unsigned long nf1,
                                           unsigned long nf2)
{
    assert(0 <= nf1);
    assert(nf1 <= CMN::MAX_FLAGS);
    assert(0 <= nf2);
    assert(nf2 <= CMN::MAX_FLAGS);
    assert(! _hierorchy.empty());
    Env* e = _hierorchy.top().get();
    assert(e);
    assert((e->type == BEAMED_TYPE) || (e->type == ORNAMENT_TYPE));
    EnvBeam* eb = static_cast<EnvBeam*>(e);
    assert(eb);
    
    // s.m. node from which the MEI container was built
    // const Inner& orig = c.origine;
    // @todo cannot get bpol from orig
    // because beaming() is not a function of Inner.
    // @todo add beamin() to Inner
    // or dynamic_cast< const Tuplet&>(orig) avec catch(std::bad_cast exp)
    const Beam::Info& bpol = eb->beaminfo;
    assert(bpol != Beam::Info::Undef);

    switch(bpol) {
        case Beam::None:
            if (nf1 == 0)
                return 99; // no need of breaksec
            else
                return 0;
            
        case Beam::Full:
            if (nf1 == nf2)
                return 99; // no need of breaksec
            else
                // all beams possible
                return (std::min(nf1, nf2));
            
        case Beam::Minus:
        {
            // all beams possible - 1
            unsigned long m = std::min(nf1, nf2);
            if (m == 0)
                return 0;
            else if (m == 1)
                return 1;
            else
                return (m - 1);
        }
            
        case Beam::One:
            if (nf1 == 1)
                return 99; // no need of breaksec
            else
                return 1;
            
        case Beam::Depth:
        {
            unsigned long depth = eb->depth;
            assert(0 <= depth);
            assert(depth <= CMN::MAX_FLAGS);
            return depth;
            break;
        }
            
        default:
            ERROR("MEIExporter: unexpected beamin policy {}", bpol);
            return 99;
            break;
    }
}


/// @todo TBR
//unsigned long MEIExporter::State::previousNbFlags()
//{
//    if (_mei_ppnote != NULL)
//    {
//        return nbFlags(_mei_ppnote);
//    }
//    else if (_mei_ppchord != NULL)
//    {
//        return nbFlags(_mei_ppchord);
//    }
//    // no former note or chord element: no need to set inter-element beam
//    else
//    {
//        // initialize inter-elements beaming policy
//        _pbeaming = _beamings.top();
//        assert(_pbeaming.info != Beam::Info::Undef);
//        // do not add a breaksec now
//        return 99;
//    }
//}


bool MEIExporter::State::empty() const
{
    if (_hierorchy.empty())
        return true;

    // the bottom element of the stack is the current layer
    // if the stack has more than 1 element,
    // then this layer is not empty.
    if (_hierorchy.size() > 1)
        return false;
    
    assert(! _hierorchy.empty());
    Env* e = _hierorchy.top().get();
    assert(e);
    assert(e->type == LAYER_TYPE);
    assert(e->container);
    mei::Layer* mei_layer = dynamic_cast<mei::Layer*>(e->container);
    assert(mei_layer);
    return MEIExporter::State::empty(mei_layer);
}


bool MEIExporter::State::invariant() const
{
    // not building a layer (irrelevant)
    if (_hierorchy.empty())
        return false;
   
    bool ret = true;

    // all previous pointers are null
    // iff no element was processed for this layer
    // bool allNULL = (_mei_pnote  == NULL) &&
    //                (_mei_prest  == NULL) &&
    //                (_mei_pchord == NULL) &&
    //                (_mei_pspace == NULL);

    // not true
    // ret = ret && ((! allNULL) || _hierorchy.size() == 1);
    // ret = ret && (_hierorchy.size() != 1 || allNULL);

    // at most one previous pointer is non-null
    ret = ret && ((_mei_pnote == NULL) || ((_mei_prest == NULL) &&
                                           (_mei_pchord == NULL) &&
                                           (_mei_pspace == NULL)));

    ret = ret && ((_mei_prest == NULL) || ((_mei_pnote == NULL) &&
                                           (_mei_pchord == NULL) &&
                                           (_mei_pspace == NULL)));

    ret = ret && ((_mei_pchord == NULL) || ((_mei_pnote == NULL) &&
                                            (_mei_prest == NULL) &&
                                            (_mei_pspace == NULL)));

    ret = ret && ((_mei_pspace == NULL) || ((_mei_pnote == NULL) &&
                                            (_mei_prest == NULL) &&
                                            (_mei_pchord == NULL)));

    /// @todo complete cases of _mei_ppnote and _mei_ppchord

    ret = ret && ((_mei_ppnote == NULL) || (_mei_ppchord == NULL));

//    ret = ret && ((_mei_pnote == NULL) || (_mei_ppnote == _mei_pnote));
//    ret = ret && ((_mei_pchord == NULL) || (_mei_ppchord == _mei_pchord));
    
    return ret;
}



/**
 *
 * MEI utilities
 *
 */


bool MEIExporter::State::empty(mei::Layer* layer)
{
    assert(layer);
    bool ret = true;
    ret = ret && (! layer->hasChildren("note"));
    ret = ret && (! layer->hasChildren("rest"));
    ret = ret && (! layer->hasChildren("chord"));
    ret = ret && (! layer->hasChildren("space"));

    return ret;
}


unsigned long MEIExporter::State::nbFlags(mei::Note* n)
{
    assert(n);
    mei::MeiAttribute* a = n->m_DurationMusical.getDur();
    return nbFlags(a);
}


unsigned long MEIExporter::State::nbFlags(mei::Chord* n)
{
    assert(n);
    mei::MeiAttribute* a = n->m_DurationMusical.getDur();
    return nbFlags(a);
}


unsigned long MEIExporter::State::nbFlags(mei::MeiAttribute* a)
{
    assert(a);
    cmnd_t d = value_tocmnd(a->getValue());
    assert(CMN::defined(d));
    unsigned long nb = CMN::to_flags(d);
    assert(0 <= nb);
    assert(nb <= CMN::MAX_FLAGS);
    return nb;
}


cmnd_t MEIExporter::State::value_tocmnd(const std::string& str)
{
    return CMN::stocmnd(str);
}


} // end namespace ScoreModel

# src/scoremodel/export

This group contains procedures for the convertion of one score model, made of one musical and one engraving part, into other formats.
The conversion is based on a visitor pattern to traverse the engraving model, dispatching between the various types of nodes in trees.

We propose here the following output formats:
- simple text, for visualization / debugging (pretty printer)
- [MEI](https://music-encoding.org) (XML), using [libmei](https://github.com/DDMAL/libmei/).




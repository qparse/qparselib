//
//  SMMEI.hpp
//  squant2
//
//  Created by Florent Jacquemard on 06/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

/// @addtogroup output
/// @{


#ifndef SMMEI_hpp
#define SMMEI_hpp

#include <stdio.h>
#include <assert.h>
#include <exception>
#include <string>
//#include <functional>  // std::reference_wrapper
#include <memory>      // std::unique_ptr
#include <stack>
#include <queue>
#include <vector>

#include "trace.hpp"
#include "MTU.hpp"
#include "SMVisitor.hpp"
#include "Score.hpp"
#include "Part.hpp"
#include "Voice.hpp"
#include "Note.hpp"
#include "GraceNote.hpp"
#include "Rest.hpp"
#include "Chord.hpp"
#include "GraceChord.hpp"
#include "Trill.hpp"

#include "Root.hpp"
#include "Tuplet.hpp"
#include "Decorated.hpp"
#include "Ornament.hpp"
#include "Tied.hpp"
#include "Dot.hpp"
#include "LeafNote.hpp"
#include "LeafRest.hpp"
#include "LeafChord.hpp"
#include "LeafHarmonic.hpp"
#include "LeafSpace.hpp"

#include "CMND.hpp"    // cmnd_t
#include "Beam.hpp"
#include "Staffed.hpp" // for UNDEF_STAFF
#include "Ornament.hpp"
#include "Appogiatura.hpp"
#include "Mordent.hpp"
#include "Gruppetto.hpp"

#include <mei/mei.h>
#include <mei/meielement.h>
#include <mei/shared.h>
#include <mei/cmn.h>
//#include <mei/cmnmixins.h>
#include <mei/cmnornaments.h>
#include <mei/xmlexport.h>

//using mei::documentToFile;
//using mei::MeiDocument;
//using mei::MeiElement;
//using mei::MeiAttribute;
//using mei::Note;
//using mei::Rest;
//using mei::Tie;
//using mei::Beam;


// REFACTOR
// _document -> _mei_document
// _scoreElement -> _mei_score


// the typical hierarchy of a MEI document is the following:
//
//     meiHead front/titlePage parts/part        staffDef
// mei/music/  body/mdiv/score/section/measure /staff/layer/Elements
//             back            ending
//

namespace ScoreModel {


/// Export the score and engraving model into MEI, based on libMEI.
/// During generation, the input score is traversed time-wise,  measure by measure.
class MEIExporter
{

public:

    /// Create the first levels of the  a structure of an empty MEI document,
    /// as follows:
    /// ```
    /// <mei> = <meiHead> <music>
    /// <music> = <front> <body> <back>
    /// <body> = <mdiv>
    /// ```
    MEIExporter();

    ~MEIExporter();
    
    /// Complete this MEI document (open mdiv element) with a MEI parts element
    /// made from all the Parts in the given Score object, in the following form:
    /// ```<mdiv> = <parts> ```
    void addParts(const Score &score);

    /// Complete this MEI document (open Mdiv element) with a MEI score element
    /// made from the one given Part, in the following form:
    /// ```<mdiv> = <score>```
    void addScore(const Part& part);

    /// return the exported MEI document.
    /// @return pointer to the MEI document built.
    /// @warning it must be deallocated elsewhere.
    mei::MeiDocument* getDocument() const;
    
    /// Save in file
    void writeInFile(const std::string filename);

private:
    
    /**
     * Convertion from score model bjects to MEI elements or attributes
     */

    /// Create a MEI parts element from all the Parts in the given Score object,  in the following form:
    /// ```<parts> = <part>*```
    mei::Parts* exportParts(const Score& score);

    /// Create a MEI score element from the given Part,  in the following form:
    /// ```<Score> = <ScoreDef> <section>```
    mei::Score* exportScore(const Part& part);
 
    /// Create a MEI score element from the Part of given name in the given Score object, as follows:
    /// ```<Score> = <ScoreDef> <section>```
    mei::Score* exportScore(const Score& score, const std::string partname);

    /// Create a MEI score definition element from the given Part object, as follows:
    /// ```<ScoreDef> = <staffGrp> @meter.*```
    /// ```<staffGrp> = <staffDef>*``` (order matters)
    mei::ScoreDef* exportScoreDef(const Part& part);
    // others possible sub-elements:
    // `<meterSig>`, `<keySig>`, `<ambitus>`, `<instrGrp>`, `<meterSigGrp>`.

    /// Create a MEI staff definition element from the given staff def object, as follows:
    /// ```<staffDef> = @clef.* @staffLines @N```
    mei::StaffDef* exportStaffDef(const StaffDef& staff);

    /// Create a MEI part element from the given Part object, as follows:.
    /// ```<part> = <scoreDef> <section>```
    mei::Part* exportPart(const Part& part);

    /// Create a MEI section element from the given Part object, as follows:.
    /// ```<section> = <measure>*```
    mei::Section* exportSection(const Part& part);

    /// cast a score model measure number into a MEI measure number
    /// @warning score model staff numbers are in 0..N-1
    /// @todo we assume here that MEI staff numbers are in 1..N
    long toMEIMeasureNb(long n);
    
    /// Create a MEI measure element from the given Measure object, as follows:.
    /// ```<measure> = <staffDef> <staff>*```
    mei::Measure* exportMeasure(const Measure& measure);

    /// cast a score model staff number into a MEI staff number
    /// @warning score model staff numbers are in 0..n-1
    /// @todo we assume here that MEI staff numbers are in 1..n
    int toMEIStaffNb(int);

    /// cast a MEI staff number into a score model staff number
    int ofMEIStaffNb(int);

    /// staff differential between a staff and a range.
    /// @param staff current staff: a staff number (in score model).
    /// @param range a staff range  (in score model). must be of size at most 2.
    /// must contain the current staff.
    /// @return 0 if the range is singleton and equal to staff.
    ///         1 if the range contain the current staff and above staff.
    ///         -1 if range contain the current staff and staff below.
    int staffDiff(int staff, const StaffRange& range);
    
    /// Create a MEI staff element containing the elements of the given Measure object,
    /// in the given staff number and in every sequences, as follows:
    /// ```<staff> = <layerDef> <layer>*```
    // @param measure a score model measure object to read
    /// @param staffnb a score model staff number in the given measure.
    mei::Staff* exportStaff(int staffnb);

    /// cast a score model sequence number into a MEI layer number
    /// @warning score model sequence numbers are in 0..n-1
    /// @todo we assume here that MEI layer numbers are in 1..n
    int toMEILayerNb(int);

    /// cast a MEI layer number into a score model sequence number
    int ofMEILayerNb(int);

    /// Create a MEI layer element containing the elementsof the given Sequence object,
    /// in the current staff.
    /// @warning one staff must be open.
    mei::Layer* exportLayer(const Sequence& sequence);

    /// Create an empty MEI beam element.
    /// @return an empty MEI beam element.
    mei::Beam* exportBeam();

    /// Create an empty MEI beam element than may span over several staves.
    /// @param staff current staff: a staff number (in score model).
    /// @param range a staff range  (in score model). must contain the current staff.
    /// @return an empty MEI beam element.
    mei::Beam* exportBeam(int staff, const StaffRange& range);
    
    /// Create a MEI beam element from the given Score Model Tuplet object.
    /// @param n a score model tuplet object to convert. must be beamed @see Beam::Info
    /// @param staff current staff number (in score model) for engraving.
    /// @return an empty MEI beam element.
    mei::Beam* exportBeam(const Tuplet& n, int staff);

    /// Create a MEI beam element from the given Score Model object.
    /// @param n a score model appogiatura object to convert.
    /// @param staff current staff number (in score model) for engraving.
    /// @return an empty MEI beam element.
    mei::Beam* exportBeam(const Appogiatura& n, int staff);

    /// empty MEI beam element. can span over several staves.
    /// @param s1 current staff number (in score model) at the creation of the beam.
    /// @param s2 staff number (in score model) the staff is connected to. must be s1 or s1 - 1 or s1 + 1.
    /// @see https://music-encoding.org/guidelines/v4/content/cmn.html#cmnBeams
    mei::Beam* exportBeam(int s1, int s2);
    
    /// Create a MEI tuplet element from the given Score Model Tuplet object `n`,
    /// and process the content of the tree rooted by `n`.
    /// @param n a score model tuplet object to convert. It must have a label to display.
    /// @param staff current staff number (in score model) for engraving.
    /// @return a MEI beam element.
    mei::Tuplet* exportTuplet(const Tuplet& n, int staff);

    /// Convert the musical duration of the given Score Model Node into CMN (used by MEI).
    cmnd_t exportDuration(const Node& node);

    /// number of flags or beams betewen the children of the given node.
    unsigned long exportInterFlags(const Tuplet& node);

    /// Update the given MEI note element with the given Pitch object
    mei::Note* exportPitch(mei::Note* note, const Pitch& pitch);

    /// Create a MEI note element from the given Note object of musical score model.
    /// @todo delete
    mei::Note* exportNote(const Note& note, const MeterSig& ts);

    /// Create a MEI note element from the given note leaf of the engraving score model.
    mei::Note* exportNote(const LeafNote& node);

    /// Create a MEI note element from the given element of the engraving score model.
    mei::Note* exportNote(const PitchedNote& node);

    /// Construct  a MEI note element to be tied to the given previous MEI note element.
    /// @param note previous MEI note element to which the constructed element will be tied.
    /// @param tie tied node in score model.
    /// @return new MEI note element with the pitch of the given MEI note element
    /// and duration of the given tie node.
    mei::Note* exportTiedNote(mei::Note* note, const Tied& tie);
    
    /// Create a MEI rest element from the given rest leaf of the engraving score model.
    mei::Rest* exportRest(const LeafRest& node);

    /// Construct a MEI rest element ought to be tied to the given previous MEI rest element.
    /// @param rest previous MEI rest element to which the constructed element will be tied
    /// @param tie tied node in score model
    /// @return new MEI rest element with the duration of the given tie node.
    /// @warning it should not happen in a score model.
    mei::Rest* exportRest(mei::Rest* rest, const Tied& tie);
    
    // @return wether the staff number of the given node corresponds to the staff currently proceeded.
    // bool checkStaff(const Leaf& node) const;
    
    /// Construct  a MEI chord element to be tied to the given previous MEI chord element.
    /// @param chord previous MEI note element to which the constructed element will be tied.
    /// @param tie tied node in score model.
    /// @return new empty MEI chord elementwith the duration of the given tie node.
    mei::Chord* exportTiedChord(mei::Chord* chord, const Tied& tie);

    /// Create a MEI space element with the duration of the given object
    mei::Space* exportSpace(const Node& node);

    /// Construct a MEI space element ought to be tied to the given previous MEI space element
    // @param space previous MEI space element to which the constructed element will be tied
    /// @param tie tied node in score model
    /// @return new MEI space element with the duration of the given tie node.
    mei::Space* exportTiedSpace(const Tied& tie);

    /// Create a MEI note element from the given element of the engraving score model.
    /// @param node grace note to convert
    /// @param before if the time for grace note must be stolen from previous note.
    /// @see Appogiatura.playBefore()
    /// @param dur printed duration for the grace note in CMN.
    /// @see Appogiatura.printedDuration()
    /// @param stroke whether a slash must be printed over the stem of grace note.
    /// @see Appogiatura.isAcciaccatura()
    mei::Note* exportGraceNote(const SingleGraceNote& node,
                          bool before = true,
                          cmnd_t dur = 8,
                          bool stroke = false);

    /// Create a MEI note element from the given element of the engraving score model.
    /// @param app an appogiatura containing exactly one grace note.
    mei::Note* exportGraceNote(const Appogiatura& app);

    /// Create a MEI GraceGrp element from the given Score Model Appogiatura object `n`,
    /// and process the content of the tree rooted by `n`.
    /// @param n a score model ornament object to convert.
    /// @return a MEI beam element
    /// @warning the MEI element `<graceGrp>` is not found in libMEI. We replace it by a `<beam>`.
    //mei::GraceGrp* exportGraceGrp(const Appogiatura& n);
    mei::Beam* exportGraceGrp(const Appogiatura& n);

    /// Create a MEI Mordent element from the given Score Model object
    /// @param id reference to the element to which the ornament applies.
    mei::Mordent* exportMordent(const Mordent& n, const std::string id);

    /// Create a MEI Turn element from the given Score Model object
    /// @param id reference to the element to which the ornament applies.
    mei::Turn* exportTurn(const Gruppetto& n, const std::string id);

    /// Create a MEI Trill element from the given Score Model object
    /// @param id reference to the element to which the ornament applies.
    mei::Trill* exportTrill(const LocalTrill& n, const std::string id);

private:

    /// The top MEI document.
    mei::MeiDocument* _mei_document;
    
    /// The MEI div element containing the music.
    mei::Mdiv* _mei_mdiv;
    // mei::MeiElement* _mei_section;
  
    ///  (pimpl) state for stepwise construction of MEI elements inside measures,
    /// by visiting the score model.
    struct State;
    State* _state;
    // std::shared_ptr<State> _state; // problem wit accept (Visitor pattern)

    // previous leaf node visited
    // Leaf* _sm_prevleaf;

    // Ties are inside MEI measure elements (not staff or layer)
    // we store them in a stack when building layers and add them after the completion of each staff.
    // std::stack<mei::Tie*, std::vector<mei::Tie*>> _ties; // stack using vector
    
    // mtu_t currentDate() const;
   
    // bool failNode(const Node* n) const;

};















/// finite state machine for the construction of MEI elements in several steps.
/// uses a visitor for the dispatch of score model objects.
/// several state components and a stack for the traersal of the engraving tree.
struct MEIExporter::State : public SMVisitor
{
public:

    /// initial state of exporter
    /// @param e embedding exporter
    State(MEIExporter& e);

    ~State();
    
    /**
     * Visitor functions for score model objects
     * are used for dispatching subtypes
     */
    
    /// should not be called
    /// @see exportScore, exportParts
    void visitScore(const Score* s);
    
    /// should not be called
    /// @see exportPart, exportSection
    void visitPart(const Part* p);
    
    /// should not be called
    /// @see exportMeasure
    void visitVoice(const Voice* v);

    /// should not be called
    void visitMeasureStack(const MeasureStack* ms);

    /// should not be called
    void visitMeasureStacked(const MeasureStacked* m); // retro-comp

    /// should not be called
    /// @see exportMeasure
    void visitMeasure(const Measure* m);

    /// create and write a MEI staff element
    /// corresponding to the given staff number in the given measure.
    /// @param measure measure in score model
    /// @param mei_measure corresponding MEI Measure element.
    /// @param staffnb number (in scrore model) of the staff to create.
    /// @warning not a  function of the visitor.
    mei::Staff* visitStaff(const Measure& measure,
                           mei::Measure* mei_measure,
                           int staffnb);

    /// create and write a MEI Layer element from the given sequence and its content.
    void visitSequence(const Sequence* s); // bool final = false);

    /// should not be called. no export of musical score model here.
    /// @see visitLeafNote for exporting engraving score model note.
    void visitNote(const Note* n);

    /// should not be called. no export of musical score model here.
    /// @see visitSingleGraceNote for exporting engraving score model grace note.
    void visitGraceNote(const GraceNote* gn);

    /// should not be called. no export of musical score model here.
    /// @see visitLeafRest for exporting engraving score model rest.
    void visitRest(const Rest* r);

    /// should not be called. no export of musical score model here.
    /// @see visitLeafChord for exporting engraving score model chord.
    void visitChord(const Chord* c);
    
    /// should not be called. no export of musical score model here.
    void visitGraceChord(const GraceChord* gc);

    /// should not be called. no export of musical score model here.
    void visitTrill(const Trill* t);

    /// should not be called.
    void visitClef(const Clef* c);

    /// should not be called.
    void visitMeterSig(const MeterSig* ts);

    /// should not be called.
    void visitKeySig(const KeySig* ks);

    /// convert into MEI the content of the given tree.
    void visitRoot(const Root* n);
    
    /// add a new MEI beam or tuplet element or both or none.
    void visitTuplet(const Tuplet* n);
    
    /// add a new decorated MEI note or chord element and its decoration.
    void visitDecorated(const Decorated* n);
    
    /// add a new MEI element tied to the previous MEI element.
    void visitTied(const Tied* n); // void read(const Tied& tie);
    
    /// increase the number of dots read and memorized
    void visitDot(const Dot* n);   // void read(const Dot& dot);

    /// process untied note leaf.
    void visitLeafNote(const LeafNote* n); // void read(const LeafNote& n);

    /// process rest leaf.
    void visitLeafRest(const LeafRest* n); // void read(const LeafRest& n);

    /// process space leaf.
    void visitLeafSpace(const LeafSpace* n); // void read(const LeafSpace& n);

    /// process chord leaf.
    void visitLeafChord(const LeafChord* n); // void read(const LeafChord& n);

    /// visit elements in chord
    void visitPitchedNote(const PitchedNote* n);

    /// @todo deprecated. TBR.
    void visitLeafHarmonic(const LeafHarmonic* n);

    /// should not be called
    void visitOrnament(const Ornament* a);
    
    /// visit elements in ornament
    void visitSingleGraceNote(const SingleGraceNote* n);

    void visitAppogiatura(const Appogiatura* a);

    // Local Annotations

    void visitMordent(const Mordent* m);
    
    void visitGruppetto(const Gruppetto* g);
    
    void visitArticulation(const Articulation* a);
    
    void visitLocalTrill(const LocalTrill* lt);
    
    void visitDynamic(const Dynamic* d);
    
    void visitJump(const Jump* j);
    
    void visitFermata(const Fermata* f);
    
    void visitBreath(const Breath* b);
    
    void visitClefChange(const ClefChange* cc);
    
    void visitTempoChange(const TempoChange* tc);
    
    // Spanning Annotations
    
    void visitSlur(const Slur* s);
    
    void visitVolta(const Volta* v);
    
    void visitOctava(const Octava* o);
    
    void visitVolChange(const VolChange* vc);
    
    void visitPedal(const Pedal* p);
    
    void visitSpanningTrill(const SpanningTrill* st);
    
    void visitVibrato(const Vibrato* v);
    
    void visitGlissando(const Glissando* g);
    
    void visitOsc(const Osc* o);
    
    
    /// start a new MEI staff element.
    /// @param mei_measure the MEI measure element currently in construction.
    /// @param n the staff number in the score model associated to the MEI staff element started,
    /// it must be positive or null.
    /// @return the MEI staff element created.
    /// @warning the score model staff number may differ from the N of the MEI staff element.
    /// @warning no MEI staff must be started.
    mei::Staff* openStaff(mei::Measure* mei_measure, int n);

    /// score model staff number currently read in the score model or UNDEF_STAFF if there is none.
    int currentStaffNb() const;

    /// close the current open MEI staff element
    /// @warning one MEI staff must be open.
    void closeStaff();

    /// start a new MEI layer element associated to the given sequence.
    /// @param sequence a score model sequence
    // @param mei_layer a MEI layer element.
    /// @warning no MEI layer must be open.
    /// @warning the score model sequence number may differ from the N of the MEI layer element.
    /// @warning allocate a new MEI layer element that  must be deallocated elsewhere.
    void openLayer(const Sequence& sequence);

    /// close the current open MEI layer element
    /// @warning one MEI layer must be open.
    void closeLayer();

    /// start a new MEI beam element if appropriate.
    /// @param n a tuplet node in the engraving score model.
    /// @return whether a beam was open.
    bool openBeam(const Tuplet& n);
    //(Beam::Info b, unsigned long d);
    // type of the beam currently open or Beam::Info::None if there is none.
    // Beam::Info beaming() const;

    /// close the beam currently open.
    /// @warning one beam must be open.
    void closeBeam();

    /// start a new  MEI tuplet element if appropriate.
    /// @param n a tuplet node in the engraving score model.
    /// @return whether a tuplet was open.
    bool openTuplet(const Tuplet& n);

    /// close the tuplet currently open.
    /// @param beamed true if the tuplet is beamed,
    /// false if it is unbeamed.
    /// @warning one tuplet must be open.
    void closeTuplet(bool beamed);

    /// wait to receive one dispatched subtype of ornament.
    /// @param n decorated node of score model (decoration = ornament and note or chord).
    void openDeco(const Decorated& n);

    /// stop receiving ornament and decorated note or chord
    /// @warning must have been open.
    void closeDeco();
    
    /// wait to receive one grace note and one note or chord
    void openGraceNote();

    /// stop rceiving grace notes and note
    /// @warning must have been open.
    void closeGraceNote();

    /// start a new MEI beam element if appropriate, (to receive a sequence of > 0 grace notes).
    /// @param app appogiatura imported in this group
    /// @param before whether the grace notes are expected before the decorated leaf (note or chord)
    /// @return whether a beam was open.
    bool openGNGrp(const Appogiatura& app, bool before);

    /// stop rceiving grace notes and note
    /// @warning must have been open.
    void closeGNGrp();

    /// wait to receive one ornament element (mordent, grupetto, trill) and one note or chord
    void openOrnament(const Ornament& ornament, bool before);

    /// stop receiving ornament and decorated note or chord
    /// @warning must have been open.
    void closeOrnament();
    
    /// add new MEI  layer element
    void write(mei::Layer* mei_layer);

    /// add new MEI  beam element
    void write(mei::Beam* mei_beam);
    
    /// add new MEI  tuplet element
    void write(mei::Tuplet* mei_tuplet);

    /// add new MEI note element or space element if note is not of the current staff.
    /// @param mei_note new MEI note element. must not be null.
    /// @param tied whether the MEI note is tied to the previous MEI element.
    void write(mei::Note* mei_note, bool tied = false);
    
    /// add new MEI rest element or space element if rest is not of the current staff.
    void write(mei::Rest* mei_rest);
    
    /// add new MEI chord element
    /// @todo complete
    void write(mei::Chord* mei_chord, bool tied = false);

    /// add new MEI space element.
    void write(mei::Space* mei_space);
    

    //inline mei::Layer* currentStaff() const;

    // number of dots read,  to add to the last event processed
    // size_t dots() const;

    // the stored previous MEI element is tied to the one before it.
    // bool tied() const;
    
    // we are inside a beam in the read score model
    // see MEI `<beam>` element :
    // https://music-encoding.org/guidelines/v4/content/cmn.html#cmnBeams
    // bool beamed() const;


private:

    /// types for the MEI elements in the stack, where the newly created elements are added.
    enum MEIContainerType {

        /// MEI layer.
        /// @see https://music-encoding.org/guidelines/v4/elements/staff.html
        /// state: staff nb  (initil and final).
        STAFF_TYPE,

        /// MEI layer.
        /// @see https://music-encoding.org/guidelines/v4/elements/layer.html
        /// state: voice nb (initil and final).
        LAYER_TYPE,

        /// MEI Beam element or beamed MEI Tuplet element.
        /// @see https://music-encoding.org/guidelines/v4/elements/beam.html
        /// state: 0 (initil and final).
        BEAMED_TYPE,

        /// unbeamed MEI Tuplet element, with label.
        /// @see https://music-encoding.org/guidelines/v4/elements/tuplet.html
        /// state: 0 (initial and final).
        TUPLETU_TYPE,

        /// beamed MEI Tuplet element, with label.
        /// @see https://music-encoding.org/guidelines/v4/elements/tuplet.html
        /// state: 0 (initial and final).
        TUPLETB_TYPE,

        /// open an MEI chord element and wait for the notes to put in this chord.
        /// @see https://music-encoding.org/guidelines/v4/elements/chord.html
        /// state: number of notes to receive (0 is final).
        CHORD_TYPE,

        /// intermediate state, waiting to receive the dispatch of an ornament.
        /// states:
        /// - 0 : wait for an ornament subtype goto 1 (end)
        /// - 1 : finished
        DECO_TYPE,

        // waiting for single grace note, before or after a note or chord (according to state).
        // states:
        // - 0: wait for grace note goto 1
        // - 1: wait for note or chord goto 2 (final)
        // - 10: wait for note or chord goto 11
        // - 11: wait for grace note goto 2 (final)
        // SGN_TYPE,
        
        /// ornament made of several grace notes (at least one),
        /// before or after a note or chord.
        /// @see https://music-encoding.org/guidelines/v4/content/cmn.html#cmnNotesGrace
        /// @see https://music-encoding.org/guidelines/v4/elements/gracegrp.html
        /// states: n > 0
        /// - n: wait for grace note goto n-1
        /// - ...
        /// - 1: wait for grace note goto 0 (final)
        GN_TYPE,

        /// ornament written above a note or chord (case of MEI Mordent, Turn, Trill)
        /// @see https://music-encoding.org/guidelines/v4/elements/mordent.html
        /// @see https://music-encoding.org/guidelines/v4/elements/turn.html
        /// @see https://music-encoding.org/guidelines/v4/elements/trill.html
        /// @see https://music-encoding.org/guidelines/v4/elements/ornam.html
        /// states:
        /// - 0: wait for note or chord goto 1
        /// - 1: wait for ornament goto 2 (final)
        ORNAMENT_TYPE,

    };
    

    /// Environment stored on a stack (called hierarchy),
    /// used to add the new MEI elements.
    /// Base class.
    struct Env
    {
        MEIContainerType type;
        
        /// MEI element to contain the new created elements.
        mei::MeiElement* container;
      
        /// state value for dinstinguishing multiple steps when feeding container
        /// 0 is the initial state.
        size_t step;
        
        // Element of Score Model used to build the container
        // a Root for a MEI Layer or
        // a Tuplet for a MEI Beam or Tuplet
        // const Inner& origine;
        
        Env(MEIContainerType t, mei::MeiElement* c, size_t s);

        /// do not deallocate the MEI container
        ~Env();
    };

    /// Extension of Environment with info on embedding MEI measure element.
    /// the container is a MEI Staff element
    struct EnvStaff : public Env
    {
        /// MEI measure element containing this staff
        mei::Measure* mei_measure;
               
        EnvStaff(mei::Staff* c, size_t n, mei::Measure* m);
        
        ~EnvStaff();
    };
    
    /// Extension of Environment with info for beaming
    /// container is a beam or a tuplet nested in a beam
    struct EnvBeam : public Env
    {
        /// Inter-Elements Beaming Policy. only relevant for beam and tuplet types
        Beam::Info beaminfo;
        
        /// depth to be applied for the Inter-Elements Beaming Policy Beam::Depth,
        /// Only relevant for beam and tuplet types
        unsigned long depth;
               
        EnvBeam(MEIContainerType t, mei::MeiElement* c, size_t s,
                Beam::Info bi, unsigned long d);
        
        ~EnvBeam();
    };

    
    /// Extension of Environment with a decorated event
    struct EnvDeco : public Env
    {

        /// decorated event
        const LeafEvent& decorated;

        /// ornament is before event
        /// @see Decorated
        bool before;
     
        EnvDeco(mei::MeiElement* c, size_t s,
                const LeafEvent& d, bool bf = true);
        
        ~EnvDeco();
    };
    
    /// Extension of Environment for reception of grace notes
    struct EnvGNGrp : public Env
    {
        /// printed duration of each grace note
        cmnd_t duration;

        /// whether the ornament is player before event
        bool before;
        
        /// whether there should be a slash over the stem of the last grace note of the group
        bool stroke;
     
        EnvGNGrp(mei::MeiElement* c, size_t nb,
                 cmnd_t dur = 8, bool bf = true, bool st = false);
        
        ~EnvGNGrp();
    };
    
    /// Extension of Environment for reception of ornament anotation
    /// inherits EnvBeam for the case of ornamented note/chord under a beam.
    struct EnvOrnament : public EnvBeam
    {
        
        /// whether the ornament is player before event
        
        /// id of the decorated element (note or chord). set at the insertion in container.
        std::string id;
     
        EnvOrnament(mei::MeiElement* c, size_t step,
                    Beam::Info bi = Beam::None, unsigned long d = 0);

        EnvOrnament(const EnvBeam& e);

        ~EnvOrnament();
    };
   
    struct BeamParam
    {
        /// Inter-Elements Beaming Policy
        Beam::Info info;
        
        /// depth to be applied for the Inter-Elements Beaming Policy Beam::Depth
        unsigned long depth;
        
        BeamParam(Beam::Info i = Beam::Info::None, unsigned long d = 0);
        
        ~BeamParam() { }
    };
    
    
    /**
     *
     * State components
     *
     */
    
private:
    
    /// container of this state
    MEIExporter& _exporter;

    // score model Score on processing
    // const Score* _sm_Score;

    // score model Measure on processing
    // const Measure* _sm_Measure;

    // MEI Measure element currently in construction.
    // const mei::Measure* _mei_measure;
    
    /// The staff number in score model currently proceeded.
    /// UNDEF_STAFF  is no staff is currently processed.
    int _staffNb;

    // The current MEI layer element.
    // NULL  is no layer is currently processed.
    // @todo rm DEPRECATED (replaced by stack)
    // mei::Layer* _mei_layer;

    // stack of the beam policy symbols encountered on inner Tuplet nodes
    // while traversing the engraving trees of the score model in depth-first mode.
    // std::stack<BeamParam> _beamings;

    /// this stack will contain a hierarchy of the following nested MEI container elements:
    /// - exactly one mei::Layer* (first-in)
    /// - one or zero mei::Beam* (second-in)
    /// - one or several mei::Tuplet* (top)
    /// The built elements Note, Rest, Chord and Space will be added to the top of hierarchy.
    /// push is done by corresponding open functions.
    /// pop is done by corresponding close functions.
    std::stack<std::unique_ptr<Env>> _hierorchy;

    /// Inter-Elements Beaming Policy, to be applied to compute the number of beams between
    /// the previous element and the current element processed.
    /// More precisely, its value is always the beaming policy in the label of the innermost common
    /// ancestor of the previous event and the new event in the engraving tree.
    /// It is Beam::Info::Undef if there is no previous element (inital value).
    /// It is updated after processing a new element and when closing beam.
    /// @todo cross-bar beams not supported by this approachj
    /// @see MEI `<beam>` element:
    /// https://music-encoding.org/guidelines/v4/content/cmn.html#cmnBeams
    /// @see MEI beamSecondary attribute:
    /// https://music-encoding.org/guidelines/v4/attribute-classes/att.beamsecondary.html
    /// @see MEI beamPresent attribute:
    /// https://music-encoding.org/guidelines/v4/attribute-classes/att.beampresent.html
    BeamParam _pbeaming;

    /// export of the local MEI elements (annotations) to be added after the current MEI staff.
    // std::queue<mei::MeiElement*,std::vector<mei::MeiElement*> > _buffer;
    std::queue<mei::MeiElement*> _buffer;
    
    /// previous MEI element constructed is a mei::Note (otherwise NULL)
    mei::Note* _mei_pnote;

    /// last previous MEI note or chord element constructed is mei::Noted (otherwise NULL)
    /// it may differ from _mei_pchord only if _mei_prest or _mei_pspace non NULL (previous is rest or space).
    mei::Note* _mei_ppnote;

    /// previous MEI element constructed is a mei::Rest (otherwise NULL)
    mei::Rest* _mei_prest;

    /// previous MEI element constructed is a mei::Chord (otherwise NULL)
    mei::Chord* _mei_pchord;

    /// last previous MEI note or chord element constructed is a mei::Chord (otherwise NULL)
    /// it may differ from _mei_pchord only if _mei_prest or _mei_pspace non NULL (previous is rest or space).
    mei::Chord* _mei_ppchord;

    /// previous MEI element constructed is a mei::Space (otherwise NULL)
    mei::Space* _mei_pspace;

    /// number of dots to be added to the previous MEI element stored.
    size_t _dots;

    /// the stored previous MEI element is tied to the one before it.
    bool _ptied;

    
    // depth to be applied for the Inter-Elements Beaming Policy Beam::Depth
    // unsigned long _pdepth;
    
    // number of beams of the previous element.
    // = log2(musical duration) - 2
    // size_t _pbeams;
    
    // we are inside a beam
    // @todo RM deprecated
    // bool _beamed;
    
    /// update the dots attribute in the previous MEI element.
    void update_dots();
    
    /// add (if needed) a tie attribute in the previous MEI element.
    /// @param tied whether the new MEI element is tied to the previous one.
    /// @warning it should be check beforehand whether a tie is legal before
    /// the previous and the new MEI elements.
    void update_tie(bool tied);

    /// add (if needed) a breaksec attribute to the previous MEI element.
    /// @param mei_note latest mei note element added
    void update_breaksec(mei::Note* mei_note);

    /// add (if needed) a breaksec attribute to the previous MEI element.
    /// @param mei_chord latest mei chord element added
    void update_breaksec(mei::Chord* mei_chord);

    /// add (if needed) a breaksec attribute to the previous MEI element.
    // @param nbFlags number of flags or beams for the latest mei element added.
    void update_breaksec(unsigned long nbFlags);

    /// beaksec value for the previous MEI element.
    /// @param nf1 number of flags or beams for the previous note or chord mei element added.
    /// @param nf2 number of flags or beams for the latest note or chord mei element added.
    // @param pol a beaming policy
    /// @return beaksec value for the previous MEI element
    /// or 99 if no beaksec should be added.
    unsigned long breaksec(unsigned long nf1,
                           unsigned long nf2);

    /// factorization of closeBeam and closeTuplet
    /// @param t type of the container to close : BEAM or TUPLET.
    void closeBT(MEIContainerType t);
    
    /// MEI element (layer, beam or tuplet) where the MEI events (note, rest, chord, space) should be added.
    /// can be NULL.
    mei::MeiElement* topContainer() const;
    
    /// type of environment at the top of stack.
    /// @warning the stack must not be empty.
    MEIContainerType topType() const;

    /// utility for update_breaksec
    /// @return number of beams of previous note or chord element added,
    /// or 99 if there are no former note or chord element.
    /// @todo TBR
    unsigned long previousNbFlags();
        
    /// this state contains no note, rest, chord or space element.
    bool empty() const;
    
    /// invariant on the pointers to previous MEI element added (when completing a MEI Layer):
    /// all NULL for the first element and exactly one non-NULL otherwise.
    bool invariant() const;

    
    /// update previous element of type note with current element of type note.
//    void update_note(mei::Note* current, bool tied);
//    void update_rest(mei::Note* current, bool tied);
//    void update_chord(mei::Note* current, bool tied);
//    void update_space(mei::Note* current, bool tied);
//
//    void update_note(mei::Rest* current);
//    void update_rest(mei::Rest* current);
//    void update_chord(mei::Rest* current);
//    void update_space(mei::Rest* current);
//
//    void update_note(mei::Chord* current);
//    void update_rest(mei::Chord* current);
//    void update_chord(mei::Chord* current);
//    void update_space(mei::Chord* current);
//
//    void update_note(mei::Space* current);
//    void update_rest(mei::Space* current);
//    void update_chord(mei::Space* current);
//    void update_space(mei::Space* current);
    
    
    /**
     * MEI utilities
     */
    
    /// the given layer contains no note, rest, chord or space element.
    /// @param layer layer to check. must not be NULL.
    static bool empty(mei::Layer* layer);

    // number of flags or beams for the given element
    // @param e MEI element. must not be NULL. Its attribute duration will be used.
    // static int nbFlags(mei::MeiElement* e);

    /// number of flags or beams for the given element
    /// @param n MEI note element. must not be NULL. Its attribute duration will be used.
    static unsigned long nbFlags(mei::Note* n);

    /// number of flags or beams for the given element
    /// @param n MEI chord element. must not be NULL. Its attribute duration will be used.
    static unsigned long nbFlags(mei::Chord* n);

    /// number of flags or beams for the given DurationMusical attribute
    static unsigned long nbFlags(mei::MeiAttribute* a);
    
    /// cast MEI duration value (string) to CMN duration
    /// see https://music-encoding.org/guidelines/v4/data-types/data.duration.cmn.html
    static cmnd_t value_tocmnd(const std::string& str);

};


} // end namespace ScoreModel

#endif /* SMMEI_hpp */

/// @} // end group scoremodel

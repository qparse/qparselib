//
//  SMPrinter.cpp
//  squant2
//
//  Created by Florent Jacquemard on 24/11/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "SMPrinter.hpp"

namespace ScoreModel {


Printer::Printer(std::ostream& os, bool pf):
o(os),
partwise(pf)
{ }


void Printer::visitScore(const Score* s)
{
//    WARN("PrettyPrinter: print(Score*) should not be called");
//    WARN("PrettyPrinter: call print_partwise or print_timewise");
//    o << (partwise?" Part-wise":"Time-wise") << " presentation:\n";
    assert(s);
    o << "Score " << s->id() << std::endl;
    
    for (PartSeq::const_iterator ip = s->cbegin(); ip != s->cend(); ++ip)
    {
#ifdef SM_MEASURE_STATIC
        Part& p = *ip;
        p.accept(this);
#else
        Part* p = *ip;
        assert(p);
        p->accept(this);
#endif
    }
//    if (partwise)
//        print_partwise(*s);
//    else
//       print_timewise(*s);
}


void Printer::print_partwise(const Score& s)
{
    size_t p = s.nbParts();
    o << "Score " << s.id();
    o << " (" << p << " part" << ((p > 1)?"s":"") << ")\n";

    for (PartSeq::const_iterator ip = s.cbegin(); ip != s.cend(); ++ip)
    {
#ifdef SM_MEASURE_STATIC
        Part& p = *ip;
        p.accept(this);
#else
        Part* p = *ip;
        assert(p);
        p->accept(this);
#endif
    }
}


void Printer::visitPart(const Part* p)
{
    assert(p);
    o << "Part " << p->id() << std::endl;

    for(Wozzeck::const_iterator iv = p->cbegin(); iv != p->cend(); ++iv)
    {
#ifdef SM_MEASURE_STATIC
        Voice& v = (*iv).second;
        v.accept(this);
#else
        Voice* v = (*iv).second;
        assert(v);
        v->accept(this);
#endif
    }
    o << std::endl;
    
    for (int i = 0; i < p->nbStaves(); ++i)
    {
        const StaffDef& staff = p->getStaff(i);
        staff.print(o);
        o << std::endl;
    }

    o << std::endl;

    for (size_t i = 0; i < p->nbMeasures(); ++i)
    {
        //o << "Measure " << i << " : ";
        const Measure& m = p->getMeasure(i);
        m.accept(this);
    }
    o << std::endl;
}


void Printer::visitVoice(const Voice* v)
{
    assert(v);
    o << "Voice " << v->number() << std::endl;
    for(EventSeq::const_iterator iae = v->cbegin(); iae != v->cend(); ++iae)
    {
        AtomicEvent* ae = *iae;
        assert(ae);
        o << "  ";
        ae->accept(this); // dispatch
        o << std::endl;
    }
}


void Printer::visitNote(const Note* n)
{
    assert(n);
    n->print(o);
}


void Printer::visitGraceNote(const GraceNote* gn)
{
    assert(gn);
    gn->print(o);
}

void Printer::visitRest(const Rest* r)
{
    assert(r);
    r->print(o);
}

void Printer::visitChord(const Chord* n)
{
    assert(n);
    n->print(o);
}


void Printer::visitGraceChord(const GraceNote* gc)
{
    assert(gc);
    gc->print(o);
}


void Printer::visitTrill(const Trill* t)
{
    assert(t);
    t->print(o);
}


void Printer::print_timewise(const Score& s)
{
    o << "Score " << s.id() << std::endl;
    bool cont = true;
    size_t n = 0;
    
    while (cont)
    {
        o << "Measure " << n  << std::endl;
        cont = false;
        for (PartSeq::const_iterator ip = s.cbegin(); ip != s.cend(); ++ip)
        {
#ifdef SM_MEASURE_STATIC
            Part& p = *ip;
#else
            assert(*ip);
            Part& p = *(*ip);
#endif
            if (n < p.nbMeasures())
            {
                o << "Part " << p.id() << ": ";
                const Measure& m = p.getMeasure(n);
                m.accept(this);
                o << std::endl;
                cont = true;
            }
        }
        n++;
    }
}


void Printer::print_timewise_stacks(const Score& s)
{
    size_t m = s.nbMeasures();
    o << "Score " << s.id();
    o << " (" << m << " measure" << ((m > 1)?"s":"") << ")\n";
    
    for (MeasureStackSeq::const_iterator ims = s.cbeginMeasures();
         ims != s.cendMeasures(); ++ims)
    {
#ifdef SM_MEASURE_STATIC
        MeasureStack& ms = *ims;
        ms.accept(this);
#else
        MeasureStack* ms = *ims;
        assert(ms);
        ms->accept(this);
#endif
    }
}


void Printer::visitClef(const Clef* c)
{
    assert(c);
    c->print(o);
}


void Printer::visitMeterSig(const MeterSig* ts)
{
    assert(ts);
    ts->print(o);
}


void Printer::visitKeySig(const KeySig* ks)
{
    assert(ks);
    ks->print(o);
}


void Printer::visitMeasureStack(const MeasureStack* ms)
{
    assert(ms);
    //DEBUGU("Measure Stack {} ({} measures)", ms->nb(), ms->size());
    for (MeasureStackedSeq::const_iterator im = ms->cbegin();
         im != ms->cend(); ++im)
    {
#ifdef SM_MEASURE_STATIC
        MeasureStacked& m = (*im);
        m.accept(this);
#else
        MeasureStacked* m = (*im);
        assert(m);
        m->accept(this);
#endif
    }
}


void Printer::visitMeasureStacked(const MeasureStacked* m)
{
    assert(m);
    visitMeasure(m);
}


void Printer::visitMeasure(const Measure* m)
{
    assert(m);
    //DEBUGU("Measure {} ({} sequences)", m->nb(), m->nbVoices());
    for (SequenceSeq::const_iterator is = m->cbegin(); is != m->cend(); ++is)
    {
#ifdef SM_MEASURE_STATIC
        Sequence& s = *is;
        s.accept(this);
#else
        Sequence* s = *is;
        assert(s);
        s->accept(this);
#endif
    }
}


void Printer::visitSequence(const Sequence* s) // bool final)
{
    o << "Measure " << std::setw(3) << std::right << s->measureNb();
    // o << " Part " << s->measure().part().id();
    o << " Seq." << s->voiceNb() << ": ";
    assert(s);
    assert(s->isSequence());
    Root* t = s->tree();
    if (t == NULL) { o << "  NaN\n"; return; }
    assert(t->isRoot());
    o << "  ";
    t->accept(this);
    o << " |\n";  // single bar
}
// o << " 𝄂\n";  // double bar


void Printer::visitRoot(const Root* r)
{
    assert(r);
    assert(r->complete());
    assert(r->node());
    // print the encapsulated tree
    r->node()->accept(this); // dispatch
}


void Printer::visitTuplet(const Tuplet* n)
{
    assert(n);
    assert(n->complete());
    size_t ar = n->size();
    assert(ar > 1);
    const TupletLabel& label = n->label();
    const Beam::Info& beam = n->beaming();
    std::string symbol = std::string();

    switch (beam) {
        case Beam::Info::None:
            symbol += "u"; // unbeamed
            break;

        case Beam::Info::Depth:
            symbol += "d";
            break;

        case Beam::Info::Full:
            symbol += "b";
            break;

        case Beam::Info::Minus:
            symbol += "m";
            break;
            
        default:
            ERROR("unexpected Beam info constant");
            break;
    }

    symbol += std::to_string(ar);
    //    += "\u0305";  // for overline
    o << symbol;
    if (label.arity() > 0)
    {
        o << "\"" << label.arity();
        if (label.multiplicity() > 0)
            o << ":" << label.multiplicity();
        o << "\"";
    }
    if (n->multiplicity() > 1)
        o << ":" << n->multiplicity();
    o << "[ ";

    for (size_t i = 0; i < ar; ++i)
    {
        assert(n->sub(i));
        n->sub(i)->accept(this);
        o << " ";
    }
    o << "]";
}


void Printer::visitDecorated(const Decorated* n)
{
    assert(n);
    assert(n->complete());
    Ornament* orn = n->decoration();
    assert(orn);
    assert(orn->size() > 0);
    Leaf* leaf = n->decorated();
    assert(leaf);
    if (! n->ornamentFirst())
    {
        leaf->accept(this);
    }
    orn->accept(this);

}


void Printer::visitTied(const Tied* n)
{
    assert(n);
    o << "⏑";      // continuation
}


void Printer::visitDot(const Dot* n)
{
    assert(n);
    o << "・";  // dot
}


void Printer::visitLeafNote(const LeafNote* n)
{
    assert(n);
    AtomicEvent* ae = n->event();
    assert(ae);
    assert(ae->isNote());
    Note* aen = dynamic_cast<Note*>(ae);
    assert(aen);
    const Pitch& p = aen->pitch();
    if (p.unpitched())
        o << "●";      // note/chord event
    else
        p.print(o);
    if (n->multiplicity() > 1)
        o << ":" << n->multiplicity();

    //ae->accept(this);
}


void Printer::visitLeafRest(const LeafRest* n)
{
    assert(n);
    //o << "R";
    o << "○";      // rest
}


/// @todo STUB complete
void Printer::visitLeafChord(const LeafChord* n)
{
    assert(n);
    o << "CHORD";
}


/// @todo STUB complete
void Printer::visitLeafHarmonic(const LeafHarmonic* n)
{
    assert(n);
    o << "HARM";
}


//else if (SymbLabel::multi(a))
//{
//    os << "+";      // continuation
//}
//}
//else if (SymbLabel::grace(a))
//{
// assert(SymbLabel::arity(a) == 2);
// os << "G"; // grace note (composed)
//}



void Printer::visitOrnament(const Ornament* a)
{
    ERROR("visit Ornament should not be called");
}


void Printer::visitAppogiatura(const Appogiatura* a)
{
    assert(a);
    std::string par = std::string("|");
    if (a->isAppogiatura())
        par = std::string("/");
    else if (a->isAcciaccatura())
        par = std::string("\\");
    o << par;
    for (size_t i = 0; i < a->size(); ++i)
    {
        assert(a->at(i));
        const GraceNote* gn = a->at(i)->gracenote();
        assert(gn);
        const Pitch& p = gn->pitch();
        if (p.unpitched())
            o << "❛";  // grace note
        else
            p.print(o);
        if (i != a->size() - 1) { o << " "; }
    }
    o << par;
}


void Printer::visitMordent(const Mordent* m)
{
    assert(m);
    if (m->upper())
        o << "M";
    else
        o << "W";
}


void Printer::visitGruppetto(const Gruppetto* g)
{
    assert(g);
    if (g->inverted())
        o << "-G";
    else
        o << "G";
}

/// @todo complete
void Printer::visitArticulation(const Articulation* a)
{
    assert(a);
}

/// @todo complete
void Printer::visitLocalTrill(const LocalTrill* lt)
{
    assert(lt);
}

/// @todo complete
void Printer::visitDynamic(const Dynamic* d)
{
    assert(d);
}

/// @todo complete
void Printer::visitJump(const Jump* j)
{
    assert(j);
}

/// @todo complete
void Printer::visitFermata(const Fermata* f)
{
    assert(f);
}

/// @todo complete
void Printer::visitBreath(const Breath* b)
{
    assert(b);
}

/// @todo complete
void Printer::visitClefChange(const ClefChange* cc)
{
    assert(cc);
}

/// @todo complete
void Printer::visitTempoChange(const TempoChange* tc)
{
    assert(tc);
}


/// @todo complete
void Printer::visitSlur(const Slur* s)
{
    assert(s);
}

/// @todo complete
void Printer::visitVolta(const Volta* v)
{
    assert(v);
}
   
/// @todo complete
void Printer::visitOctava(const Octava* o)
{
    assert(o);
}

/// @todo complete
void Printer::visitVolChange(const VolChange* vc)
{
    assert(vc);
}

/// @todo complete
void Printer::visitPedal(const Pedal* p)
{
    assert(p);
}

/// @todo complete
void Printer::visitSpanningTrill(const SpanningTrill* st)
{
    assert(st);
}

/// @todo complete
void Printer::visitVibrato(const Vibrato* v)
{
    assert(v);
}

/// @todo complete
void Printer::visitGlissando(const Glissando* g)
{
    assert(g);
}

/// @todo complete
void Printer::visitOsc(const Osc* o)
{
    assert(o);
}


} // end namespace ScoreModel

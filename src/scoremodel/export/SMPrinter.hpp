//
//  SMPrinter.hpp
//  squant2
//
//  Created by Florent Jacquemard on 24/11/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

/// @addtogroup output
/// @{


#ifndef SMPrinter_hpp
#define SMPrinter_hpp

#include <stdio.h>
//#include <stdlib.h>
#include <assert.h>
#include <iostream>
#include <iomanip>

#include "MTU.hpp"
#include "SMVisitor.hpp"
#include "Score.hpp"
#include "Part.hpp"
#include "Voice.hpp"
#include "Note.hpp"
#include "GraceNote.hpp"
#include "Rest.hpp"
#include "Chord.hpp"
#include "Trill.hpp"

#include "Root.hpp"
#include "Tuplet.hpp"
#include "Decorated.hpp"
#include "Ornament.hpp"
#include "Tied.hpp"
#include "Dot.hpp"
#include "LeafNote.hpp"
#include "LeafRest.hpp"
#include "LeafChord.hpp"
#include "LeafHarmonic.hpp"

#include "Ornament.hpp"
#include "Appogiatura.hpp"
#include "Mordent.hpp"
#include "Gruppetto.hpp"


namespace ScoreModel {


/// Pretty Printer for the score and engraving model.
/// The score can be printed part-wise or time-wise
/// @see https://w3c.github.io/mnx/overview/#score-structure
class Printer : public SMVisitor
{
public:

    /// @param os output stream
    /// @param pflag true if the score must be printed part-wise,
    ///             false if the score must be printed time-wise.
    Printer(std::ostream& os, bool pflag = true);
    
    void print(const Score& s) { visitScore(&s); }
    void print_partwise(const Score& s);
    void print_timewise(const Score& s);
    
    void visitScore(const Score* s);
    void visitPart(const Part* p);
    void visitVoice(const Voice* v);

    void visitNote(const Note* n);
    void visitGraceNote(const GraceNote* gn);
    void visitRest(const Rest* r);
    void visitChord(const Chord* c);
    void visitGraceChord(const GraceNote* gc);
    void visitTrill(const Trill* t);

    void visitClef(const Clef* c);
    void visitMeterSig(const MeterSig* ts);
    void visitKeySig(const KeySig* ks);

    void visitMeasureStack(const MeasureStack* ms);
    void visitMeasureStacked(const MeasureStacked* ms); // retro
    void visitMeasure(const Measure* m);
    void visitSequence(const Sequence* s); // bool final = false);

    void visitRoot(const Root* n);
    void visitTuplet(const Tuplet* n);
    void visitDecorated(const Decorated* n);
    void visitTied(const Tied* n);
    void visitDot(const Dot* n);
    void visitLeafNote(const LeafNote* n);
    void visitLeafRest(const LeafRest* n);
    void visitLeafChord(const LeafChord* n);
    void visitLeafHarmonic(const LeafHarmonic* n);
    
    void visitOrnament(const Ornament* a);
    void visitAppogiatura(const Appogiatura* a);
    void visitMordent(const Mordent* m);
    void visitGruppetto(const Gruppetto* g);
    void visitArticulation(const Articulation* a);
    void visitLocalTrill(const LocalTrill* lt);
    void visitDynamic(const Dynamic* d);
    void visitJump(const Jump* j);
    void visitFermata(const Fermata* f);
    void visitBreath(const Breath* b);
    void visitClefChange(const ClefChange* cc);
    void visitTempoChange(const TempoChange* tc);
    
    // Spanning Annotations
    void visitSlur(const Slur* s);
    void visitVolta(const Volta* v);
    void visitOctava(const Octava* o);
    void visitVolChange(const VolChange* vc);
    void visitPedal(const Pedal* p);
    void visitSpanningTrill(const SpanningTrill* st);
    void visitVibrato(const Vibrato* v);
    void visitGlissando(const Glissando* g);
    void visitOsc(const Osc* o);

private:
    
    /// output channel
    std::ostream& o;

    /// whether the score should be printed part-wise or time-wise.
    bool partwise;
    
    /// for retro compatibility
    void print_timewise_stacks(const Score& s);

        
};


} // end namespace ScoreModel

#endif /* SMPrinter_hpp */

/// @} // end group scoremodel


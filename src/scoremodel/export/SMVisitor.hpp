//
//  SMVisitor.hpp
//  squant2
//
//  Created by Florent Jacquemard on 25/11/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup output
/// @{

#ifndef SMVisitor_hpp
#define SMVisitor_hpp

#include <stdio.h>
#include <assert.h>
#include <exception>

//#include "Score.hpp"
//#include "Part.hpp"
//#include "Voice.hpp"
//#include "Root.hpp"
//#include "Tuplet.hpp"
//#include "Decorated.hpp"
//#include "Ornament.hpp"
//#include "Tied.hpp"
//#include "Dot.hpp"
//#include "LeafNote.hpp"
//#include "LeafRest.hpp"
//#include "LeafChord.hpp"
//#include "LeafHarmonic.hpp"


namespace ScoreModel {

class sme_exception: public std::exception
{
  virtual const char* what() const throw()
  {
      return "Bad structure in Score Model";
  }
};


class Score;
class Part;
class Voice;
class Note;
class GraceNote;
class Rest;
class Chord;
class GraceChord;
class Trill;
class Clef;
class MeterSig;
class KeySig;
class MeasureStack;
class Measure;
class Sequence;

class Root;
class Tuplet;
class Decorated;
class Ornament;
class Tied;
class Dot;
class LeafNote;
class LeafRest;
class LeafChord;
class LeafHarmonic;
class LeafSpace;
class PitchedNote;
class SingleGraceNote;

class Appogiatura;
class Mordent;
class Gruppetto;
class Articulation;
class LocalTrill;
class Dynamic;
class Jump;
class Fermata;
class Breath;
class ClefChange;
class TempoChange;

class Slur;
class Volta;
class Octava;
class VolChange;
class Pedal;
class SpanningTrill;
class Vibrato;
class Glissando;
class Osc;

/// const Visitor for the musical and engraving score models.
class SMVisitor
{
public:
    
    // Musical Score Model
    virtual void visitScore(const Score*);
    virtual void visitPart(const Part*);
    virtual void visitVoice(const Voice*);
    virtual void visitNote(const Note*);
    virtual void visitGraceNote(const GraceNote*);
    virtual void visitRest(const Rest*);
    virtual void visitChord(const Chord*);
    virtual void visitGraceChord(const GraceChord*);
    virtual void visitTrill(const Trill*);
    virtual void visitClef(const Clef*);
    virtual void visitMeterSig(const MeterSig*);
    virtual void visitKeySig(const KeySig*);
    virtual void visitMeasureStack(const MeasureStack*);
    virtual void visitMeasureStacked(const Measure*); // retro-compatibility
    virtual void visitMeasure(const Measure*);
    virtual void visitSequence(const Sequence*);
    
    // Rhythm Trees
    virtual void visitRoot(const Root*);
    virtual void visitTuplet(const Tuplet*);
    virtual void visitDecorated(const Decorated*);
    virtual void visitTied(const Tied*);
    virtual void visitDot(const Dot*);
    virtual void visitLeafNote(const LeafNote*);
    virtual void visitLeafRest(const LeafRest*);
    virtual void visitLeafChord(const LeafChord*);
    virtual void visitLeafHarmonic(const LeafHarmonic*);
    virtual void visitLeafSpace(const LeafSpace*);

    // Local Annotations
    virtual void visitPitchedNote(const PitchedNote*);
    virtual void visitSingleGraceNote(const SingleGraceNote*);
    virtual void visitOrnament(const Ornament*);
    virtual void visitAppogiatura(const Appogiatura*);
    virtual void visitMordent(const Mordent*);
    virtual void visitGruppetto(const Gruppetto*);
    virtual void visitArticulation(const Articulation*);
    virtual void visitLocalTrill(const LocalTrill*);
    virtual void visitDynamic(const Dynamic*);
    virtual void visitJump(const Jump*);
    virtual void visitFermata(const Fermata*);
    virtual void visitBreath(const Breath*);
    virtual void visitClefChange(const ClefChange*);
    virtual void visitTempoChange(const TempoChange*);
    
    // Spanning Annotations
    virtual void visitSlur(const Slur*);
    virtual void visitVolta(const Volta*);
    virtual void visitOctava(const Octava*);
    virtual void visitVolChange(const VolChange*);
    virtual void visitPedal(const Pedal*);
    virtual void visitSpanningTrill(const SpanningTrill*);
    virtual void visitVibrato(const Vibrato*);
    virtual void visitGlissando(const Glissando*);
    virtual void visitOsc(const Osc*);
   
};

} // endamespace ScoreModel

#endif /* SMVisitor_hpp */

/// @} // end group scoremodel


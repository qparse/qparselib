//
//  SMVisitor.cpp
//  squant2
//
//  Created by Florent Jacquemard on 25/11/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "SMVisitor.hpp"

namespace ScoreModel {


// Musical Score Model
void SMVisitor::visitScore(const Score*) { }
void SMVisitor::visitPart(const Part*) { }
void SMVisitor::visitVoice(const Voice*) { }
void SMVisitor::visitNote(const Note*) { }
void SMVisitor::visitGraceNote(const GraceNote*) { }
void SMVisitor::visitRest(const Rest*) { }
void SMVisitor::visitChord(const Chord*) { }
void SMVisitor::visitGraceChord(const GraceChord*) { }
void SMVisitor::visitTrill(const Trill*) { }
void SMVisitor::visitClef(const Clef*) { }
void SMVisitor::visitMeterSig(const MeterSig*) { }
void SMVisitor::visitKeySig(const KeySig*) { }
void SMVisitor::visitMeasureStack(const MeasureStack*) { }
void SMVisitor::visitMeasureStacked(const Measure*) { }
void SMVisitor::visitMeasure(const Measure*) { }
void SMVisitor::visitSequence(const Sequence*) { }

// Rhythm Trees
void SMVisitor::visitRoot(const Root*) { }
void SMVisitor::visitTuplet(const Tuplet*) { }
void SMVisitor::visitDecorated(const Decorated*)  {  }
void SMVisitor::visitTied(const Tied*)  {  }
void SMVisitor::visitDot(const Dot*)  {  }
void SMVisitor::visitLeafNote(const LeafNote*)  {  }
void SMVisitor::visitLeafRest(const LeafRest*)  {  }
void SMVisitor::visitLeafChord(const LeafChord*)  {  }
void SMVisitor::visitLeafHarmonic(const LeafHarmonic*)  {  }
void SMVisitor::visitLeafSpace(const LeafSpace*)  {  }
void SMVisitor::visitPitchedNote(const PitchedNote*)  {  }
void SMVisitor::visitSingleGraceNote(const SingleGraceNote*)  {  }

// Local Annotations
void SMVisitor::visitOrnament(const Ornament*)  {  }
void SMVisitor::visitAppogiatura(const Appogiatura*)  {  }
void SMVisitor::visitMordent(const Mordent*)  {  }
void SMVisitor::visitGruppetto(const Gruppetto*)  {  }
void SMVisitor::visitArticulation(const Articulation*)  {  }
void SMVisitor::visitLocalTrill(const LocalTrill*)  {  }
void SMVisitor::visitDynamic(const Dynamic*)  {  }
void SMVisitor::visitJump(const Jump*)  {  }
void SMVisitor::visitFermata(const Fermata*)  {  }
void SMVisitor::visitBreath(const Breath*)  {  }
void SMVisitor::visitClefChange(const ClefChange*)  {  }
void SMVisitor::visitTempoChange(const TempoChange*)  {  }

// Spanning Annotations
void SMVisitor::visitSlur(const Slur*)  {  }
void SMVisitor::visitVolta(const Volta*)  {  }
void SMVisitor::visitOctava(const Octava*)  {  }
void SMVisitor::visitVolChange(const VolChange*)  {  }
void SMVisitor::visitPedal(const Pedal*)  {  }
void SMVisitor::visitSpanningTrill(const SpanningTrill*)  {  }
void SMVisitor::visitVibrato(const Vibrato*)  {  }
void SMVisitor::visitGlissando(const Glissando*)  {  }
void SMVisitor::visitOsc(const Osc*)  {  }


} // endamespace ScoreModel

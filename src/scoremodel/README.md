# `src/scoremodel`

Intermediate abstract model used to represent a score.

It can be imported and exported from and to various formats.
It can also be built from the results of parsing  (*i.e.* from `Run`s of WTAs).

In a score model, the music information is organized hierarchicaly in parts (one part for one instrument), 
every part contains one or several monophonic voices, 
and a monophonic voice is a sequence of atomic events (notes, chords and rests) without overlaps.

Moreover, the score is cut into a sequence of measures. 
Every  measure has a fixed duration of 1 MTU (Musical Time Unit), and an associated Time Signature.
It contains the engraving information for the contents of the voices in the same time window: the rhythmic structure (beams, ties dots...) stored in rhythm trees, pitch spelling, expressive and dynamic marks *etc*.

```mermaid
graph LR

    INq -- qparse --> TT
    QM -- import --> MM
    TT -- import --> EM
    EM -- rewriting --> EM
    MM -- pitchspelling --> MM
    SM -- export --> OUT
    EM --> MM
    MM --> EM
    
    subgraph inpiut
    INq{{Input qparse}}
    UM(performace MIDI file)
    CFG(Rythm Grammar)
    end 

    subgraph put
    IN{{Input}}
    QM(quantized MIDI file)
    TT(Parse Trees)
    end 
    
    subgraph scoremodel
    SM{{Score Model}}
    MM(Musical Model)
    EM(Engraving Model)
    end
    
    subgraph output
    OUT{{Output}}
    MEI(MEI file)
    PP("text (pretty printer)")
    end

    



```






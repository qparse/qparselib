// Implementation of Part functions

#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>

#include "Voice.hpp"
#include "Pitch.hpp"
#include "Part.hpp"
#include "MeasureStack.hpp"
#include "Note.hpp"


namespace ScoreModel {

// Last note met during a voice creation. Used for ties.
// Try to find better
// static Event* lastEvent = NULL;


// Main constructor
// duration initialy null
Voice::Voice(mtu_t start, Part& part, int i) :
Event(start, MTU::ZERO, part.id() + name() + std::to_string(i)),
_nb(i),
_part(part),
_events(),  // initialy empty
_ambitus()  // undef
{ }


Voice::Voice(Part& part, int i) :
Event(MTU::ZERO, MTU::ZERO, part.id() + name() + std::to_string(i)),
_nb(i),
_part(part),
_events(),  // initialy empty
_ambitus()  // undef
{ }


Voice::Voice(mtu_t start, Part& part, const std::string name):
Event(start, MTU::ZERO, name),
_nb(0),
_part(part),
_events(),  // initialy empty
_ambitus()  // undef
{
    assert(! name.empty());
}


// Destructor
Voice::~Voice()
{
    TRACE("delete Voice {} in part:{}", id(), _part.id());
    for (AtomicEvent* event : _events)
    {
        assert(event);
        delete event;
    }
}


// Add an event
void Voice::add(AtomicEvent* event)
{
    assert(event);
    
    //event->setVoice(this); // already set at event's construction

    // start date of the voice is the start date of its first event
    if (_events.empty())
    {
        _onset = event->date();
    }
    // previous voice end
    mtu_t vend = _onset + _duration;
    // event end
    mtu_t eend = event->date() + event->duration();
    /// @todo vend == event->date() if we want no gap
    assert(vend <= event->date()); // voice is monophonic
    _duration += event->date() - vend;
    _duration += event->duration();
    _events.push_back(event);
    _ambitus.add(event);
}


Part& Voice::part() const
{
    return _part;
}


Score& Voice::score() const
{
    return _part.score();
}


/// @todo STUB
Voice* Voice::trimMeasure(const MeasureStack& m) const
{
    const std::string measName = id() + "_" + m.id();
    Voice* measVoice = new Voice(m.date(), _part, measName);

//    for (AtomicEvent* const event : _events)
    for (EventSeq::const_iterator ie = _events.cbegin();
         ie != _events.cend(); ++ie)
    {
        AtomicEvent* event = *ie;
        if (MTU::measure(event->date()) == m.nb())
            measVoice->add(event);
    }

    return measVoice;
}


const Ambitus& Voice::ambitus() const
{
    if (_ambitus.undef())
        WARN("Voice {} : ambitus is undef", _nb);
    return _ambitus;
}

//   std::pair<Pitch, Pitch> getRange() const
//   {
//    std::pair<Pitch, Pitch> range(Pitch(127, Pitch::MIDI),
//                                  Pitch(0, Pitch::MIDI));
//    for (AtomicEvent* event : _events) {
//        if (event->isNote()) {
//            Note* noteEvent = (Note*) event;
//            Pitch pitch = noteEvent->pitch();
//            if (pitch.midi() < range.first.midi())
//                range.first = pitch;
//            if (pitch.midi() > range.second.midi())
//                range.second = pitch;
//        }
//    }
//   return range;
//   }


//std::vector<Tie*> Voice::getTies() const
//{
//    return _ties;
//}
//
//std::vector<Tuplet*> Voice::getTuplets() const
//{
//    return _tuplets;
//}
//
//std::vector<Beam*> Voice::getBeams() const
//{
//    return _beams;
//}


void Voice::accept(SMVisitor* v) const
{
    v->visitVoice(this);
}


} // end namespace ScoreModel


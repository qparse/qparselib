//
//  AtomicEvent.cpp
//  squanty
//
//  Created by Florent Jacquemard on 12/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "AtomicEvent.hpp"
#include "Voice.hpp"
#include "Part.hpp"
#include "LeafEvent.hpp"


namespace ScoreModel {


AtomicEvent::AtomicEvent(mtu_t start, mtu_t duration, Voice& voice):
Event(start, duration, voice.id()+name()+MTU::to_string(start)),
_voice(voice),
_engraving_main(NULL),
_engraving_cont()
{ }


AtomicEvent::AtomicEvent(const LeafEvent* leaf, Voice& voice):
Event(leaf->start(), leaf->duration(),
      voice.id()+name()+MTU::to_string(leaf->start())),
_voice(voice),
_engraving_main(leaf),
_engraving_cont()
{
    assert(leaf);
}



/// do not delete backlinks (engraving elements)
AtomicEvent::~AtomicEvent()
{ }


//void AtomicEvent::setVoice(Voice* voice)
//{
//    assert(voice);
//    assert(_voice == NULL); // can only be set once
//    _voice = voice;
//};


Part& AtomicEvent::part() const
{
    return _voice.part();
}


Score& AtomicEvent::score() const
{
    return part().score();
}


void AtomicEvent::print(std::ostream& o) const
{
    printOnset(o);
    o << ":";
    o << "AtomicEvent";
    o << "dur=" << _duration;
}


std::ostream& operator<<(std::ostream& o, const AtomicEvent& e)
{
    e.print(o);
    return o;
}


}

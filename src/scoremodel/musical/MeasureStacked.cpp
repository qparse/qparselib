//
//  MeasureStacked.cpp
//  squanty
//
//  Created by Florent Jacquemard on 04/04/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "MeasureStacked.hpp"
#include "MeasureStack.hpp"
#include "Score.hpp"
#include "MeterSig.hpp"
#include "KeySig.hpp"
#include "Local.hpp"

namespace ScoreModel {


class Node;


MeasureStacked::MeasureStacked(Part& p,
                               MeasureStack& parent,
                               const MeterSig* ts,
                               const KeySig* ks,
                               int staffmin, int staffmax):
Measure(p, parent.nb(),ts, ks, staffmin, staffmax),
_container(parent),
_prev((parent.nb() == 0)?NULL:&(parent.previous()->get(p)))
{
    // @param nb measure number (MTU date of measure start).
    assert((_prev != NULL) || (date() == MTU::ZERO));
    assert(_ts);
    assert(_ks);
#ifdef SM_MEASURE_STATIC
    _content.resize(SM_MAX_SEQUENCE, Sequence(0, NULL, *this));
#endif
}


MeasureStacked::~MeasureStacked()
{
    TRACE("delete MeasureStacked {} part:{}", nb(), part().id());

    if (_newTS)
    {
        assert(_ts);
        delete _ts;
    }
    if (_newKS)
    {
        assert(_ts);
        delete _ks;
    }
    
#ifndef SM_MEASURE_STATIC
    for (Sequence* s : _content)
    {
        assert(s);
        delete s;
    }
#endif
    _content.clear();  // useless? vectors are deleted anyway
    
    for (const Local* link : _backlinks)
    {
        assert(link);
        delete link;
    }
}


Score& MeasureStacked::score() const
{
    return stack().score();
}


/**
 * Navigation in the Measure Matrix
 */


Measure& MeasureStacked::next() const
{
    size_t n = nb() + 1;
    return score().stack(n).ofPart(_part);
}


bool MeasureStacked::last() const
{
    return (nb() == score().nbMeasures() - 1);
}


Measure& MeasureStacked::previous() const
{
    assert(_prev);
    return *(_prev);
}


Measure& MeasureStacked::goPart(Part& p) const
{
    return _container.ofPart(p);
}





const MeterSig* MeasureStacked::defaultMeterSig(const MeterSig* ts) const
{
    if (ts != NULL)
        return ts;
    else if (stack().nb() == 0)
        return new MeterSig(score().meterSignature()); // copy
    else
    {
        assert(_prev);
        return _prev->meterSignature();
    }
}


const KeySig* MeasureStacked::defaultKeySig(const KeySig* ks) const
{
    if (ks != NULL)
        return ks;
    else if (stack().nb() == 0)
        return new KeySig(score().keySignature());
    else
    {
        assert(_prev);
        return _prev->keySignature();
    }
}



//void MeasureStacked::setClef(const Clef& clef)
//{
//    _clef = clef;
//}

//bool MeasureStacked::changeClef() const
//{
//    if (nb() == 0)
//    {
//        return true;
//    }
//    else
//    {
//        assert(previous());
//        return (this->clef() == previous()->clef());
//    }
//}


//void MeasureStacked::setMeterSig(const MeterSig& ts)
//{
//    _ts = ts;
//}


//void MeasureStacked::setKeySig(const KeySig& key)
//{
//    _key = key;
//}




void MeasureStacked::accept(SMVisitor* v) const
{
    v->visitMeasureStacked(this);
}



void MeasureStacked::print(std::ostream& o) const
{
    size_t n = _content.size();
    if (n == 0)
        o << "MeasureStacked: empty";
    else if (n == 1)
        o << "MeasureStacked: ";
    else   // (n > 1)
        o << "MeasureStacked (" << n << " voices):";

#ifdef SM_MEASURE_STATIC
    for (Sequence s : _content)
        o << s;
#else
    for (Sequence* s : _content)
    {
        assert(s);
        o << s;
    }
#endif
}


std::ostream& operator<<(std::ostream& o, const MeasureStacked& m)
{
    m.print(o);
    return o;
}


} // end namespace ScoreModel

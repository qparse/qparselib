
# musical content of Score Model

DRAFT v.0


```mermaid
classDiagram
   Event <|-- AtomicEvent
   Event: string id
   Event: mtu onset
   Event: mtu duration
   AtomicEvent <|-- Note
   AtomicEvent <|-- Rest
   AtomicEvent <|-- Chord
   AtomicEvent <|-- Trill
   AtomicEvent: Voice voice
   AtomicEvent: LeafEvent* engraving
   Note <|-- GraceNote
   Note: Pitch pitch
   Chord: Pitch-set pitchs
   Chord: add(Pitch)
   Chord: pitch(int) Pitch
   Chord: size() int
   Trill: Pitch low_pitch
   Trill: Pitch high_pitch
   Event <|-- Voice
   Voice: int nb
   Voice: Part* part
   Voice: AtomicEvent*-seq events
   Voice: add(AtomicEvent*)
   Voice -->"vec" AtomicEvent : contains
   Event <|-- Measure
   Measure: int n
   Measure: TimeSig meter
   Measure: string-MeasurePart-map mp
   Measure: Score* parent
   Measure -->"map()" MeasurePart : contains
   MeasurePart: MeasureVoice-vec mv
   MeasurePart: Measure* parent
   MeasurePart -->"vec" MeasureVoice : contains
   MeasureVoice: int voicenb
   MeasureVoice: Node* engraving
   MeasureVoice: MeasurePart* parent
   class Part
   Part -->"vec" Voice : contains
   Part: string name
   class Score
   Score: string name
   Score -->"vec" Part : contains
   Score -->"vec" Measure : contains
   Score: Part-vec parts
   Score: Measure-vec measures
```
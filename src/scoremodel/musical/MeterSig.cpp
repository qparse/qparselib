// Implementation of ScoreMeter functions

#include <limits.h>

#include "MeterSig.hpp"
#include "util.hpp"


namespace ScoreModel {

//Duration ScoreMeter::getMeasureDuration() const {
//	return Duration (Rational (_ts_count, _ts_unit));
//}


MeterSig::MeterSig():
_ts_count(0),
_ts_unit(0)
{
    assert(this->undef());
}


MeterSig::MeterSig(int ts_count, int ts_unit):
_ts_count(ts_count),
_ts_unit(ts_unit)
{
    assert(ts_count > 0);
    assert(ts_unit > 0);
}


MeterSig::MeterSig(const MeterSig& ts):
_ts_count(ts._ts_count),
_ts_unit(ts._ts_unit)
{ }


MeterSig& MeterSig::operator=(const MeterSig& rhs)
{
    if(this != &rhs)
    {
        _ts_count = rhs._ts_count;
        _ts_unit = rhs._ts_unit;
    };
    return *this;
}


bool MeterSig::operator==(const MeterSig& rhs) const
{
    return ((this->_ts_count == rhs._ts_count) &&
            (this->_ts_unit == rhs._ts_unit));
}


Rational MeterSig::MTUtoWhole(mtu_t d) const
{
    assert(! undef());
    assert(d > 0);
    assert(_ts_count > 0); /// _ts_count > 1?
    assert(_ts_unit > 0);
    return MTU::to_Rational(d) * Rational(_ts_count, _ts_unit);
}


/// @todo check whether the note is dotted or not
cmnd_t MeterSig::MTUtoCMN(mtu_t d) const
{
    assert(_ts_count > 0);
    assert(_ts_unit > 0);
    assert(d >= MTU::ZERO);
    if (d == MTU::ZERO)
    {
        ERROR("TS.cast to CMN: null duration");
        return CMN::UNDEF;
    }

    // convert duration from number of bars to number of whole notes
    Rational dw = MTUtoWhole(d);
    
    // longer than or equal to 1 whole note
    if (dw >= Rational(16))
    {
        ERROR("TS.cast to CMN: duration {} too long for CMN!", d);
        return CMN::UNDEF;
    }
    else if ((Rational(8) <= dw) && (dw < Rational(16)))
    {
        return CMN::MAXIMA;
        // long tdw  = trunc(dw);
        // assert(tdw > 0);
        // return - util::trunclog2(tdw);
    }
    else if ((Rational(4) <= dw) && (dw < Rational(8)))
    {
        return CMN::LONG;
    }
    else if ((Rational(2) <= dw) && (dw < Rational(4)))
    {
        return CMN::BREVE;
    }
    else if ((Rational(1) <= dw) && (dw < Rational(2)))
    {
        return CMN::WHOLE;
    }
    assert(Rational(0) < dw);
    assert(dw < Rational(1));
    // smaller than 1 whole note
    // EG: 3/4 = 3  quarters, 1/4 = 1 quarter, 2/8 = 2 eigth = 1 quarter, etc.
    //long idw = dw.denominator() /  dw.numerator();
    //assert(idw >= 1);

    Rational idw = dw.invert();

    unsigned int m1 = 1;
    unsigned int m2 = 2;
    while (m2 > 1) // more than 1 bit remaining
    {
        if ((m1 < idw) && (idw <= m2))
            return m2;
        m1 = m2;
        m2 *= 2;
    }
    ERROR("bad int {}", idw);
    return 1; // not found
    //return util::trunclog2(idw);
}


bool MeterSig::compound() const
{
    return ((_ts_count % 3 == 0) &&
            (_ts_count != 3) &&
            (_ts_count != 0));
}


bool MeterSig::simple() const
{
    return ((! compound()) && (! undef()));
}


bool MeterSig::undef() const
{
    return (_ts_count == 0);
}


int MeterSig::beats() const
{
    if (undef())
        return 0;
    else if (simple())
        return _ts_count;
    else
    {
        assert(compound());
        assert(_ts_count % 3 == 0);
        return _ts_count / 3;
    }
}


void MeterSig::accept(SMVisitor* v) const
{
    v->visitMeterSig(this);
}


void MeterSig::print(std::ostream& o) const
{
    if (_ts_count != 0)
        o << _ts_count << "/" << _ts_unit;
    else
        o << "NaN";
}


std::ostream& operator<<(std::ostream& o, const MeterSig& ts)
{
    ts.print(o);
    return o;
}


}

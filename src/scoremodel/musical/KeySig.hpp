//
//  KeySig.hpp
//  qparse
//
//  Created by Florent Jacquemard on 02/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#ifndef KeySig_hpp
#define KeySig_hpp

#include <stdio.h>
#include "trace.hpp"

#include "SMVisitor.hpp"


namespace ScoreModel {


/// @brief Representation of the key* signature.
/// We assume the following key signatures:
/// | major | minor | key sig |
/// |:-----:|:-----:|:-------:|
/// |  Cb   |  Ab   |   -7    |
/// |  Gb   |  Eb   |   -6    |
/// |  Db   |  Bb   |   -5    |
/// |  Ab   |  F    |   -4    |
/// |  Eb   |  C    |   -3    |
/// |  Bb   |  G    |   -2    |
/// |  F    |  D    |   -1    |
/// |  C    |  A    |    0    |
/// |  G    |  E    |    1    |
/// |  D    |  B    |    2    |
/// |  A    |  F#   |    3    |
/// |  E    |  C#   |    4    |
/// |  B    |  G#   |    5    |
/// |  F#   |  D#   |    6    |
/// |  C#   |  A#   |    7    |
class KeySig
{

public:

    KeySig(int signature);

    KeySig(const KeySig& ks);

    KeySig& operator=(const KeySig& rhs);

    bool operator==(const KeySig& rhs) const;

    /// @return the number of fifths distance
    /// from a signature with no accidentals.
    inline int fifths() const { return _sig; };
    
    /// number of sharp symbols to denote this key signature.
    size_t sharps() const;

    /// number of flat symbols to denote this key signature.
    size_t flats() const;

    virtual void accept(SMVisitor*) const;

    void print(std::ostream& o) const;
        
private:
    
    /// the number of fifths distance
    /// from a signature with no accidentals.
    int _sig;
    
};

std::ostream& operator<<(std::ostream&, const KeySig&);

} // end namespace ScoreModel
    
#endif /* KeySig_hpp */

/// @} // end group scoremodel

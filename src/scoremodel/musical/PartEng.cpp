//
//  PartEng.cpp
//  squant2
//
//  Created by Florent Jacquemard on 11/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "PartEng.hpp"


namespace ScoreModel {


PartEng::PartEng(Part& part):
_part(part),
_content(),
_staves(0)
{
    
}


PartEng::~PartEng()
{
    
}


void PartEng::addStaff(int n)
{
    assert(n >= 0);
    assert(_staves > 0);
    if (n >= _staves)
    {
        _staves = n - 1;
        //TRACE("Part {}: new staff {}", id(), n);
    }
}






} // end namespace ScoreModel



// Implementation of Score functions

#include "Score.hpp"



namespace ScoreModel {


// Main constructor
Score::Score(const std::string name,
             // const Clef& clef,
             const MeterSig& ts,
             const KeySig& ks):
_id((name.empty())?std::string("score"):name),
_ts(ts),
_ks(ks),
_parts(),   // empty
_measures() // empty
{
#ifdef SM_MEASURE_STATIC
    _parts.resize(SM_MAX_PART, Part(*this, std::string()));
    /// @todo check if number is reassigned
    _measures.resize(SM_MAX_MEASURE, MeasureStack(0, *this));
#endif
}


// Destructor
Score::~Score()
{
    TRACE("delete Score {} ({} measure stacks)", _id, _measures.size());
    //    assert(_ts);
    //    delete _ts;
    //    assert(_ks);
    //    delete _ks;
    //  @todo TBR: useless: vectors are deleted

#ifndef SM_MEASURE_STATIC
    for (auto m : _measures)
        delete m;
    for (auto p : _parts)
        delete p;
#endif
    //MeasureStackList().swap(_measures); // clear & reallocate
    _measures.clear();
    //PartSet().swap(_parts); // clear & reallocate
    _parts.clear();
}


const std::string& Score::id() const
{
    return _id;
}


Part& Score::part(const std::string& name)
{
    assert(! name.empty());
#ifdef SM_MEASURE_STATIC
    // check if part exists with same name
    for (PartSet::iterator i = _parts.begin(); i != _parts.end(); i++)
    {
        if (i->id() == name) // found
            return *i;
    }

    // create a new part
    _parts.emplace_back(*this, name);
    Part& part = _parts.back();
    TRACE("Score {}: new part {}", _id, part.id());
    assert(part.id() == name);
    // add the new part in every existing measure stack
    // for (MeasureStack m : _measures)
    // {
    // @todo copy meterSig and KeySig of other parts?
    //        m.newPart(part);
    // }
    return part;
#else
    // search part of given name in existing parts
    for (Part* part : _parts)
    {
        if (part->id() == name) // found
            return *part;
    }

    // create a new part
    Part* part = new Part(*this, name);
    _parts.push_back(part);
    assert(part->id() == name);
    TRACE("Score {}: new part {}", _id, part->id());
    // add the new part in every existing measure stack
    // for (MeasureStack* m : _measures)
    // {
    // @todo copy timeSig and KeySig of other parts?
    //    m->newPart(*part);
    // }
    return *part;
#endif
}


const Part& Score::getPart(const std::string& name) const
{
#ifdef SM_MEASURE_STATIC
    // check if part exists with same name
    for (PartSet::iterator i = _parts.begin(); i != _parts.end(); i++)
    {
        if (i->id() == name) // found
            return *i;
    }
#else
    for (Part* part : _parts)
    {
        if (part->id() == name) // found
            return *part;
    }
#endif
    // failure
    ERROR("Score: part {} not found", name); // assert(false);
    // return an empty part.
    Part* part = new Part(*((Score*) this), name);
    return *part;
}


// replaced by emplace_back
//const std::string Score::addPart(Part* const p)
//{
//    assert(p);
//    _parts.push_back(*p);
//    return p->getName();
//}

Part& Score::addPart(const std::string& name)
{
    const std::string rename =
    name.empty()?(std::string("part")+std::to_string(_parts.size())):name;

    return part(rename);
}


size_t Score::nbParts() const
{
    return _parts.size();
}


/// for backward compatibility
Voice& Score::voice(const std::string name, int id)
{
    Part&  part = this->part(name);
    return part.voice(id);
}


MeasureStack& Score::stack(size_t i)
{
    size_t n = _measures.size();

    // otherwise create all missing stacks (if i >= n)
    for (size_t j = n; j <= i; ++j)
    {
        //_measures.emplace_back(j, _ts);
        addbackStack();
    }
    assert(i < _measures.size());
#ifdef SM_MEASURE_STATIC
    return _measures[i];
#else
    return *(_measures[i]);
#endif
}


#ifdef SM_MEASURE_STATIC
MeasureStack& Score::addbackStack(bool fill)
{
    // Increment id
    size_t n = _measures.size();
    // create a new empty measure stack
//  MeasureStackList::iterator is =
//      _measures.emplace(_measures.end(), n, *this); // emplace_back _measures.end()
    _measures.emplace_back(n, *this);
    assert(_measures.size() == n+1);
    MeasureStack& s =  _measures.back(); // *is;
    assert(s.nb() == n);
    assert(s.empty());
    // create an new Measure for each part
    if (fill)
        for (Part& p : _parts)
            s.newPart(p);
    return s;
}
#else
MeasureStack& Score::addbackStack(bool fill)
{
    // Increment id
    size_t n = _measures.size();
    // create and add a new empty measure stack
    MeasureStack* ms = new MeasureStack(n, *this);
    assert(ms->nb() == n);
    assert(ms->empty());
    _measures.push_back(ms);
    assert(_measures.size() == n+1);
    //assert(_measures.back() == ms);
    // create an new Measure for each part
    if (fill)
        for (Part* p : _parts)
            ms->newPart(*p);
    return *ms;
}
#endif

Measure& Score::newMeasure(Part& p, const MeterSig* ts, const KeySig* ks)
{
    MeasureStack& s = addbackStack(false);
    Measure& m = s.newPart(p, ts, ks);
    return m;
}


size_t Score::nbMeasures() const
{
    return _measures.size();
}


//void Score::setClef(const Clef& clef)
//{
//    _clef = clef;
//}



void Score::setMeterSignature(const MeterSig& ts)
{
    //assert(ts);
    //if (_ts) delete _ts;
    _ts = ts;
}


void Score::setKeySignature(const KeySig& ks)
{
    //assert(ks);
    //if (_ks) delete _ks;
    _ks = ks;
}


void Score::accept(SMVisitor* v) const
{
    v->visitScore(this);
}

}

//
//  GraceChord.hpp
//  squant2
//
//  Created by Florent Jacquemard on 20/01/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#ifndef GraceChord_hpp
#define GraceChord_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>

#include "Pitch.hpp"
#include "Note.hpp"
#include "Chord.hpp"
#include "AEVisitor.hpp"

namespace ScoreModel {

/// a grace-chord is a chord with a null MTU duration.
class GraceChord : public Chord
{
public:

    GraceChord(mtu_t start, Voice& voice);

    virtual ~GraceChord() {}
    
    virtual std::string name() const { return std::string("_gchord_"); }

    virtual bool isGraceChord() const { return true; }

    virtual void accept(SMVisitor*) const;
    virtual void accept(AEVisitor*);

    virtual void print(std::ostream& o) const;

};

std::ostream& operator<<(std::ostream&, const GraceChord&);


} // end namespace ScoreModel

/// @}

#endif /* GraceChord_hpp */

//
//  GraceNote.cpp
//  qparse
//
//  Created by Florent Jacquemard on 18/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "GraceNote.hpp"

namespace ScoreModel {

// pitched note
GraceNote::GraceNote(mtu_t start, Voice& voice, const Pitch& p):
Note(start, MTU::ZERO, voice, p)
{    }


void GraceNote::accept(SMVisitor* v) const
{
    v->visitGraceNote(this);
}


void GraceNote::accept(AEVisitor* v) 
{
    v->visitGraceNote(this);
}


void GraceNote::print(std::ostream& o) const
{
    printOnset(o);
    o << ": ";
    o << "GraceNote " << _pitch;
}


std::ostream& operator<<(std::ostream& o, const GraceNote& e)
{
    e.print(o);
    return o;
}


}

//
//  Clef.cpp
//  qparse
//
//  Created by Florent Jacquemard on 02/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Clef.hpp"


namespace ScoreModel {


Clef::Clef(Kind kind, int octave):
_kind(kind),
_oct(octave)
{ }



// Choose the clef from the range opf the voice
//std::pair<std::string, int> MEI::chooseClef(std::pair<Pitch, Pitch> range)

Clef::Clef(const Pitch& lowest_pitch, const Pitch& highest_pitch):
_kind(Kind::GTreble), // default
_oct(0)
{
    if (highest_pitch.midi() < 65)
        _kind = Kind::FBass; // line == 4;
    else if (lowest_pitch.midi() >= 57)
        _kind = Kind::GTreble; // line == 2;
    else if (highest_pitch.midi() < 74)
        _kind = Kind::CAlto; // line == 3;
}


Clef& Clef::operator=(const Clef& rhs)
{
    if(this != &rhs)
    {
        _kind = rhs._kind;
        _oct = rhs._oct;
    }
    
    return *this;
}


bool Clef::operator==(const Clef& rhs) const
{
    return ((this->_kind == rhs._kind) &&
            (this->_oct == rhs._oct));
}


char Clef::sign() const
{
    switch (_kind)
    {
        case Clef::None:
            return 0;

        case Clef::GTreble:
        case Clef::GSoprano:
            return 'G';

        case Clef::FBass:
        case Clef::FBaritone:
        case Clef::FSubBass:
            return 'F';

        case Clef::CAlto:
        case Clef::CTenor:
        case Clef::CSoprano:
        case Clef::CMezzoSoprano:
        case Clef::CBaritone:
            return 'C';

        case Clef::Percussion:
            return 'N';
            
        default:
        {
            ERROR("unknown clef kind {}", _kind);
            return 0;
        }
    }
}


int Clef::line() const
{
    switch (_kind)
    {
        case Clef::None:
        case Clef::Percussion:
            return 0;

        case Clef::GSoprano:
        case Clef::CSoprano:
            return 1;

        case Clef::GTreble:
        case Clef::CMezzoSoprano:
            return 2;

        case Clef::CAlto:
        case Clef::FBaritone:
            return 3;
            
        case Clef::FBass:
        case Clef::CTenor:
            return 4;
            
        case Clef::FSubBass:
        case Clef::CBaritone:
            return 5;
            
        default:
        {
            ERROR("unknown clef kind {}", _kind);
            return 0;
        }
    }

}


int Clef::octave() const
{
    return _oct;
}


void Clef::accept(SMVisitor* v) const
{
    v->visitClef(this);
}


void Clef::print(std::ostream& o) const
{
    o << sign() << line();
    if (_oct != 0)
        o << "+" << _oct;
}


std::ostream& operator<<(std::ostream& o, const Clef& c)
{
    c.print(o);
    return o;
}


} // end namespace ScoreModel

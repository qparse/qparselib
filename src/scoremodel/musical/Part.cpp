// Implementation of Part functions

#include "Part.hpp"
#include "Score.hpp"
#include "AtomicEvent.hpp"


namespace ScoreModel {

// Main constructor
Part::Part(Score& score, const std::string& name):
_id(name),
_container(score),
_ambitus(),
_voices(), // empty
_measures(),
_staffdefs()
{
    assert(! name.empty());
}


Part::~Part()
{
    TRACE("delete Part {} in score:{}", _id, score().id());
#ifndef SM_MEASURE_STATIC
    for (std::pair<int, Voice*> v : _voices)
    {
        assert(v.second);
        delete v.second;
    }
#endif
    ///  @todo TBR: useless: vectors are deleted
    _voices.clear();
    
    for (StaffDef* s : _staffdefs)
    {
        assert(s);
        delete s;
    }
    ///  @todo TBR: useless: vectors are deleted
    _staffdefs.clear();
}


void Part::accept(SMVisitor* v) const
{
    v->visitPart(this);
}


void Part::print(std::ostream& o) const
{
    /// @todo complete
}


/**
 *
 *    Musical stuff: Voices
 *
 */


size_t Part::nbVoices() const
{
    return _voices.size();
}


//Voice* Part::voice(const std::string voiceName)
//{
//    assert(! voiceName.empty());
//    for (Voice& voice : _voices)
//    {
//		if (voice.id() == voiceName)
//        {
//			return &voice;
//		}
//	}
//	//Gloup //throw std::string("UNKNOWN VOICE ");
//    ERROR("Voice {} not found in score {}", voiceName, _id);
//    return NULL;
//}


#ifdef SM_MEASURE_STATIC
Voice& Part::voice(int i)
{
    std::pair<Wozzeck::iterator, bool> res =
    _voices.emplace(std::piecewise_construct,
                    std::forward_as_tuple(i),
                    std::forward_as_tuple(*this, i));
//    _content.try_emplace(name, _nb, name, *this); // C++17

    if (res.second == true)
        TRACE("Part {}: new voice {}", _id, i);

    assert((res.first)->first == (res.first)->second.number());
    return (res.first)->second;
}
#else
Voice& Part::voice(int i)
{
    Wozzeck::iterator iv = _voices.find(i);
    if (iv != _voices.end()) // found
    {
        return *(iv->second);
    }
    
    // create, add and return a new voice
    Voice* v = new Voice(*this, i);
    std::pair<Wozzeck::iterator, bool> res =
        _voices.insert(std::pair<int, Voice*>(i, v));
    assert(res.second == true); // new voice was inserted
    TRACE("Part {}: new voice {}", _id, i);
    assert((res.first)->first == (res.first)->second->number());
    assert(v == (res.first)->second);
    return *v;
}
#endif


Voice& Part::newVoice(int i)
{
    return voice(i);
}


//void Part::addVoice(Voice& v)
//{
//	_voices.emplace_back(v);
//}


//Voice* Part::newVoice(mtu_t start, const std::string name)
//{
//    const std::string rename =
//        name.empty()?(_id+std::to_string(_voices.size())):name; // C++11
//
////    part.name()+std::to_string(part.size())):name
//    _voices.emplace_back(start, *this, rename);
//    assert(! _voices.empty());
//    assert(! _voices.back().id().empty());
//    TRACE("Part {}: new voice {}", _id, _voices.back().id());
//
//    return &(_voices.back());
//}



/**
 *
 *    Engraving stuff: Measures
 *
 */


size_t Part::nbMeasures() const
{
    return _measures.size();
}


Measure& Part::measure(size_t n, const MeterSig* ts, const KeySig* ks)
{
    return addMeasure(n, false, false, ts, ks);
}


Measure& Part::newMeasure(const MeterSig* ts, const KeySig* ks)
{
    /// @tidi revise with push_back
    size_t n = _measures.size();
    return addMeasure(n, true, false, ts, ks);
}



const Measure& Part::getMeasure(size_t n) const
{
    assert(n < _measures.size());
    Measure* m = _measures.at(n);
    assert(m);
    return *m;
}


/// @todo add one or several StaffDef ?
Measure& Part::addMeasure(size_t n,
                          bool fnew, bool fnotnew,
                          const MeterSig* ts, const KeySig* ks)
{
    assert(! (fnew && fnotnew)); // flags mutually exclusive
    assert((! fnotnew) || ((ts == NULL) && (ks == NULL)));
    
    // there is already a measure n in this part
    if (n < _measures.size()) // measure n already exists
    {
        assert(! fnew);
        if (! fnotnew) //
            WARN("addMeasure: part:{} already has a measure {},\
                  cannot change TS and KS", id(), n);
#ifdef SM_MEASURE_STATIC
        Measure& m = _measures[j];
        return m;
#else
        Measure* m = _measures.at(n);
        assert(m);
        return *m;
#endif
    }
    // new measure.s is.are created in this part
    else
    {
        assert(! fnotnew);
        for (int i = (int) _measures.size(); i <= n; ++i)
        {
            TRACE("Part {}: new measure {}", id(), i);
#ifdef SM_MEASURE_STATIC
            _measures.emplace_back(*this, i, ts, ks); // new measure
#else
            Measure* m = new Measure(*this, i, ts, ks);
            _measures.push_back(m);
#endif
        }
        assert(_measures.size() == n+1);

#ifdef SM_MEASURE_STATIC
        return _measures[n];  // == _measures.back();
#else
        return *(_measures.back());  // return measure(n);
#endif
    }
}
        

/**
 *
 *    Engraving stuff: Staves
 *
 */


size_t Part::nbStaves() const
{
    return _staffdefs.size();
}


StaffDef& Part::staff(int n, const Clef& clef)
{
    assert(n >= 0);
    if (n < _staffdefs.size())
    {
        StaffDef* s = _staffdefs.at(n);
        assert(s);
        return *s;
    }
    else
    {
        for (int i = (int) _staffdefs.size(); i <= n; ++i)
        {
            TRACE("Part {}: new staff {}", id(), i);
            StaffDef* s = new StaffDef(*this, i, clef);
            _staffdefs.push_back(s);
        }
        return *(_staffdefs.back());  // return staff(n);
    }
}


void Part::addStaff(int n, const Clef& clef)
{
    assert(n >= 0);
    if (n < _staffdefs.size())
        WARN("Part {} : staff {} already exists", id(), n);
    else
        staff(n, clef);
}


// add element
void Part::add(int i, int n, AtomicEvent* e)
{
    assert(e);
    _ambitus.add(e);
    Voice& v = voice(i); // create if it does not exist
    v.add(e);
    StaffDef& s = staff(n); // create if it does not exist
    s.add(e);
}


const StaffDef& Part::getStaff(int n) const
{
    assert(0 <= n);
    assert(n < _staffdefs.size());
    StaffDef* s = _staffdefs[n];
    assert(s);
    return *s;
}


Part::Flattener::Flattener(Part& part):
_part(part),
_auxbag()
{ }


Part::Flattener::~Flattener()
{
    _auxbag.clear(); // the pointed notes are not deleted
}


void Part::Flattener::add(const AtomicEvent* e)
{
    assert(e);
    // dispatch the subtypes of e
    e->accept(this);
}


void Part::Flattener::visitNote(const Note* n)
{
    assert(n);
    // there might be several insert of the same note (voice crossing)
    _auxbag.insert(n);
}


void Part::Flattener::visitGraceNote(const GraceNote* n)
{
    assert(n);
    // there might be several insert of the same note (voice crossing)
    _auxbag.insert((Note*) n);
}


void Part::Flattener::visitRest(const Rest* r)
{
    // do nothing
}


void Part::Flattener::visitChord(const Chord* c)
{
    assert(c);
    for (NoteBag::const_iterator i = c->cbegin(); i != c->cend(); ++i)
    {
        const Note* n = *i;
        _auxbag.insert(n);
    }
}


void Part::Flattener::visitGraceChord(const GraceChord* c)
{
    assert(c);
    for (NoteBag::const_iterator i = c->cbegin(); i != c->cend(); ++i)
    {
        const Note* n = *i;
        _auxbag.insert(n);
    }
}


void Part::Flattener::visitTrill(const Trill* o)
{
    assert(o);
    /// @todo also insert the second note of the trill
    _auxbag.insert((Note*) o);
}


Voice* Part::Flattener::getvoice(int id) const
{
    assert(id >= 0);
    Voice* v = new Voice(_part, id);
    for (auto i = _auxbag.begin(); i != _auxbag.end(); ++i)
    {
        const Note* n = *i;
        assert(n);
        v->add((AtomicEvent*) n);
    }
    return v;
}


Voice* Part::flattening(int id)
{
    Flattener flat = Flattener(*this);
    for (Wozzeck::iterator iv = _voices.begin(); iv != _voices.end(); ++iv)
    {
#ifdef SM_MEASURE_STATIC
        Voice& v = iv->second;
#else
        assert(iv->second);
        Voice& v = *(iv->second);
#endif
        for (EventSeq::const_iterator ie = v.cbegin(); ie != v.cend(); ++ie)
        {
            assert(*ie); // AtomicEvent*
            flat.add(*ie);
        }
    }
    return flat.getvoice(id);
}


} // end namespace ScoreModel

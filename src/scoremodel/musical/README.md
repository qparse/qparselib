# `src/scoremodel/musical`

The first part of a score model represents the musical content of the score. This content is organized hierarchicaly into parts, one part for one instrument or group of related instruments). 
Every part contains one or several monophonic voices, and a monophonic voice is a sequence of atomic events (notes, chords and rests) without overlaps.

Moreover, the score is cut into a sequence of measures. 
Every  measure has a fixed duration of 1 MTU, an associated  specific time signature 
and pointers to engraving info on the voice contents in the same time window.



---
The following diagram of the classes of the model of musical content describe
- the inheritance relationships (triangle arrows),
- the content relationships (arrows with a label `*` or `1`).

Note the pointers to elements of the  engraving model:
- every `AtomicEvent` contains a pointer to a `LeafEvent` = `Leaf` `Node`  in a Rhythm Tree, marking  the beginning of a sequence of leaves (notational events including notes, ties, dots *etc*) making this atomic event.
- every monophonic  `Sequence`   in a measure (segment of `Voice`) contains a pointer to a `Root` `Node` = a Rhythm Tree.
- every `Measure` contains a set of `Local`s = annotations in the score (dynamics, articulations *etc*) , also called Directives in [MEI](https://music-encoding.org/guidelines/v4/elements/dir.html) and Directions in MusicXML.

Rem1: on time values: The onset and durations in `Event` are in Musical Time Unit = fraction of measure. Every measure number  (in `MeasureStack`, `Measure`, `Sequence`) is exactly the onset time (in MTU) of the start of the measure. In the diagram, the onset and durations are  members of the event, but they could also be computed, following on of the 2 above links, using the engraving model (the Rhythm Tree in a `Sequence` defines durations).


Rem2: Some NoteEvents do not belong to a Voice: the notes in a Chord - but the chord belongs to a Voice.




```mermaid
classDiagram
   class Event
   Event <|-- AtomicEvent
   Event: string id
   Event: MTU onset
   Event: MTU duration
   Event <|-- Measure
   Event <|-- Voice

   class AtomicEvent
   AtomicEvent: voice() Voice
   AtomicEvent: part() Part
   AtomicEvent: score() Score
   AtomicEvent <|-- Note
   AtomicEvent <|-- Chord
   AtomicEvent <|-- Rest
   AtomicEvent -->"1" LeafEvent
   AtomicEvent -->"*" Continuation

   class LeafEvent
   <<engraving>> LeafEvent

   class Continuation
   <<engraving>> Continuation

   class Note
   Note: Pitch pitch
   Note <|-- GraceNote
   Note <|-- Trill

   class GraceNote
   GraceNote: (duration = 0)   

   class Trill
   Trill: Pitch pitch2

   class Chord
   Chord: int size()
   Chord <|-- GraceChord
   Chord -->"*" Note
   
   class GraceChord
   GraceChord: (duration = 0)   
   
   class Voice
   Voice: int id
   Voice: part() Part
   Voice: score() Score
   Voice: add(AtomicEvent*)
   Voice -->"*" AtomicEvent

   class Sequence
   Sequence: int voice-nb
   Sequence: measure() int 
   Sequence: parent() Measure
   Sequence -->"1" Root
   
   class Measure
   Measure: int nb
   Measure: MeterSig meter
   Measure: KeySig key
   Measure: StaffRange staves
   Measure: part() Part
   Measure: score() Score
   Measure: next() Measure
   Measure: previous() Measure  
   Measure: addSequence(id)
   Measure -->"*" Sequence
   Measure -->"*" Local
  
   class Root
   <<engraving>> Root

   class Local
   <<engraving>> Local

   class Part
   Part: string name
   Part: StaffRange staves
   Part: score() Score
   Part: newVoice(id)
   Part: measure(id)
   Part -->"*" Voice
   Part -->"*" Measure
   Part -->"*" StaffDef
       
   class StaffDef{
   <<engraving>>   
   StaffDef: int nb
   StaffDef: Clef clef
   StaffDef: KeySig key
   StaffDef: MeterSig ts
   StaffDef: Ambitus ambitus
   highest() Pitch
   lowest() Pitch
   add(AtomicEvent)
}

   class Score
   Score -->"*" Part
   Score -->"*" MeasureStack
   Score: string name
   Score: MeterSig default
   Score: KeySig default
   Score: addPart(name)
   
   class MeasureStack
   <<deprecated>> MeasureStack
 
```

//
//  PartEng.hpp
//  squant2
//
//  Created by Florent Jacquemard on 11/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef PartEng_hpp
#define PartEng_hpp

#include <stdio.h>
#include <string>
#include <map>

#include "MTU.hpp"
#include "Part.hpp"
#include "StaffDef.hpp"
#include "SMVisitor.hpp"

namespace ScoreModel {


typedef std::vector<Measure*> MeasureSeq;


/// Part Engraving : engraving content associated to a Part.
/// This class acts as an interface between the score and the engraving models.
class PartEng
{
public:
    /// Empty part
    PartEng(Part& part);
    
    virtual ~PartEng();

    /// The part this engraving content belongs to
    inline Part& part() { return _part; }
    
    /// number of staves for the engraving of all the measures of this Part.
    inline size_t nbStaves() const { return _staves; }
    
    /// Increase the number of staves for this part, if necessary,
    /// in order to be able to display a staff of given number.
    /// @param nb a staff number. must be positive or null.
    void addStaff(int nb);

    
    //Staff& staff(int n);
    
private:

    /// The part this engraving content belongs to
    Part& _part;

    /// engraving content
    MeasureSeq _content;
    
    /// number of staves for the engraving of all the measures of this Part.
    size_t _staves;
    
    ///  The order of staves reflects their order in the engraved score.
    
    /// measure associated to a part in stack.
    /// @param p associated part.
    /// @param fnew flag: the measure must be new in the stack
    /// (part not registered in this track).
    /// @param fnotnew flag: the measure must not be new in the stack
    /// (part registered in this stack).
    /// @param ts parameter time signature for the creation of a new measure.
    /// @param ks parameter key signature for the creation of a new measure.
    /// @return the measure associated to the given part in the stack,
    /// or, if there is none, a newly created empty measure.
    Measure& add(Part& p, bool fnew, bool fnotnew,
                 const MeterSig* ts, const KeySig* ks);
    
};


} // end namespace ScoreModel




#endif /* PartEng_hpp */

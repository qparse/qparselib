//
//  MeasureStacked.hpp
//  squanty
//
//  Created by Florent Jacquemard on 04/04/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef MeasureStacked_hpp
#define MeasureStacked_hpp
/// @addtogroup scoremodel
/// @{


#include <stdio.h>
#include <string>
#include <vector>

#include "Event.hpp"
#include "Part.hpp"
#include "Measure.hpp"
#include "Clef.hpp"
#include "MeterSig.hpp"
#include "KeySig.hpp"
#include "Sequence.hpp"
#include "SMVisitor.hpp"


namespace ScoreModel {

class Score;
class Root;
//class Sequence;
class MeasureStack;
class Local;

/// A stacked measure is a container with a fixed duration of 1 mtu,
/// containing engraving info on the content of the voices of some part in the corr. time window.
/// It is to be embedded in a MeasureStack.
class MeasureStacked : public Measure
{

public:
    
    /// empty measure to be embedded in a measure stack.
    /// @param p the part this measure belongs to.
    /// @param parent measure stack container.
    /// @param ts new time signature, taking effect at the beginning of this measure.
    ///        deallocated with measure.
    ///        if NULL, the time signature for this measure is the time signature of the previous one
    /// @param ks new key signature, taking effect at the beginning of this measure.
    /// if NULL, the key signature for this measure is the key signature of the previous one.
    ///        deallocated with measure.
    /// @param staffmin initial minimal staff number.
    /// @param staffmax initial maximal staff number.
    MeasureStacked(Part& p,
                   MeasureStack& parent,
                   const MeterSig* ts = NULL,
                   const KeySig* ks = NULL,
                   int staffmin = 0, int staffmax = 0);
    
    /// deallocate ts and ks if they change with this measure.
    virtual ~MeasureStacked();
    
    virtual bool isMeasureStacked() const { return true; }

    /// Get the measure stack containing this measure.
    inline MeasureStack& stack() const { return _container; }

    /// the score containing this measure.
    virtual Score& score() const;


    /**
     * Navigation in the Measure Matrix
     */

    /// horizontal move in the matrix of measures: next measure in the same part.
    /// If this measure is the last, then a new measure is created.
    virtual Measure& next() const;

    /// @todo the measure is the last in the part.
    virtual bool last() const;

    /// @return previous measure in part or this Measure it is the first measure in part.
    /// @warning this measure must not be the first in part (otherwise return this measure).
    virtual Measure& previous() const;
    // inline MeasureStacked* prevStacked() const { return _prev; }

    /// @todo vertical move in the matrix of measures: measure in given part and same time window.
    /// @see MeasureStack.ofPart and newPart
    Measure& goPart(Part&) const;

    /// @todo use the above in Builder

    
    
    /**
     * TS, KS and Clef
     */

    
    
    
    virtual void accept(SMVisitor*) const;

    virtual void print(std::ostream& o) const;
    
protected:
    
    MeasureStack& _container;
    
    /// previous Measure associated to the same part.
    MeasureStacked* _prev;
    
    /**
     * TS, KS and Clef
     */
    
    /// @todo TBR (useless?)
    virtual const MeterSig* defaultMeterSig(const MeterSig* ts=NULL) const;
    
    /// @todo TBR (useless?)
    virtual const KeySig* defaultKeySig(const KeySig* ks=NULL) const;


};


std::ostream& operator<<(std::ostream&, const MeasureStacked&);


} // end namespace ScoreModel

/// @}

#endif /* MeasureStacked_hpp */

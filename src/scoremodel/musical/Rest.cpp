// Implementation of Rest functions

#include "Rest.hpp"

namespace ScoreModel {

// pitched note
Rest::Rest(mtu_t start, mtu_t duration, Voice& voice):
AtomicEvent(start, duration, voice)
{ }

Event* Rest::clone()
{
    return new Rest(date(), duration(), voice());
}


void Rest::accept(SMVisitor* v) const
{
    v->visitRest(this);
}


void Rest::accept(AEVisitor* v)
{
    v->visitRest(this);
}


void Rest::print(std::ostream& o) const
{
    printOnset(o);
    o << ":";
    o << "Rest ";
    o << "dur=" << _duration;
}


std::ostream& operator<<(std::ostream& o, const Rest& e)
{
    e.print(o);
    return o;
}


}

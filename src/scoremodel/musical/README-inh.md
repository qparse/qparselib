# `src/scoremodel/musical`

Intermediate abstract model used to produce scores in various format.
Can be built from parsing results (*i.e.* from Runs).

In a score model, the music information is organized hierarchicaly in parts (one part for one instrument), 
every part contains one or several monophonic voices, 
and a monophonic voice is a sequence of atomic events (notes, chords and rests) without overlaps.

Moreover, the score is cut into a sequence of measures. 
Every  measure has a fixed duration of 1 MTU, 
an associated with a specific time signature 
and pointers to engraving info on the voice contents in the same time window.

### content:

```mermaid
graph TD;
  Score-->|vec| Part;
  Part-->|vec| Voice;
  Voice-->|vec| AtomicEvent;
  Score-->|vec| Measure;
  Measure-->Meter;
  Measure-->|ptr| Node;
```

### inheritance:

```mermaid
graph TD;
  Event-->AtomicEvent;
  AtomicEvent-->Note;
  AtomicEvent-->Rest;
  AtomicEvent-->Chord;
  AtomicEvent-->Trill;
  Note-->GraceNote;
  Event-->Voice;
```


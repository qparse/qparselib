// Implementation of Sequence functions

#include "Sequencm.hpp"

namespace ScoreModel {

// Main constructor
Sequencm::Sequencm()  {

}

// Add an event
void Sequencm::addEvent(Event* event) {
	_events.push_back(event);
}


std::vector<Event*> Sequencm::getEvents() const {
	return _events;
}

void Sequencm::concatenate(const Sequencm& seq)
{
	for (Event* event : seq.getEvents()) {
		_events.push_back(event);
	}
}

size_t Sequencm::nbEvents() const
{
    return _events.size();
}

/**
 * First event
 */
Event* Sequencm::getFirstEvent() const
{
    assert(_events.size() > 0);
	return _events.front();
}

/**
 * Last event
 */
Event* Sequencm::getLastEvent() const
{
    assert(_events.size() > 0);
	return _events.back();
}


// Destructor

Sequencm::~Sequencm() {

}
}

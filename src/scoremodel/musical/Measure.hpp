//
//  Measure.hpp
//  squant2
//
//  Created by Florent Jacquemard on 14/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef Measure_hpp
#define Measure_hpp
/// @addtogroup scoremodel
/// @{


#include <stdio.h>
#include <string>
#include <vector>

#include "Event.hpp"
#include "Part.hpp"
#include "Clef.hpp"
#include "MeterSig.hpp"
#include "KeySig.hpp"
#include "Sequence.hpp"
#include "StaffRange.hpp"
#include "SMVisitor.hpp"


namespace ScoreModel {

/// @see definition SM_MEASßURE_STATIC in Voice.hpp
#ifdef SM_MEASURE_STATIC   // store objects Sequence
typedef std::vector<Sequence> SequenceSeq;
#else // store pointer to Sequence in stack
typedef std::vector<Sequence*> SequenceSeq;
#endif

class Score;
class Root;
//class Sequence;
class MeasureStack;
class Local;


/// A measure is a container with a fixed duration of 1 mtu,
/// containing engraving info on the voices content in the corr. time window.
/// It is to be embedded in a Part.
class Measure : public Event
{

public:
    
    /// empty measure to be embedded in a part.
    /// @param p the part this measure belongs to.
    /// @param n the measure number. It is also the start time in MTU of this measure.
    /// @param ts new time signature, taking effect at the beginning of this measure.
    ///        deallocated with this measure.
    ///        if NULL, the time signature for this measure is the time signature of the previous one
    /// @param ks new key signature, taking effect at the beginning of this measure.
    ///        deallocated with this measure.
    ///        if NULL, the key signature for this measure is the key signature of the previous one
    /// @param staffmin initial minimal staff number.
    /// @param staffmax initial maximal staff number.
    Measure(Part& p,
            size_t n,
            const MeterSig* ts = NULL,
            const KeySig* ks = NULL,
            int staffmin = 0,
            int staffmax = 0);

    /// deallocate ts and ks if they change with this measure.
    virtual ~Measure();
    
    virtual std::string name() const { return std::string("_bar_"); }
    
    virtual bool isMeasure() const { return true; }

    /// Measure number in part (and score).
    /// @return the measure number
    /// @warning it is also the starting date of the measure.
    /// @warning the string id of the measure is accessible under Event.id()
    long nb() const;

    /// the score containing this measure.
    virtual Score& score() const;

    /// the part containing this measure.
    virtual Part& part() const;
    
    
    /**
     * Navigation in the Measure Matrix
     */
        
    /// @brief the measure is the first in the part.
    virtual bool first() const;

    /// @brief the measure is the last in the part.
    virtual bool last() const;

    /// horizontal move in the matrix of measures: next measure in the same part.
    /// If this measure is the last, then a new measure is created.
    virtual Measure& next() const;

    /// @return previous measure in part or this Measure it is the first measure in part.
    /// @warning this measure must not be the first in part (otherwise return this measure).
    virtual Measure& previous() const;

    /// vertical move in the matrix of measures: measure in given part and same time window.
    /// @param p the part of the measure to find.
    /// @return the measure in part p qith the same number as this measure.
    /// @warning the returned measure is created if it does not exist (the part is modified in this case).
    virtual Measure& goPart(Part& p) const;
    
    
    /// vertical move in the matrix of measures: measure in given part and same time window.
    /// @param partname name of the part of the measure to find.
    /// If there is no part with this name in the score, it is created.
    /// @return the measure in part with the same number as this measure.
    /// @warning the returned measure is created if it does not exist (the part is modified in this case).
    virtual Measure& goPart(const std::string& partname) const;
    
    /// @todo use the above in Builder
    
    /**
     * Sequences (voices)
     */

    size_t nbVoices() const;
    
    /// Scan the Sequence (voices) in this Measure
    SequenceSeq::iterator begin();

    SequenceSeq::iterator end();

    SequenceSeq::const_iterator cbegin() const;

    SequenceSeq::const_iterator cend() const;

    /// search a voice in this measure.
    /// @return the voice of number v in this measure
    /// or NULL if it does not exist.
    Sequence* findVoice(int v) const;

    /// find a voice of number n in this measure or add one if there is none.
    /// @param n voice number
    /// @return the sequence or number n in measure (found or added).
    Sequence& voice(int n);
    
    /// add a voice of number n and content tree t to this measure
    /// @param n voice number
    /// @param t engraving content of the measure for this voice. must be a complete tree.
    /// @return the sequence that has been added.
    /// @warning there must be no sequence of number n in this measure.
    Sequence& addVoice(int n, Root* t);

    
    /**
     * Staves
     */
    
    /// range of  numbers of staves present in this measure
    /// @todo make private
    StaffRange staves;

    /// number of staves for the engraving of this measure.
    inline size_t nbStaves() const { return staves.size(); }
    
    /// Augment the number of staves for this measure, if necessary,
    /// in order to be able to display a staff of given number.
    /// @param nb a staff number. must be positive or null.
    inline void addStaff(int nb) { assert(nb >= 0); staves.add(nb); }

    /**
     * Locals (backlinks)
     */
    
    /// scan back-links = annotations (Local, Floating, Spanning) located in this measure.
    size_t nbBacklinks() const;

    const Local* backlink(size_t i) const;

    /// Register an annotation located in this measure.
    /// @param d annotation to register. must be allocated.
    void addLink(const Local* d);

    
    /**
     * TS, KS and Clef
     */

    /// @todo there can be different clefs & keys for diff. staves
    /// @todo this is engraving info: mv to engraving model??
    // Clef for this measure.
    // inline const Clef& clef() const { return _clef; }
    /// @todo change clef t position other than beginning of measure.
    // void setClef(const Clef& ts);
    // The clef changes in this measure.
    // bool changeClef() const;
    
    /// Time signature for this measure.
    inline const MeterSig* meterSignature() const { return _ts; }
    //void setMeterSig(const MeterSig& ts);

    /// The time signature changes at the beginning of this measure.
    inline bool newMeterSignature() const { return _newTS; }

    /// Key signature for this measure.
    inline const KeySig* keySignature() const { return _ks; }
    //void setKeySig(const KeySig& ts);

    /// The key signature changes at the beginning of this measure.
    inline bool newKeySignature() const { return _newKS; }
    
    virtual void accept(SMVisitor*) const;

    virtual void print(std::ostream& o) const;

protected:
    
    // measure number, should be unique for a score.
    // size_t _nb;

    /// @brief engraving content.
    /// one engraving tree for each voice in this measure.
    SequenceSeq _content;
       
    Part& _part;
    
    /// list of annotations: Local, Floating, Spanning, located in this measure.
    /// @todo smart pointers std::vector<std::shared_ptr<Local>>
    std::vector<const Local*> _backlinks;

    /// Clef for this measure.
    /// @todo moved to engraving model
    // Clef _clef;
    
    /// The time signature changes at the beginning of this measure.
    bool _newTS;

    /// Time signature for this measure.
    /// The default for the first measure is the score time signature.
    /// @todo change by part TS
    /// The default for the next measure is the time signature of the previous one.
    const MeterSig* _ts;
    
    /// @todo TBR (useless?)
    virtual const MeterSig* defaultMeterSig(const MeterSig* ts=NULL) const;
    
    /// The key signature changes at the beginning of this measure.
    bool _newKS;

    /// Key signature for this measure.
    /// The default for the first measure is the score key signature.
    /// @todo change by part KS
    /// The default for the next measures is the key signature of the previous one.
    const KeySig* _ks;

    /// @todo TBR (useless?)
    virtual const KeySig* defaultKeySig(const KeySig* ks=NULL) const;

    /// number of staves for the engraving of this measure.
    //size_t _staves;
    
};


std::ostream& operator<<(std::ostream&, const Measure&);


} // end namespace ScoreModel

/// @}

#endif /* Measure_hpp */

// Implementation of Event functions

#include "Event.hpp"
#include "Score.hpp"
#include <iostream>     // std::cout, std::endl
#include <iomanip>      // std::setw

namespace ScoreModel {

// Main constructor
Event::Event(mtu_t start, mtu_t duration, std::string id):
_onset(start), // set only once
_duration(duration),
_id(id)
{
    assert(MTU::finite(start));
    assert(MTU::defined(duration));
    assert(! id.empty());
}


Event::Event(mtu_t start, mtu_t duration):
_onset(start), // set only once
_duration(duration),
_id(name()+MTU::to_string(start)) // default id
{
    assert(MTU::finite(start));
    assert(MTU::defined(duration));
}

Event::Event(const Event& e):
_onset(e._onset),
_duration(e._duration),
_id(e._id)
{ }

// Destructor
Event::~Event()
{ }


//Measure& Event::measure() const
//{
//    long n = MTU::measure(_onset);
//    assert(n >= 0);
//    return score().measure(n);
//}


//Score& Event::score() const
//{
//    return part().score();
//}


void Event::printOnset(std::ostream& o) const
{
    o << std::setw(10) << std::left << MTU::to_string(_onset);
}


void Event::print(std::ostream& o) const
{
    printOnset(o);
    o << "Event";
    o << "dur=" << _duration;
}




std::ostream& operator<<(std::ostream& o, const Event& e)
{
    e.print(o); // dispatch to descendant classes
    return o;
}


}

/**
 * Abstract model of Events
 *
 * @author Philippe Rigaux
 */

/// @addtogroup scoremodel
/// @{

#ifndef SM_Event_hpp
#define SM_Event_hpp

#include <stdio.h>

#include "trace.hpp"
#include "MTU.hpp"
//#include "MusEvent.hpp"

/// abstract class.
/// An Event is anything that has a start date (onset) and a duration
/// Both start date and duration are expressed in MTU (fraction of bars).
namespace ScoreModel {

class MeasureStack;
//class Voice;
//class Part;
//class Score;
//class Beam;

class Event {

public:
    //static const unsigned int UNDEF_VELOCITY;

    /// Main constructor.
    /// @param start event's onset in MTU
    /// @param duration event's duration in MTU
    /// @param id event's id. must not be empty.
    Event(mtu_t start, mtu_t duration, std::string id);

    /// Event with default id.
    /// @param start event's onset in MTU
    /// @param duration event's duration in MTU
    Event(mtu_t start, mtu_t duration);

    /// copy
    Event(const Event& e);

     /// Destructor
    virtual ~Event();
    
    inline std::string id() const { return _id; };
    
    /// a name specific to every concrete descendant class.
    virtual std::string name() const { return std::string("_VIRTUAL_"); };

    inline void setId(std::string id) { _id = id; };

    /// Each event has a start time in MTU (number of measures).
    /// @todo The start time can be changed for rewriting. use with caution.
    virtual mtu_t date() const { return _onset; };
    virtual mtu_t start() const { return _onset; };
    inline void setDate(mtu_t d)  { _onset = d; };

    /// Each event has a duration in MTU (number of measures).
    virtual mtu_t duration() const { return _duration; };
    /// Reset the duration of this event.
    inline void setDuration(mtu_t dur)  { _duration = dur; };
    /// Extend the duration of this event by the given duration.
    void extendDuration(mtu_t dur) { _duration += dur; }

    // Each event starts in a measure
    // useless. easy to get from start date.
    //Measure& measure() const;
    //inline void setMeasure(Measure* measure) { _measure = measure; };
    
    // Each event belongs to a part
    //virtual Part& part() const = 0;

    // Each event belongs to a score
    //virtual Score& score() const;

    // inline void setStartBeam(Beam *beam) { _startBeam = beam; };
    
    // inline Beam* getStartBeam() { return _startBeam ; };

    // inline void setEndBeam(Beam *beam) { _endBeam = beam; };
    
    // inline Beam* getEndBeam() { return _endBeam ; };

    virtual bool isVoice() const          { return false; }
    virtual bool isAtomic() const         { return false; }

    virtual bool isNote() const           { return false; }
    virtual bool isRest() const           { return false; }
    virtual bool isChord() const          { return false; }
    virtual bool isGraceNote() const      { return false; }
    virtual bool isGraceChord() const     { return false; }
    virtual bool isTrill() const          { return false; }

    virtual bool isMeasureStack() const   { return false; }
    virtual bool isMeasureStacked() const { return false; }
    virtual bool isMeasure() const        { return false; }
    virtual bool isSequence() const       { return false; }
    virtual bool isNode() const           { return false; }

//    inline void setGraceNote()  {_isGrace = true;}
//    inline bool isGraceNote()  const {return _isGrace;}


    /// formatted onset
    void printOnset(std::ostream& o) const;

    virtual void print(std::ostream& o) const;

protected:

    /// Each event has a start time in MTU (number of measures)
    mtu_t _onset;

    /// Each event has a duration in MTU (number of measures)
    mtu_t _duration;

    // An event always starts in a measure
    // MeasureStack* _measure;
    
    // A beam can start at an event ; a beam can end on an event
    // Beam *_startBeam, *_endBeam;

    /// @brief Id as a string for XML output
    std::string _id;

    // Grace note or chord indicator
    // bool _isGrace = false;
};


std::ostream& operator<<(std::ostream&, const Event&);


}

#endif /* SM_Event_hpp */

/// @}

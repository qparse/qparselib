//
//  GraceChord.cpp
//  squant2
//
//  Created by Florent Jacquemard on 20/01/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#include "GraceChord.hpp"


namespace ScoreModel {

// pitched note
GraceChord::GraceChord(mtu_t start, Voice& voice):
Chord(start, MTU::ZERO, voice)
{    }


void GraceChord::accept(SMVisitor* v) const
{
    v->visitGraceChord(this);
}


void GraceChord::accept(AEVisitor* v)
{
    v->visitGraceChord(this);
}


void GraceChord::print(std::ostream& o) const
{
    printOnset(o);
    o << ": ";
    if (size() > 0)
    {
        o << "{ ";
        for (Note* n : _notes )
        {
            assert(n);
            o << n->pitch();
            o << " ";
        }
        o << "} ";
    }
}


std::ostream& operator<<(std::ostream& o, const GraceChord& e)
{
    e.print(o);
    return o;
}


}

//
//  Chord.hpp
//  qparse
//
//  Created by Florent Jacquemard on 18/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//


#ifndef SM_Chord_hpp
#define SM_Chord_hpp
/// @addtogroup scoremodel
/// @{


#include <stdio.h>
#include <set>
#include <initializer_list>

#include "trace.hpp"
#include "AtomicEvent.hpp"
#include "Pitch.hpp"
#include "Note.hpp"
#include "AEVisitor.hpp"

namespace ScoreModel {

struct NoteComp
{
  bool operator() (const Note* lhs, const Note* rhs) const
  {
      assert(lhs);
      assert(rhs);
      return (*lhs < *rhs);
  }
};


/// @todo Note*
typedef std::set<Note*, NoteComp> NoteBag;


class Chord : public AtomicEvent
{
public:
    /// chord of size 0
    Chord(mtu_t start, mtu_t duration, Voice& voice);

    /// chord with variable number of pitches in arg.
//    Chord(mtu_t start, mtu_t duration,
//          std::initializer_list<const Pitch&> args);
    
    virtual ~Chord();
    
    virtual std::string name() const { return std::string("_chord_"); }

    virtual Event* clone() ;

    virtual bool isChord() const { return true; }

    /// number of notes in this chord.
    size_t size() const;
    
    /// return the ith pitch in this chord.
    /// @param i note index in chord. bass is 0.
    const Pitch& pitch(size_t i) const;
    
    /// add a note with the given pitch to this chord.
    /// @param p pitch to add. it is copied in this chord.
    /// @return true iff a new note was succesully added to this chord.
    bool add(const Pitch& p);
    
    /// return the ith note in this chord.
    /// @param i note index in chord. bass is 0.
    /// @return note is not const : pitch can be named afterwards.
    Note* note(size_t i) const;
    
    /// Scan the notes in this chord
    inline NoteBag::iterator begin()
    { return _notes.begin(); }

    inline NoteBag::iterator end()
    { return _notes.end(); }

    inline NoteBag::const_iterator cbegin() const
    { return _notes.cbegin(); }

    inline NoteBag::const_iterator cend() const
    { return _notes.cend(); }
    
    virtual void accept(SMVisitor*) const;
    virtual void accept(AEVisitor*);

    virtual void print(std::ostream& o) const;

protected:
    
    NoteBag _notes;
    
};


std::ostream& operator<<(std::ostream&, const Chord&);


} // end namespace ScoreModel


/// @}


#endif /* SM_Chord_hpp */

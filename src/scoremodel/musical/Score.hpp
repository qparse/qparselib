/**
 * Model of a score
 *
 * @author Philippe Rigaux
 */
/// @defgroup scoremodel module
///
/// The `scoremodel` module contains an intermediate representation
/// for traducing a concrete score file, in various possible formats,
/// from the result of k-best parsing of input with a tree automaton.

#ifndef Score_hpp
#define Score_hpp
/// @addtogroup scoremodel
/// @{


#include <stdio.h>
#include <vector>

#include "Clef.hpp"
#include "MeterSig.hpp"
#include "KeySig.hpp"
#include "Part.hpp"
#include "MeasureStack.hpp"
#include "SMVisitor.hpp"


namespace ScoreModel {

/// @see definition SM_MEASURE_STATIC in Voice.hpp
/// @todo change PartSet to map str name into Part ?
#ifdef SM_MEASURE_STATIC
#define SM_MAX_MEASURE 256
#define SM_MAX_PART 32
#define SM_MAX_SEQUENCE 4
typedef std::vector<MeasureStack> MeasureStackSeq;
typedef std::vector<Part> PartSeq;
#else
typedef std::vector<MeasureStack*> MeasureStackSeq;
typedef std::vector<Part*> PartSeq;
#endif


/// The score class: models a score content
class Score
{

public:

    /// Builds an new empty score.
    /// @param name name of the score. If empty, a generic name is given.
    /// @param meter the default time signature.
    /// @param key the default key signature.
    /// @todo mv default ts and ks values to Builder
	Score(const std::string name = std::string(),
          const MeterSig& meter = MeterSig(4, 4),
          const KeySig& key = KeySig(0));
   
    /// Destructor
    virtual ~Score();

    const std::string& id() const;
    
//    inline const Clef& clef() const { return _clef; }
//    void setClef(const Clef& clef);

    /// @return the default time signature.
    inline const MeterSig& meterSignature() const { return _ts; }
    void setMeterSignature(const MeterSig& ts);

    /// @return the default key signature.
    inline const KeySig& keySignature() const { return _ks; }
    void setKeySignature(const KeySig& ks);
    

    /**
     * Parts
     */
    
    /// number of parts
    size_t nbParts() const;

    /// Get the part of given name if it exists in score
    /// or add a new empty part.
    /// @param name the name of the part. must not be empty.
    /// @return a reference to the part of given name in score.
    Part& part(const std::string& name);
    
    /// Get a  part of the score form its name.
    /// @param name the name of the part.it must be present in the score.
    /// @return a reference to the part of given name in the score.
    /// return an empty part if no part of this name found in the score.
    const Part& getPart(const std::string& name) const;

    /// Add a new empty part.
    /// @param name the name of the new part.
    /// If ommitted, defaulted to "part"+number of parts.
    /// @return a reference to the new part in score.
    /// @warning if a part already exists with the same name,
    /// it is returned and the score is not changed.
    Part& addPart(const std::string& name = std::string());


    /// Scan the parts in this score
    //inline PartSet& getParts() const { return _parts; }
    inline PartSeq::iterator begin()
    { return _parts.begin(); }

    inline PartSeq::iterator end()
    { return _parts.end(); }
    
    inline PartSeq::const_iterator cbegin() const
    { return _parts.cbegin(); }
    
    inline PartSeq::const_iterator cend() const
    { return _parts.cend(); }
    
	// Get a part from its name
    // @return a part to the part of given name if it exists
    // or NULL otherwise.
	//Part* getPart(std::string name) const;

     
    // Get a part from its index
    // @return a part to the part of given index if it exists
    // or NULL otherwise.
    //Part* getPart(size_t i) const;

    /// Get a voice from the part and voice id.
    /// @param name the part name
    /// @param id the voice id
    /// @todo replace voice name by voice id (int)
    Voice& voice(const std::string name, int id);
        
    /// Get a measure stack from its index.
    /// @param i the index of the mesure. It is its MTU start date
    /// (0 for the first measure).
    /// @return the stack of measures number i.
    /// If there are currently less than i measures,
    /// then an appropriate number of empty measure stacks are added.
    /// @todo TBD deprecated
    MeasureStack& stack(size_t i);

    
    
    /**
     * Measure Stacks
     * deprecated Measure since Measures are contained in Parts
     * list of MeasureStacks is always empty
     * @todo TBD
     */
    
    /// number of measures
    /// @todo TBD (in Part)
    size_t nbMeasures() const;

    /// Add a new measure stack and a new Measure associated
    /// to the given part in the stack.
    /// @param p reference to a part.
    /// @param ts parameter time signature for the creation of a new measure.
    /// @param ks parameter key signature for the creation of a new measure.
    /// @return the new measure in new stack.
    /// @todo TBD (in Part)
    Measure& newMeasure(Part& p,
                        const MeterSig* ts = NULL,
                        const KeySig* ks =NULL);

    /// Iterator to scan measure stacks
    /// @todo TBD (in Part)
    inline MeasureStackSeq& getMeasures() { return _measures; }
   
    /// @todo TBD (in Part)
    inline MeasureStackSeq::iterator beginMeasures()
    { return _measures.begin(); }

    /// @todo TBD (in Part)
    inline MeasureStackSeq::iterator endMeasures()
    { return _measures.end(); }

    /// @todo TBD (in Part)
    inline MeasureStackSeq::const_iterator cbeginMeasures() const
    { return _measures.cbegin(); }

    /// @todo TBD (in Part)
    inline MeasureStackSeq::const_iterator cendMeasures() const
    { return _measures.cend(); }
       
    /// @todo TBD (in Part)
    virtual void accept(SMVisitor*) const;

    // complete this score with the parse tree at associated to
    // the n-best run of target k in the given parse table tab.
    // @param tab a parse table indexed by keys of type K
    // @param k key (index of row in table) can be partial or complete.
    // @param n (as in n-best) is the rank of the run with target the key
    //        of the record. must be larger or equal to 1 (default).
    // @param unfailing if true, construct an empty score
    // iff there is a NULL subrun in the n-best run for k in tab
    // (bottom-up NULL propagation).
    // otherwise the score may contain empty parts
    // when parsing partially failed e.g. for 1 bar (NULL subrun).
//    template<class K>
//    void ofTree(parsing::TableA<K>* tab,
//                const K& k,
//                size_t n=1,
//                bool unfailing=false);

private:
    /// A score has a name
    const std::string _id;
    //std::string getName() const;

    // A score has a default clef
    //Clef _clef;

    /// A score has a default meter signature. deallocated with score.
    MeterSig _ts;

    /// A score has a default key signature.  deallocated with score.
    KeySig _ks;
    
	/// The musical content of a score, made of a set of parts.
	PartSeq _parts;

	/// The engraving of a score, organized as a list of mappings of
    /// part names into measures.
	MeasureStackSeq _measures;
    
    /// Add a new measure stack at the end of last stack.
    /// @param fill flag, if true, a new measure for each existing part
    /// is created in the stack. otherwise, the stack is returned empty.
    /// @return the measure stack added.
    /// @warning every Measure created has same TS and KS as previous.
    MeasureStack& addbackStack(bool fill=false);

};

} // end namespace ScoreModel

/// @}

#endif /* Score_hpp */



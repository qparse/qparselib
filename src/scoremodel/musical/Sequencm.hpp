/**
 * A sequence = a list of events
 *
 * Utility class used for sequential calculations
 *
 * @author Philippe Rigaux
 */

#ifndef Sequencm_hpp
#define Sequencm_hpp

#include <stdio.h>

#include <vector>

#include "Event.hpp"

namespace ScoreModel {

class Part;
class Score;
class Note;


/// @brief A sequence is a list of events
class Sequencm {

public:
	/**
	 * Main constructor.
	 */
	Sequencm();


	/**
	 * Add an event
	 */
	void addEvent(Event* event);

	/**
	 * Get events
	 */
	std::vector<Event*> getEvents() const;

	/**
	 * Concatenate a sub-sequence
	 */
	void concatenate(const Sequencm& seq) ;

	/**
	 * Nb events
	 */
	size_t nbEvents() const;

	/**
	 * First event
	 */
	Event* getFirstEvent() const;

	/**
	 * Last event
	 */
	Event* getLastEvent() const;

	/**
	 * Destructor
	 */
	~Sequencm();

private:

	/**
	 * A sequence is a sequence of events
	 */
	std::vector<Event*> _events;

};

}
#endif /* Sequencm_hpp */

/**
 * Representation of a time signature
 * (aka meter signature, or measure signature)
 *
 * @author Philippe Rigaux
 */
/// @addtogroup scoremodel
/// @{

#ifndef SM_MeterSig_hpp
#define SM_MeterSig_hpp

#include <stdio.h>
#include "trace.hpp"

#include "MTU.hpp"
#include "CMND.hpp"
#include "SMVisitor.hpp"


namespace ScoreModel {

/// @brief Representation of a time signature
/// (aka metre signature, or measure signature).
/// @todo add additive expressions
/// @see https://music-encoding.org/guidelines/v4/elements/metersig.html
class MeterSig
{

public:

    /// undef time signature
    MeterSig();

    /// @param ts_count numerator: number of units. must not be 0.
    /// @param ts_unit denominator : inverse of duration of unit in whole notes.
    /// must not be 0.
    MeterSig(int ts_count, int ts_unit);
    
    MeterSig(const MeterSig& ts);
    
    /// Destructor
    ~MeterSig() {};

    MeterSig& operator=(const MeterSig& rhs);

    bool operator==(const MeterSig& rhs) const;

    /// Accessors
    inline int getCount() const { return _ts_count; }
    inline int getUnit() const { return _ts_unit; }

    // Duration getMeasureDuration() const;

    /// cast duration from MTU (number of bars) into number of whole notes.
    /// @param d duration in MTU. Must be smaller than or equal to 1.
    /// @warning the measure must not be undef.
    /// @todo move to Event ?
    Rational MTUtoWhole(mtu_t d) const;

    /// cast given duration wrt this time signature,
    /// from MTU (number of bars) into the exponent of 2 of CMN note value:
    /// - -2  quadruple whole note (longa)
    /// - -1  double whole note (breve)
    /// -  0  whole note.
    /// -  1  half note.
    /// -  2  quarter note.
    /// -  3  8th note.
    /// - ...
    /// @param d duration to cast, in mtu, must not be 0.
    cmnd_t MTUtoCMN(mtu_t d) const;
    
    /// measure where the beat divided by 3.
    /// @return whether the numerator is divisible by 3 and not 3.
    bool compound() const;

    /// measure where the beat divided by 2.
    /// @return whether the measure is not compound or undef.
    bool simple() const;

    /// unknown time signature created with empty constructor.
    bool undef() const;

    /// number of beats in the measure.
    /// failure if it is not an integer.
    int beats() const;
    
    virtual void accept(SMVisitor*) const;

    void print(std::ostream& o) const;
    
private:
    
    /// number of beats in a measure
    /// = the top number of the meter signature
    /// @todo add additive expressions
    int _ts_count;
    
    /// number indicating the beat unit
    /// = the bottom number of the meter signature
    int _ts_unit;

};

std::ostream& operator<<(std::ostream&, const MeterSig&);

} // end namespace ScoreModel

#endif /* SM_MeterSig_hpp */

/// @} // end group scoremodel

//
//  Chord.cpp
//  qparse
//
//  Created by Florent Jacquemard on 18/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Chord.hpp"

namespace ScoreModel {

// empty chord
Chord::Chord(mtu_t start, mtu_t duration, Voice& voice):
AtomicEvent(start, duration, voice),
_notes()
{ }


//Chord::Chord(mtu_t start, mtu_t duration,
//             std::initializer_list<const Pitch&> args):
//AtomicEvent(start, duration),
//_pitchs()
//{
//    for(const Pitch& p : args )
//    {
//        add(p);
//    }
//}


Chord::~Chord()
{ }


Event* Chord::clone()
{
    Chord* c = new Chord(date(), duration(), voice());
    
    return c;
}


size_t Chord::size() const
{
    return _notes.size();
}


const Pitch& Chord::pitch(size_t i) const
{
    assert(i < _notes.size());
    NoteBag::const_iterator it = _notes.cbegin();
    for(size_t c = 0; (it != _notes.cend()) && (c < i); ++c) { ++it; }
    // it = it + i;
    assert(it != _notes.cend());
    return (*it)->pitch();
}


Note* Chord::note(size_t i) const
{
    assert(i < _notes.size());
    NoteBag::iterator it = _notes.begin();
    for(size_t c = 0; (it != _notes.end()) && (c < i); ++c) { ++it; }
    // it = it + i;
    assert(it != _notes.cend());
    return *it;
}


bool Chord::add(const Pitch& p)
{
    // auto ret = _notes.emplace(date(), duration(), voice(), p);
    auto ret = _notes.insert(new Note(date(), duration(), voice(), p));
    return ret.second;
}


void Chord::accept(SMVisitor* v) const
{
    v->visitChord(this);
}


void Chord::accept(AEVisitor* v)
{
    v->visitChord(this);
}


void Chord::print(std::ostream& o) const
{
    printOnset(o);
    o << ":";
    if (size() > 0)
    {
        o << "{ ";
        for (Note* n : _notes )
        {
            assert(n);
            o << n->pitch();
            o << " ";
        }
        o << "} ";
    }
    o << "dur=" << _duration;
}


std::ostream& operator<<(std::ostream& o, const Chord& e)
{
    e.print(o);
    return o;
}

}

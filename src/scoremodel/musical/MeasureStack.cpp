// Implementation of Measure functions

#include "MeasureStack.hpp"
#include "Score.hpp"
//#include "MeasureStacked.hpp"


namespace ScoreModel {



// Main constructor
MeasureStack::MeasureStack(size_t n, Score& parent):
Event(MTU::of_int(n), MTU::ONE, name()+std::to_string(n)),
_nb(n),
_content(), // empty
_index(),
_container(parent)
//_prev((n == 0)?NULL:&(parent.stack(n-1)))
{
#ifdef SM_MEASURE_STATIC
    _content.resize(SM_MAX_PART, Measure(Part(parent, std::string()), *this));
#endif

}

// Destructor

MeasureStack::~MeasureStack()
{
    TRACE("delete MeasureStack {}", nb());

#ifndef SM_MEASURE_STATIC
    for (MeasureStacked* m : _content)
    {
        assert(m);
        delete m;
    }
#endif
    // useless: vectors are always deleted
    _content.clear();
}


// equivalent to _nb
//long MeasureStack::nb() const
//{
//    return MTU::measure(this->date());
//}


MeasureStack* MeasureStack::previous() const
{
    size_t n = nb(); // measure number
    if (n == 0)
        return NULL;
    else
        return &(_container.stack(n-1));
}


MeasureStacked& MeasureStack::add(Part& p,
                                  bool fnew, bool fnotnew,
                                  const MeterSig* ts, const KeySig* ks)
{
    assert(! (fnew && fnotnew)); // flags mutually exclusive
    assert((! fnotnew) || ((ts == NULL) && (ks == NULL)));
    assert(p.id().size() > 0);
    size_t n = _content.size(); // index of next measure
    std::pair<std::map<std::string, size_t>::iterator, bool> res =
        _index.insert(std::pair<std::string, size_t>(p.id(), n));
//   _index.emplace(std::piecewise_construct,
//                  std::forward_as_tuple(p.id()),
//                  std::forward_as_tuple(p, *this, ts, ks));
//    _content.try_emplace(name, _nb, name, *this); // C++17

    // there is already a measure for the part p
    if (res.second == false)
    {
        assert(! fnew);
        if (! fnotnew) // 
            WARN("MeasureStack.add: part:{} already has a measure in stack:{},\
                 cannot change TS and KS", p.id(), _id);
        assert((res.first)->first == p.id());
        size_t j = (res.first)->second; // index of measure alread associated to part
        assert(j < _content.size());
        /// @todo shall we change ts and ks for measure _content[j] ?
        /// it is not possible yet in measure
#ifdef SM_MEASURE_STATIC
        Measure& m = _content[j];
        return m;
#else
        MeasureStacked* m = _content[j];
        return *m;
#endif
    }
    // a new measure is created for the part p
    else
    {
        assert(! fnotnew);
        // check that <partname, n> was inserted in the index
        assert((res.first)->first == p.id());
        assert((res.first)->second == n);
        assert(_index.at(p.id()) == n);
        TRACE("MeasureStack.add: new part {} in measure stack {}", p.id(), _id);
#ifdef SM_MEASURE_STATIC
        _content.emplace_back(p, *this, ts, ks); // new measure
        assert(_content.size() == n+1);
        return _content[n];  // _content.back();
#else
        MeasureStacked* m = new MeasureStacked(p, *this, ts, ks);
        _content.push_back(m);
        assert(_content.size() == n+1);
        assert(_content[n] == m);
        return *m;
#endif
    }
}


MeasureStacked& MeasureStack::ofPart(Part& p, const MeterSig* ts, const KeySig* ks)
{
    return add(p, false, false, ts, ks);
}


MeasureStacked& MeasureStack::newPart(Part& p, const MeterSig* ts, const KeySig* ks)
{
    return add(p, true, false, ts, ks);
}


MeasureStacked& MeasureStack::get(Part& p)
{
    return add(p, false, true, NULL, NULL);
}


MeasureStacked& MeasureStack::get(const std::string& partname)
{
    Part& p = _container.part(partname);
    return get(p);
}



//Measure& MeasureStack::addPart(const std::string& name)
//{
//    return addPart(p.id());
//}


//Measure& MeasureStack::ofPart(const std::string& name)
//{
//    assert(! name.empty());
//    return _content[name];
//}


void MeasureStack::accept(SMVisitor* v) const
{
    v->visitMeasureStack(this);
}


void MeasureStack::print(std::ostream& o) const
{
    size_t n = _content.size();
    if (n == 0)
        o << "MeasureStack: empty";
    else   // (n > 1)
        o << "MeasureStack (" << n << " measures):";

    for (auto p : _index)
    {
        o << " part:" << p.first << " : ";
        assert(p.second < _content.size());
        o << _content[p.second];
        o << "\n";
    }
}

} // end namespace ScoreModel

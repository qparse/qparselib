//
//  Rest.hpp
//  qparse
//
//  Created by Florent Jacquemard on 18/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//


#ifndef SM_Rest_hpp
#define SM_Rest_hpp
/// @addtogroup scoremodel
/// @{


#include <stdio.h>

#include "AtomicEvent.hpp"
#include "AEVisitor.hpp"

namespace ScoreModel {


class Rest : public AtomicEvent
{
public:
    Rest(mtu_t start, mtu_t duration, Voice& voice);

    virtual ~Rest() {}
    
    virtual std::string name() const { return std::string("_rest_"); }

    virtual Event* clone() ;

    virtual bool isRest() const { return true; }

    virtual bool isNote() const { return false; }

    virtual void accept(SMVisitor*) const;
    virtual void accept(AEVisitor*);

    virtual void print(std::ostream& o) const;

};


std::ostream& operator<<(std::ostream&, const Rest&);


} // end namespace ScoreModel


/// @}


#endif /* SM_Rest_hpp */

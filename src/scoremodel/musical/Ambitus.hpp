//
//  Ambitus.hpp
//  squant2
//
//  Created by Florent Jacquemard on 14/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef Ambitus_hpp
#define Ambitus_hpp
/// @addtogroup scoremodel
/// @{


#include <stdio.h>
#include <iostream>

#include "trace.hpp"
#include "Pitch.hpp"
#include "SMVisitor.hpp"

namespace ScoreModel {

class AtomicEvent;
class Note;
class GraceNote;
class Rest;
class Chord;
class Trill;


/// @brief Range in a set of pitches.
///  Only the minn and max pitches are stored, not the other values.
class Ambitus : private SMVisitor
{
public:
    
    Ambitus();
    
    /// create an ambitus with given extrema values.
    /// @param pl a pitch value. must not be undef.
    /// @param ph a pitch value. must not be undef. must be higher than pl.
    Ambitus(const Pitch& pl, const Pitch& ph);

    ~Ambitus();
    
    inline bool undef() const { return (_low.undef() && _high.undef()); }
    inline bool empty() const { return undef(); }
    
    /// lowest pitch in the staff.
    /// @return the lowest pitch in the staff. can be undef if staff is empty.
    inline const Pitch& low() const { return _low; }

    /// highest pitch in the staff.
    /// @return the highest pitch in the staff. can be undef if staff is empty.
    inline const Pitch& high() const { return _high; }

    /// update this Ambitus with the given pitch value.
    /// @param p a pitch value. must not be undef.
    void add(const Pitch& p);

    /// update this ambitus with pitch of the given event.
    /// @param e an atomic event. can be unpitched (e.g. Rest)
    void add(const AtomicEvent* e);
        
    virtual void print(std::ostream& o) const;

private:
    
    Pitch _low;
    
    Pitch _high;
        
    /// update staff def with the pitch value of the given element.
    virtual void visitNote(const Note* e);

    /// update staff def with the pitch value of the given element.
    virtual void visitGraceNote(const GraceNote* e);

    /// update staff def with the pitch value of the given element.
    virtual void visitRest(const Rest* e);

    /// update staff def with the pitch value of the given element.
    virtual void visitChord(const Chord* e);

    /// update staff def with the pitch value of the given element.
    virtual void visitTrill(const Trill* e);
    
    // the other visitors do nothing
    
};

std::ostream& operator<<(std::ostream& o, const Ambitus& a);


} // namespace ScoreModel

/// @}

#endif /* Ambitus_hpp */

//
//  Trill.cpp
//  qparse
//
//  Created by Florent Jacquemard on 18/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Trill.hpp"

namespace ScoreModel {

Trill::Trill(mtu_t start, mtu_t dur, Voice& voice,
             const Pitch& mp, const Pitch& sp):
Note(start, dur, voice, mp),
_second(new Note(start, dur, voice, sp))
{
    // assert(mp < sp);
}


Event* Trill::clone()
{
    return new Trill(date(), duration(), voice(), pitch(), pitch2());
}


Note* Trill::second() const
{
    return _second;    
}


const Pitch& Trill::pitch2() const
{
    assert(_second);
    return _second->pitch();
}


void Trill::accept(SMVisitor* v) const
{
    v->visitTrill(this);
}


void Trill::accept(AEVisitor* v)
{
    v->visitTrill(this);
}


void Trill::print(std::ostream& o) const
{
    printOnset(o);
    o << ":";
    o << pitch();
    o << "~";
    o << pitch2();
    o << "dur=" << _duration;
}


std::ostream& operator<<(std::ostream& o, const Trill& e)
{
    e.print(o);
    return o;
}


}

//
//  Sequence.cpp
//  squanty
//
//  Created by Florent Jacquemard on 04/04/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Sequence.hpp"
#include "Measure.hpp"
#include "Node.hpp"
#include "Root.hpp"

namespace ScoreModel {

Sequence::Sequence(int n, Root* t, Measure& parent):
Event(parent.date(), MTU::ONE, parent.id()+name()+std::to_string(n)),
// straves(), // empty
_nb(n),
_content(t),
_container(parent)
{
    assert(t != NULL);
    assert(t->size() == 1);
    assert(! t->staves().empty());
}


Sequence::Sequence(int n, Measure& parent):
Event(parent.date(), MTU::ONE, parent.id()+name()+std::to_string(n)),
// straves(), // empty
_nb(n),
_content(NULL),
_container(parent)
{
    assert(parent.duration() == MTU::ONE);
}


Sequence::~Sequence()
{
    TRACE("delete Sequence measure {} voice#{} part:{}",
          measureNb(), _nb, _container.part().id());
    if (_content)
        delete _content;
}
 

long Sequence::measureNb() const
{
    //DEBUGU("Sequence {}: {} mtu", voice(), date());
    return MTU::measure(this->date());
}


void Sequence::add(Root* t)
{
    assert(t);
    // t may be incomplete. otherwise, t has one subtree
    assert((! t->complete()) || (t->size() == 1));
    // and some content
    assert((! t->complete()) || (! t->staves().empty()));

    assert(_content == NULL);
    _content = t;
    // update container measure in case this sequence was added empty
    _container.staves.merge(this->staves());
}


const StaffRange& Sequence::staves() const
{
    assert(_content);

    return _content->staves();
}


//void Sequence::addStaff(int n)
//{
//    assert(n >= 0);
//    straves.add(n);
//    _container.addStaff(n); // update containinng measure
//}


void Sequence::accept(SMVisitor* v) const
{
    v->visitSequence(this);
}


void Sequence::print(std::ostream& o) const
{
    if (_content == NULL)
    {
        o << "Sequence: NULL";
        return;
    }
    
    o << "Sequence: ";
    _content->print(o);
    /// @todo STUB
}

} // end namespace ScoreModel

/**
 * Abstract model of voice
 *
 * @author Philippe Rigaux
 */

#ifndef Voice_hpp
#define Voice_hpp
/// @addtogroup scoremodel
/// @{



// if defined, the containers of score contain objects,
// otherwise they contain pointers.
//#define SM_MEASURE_STATIC
//#define SM_MEASURE_POINTERS


#include <stdio.h>
#include <vector>

//#include "SymbLabel.hpp"
#include "MTU.hpp"
//#include "Pitch.hpp"
#include "Ambitus.hpp"
//#include "PointedRT.hpp" // for v.1
//#include "LRT.hpp" // for v.2
//#include "InputSegment.hpp"
//#include "Measure.hpp"
//#include "Sequence.hpp"
#include "AtomicEvent.hpp"
#include "SMVisitor.hpp"
//#include "Tuplet.hpp"
//#include "Beam.hpp"
//#include "Score.hpp"
//#include "Rest.hpp"


namespace ScoreModel {


class Score;
class Part;
class AtomicEvent;
//class Note;


//typedef std::pair<Pitch, Pitch> VoiceRange;
//typedef std::pair<Note*, Note*> Tie;

/// polymorphic list of atomic (non-voice) events
typedef std::vector<AtomicEvent*> EventSeq;


///
/// A voice is a sequence of events, belonging to a Part
///
/// @todo question: is a part monophonic? i.e. no overlap between voices.
/// in this case, duration() for Voices can be a function.
class Voice : public Event
{

public:

    /// @brief new empty voice in a part.
    /// @param start voice's start date in MTU.
    /// @param part container.
    /// @param id voice number (index of the voice in the part), unique.
    /// The created voice is initialy empty and its duration is set to 0.
    Voice(mtu_t start, Part& part, int id);

    /// @brief new empty voice in a part.
    /// @param part container.
    /// @param i voice number (index of the voice in the part), unique.
    /// The created voice is initialy empty and its duration
    /// and start date are set to 0 MTU.
    Voice(Part& part, int i);
    
    /// @brief new voice empty voice in a part with given name.
    /// @param start voice's start date in MTU
    /// @param part container.
    /// @param name voice id, must be unique and not empty.
    /// This parameter is optional; if not set,
    /// a unique name is created from the part's name.
    /// The created voice is initialy empty and its duration is set to 0.
    Voice(mtu_t start, Part& part, const std::string name = std::string());

    /// Destructor
    virtual ~Voice();

    virtual std::string name() const { return std::string("_voice_"); }

    inline int number() const { return _nb; }
    
    /// part container of this voice
    virtual Part& part() const;

    /// score container of this voice
    virtual Score& score() const;
    
    // get the range of a voice a a pair of pitches
    const Ambitus& ambitus() const;
    inline const Ambitus& getRange() const { return ambitus(); }

    /// Add an event at the end of this voice.
    /// the voice's duration is updated.
    /// @param event pointer to allocated atomic event.
    void add(AtomicEvent* event);

    
    /// scan events
    //EventSeq getEvents() const;
    inline EventSeq::iterator begin() { return _events.begin(); }

    inline EventSeq::iterator end() { return _events.end(); }

    inline EventSeq::const_iterator cbegin() const
    { return _events.cbegin(); }

    inline EventSeq::const_iterator cend() const
    { return _events.cend(); }
    
    /**
     * Extract the part that belongs to a measure
     */
    Voice* trimMeasure(const MeasureStack& m) const;
    
    /// @todo trim voice:
    ///  reset start and duration to fit content.
    

    // Get ties - mv to engraving model
    // std::vector<Tie*> getTies() const;

    // Get tuplets - mv to engraving model
    // std::vector<Tuplet*> getTuplets() const;

    // Get beams - mv to engraving model
    // std::vector*<Beam*> getBeams() const;

    virtual void accept(SMVisitor*) const;

private:

    ///voice number.
    int _nb;

    /// The part this voice belongs to
    Part& _part;

    /// A voice is a monophonic sequence of events
    EventSeq _events;

    // Ties between events  - mv to engraving model
    // std::vector<Tie*> _ties;

    // Tuplets - mv to engraving model
    // std::vector<Tuplet*> _tuplets;

    // Beams  - mv to engraving model
    //std::vector<Beam*> _beams;
    
    Ambitus _ambitus;
    
};

} // end namespace ScoreModel

/// @}


#endif /* Voice_hpp */

//
//  AtomicEvent.hpp
//  squanty
//
//  Created by Florent Jacquemard on 12/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//



#ifndef SM_AtomicEvent_hpp
#define SM_AtomicEvent_hpp
/// @addtogroup scoremodel
/// @{


#include <stdio.h>

//#include "Measure.hpp"
//#include "Duration.hpp"
#include "Event.hpp"
//#include "Part.hpp"
//#include "Voice.hpp"
//#include "LeafEvent.hpp"
#include "SMVisitor.hpp"
#include "AEVisitor.hpp"


/// An Atomic Event is an Event contained in a (unique) monophonic voice
namespace ScoreModel {

class Score;
class Part;
class Voice;
class LeafEvent;
class Continuation;

class AtomicEvent : public Event
{

public:
    
    //using Event::Event;
    
    /// manual creation, without engraving content.
    AtomicEvent(mtu_t start, mtu_t duration, Voice& voice);
    
    /// creation from an engraving element
    /// @param leaf event in engraving model corresponding to the start
    /// of this event. must be allocated.
    AtomicEvent(const LeafEvent* leaf, Voice& voice);

    virtual ~AtomicEvent();
        
    /// should not be used.
    virtual std::string name() const { return std::string("_aevt_"); }

    /// An atomic event always belongs to a unique voice
    inline Voice& voice() const { return _voice; };
    //void setVoice(Voice& voice);

    /// An atomic event always belongs to a unique part
    virtual Part& part() const;

    /// An atomic event always belongs to a unique score
    virtual Score& score() const;
    
    /// leaf in engraving model corresponding to this event (backlink)
    /// or to the beginning (prefix) of this event in case of continuation.
    inline const LeafEvent* engraving() { return _engraving_main; }
    
    virtual void accept(SMVisitor*) const = 0;
    virtual void accept(AEVisitor*) = 0;

    virtual void print(std::ostream& o) const;

protected:

     /// An atomic event always belongs to a voice
    Voice& _voice;
    
    /// leaf in engraving model corresponding to this event (backlink)
    /// or to the beginning (prefix) of this event in case of continuation.
    const LeafEvent* _engraving_main;

    /// events in engraving model corresponding to
    /// the continuation (of the prefix) for this event (backlink).
    std::vector<const Continuation*> _engraving_cont;

};


std::ostream& operator<<(std::ostream&, const AtomicEvent&);


} // end namespace ScoreModel


/// @}


#endif /* SM_AtomicEvent_hpp */



//
//  Ambitus.cpp
//  squant2
//
//  Created by Florent Jacquemard on 14/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Ambitus.hpp"

#include "AtomicEvent.hpp"
#include "Note.hpp"
#include "GraceNote.hpp"
#include "Rest.hpp"
#include "Chord.hpp"
#include "Trill.hpp"


namespace ScoreModel {



Ambitus::Ambitus():
_low(Pitch()),   // undef
_high(Pitch())
{ }


Ambitus::Ambitus(const Pitch& pl, const Pitch& ph):
_low(pl),
_high(ph)
{
    assert(! pl.undef());
    assert(! ph.undef());
    assert(pl <= ph);
}


Ambitus::~Ambitus()
{ }


// robust: works for arb. combinations of undef or def low and high pitches.
// in principle, low and hight are both simultaneously def or undef.
void Ambitus::add(const Pitch& p)
{
    assert(! p.undef());
    Pitch& low = _low;
    Pitch& high = _high;
    
    if (low.undef())
    {
        if (high.undef())
        {
            low = p;
            high = p;
        }
        else if (high < p)
        {
            low = high;
            high = p;
        }
        else
        {
            low = p;
        }
    }
    else if (high.undef())
    {
        if (p < low)
        {
            high = low;
            low = p;
        }
        else
        {
            high = p;
        }
    }
    // high and low are defined
    else if (p < low)
    {
        assert(low <= high);
        low = p;
    }
    else if (high < p)
    {
        assert(low <= high);
        high = p;
    }
    else // ignore
    {  }

    assert(! low.undef());
    assert(! high.undef());
    assert(low <= p);
    assert(p <= high);
}


void Ambitus::add(const AtomicEvent* e)
{
    assert(e);
    //ERROR("Ambitus.add AtomicEvent should not be called");
    e->accept(this);
}


void Ambitus::visitNote(const Note* e)
{
    assert(e);
    add(e->pitch());
}


void Ambitus::visitGraceNote(const GraceNote* e)
{
    assert(e);
    add(e->pitch());
}


void Ambitus::visitRest(const Rest* e)
{
    assert(e);
    // do nothing
}


void Ambitus::visitChord(const Chord* e)
{
    assert(e);
    for (NoteBag::const_iterator i = e->cbegin(); i != e->cend(); ++e)
    {
        const Note* n = *i;
        assert(n);
        add(n->pitch());
    }
}


void Ambitus::visitTrill(const Trill* e)
{
    assert(e);
    add(e->pitch());
    add(e->pitch2());
}


void Ambitus::print(std::ostream& o) const
{
    o << "low=" << _low;
    o << " high=" << _high;
}


std::ostream& operator<<(std::ostream& o, const Ambitus& a)
{
    a.print(o);
    return o;
}


} // end namespace ScoreModel


/**
 * Model of a part
 *
 * @author Philippe Rigaux
 */


#ifndef Part_hpp
#define Part_hpp
/// @addtogroup scoremodel
/// @{


#include <stdio.h>
#include <utility>
#include <string>

#include "MTU.hpp"
#include "SMVisitor.hpp"
// musical model stuff
#include "Voice.hpp"
// engraving model stuff
#include "StaffDef.hpp"
#include "Measure.hpp"
#include "Note.hpp"
#include "Chord.hpp" // NoteComp
#include "GraceChord.hpp"
#include "Trill.hpp" 


/// A part is a set of monophonic voices,
/// to be played by a single instrument/performer.
namespace ScoreModel {

class Score;
class Node;


/// @see definition SM_MEASURE_STATIC in Voice.hpp
#ifdef SM_MEASURE_STATIC
typedef std::map<int, Voice> Wozzeck; // VoiceSeq
#else
typedef std::map<int, Voice*> Wozzeck;
#endif

#ifdef SM_MEASURE_STATIC // Store objects (not pointers)
typedef std::vector<Measure> MeasureSeq;
#else
typedef std::vector<Measure*> MeasureSeq;
#endif

typedef std::vector<StaffDef*> StaffSeq;


class Score;
class Pitch;
class AtomicEvent;


class Part
{

public:
	/// Main constructor. Builds an empty part
    /// @param score container score.
    /// @param name part's name. if empty, a new name is created.
    Part(Score& score, const std::string& name = std::string());
    
    virtual ~Part();
    
    /// part's name (unique)
    inline std::string id() const { return _id; }

    /// score containing this part
    inline Score& score() const { return _container; }

    inline const Ambitus& ambitus() const { return _ambitus; }

    virtual void accept(SMVisitor*) const;
    
    virtual void print(std::ostream& o) const;

    /**
     *
     *    Musical stuff: Voices
     *
     */
    
    /// number of voices.
    size_t nbVoices() const;

	// @brief Get a voice from its name
    // @param voiceName the name of the voice to find. must not be empty.
    // @return a pointer to the voice of name voiceName in this part
    // or NULL if the voice is not found.
    //Voice* voice(const std::string voiceName);

    /// @brief Get a voice from its number.
    /// @param i the voice number.
    /// @return the voice of number i in this part.
    /// if it does not exist, a new empty voice is create and added.
    Voice& voice(int i);
    
    /// @brief create an empty voice of number i.
    /// @param i the voice number.
    /// @return the voice of number i in this part.
    Voice& newVoice(int i);
    
    // add the given event to the voice of given number
    // @param v voice number. must be positive or null.
    // @param e an atomic event to add to the part.
    // void add(int v, const AtomicEvent* e);

    
    // Add a voice
    //void addVoice(Voice& voice);

    // Allocate a new empty voice in this part.
    // @param start start date for new voice, in MTU
    // @param name XML id. This parameter is optional; if not set,
    // a unique name is created from this part's name.
    // @return a pointer to the new voice.
    //Voice* newVoice(mtu_t start=MTU::ZERO, std::string name=std::string());
    
    /// Scan the voices in this part
    //inline VoiceSeq getVoices() { return _voices; };
    inline Wozzeck::iterator begin()
    { return _voices.begin(); }

    inline Wozzeck::iterator end()
    { return _voices.end(); }

    inline Wozzeck::const_iterator cbegin() const
    { return _voices.cbegin(); }

    inline Wozzeck::const_iterator cend() const
    { return _voices.cend(); }
    
    /// merge all the voices of this part into one single voice.
    /// @param id a voice number for the new voice.
    /// @return a new voice containing non-const pointers to the notes
    /// in the voices of this part, inclusing the notes contained in chords.
    Voice* flattening(int id);

    
    /**
     *
     *    Engraving stuff: Measures
     *
     */

    /// number of measures in this part.
    size_t nbMeasures() const;

    /// measure of given number in this part,
    /// newly created if it does not exist.
    /// @param n measure number.
    /// @param ts parameter time signature for the creation of a new measure.
    /// @param ks parameter key signature for the creation of a new measure.
    /// @return the measure associated to the given part in this stack
    /// or, if there is none, a newly created empty measure.
    Measure& measure(size_t n,
                     const MeterSig* ts = NULL,
                     const KeySig* ks = NULL);

    /// create a new empty measure and add it ar the end of this part.
    /// @param ts parameter time signature for the creation of a new measure.
    /// @param ks parameter key signature for the creation of a new measure.
    /// @return a newly created empty measure.
    Measure& newMeasure(const MeterSig* ts = NULL,
                        const KeySig* ks = NULL);

    /// measure of given number in this part.
    /// @return the measure associated to the given part in this stack
    /// @warning the measure must already be defined in this part.
    /// @todo a non const getMeasure?
    const Measure& getMeasure(size_t n) const;
    //Measure& get(const std::string& partname);
    
    
    /**
     *
     *    Engraving stuff: Staff numbers and Staff definitions
     *
     */
    
    /// @todo consider also storing the range of staff numbers (StaffRange) of the measures ?
    /// and check that it is in accordance with the nunmbers of the StaffDef ?
    
    /// number of staves for the engraving of this measure.
    size_t nbStaves() const;
    
    /// get the staff number n. It is created if necessary.
    /// @param n a staff number. must be positive or null.
    /// @param clef in case of creation of a new staff
    StaffDef& staff(int n, const Clef& clef = Clef(Clef::GTreble));
    
    /// Increase the number of staves for this part, if necessary,
    /// in order to be able to display a staff of given number.
    /// @param n a staff number. must be positive or null.
    /// @param clef for the new staff.
    void addStaff(int n, const Clef& clef = Clef(Clef::GTreble));

    /// add the given event to the voice number v and update the info of the staff number n.
    /// @param v a voice number. must be positive or null.
    /// @param n a staff number. must be positive or null.
    /// @param e an atomic event present in the part.
    /// The voice v or staff n is created if necessary.
    void add(int v, int n, AtomicEvent* e);

    /// get the staff number n.
    /// @param n a staff number. Must be the number of an existing staff
    /// (small than nb of staves).
    const StaffDef& getStaff(int n) const;


private:

    /// A part has a name
    std::string _id;

    /// A part belongs to a score
    Score& _container;

    /// Ambitus for the whole part
    Ambitus _ambitus;

    /// A part is a bag of voices.
    /// every voice is accessible from its number.
    Wozzeck _voices;
    
    /// engraving content of the Measures for this part.
    MeasureSeq _measures;
    
    /// engraving content : staff information (StaffDef)
    StaffSeq _staffdefs;
    
    /// measure associated to a part in stack.
    /// @param n measure number.
    /// @param fnew flag: the measure must be new in the stack
    /// (part not registered in this track).
    /// @param fnotnew flag: the measure must not be new in the stack
    /// (part registered in this stack).
    /// @param ts parameter time signature for the creation of a new measure.
    /// @param ks parameter key signature for the creation of a new measure.
    /// @return the measure associated to the given part in the stack,
    /// or, if there is none, a newly created empty measure.
    Measure& addMeasure(size_t n,
                        bool fnew, bool fnotnew,
                        const MeterSig* ts, const KeySig* ks);


    /// more detailed approach
    /// merger / sorter : merge all added atomic events into 1 voice of
    /// atomic event (sorted)
    /// voice flattener : all note from a voice
    
    /// intermediate structure for extracting all the notes in atomic events
    /// and sorting them by increasing date.
    // flatening the voices into single voice
    class Flattener : public SMVisitor
    {

    public:
        /// @brief new empty auxialiary sorter.
        /// @param part container part of the element that will be added.
        Flattener(Part& part);
        
        ~Flattener();

        /// add the fiven event
        void add(const AtomicEvent* e);
        
        ///  the set notes added
        ///  @param id the number of the new voice.
        ///  should not be a number of an existing voice of the part.
        /// @return a new voice containing all the notes in added elements,
        /// sorted by increasing date.
        Voice* getvoice(int id) const;
        
        /// dispatch on the difference types of atomic events
        virtual void visitNote(const Note*);
        virtual void visitGraceNote(const GraceNote*);
        virtual void visitRest(const Rest*);
        virtual void visitChord(const Chord*);
        virtual void visitGraceChord(const GraceChord*);
        virtual void visitTrill(const Trill*);
        
    private:
        
        /// The part to flatten
        Part& _part;
        
        /// temporary set of notes to be put in the new voice.
        struct NotePtrComp
        {
          bool operator() (const Note* lhs, const Note* rhs) const
            { assert(lhs); assert(rhs); return (lhs->date() < rhs->date()); }
        };

        std::set<const Note*, NotePtrComp> _auxbag;
        
    };
    
    

};


} // end namespace ScoreModel

/// @}

#endif /* Part_hpp */

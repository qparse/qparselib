//
//  Clef.hpp
//  qparse
//
//  Created by Florent Jacquemard on 02/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup scoremodel
/// @{

#ifndef Clef_hpp
#define Clef_hpp

#include <stdio.h>
#include "trace.hpp"

#include "Pitch.hpp"
#include "SMVisitor.hpp"

namespace ScoreModel {

/// @brief Representation of the clef associated with a staff
/// @todo mv here Philippe function to infer clef from pitches
class Clef
{

public:
    
enum Kind {
    None,
    GTreble,       // second line
    FBass,         // fourth line
    CAlto,         // third  line
    CTenor,        // fourth line
    GSoprano,      // first  line, french violin
    CSoprano,      // first  line
    CMezzoSoprano, // second line
    CBaritone,     // fifth  line
    FBaritone,     // third  line
    FSubBass,      // fifth  line
    Percussion
};

    /// main constructor
    Clef(Kind kind, int octave = 0);

    /// construct a Clef appropriate for a voice with the given range.
    Clef(const Pitch& lowest_pitch, const Pitch& highest_pitch);

    Clef& operator=(const Clef& rhs);

    bool operator==(const Clef& rhs) const;


    /// @return the sign of this clef
    /// 'G', 'F', 'C', or 'N' (percussion) or 0 (none)
    char sign() const;
    
    /// @return the line number of this clef:
    /// 1 to 5 and 0 for no clef, percussion or in case or error.
    int line() const;

    /// @return the number of octaves by which this clef’s normal pitches
    /// should be transposed.
    int octave() const;

    virtual void accept(SMVisitor*) const;

    void print(std::ostream& o) const;
    
private:

    Kind _kind;
    
    /// optional number of octaves
    int _oct;
    
};

std::ostream& operator<<(std::ostream&, const Clef&);


//bool operator==(const Clef& lhs, const Clef& rhs);



} // end namespace ScoreModel

#endif /* Clef_hpp */

/// @} // end group scoremodel

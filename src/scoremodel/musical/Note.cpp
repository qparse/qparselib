// Implementation of Note functions

#include "Note.hpp"

namespace ScoreModel {

// pitched note
Note::Note(mtu_t start, mtu_t duration, Voice& voice, const Pitch& p):
AtomicEvent(start, duration, voice),
_pitch(p)
{ }


Event* Note::clone()
{
    return new Note(date(), duration(), voice(), pitch());
}


bool Note::equal(const Note& p) const
{
    return ((date() == p.date()) &&
            (duration() == p.duration()) &&
            (_pitch == p._pitch));
}


bool Note::smaller(const Note& p) const
{
    return (_pitch < p.pitch());
}


void Note::namePitch(char n, float alt, int oct, bool altprint)
{    
    _pitch.rename(n, alt, oct, altprint);
}


void Note::accept(SMVisitor* v) const
{
    v->visitNote(this);
}


void Note::accept(AEVisitor* v)
{
    v->visitNote(this);
}


void Note::print(std::ostream& o) const
{
    printOnset(o);
    o << ":";
    o << "Note " << _pitch << " ";
    o << "dur=" << _duration;
}


std::ostream& operator<<(std::ostream& o, const Note& e)
{
    e.print(o);
    return o;
}


}

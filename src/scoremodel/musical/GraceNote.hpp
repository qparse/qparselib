//
//  GraceNote.hpp
//  qparse
//
//  Created by Florent Jacquemard on 18/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//


#ifndef SM_GraceNote_hpp
#define SM_GraceNote_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>

#include "Pitch.hpp"
#include "Note.hpp"
#include "AEVisitor.hpp"
#include "AEVisitor.hpp"

namespace ScoreModel {

/// a grace-note is a pitched note with a null MTU duration.
/// @todo pb id of grace note and note non unique. change it at creation.
class GraceNote : public Note
{
public:

    GraceNote(mtu_t start, Voice& voice, const Pitch& p);

    virtual ~GraceNote() {}
    
    virtual std::string name() const { return std::string("_gnote_"); }

    virtual bool isGraceNote() const { return true; }

    virtual void accept(SMVisitor*) const;
    virtual void accept(AEVisitor*);

    virtual void print(std::ostream& o) const;

};

std::ostream& operator<<(std::ostream&, const GraceNote&);

} // end namespace ScoreModel

/// @}

#endif /* SM_GraceNote_hpp */

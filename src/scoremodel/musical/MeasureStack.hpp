/**
 * MeasureStack
 *
 * @author Philippe Rigaux
 */


#ifndef SM_MeasureStack_hpp
#define SM_MeasureStack_hpp

#include <stdio.h>
#include <utility>
//#include <unordered_map>
#include <map>
#include <tuple>
#include <string>

#include "Event.hpp"
//#include "Score.hpp"
//#include "Part.hpp"
#include "MeasureStacked.hpp"
#include "SMVisitor.hpp"


namespace ScoreModel {

class Score;
class Part;
class Node;

/// @see definition SM_MEASURE_STATIC in Voice.hpp
#ifdef SM_MEASURE_STATIC // Store objects (not pointers)
typedef std::vector<MeasureStacked> MeasureStackedSeq;
#else
typedef std::vector<MeasureStacked*> MeasureStackedSeq;
#endif


/// An measure stack is a container with the engraving info
/// of one measure (fixed duration of 1 musical mtu) for each part.
class MeasureStack : public Event
{

public:

    /// Main constructor.
    /// @param n the number of this measure in the stack.
    /// @param parent the score containing this measure stack.
    MeasureStack(size_t n, Score& parent);

    virtual ~MeasureStack();

    virtual std::string name() const { return std::string("bars_"); }
    
    virtual bool isMeasureStack() const { return true; }
    
    /// Measure number in score
    /// @return the measure number
    /// @warning it is also the starting date of the measure
    /// @warning the string id of the measure is accessible under Event.id()
    inline long nb() const { return _nb; }     //long nb() const;
        
    /// Get the score container of this measure stack.
    inline Score& score() const { return _container; }

    /// previous measure stack in embedding score.
    MeasureStack* previous() const;
    
    inline bool empty() const { return _content.empty(); }

    /// measure associated to the given part in this stack,
    /// or a newly created empty measure if there is none.
    /// @param p associated part.
    /// @param ts parameter time signature for the creation of a new measure.
    /// @param ks parameter key signature for the creation of a new measure.
    /// @return the measure associated to the given part in this stack
    /// or, if there is none, a newly created empty measure.
    MeasureStacked& ofPart(Part& p,
                           const MeterSig* ts = NULL,
                           const KeySig* ks = NULL);

    /// create a new empty measure and associate it to the given part in this stack.
    /// @param p associated part.
    /// @param ts parameter time signature for the creation of a new measure.
    /// @param ks parameter key signature for the creation of a new measure.
    /// @return a newly created empty measure associated to the given part in this stack.
    /// @warning there must be no measure already associated to
    /// the given part in this stack.
    MeasureStacked& newPart(Part& p,
                            const MeterSig* ts = NULL,
                            const KeySig* ks = NULL);

    /// measure associated to the given part in this stack.
    /// @param p corresponding part.
    /// @return the measure associated to the given part in this stack
    /// @warning the measure must already be defined in this stack.
    MeasureStacked& get(Part& p);
    MeasureStacked& get(const std::string& partname);

    size_t size() const { return _content.size(); }
    
    /// Iterator to scan measures
    inline MeasureStackedSeq::iterator begin()
    { return _content.begin(); }

    inline MeasureStackedSeq::iterator end()
    { return _content.end(); }

    inline MeasureStackedSeq::const_iterator cbegin() const
    { return _content.cbegin(); }

    inline MeasureStackedSeq::const_iterator cend() const
    { return _content.cend(); }

    virtual void accept(SMVisitor*) const;

    virtual void print(std::ostream& o) const;

private:

    /// measure number, should be unique for a score.
    size_t _nb;

    // a list of voices intersecting this measure for each part name
    //std::map<std::string, std::vector<Voice*>> _index;
    
    /// engraving content = one Measure for each part
    MeasureStackedSeq _content;

    /// engraving content.
    /// One Measure for each part name.
    /// The measure is stored as it index in _content.
    std::map<std::string, size_t> _index; // _content;

    /// parent
    Score& _container;
    
    //MeasureStack* _prev;
    
    /// measure associated to a part in stack.
    /// @param p associated part.
    /// @param fnew flag: the measure must be new in the stack
    /// (part not registered in this track).
    /// @param fnotnew flag: the measure must not be new in the stack
    /// (part registered in this stack).
    /// @param ts parameter time signature for the creation of a new measure.
    /// @param ks parameter key signature for the creation of a new measure.
    /// @return the measure associated to the given part in the stack,
    /// or, if there is none, a newly created empty measure.
    MeasureStacked& add(Part& p, bool fnew, bool fnotnew,
                        const MeterSig* ts, const KeySig* ks);

};


} // end namespace ScoreModel

#endif /* SM_MeasureStack_hpp */

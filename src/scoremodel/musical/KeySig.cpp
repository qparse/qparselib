//
//  KeySig.cpp
//  qparse
//
//  Created by Florent Jacquemard on 02/06/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "KeySig.hpp"

namespace ScoreModel {


KeySig::KeySig(int signature):
_sig(signature)
{
    assert(-7 <= signature);
    assert(signature <= 7);
}


KeySig::KeySig(const KeySig& ks):
_sig(ks._sig)
{ }




KeySig& KeySig::operator=(const KeySig& rhs)
{
    if(this != &rhs)
    {
        _sig = rhs._sig;
    }

    return *this;
}


bool KeySig::operator==(const KeySig& rhs) const
{
    return (this->_sig == rhs._sig);
}


size_t KeySig::sharps() const
{
    if (_sig > 0)
        return _sig;
    else
        return 0;
}


size_t KeySig::flats() const
{
        if (_sig < 0)
            return abs(_sig);
        else
            return 0;
}


void KeySig::accept(SMVisitor* v) const
{
    v->visitKeySig(this);
}


void KeySig::print(std::ostream& o) const
{
    if ( _sig == 0 )
        o << "0";
    else if ( _sig > 0 )
        o << sharps() << "#";
    else
        o << flats() << "b";
}


std::ostream& operator<<(std::ostream& o, const KeySig& ks)
{
    ks.print(o);
    return o;
}


} // end namespace ScoreModel


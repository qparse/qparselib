//
//  Note.hpp
//  qparse
//
//  Created by Florent Jacquemard on 18/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef SM_Note_hpp
#define SM_Note_hpp
/// @addtogroup scoremodel
/// @{


#include <stdio.h>

#include "Pitch.hpp"
#include "AtomicEvent.hpp"
#include "AEVisitor.hpp"

namespace ScoreModel {


/// Pitched note in a voice
class Note : public AtomicEvent
{
public:

    Note(mtu_t start, mtu_t duration, Voice& voice, const Pitch& p);

    virtual ~Note() {}
        
    /// compare the date, duration and pitches of this note and the given note
    bool equal(const Note&) const;

    /// compare the pitch of this note and the given note
    bool smaller(const Note&) const;
    
    virtual std::string name() const { return std::string("_note_"); }

    virtual Event* clone();

    virtual bool isNote() const { return true; }

    /// pitch of this note.
    /// not const : pitch can be named afterwards.
    inline const Pitch& pitch() const { return _pitch; }
    
    /// change the name of the pitch of this note.
    void namePitch(char n, float alt, int oct, bool altprint);

    virtual void accept(SMVisitor*) const;
    virtual void accept(AEVisitor*);

    virtual void print(std::ostream& o) const;

protected:
    
    Pitch _pitch;

};


// boolean operators

/// compare the date, duration and pitch of the two given notes
inline bool operator==(const Note& lhs, const Note& rhs)
{
    return lhs.equal(rhs);
}

/// compare the date, duration and pitch of the two given notes
inline bool operator!=(const Note& lhs, const Note& rhs)
{
    return !operator==(lhs,rhs);
}

/// compare the pitch of the two given notes
inline bool operator<(const Note& lhs, const Note& rhs)
{
    return (lhs.smaller(rhs));
}

/// compare the pitch of the two given notes
inline bool operator>(const Note& lhs, const Note& rhs)
{
    return  operator<(rhs,lhs);
}

/// compare the pitch of the two given notes
inline bool operator<=(const Note& lhs, const Note& rhs)
{
    return !operator> (lhs, rhs);
}

/// compare the pitch of the two given notes
inline bool operator>=(const Note& lhs, const Note& rhs)
{
    return !operator< (lhs,rhs);
}


std::ostream& operator<<(std::ostream&, const Note&);


} // end namespace ScoreModel

/// @}

#endif /* SM_Note_hpp */


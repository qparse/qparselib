//
//  Measure.cpp
//  squant2
//
//  Created by Florent Jacquemard on 14/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Measure.hpp"
#include "Score.hpp"
#include "MeterSig.hpp"
#include "KeySig.hpp"
#include "Local.hpp"


namespace ScoreModel {


Measure::Measure(Part& p,
                 size_t n,
                 const MeterSig* ts,
                 const KeySig* ks,
                 int staffmin, int staffmax):
Event(MTU::of_int(n), MTU::ONE, p.id()+name()+std::to_string(n)),
staves(staffmin, staffmax),
_content(), // 0 sequence
_part(p),
_backlinks(),
_newTS(ts != NULL),
_ts(_newTS?ts:( n == 0)? // defaultMeterSig
                (new MeterSig(score().meterSignature())): // copy default
                previous().meterSignature()),              // ptr
_newKS(ks != NULL),
_ks(_newKS?ks:((n == 0)? // defaultKeySig
               (new KeySig(score().keySignature())):      // copy default
               previous().keySignature()))               // ptr
{
    // @param nb measure number (MTU date of measure start).
    assert(_ts);
    assert(_ks);
    assert(staves.size() >= 1);
#ifdef SM_MEASURE_STATIC
    _content.resize(SM_MAX_SEQUENCE, Sequence(0, NULL, *this));
#endif
}


Measure::~Measure()
{
    TRACE("delete Measure {} part:{}", nb(), part().id());

    if (_newTS)
    {
        assert(_ts);
        delete _ts;
    }
    if (_newKS)
    {
        assert(_ts);
        delete _ks;
    }
    
#ifndef SM_MEASURE_STATIC
    for (Sequence* s : _content)
    {
        assert(s);
        delete s;
    }
#endif
    _content.clear();  // useless? vectors are deleted anyway
    
    for (const Local* link : _backlinks)
    {
        assert(link);
        delete link;
    }
}


long Measure::nb() const
{
    return MTU::measure(date());
}


Score& Measure::score() const
{
    return _part.score();
}


Part& Measure::part() const
{
    return _part;
}


/**
 * Navigation in the Measure Matrix
 */

bool Measure::first() const
{
    return (nb() == 0);
}

bool Measure::last() const
{
    return (nb() == _part.nbMeasures() - 1);
}


Measure& Measure::next() const
{
    size_t n = nb();
    assert(n < _part.nbMeasures());
    if (this->last()) // measure creation
    {
        Measure& m = _part.newMeasure();
        assert (m.nb() == n+1);
        return m;
    }
    else // measure extraction
    {
        return _part.measure(n+1); // no creation
    }
}


Measure& Measure::previous() const
{
    size_t n = nb();
    
    if (n == 0) // first()
    {
        ERROR("Measure : call of previous on first measure");
        return _part.measure(n);
    }
    else // measure extraction
    {
        assert(0 < n);
        assert(n - 1 < _part.nbMeasures());
        return _part.measure(n-1);
    }
}


Measure& Measure::goPart(Part& p) const
{
    size_t n = nb();
    return p.measure(n); // may create the measure
}


Measure& Measure::goPart(const std::string& partname) const
{
    // may create a new part with name partname
    Part& p = score().part(partname);
    return goPart(p);
}


/**
 * Sequences (voices)
 */


size_t Measure::nbVoices() const
{
    return _content.size();
}


/// Scan the Sequence (voices) in this Measure
SequenceSeq::iterator Measure::begin()
{
    return _content.begin();
}

SequenceSeq::iterator Measure::end()
{
    return _content.end();
}

SequenceSeq::const_iterator Measure::cbegin() const
{
    return _content.cbegin();
}

SequenceSeq::const_iterator Measure::cend() const
{
    return _content.cend();
}


Sequence* Measure::findVoice(int v) const
{
#ifdef SM_MEASURE_STATIC
    for (Sequence& s : _content)
    {
        if (s.voice() == v)
            return &s;
    }
#else
    for (Sequence* s : _content)
    {
        assert(s);
        if (s->voiceNb() == v)
            return s;
    }
#endif
    return NULL;
}


Sequence& Measure::voice(int v)
{
    assert(v >= 0);
    Sequence* seq = findVoice(v);
    
    // existing sequence
    if (seq != NULL)
    {
        return *seq;
    }
    // new empty sequence in measure
    else
    {
#ifdef SM_MEASURE_STATIC
        _content.emplace_back(v, *this);
        Sequence& ret = _content.back();
#else
        Sequence* s = new Sequence(v, *this);
        _content.push_back(s);
        Sequence& ret = *s;
#endif
        // sequence is empty, no staff numbers to add to this measure
        // assert(ret.staves().empty()); // always empty for new sequence!

        return ret;
    }
}


Sequence& Measure::addVoice(int v, Root* t)
{
    assert(v > 0);
#ifdef SM_MEASURE_STATIC
    _content.emplace_back(v, t, *this);
    Sequence& ret = _content.back();
#else
    Sequence* s = new Sequence(v, t, *this);
    _content.push_back(s);
    Sequence& ret = *s;
#endif
    // add to this measure the staff numbers for the sequence
    staves.merge(ret.staves());

    return ret;
}




/**
 * Staves
 */


// void Measure::addStaff(int n)
// {
    // assert(_staves > 0);
    // if (n >= _staves)
    // {
    //    _staves = n - 1;
    //    TRACE("MeasureStacked {}: new staff {}", nb(), n);
    //    part().addStaff(n);
    // }
//}


  /**
   * Locals (backlinks)
   */
  

size_t Measure::nbBacklinks() const
{
    return _backlinks.size();
}


const Local* Measure::backlink(size_t i) const
{
    assert(i < _backlinks.size());
    return _backlinks[i];
}


void Measure::addLink(const Local* d)
{
    assert(d);
    
    // add to the list of annotations
    _backlinks.push_back(d);

    // add the staves concerned with the local
    // const VPosition& vpos = d->vposition();
    staves.merge(d->vposition().staves());
}



/**
 * TS, KS and Clef
 */




//void MeasureStacked::setClef(const Clef& clef)
//{
//    _clef = clef;
//}

//bool MeasureStacked::changeClef() const
//{
//    if (nb() == 0)
//    {
//        return true;
//    }
//    else
//    {
//        assert(previous());
//        return (this->clef() == previous()->clef());
//    }
//}


//void MeasureStacked::setMeterSig(const MeterSig& ts)
//{
//    _ts = ts;
//}


//void MeasureStacked::setKeySig(const KeySig& key)
//{
//    _key = key;
//}



const MeterSig* Measure::defaultMeterSig(const MeterSig* ts) const
{
    if (ts != NULL)
        return ts;
    else if (nb() == 0)
        return new MeterSig(score().meterSignature()); // copy
    else
    {
        return previous().meterSignature();
    }
}


const KeySig* Measure::defaultKeySig(const KeySig* ks) const
{
    if (ks != NULL)
        return ks;
    else if (nb() == 0)
        return new KeySig(score().keySignature());
    else
    {
        return previous().keySignature();
    }
}


// dispatch
void Measure::accept(SMVisitor* v) const
{
    v->visitMeasure(this);
}


void Measure::print(std::ostream& o) const
{
    size_t n = _content.size();
    if (n == 0)
        o << "Measure: empty";
    else if (n == 1)
        o << "Measure: ";
    else   // (n > 1)
        o << "Measure (" << n << " voices):";

#ifdef SM_MEASURE_STATIC
    for (Sequence s : _content)
        o << s;
#else
    for (Sequence* s : _content)
    {
        assert(s);
        o << s;
    }
#endif
}


std::ostream& operator<<(std::ostream& o, const Measure& m)
{
    m.print(o);
    return o;
}


} // end namespace ScoreModel

//
//  Trill.hpp
//  qparse
//
//  Created by Florent Jacquemard on 18/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//


#ifndef SM_Trill_hpp
#define SM_Trill_hpp
/// @addtogroup scoremodel
/// @{


#include <stdio.h>

#include "Pitch.hpp"
#include "AtomicEvent.hpp"
#include "Note.hpp"
#include "AEVisitor.hpp"

namespace ScoreModel {

/// note with additional second pitch
class Trill : public Note
{
public:

    Trill(mtu_t start, mtu_t duration, Voice& voice,
          const Pitch& mainPitch, const Pitch& secondPitch);

    virtual ~Trill() {}

    virtual std::string name() const { return std::string("_trill_"); }

    virtual Event* clone() ;

    virtual bool isTrill() const { return true; }

    /// the second note in this trill.
    /// @return note is not const : pitch can be named afterwards.
    Note* second() const;
    
    /// pitch of the second note in this trill.
    const Pitch& pitch2() const;
    
    virtual void accept(SMVisitor*) const;
    virtual void accept(AEVisitor*);

    virtual void print(std::ostream& o) const;

protected:
    
    Note* _second;

};


std::ostream& operator<<(std::ostream&, const Trill&);


} // end namespace ScoreModel


/// @}


#endif /* SM_Trill_hpp */

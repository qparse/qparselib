//
//  Sequence.hpp
//  squanty
//
//  Created by Florent Jacquemard on 04/04/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#ifndef Sequence_hpp
#define Sequence_hpp
/// @addtogroup scoremodel
/// @{

#include <stdio.h>

#include "Event.hpp"
//#include "Node.hpp"
//#include "Measure.hpp"
#include "StaffRange.hpp"
#include "SMVisitor.hpp"


namespace ScoreModel {

class Measure;
class Node;
class Root;


/// A Sequence is a container with a fixed duration of 1 mtu,
/// containing the engraving info (engraving tree) of the events of
/// one homophonic voice within one measure.
/// see MNX `<sequence>` elements https://w3c.github.io/mnx/by-example/#voices
class Sequence : public Event
{

public:

    /// sequence with given content tree
    /// @param n voice number
    /// @param t engraving content of the measure for this voice (tree). it must be a complete tree.
    /// @param parent container measure of part.
    /// @todo parent useful ?
    Sequence(int n, Root* t, Measure& parent);

    /// sequence with content tree unset
    Sequence(int n, Measure& parent);

    virtual ~Sequence();
       
    virtual std::string name() const { return std::string("_voiceb_"); }

    virtual bool isSequence() const  { return true; }

    /// Measure number in score
    /// @return the measure number
    /// @warning it is also the starting date of the measure
    /// @warning the string id of the measure is accessible under Event.id()
    long measureNb() const;
    
    /// voice number
    /// @return the voice number associated to this sequence.
    inline int voiceNb() const { return _nb; }
    
    /// top node of the tree-content of the sequence
    inline Root* tree() const { return _content; }

    /// embedding measure    
    inline Measure& measure() { return _container; }
    
    /// set the tree content of the sequence
    /// @param t tree content of the sequence. must be allocated.
    /// will be deallocated with the sequence.
    /// @warning the content of this sequence must be unset.
    void add(Root* t);
    
    const StaffRange& staves() const;
    
    /// container
    inline Measure&  measure() const { return _container; }
        
    virtual void accept(SMVisitor*) const;

    virtual void print(std::ostream& o) const;
    
public:
    
    // range of  numbers of staves present in this sequence
    // StaffRange straves;

    // number of staves for the engraving of this sequence.
    // inline size_t nbStaves() const { return straves.size(); }
    
    // Augment the number of staves for this sequence, if necessary,
    // in order to be able to display a staff of given number.
    // @param nb a staff number. must be positive or null.
    // @warning also add to embedding measure
    // void addStaff(int nb);
    
private:

    /// Associated voice number
    int _nb;
    
    /// @brief engraving content.
    /// one engraving tree played during this measure.
    Root* _content;
    
    /// @todo parent useful ?
    Measure& _container;
        
};

} // end namespace ScoreModel

/// @}

#endif /* Sequence_hpp */

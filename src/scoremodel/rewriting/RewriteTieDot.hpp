//
//  RewriteTieDot.hpp
//  squant2
//
//  Created by Florent Jacquemard on 29/01/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup output
/// @{

#ifndef RewriteTieDot_hpp
#define RewriteTieDot_hpp

#include <stdio.h>

#include "trace.hpp"
#include "Inner.hpp"
#include "Tuplet.hpp"
#include "Dot.hpp"
#include "Tied.hpp"
#include "RewriteRule.hpp"


namespace ScoreModel {


/// rewrite rule for the replacement of tie by a dot when appropriate.
class RewriteTieDot : public RewriteRule
{
    RewriteTieDot();

    ~RewriteTieDot();

    /// the given node is the root of of tree matching the lhs of this rule.
    /// @param n head of the instance of the lhs in an engraving tree.
    virtual bool match(const Node* n) const;
        
protected:
    
    /// new node obtained by top-rewriting the given node.
    /// @param n head of the instance of the lhs in an engraving tree.
    /// It must match a Tie node.
    /// @return a newly allocated Dot object.
    /// @warning the returned node is a newly affected Dot.
    /// The given Tie node is deleted.
    virtual Node* rhs(Node* n) const;
    
    /// first case of tie to be casted to dot:
    /// k( ... x 2(tie .) ...)  where x is a leaf
    /// (tie or dot or note or chord or rest)
    bool match1(const Node* n) const;

    /// second case of tie to be casted to dot:
    /// k( ... 2(. x) tie  ...)  where x is a leaf
    bool match2(const Node* n) const;

};


} // end namespace ScoreModel

#endif /* RewriteTieDot_hpp */

/// @} // end group scoremodel

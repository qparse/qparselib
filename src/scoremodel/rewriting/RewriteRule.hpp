//
//  RewriteRule.hpp
//  squant2
//
//  Created by Florent Jacquemard on 29/01/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

/// @addtogroup output
/// @{

#ifndef RewriteRule_hpp
#define RewriteRule_hpp

#include <stdio.h>

#include "trace.hpp"
#include "MTU.hpp"
#include "SMVisitor.hpp"
#include "Node.hpp" // node of engraving tree
#include "Root.hpp"
#include "Tuplet.hpp"
#include "Decorated.hpp"
#include "Leaf.hpp"


namespace ScoreModel {


/// representation of a term rewriting rules with lhs and rhs,
/// to be applied to an engraving tree to transform it.
/// An engraving tree is a tree in the measures of an engraving model,
/// accessed by its root Node.
class RewriteRule
{
public:

    RewriteRule();

    ~RewriteRule();

    /// the given node is the root of of tree matching the lhs of this rule.
    /// @param n head of the instance of the lhs in an engraving tree.
    virtual bool match(const Node* n) const = 0;

    /// Apply this rewrite rule to the given node in an engraving tree.
    /// @param n node in engraving tree (rewriting position).
    /// @return whether some rewriting was effectively performed,
    /// NULL otherwise.
    /// @warning if this rule is effectively applied, the given
    /// node is deleted (but not its subnodes) and replaced in the
    /// embedding engraving tree by a newly allocated node.
    /// @warning should never be applied to a node of type Root.
    /// i.e. the given node must always have a parent.
    bool rewrite(Node* n) const;

    /// make a IO pass applying this rule to every node in the given tree.
    /// @param n node in engraving tree (rewriting position).
    /// @return whether at least one rewriting was effectively performed,
    bool reduce(Root* n) const;

    /// iterate IO pass of thie rule until no rewriting is possible.
    void iterate(Root* n) const;

protected:
    
    /// new node obtained by top-rewriting the given node.
    /// @param n head of the instance of the lhs in an engraving tree.
    /// It must match the lhs.
    /// @return the instance of the rhs of this rule obtained by rewriting
    /// the tree at n or NULL in case of failure.
    /// @warning the returned node is a newly allocated Node object,
    /// and all it content (sub-nodes) are newly allocated.
    /// The tree at node n can be deleted without affecting the returned tree.
    virtual Node* rhs(Node* n) const = 0;

    /// @warning n must not be a Root node.
    bool inner_reduce(Node* n) const;
    // bool inner_reduce(Tuplet* n);
    // bool inner_reduce(Decorated* n);
    // bool inner_reduce(Leaf* n);

};



} // end namespace ScoreModel

#endif /* RewriteRule_hpp */

/// @} // end group scoremodel

//
//  RewriteRule.cpp
//  squant2
//
//  Created by Florent Jacquemard on 29/01/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#include "RewriteRule.hpp"

namespace ScoreModel {


RewriteRule::RewriteRule()
{ }


RewriteRule::~RewriteRule()
{ }


bool RewriteRule::rewrite(Node* n) const
{
    assert(n);
    Inner* parent = n->parent();
    if (parent == nullptr) // Root
    {
        ERROR("Rewrite: No rewriting on Root nodes");
        return false;
    }

    if (match(n) == false)    // assert(match(n));
        return false;
    
    Node* r = this->rhs(n);
    if (r == nullptr)
    {
        ERROR("Rewrite: NULL rhs of rule");
        return false;
    }
    
    bool ret = n->replace(r); // n replaced by r in its embedding tree.
    if (ret)
    {
        n->delete_tree(); // destroy the content of node n (rec. deletion of tree)
        delete n;         // destroyed the node n
    }
    
    return ret;
}


bool RewriteRule::reduce(Root* n) const
{
    assert(n);
    assert(n->complete());
    assert(n->size() == 1);
    return inner_reduce(n->sub(0));
}


bool RewriteRule::inner_reduce(Node* n) const
{
    assert(n);
    bool ret = false;
    assert(! n->isRoot());

    if (n->isTuplet())
    {
        for (size_t i = 0; i < n->size(); ++i)
        {
            ret = ret || inner_reduce(n->sub(i));
        }
        return ret || rewrite(n);
    }
    else if (n->isDecorated())
    {
        Decorated* d = dynamic_cast<Decorated*>(n);
        assert(d);
        return rewrite(d->decorated());
    }
    else if (n->isLeaf())
    {
        return rewrite(n);
    }
    else // should not happen
    {
        ERROR("");
        return false;
    }
}


void RewriteRule::iterate(Root* n) const
{
    assert(n);
    bool cont = true;
    while (cont)
    {
        cont = reduce(n);
    }
}


} // end namespace ScoreModel

//
//  RewriteTieDot.cpp
//  squant2
//
//  Created by Florent Jacquemard on 29/01/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#include "RewriteTieDot.hpp"


namespace ScoreModel {


RewriteTieDot::RewriteTieDot():
RewriteRule()
{ }


RewriteTieDot::~RewriteTieDot()
{ }



bool RewriteTieDot::match(const Node* n) const
{
    return (match1(n) || match2(n));
}


// pattern: `k( ... x 2(tie .) ...)` where `x` is a leaf
// checking the match starting from the leaf `tie`
bool RewriteTieDot::match1(const Node* n) const
{
    assert(n); // `n` = leaf `tie` in the pattern
    assert(n->complete());

    if (! n-> isTie())
        return false;
    
    Inner* up = n->parent(); // `up` = node `2` in the pattern
    
    if ((up == NULL) || (! up->isTuplet()))
        return false;
    assert(up->complete());
   
    if (n != up->sub(0)) // `n` first child of `2` i.e. try n->index() == 0
        return false;

    Node* pu = up->prevSibling(); // previous uncle `pu` = node `x` in the pattern
    if (pu == NULL)
        return false;

    if (! pu->isLeaf())
        return false;
    
    return (pu->duration() == 2*n->duration()); // = definition of dot
}


// pattern: `k( ... 2(. x) tie  ...)`  where `x` is a leaf
// checking the match starting from the leaf `tie`
bool RewriteTieDot::match2(const Node* n) const
{
    assert(n); // `n` = leaf `tie` in the pattern
    assert(n->complete());

    if (! n-> isTie())
        return false;
    
    Node* ps = n->prevSibling(); // previous sibling `ps` = node `2` in the pattern
    
    if ((ps == NULL) || (! ps->isTuplet()))
        return false;
    assert(ps->complete());
    size_t m = ps->size();
    if (m <= 1)
        return false;

    Node* main = ps->sub(m-1); // last chold `main` = node `x` in pattern
    if (main == NULL)
        return false;

    if (! main->isLeaf())
        return false;
    
    return (n->duration() == 2*main->duration()); // inverse definition of dot
    // in score model, the dot is always after the extended note.
}


Node* RewriteTieDot::rhs(Node* n) const
{
    assert(n);
    if (! n-> isTie())
    {
        ERROR("RewriteTieDot: matching error");
        return NULL;
    }
    
    Tied* c = dynamic_cast<Tied*>(n);
    if (c == NULL)
        return NULL;
    Dot* d = new Dot(*c);
    delete c;
    
    // copy pf leaf, with same date, same duration, and same parent.
    return d;
}


} // end namespace ScoreModel

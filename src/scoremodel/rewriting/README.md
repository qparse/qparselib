# src/scoremodel/rewriting



This group contains facilities for transforming one score model into another score model, 
following term rewriting rules applied in an innermost-outermost fashion to the engraving trees of the model.

The following rules are proposed:

- Tie to Dot, case 1

```mermaid
graph TD
A(k) --- B(...)
A --- X(x)
A --- D(2)
A --- C(...)
D --- T(tie)
D --- O(.)


A2(k) --- B2(...)
A2 --- X2(x)
A2 --- D2(2)
A2 --- C2(...)
D2 --- T2(dot)
D2 --- O2(.)
```

where `x` is a leaf (tie or dot or note or chord or rest)

- Tie to Dot, case 2

```mermaid
graph TD
A(k) --- B(...)
A --- D(2)
D --- O(.)
D --- X(x)
A --- T(tie)
A --- C(...)

A2(k) --- B2(...)
A2 --- D2(2)
D2 --- O2(.)
D2 --- X2(x)
A2 --- T2(dot)
A2 --- C2(...)
```

where `x` is a leaf (tie or dot or note or chord or rest)



- Staccato

```mermaid
graph TD
A(k) --- X(x)
A --- R1(r1)
A --- B(...)
A --- Rn(rn)

   

X2(x')
```

where
- `k` is a tuplet node
- `x` is a note or chord leaf
- `r1` ... `rn` are rest leaves,
- `x'` has a Staccato directive.


//
//  RewriteStaccato.hpp
//  squant2
//
//  Created by Florent Jacquemard on 01/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

/// @addtogroup output
/// @{

#ifndef RewriteStaccato_hpp
#define RewriteStaccato_hpp

#include <stdio.h>

#include "trace.hpp"
#include "Inner.hpp"
#include "Tuplet.hpp"
#include "LeafRest.hpp"
#include "LeafNote.hpp"
#include "LeafChord.hpp"
#include "Articulation.hpp"
#include "RewriteRule.hpp"



namespace ScoreModel {


/// rewrite rule for the replacement of tie by a dot when appropriate.
class RewriteStaccato : public RewriteRule
{
    
    /// @param mark whether the note obtained by rewriting must have
    /// staccato mark or not (in the second case, we just remove rests).
    RewriteStaccato(bool mark = true);

    ~RewriteStaccato();
    
    /// the given node is the root of of tree matching the lhs of this rule:
    /// `k(x r1 ... rn)`  where
    /// - k is a tuplet node
    /// - x is a note or chord leaf
    /// - r1 ... rn are rest leaves
    /// @param n head of the instance of the lhs in an engraving tree.
    virtual bool match(const Node* n) const;
        
protected:
    /// add a staccato mark or not
    bool _mark;
    
    /// new node obtained by top-rewriting the given node.
    /// @param n head of the instance of the lhs in an engraving tree.
    /// It must match a tuplet node.
    /// @return a newly allocated Dot object.
    /// @warning if the leaf `x` is a chord, it must be non-empty.
    /// @warning the returned node is a newly allocated Note or Chord node.
    /// All the given node n in and all it subtrees are deleted.
    virtual Node* rhs(Node* n) const;
    
};
     
} // end namespace ScoreModel

#endif /* RewriteStaccato_hpp */

/// @} // end group scoremodel

//
//  RewriteStaccato.cpp
//  squant2
//
//  Created by Florent Jacquemard on 01/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#include "RewriteStaccato.hpp"



namespace ScoreModel {


RewriteStaccato::RewriteStaccato(bool mark):
RewriteRule(),
_mark(mark)
{ }


RewriteStaccato::~RewriteStaccato()
{ }


// pattern: `k(x r1 ... rn)` where `k` tuplet, `x` is a leaf, `r1`... rests
// checking the match starting from the root
bool RewriteStaccato::match(const Node* n) const
{
    assert(n); // `n` = root nodel `k` in the pattern
    assert(n->complete());

    if (! n-> isTuplet())
        return false;

    if (n->size() < 2)
        return false;

    Node* fs = n->sub(0); // first child `fs` = node `x` in pattern
    if (fs == NULL)
    {
        ERROR("RewriteStaccato: NULL subtree");
        return false;
    }

    if (! (fs->isLeaf() && (fs->isNote() || fs->isChord())))
        return false;

    // all other children are rests
    for (size_t i = 1; i < n->size(); ++i)
    {
        assert(n->sub(i)); // node `ri` in pattern
        if (! n->sub(i)->isRest())
            return false;
    }
    
    return true;
}


Node* RewriteStaccato::rhs(Node* n) const
{
    assert(n);
    assert(match(n));

    Node* fs = n->sub(0);
    Leaf* r = NULL;
    int staff = 0;
    
    if (fs->isNote())
    {
        // alt: use dispatch with Visitor pattern
        LeafNote* ln = dynamic_cast<LeafNote*>(n);
        assert(ln); // cast should be succesful
        // copy of leaf,
        r = new LeafNote(*ln);
        staff = ln->staff();
    }
    else if (fs->isChord())
    {
        // alt: use dispatch with Visitor pattern
        LeafChord* lc = dynamic_cast<LeafChord*>(n);
        assert(lc); // cast should be succesful
        // copy of leaf,
        r = new LeafChord(*lc);
        assert(! lc->empty());
        staff = lc->treble().staff();
    }
    else
    {
        ERROR("RewriteStaccato: matching error");
        return NULL;
    }
    assert(r);
    // same date, same duration, and same parent as redex root.
    r->setDate(n->date());
    r->setDuration(n->duration());
    r->setParent(n->parent());

    bool ret = n->replace(r); // n replaced by r in its embedding tree.
    if (ret)
    {
        n->delete_tree(); // destroy the content of node n (rec. deletion of tree)
        delete n;         // destroyed the node n
    }
    else
    {
        ERROR("RewriteStaccato: error in replacement");
        delete r;
        return NULL;
    }

    // add Local Articulation located on r
    if (_mark)
    {
        VPosition vpos = VPosition(staff, VPosition::Orientation::Above);
        const Articulation* art =
        new Articulation(vpos, r, // link to event marked staccato
                         Articulation::Kind::Staccato);
        r->measure().addLink(art);
    }
       
    return r;
}


} // end namespace ScoreModel

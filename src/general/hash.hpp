//
//  hash.hpp
//  squanty
//
//  Created by Florent Jacquemard on 13/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup general
/// @{

#ifndef hash_hpp
#define hash_hpp

#include <stdio.h>
#include <assert.h>
#include <math.h> // fabs


/// Hash functions
// see http://www.eternallyconfuzzled.com/tuts/algorithms/jsw_tut_hashing.aspx
// see also Josh Bloch, Effective Java
// and also
// http://stackoverflow.com/questions/17016175 // define hash for objects of user classes
//                                                Bernstein with seed=17,   factor=31
//                                                and below with seed=1009, factor=9176;
// https://stackoverflow.com/a/1646913/6930643 // Bernstein with seed=17, factor=31

#define HASH_SEED    seed_Bernstein
#define REHASH(h, x) rehash_Bernstein(h, x)
#define POSTHASH(h)  h

/// hash function parameters
//extern long HASH_SEED;   //constexpr int HASH_SEED = 17;
extern long HASH_FACTOR;   //constexpr int HASH_FACTOR = 31;

/// Bernstein hash
extern std::size_t seed_Bernstein;
extern std::size_t rehash_Bernstein(std::size_t h, std::size_t x);

/// modified Bernstein with XOR
extern std::size_t seed_BernsteinXOR;
extern std::size_t rehash_BernsteinXOR(std::size_t h, std::size_t x);

/// Shift-Add-XOR hash (originaly for strings)
extern std::size_t seed_ShiftAddXOR;
extern std::size_t rehash_ShiftAddXOR(std::size_t h, std::size_t x);

/// Fowler/Noll/Vo
/// see http://www.isthe.com/chongo/tech/comp/fnv/
extern std::size_t seed_FNV;
extern std::size_t rehash_FNV(std::size_t h, std::size_t x);

extern std::size_t seed_One_at_a_Time;
extern std::size_t rehash_One_at_a_Time(std::size_t h, std::size_t x);
extern std::size_t rehash_One_at_a_Time(std::size_t h);



#endif /* hash_hpp */

/// @}

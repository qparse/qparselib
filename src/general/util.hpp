//
//  util.hpp
//  qparse
//
//  Created by Florent Jacquemard on 19/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup general
/// @{


#ifndef util_hpp
#define util_hpp

#include <stdio.h>
#include <string>
#include <tgmath.h>
#include <limits.h>
//#include <ctype.h>
//#include <iostream>
#include <assert.h>
#include <cfloat>  // DBL_EPSILON

#include "trace.hpp"


/// precision for floting point unit calculations
extern double CST_PRECISION;

/// return true if the difference between a and b
/// is less than absEpsilon (near zero case),
/// or within relEpsilon percent of the larger of a and b.
extern bool DBL_APPROX_EQUAL(double a, double b,
                             double absEpsilon = CST_PRECISION,
                             double relEpsilon = DBL_EPSILON);


#if QP_PLATFORM == PLATFORM_LINUX
/// patch for a bug in g++
/// see https://stackoverflow.com/questions/12975341/to-string-is-not-a-member-of-std-says-g-mingw
namespace patch
{
    template < typename T > std::string to_string(const T& n)
    {
        std::ostringstream os;
        os << n;
        return os.str();
    }
}
#endif


class util
{
public:
    
    static double duration(clock_t start);
    
    /// string parsing utility: skip spaces.
    /// @param it iterator inside parsed string.
    /// @param beg iterator to begining of parsed string.
    /// @param end iterator to end of parsed string.
    static void read_spaces(std::string::const_iterator& it,
                            const std::string::const_iterator& beg,
                            const std::string::const_iterator& end);

    
    /// string parsing utility.
    /// parse a positive int.
    /// @param it iterator to parsed string must point to a digit character.
    /// @param beg iterator to begining of parsed string.
    /// @param end iterator to end of parsed string.
    static long read_int(std::string::const_iterator& it,
                         const std::string::const_iterator& beg,
                         const std::string::const_iterator& end);
    
    
    /// string parsing utility: parse a positive float.
    /// @param it iterator to parsed string must point to a digit character or a dot.
    /// @param beg iterator to begining of parsed string
    /// @param end iterator to end of parsed string
    static double read_float(std::string::const_iterator& it,
                            const std::string::const_iterator& beg,
                            const std::string::const_iterator& end);
    
    
    /// string parsing utility: parse the given character.
    /// @return true iff the given character was succesfully read.
    /// In that case it is advanced to the next character.
    static bool read_char(const char& c,
                          std::string::const_iterator& it,
                          const std::string::const_iterator& beg,
                          const std::string::const_iterator& end);

    
    /// string filename utility: extract suffix.
    /// @param filename name of file in the form prefix.suffix
    /// @return the suffix from the given filename, converted to uppercase.
    static std::string suffix(std::string filename);

    /// string filename utility: extract prefix.
    /// @param filename name of file in the form prefix.suffix
    /// @return the prefix from the given filename

    static std::string prefix(std::string filename);

//    static inline std::string to_string(size_t a);
    /// string conversion
    static inline std::string to_string(long a)
    {
#if QP_PLATFORM == PLATFORM_DARWIN
        return std::to_string(a);
#elif QP_PLATFORM == PLATFORM_CYGWIN
        return std::to_string(a);
#elif QP_PLATFORM == PLATFORM_LINUX
        return patch::to_string(a);
#elif QP_PLATFORM == PLATFORM_FREEBSD
        return std::to_string(a);
#else
#error "unknown PLATFORM"
#endif
    }
    
    /// integral lower approximation (floor) of log2
    /// = number of beams for given arity.
    /// @param x must not be null.
    static long trunclog2(unsigned long x);

    /// given integer is a power of 2.
    static bool ispow2(unsigned long x);
 
};

#endif /* util_hpp */

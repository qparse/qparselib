# src/general
Misc classes:


* ini : project constants and parameters. Some are read from a `.ini` file, based on the C++ header only [version](https://github.com/jtilly/inih) of the library [inih](https://github.com/benhoyt/inih).

* QPconfig : 
verification of compile variables.

* Rational numbers.

* tracing : based on the library [spdlog](https://github.com/gabime/spdlog) of Gabi Melman.


//
//  hash.cpp
//  squanty
//
//  Created by Florent Jacquemard on 13/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup general
/// @{


#include "hash.hpp"


// Hash functions
//long HASH_SEED   = 17;
long HASH_FACTOR = 31;
//long HASH_SEED   = 2166136261;   for FNV
//long HASH_FACTOR = 16777619; for FNV

// Bernstein hash
std::size_t seed_Bernstein = 17;
std::size_t rehash_Bernstein(std::size_t h, std::size_t x)
{
    return h * 31 + x;
}

// modified Bernstein with XOR
std::size_t seed_BernsteinXOR = 17;
std::size_t rehash_BernsteinXOR(std::size_t h, std::size_t x)
{
    return h * 31 ^ x;
}

// Shift-Add-XOR hash (originaly for strings)
std::size_t seed_ShiftAddXOR = 0;
std::size_t rehash_ShiftAddXOR(std::size_t h, std::size_t x)
{
    return h ^ ((h << 5) + (h >> 2) + x);
}

// Fowler/Noll/Vo
// for 32 bit int
std::size_t seed_FNV = 2166136261;
std::size_t rehash_FNV(std::size_t h, std::size_t x)
{
    return (h * 16777619) ^ x;
}

std::size_t seed_One_at_a_Time = 0;
std::size_t rehash_One_at_a_Time(std::size_t h, std::size_t x)
{
    h += x;
    h += (h << 10);
    h ^= (h >> 6);
    return h;
}

std::size_t rehash_One_at_a_Time(std::size_t h)
{
    h += (h << 3);
    h ^= (h >> 11);
    h += (h << 15);
    return h;
}




/// @}

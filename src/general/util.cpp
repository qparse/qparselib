//
//  util.cpp
//  qparse
//
//  Created by Florent Jacquemard on 19/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//


#include "util.hpp"


double CST_PRECISION = 1e-12;

// http://www.learncpp.com/cpp-tutorial/35-relational-operators-comparisons/
// https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
// https://docs.oracle.com/cd/E19957-01/806-3568/ncg_goldberg.html
bool DBL_APPROX_EQUAL(double a, double b,
                      double absEpsilon,
                      double relEpsilon)
{
    // Handle the near-zero case.
    const double diff = fabs(a - b);
    if (diff <= absEpsilon)
        return true;
    
    // Otherwise follow Knuth recommendation
    return diff <= ( (fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * relEpsilon);
    // TBC ALT ULPs distance see e.g. https://bitbashing.io/comparing-floats.html
}


double util::duration(clock_t start)
{
    return ((double)(clock() - start)/CLOCKS_PER_SEC * 1000);
}


void util::read_spaces(std::string::const_iterator& it,
                       const std::string::const_iterator& beg,
                       const std::string::const_iterator& end)
{
    while (it != end && std::isspace(*it))
        it++;
}


long util::read_int(std::string::const_iterator& it,
                    const std::string::const_iterator& beg,
                    const std::string::const_iterator& end)
{
    assert (isdigit(*it));
    long v = 0;
    while (it != end && isdigit(*it))
    {
        v = v * 10 + *it - '0';
        it++;
    }
    assert (v >= 0);
    return v;
}


double util::read_float(std::string::const_iterator& it,
                       const std::string::const_iterator& beg,
                       const std::string::const_iterator& end)
{
    assert (isdigit(*it) || (*it == '.'));
    double v = 0;

    // integral part
    while (it != end && isdigit(*it))
    {
        v = v * 10 + *it - '0';
        it++;
    }
    assert (v >= 0);
    
    // decimal part
    if (*it == '.')
    {
        it++;
        double p = 1;
        while (it != end && isdigit(*it))
        {
            v = v + (*it - '0')*pow(10.0, -p);
            it++;
            p = p + 1.0;
        }
    }
    
    return v;
}


bool util::read_char(const char& c,
                     std::string::const_iterator& it,
                     const std::string::const_iterator& beg,
                     const std::string::const_iterator& end)
{
    if ((it == end) || (*it != c))
        return false;
    else
    {
        it++;
        return true;
    }
}



std::string util::suffix(std::string filename)
{
    std::string suffix = "";
    
    if (filename.size() > 0) // empty string : no output
    {
        size_t dotindex = filename.find_last_of(".");
        //string prefix = filename.substr(0, dotindex);
        suffix = filename.substr(dotindex + 1);
        std::transform(suffix.begin(), suffix.end(), suffix.begin(), ::toupper);
    }
    
    TRACE("suffix of file {} is {}", filename, suffix);
    if (suffix.size() == 0)
        WARN("file {}: empty suffix", filename);
        
    return suffix;
}


std::string util::prefix(std::string filename)
{
    std::string prefix = "";
    
    if (filename.size() > 0) // empty string : no output
    {
        size_t dotindex = filename.find_last_of(".");
        prefix = filename.substr(0, dotindex-1);
    }
    
    TRACE("prefix of file {} is {}", filename, prefix);
    if (prefix.size() == 0)
        WARN("file {}: empty prefix", filename);
        
    return prefix;
}


/// unroll
/// for solutions in O(log(N)) or less operations,
/// see http://www.graphics.stanford.edu/~seander/bithacks.html
long util::trunclog2(unsigned long x)
{
    assert(x > 0);
    if (x == 0)
    {
        ERROR("uintlog2 of 0");
        return LONG_MIN;
    }
    if (x == 1) return 0;
    long c = x;
    long ret = 0;
    while (c > 1) // more than 1 bit remaining
    {
        c >>= 1;
        ++ret;
    }

    return ret;
}

/// from http://www.graphics.stanford.edu/~seander/bithacks.html
bool util::ispow2(unsigned long x)
{
    return ((x > 0) && ((x & (x - 1)) == 0));
}




//
//  ValueList.cpp
//  qparse
//
//  Created by Florent Jacquemard on 29/07/2017.
//  Copyright © 2017 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#include "ValueList.hpp"


// empty value list
ValueList::ValueList(const mtu_t len):
_continuation(MTU::ZERO),
_main(),
_length(len),
_accu(MTU::ZERO),
_nbevt(0),
_nbgn(0),
_status(0)
{
    assert (MTU::defined(len));
    assert (this->empty());
}


ValueList::ValueList(const DurationList& dl):
_continuation(dl._continuation),
_main(dl._main), // copy
_length(dl.length()),
_accu(_length),
_nbevt(0),
_nbgn(0),
_status(0)
{
    assert(MTU::defined(_continuation));
    assert(MTU::defined(_length));

    for (std::list<mtu_t>::const_iterator i = _main.cbegin();
         i != _main.cend();
         i++)
    {
        if (i->null())
            _nbgn++;
        else
            _nbevt++;
    }
}


ValueList::ValueList(const ValueList& rhs):
_continuation(rhs._continuation),
_main(rhs._main),
_length(rhs._length),
_accu(rhs._accu),
_nbevt(rhs._nbevt),
_nbgn(rhs._nbgn),
_status(rhs._status)
{
    assert(MTU::defined(rhs._continuation));
    assert(MTU::defined(rhs._length));
    assert(MTU::defined(rhs._accu));
}


// sublist
ValueList::ValueList(const ValueList& vl, const mtu_t d, const mtu_t len):
_continuation(MTU::ZERO),
_main(),
_length(MTU::infinite(len)?(vl.length() - d):len),
_accu(MTU::ZERO),
_nbevt(0),
_nbgn(0),
_status(0)
{
    assert (MTU::defined(d));
    // sublist start inside superlist
    assert(0 <= d);
    assert(d <= vl.length());
    assert (MTU::defined(len));
    // sublist end inside superlist (unless unbounded)
    assert(MTU::infinite(len) || (0 <= len));
    assert(MTU::infinite(len) || (d + len <= vl.length()));
    assert(d + _length <= vl.length());
    assert (this->empty());
       
    // start date d is inside the continuation of vl
    if (d <= vl._continuation)
    {
        // sublist completely contained in the continuation of vl.
        if (d + _length <= vl._continuation)
        {
            addcont(_length); // finished
        }
        else
        {
            addcont(vl._continuation - d);
            std::list<mtu_t>::const_iterator i = vl._main.cbegin();
            while ((i != vl._main.cend()) && (_accu + (*i) <= _length))
            {
                add(*i);
                ++i;
            }
            // not finished : complete with a part of current element in vl
            assert(_accu <= _length);
            if (_accu < _length)
            {
                mtu_t last = _length - _accu;
                assert(i != vl._main.cend());
                assert(last < *i);
                add(last); // finished
            }
        }
    }
    // date d is after the continuation of vl
    else
    {
        mtu_t cursor = vl._continuation;
        // advance in vl to the starting date d
        std::list<mtu_t>::const_iterator i = vl._main.cbegin();
        while ((i != vl._main.cend()) && (cursor + (*i) <= d))
        {
            cursor += *i;
            ++i;
        }

        assert(cursor <= d);
        if (cursor < d)
        {
            // mtu_t front = d - cursor;
            assert(i != vl._main.cend());
            // assert(front < *i);
            // addcont(*i - front);
            cursor += *i;
            assert(cursor > d);
            if (cursor - d >= _length)
            {
                addcont(_length);
                assert(_accu == _length);
                return;
            }
            else
            {
                addcont(cursor - d);
                ++i;
            }
        }
        
        while ((i != vl._main.cend()) && (cursor + (*i) <= d + _length))
        {
            add(*i);
            cursor += *i;
            ++i;
        }
        assert(cursor <= d + _length);
        if (cursor < d + _length)
        {
            assert(i != vl._main.cend());
            assert(cursor + (*i) > d + _length);
            add((d + _length) - cursor);
        }
        
    }
    assert(_accu == _length);
}


ValueList& ValueList::operator=(const ValueList& other)
{
    if (this != &other) // protect against invalid self-assignment
    {
        _continuation = other._continuation;
        _main.assign(other._main.begin(), other._main.end()); // copy of _main
        _length = other._length;
        _accu = other._accu;
        _nbevt = other._nbevt;
        _nbgn = other._nbgn;
        _status = other._status;
    }
    return *this;
}


bool ValueList::empty() const
{
    // _main.empty() && _continuation.null()
    return (_accu.null() && (_nbgn == 0));
}


//bool ValueList::unit() const
//{
//}


bool ValueList::complete() const
{
    return (_accu == _length);
}


bool ValueList::single_continuation() const
{
    return ((_continuation > MTU::ZERO) &&
            (_main.empty()));
}


bool ValueList::event() const
{
    return  (_continuation.null() &&
             (_nbevt == 1));
}


bool ValueList::single_event() const
{
    return  (event() && (_nbgn == 0));
}


// used only for terminal runs
void ValueList::add(mtu_t q)
{
    assert(MTU::defined(q));
    
    if (q.null())
        _nbgn++;
    else
        _nbevt++;

    _accu += q;
    assert(_accu <= _length);
    
    _main.push_back(q);
}


void ValueList::addcont(mtu_t q)
{
    assert (MTU::defined(q));
    assert (MTU::defined(_continuation));
    
    _accu += q;
    assert(_accu <= _length);

    _continuation += q;
}


mtu_t ValueList::front() const
{
    assert (_continuation.null());
    assert (! _main.empty());
    
    return _main.front();    
}


mtu_t ValueList::pop()
{
    assert (_continuation.null());
    assert (! _main.empty());

    mtu_t first = _main.front();
    if (first.null())
        _nbgn--;
    else
        _nbevt--;
    _accu -= first;
    _main.pop_front();
    
    return first;
}


mtu_t ValueList::popcont()
{
    mtu_t first = _continuation;

    _accu -= first;
    _continuation = MTU::ZERO;

    return first;
}


void ValueList::popcont(mtu_t q)
{
    assert (q <= _continuation);
    _accu -= q;
    _continuation -= q;
}


std::ostream& operator<<(std::ostream& o, const ValueList& l)
{
    o << '[';
    assert(MTU::defined(l._continuation));
    if (! MTU::null(l._continuation))
    {
        o << " " << -(l._continuation);
    }
    for(std::list<mtu_t>::const_iterator i = l._main.cbegin();
        i != l._main.cend();
        ++i)
    {
        assert (*i >= 0);
        o << " " << *i;
    }
    o << " ]";
    o << (l.complete()?"":"*");

    return o;
}


/// @}

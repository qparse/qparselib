//
//  AlignedInterval.cpp
//  qparse
//
//  Created by Florent Jacquemard on 07/02/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#include "AlignedInterval.hpp"
#include "IntervalHeap.hpp"


AlignedInterval::AlignedInterval(const InputSegment* s,
                                 mtu_t me,
                                 bool f_align):
Interval(s, me),
_seg_llen(0),
_seg_loff(0),
_seg_lbeg(0),
_seg_rlen(0),
_seg_roff(0),
_seg_rbeg(0),
_seg_next(0),
_seg_first(0)
{
    assert(s);
    _seg_next = s->size(); // no next
    // assert(! ropen());
    assert(s->size() > 0);
    if (f_align)
    {
        align(s);
    }
    else
    {
        _seg_next = s->size();
    }
    assert(_check_first(s));
    assert(_check_next(s));
}


// internal use only
AlignedInterval::AlignedInterval(const InputSegment* s,
                                 rtu_t rb, rtu_t re,
                                 mtu_t mb, mtu_t me,
                                 size_t first):
Interval(s, rb, re, mb, me),
_seg_llen(0),
_seg_loff(0),
_seg_lbeg(0),
_seg_rlen(0),
_seg_roff(0),
_seg_rbeg(0),
_seg_next(0),
_seg_first(first)
{
    align(s);
    assert(_check_first(s));
    assert(_check_next(s));
}


// internal use only
AlignedInterval::AlignedInterval(const InputSegment* s,
                                 rtu_t rb, rtu_t re,
                                 mtu_t mb, mtu_t me,
                                 size_t first,
                                 size_t next):
Interval(s, rb, re, mb, me),
_seg_llen(0),
_seg_loff(0),
_seg_lbeg(0),
_seg_rlen(0),
_seg_roff(0),
_seg_rbeg(0),
_seg_next(next),
_seg_first(first)
{
    assert(_check_first(s));
    _seg_next = s->size();
}


AlignedInterval::AlignedInterval(const AlignedInterval& p):
Interval(p),
_seg_llen(p._seg_llen),
_seg_loff(p._seg_loff),
_seg_lbeg(p._seg_lbeg),
_seg_rlen(p._seg_rlen),
_seg_roff(p._seg_roff),
_seg_rbeg(p._seg_rbeg),
_seg_next(p._seg_next),
_seg_first(p._seg_first)
{ }


AlignedInterval& AlignedInterval::operator= (const AlignedInterval& p)
{
    if(this != &p)
    {
        Interval::operator=(p);
        _seg_llen = p._seg_llen;
        _seg_loff = p._seg_loff;
        _seg_lbeg = p._seg_lbeg;
        _seg_rlen = p._seg_rlen;
        _seg_roff = p._seg_roff;
        _seg_rbeg = p._seg_rbeg;
        _seg_next = p._seg_next;
        _seg_first = p._seg_first;
    };

    return *this;
}


bool AlignedInterval::operator==(const AlignedInterval& p) const
{
    if (! Interval::operator==(p))
        return false;
    
    // compare as interval (for table iheap)
    return true;

    //    return ((rbegin == p.rbegin) &&
    //            (rend   == p.rend)   &&
    //            (mbegin == p.mbegin) &&
    //            (mend   == p.mend));
}


size_t AlignedInterval::align(const InputSegment* s, size_t b)
{
    _seg_first = b;
    return align(s);
}


// we assume rbegin and rend are set and not RTU::UNBOUNDED.
size_t AlignedInterval::align(const InputSegment* s)
{
    assert(rbegin() != RTU::UNBOUNDED);
    assert(rend()   != RTU::UNBOUNDED);
    assert(s);
    // don't recompute alignment
    if (aligned())
    {
        WARN("align called twice on the same interval {}", *this);
        assert(_check_next(s));
        return _seg_next;
    }
    // the realtime begin date of this interval
    // is not necessarly in the input segment bounds.
    assert(s->rbegin() <= rbegin());
    // assert(rbegin <= s->rend());

    // correctness of seg_first
    assert(_check_first(s));
    
    size_t segend = s->size();
    rtu_t rlen = rend() - rbegin();
    rtu_t rmid = rbegin() + (rlen / 2.0);
    int c;
    int coff = 0;
    size_t i = _seg_first;
    
    // count # segment points from b on in the first half of Interval
    for(c=0; (i < segend) && (s->point(i).rdate() < rmid); i++)
    {
        c++;
        if (s->offset(i)) { coff++; }
    }
    
    _seg_llen = c;
    _seg_loff = coff;
    // no segment points in first half of interval
    if (_seg_llen == 0)
        _seg_lbeg = segend;  // out_of_range (should not be used)
    else
        _seg_lbeg = _seg_first;       // rbegin <= rdate(b) < rmid
    
    // end of segment reached, no segment points in second half of interval
    if (i >= segend)
    {
        _seg_rlen = 0;
        _seg_roff = 0;
        _seg_rbeg = segend;
    }
    else
    {
        assert (s->point(i).rdate() >= rmid);
        _seg_rbeg = i;
        // count # segment points from i on in the second half of Interval
        coff = 0;
        for(c=0; (i < segend) && (s->point(i).rdate() < rend()); i++)
        {
            c++;
            if (s->offset(i)) { coff++; }
        }
        
        _seg_rlen = c;
        _seg_roff = coff;

        // no segment points in second half of interval
        if (_seg_rlen == 0)
            _seg_rbeg = segend;
    }
    
    // post-conds:
    assert (_seg_llen >= 0);
    assert (_seg_llen <= segend);
    assert (_seg_lbeg >= 0);
    
    assert ((_seg_llen >  0) || (_seg_lbeg >= segend));
    assert ((_seg_llen == 0) || (_seg_lbeg < segend));
    assert ((_seg_llen == 0) || (_seg_lbeg+_seg_llen-1 < segend));
    assert ((_seg_llen == 0) || (s->point(_seg_lbeg).rdate() >= rbegin()));
    assert ((_seg_llen == 0) || (s->point(_seg_lbeg).rdate() < rmid));
    assert ((_seg_llen == 0) || (s->point(_seg_lbeg+_seg_llen-1).rdate() >= rbegin()));
    assert ((_seg_llen == 0) || (s->point(_seg_lbeg+_seg_llen-1).rdate() < rmid));

    assert (_seg_loff >= 0);
    assert (_seg_loff <= _seg_llen);
    
    assert (_seg_rlen >= 0);
    assert (_seg_rlen <= segend);
    assert (_seg_rbeg >= 0);
    
    assert ((_seg_rlen >  0) || (_seg_rbeg >= segend));
    assert ((_seg_rlen == 0) || (_seg_rbeg < segend));
    assert ((_seg_rlen == 0) || (_seg_rbeg+_seg_rlen-1 < segend));
    assert ((_seg_rlen == 0) || (s->point(_seg_rbeg).rdate() >= rmid));
    assert ((_seg_rlen == 0) || (s->point(_seg_rbeg).rdate() < rend()));
    assert ((_seg_rlen == 0) || (s->point(_seg_rbeg+_seg_rlen-1).rdate() >= rmid));
    assert ((_seg_rlen == 0) || (s->point(_seg_rbeg+_seg_rlen-1).rdate() < rend()));
    
    assert (_seg_roff >= 0);
    assert (_seg_roff <= _seg_rlen);
    
    // i (next point) out of interval
    assert ((i >= segend) || (i == _seg_first + _seg_llen + _seg_rlen));

    // consistency of first wrt alignement
    assert((_seg_first < segend) ||
           (_seg_first == std::min(_seg_lbeg, _seg_rbeg)));
    assert((_seg_first == segend) ||
           (s->point(_seg_first).rdate() < rend()) ||
           (std::min(_seg_lbeg, _seg_rbeg) == segend));
    assert((_seg_first == segend) ||
           (s->point(_seg_first).rdate() >= rend()) ||
           (std::min(_seg_lbeg, _seg_rbeg) == _seg_first));
    
    _seg_next = i;
    assert(_check_next(s));    // correctness of seg_next
    
    return i;
}


size_t AlignedInterval::rewind(const InputSegment* s, size_t b)
{
    _seg_first = b;
    return rewind(s);
}


// we assume rbegin and rend are set and not MTU::UNBOUNDED.
size_t AlignedInterval::rewind(const InputSegment* s)
{
    assert(s);
    // don't recompute alignment
    //assert (! aligned());
    if (aligned())
    {
        WARN("AInt.rewind called on aligned interval {}", *this);
        assert(_check_next(s));
        return _seg_next;
    }
    // correctness of seg_first
    assert(_check_first(s));
    
    size_t segend = s->size();
    size_t i = _seg_first;
    
    // advances until finding a point out of the Interval
    for(; (i < segend) && (ropen() || s->point(i).rdate() < rend()); i++)
    {  }
    
    _seg_next = i;
    assert(_check_next(s));    // correctness of seg_next
    
    return i;
}


bool AlignedInterval::_check_first(const InputSegment* s)
{
    // if there is no first after the left bound,
    // then there is no point.
    if ((_seg_first >= s->size()) &&
        (s->point(s->size()-1).rdate() >= rbegin()))
        return false;

    // first must be after the left bound.
    // if first index is not out of bounds,
    // the first point must be after the left bound of the interval (rbegin)
    // (it can be out of the interval, i.e. after the right bound (rend))
    if ((_seg_first < s->size()) &&
        (s->point(_seg_first).rdate() < rbegin()))
        return false;
    
    // first must be the min
    if ((_seg_first < s->size()) && (_seg_first > 0) &&
        (s->point(_seg_first-1).rdate() >= rbegin()))
        return false;
    
    return true;
}


bool AlignedInterval::_check_next(const InputSegment* s)
{
    if (_seg_next > s->size())
        return false;
    
    // there must be no next after the right bound
    if ((_seg_next == s->size()) &&
        (! ropen()) &&
        s->point(s->size()-1).rdate() >= rend())
        return false;
    
    // there must be no next in case of open real interval
    if ((_seg_next < s->size()) &&
        (ropen()))
        return false;

    // next must be after the right bound
    if ((_seg_next < s->size()) &&
        (! ropen()) &&
        (s->point(_seg_next).rdate() < rend()))
        return false;
    
    // next must be the min next after right bound
    if ((_seg_next < s->size()) &&
        (! ropen()) &&
        (_seg_next > 0) &&
        (s->point(_seg_next-1).rdate() >= rend()))
        return false;
    
    return true;
}


std::ostream& operator<<(std::ostream& o, const AlignedInterval& p)
{
    
//    o << "[";
//    p.mbegin.printint(o);
//    o << "-";
//    p.mend.printint(o);
//    o << "]";
//    o << " [" << p.rbegin << "-" << p.rend << "]";
    o << static_cast<const Interval&>(p);
    
    o << " { ";
    //    if (p._seg_lbeg < segend)
    {
        for (size_t i = p._seg_lbeg; i < p._seg_lbeg + p._seg_llen; i++)
        {
            o << i << " ";
        }
    }
    o << "| ";
    //    if (p._seg_rbeg < segend)
    {
        for (size_t i = p._seg_rbeg; i < p._seg_rbeg + p._seg_rlen; i++)
        {
            o << i << " ";
        }
    }
    o << "}";
    
    return o;
}


/// @}

//
//  Point.cpp
//  qparse
//
//  Created by Florent Jacquemard on 05/02/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#include "Point.hpp"


long POINTREF_NULL = LONG_MIN; // limits.h


// event and link can be NULL
Point::Point(MusEvent* e,
             rtu_t rdate,
             bool noteon,
             long link,
             long backlink):
_event(e),
_rdate(rdate),
_onoff(noteon),
link(link),
backlink(backlink)
{
    assert(e);
    assert(e->isNote() || e->isRest());
//    assert (linked == NULL || linked->_rdate >= _rdate);
}


// event can be NULL
Point::Point(const Point& p):
link(p.link),
backlink(p.backlink),
_rdate(p._rdate),
_onoff(p._onoff)
{
    // TBC: not nec. to clone if input events are never modified
    if (p._event)
    {
        assert(p._event->isNote() || p._event->isRest());
        _event = p._event->clone();
        assert(_event->isNote() || _event->isRest());
    }
    else
        _event = NULL;
}


Point::~Point()
{
    if (_event)
    {
        delete _event;
        _event = NULL;
    }
}


Point& Point::operator=(const Point& p)
{
    if(this != &p)
    {
        link = p.link;
        backlink = p.backlink;
        _rdate = p._rdate;
        _onoff = p._onoff;

        /// @todo TBC:
        /// not nec. to clone if input events are never modified
        if (p._event)
            _event = p._event->clone();
        else
            _event = NULL;
    };
    
    return *this;
}


bool Point::operator==(const Point& p) const
{
    if (typeid(*this) != typeid(p))
        return false;
    
    if ((_rdate != p._rdate) || (onset() != p.onset()))
    {
        return false;
    }
    // cannot compare durations without input segment
    // @todo TBR
    // else if (rduration() != p.rduration())
    // {
    //    return false;
    // }
    else if (link != p.link)
    {
            return false;
    }
    else if (backlink != p.backlink)
    {
            return false;
    }
    else if (_event == NULL)
    {
        return (p._event == NULL);
    }
    else if (p._event == NULL)
    {
        return false;
    }
    else if (_event->isRest())
    {
        return (p._event->isRest());
    }
    else
    {
        assert(_event->isNote());
        if (p._event->isNote())
        {
            NoteEvent* ne = dynamic_cast<NoteEvent*>(_event);
            NoteEvent* pne = dynamic_cast<NoteEvent*>(p._event);
            assert(ne);
            assert(pne);
            return ((ne->pitch() == pne->pitch()) &&
                    (ne->velocity() == pne->velocity()));
        }
        else
        {
            return false;
        }
    }
}


void Point::print(std::ostream& o) const
{
    //INFO(":<15", _rdate); // left justify, width=15
    o << std::setw(12) << std::setiosflags(std::ios::left);
    o << _rdate << " ";
    
    if (_onoff)
        o << " NOTE_ON ";
    else
        o << " NOTE_OFF";
    
    if (_event)
        o << *_event;
    else
        o << "___";

    if (link != POINTREF_NULL)
        o << " link=" << link;

    if (backlink != POINTREF_NULL)
        o << " backlink=" << backlink;


}


/// @}

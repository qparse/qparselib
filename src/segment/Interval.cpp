//
//  Interval.cpp
//  qparse
//
//  Created by Florent Jacquemard on 06/02/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#include "Interval.hpp"
#include "InputSegment.hpp"


Interval::Interval(const InputSegment* s, mtu_t me):
_mbegin(MTU::ZERO),
_mend(me),
_rbegin(s->rbegin()),
_rend(s->rend())
{
    assert(s);
    assert(this->sanity_check(s));
}


//Interval::Interval(const InputSegment* s, double rdur, size_t bar):
//bars(bar),
//begin(MTU::ZERO),
//end(MTU::ONE),
//rbegin(s->rbegin()),
//rend(s->rbegin()+rdur)
//{
//    assert(s);
//    assert(rdur > 0);
//    assert(rend <= s->rend());
//}


//Interval::Interval(const InputSegment* s, const Interval& p):
//bars(p.bars+1),
//begin(MTU::ZERO),
//end(MTU::ONE),
//rbegin(p.rend),
//rend(p.rend+p.rduration())
//{
//    assert(s->rbegin() <= rbegin);
//    assert(rend <= s->rend());
//}


Interval::Interval(double rb, double re, mtu_t mb, mtu_t me):
_rbegin(rb),
_rend(re),
_mbegin(mb),
_mend(me)
{ }


// internal use only
Interval::Interval(const InputSegment* s,
                   double rb, double re,
                   mtu_t mb, mtu_t me):
_rbegin(rb),
_rend(re),
_mbegin(mb),
_mend(me)
{
    assert(s);
    assert(this->sanity_check(s));
}

// next of p in s
Interval::Interval(const InputSegment* s, const Interval& p, Rational factor):
_rbegin(p._rend),
_rend(_rbegin + RTU::mult(p.rduration(), factor)),
_mbegin(p._mend),
_mend(_mbegin + MTU::mult(p.mduration(), factor))
{
    assert(s);
    assert(p._mend != MTU::UNBOUNDED);
    assert(this->sanity_check(s));
    assert(_rend <= s->rend());
}


Interval::Interval(const Interval& p):
_mbegin(p._mbegin),
_mend(p._mend),
_rbegin(p._rbegin),
_rend(p._rend)
{
    assert(this->sanity_check());
}


Interval::Interval(Interval* p):
_mbegin(p->_mbegin),
_mend(p->_mend),
_rbegin(p->_rbegin),
_rend(p->_rend)
{
    assert(p);
    assert(this->sanity_check());
}


bool Interval::sanity_check()
{
    assert(MTU::defined(mbegin()));
    assert(mopen() || (mbegin() <= mend()));
    assert(ropen() || (rbegin() <= rend()));
    
    return true;
}


bool Interval::sanity_check(const InputSegment* s)
{
    sanity_check();
    assert(s);
    assert(s->rbegin() <= this->rbegin());
    // the interval can start after the input segment's end
    //assert((mduration() == 0) || (rbegin <= s->rend()));
    //assert(rend <= s->rend());
    
    return true;
}


Interval& Interval::operator= (const Interval& p)
{
    if(this != &p)
    {
        _mbegin = p._mbegin;
        _mend = p._mend;
        _rbegin = p._rbegin;
        _rend = p._rend;
    };
    return *this;
}


bool Interval::operator==(const Interval& p) const
{
    return ((_rbegin == p._rbegin) &&
            (_rend   == p._rend)   &&
            (_mbegin == p._mbegin) &&
            (_mend   == p._mend));
}


mtu_t Interval::mduration() const
{
    if (_mend == MTU::UNBOUNDED)
        return MTU::UNBOUNDED;
    else
    {
        assert (_mbegin != MTU::UNBOUNDED);
        return (_mend - _mbegin);
    }
}


double Interval::rduration() const
{
    if (_rend == RTU::UNBOUNDED)
        return RTU::UNBOUNDED;
    else
    {
        assert (_rbegin != RTU::UNBOUNDED);
        return (_rend - _rbegin);
    }
}


//int ::rdate(size_t i) const
//{
//    assert(0 <= i);
//    assert(i < _seg->size());
//    return _seg->rdate(i);
//}


//bool Interval::insideBar() const
//{
//    return (mduration() <= MTU::ONE);
//}


std::ostream& operator<<(std::ostream& o, const Interval& p)
{
    o << "mtu[";
    p.mbegin().printint(o);
    o << ", ";
    if (p.mopen())
        o << "+∞";
    else
        p.mend().printint(o);
    o << "[";
    o << " rtu[";
    o << p.rbegin();
    o << ", ";
    if (p.ropen())
        o << "+∞";
    else
        o << p.rend();
    o << "[";
    
    return o;
}


/// @}

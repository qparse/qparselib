//
//  MTU.cpp
//  qparse
//
//  Created by Florent Jacquemard on 16/02/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include <math.h>       /* floor */

#include "AMTU.hpp"


AMTU::AMTU():
_quotient(0),
_modulo(0)
{ }


AMTU::AMTU(size_t bars, size_t rel):
_quotient(bars),
_modulo(rel)
{ }


AMTU::AMTU(const AMTU& d):
_quotient(d._quotient),
_modulo(d._modulo)
{ }


AMTU& AMTU::operator=(const AMTU& d)
{
    if(this != &d)
    {
        _quotient = d._quotient;
        _modulo = d._modulo;
    };
    return *this;
}


AMTU* AMTU::clone() const
{
    return new AMTU(*this);
}


bool AMTU::operator==(const AMTU& d) const
{
    return (_quotient == d._quotient) && (_modulo == d._modulo);
}


const AMTU& AMTU::operator+=(const AMTU& rhs)
{
    if ( (rhs._quotient > 0) && (rhs._modulo > 0))
    {
        WARN("add mtu {} to {}: possible unexpected behaviour",
             rhs, *this);
    }
    _quotient += rhs._quotient;
    _modulo += rhs._modulo;
    
    return *this;
}

const AMTU& AMTU::operator-=(const AMTU& rhs)
{
    assert (_quotient >= rhs._quotient);
    assert (_modulo >= rhs._modulo);

    if ( (rhs._quotient > 0) && (rhs._modulo > 0))
    {
        WARN("substract mtu {} from {}: possible unexpected behaviour",
             rhs, *this);
    }
    _quotient -= rhs._quotient;
    _modulo -= rhs._modulo;
    
    return *this;
}

Rational AMTU::absolutetoRatio(size_t res) const
{
    assert (res > 0);
    return (Rational(_quotient) + insidetoRatio(res));
}


Rational AMTU::insidetoRatio(size_t res) const
{
    assert (res > 0);
    return (Rational(_modulo, res));
}


void AMTU::print(std::ostream& o) const
{
    o << _quotient << "." << _modulo;
}


void AMTU::print(std::ostream& o, size_t res) const
{
    if (_quotient == 0)
    {
        o << Rational(_modulo, res);
    }
    else
    {
        o << _quotient << "+" << Rational(_modulo, res);
    }
}





MTU::MTU(size_t res):
AMTU(),
_resolution(res)
{
    assert(res > 0);
    assert(_modulo < res);
}


MTU::MTU(size_t bars, size_t rel, size_t res):
AMTU(bars, rel),
_resolution(res)
{
    assert(res > 0);
    assert(0 <= rel);
    assert(rel < res);
}


MTU::MTU(const MTU& d):
AMTU(d),
_resolution(d._resolution)
{ }


MTU& MTU::operator= (const MTU& d)
{
    if(this != &d)
    {
        AMTU::operator=(d);
        _resolution = d._resolution;
    };

    return *this;
}


MTU* MTU::clone() const
{
    return new MTU(*this);
}


bool MTU::operator==(const MTU& d) const
{
    return AMTU::operator==(d) && (_resolution == d._resolution);
}


const MTU& MTU::operator+=(const MTU& rhs)
{
    
    size_t sum = _modulo + rhs._modulo;

    // inside the bar. safe.
    if ((rhs._quotient == 0) && (sum < _resolution))
    {
        _modulo = sum;
        assert(rhs._resolution == _resolution); // resolution unchanged
    }
    // change bar. unsafe.
    // arbitrary assumption that resolution is rhs._resolution
    // in all bars between
    // the current bar (excluded) and
    // and bbar of the date of sum (included).
    else if ((rhs._quotient == 0) && (sum >= _resolution))
    {
        WARN("add mtu {} to {}: possible unexpected behaviour",
             rhs, *this);

        size_t rem = sum - _resolution;
        assert (rhs._resolution > 0);
        _quotient += floor(rem / rhs._resolution);
        _modulo = (rem % rhs._resolution);
        _resolution = rhs._resolution;
    }
    // change bar. safe.
    // add first the bars (quotients) and then the modulo.
    else if ((rhs._quotient > 0) && (sum < rhs._resolution))
    {
        if (rhs._modulo > 0)
            WARN("add mtu {} to {}: add first the bar then the modulo",
                 rhs, *this);
        _quotient += rhs._quotient;
        _modulo = sum;
        _resolution = rhs._resolution;
    }
    // unsafe.
    // add first the bars (quotients) and then the modulo.
    // with same assumption as above.
    else
    {
        WARN("add mtu {} to {}: possible unexpected behaviour",
             rhs, *this);

        _quotient += rhs._quotient;
        size_t rem = sum - rhs._resolution;
        assert (rhs._resolution > 0);
        _quotient += floor(rem / rhs._resolution);
        _modulo = (rem % rhs._resolution);
        _resolution = rhs._resolution;
    }

    return *this;
}


const MTU& MTU::operator-=(const MTU& rhs)
{
    assert (_quotient >= rhs._quotient);
    _quotient -= rhs._quotient;

    // inside the bar. safe.
    if ((rhs._quotient == 0) && (rhs._modulo <= _modulo))
    {
        _modulo -= rhs._modulo;
        assert(rhs._resolution == _resolution); // resolution unchanged
    }
    // change bar. unsafe.
    else if ((rhs._quotient == 0) && (rhs._modulo > _modulo))
    {
        WARN("substract mtu {} from {}: possible unexpected behaviour",
             rhs, *this);
        
        size_t diff = rhs._modulo - _modulo;
        assert (rhs._resolution > 0);
        _quotient -= ceil(diff / rhs._resolution);
        assert(_quotient >= 0);
        _modulo = rhs._resolution - (diff % rhs._resolution);
        _resolution = rhs._resolution;
    }
    // change bar. safe.
    else if ((rhs._quotient > 0) && (rhs._modulo <= _modulo))
    {
        if (rhs._modulo > 0)
            WARN("substract mtu {} from {}: substract first the bar then the modulo",
                 rhs, *this);
        _quotient -= rhs._quotient;
        _modulo -= rhs._modulo;
        _resolution = rhs._resolution;
    }
    // change bar. unsafe.
    else if ((rhs._quotient > 0) && (rhs._modulo > _modulo))
    {
        WARN("substract mtu {} from {}: possible unexpected behaviour",
             rhs, *this);
        
        _quotient -= rhs._quotient;
        size_t diff = rhs._modulo - _modulo;
        assert (rhs._resolution > 0);
        _quotient -= ceil(diff / rhs._resolution);
        assert(_quotient >= 0);
        _modulo = rhs._resolution - (diff % rhs._resolution);
        _resolution = rhs._resolution;
    }    
    
    return *this;
}


Rational MTU::insidetoRatio() const
{
    assert (_resolution > 0);
    return (Rational(_modulo, _resolution));
}


void MTU::print(std::ostream& o) const
{
    o << _quotient << "+" << insidetoRatio();
}

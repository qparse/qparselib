//
//  DuraFile.cpp
//  squant2
//
//  Created by Florent Jacquemard on 09/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{

/// read a Duration List from a file

#include <stdio.h>

#include "trace.hpp"
#include "MTU.hpp" // read a mtu_t (Rational) from a stream
#include "DuraList.hpp"


/// import of a duration list from a text file
/// format for the file: 1 fraction (IOI) per line
DurationList::DurationList(std::string filename):
_continuation(MTU::ZERO),
_main(),
_sum(0.0),
_summed(1), // TBC
//_dummy(0),
_state(0),
_status(0)
{
    std::ifstream file;
    
    file.open(filename, std::ios_base::in);

    if(!file.is_open())
    {
        ERROR("cannot open {}", filename);
        _status = 1;
    }

    bool first = true;
    
    for(std::string line; getline(file, line); )
    {
        mtu_t d;
        
        // skip empty line
        if (line.size() == 0) continue;
        
        std::istringstream in(line);   //make a stream from the line
        if (!(in >> d))
        {
            ERROR("parse error in {}, skip line.", filename);
            continue;      // parse error: skip line
        }

        if (first && (d < 0))
        {
            addcont(-d);
            first = false;
        }
        else
        {
            assert(d >= 0);
            add(d);
        }
    }
    file.close();
}



# src/segment/import

Facilities for reading the data given in input to the transcription by parsing algorithm:
* sequence of musical events timestamped in Real-Time Units (`InputSegment`).
* sequence of IOI in Musical-Time Units (`ValueList` or `DurationList`).
 
 The input segments can be read from files in two formats:
* text format.
* MIDI file input. based on the library [Midifile](http://midifile.sapp.org) of Craig Stuart Sapp.

The IOI sequences are read from a text file.



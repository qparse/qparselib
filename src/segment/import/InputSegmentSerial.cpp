//
//  InputSegmentSerial.cpp
//  squant
//
//  Created by Florent Jacquemard on 06/04/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//

#include "InputSegmentSerial.hpp"


InputSegmentSerial::InputSegmentSerial(const std::string filename,
                                       bool mono):
InputSegment(),
_filename(filename),
_status(0)
{
    //assert (mono);
    std::ifstream file;
        
    file.open(filename, std::ios_base::in);
    if( file.is_open() )
    {
        _status = this->import_textfile(file);
    }
    else
    {
        ERROR("failed to open {}", filename);
        _status = 1;
    }

    assert(0 <= _rbegin);
    assert(_rbegin <= _rend);
}


InputSegmentSerial::InputSegmentSerial(const InputSegmentSerial& s):
InputSegment(s),
_filename(s._filename),
_status(s._status)
{ }


size_t InputSegmentSerial::import_textfile(std::ifstream& file)
{
    // fill a vector of dates from input file
    std::vector<rtu_t> dates;
    int ln = 1;
    for(std::string line; getline(file, line); ln++)
    {
        rtu_t d = -1;
        // skip empty line
        if (line.size() == 0) continue;
        
        std::istringstream in(line);   // make a stream from the line
        if (in >> d) // read duration (rtu_t)
        {
            if (d < 0)
            {
                ERROR("{}:{}: durations must be positive",
                      _filename, ln);
                return 2;
            }
            else
            {
                dates.push_back(d);
            }
        }
        // not a number: skip line (comments etc)
        else
        {
            TRACE("{}: line {} ignored", _filename, ln);
            continue;
        }
    }
    file.close();
    TRACE("{}: closing, {} events read", _filename, (dates.size()-2));
    
    if (dates.size() < 3)
    {
        ERROR("{}: input file must contain at least \
              start date, 1 event, end date", _filename);
        return 2;
    }

    // std::vector<rtu_t> v;

    // first line = begin date
    _rbegin = dates[0];
    if (_rbegin < 0)
    {
        ERROR("{}: begin date must be positive ({})", _filename, _rbegin);
        return 3;
    }
    TRACE("{}: start date={}s", _filename, _rbegin);

    // last line = end date
    _rend = dates[dates.size()-1];
    if (_rend < _rbegin)
    {
        ERROR("{}: end date ({}) must after begin date ({})",
              _filename, _rend, _rbegin);
        return 4;
    }
    TRACE("{}: end date={}s", _filename, _rend);
    
    rtu_t prev_rdate = _rbegin;
    rtu_t rdate = -1;
    long prev = POINTREF_NULL; // index in this segment
    long current = POINTREF_NULL;
    for (size_t i = 1; i < dates.size()-1; i++)
    {
        rdate = dates[i];

        // the first point can be at segment beggining
        if ((rdate < _rbegin) || (_rend < rdate))
        {
            ERROR("{}: point[{}] has date {} out of bounds in file",
                  _filename, i, rdate);
            return 3;
        }
        // two successive points can coincide
        assert (0 <= prev_rdate);
        if (prev_rdate > rdate)
        {
            ERROR("{}: point[{}] ({}s) sould be after previous point ({}s)",
                  _filename, i, rdate, prev_rdate);
            return 4;
        }

        // add as note-on (unpitched note)
        MusEvent* e = new NoteEvent();
        current = this->add_back(e, rdate, true);
        assert(check_index(prev));
        assert(check_index(current));

        // not first note
        if (i > 1)
        {
            // link previous note to this note
            assert(prev != POINTREF_NULL);
            this->link(prev, current);
            assert(this->point(prev).rdate() == prev_rdate);
            TRACE("{}: note-on[{}] at {}s, linked-to {} ({}), dur={}s",
                  _filename,
                  prev,
                  this->point(prev).rdate(),
                  current, rdate,
                  this->rduration(prev));
        }
        
        prev_rdate = rdate;
        prev = current;
    }
    
    // add a closing event to heap (at end date)
    // as note-off (unpitched note).
    MusEvent* e = new NoteEvent();
    long last = add_floating(e, _rend, false);
    // link the (real) event before to closing event.
    link(current, last);
    TRACE("{}: note-on[{}] at {}s, linked-to {} ({}), dur={}s",
          _filename,
          current,
          this->point(current).rdate(),
          last, _rend,
          this->rduration(current));
    TRACE("{}: closing event[{}] at {}s", _filename, last, _rend);

//    for (size_t c = 0; c < _events.size(); c++)
//    {
//        DEBUGU(".....EVENT[{}] {}",
//               c, (_events[c].event() != NULL)?"OK":"NULL");
//        DEBUGU(".....EVENT[{}] CAST {}",
//               c, (dynamic_cast<NoteEvent*>(_events[c].event()) != NULL)?"OK":"FAIL");
//        DEBUGU(".....EVENT[{}] ISNOTE {}",
//               c, (_events[c].event()->isNote())?"OK":"NO");
//    }
    
    return 0;
}


size_t InputSegmentSerial::save(std::string filename)
{
    std::ofstream file;
    
    file.open(filename, std::ios_base::out);
    if(!file.is_open())
    {
        ERROR("InputSegment export : cannot open {} for writing", filename);
        return 1;
    }
    
    file << _rbegin << "\n";
    
    for (size_t i = 0; i < _events.size(); i++)
        if(_rbegin <= _events[i].rdate() && _events[i].rdate() <= _rend)
        {
            file << _events[i].rdate() << "\n";
        }
    
    file << _rend << "\n";
    
    file.close();
    return 0;
}



//
//  ValueFile.cpp
//  qparse
//
//  Created by Florent Jacquemard on 03/03/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup intput
/// @{


/// read a Value List from a file

#include <stdio.h>

#include "trace.hpp"
#include "ValueList.hpp"

/// import of a value list from a text file
/// format for the file: 1 fraction (IOI) per line
ValueList::ValueList(std::string filename):
_continuation(MTU::ZERO),
_main(),
_length(MTU::ZERO),
_accu(MTU::ZERO),
_nbevt(0),
_nbgn(0),
_status(0)
{
    std::ifstream file;
    
    file.open(filename, std::ios_base::in);

    if(!file.is_open())
    {
        ERROR("cannot open {}", filename);
        _status = 1;
    }
    
    bool first = true;
    
    for(std::string line; getline(file, line); )
    {
        mtu_t d;
        
        // skip empty line
        if (line.size() == 0) continue;
        
        std::istringstream in(line);   //make a stream from the line
        if (!(in >> d)) continue; // parse error: skip line
        
        if (first && (d < 0))
        {
            mtu_t q = -d;
            assert (MTU::defined(q));
            assert (MTU::defined(_continuation));
            _continuation += q;
            _accu += q;
            _length += q;
            first = false;
        }
        else
        {
            if (d < 0)
            {
                ERROR("error reading IOI ratios from file: negative (cont) can only be first.");
                _status = 2;
            }
            else
            {
                assert(MTU::defined(d));
                if (d.null())
                    _nbgn++;
                else
                    _nbevt++;

                _main.push_back(d);
                _accu += d;
                _length += d;
            }
        }
    }
    file.close();
}


/// @}

//
//  InputSegment.hpp
//  qparse
//
//  Created by Florent Jacquemard on 06/10/16.
//  Copyright © 2016 Florent Jacquemard. All rights reserved.
//
/// @defgroup segment Segment module
///
/// The `segment` module contains classes for abstract representation
/// of time intervals and sequences of timed musical events,
/// either in input of transcription (events dated in real-time unit - i.e. seconds )
/// or as output (events dated in musical-time unit - i.e. bars or beats).
///
/// @addtogroup segment
/// @{


#ifndef InputSegment_hpp
#define InputSegment_hpp

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <iostream>
#include <string>
#include <vector>

//#include "MidiFile.h"

#include "MTU.hpp"
#include "MusEvent.hpp"
#include "MusPoint.hpp"
#include "Spiral.hpp"
#include "Label.hpp"
#include "SymbLabel.hpp"
#include "Interval.hpp"
#include "TableA.hpp"

//#include "InputSequence.hpp"


template<class P> class Atable;
template<class P> class Run;
class MidiFile;

typedef std::vector<MusPoint> MusPointSeq;


/// @brief intermediate representation for input performance data
/// (sequence of timestamped events).
///
/// an input segment is made of:
/// - a time interval containing some timed events (muspoints),
///   always sorted by increasing realtime date (vector of events).
/// - a heap of floating points.
///
/// The links in points are indexes in the input segment,
/// where an index is a unique identifier of a point
/// in either of the two above structures.
/// @see Point::linked
///
/// The realtime duration of a linked point is the difference
/// of realtime dates (between the link and the point).
/// the realtime duration of a point without link (with unknown link) is zero.
///
/// @todo do the same think with musical time duration.
/// @todo suppr.  samplestosec
/// @todo suppr. member _res (resolution)
class InputSegment : public Interval
{
public:
 
    /// @brief constructs an empty input segment (no events)
    /// @param rb start date (in RTU - seconds)
    /// @param re end date (in RTU - seconds)
    InputSegment(rtu_t rb=0, rtu_t re=0);
    
    InputSegment(const InputSegment&);
    
    /// @brief copy and resize.
    /// @param s input segment to copy.
    /// @param b new start date (in seconds)
    /// @param e new end date (in seconds)
    /// @warning copy only the events inside the new bounds.
    InputSegment(const InputSegment& s, rtu_t b, rtu_t e);
    
    virtual ~InputSegment();
    
    // @brief real-time start date (in seconds) of segment.
    //inline rtu_t rbegin() const { return _rbegin; }
    
    // @brief real-time end date (in seconds) of segment.
    //inline rtu_t rend() const { return _rend; }
    
    // @brief real-time total duration (in seconds) of segment.
    //inline rtu_t rduration() const { return _rduration; }
    
    /// @brief number of non-floating points in segment.
    size_t size() const;

    /// @brief iterators to the segment's contents.
    MusPointSeq::iterator begin();
    MusPointSeq::iterator end();
    MusPointSeq::const_iterator cbegin() const;
    MusPointSeq::const_iterator cend() const;
    
    /// @brief return a ref to the point at index i.
    /// - ith point in this input segment if 0 <= i < input segment size
    /// - or the -i-1th floating point    if heap size <= i < 0.
    /// @param i must be in the above range of values.
    const MusPoint& point(long i) const;
    
    /// @brief return the event for the point at index i.
    inline MusEvent* event(long i) const;
    
    /// @brief return the real-time date (in seconds)
    /// of the point of index i
    inline rtu_t rdate(long i) const;
    
    /// @brief return the real-time duration (in seconds)
    /// of the point of index i.
    /// @return 0 if the point i is an offset,
    /// the distance (in real time) to matching offset if
    /// point i is an onset.
    rtu_t rduration(long i) const;

    /// @brief return a reference to the musical-time date
    /// (in fraction of bar)
    /// of the point of index i.
    mtu_t mdate(long i) const;
    
    /// @param i index of point to change musical date.
    /// @param d new date for point i.
    /// must be defined, bounded and positive or null.
    void set_mdate(long i, mtu_t d);

    /// @brief return a reference to the musical-time duration
    /// (in fraction of bar)
    /// of the point of index i.
    mtu_t mduration(long i) const;

    /// @param i index of point to change musical duration.
    /// @param d new duration for point i.
    /// must be defined, bounded and positive or null.
    void set_mduration(long i, mtu_t d);
    
    /// @param i index of point.
    int voice(long i) const;
    
    /// @param i index of point to change voice.
    /// @param v a voice number. must not be VOICE_UNDEF.
    void set_voice(long i, int v);

    /// @brief the point of index i corresponds to an onset.
    bool onset(long i) const;

    /// @brief the point of index i corresponds to an offset.
    /// @warning in the monophonic case, it implies that the event
    /// at the point of index i is a rest.
    bool offset(long i) const;
     
    /// @brief compute the range of indexes inside a mtu time window
    /// @param b begin date of the time window (included).
    /// @param e end date of the time window (excluded).
    ///        must be larger than b.
    /// @return return a pair made of the index of the first and the last
    /// points of this segment inside the given mtu time window,
    /// or the size of this segment (two times) if there are none.
    /// @warning for the range of points inside the bar i,
    /// use b = MTU::of_int(i) and b = MTU::of_int(i+1).
    std::pair<long, long> range(mtu_t b, mtu_t e);
    
    
    // compare the points of index i and j
    // lexicographicaly by rdate, rduration, event MIDI number
    // return wether point[i] < point[j]
    //static bool point_comp(long i, long j);

    /// @brief insert new timestamped muspoint created from the parameters,
    /// at the end of the segment.
    /// @warning The realtime dateof the point must be after
    /// the current last point of this segment.
    /// @return the index of the inserted point (can be used as link).
    long add_back(MusEvent* e,
                  rtu_t rdate,
                  bool on,
                  long link=POINTREF_NULL,
                  long backlink=POINTREF_NULL,
                  mtu_t mdate=MTU::UNDEF,
                  mtu_t mduration=MTU::UNDEF);
    
    long add_back(const MusPoint& p);

    /// @brief create new timestamped muspoint from the parameters,
    /// and add the the heap of floating points (not in segment).
    ///
    /// (allocated and freed by this segment)
    /// @return the index of the new point (can be used as link).
    long add_floating(MusEvent* e,
                      rtu_t rdate,
                      bool on,
                      long link=POINTREF_NULL,
                      long backlink=POINTREF_NULL,
                      mtu_t mdate=MTU::UNDEF,
                      mtu_t mduration=MTU::UNDEF);

    long add_floating(const MusPoint& p);
    
    // r must be strictly positive
    //void set_resolution(size_t r);
    
    /// @brief set end date.
    void close(rtu_t e);
    
    /// @brief resert start and end dates.
    /// @param b new start date (in seconds)
    /// @param e new end date (in seconds)
    /// @warning e must be after b
    void resize(double b, double e);
    
    // save to text file
    // size_t save(std::string);
        
    // set resolution
    //void setResolution(size_t);
    
    /// @brief return wether the segment has the type of the code.
    ///
    /// type InputSegment:
    /// plain InputSegment imported from MIDI file (or text) without filters.
     virtual bool hasType(std::string code) const
    { return (code == "InputSegment"); }
    
    /// @brief write segment content to output stream.
    friend std::ostream& operator<<(std::ostream&, const InputSegment&);
    
    /// @brief print size to output stream.
    void print(std::ostream&) const;
    
    
protected:

    /// @brief event list.
    ///
    /// polymorphic (mono or poly)
    MusPointSeq _events;

    /// @brief floating events.
    ///
    /// not in the list but can be linked by events on the list
    MusPointSeq _heap;

    /// @brief same as point but not const.
    MusPoint& ncpoint(long i);

    bool check_index(long i) const;

    /// @brief the event of index i is linked to the event of index j,
    ///        and j is backlinked to i.
    /// @param i must be a valid and not NULL index of an onset point.
    /// @param j must be a valid or NULL index.
    ///        if non NULL it must index an offset.
    /// @warning both i and j can be in heap (negative index).
    /// @warning the point at i and j must not be linked (NULL link index).
    /// @warning the realtime date of i must be <= realtime date of j
    /// (if j not NULL).
    void link(long i, long j);
    
};


#endif /* InputSegment_hpp */


/// @}

//
//  InputSegment.cpp
//  qparse
//
//  Created by Florent Jacquemard on 06/10/16.
//  Copyright © 2016 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{

// segment of points (events) in input
//
// @todo suppr.  samplestosec
// @todo suppr. member _res (resolution)

#include "InputSegment.hpp"


InputSegment::InputSegment(rtu_t rb, rtu_t re):
Interval(rb, re, MTU::ZERO, MTU::UNBOUNDED),
//_rduration(re-rb),
//_mduration(MTU::UNBOUNDED),
_events(), // empty list of timestamped events
_heap()
{
    assert(0 <= rb);
    assert(rb <= re);
}


InputSegment::InputSegment(const InputSegment& s):
Interval(s._rbegin, s._rend, s._mbegin,s._mend),
//_rduration(s._rduration),
//_mduration(s._mduration),
_events(s._events), // copy all MusPoint's
_heap(s._heap)
{ }


InputSegment::InputSegment(const InputSegment& s, rtu_t rb, rtu_t re):
Interval(rb, re, MTU::ZERO, MTU::UNBOUNDED),
//_rduration(e-b),
//_mduration(MTU::UNBOUNDED), // must be requantized
_events(),
_heap(s._heap) // copy all heap points to preserve references
               // (some may be out of the new time interval)
{
    assert(0 <= rb);
    assert(rb <= re);
    
    // copy only the points in the new time interval
    for (size_t i = 0; i < s._events.size(); i++)
    {
        if ((rb <= s._events[i].rdate()) && (s._events[i].rdate() <= re))
            _events.emplace_back(s._events[i]); // copy
    }
}


// TBC
InputSegment::~InputSegment()
{
    _events.clear();
    _heap.clear();
}


size_t InputSegment::size() const
{
    return _events.size();
}

MusPointSeq::iterator InputSegment::begin()
{
    return _events.begin();
}

MusPointSeq::iterator InputSegment::end()
{
    return _events.end();
}

MusPointSeq::const_iterator InputSegment::cbegin() const
{
    return _events.cbegin();
}

MusPointSeq::const_iterator InputSegment::cend() const
{
    return _events.cend();
}


//MusPointSeq::iterator
//InputSegment::emplace(const MusPointSeq::iterator i,
//                      MusEvent* e,
//                      rtu_t rdate, bool on,
//                      Point* link,
//                      mtu_t mdate,
//                      mtu_t mduration)
//{
//    MusPointSeq::iterator p = i;
//    if (p == _events.end())
//    {
//        // non empty segment: go to last point of segment
//        if (p != _events.begin())
//        {
//            p--;
//        }
//        // else: empty segment: don't move p
//    }
//    // move p to its correct position.
//    for (; (p !=  _events.end()) && (rdate >= p->rdate()); p++);
//    for (; (p !=  _events.begin()) && (rdate < (p-1)->rdate()); p--);
//    // now p is at the right position; insert.
//    assert(((p == _events.begin()) && (p == _events.end()))    ||
//           ((p == _events.begin()) && (rdate < p->rdate()))    ||
//           ((p == _events.end()) && ((p-1)->rdate() <= rdate)) ||
//           (((p-1)->rdate() <= rdate) && ( rdate < p->rdate())));
//    return _events.emplace(p, e, rdate, on, link, mdate, mduration);
//}


bool InputSegment::check_index(long i) const
{
    if (i == POINTREF_NULL)
        return true;
    else if ((0 <= i) && (i < _events.size()))
        return true;
    else if ((-_heap.size() <= i) && (i < 0))
        return true;
    else
        return false;
}


void InputSegment::link(long i, long j)
{
    assert(check_index(i));
    assert(i != POINTREF_NULL);
    assert(check_index(j));
    // forward link
    // link set for the first time.
    if (point(i).link != POINTREF_NULL)
    {
        assert(j != POINTREF_NULL);
        WARN("link of point {} reset from {} to {}",
             i, point(i).link, j);
    }
    ncpoint(i).link = j;
    TRACE("new link {}({}) - {}({})",
          i, point(i).rdate(), j, point(j).rdate());

    // backward link
    if  (j != POINTREF_NULL)
    {
        assert(point(i).rdate() <= point(j).rdate());
        if (point(j).backlink != POINTREF_NULL)
        {
            WARN("back link of point {} reset from {} to {}",
                 j, point(j).backlink, i);
        }
        ncpoint(j).backlink = i;
        TRACE("new backlink {}({}) - {}({})",
              j, point(j).rdate(), i, point(i).rdate());
    }
}


long InputSegment::add_back(MusEvent* e,
                            rtu_t rdate,
                            bool on,
                            long link,
                            long backlink,
                            mtu_t mdate,
                            mtu_t mduration)
{
    assert(e);
    assert(check_index(link));
    // preserve ordering
    assert(_events.empty() || _events.back().rdate() <= rdate);
    _events.emplace_back(e, rdate, on,
                         link, backlink,
                         mdate, mduration);
    return (_events.size() - 1);
}


long InputSegment::add_back(const MusPoint& p)
{
    _events.emplace_back(p);
    return (_events.size() - 1);
}


long InputSegment::add_floating(MusEvent* e,
                                rtu_t rdate,
                                bool on,
                                long link,
                                long backlink,
                                mtu_t mdate,
                                mtu_t mduration)
{
    assert(e);
    assert(check_index(link));
    assert(_events.empty() || _events.back().rdate() <= rdate);
    _heap.emplace_back(e, rdate, on,
                       link, backlink,
                       mdate, mduration);
    return (- _heap.size());
}


long InputSegment::add_floating(const MusPoint& p)
{
    _heap.emplace_back(p);
    return (- _heap.size());
}


const MusPoint& InputSegment::point(long i) const
{
    assert(i != POINTREF_NULL);
    if (i >= 0)
    {
        assert(i < _events.size());
        return _events[i];
    }
    else
    {
        long j = -(i+1);
        assert(0 <= j);
        assert(j < _heap.size());
        return _heap[j];
    }
}


// non-const version
MusPoint& InputSegment::ncpoint(long i) 
{
    assert(i != POINTREF_NULL);
    if (i >= 0)
    {
        assert(i < _events.size());
        return _events[i];
    }
    else
    {
        long j = -(i+1);
        assert(0 <= j);
        assert(j < _heap.size());
        return _heap[j];
    }
}


MusEvent* InputSegment::event(long i) const
{
    return point(i).event();
}


rtu_t InputSegment::rdate(long i) const
{
    return point(i).rdate();
}


rtu_t InputSegment::rduration(long i) const
{
    assert(check_index(i)); // true for POINTREF_NULL
    assert(i != POINTREF_NULL);
    const MusPoint& p = point(i);

    if (p.link == POINTREF_NULL)
    {
        ERROR("Input segment: link not set for Point[{}]:{} ({}),\
               cannot compute rduration",
               i, p.rdate(), (p.onset()?"ON":"OFF"));
        return 0;
    }
    const MusPoint& poff = point(p.link);
    assert(poff.rdate() >= p.rdate());
    return(poff.rdate() - p.rdate());
}


mtu_t InputSegment::mdate(long i) const
{
    return point(i).mdate();
}


void InputSegment::set_mdate(long i, mtu_t d)
{
    ncpoint(i).set_mdate(d);
}


/// @todo compute like rduration(i)
mtu_t InputSegment::mduration(long i) const
{
    return point(i).mduration();
}


void InputSegment::set_mduration(long i, mtu_t d)
{
    ncpoint(i).set_mduration(d);
}


int InputSegment::voice(long i) const
{
    return point(i).voice();
}


void InputSegment::set_voice(long i, int v)
{
    ncpoint(i).set_voice(v);
}


bool InputSegment::onset(long i) const
{
    return event(i)->isNote();
    //return point(i).noteon();
}


/// @warning changed
bool InputSegment::offset(long i) const
{
    return event(i)->isRest();
    //return point(i).noteoff();
}

std::pair<long, long> InputSegment::range(mtu_t b, mtu_t e)
{
    assert(b < e);
    long undef = this->size();
    long first = undef;
    long last = undef;
    for (size_t i = 0; i < _events.size(); ++i)
    {
        mtu_t date = _events[i].mdate();
        if (date < b)
        {
            continue;
        }
        else if (b <= date && date < e)
        {
            // first unset and last unset
            if (first == last && first == undef)
            {
                first = i;
            }
            last = i;
        }
        else
        {
            assert(e <= date);
            break;
        }
    }
    
    return std::make_pair(first, last);
}


//void InputSegment::resize(double b, double e)
//{
//    assert(0 <= b);
//    assert(b <= e);
//
//    _begin = b;
//    _end = e;
//    _len = e-b;
//}

//bool InputSegment::point_comp(long lhs, long rhs)
//{
//    rtu_t lrdate = rdate(lhs);
//    rtu_t rrdate = rdate(rhs);
//    if (lrdate < rrdate)
//        return true;
//    else if (lrdate > rrdate)
//        return false;
//    else
//    {
//        assert(lrdate == rrdate);
//        rtu_t lrduration = rduration(lhs);
//        rtu_t rrduration = rduration(rhs);
//        if (lrduration < rrduration)
//            return true;
//        else if (lrduration > rrduration)
//            return false;
//        else
//        {
//            assert(lrduration== rrduration);
//            assert(event(lhs));
//            assert(event(rhs));
//            // prioterize according to position in MIDI file
//            return (event(lhs)->number < event(rhs)->number);
//        }
//    }
//}


void InputSegment::close(rtu_t e)
{
    _rend = e;
}


void InputSegment::print(std::ostream& o) const
{
    for (size_t i = 0; i < _events.size(); i++)
    {
        o << "point[" << i << "]: ";
        o << point(i);
        //o << " rdate=" << rdate(i) << "s";
        if (point(i).link != POINTREF_NULL)
            o << " rdur=" << rduration(i) << "s";
        o << "\n";
    }
}


std::ostream& operator<<(std::ostream& o, const InputSegment& s)
{
//    o << s.size() << "events: ";
    s.print(o);
    return o;
}


/// @}

//
//  RTU.hpp
//  qparse
//
//  Created by Florent Jacquemard on 06/01/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#ifndef RTU_hpp
#define RTU_hpp

#include <stdio.h>
#include <assert.h>
#include <limits.h>

#include "trace.hpp"
#include "Rational.hpp"

//#define RTU_INFINITY numeric_limits<double>::max()


/// @brief Real Time Unit.
/// time expressed in seconds.
/// @todo change to time in number of samples,
/// where the sample duration is fixed when loading input (MIDI).
typedef double rtu_t;


namespace RTU
{

    /// infinite real time value
    /// for right bound of unbounded time intervals.
    const rtu_t UNBOUNDED = std::numeric_limits<double>::max();

    const rtu_t ZERO = 0;
    const rtu_t ONE = 1;

    /// cast of rational
    rtu_t of_Rational(const Rational& q);

    /// product of given rtu duration by a rational
    rtu_t mult(rtu_t d, const Rational& q);

}


#endif /* RTU_hpp */

/// @}

# src/segment/export


Facilities for writing the data given in input to the quantization by parsing algorithm, i.e.
* sequence of musical events timestamped in Real-Time Units (`InputSegment`).
* sequence of IOI in Musical-Time Units (`ValueList` or `DurationList`).

The input segments (updated with quantized dates after quantization) can be exported (written) as a MIDI file by `MIDIOutput`.

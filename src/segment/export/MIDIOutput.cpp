//
//  MIDIOutput.cpp
//  squanty
//
//  Created by Florent Jacquemard on 24/03/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "MIDIOutput.hpp"
#include "InputSegmentMIDI.hpp"


MIDIExporter::MIDIExporter(std::string filename_out,
                           Rational beatperbar):
_filename_out(filename_out),
_beatperbar(beatperbar)
{
    assert(! _filename_out.empty());
    assert(_beatperbar > Rational(0));
}


size_t MIDIExporter::export_midifile(const InputSegmentMIDI& seg)
{
    if (seg.status() != 0)
    {
        ERROR("MIDI export: input segment was not imported succesfully from      {}",
              seg.filename());
        return 2;
    }
    return export_midifile(seg,
                           seg.filename(),
                           seg.track());
}
    

size_t MIDIExporter::export_midifile(const InputSegment& seg,
                                     std::string filename_in,
                                     int tracknb)
{
    DEBUGU("MIDI export: read origin MIDI info from input MIDI file: {}",
           filename_in);
    
    if (filename_in.empty())
    {
        ERROR("name of input MIDI file missing, cannot export as MIDI file");
        return 1;
    }
    
    MidiFile input_midifile;
    input_midifile.read(filename_in);
    
    if (input_midifile.status() == 0)
    {
        ERROR("Error reading input MIDI file {}", filename_in);
        return 1;
    }

    return export_midifile_mono(seg,
                                input_midifile,
                                filename_in,
                                tracknb);
}


// TBC
size_t MIDIExporter::export_midifile_mono(const InputSegment& seg,
                                     MidiFile& input_midifile,
                                     std::string filename_in,
                                     int tracknb)
{
    //convert delta ticks values for all MidiEvents into absolute tick times
    input_midifile.absoluteTicks();
    input_midifile.joinTracks();
    input_midifile.linkNotePairs();
    
    // delta time ticks units occuring during a bar
    // TBC
    Rational tpb =
        Rational(input_midifile.getTicksPerQuarterNote()) * _beatperbar;
    
    int eventcount = input_midifile.getEventCount(0);
    
    unsigned int lastkey = 128; // 0-127 = MIDI key number
    
    TRACE("input MIDIfile has {} tracks", input_midifile.getNumTracks());
    TRACE("input MIDIfile has {} joint tracks",
         (input_midifile.hasJoinedTracks())?"":"not");
    TRACE("input MIDIfile: Ticks per Quarter Note={}",
         input_midifile.getTicksPerQuarterNote());
    
    // all tracks merged into track 0
    if (! input_midifile.hasJoinedTracks())
    {
        WARN("input MIDIfile has not joined tracks, read track 0");
        tracknb = 0;
    }
    
    TRACE("# MIDI events={}", eventcount);

    // index and track in event list of input_midifile
    int i = 0;
    int track;

    // index in event list of input segment
    size_t j = 0;

    
    while (i < eventcount)
    {
        TRACE("process MIDI event #{}", i);
        MidiEvent& e = input_midifile[0][i];
        track = e.track;
        
        // ignore messages on other tracks
        if (track != tracknb)
        {
            WARN("event {} track {} ignored", i, track);
            i++;
            continue;
        }
        
        // new note
        if (e.isNoteOn())
        {
            if ( j >= seg.size() )
            {
                WARN("MIDI note {} missing in input segment", i);
                i++;
                continue;
            }
            
            const MusPoint& point = seg.point(j);
            assert(point.event());
            // MIDI note is skip in input segment
            if (point.event()->number != i)
            {
                WARN("event {} not present in inputseg, skip", i);
                i++;
                continue;
            }
            // MIDI note synchronized with input segment
            // quantize the note on and corresponding note off MIDI events
            else
            {
                // constant copies
                const mtu_t mdate = point.mdate();
                const mtu_t mduration  = point.mduration();
                assert(MTU::defined(mdate));
                assert(MTU::finite(mdate));
                assert(MTU::defined(mduration));
                assert(MTU::finite(mduration));
                TRACE("quantize NOTEON[{}] {}: {}->{}",
                      i, e.getKeyNumber(),
                      e.tick,
                      floor(mdate*tpb));
                e.tick = floor(mdate*tpb);
                MidiEvent* elink = e.getLinkedEvent();
                if (elink == NULL)
                {
                    WARN("unlinked note-on event {}[{}]", filename_in, i);
                }
                else
                {
                    TRACE("quantize NOTEOFF[{}] {}: {}->{}",
                          i, elink->getKeyNumber(),
                          elink->tick,
                          floor(mdate*tpb + mduration*tpb));
                    elink->tick = floor(mdate*tpb + mduration*tpb);
                }
                i++;
                j++;
            }
            lastkey = e.getKeyNumber();
            continue;
        }
        // new rest
        else if (e.isNoteOff() && e.getKeyNumber() == lastkey)
        {
            assert (0 <= lastkey);
            assert (lastkey <= 127);
            // may be a rest event in cast of no norest
            if ( j < seg.size() )
            {
                const MusPoint& point = seg.point(j);
                assert(point.event());
                if (point.event()->isRest() && point.event()->number == i)
                {
                    j++;
                }
            }
            i++;
            continue;
        }
        // event ignored
        else
        {
            TRACE("MIDI event {} track {} ignored", i, track);
            i++;
            continue;
        }
    }
    
    input_midifile.splitTracks();
    input_midifile.sortTracks();
    input_midifile.write(_filename_out);
    
    return 0;
}




//
//  MIDIOutput.hpp
//  squanty
//
//  Created by Florent Jacquemard on 24/03/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup output
/// @{

#ifndef MIDIOutput_hpp
#define MIDIOutput_hpp

#include <stdio.h>

#include "trace.hpp"
#include "Rational.hpp"
#include "InputSegment.hpp"
#include "InputSegmentMIDI.hpp"


/// export the content of an input segment to a MIDI file,
/// using the input MIDI file from which this segment has been built.
class MIDIExporter
{
public:
    
    MIDIExporter(std::string filename_out,
                 Rational beatperbar);
     
    /// @brief write in a new MIDI file the content of the given
    /// input MIDI file updated with the quantized MTU dates in this segment.
    /// @param seg input segment to export. must be quantized,
    /// i.e. the musical date and duration must have been set
    /// in this segment by a call to quantize().
    /// @param filename_in name of input MIDI file.
    ///     The segment must have been created from this MIDI file.
    /// @param tracknb track number used for importing from MIDI file.
    size_t export_midifile(const InputSegment& seg,
                           std::string filename_in,
                           int tracknb);

    /// @brief write in a new MIDI file the content of the given
    /// input MIDI file updated with the quantized MTU dates in this segment.
    /// @param seg input segment to export.
    /// must have been imported succesfully (status = 0) from MIDI file.
    /// must be quantized.
    size_t export_midifile(const InputSegmentMIDI& seg);
    
private:
    
    /// @brief write in the given MIDI file the content of the given
    /// input MIDI file updated with the quantized MTU dates in this segment.
    /// @param seg input segment to export. must be quantized.
    /// @param midifile MIDIfile struct in input.
    /// @param filename_in name of input MIDIfile.
    /// This segment must have been created from this MIDI file.
    /// @param tracknb track number used for importing from MIDI file
    /// @warning the segment must have been created from a midi file.
    /// @warning the musical date and duration must have been set in this segment.
    /// @todo TBR mv export to segment/InputSegment* classes
    size_t export_midifile(const InputSegment& seg,
                           MidiFile& midifile,
                           std::string filename_in,
                           int tracknb);
    
    /// @brief copy input midifile into output_midifile, monophonic case.
    /// update the onsets / offsets to the quantized values in this segment.
    /// @param seg input segment to export. must be quantized.
    /// @param midifile MIDIfile struct
    /// @param filename_in name of input MIDIfile. This segment must have been created from this MIDI file.
    /// @param tracknb track number used for importing from MIDI file
    /// @warning this segment must have been created from a midi file.
    /// @warning the musical date and duration must have been set in this segment.
    /// @todo TBR mv export to segment/InputSegment* classes
    size_t export_midifile_mono(const InputSegment& seg,
                                MidiFile& midifile,
                                std::string filename_in,
                                int tracknb);
    
    
private:
    /// @brief name of MIDI file for exporting the input segment
    std::string _filename_out;

    /// @brief number of beats per measure.
    Rational _beatperbar;
    
};

#endif /* MIDIOutput_hpp */

/// @}

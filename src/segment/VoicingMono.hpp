//
//  Voicing.hpp
//  squanty
//
//  Created by Florent Jacquemard on 19/03/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Voicing.hpp"


/// implementation of a trivial Voicing for the case of
/// monophonic input segments.
/// The blocks are bars.
class VoicingMono : public Voicing
{
public:

    /// a voicing is attached to a unique input segment,
    /// that cannot be changed.
    /// @param seg associated input segment to be voiced. must be quantized.
    /// its content (MusPoint) can be modified (voice numbers computation).
    VoicingMono(InputSegment& seg);
    
    //~VoicingMono();

    /// @brief set the voice numbers of events in the associated
    /// input segment, to product a correct voicing.
    /// support segment will be modified.
    /// @see Voicing.revoice() for details.
    virtual void revoice_all();

    
    virtual void revoice_online();

    
    /// @brief set the voice numbers of events in the given bar
    /// of the associated input segment, to product a correct voicing
    /// for that bar.
    /// @param i a bar number. must be smaller that the total number of bars
    /// in the support input segment.
    /// @warning only the bar i of the support segment will be modified.
    //virtual void revoice(size_t i);

};


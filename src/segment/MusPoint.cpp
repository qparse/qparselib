//
//  MusPoint.cpp
//  qparse
//
//  Created by Florent Jacquemard on 10/03/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#include "MusPoint.hpp"


int VOICE_UNDEF = INT_MIN; // limits.h


// event can be NULL
MusPoint::MusPoint(MusEvent* e, rtu_t rdate, bool on,
                   long link, long backlink,
                   mtu_t mdate, mtu_t mduration,
                   int voice):
Point(e, rdate, on, link, backlink),
_mdate(mdate),
_mduration(mduration),
_voicenb(voice)
{
    assert(e != NULL);
}


// event can be NULL
MusPoint::MusPoint(const Point& p,
                   mtu_t mdate, mtu_t mduration,
                   int voice):
Point(p),
_mdate(mdate),
_mduration(mduration),
_voicenb(voice)
{
    assert (mdate >= 0);
    assert (mduration >= 0);
}


MusPoint::MusPoint(const MusPoint& p):
Point(p),
_mdate(p._mdate),
_mduration(p._mduration),
_voicenb(p._voicenb)
{
    // event cloned in Point copy
}


MusPoint::~MusPoint()
{
    // event deleted by Point destructor
}


MusPoint& MusPoint::operator= (const MusPoint& p)
{
    if(this != &p)
    {
        Point::operator=(p);
// Point assignement
//        if (p._event)
//            _event = p._event->clone();
//        else
//            _event = NULL;
//        _onset = p._onset;
//        _duration = p._duration;
        _mdate = p._mdate;
        _mduration = p._mduration;
        _voicenb = p._voicenb;
    };
    
    return *this;
}


bool MusPoint::operator==(const Point& p) const
{
    if (! Point::operator==(p))
        return false;
    
    return ((_mdate == static_cast<const MusPoint&>(p)._mdate) &&
            (_mduration == static_cast<const MusPoint&>(p)._mduration) &&
            (_voicenb == static_cast<const MusPoint&>(p)._voicenb));
}


void MusPoint::set_mdate(mtu_t d)
{
    assert(MTU::finite(d));
    assert(MTU::positive(d) || MTU::null(d));
    _mdate = d;
}


void MusPoint::set_mduration(mtu_t d)
{
    assert(MTU::finite(d));
    assert(MTU::positive(d) || MTU::null(d));
    _mduration = d;
}

void MusPoint::set_voice(int v)
{
    assert(v != VOICE_UNDEF);
    _voicenb = v;
}


void MusPoint::print(std::ostream& o) const
{
    MusEvent* evt = event();
    
    //INFO(":<15", _rdate); // left justify, width=15
    o << std::setw(8) << std::setiosflags(std::ios::left);
    o << _rdate << " ";
    
    if (_onoff)
        o << " NOTE_ON ";
    else
        o << " NOTE_OFF";
    
    if (_event)
        o << " " << *_event;
    else
        o << "_";

    if (link != POINTREF_NULL)
        o << " link=" << link;

    if (backlink != POINTREF_NULL)
        o << " backlink=" << backlink;  
    
    if (_voicenb != VOICE_UNDEF)
        o << " voice:" << _voicenb;

    if (MTU::defined(_mdate))
    {
        o << " mdate=";
        _mdate.printint(o);
        //o << " bars";
    }
    if (MTU::defined(_mduration))
    {
        o << " mdur=";
        _mduration.printint(o);
        //o << " bars";
    }
    if (evt != nullptr)
    {
        o << " event=<";
        o << *evt;
        o << ">";
    }
    else
    {
        o << " event=NULL";
    }

}


/// @}

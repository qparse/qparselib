//
//  AMTU.hpp
//  qparse
//
//  Created by Florent Jacquemard on 16/02/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{



#ifndef AMTU_hpp
#define AMTU_hpp

#include <stdio.h>
#include <assert.h>
#include <vector>

#include "trace.hpp"
#include "hash.hpp"
#include "Rational.hpp"


/// time values expressed in musical time units
/// (1 mtu = 1 bar)
///
/// expressed in nomber of bars + number of samples
/// on the base of RESOLUTION samples per bar.
///
/// the value of RESOLUTION
/// is not stored in objects of this class.
/// It must be fixed and used externally for conversions.
class AMTU
{
public:
    AMTU();
    
    /// @param bars number of bars before date
    /// @param rel temporal position, in samples,
    ///            inside the last bar, in [0, RESOLUTION].
    AMTU(size_t bars, size_t rel);
    
    /// copy constructor
    AMTU(const AMTU&);
    
    ~AMTU() {}
    
    virtual AMTU& operator=(const AMTU&);
    
    virtual AMTU* clone() const;
    
    /// number of bars
    inline size_t measure() const
    { return _quotient; }
    
    /// mtu position (in samples) indide the bar (= modulo bar length)
    inline size_t inside() const
    { return _modulo;}

    
    bool operator==(const AMTU&) const;

    /// @param rhs other mtu date
    /// @warning no check wether number of samples (inside-the-bar part) exceed resolution.
    const AMTU& operator+=(const AMTU& rhs);

    /// @param rhs other mtu date
    /// @warning the number of samples (inside-the-bar part) of rhs must be >= for this.
    /// @warning the number of bars of rhs must be >= for this.
    const AMTU& operator-=(const AMTU& rhs);
    
    /// mtu date as rational value.
    /// @param resolution length of 1 bar (in samples)
    /// @warning the resolution must be constant
    virtual Rational absolutetoRatio(size_t resolution) const;

    /// mtu date inside-the-bar as rational value.
    /// @param resolution length of 1 bar (in samples)
    /// @warning the resolution must be constant inside the last bar
    virtual Rational insidetoRatio(size_t resolution) const;
    
    /// display the number of bars and position inside the bar.
    /// @param o output stream
    void print(std::ostream& o) const;
    
    /// fractional print using resolution value.
    /// @param o output stream
    /// @param resolution length of 1 bar (in samples)
    void print(std::ostream& o, size_t resolution) const;
    
    friend inline std::ostream& operator<<(std::ostream&, const AMTU&);
    
protected:
    
    /// number of bars in date
    /// = date in samples / RESOLUTION
    size_t _quotient;;
    
    /// number of samples inside-the-bar, in [0, RES]
    /// = date in samples modulo RESOLUTION
    size_t _modulo;;
    
};


const AMTU operator+(const AMTU& lhs, const AMTU& rhs)
{
    return AMTU(lhs) += rhs;
}


const AMTU operator-(const AMTU& lhs, const AMTU& rhs)
{
    return AMTU(lhs) -= rhs;
}


inline std::ostream& operator<<(std::ostream& o, const AMTU& rhs)
{
    rhs.print(o);
    return o;
}

namespace std
{
    template<> class hash<AMTU>
    {
    public:
        std::size_t operator()(const AMTU& x) const
        {
            using std::hash;
            std::size_t h = HASH_SEED;
            h = REHASH(h, hash<long>()(x.measure()));
            h = REHASH(h, hash<long>()(x.inside()));
            return POSTHASH(h);
        }
    };
}


/// time values expressed in musical time units
/// (1 mtu = 1 bar)
///
/// expressed in number of samples
/// on the base of RESOLUTION samples per bar.
///
/// the value of RESOLUTION is stored in each object of this class.
/// It must be constant inside each bar.
class MTU: public AMTU
{
public:
    /// origin date
    /// @param res number of samples in the current bar
    MTU(size_t res);
    
    /// @param bars number of bars before date
    /// @param rel temporal position, in samples,
    ///            inside the last bar, in [0, res].
    /// @param res number of samples in the last bar.
    MTU(size_t bars, size_t rel, size_t res);
    
    /// copy constructor
    MTU(const MTU&);
    
    ~MTU() {}
    
    virtual MTU& operator= (const MTU&);
    
    virtual MTU* clone() const;
    
    bool operator==(const MTU&) const;
    
    /// number of samples in the current bar
    inline size_t resolution() const
    { return _resolution;}
    
    /// @param rhs other mtu date
    /// @warning the resolution of the result is the resolution of rhs
    const MTU& operator+=(const MTU& rhs);
    
    /// @param rhs other mtu date
    /// @warning the number of samples (inside-the-bar part) of rhs must be >= for this.
    /// @warning the number of bars of rhs must be >= for this.
    const MTU& operator-=(const MTU& rhs);

    
    /// mtu date inside-the-bar as rational value.
    /// @warning the resolution must be constant inside the last bar
    Rational insidetoRatio() const;
    
    /// display the number of bars and position inside the bar.
    /// @param o output stream
    void print(std::ostream& o) const;
        
    friend inline std::ostream& operator<<(std::ostream&, const MTU&);
   
protected:
    
    /// number of samples in the current bar
    size_t _resolution;;
};


const MTU operator+(const MTU& lhs, const MTU& rhs)
{
    return MTU(lhs) += rhs;
}


const MTU operator-(const MTU& lhs, const MTU& rhs)
{
    return MTU(lhs) -= rhs;
}

inline std::ostream& operator<<(std::ostream& o, const MTU& rhs)
{
    rhs.print(o);
    return o;
}


namespace std
{
    template<>
    class hash<MTU>
    {
    public:
        std::size_t operator()(const MTU& x) const
        {
            using std::hash;
            std::size_t h = REHASH(hash<AMTU>()(x), hash<long>()(x.resolution()));
            return POSTHASH(h);
        }
    };
}

#endif /* AMTU_hpp */


/// @}

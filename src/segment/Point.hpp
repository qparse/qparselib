//
//  Point.hpp
//  qparse
//
//  Created by Florent Jacquemard on 05/02/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{

#ifndef Point_hpp
#define Point_hpp

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>

#include "RTU.hpp"
#include "MusEvent.hpp"

extern long POINTREF_NULL;


/// @brief timestamped event.
///
/// event extended with
/// - realtime date (in seconds),
/// - on/off attribute,
/// - optional [on-off] link.
///
/// if on/off attibute is true, the point is called onset or note-on.
/// otherwise it is called offset or note-off.
///
/// the [on-off] link is an index of an associated point in an input segment.
///
/// for the computation of realtime duration of points, see @ref InputSegment.
class Point
{
public:
    
    
    /// @brief timestamped monophonic or polyphonic event.
    Point(MusEvent* e,
          rtu_t rdate,
          bool on,
          long link=POINTREF_NULL,
          long backlink=POINTREF_NULL);


    /// @warning event (if any) is cloned.
    Point(const Point&);

     /// @warning event is deallocated and matcher (linked) also.
    ~Point();
    
     /// @warning event (if any) is cloned.
    virtual Point& operator=(const Point&);
    
    virtual bool operator==(const Point&) const;
    
    /// @warning can be NULL.
    MusEvent* event() const
    { return _event; }
    
    inline rtu_t rdate() const { return _rdate; }

    /// @brief is note on.
    inline bool onset() const { return _onoff; }
    //{ return (linked != POINTREF_NULL); }

    /// @brief is note off.
    inline bool offset() const { return(! onset()); }

    /// @brief link to a point in an input segment.
    /// It is an index in an input segment structure:
    /// - a point of segment if >= 0,
    /// - or a floating point if < 0.
    ///
    /// If this point is an onset, its link is an index
    /// - of the nearest posterior onset point
    ///   in case of segment monophonic without rests
    ///   (with a realtime date larger or equal).
    /// - of the matching posterior offset point in other cases.
    ///
    /// If this point is an offset, its link is the index
    /// of the nearest postterior onset point
    /// (with a realtime date larger or equal).
    /// i.e. it marks the end of the silence possibly starting at this offset.
    long link;
    
    /// If this point is an offset,
    /// its backlink is an index of an anterior onset point,
    /// (with a realtime date smaller or equal).
    long backlink;

    
    friend inline std::ostream& operator<<(std::ostream& o, const Point& rhs);
    
protected:
    /// @brief input event.
    MusEvent* _event;

    /// @brief timestamp in real-time (sec).
    rtu_t _rdate;

    /// @brief flag true if note-on, false if note-off.
    bool _onoff;
        
    virtual void print(std::ostream& o) const;
};


std::ostream& operator<<(std::ostream& o, const Point& rhs)
{
    rhs.print(o);
    return o;
}


#endif /* Point_hpp */


/// @}

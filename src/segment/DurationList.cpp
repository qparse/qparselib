//
//  DurationList.cpp
//  qparse
//
//  Created by Florent Jacquemard on 02/11/16.
//  Copyright © 2016 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#include "DurationList.hpp"

// empty duration list
DurationList::DurationList():
_continuation(MTU::ZERO),
_main(),
_sum(0.0),
_summed(0),
_dummy(1),
_state(0)
{
    assert(this->empty());
}


DurationList::DurationList(const DurationList& l):
_continuation(l._continuation),
_main(l._main),
_sum(l._sum),
_summed(l._summed),
_dummy(l._dummy),
_state(l._state)
{
    assert(MTU::defined(l._continuation));
}


DurationList& DurationList::operator=(const DurationList& other)
{
    if (this != &other) // protect against invalid self-assignment
    {
        _continuation = other._continuation;
        _sum = other._sum;
        _summed = other._summed;
        _dummy = other._dummy;
        _state = other._state;
        
        _main.assign(other._main.begin(), other._main.end()); // copy of _main
    }
    return *this;
}


//bool DurationList::operator==(const DurationList& rhs) const
//{
//    if (_continuation != rhs._continuation)
//        return false;
//
//    if (_main.size() != rhs._main.size())
//        return false;
//
//    std::list<mtu_t>::const_iterator li =  _main.cbegin();
//    std::list<mtu_t>::const_iterator ri =  rhs._main.cbegin();
//
//    while (li != _main.cend())
//    {
//        if (*li != *ri)
//            return false;
//        ++li;
//        ++ri; // lists have same size
//    }
//
//    assert(_sum == rhs._sum);
//    assert(_state == rhs._state);
//    return true;
//}


DurationList::DurationList(const DurationList& l, mtu_t q):
_main(),
_sum(l._sum * q),
_summed(l._summed),
_dummy(l._dummy),
_state(l._state)
{
    assert(MTU::defined(l._continuation));

    _continuation = l._continuation * q;
    for(std::list<mtu_t>::const_iterator i = l._main.begin();
                                            i != l._main.end(); ++i)
    {
        _main.push_back((*i) * q);
    }
}


bool DurationList::empty() const
{
    assert((_state != 0) || (_continuation.null() && _main.empty()));
    assert((_state != 7) || (_continuation.null() && _main.empty()));
    
    return ((_state == 0) || (_state == 7));
}


bool DurationList::_check_empty() const
{
    return (_summed + _dummy == 1) &&
            _continuation.null() && _main.empty();
}


bool DurationList::complete() const
{
    return ((_state == 1) || (_state == 3) || (_state == 4) || (_state == 6));
}

            
bool DurationList::_check_complete() const
{
    return (_sum == _summed);
}


bool DurationList::unit() const
{
    return (_state == 1) || (_state == 3) || (_state == 4);
}


bool DurationList::_check_unit() const
{
    if (empty())
        return false;
    
    assert(MTU::defined(_continuation));

    if (_continuation == MTU::ONE)
    {
        assert(_main.size() == 0);
        return true;
    }
    
    if (_continuation.null())
    {
        for (std::list<mtu_t>::const_iterator i = _main.begin();
             i != _main.end(); i++)
        {
            // grace note
            if ((*i).null())
                continue;
            // single event in interval
            else if ((*i) == MTU::ONE)
            {
                assert(++i == _main.end());
                return true;
            }
            // shorter event
            else
                return false;
        }
    }
    
    return false;
}


bool DurationList::single_continuation() const
{
    return (_state == 1);
}

bool DurationList::_check_single_continuation() const
{
    return (( _main.empty()) &&
            (! _continuation.null()) &&
            (_continuation == MTU::make(_summed)));
}


bool DurationList::single_event() const
{
    return (_state == 3);
}


bool DurationList::_check_single_event() const
{
    return ((_continuation.null()) &&
            (_main.size() == 1) &&
            (_main.front() == MTU::make(_summed)));
}


bool DurationList::event() const
{
    return (_state == 4);
}


bool DurationList::_check_event() const
{
    if ( _continuation.null() && (_main.size() >= 1))
    {
        // last element in main
        std::list<mtu_t>::const_reverse_iterator i = _main.crbegin();
        assert(i != _main.crend());
        assert(MTU::defined(*i));
        // sequence of grace notes followed by non null event
        if ( *i == MTU::make(_summed) )
            ++i;
        else
            return false;

        // other elements in main are 0
        while (i != _main.crend())
        {
            assert(MTU::defined(*i));
            if (! MTU::null(*i) ) return false;
            ++i;
        }
        return true;
    }
    else
    {
        return false;
    }
}

bool DurationList::_check_gracenotes() const
{
    if ( _continuation.null() && (_main.size() >= 1))
    {
        // sequence of grace notes
        for (std::list<mtu_t>::const_iterator i = _main.cbegin();
             i != _main.cend();
             ++i)
        {
            assert(MTU::defined(*i));
            if (! MTU::null(*i)) return false;
        }
        return true;
    }
    else
    {
        return false;
    }
}


bool DurationList::_check_state() const
{
    
    switch (_state)
    {
        case 0:
        case 7:
            return _check_empty();
            break;
        case 1:
            return _check_single_continuation();
            break;
        case 2:
            return _check_gracenotes();
            break;
        case 3:
            return _check_single_event();
            break;
        case 4:
            return _check_event();
            break;
        case 5:
            return (! _check_complete());
            break;
        case 6:
            return _check_complete();
            break;
    }
    
    return (true);
}


size_t DurationList::nbgn() const
{
    assert( _continuation.null());
    assert(_main.size() >= 1);
    std::list<mtu_t>::const_reverse_iterator i = _main.crbegin();
    assert(i != _main.crend());
    size_t res = 0;
    assert(*i > 0); // last element is the event
    // previouses are the grace notes
    while (i != _main.crend())
    {
        assert((*i).null());
        ++res;
        ++i;
    }
    return res;        
}

// not used
//mtu_t DurationList::_compute_length() const
//{
//    mtu_t sum = _continuation;
//    assert(MTU::defined(sum));
//    for(std::list<mtu_t>::const_iterator i = _main.cbegin();
//        i != _main.cend(); ++i)
//    {
//        assert(MTU::defined(*i));
//        sum += *i;
//    }
//    return sum;
//}


mtu_t DurationList::length() const
{
    assert(MTU::defined(_continuation));

    return (_sum);
}


// used only for terminal runs
void DurationList::add(mtu_t q)
{
    assert(MTU::defined(q));
    assert(MTU::ZERO <= q);
    assert(q <= MTU::ONE);
    assert(_summed + _dummy == 1);
    
    if ((_summed == 0) && (_dummy == 1))
    {
        _summed = 1;
        _dummy = 0;
    }
    else
    {
        assert(_summed == 1);
        assert(_dummy == 0);
    }
    
    mtu_t tmp_sum = _sum + q;

    assert(tmp_sum >= 0);
    assert(tmp_sum <= 1);  // if (tmp_sum > 1) return false;
    
    // complete cases
    // we cannot add duration, even if 0
    // because we assume grace notes always before note
    assert(! complete());
    
    switch (_state){
        case 0:
        case 7:
            if (q == MTU::ZERO)
                _state = 2;
            else if (q == MTU::ONE)
                _state = 3;
            else
                _state = 5;
            break;
        case 1:
        case 3:
        case 4:
        case 6:
            // we can add no grace note here (terminal node)
            assert(false); // return false
            break;
        case 2:
            if (q == MTU::ZERO)
                _state = 2;
            else if (q == MTU::ONE)
                _state = 4;
            else
                _state = 5;
            break;
        case 5:
            if (tmp_sum < 1)
                _state = 5;
            else
                _state = 6;
            break;
    }

    
    _sum = tmp_sum;
    _main.push_back(q);

    // @todo TBR
    assert(_check_state());
    // return true
}


void DurationList::addcont(mtu_t q)
{
    assert(MTU::ZERO <= q);
    assert(q <= MTU::ONE);
    assert(_summed + _dummy == 1);
    
    if ((_summed == 0) && (_dummy == 1))
    {
        _summed = 1;
        _dummy = 0;
    }
    else
    {
        assert(_summed == 1);
        assert(_dummy == 0);
    }

    assert(MTU::ZERO <= _continuation);
    assert(_continuation <= MTU::ONE);

    mtu_t tmp_sum = _sum + q;
    
    assert(0 <= tmp_sum);
    assert(tmp_sum <= 1);    //  if (tmp_sum > 1) return false;

    switch (_state){
        case 0:
        case 7:
            if (q == MTU::ONE)
                _state = 1;
            else
                _state = 5;
            break;
        // complete cases
        // we cannot add duration
        case 1:
        case 3:
        case 4:
        case 6:
            assert(q == MTU::ZERO);  // return false;
            break;
        case 2:
            assert(q < MTU::ONE); // return false;
            _state = 5;
            break;
        case 5:
            if (tmp_sum < 1)
                _state = 5;
            else
                _state = 6;
            break;
    }
    
    _continuation += q;
    _sum = tmp_sum;

    assert(_continuation <= 1);
    //  @todo TBR
    assert(_check_state());
    //return true;
}


DurationList& DurationList::operator+=(const DurationList& rhs)
{
    assert(MTU::defined(_continuation));
    assert(MTU::defined(rhs._continuation));
    assert(rhs._summed + rhs._dummy == 1);
    assert(empty() || complete());
    
    
    // (_state == 1) || (_state == 3) || (_state == 4) || (_state == 6);
    // or concatenate 2 empty lists : TBC
    assert(rhs.complete() || (this->empty() && rhs.empty()));
    
    unsigned int _state_original = _state;
    
    // update state
    // note that the new d.l. is complete
    switch (_state){
        case 0:
            assert(_main.empty());
            switch (rhs._state){
                case 0:
                    _state = 7;
                    break;
                default:
                    _state = rhs._state;
                    break;
            }
            break;
        case 7:
            assert(_main.empty());
            switch (rhs._state){
                case 0:
                    _state = 7;
                    break;
                default:
                    _state = rhs._state;
                    break;
            }
            break;
        case 1:
            assert(_main.empty());
            assert(_continuation == _summed);
            switch (rhs._state){
                case 0:
                    _state = 1;
                    break;
                case 1:
                    _state = 1;
                    break;
                case 3:
                case 4:
                case 6:
                    _state = 6;
                    break;
            }
            break;
        case 3:
            assert(! _main.empty());
            switch (rhs._state){
                case 1:
                    _state = 3;
                    break;
                case 3:
                case 4:
                case 6:
                    _state = 6;
                    break;
            }
            break;
        case 4:
            assert(! _main.empty());
            switch (rhs._state){
                case 1:
                    _state = 4;
                    break;
                case 3:
                case 4:
                case 6:
                    _state = 6;
                    break;
            }
            break;
        case 6:
            assert(! _main.empty());
            _state = 6;
            break;
    }

    if (_main.empty())
    {
        _continuation += rhs._continuation;
    }
    else
    {
        std::list<mtu_t>::reverse_iterator last = _main.rbegin();
        assert(last != _main.rend());
        *last += rhs._continuation;
    }
    
    _main.insert(_main.end(), rhs._main.begin(), rhs._main.end());
    _sum += rhs._sum;
    
    if (_state_original == 0)
    {
        assert(_summed == 0);
        assert(_dummy == 1);
        assert(_state != 0);
        _dummy = 0;
    }
    if (rhs._state == 0)
    {
        _dummy++;
    }
    else
    {
        _summed++;
    }
    
    // @todo TBR
    assert(_check_state());

    return *this;
}


//void DurationList::mult(mtu_t q)
//{
//    assert(MTU::defined(_continuation));
//    assert(MTU::defined(q));
//
//    _continuation *= q;
//    // multiply every element of _main in place
//    for(mtu_t &x : _main) { x *= q; }
//}


void DurationList::normalize()
{
    // only dummies were concatenated - should not happen
    if (_summed == 0)
    {
        assert(1 <= _dummy);
        assert(_sum == MTU::ZERO);
        assert(empty());
        assert(_state == 7);
        _dummy = 1;
    }
    else
    {
        assert(1 <= _summed);
        assert(_sum == MTU::make(_summed));
        assert(_state != 0);
        assert(_state != 7);

        _continuation /= _summed;
        // normalize every element of _main in place
        for(mtu_t &x : _main)
        {
            x /= _summed;
        }
        _sum = MTU::ONE;
        _summed = 1;
        _dummy = 0;
        // note that _state is unchanged
    }
}


std::ostream& operator<<(std::ostream& o, const DurationList& l)
{
    o << '[';
    assert(0 <= l._continuation);
    if (0 < l._continuation)
    {
        o << " " << -(l._continuation);
    }
    for(std::list<mtu_t>::const_iterator i = l._main.cbegin();
        i != l._main.cend();
        ++i)
    {
        assert(*i >= 0);
        o << " " << *i;
    }
    o << " ]";
#ifdef DEBUG_ON
    o << "_" << l._state;
    //o << l._sum << "/" << l._summed;
#endif

    return o;
}


/// @}

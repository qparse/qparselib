//
//  Voicing.hpp
//  squanty
//
//  Created by Florent Jacquemard on 19/03/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#ifndef Voicing_hpp
#define Voicing_hpp

#include <stdio.h>
#include <assert.h>

#include "InputSegment.hpp"

// such that every two points that sound in the same time
// belong to the same chord (i.e. they start and end at the same time).
  
/// @brief a Voicing is a mapping of each point of an input segment
/// into a voice number, such that:
/// - every voice is monophonic:
///   for all musical time point t, every two distinct points
///   sounding at time t belong to the same chord
///   (i.e. they start and end at the same musical time).
//   for each voice v, for all musical time point t,
//   there is at most one event e sounding at t such that
//   the voice of e is v.
/// - there exists a partial ordering on voices such that:
///   v1 < v2 for 2 voices iff
///   for all musical time t and points p1 and p2 sounding at t
///   such that v1 is the voice of p1 and v2 is the voice of p2,
///   then pitch(p1) < pitch(p2).
///   rem: the pitch of point in a chord or trill is the pitch of the bass.
///
/// the voicing of an InputSegment is implemented by setting
/// the voice information of each MusPoint in the segment.
/// This abstract class is intended to be derived into
/// several voicing algorithms.
class Voicing
{
public:
    
    /// a voicing is attached to a unique input segment,
    /// that cannot be changed.
    /// @param seg associated input segment to be voiced. must be quantized.
    /// its content (MusPoint) can be modified (voice numbers computation).
    Voicing(InputSegment& seg);
    
    ~Voicing();
    
    
    /// Number of points in the associated input segment
    /// which were already voiced;
    /// i.e. the points 0 to voiced()-1 have a voice number.
    virtual bool voiced();

    /// @brief set the voice numbers of all the points in the
    /// associated input segment to product a conform voicing.
    /// The size of blocks is up to the implementation.
    /// @warning eventual previously set voice values of points in the
    /// segment will be overwritten.
    virtual void revoice_all() = 0;
    
    /// @brief set the voice numbers of points in the
    /// associated input segment to product a conform voicing.
    /// Only the points not already voiced will be modified, i.e. the
    /// points from voiced() to the current end of segment.
    /// The sequence of newly voiced points is called a block,
    /// accessible by voices().
    /// @warning typically, call revoice_onnline() after adding one bar
    /// to the segment, in order to have 1 block = 1 bar.
    virtual void revoice_online() = 0;
    
    // @brief set the voice numbers of events in the given bar
    // of the associated input segment, to product a correct voicing
    // for that bar.
    // @param i a bar number. must be smaller that the total number of bars
    // in the support input segment.
    // @warning only the bar i of the support segment will be modified.
    //virtual void revoice(size_t i) = 0;
    
    /// @brief vector of voices for block of given number added by revoice.
    /// Voices are enumerated by increasing order in the vector.
    /// @param i block number.
    /// must be strictly smaller that number of previous calls to revoice().
    /// @warning a good practice is to have each block corresponding to a bar.
    virtual std::vector<int>& voices(size_t i);


    
protected:
    /// associated input segment
    InputSegment& _support;

    /// total number of voices.
    size_t _voices;
    
    /// number of points in already voiced in input segment.
    size_t _done;
    
    /// table with a vector of voices (by increasing order)
    /// for each bar voiced.
    std::vector<std::vector<int>> _blocks;
    
};




#endif /* Voicing_hpp */


/// @}

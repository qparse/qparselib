//
//  IntervalTree.cpp
//  qparse
//
//  Created by Florent Jacquemard on 11/03/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#include "IntervalTree.hpp"
#include "IntervalHeap.hpp"


// internal use
// TBR not used.
IntervalTree::IntervalTree(const InputSegment* s, mtu_t me, bool f_align):
AlignedInterval(s, me, f_align),
_parent(NULL),
_previous_sibling(NULL)
{
    assert(s);
}


// internal use
IntervalTree::IntervalTree(const InputSegment* s,
                           rtu_t rb, rtu_t re,
                           mtu_t mb, mtu_t me,
                           size_t first,
                           IntervalTree* p,
                           IntervalTree* ps):
AlignedInterval(s, rb, re, mb, me, first),
_parent(p),
_previous_sibling(ps),
_children()
{
    assert(s);
    // sanity check done in parent's constructor
}


// internal use
IntervalTree::IntervalTree(const InputSegment* s,
                           rtu_t rb, rtu_t re,
                           mtu_t mb, mtu_t me,
                           size_t first,
                           size_t next,
                           IntervalTree* p,
                           IntervalTree* ps):
AlignedInterval(s, rb, re, mb, me, first, next),
_parent(p),
_previous_sibling(ps),
_children()
{
    assert(s);
    // sanity check done in parent's constructor
}

IntervalTree::~IntervalTree()
{
    TRACE("delete IntervalTree {}", *this);
    // all the children IntervalTree* are deleted when heap is cleared
    _children.clear();
}


// static
IntervalTree* IntervalTree::top(const InputSegment* s,
                                IntervalHeap* ih,
                                mtu_t mend,
                                bool f_align)
{
    assert(s);
    assert(ih);
    
    return ih->make(s, mend, f_align);
}


IntervalTree* IntervalTree::split(const InputSegment* s,
                                  IntervalHeap* ih,
                                  rtu_t rdur,
                                  mtu_t mdur,
                                  size_t i)
{
    assert(s);
    assert(ih);
    assert (rdur > 0);
    assert (mdur > MTU::ZERO);
    assert (mopen() || mdur <= mduration());

    //assert (rdur0 <= this->rduration());
    assert((i == 1) || (i == 2));
    
    assert(rbegin() != RTU::UNBOUNDED);
    rtu_t rpivot = rbegin() + rdur;
    mtu_t mpivot = mbegin() + mdur;
    size_t j = this->first();
    assert(rbegin() < rpivot);

    IntervalTree* p[2];

    // first new sub-interval starting at rbegin of dur rdur
    // aligned
    TRACE("iTree.split(1) of interval {} = mtu[{}, {}[ rtu[{}, {}[",
          *(this), mbegin(), mpivot, rbegin(), rpivot);
    // p[0] is aligned
    p[0] = ih->make(s,
                    rbegin(), rpivot,
                    mbegin(), mpivot,
                    j,
                    this,
                    NULL); // no previous sibling
    assert(p[0]);
    assert(p[0]->first() == j);
    //j = p[0]->rewind(s); //p[0]->align(s);
    assert(p[0]->aligned());
    j = p[0]->next();
    
    // second new sub-alignment starting at rbegin+rdur
    // real-time duration null if start out of input segment
    // not aligned
    rtu_t rendnew;
    if (ropen() || (rpivot <= rend()))
        rendnew = rend();
    else
        rendnew = rpivot;

    TRACE("iTree.split(2) of interval {} = mtu[{}, {}[ rtu[{}, {}[",
          *(this), mpivot, mend(), rpivot, rendnew);
    // p[1] is not aligned
    p[1] = ih->make(s,
                    rpivot, rendnew,
                    mpivot, mend(),
                    j,
                    s->size(), // the right interval has no next
                    this,
                    p[0]);
    assert(p[1]);
    //p[1]->rewind(s);
    
    return p[i-1];
}


IntervalTree* IntervalTree::split_back(const InputSegment* s,
                                       IntervalHeap* ih,
                                       rtu_t rdur,
                                       mtu_t mdur,
                                       size_t i)
{
    assert(s);
    assert(ih);
    assert(! this->ropen());
    assert (rdur > 0);
    assert(! this->mopen());
    assert (mdur > MTU::ZERO);
    assert (mdur <= mduration());
    //assert (rdur0 <= this->rduration());
    assert ((i == 1) || (i == 2));
    
    size_t j = this->first();
    IntervalTree* p[2];
    
    //rtu_t rrest = (this->rduration() > rdur)?(this->rduration() - rdur):0.0;
    rtu_t rpivot = (rduration() > rdur)?(rbegin()+rdur):rbegin();
    assert(this->mduration() >= mdur);
    mtu_t mpivot = mbegin() + mdur;
    

    // first new sub-interval starting at rbegin
    // aligned
    TRACE("iTree.split_back(1) of meta {} = mtu[{}, {}[ rtu[{}, {}[",
          *(this), mbegin(), mpivot, rbegin(), rpivot);
    p[0] = ih->make(s,
                    rbegin(), rpivot,
                    mbegin(), mpivot,
                    j,
                    this, NULL);

    assert(p[0]);
    assert(p[0]->first() == j);
    //j = p[0]->rewind(s);
    assert(p[0]->aligned());
    j = p[0]->next();
    
    // second new sub-interval starting at rbegin of dur rdur
    // aligned
    TRACE("iTree.split_back(2) of meta {} = mtu[{}, {}[ rtu[{}, {}[",
          *(this),  mpivot, mdur, rpivot, rdur);
    p[1] = ih->make(s,
                    rpivot, rdur,
                    mpivot, mdur,
                    j,
                    this, p[0]); // no previous sibling
    assert(p[1]);
    //p[1]->rewind(s); //p[1]->align(s);
    
    return p[i-1];
}


IntervalTree* IntervalTree::sub(const InputSegment* s,
                                IntervalHeap* ih,
                                size_t a, size_t i)
{
    assert (a > 1);
    assert (1 <= i);
    assert (i <= a);
    assert(this->aligned());
    assert(! this->ropen());
    assert(! this->mopen());

    // creates an empty vector if it is not found
    std::pair<std::map<size_t, std::vector<IntervalTree*>>::iterator, bool>
    ret = _children.insert(std::pair<size_t,
                           std::vector<IntervalTree*>>(a,
                                                std::vector<IntervalTree*>()));
    std::vector<IntervalTree*>& v = ret.first->second;

    // v is a newly created vector for a, fill it with aligned sub intervals
    if (ret.second)
    {
        fillSubtrees(s, ih, v, a);
    }
    //  key a already present in tree
    else
    {
        assert(v.size() == a);
    }
    assert(v.size() == a);
    return v[i-1];
}


void IntervalTree::fillSubtrees(const InputSegment* s,
                                IntervalHeap* ih,
                                std::vector<IntervalTree*>& v,
                                size_t a)
{
    assert(v.size() == 0);
    size_t b = this->first();
    mtu_t mbeg = mbegin();
    mtu_t mdur = (mend() - mbegin()) / MTU::make(a);
    rtu_t rbeg = rbegin();
    rtu_t rdur = (rend() - rbegin()) / ((rtu_t) a);
    rtu_t rbeg_next;
    IntervalTree* sibling = NULL;
    IntervalTree* pt;

    for (int j = 0; j < a; j++)
    {
        // last sub-interval
        if (j == a-1)
        {
            rbeg_next = rend();
            assert(mbeg + mdur == mend());
            // not aligned
            pt = ih->make(s,
                          rbeg, rbeg_next,
                          mbeg, mbeg+mdur,
                          b,
                          //this->next(),
                          this, sibling);
            assert(pt);
            assert(pt->aligned());
            assert(pt->next() == this->next());
        }
        // sub-interval i < a-1
        else
        {
            rbeg_next = rbeg + rdur;
            pt = ih->make(s,
                          rbeg, rbeg_next,
                          mbeg, mbeg+mdur,
                          b,
                          this, sibling);
            assert(pt);
            if (pt->aligned())
                b = pt->next();
            else
                b = pt->align(s, b);
        }

        TRACE("iTree.sub({}/{}) of {} = mtu[{}, {}[ rtu[{}, {}[",
              j+1, a, *(this), mbeg, mbeg+mdur, rbeg, rbeg_next);

        v.push_back(pt);
        mbeg += mdur;
        rbeg = rbeg_next;
        sibling = pt;
    }

}


IntervalTree* IntervalTree::subm(const InputSegment* s,
                                 IntervalHeap* ih,
                                 size_t a, size_t i, size_t m)
{
    assert (a > 1);
    assert (1 <= i);
    assert (i <= a);
    assert(this->aligned());
    assert(! this->ropen());
    assert(! this->mopen());

    if (m == 1)
    {
        return sub(s, ih, a, i);
    }
    
    // first child, of duration m times the normal dur.
    if (i == 1)
    {
        size_t b = this->first();
        rtu_t rbeg = rbegin();
        rtu_t rdur = ((rtu_t) m) * (rend() - rbegin()) / ((rtu_t) a);
        mtu_t mbeg = mbegin();
        mtu_t mdur = MTU::make(m) * ((mend() - mbegin()) / MTU::make(a));
        TRACE("iTree.subm({}:{}/{}) of {} = mtu[{}, {}[ rtu[{}, {}[",
              i+1, m, a, *(this), mbeg, mbeg+mdur, rbeg, rbeg+rdur);

        return ih->make(s,
                        rbeg, rbeg+rdur,
                        mbeg, mbeg+mdur,
                        b,
                        this, NULL);
    }
    else
    {
        // previous sibling. we complete the tree for arity a.
        IntervalTree* prev = sub(s, ih, a, i-1);
        assert(prev);
        // immediately after the previous sibling and a duration m times more
        return prev->nexti(s, ih, Rational(m));
    }
}


IntervalTree* IntervalTree::nexti(const InputSegment* s,
                                  IntervalHeap* ih,
                                  const Rational& factor)
{
    size_t b = this->next();
    IntervalTree* sibling = this;
    rtu_t rbeg = this->rend();
    rtu_t rdur = RTU::mult(this->rduration(), factor);
    mtu_t mbeg = mend();
    mtu_t mdur = MTU::mult(this->mduration(), factor);
    assert(ih);
    // see Interval constructor next
    // Interval::Interval(const InputSegment*, const Interval&, Rational):
    IntervalTree* pt = ih->make(s,
                                rbeg, rbeg+rdur,
                                mbeg, mbeg+mdur,
                                b,
                                this->parent(), sibling);
    assert(pt);
    assert(pt->aligned());
        // if (! pt->aligned()) pt->align(s, b);
    TRACE("iTree.nexti of {} = mtu[{}, {}[ rtu[{}, {}[",
          *(this), mbeg, mbeg+mdur, rbeg, rbeg+rdur);

    return pt;
}


std::ostream& operator<<(std::ostream& o, const IntervalTree& p)
{
    o << static_cast<const Interval&>(p);
    
    return o;
}


/// @}

//
//  MTU.hpp
//  squant
//
//  Created by Florent Jacquemard on 06/01/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{

#ifndef MTU_hpp
#define MTU_hpp

#include <stdio.h>
#include <assert.h>
#include <limits.h>

#include "trace.hpp"
#include "Rational.hpp"


/// @brief Musical Time Unit.
/// time expressed in number of bars.
typedef Rational mtu_t;

namespace MTU
{

    /// unknown musical time value
    const mtu_t UNDEF = Rational(-1);

/// for backward compatibility. @todo TBR
    const mtu_t UNKNOWN = UNDEF;

    /// infinite musical time value
    /// for right bound of unbounded time intervals.
    const mtu_t UNBOUNDED = Rational(numeric_limits<long>::max());

    /// the given time value is not UNDEF
    /// @warning it can be UNBOUNDED
    bool defined(mtu_t t);

    /// the given time value is UNDEF
    bool undefined(mtu_t t);

    /// the given time value is not UNDEF and not UNBOUNDED
    /// @warning not the negation of undefined
    bool finite(mtu_t t);

    /// the given time value is UNBOUNDED
    bool infinite(mtu_t t);

    /// the time value is zero (defined)
    bool null(mtu_t t);

    /// the given time value is defined and strictly greater than zero
    bool positive(mtu_t t);

    /// create a new MTU value.
    /// @param b number of (whole) bars. must be positive.
    /// @param n numerator of fractional duration in last bar
    /// must be positive.
    /// @param d denominator of fractional duration in last bar.
    /// must be strictly positive. must be strictly larger than n.
    /// @return the MTU b + n/d
    mtu_t make(long b, long n=0, long d=1);

    /// product by rational factor.
    mtu_t mult(mtu_t t, const Rational& factor);

    /// create the MTU value corresponding to the fiven int.
    /// @warning note that `of_int(i)` is the mtu start date of the bar
    /// number `i` (0 begin the first bar).
    mtu_t of_int(long b);

    Rational to_Rational(mtu_t t);
    mtu_t invert(mtu_t t);

    const mtu_t ZERO = Rational(0);
    const mtu_t ONE = Rational(1);

    /// integral number of bars.
    /// = floor
    long measure(mtu_t);

    /// time inside the last bar
    Rational inside(mtu_t);

    std::string to_string(mtu_t);

}


/// hash<mtu_t> = hash<Rational>


#endif /* MTU_hpp */

/// @}

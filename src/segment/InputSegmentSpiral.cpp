//
//  InputSegmentSpiral.cpp
//  qparse
//
//  Created by Florent Jacquemard on 04/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "InputSegmentSpiral.hpp"



void InputSegmentSpiral::respell(int k)
{
    TRACE("respell segment (h={}, r={}", NoteName::h, NoteName::r);

// dump the table of synonym notes
//    for (size_t i = 0; i < 12; i++)
//    {
//        for (size_t j = 0; j < 3; j++)
//        {
//            INFO("syn[{}][{}] = {}", i, j, NoteName::synonyms[i][j]);
//        }
//    }
    
    //assert(quantized());
    //const NoteName& current = NoteName::ofkey(k);
    // unwindowed: centroid of all pitches so far.
    SpiralPoint centroid(NoteName::ofkey(k));
    
    for (MusPointSeq::iterator i = _events.begin();
         i != _events.end(); i++)
    {
        MusPoint& p = *i;
        MusEvent* e = p.event();
        assert(e);
        if (e->isNote())
        {
            NoteEvent* ne = dynamic_cast<NoteEvent*>(e);
            assert(ne);
            if (ne->unpitched())
            {
                continue;
            }
            else
            {
                const NoteName& nn =
                    NoteName::closest(ne->pitch().midi(), centroid);
                // update musevent
                INFO("respell {} to {}", *e, nn);
//                      ne->pitch().name, ne->pitch().alteration,
//                      nn.name, nn.alteration);
                ne->pitch().name = nn.name;
                ne->pitch().alteration = nn.alteration;
                SpiralPoint update(nn);
                // segment is quantized
                assert(MTU::defined(p.mduration()));
                // TBC: update = zero for grace-notes (ignored)
                update *= toDouble(p.mduration()/Interval::mduration());
                centroid += update;
                TRACE("new point: {} centroid={}", nn, centroid);
            }
        }
        else
        {
            continue;
        }
    }
}


void InputSegmentSpiral::respell(mtu_t wlength, int k)
{
    WARN("respell SlidingWindow: not implemented");
}

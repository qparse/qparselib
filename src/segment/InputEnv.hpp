//
//  InputEnv.hpp
//  qparse
//
//  Created by Florent Jacquemard on 15/02/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#ifndef InputEnv_hpp
#define InputEnv_hpp

#include <stdio.h>

#include "trace.hpp"
#include "InputSegment.hpp"
#include "IntervalHeap.hpp"

//template<class K, class H> class Record;


/// @brief wrapper abstract class
/// embedding a standard input environment for parsing algos.
/// It encapsulates some input data and structures for memory management.
class InputEnv
{
public:
   
    /// @brief input points to quantize.
    /// NULL when not given (inputless parser)
    InputSegment* segment;
    
    /// @brief table of the registered aligned input intervals.
    /// NULL when not needed (inputless parser)
    /// (if there are no input points to process).
    IntervalHeap* iheap;
    
    /// @param s input segment can be :
    /// - NULL :
    /// e.g. for simple enumeration of the given wcfg
    /// - non-NULL:
    ///  e.g. for quantization of the points of given input segment
    ///  using a given wcfg
    InputEnv(InputSegment* s = NULL);
    
    /// copy
    InputEnv(const InputEnv&);
        
    /// delete the heap if allocated.
    /// do not delete WCFG and segment :
    /// they must be deallocated externaly.
    virtual ~InputEnv();
    
};

#endif /* InputEnv_hpp */


/// @}

//
//  VMusPoint.hpp
//  squanty
//
//  Created by Florent Jacquemard on 19/03/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#ifndef VMusPoint_hpp
#define VMusPoint_hpp

#include <stdio.h>

#include "MusPoint.hpp"


/// @brief A voiced musPoint is a MusPoint extended with voicing information (voice number).
/// The voicing must be such that every voice in an input segment
/// is monophonic with chords.
class VMusPoint : public MusPoint
{
public:
    
    /// @brief copy the given point and add voice number info.
    VMusPoint(const MusPoint& p, size_t voice);
    
    /// @brief event (if any) is cloned.
    VMusPoint(const VMusPoint& p);
    
    ~VMusPoint();
    
    /// @brief event (if any) is cloned.
    VMusPoint& operator=(const VMusPoint&);
    
    bool operator==(const VMusPoint&) const;
    
    inline size_t voice() const { return _voicenb; }

    
    
protected:

    /// @brief Voice number.
    size_t _voicenb;
    
    virtual void print(std::ostream& o) const;
};


#endif /* VMusPoint_hpp */

/// @}

//
//  VMusPoint.cpp
//  squanty
//
//  Created by Florent Jacquemard on 19/03/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "VMusPoint.hpp"


VMusPoint::VMusPoint(const MusPoint& p, size_t voice):
MusPoint(p),
_voicenb(voice)
{ }


VMusPoint::VMusPoint(const VMusPoint& p):
MusPoint(p),
_voicenb(p._voicenb)
{
    // event cloned in Point copy
}


VMusPoint::~VMusPoint()
{
    // event deleted by Point destructor
}


VMusPoint& VMusPoint::operator= (const VMusPoint& p)
{
    if(this != &p)
    {
        MusPoint::operator=(p);
        _voicenb = p._voicenb;
    };
    
    return *this;
}


bool VMusPoint::operator==(const VMusPoint& p) const
{
    if (! MusPoint::operator==(p))
        return false;
    
    return (_voicenb == p._voicenb);
}


void VMusPoint::print(std::ostream& o) const
{
    MusPoint::print(o);
    o << " voice:" << _voicenb;
}

//
//  Event.cpp
//  qparse
//
//  Created by Florent Jacquemard on 06/10/16.
//  Copyright © 2016 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#include "MusEvent.hpp"


const int MusEvent::EVENTNB_UNKNOWN = -1;

const unsigned int MusEvent::UNDEF_VELOCITY = 128;


MusEvent::MusEvent(int nb):
number(nb)
{
    assert(nb == EVENTNB_UNKNOWN || nb >= 0);
}


MusEvent::MusEvent(const MusEvent& e):
number(e.number)
{}


std::ostream& operator<<(std::ostream& o,
                         const MusEvent& rhs)
{
    assert(rhs.isRest() || rhs.isNote());
    rhs.print(o);
    return o;
}


ScoreModel::Pitch RestEvent::Unpitch = ScoreModel::Pitch();


RestEvent::RestEvent(int nb):
MusEvent(nb)
{ }


RestEvent::RestEvent(const RestEvent& e):
MusEvent(e)
{ }


MusEvent* RestEvent::clone() const
{
    return (new RestEvent(this->number));
}


ScoreModel::Pitch& RestEvent::pitch()
{
    return Unpitch;
}


void RestEvent::print(std::ostream& o) const
{
    o << "R ";
//    if (number != EVENTNB_UNKNOWN)
//        o << " (#" << number << ")";
//    else
//        o << " (#UNKNOWN)";
}





// unpitched note
NoteEvent::NoteEvent():
MusEvent(EVENTNB_UNKNOWN),
_pitch(),
_velocity(MusEvent::UNDEF_VELOCITY)
{ }

// pitched note
NoteEvent::NoteEvent(ScoreModel::Pitch p, unsigned int v, int nb):
MusEvent(nb),
_pitch(p),
_velocity(v)
{}

NoteEvent::NoteEvent(unsigned int p, unsigned int v, int nb):
MusEvent(nb),
_pitch(ScoreModel::Pitch(p, ScoreModel::Pitch::MIDI)),
_velocity(v)
{}

NoteEvent::NoteEvent(const NoteEvent& e):
MusEvent(e),
_pitch(e._pitch),
_velocity(e._velocity)
{}


MusEvent* NoteEvent::clone() const
{
    return (new NoteEvent(_pitch, _velocity, number));
}


ScoreModel::Pitch& NoteEvent::pitch()
{
    return _pitch;
}


void NoteEvent::print(std::ostream& o) const
{
    o << _pitch;
    if (_velocity != MusEvent::UNDEF_VELOCITY)
    {
        o << " vel=" << _velocity;
    }
//    if (number != EVENTNB_UNKNOWN)
//        o << " (#" << number << ")";
}


/// @}

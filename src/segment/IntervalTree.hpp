//
//  IntervalTree.hpp
//  qparse
//
//  Created by Florent Jacquemard on 11/03/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#ifndef IntervalTree_hpp
#define IntervalTree_hpp

#include <stdio.h>
#include <utility>      // std::pair, std::make_pair
#include <unordered_map>
#include <map>

#include "trace.hpp"
#include "Rational.hpp"
#include "RTU.hpp"
#include "MTU.hpp"
#include "InputSegment.hpp"
#include "AlignedInterval.hpp"
//#include "IntervalHeap.hpp"

class IntervalHeap;


/// @brief extension of Aligned Interval
/// to define a tree of nested Alignements with sharing
/// using hash table to store all alignment constructed.
///
/// to construct IntervalTree use IntervalHeap.make
/// and the members top, split, split_back and sub.
class IntervalTree: public AlignedInterval
{
    friend class IntervalHeap;

public:

    //IntervalTree():Interval() {}
    
    ~IntervalTree();
    
    /// @return the embedding Interval in the Interval tree.
    /// @return NULL if this Interval is the root of the tree.
    virtual inline IntervalTree* parent()
    { return _parent; }
    
    /// @return the previous sibling Interval in the Interval tree.
    /// @return NULL if this Interval is the leftmost sibling.
    virtual inline IntervalTree* previous_sibling()
    { return _previous_sibling; };
    
    
    /// @brief top interval (root of interval tree)
    /// covering the whole length of the given input segment s.
    ///
    /// inside-bar interval of musical time duration of 1 bar if flag bar is true.
    /// multi-bar interval if flag bar is false.
    /// @param f_align flag says wether alignment is computed
    /// for the interval.
    /// if not, the next (first element of input segment after the
    /// right bound of this interval) is set to out_of_range
    /// (= size of segment), meaning that there is none.
    static IntervalTree* top(const InputSegment* s,
                             IntervalHeap* h,
                             mtu_t mend = MTU::UNBOUNDED, // right open
                             bool f_align=false);
    
    
    /// @brief return a sub interval.
    /// @param rdur must be strictly positive.
    /// @param mdur must be strictly positive.
    /// @param i must be 1 or 2.
    /// @return
    /// - if i = 1
    ///   first sub-interval starting at the same point as this interval
    ///   of realtime duration rdur sec.
    ///   of musical duration mdur bar.
    ///   it is aligned.
    /// - if i = 2
    ///   second sub-interval (rest)
    ///   starting at this interval realtime start + rdur
    ///   and this interval musical time start + mdur
    ///   of realtime duration this realtime duration - rdur.
    ///   if the real starting date is out of this interval,
    ///   then the real duration of the returned second sub-interval is zero.
    ///   the musical starting date must be inside this interval.
    ///   it is not aligned.
    IntervalTree* split(const InputSegment*,
                        IntervalHeap*,
                        rtu_t rdur, mtu_t mdur,
                        size_t i);
    
    
    /// @brief return a sub interval.
    /// @param rdur must be strictly positive.
    /// @param mdur must be strictly positive.
    /// @param i must be 1 or 2.
    /// @return
    /// - if i = 1
    ///   first sub-interval starts at same point as this interval
    ///   of realtime duration : duration of this interval - rdur
    ///   of musical duration : musical duration of this interval - mdur bars.
    ///   if the starting date is out of the input segment,
    ///   then the real duration of the returned first sub-interval is zero.
    ///   it is aligned.
    /// - if i = 2
    ///   second sub-interval (rest)
    ///   starts at this interval realtime end - rdur
    ///   and this interval musical time end - mdur
    ///   of realtime duration rdur.
    ///   it is aligned.
    /// @warning this interval must not be open.
    /// @warning the interval returned is aligned.
    IntervalTree* split_back(const InputSegment*,
                             IntervalHeap*,
                             rtu_t rdur, mtu_t mdur,
                             size_t i);
    
    /// @brief return the i-1th sub-interval
    /// of the division of this interval in a equal parts.
    /// @param a total number of parts for division, must be > 1
    /// @param i index of the part to return, must be in 1..a
    /// @warning this interval must be aligned.
    /// @warning this interval must not be open.
    /// @warning the sub-interval returned is aligned.
    IntervalTree* sub(const InputSegment* s,
                      IntervalHeap* h,
                      size_t a, size_t i);

    /// @brief return the a sub-interval merging the (i-1)th to (i-1+m)th
    /// sub-intervals of the division of this interval in ar equal parts.
    /// @param ar total number of parts for division, must be > 1
    /// @param i index of the part to return, must be in 1..a
    /// @param m number of parts to merge (multiplicity)
    /// @warning this interval must be aligned.
    /// @warning this interval must not be open.
    /// @warning the sub-interval returned is aligned.
    IntervalTree* subm(const InputSegment* s,
                       IntervalHeap* h,
                       size_t ar, size_t i, size_t m);

    /// interval starting just after this interval,
    /// and of real-time (RTU) and musical-time (MTU) duration the factor of
    /// the duration of this interval.
    /// @param s input segment.
    /// @param factor multiplicative factor to compute the duration of the
    ///        constructed interval from the duration of this interval.
    /// @warning this interval must not be unbounded (MTU end).
    /// @warning this interval must be inside given the given input segment.
    /// The interval created is added to the interval heap. It is not added
    /// to the tree.
    IntervalTree* nexti(const InputSegment* s,
                        IntervalHeap* h,
                        const Rational& factor);
    
protected:
    
    IntervalTree* _parent;
    
    /// @brief previous sibling Interval in the Interval tree.
    ///
    /// NULL if this Interval is the leftmost sibling
    IntervalTree* _previous_sibling;
    
    /// @brief direct access to subtrees.
    ///
    /// every entry in this map associate to
    /// an arity a a partition t1,...,ta of the root interval.
    std::map<size_t, std::vector<IntervalTree*>> _children;
    
    /// @brief top interval (root of interval tree).
    /// covering the whole length of the given input segment s
    /// inside-bar interval of musical time duration of 1 bar if flag bar is true
    /// multi-bar interval if flag bar is false.
    /// @param mend end date in musical-time. must not be MTU::UNBOUNDED.
    /// @param f_align flag says wether alignment is computed
    /// for the interval.
    /// if not, the next (first element of input segment after the
    /// right bound of this interval) is set to out_of_range
    /// (= size of segment), meaning that there is none.
    /// @warning the interval tree created is not registered to an interval heap.
    /// @warning not aligned.
    /// @todo TBR not used.
    IntervalTree(const InputSegment* s,
                 mtu_t mend = MTU::UNBOUNDED, // right open
                 bool f_align=false);

    /// @brief build an interval tree with
    /// musical-time bounds [mbegin, mbegin+mdur[ and
    /// real-time bounds [rbegin, rbegin+rdur[
    /// for the input segment s.
    /// @param first must be the first element of input segment
    /// after the left bound of this interval.
    /// @param p pointer to the parent.
    /// @param ps pointer to the previous sibling.
    /// @warning alignment is computed.
    /// @warning use only internaly construction of recursive paths.
    IntervalTree(const InputSegment* s,
                 rtu_t rbeg, rtu_t rend,
                 mtu_t mbeg, mtu_t mend,
                 size_t first,
                 IntervalTree* p,
                 IntervalTree* ps);

    /// @brief build an interval tree with
    /// musical-time bounds [mbegin, mbegin+mdur[ and
    /// real-time bounds [rbegin, rbegin+rdur[
    /// for the input segment s.
    /// @param first must be the first element of input segment
    /// after the left bound of this interval.
    /// @param next must be the first element of input segment
    /// after the right bound of this interval
    /// or out_of_range (= size of segment) if there is none.
    /// @param p pointer to the parent.
    /// @param ps pointer to the previous sibling.
    /// @warning alignment is not computed - can be aligned afterwards.
    /// @warning use only internaly construction of recursive paths.
    IntervalTree(const InputSegment* s,
                 rtu_t rbeg, rtu_t rend,
                 mtu_t mbeg, mtu_t mend,
                 size_t first,
                 size_t next,
                 IntervalTree* p,
                 IntervalTree* ps);
    
private:
    
    void fillSubtrees(const InputSegment* s,
                  IntervalHeap* ih,
                  std::vector<IntervalTree*>& v,
                  size_t a);
    
};


#endif /* IntervalTree_hpp */


/// @}

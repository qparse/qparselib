//
//  MusEvent.hpp
//  qparse
//
//  Created by Florent Jacquemard on 06/10/16.
//  Copyright © 2016 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#ifndef MusEvent_hpp
#define MusEvent_hpp

#include <stdio.h>
#include <assert.h>
#include <vector>

#include "trace.hpp"
#include "Pitch.hpp"


/// @brief input events.
/// input interface to MIDI, OpenMusic, MEI etc
/// keep track of input event list
/// event are not stored internaly (in automata).
/// we just preserve the order.
/// and remap afterwards to input event list (with dfs).


/// @brief abstract class to built polymorphic event lists (in input or output).
///
/// can be downcasted to descendant class with dynamic_cast
/// for using particular operations
class MusEvent
{
public:
    /// type for MIDI event numbers
    static const int EVENTNB_UNKNOWN;

    static const unsigned int UNDEF_VELOCITY;
   
    
    /// MIDI event number in original MIDIfile
    int number;
    
    MusEvent(int nb);

    MusEvent(const MusEvent&);

    virtual ~MusEvent() {}
    
    virtual MusEvent* clone() const = 0;
    
    virtual bool isRest() const { return false; }
    virtual bool isNote() const { return false; }
    
    /// can be modified
    virtual ScoreModel::Pitch& pitch() = 0;

    virtual void print(std::ostream& o) const = 0;

    friend std::ostream& operator<<(std::ostream& o,
                                    const MusEvent& rhs);
};



class RestEvent : public MusEvent
{
public:
    RestEvent(int nb = EVENTNB_UNKNOWN);

    RestEvent(const RestEvent&);
    
    ~RestEvent() {}
    
    MusEvent* clone() const;
    
    virtual bool isRest() const { return true; }

    /// should not be called
    virtual ScoreModel::Pitch& pitch();

    virtual void print(std::ostream& o) const;

    //friend std::ostream& operator<<(std::ostream&, const RestEvent&);

private:
    static ScoreModel::Pitch Unpitch;
};




class NoteEvent : public MusEvent
{
public:
    

    /// @brief unpitched note (drums)
    NoteEvent();
    
    // @brief unpitched note (drums)
    //NoteEvent(int nb = EVENTNB_UNKNOWN);
    
    /// @brief pitched note.
    NoteEvent(ScoreModel::Pitch p,
              unsigned int vel,
              int nb = EVENTNB_UNKNOWN);
    
    /// @brief pitched note with MIDI pitch in 0..127.
    NoteEvent(unsigned int p,
              unsigned int vel,
              int nb = EVENTNB_UNKNOWN);

    NoteEvent(const NoteEvent&);
    
    ~NoteEvent() {}

    MusEvent* clone() const;

    virtual bool isNote() const { return true; }
    
    bool unpitched() const { return _pitch.undef(); }
    
    inline unsigned int velocity() const { return _velocity; }
    
    /// can be modified.
    virtual ScoreModel::Pitch& pitch();
    
    virtual void print(std::ostream& o) const;

    //friend std::ostream& operator<<(std::ostream&, const NoteEvent&);

protected:

    /// @todo replace by int (MIDI pitch) - Pitch in score model
    ScoreModel::Pitch _pitch;
    
    /// @brief  MIDI velocity.
    unsigned int _velocity;
    
};


#endif /* MusEvent_hpp */


/// @}

//
//  Voicing.cpp
//  squanty
//
//  Created by Florent Jacquemard on 26/03/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "Voicing.hpp"

Voicing::Voicing(InputSegment& seg):
_support(seg),
_voices(0),
_done(0),
_blocks() // empty
{
    // assert that seq is quantized ?
}


Voicing::~Voicing()
{
    // destruction of bar vector
}


bool  Voicing::voiced()
{
    return _done; //_bars.size();
}


std::vector<int>& Voicing::voices(size_t i)
{
    assert(i < _blocks.size());
    assert(i < _support.size());
    return _blocks[i];
}

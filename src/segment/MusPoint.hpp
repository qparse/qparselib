//
//  MusPoint.hpp
//  qparse
//
//  Created by Florent Jacquemard on 10/03/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#ifndef MusPoint_hpp
#define MusPoint_hpp

#include <stdio.h>

#include "RTU.hpp"
#include "MTU.hpp"
#include "Point.hpp"


/// undefined voice value
extern int VOICE_UNDEF;


/// @brief Point extended with
/// - mutable musical time date (expressed in fraction of bars)
/// - mutable musical time duration
/// - mutable voice number
/// @todo redefine musical time duration like realtime duration, with links.
/// @todo replace _mduration by mduration computed from linked point's date
/// @warning mduration is reset in InputSegment.quantize()
class MusPoint : public Point
{
public:

    MusPoint(MusEvent* e, rtu_t rdate, bool on,
             long link=POINTREF_NULL,
             long backlink=POINTREF_NULL,
             mtu_t mdate=MTU::UNDEF,
             mtu_t mduration=MTU::UNDEF,
             int voice=VOICE_UNDEF);

    /// @brief copy of point.
    ///
    /// extended with given onset and duration values (in fraction of bars)
    MusPoint(const Point& p,
             mtu_t mdate=MTU::UNDEF,
             mtu_t mduration=MTU::UNDEF,
             int voice=VOICE_UNDEF);
    
    /// @brief event (if any) is cloned.
    MusPoint(const MusPoint& p);
    
    ~MusPoint();
    
    /// @brief event (if any) is cloned.
    MusPoint& operator= (const MusPoint&);
    
    bool operator==(const Point&) const;
    
    inline mtu_t mdate() const { return _mdate; }

    /// @param d new date. must be defined, bounded and positive or null.
    void set_mdate(mtu_t d);

    /// @warning only for polyphonic events.
    inline mtu_t mduration() const { return _mduration; }
    
    /// @param d new duration. must be defined, bounded and positive or null.
    void set_mduration(mtu_t d);

    /// @warning can be modified.
    inline int voice() const { return _voicenb; }
    
    /// @param v a voice number. must not be VOICE_UNDEF.
    void set_voice(int v);

    
    friend inline std::ostream& operator<<(std::ostream& o, const Point& rhs);
    
protected:
    /// @brief timestamp in musical time (number of bars).
    mtu_t _mdate;
    
    /// @brief The duration is computable as the difference of mdates
    /// between this point and its link in an InputSegment.
    /// We store it for verification.
    mtu_t _mduration;
    
    /// @brief Voice number.
    int _voicenb;

    
    virtual void print(std::ostream& o) const;
};


#endif /* MusPoint_hpp */


/// @}

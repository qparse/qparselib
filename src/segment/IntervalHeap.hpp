//
//  IntervalHeap.hpp
//  qparse
//
//  Created by Florent Jacquemard on 06/02/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#ifndef IntervalHeap_hpp
#define IntervalHeap_hpp

#include <stdio.h>
#include <utility>      // std::pair, std::make_pair
//#include <unordered_map>
//#include <map>
#include <unordered_set>

#include "RTU.hpp"
#include "MTU.hpp"
#include "InputSegment.hpp"
#include "Interval.hpp"
#include "AlignedInterval.hpp"
#include "IntervalTree.hpp"


//class AlignedInterval;
//class IntervalTree;


struct PointedIntervalEq
{
    bool operator() (IntervalTree const* lhs,
                     IntervalTree const* rhs) const
    {
        return
            lhs->rbegin() == rhs->rbegin() &&
            lhs->rend()   == rhs->rend()   &&
            lhs->mbegin() == rhs->mbegin() &&
            lhs->mend()   == rhs->mend();
    }
};


struct PointedIntervalHash
{
    std::size_t operator()(const IntervalTree* p) const
    {
        using std::size_t;
        using std::hash;
        
        // see constant.h
        size_t h = HASH_SEED;
        h = h * HASH_FACTOR + hash<rtu_t>()(p->rbegin());
        h = h * HASH_FACTOR + hash<rtu_t>()(p->rend());
        h = h * HASH_FACTOR + hash<mtu_t>()(p->mbegin());
        h = h * HASH_FACTOR + hash<mtu_t>()(p->mend());
        return h;
    }
};


typedef std::unordered_set<IntervalTree*,
                           PointedIntervalHash,
                           PointedIntervalEq>     IntervalSet;





/// @brief table for storage of aligned intervals
/// to avoid recomputation of alignments.
///
/// the aligned interval are indexed (in table) as intervals
/// (i.e. indexed with bound of real-time and musical-time interval bounds).
class IntervalHeap
{
    friend class IntervalTree;
    
public:
    IntervalHeap();
    
    ~IntervalHeap();
    
    //static void init(InputSegment*);
    
    // input segment
    //static InputSegment* segment;
    
    bool empty() const;
    
    size_t size() const;
    
    /// @brief find or create (and push) a top interval
    /// of real-time duration covering the whole length
    /// of the given input segment s (root of interval tree) + the given extension.
    /// - inside-bar interval (musical time duration of 1 bar) if flag bar is true
    /// - multiple interval if flag bar is false (default).
    /// @param mend end date in musical-time. must not be MTU::UNBOUNDED.
    /// @param f_align flag says wether alignment is computed for the interval.
    /// if not, the next (first element of input segment after the
    /// right bound of this interval) is set to out_of_range  (= size of segment),
    /// meaning that there is none (no next element).
    /// @warning the returned IntervalTree will be deleted with this Heap.
    IntervalTree* const make(const InputSegment* s,
                             mtu_t mend = MTU::UNBOUNDED, // right open
                             bool f_align = false,
                             rtu_t rext = 0);


protected:
     /// @brief table of nodes in interval tree.
    IntervalSet _interval_heap;
    
    int _added;
    int _found;

    /// @brief get interval from heap, or build it if not present.
    /// @param first must be the first element of input segment
    /// after the left bound of this interval.
    /// @param p pointer to the parent.
    /// @param ps pointer to the previous sibling.
    /// @warning alignment is computed.
    IntervalTree* const make(const InputSegment* s,
                             rtu_t rbeg, rtu_t rend,
                             mtu_t mbeg, mtu_t mend,
                             size_t first,
                             IntervalTree* p,
                             IntervalTree* ps);

    /// @param first must be the first element of input segment
    /// after the left bound of this interval.
    /// @param next must be the first element of input segment
    /// after the right bound of this interval
    /// or out_of_range (= size of segment) if there is none.
    /// @param p pointer to the parent.
    /// @param ps pointer to the previous sibling.
    /// @warning alignment is not computed - can be aligned afterwards.
    IntervalTree* const make(const InputSegment* s,
                             rtu_t rbeg, rtu_t rend,
                             mtu_t mbeg, mtu_t mend,
                             size_t first,
                             size_t next,
                             IntervalTree* p,
                             IntervalTree* ps);

private:
    IntervalTree* const push(IntervalTree* p);
    
    
};



#endif /* IntervalHeap_hpp */


/// @}

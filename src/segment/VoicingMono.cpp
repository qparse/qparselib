//
//  Voicing.cpp
//  squanty
//
//  Created by Florent Jacquemard on 19/03/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "VoicingMono.hpp"


VoicingMono::VoicingMono(InputSegment& seg):
Voicing(seg)
{
    _voices = 1;
}


// all points in the same unique voice
void VoicingMono::revoice_all()
{
    size_t i = 0;
    long bar = 0;
    while (i < _support.size())
    {
        // 1 bar
        while ((i < _support.size()) &&
               (MTU::measure(_support.mdate(i)) == bar))
        {
            _support.set_voice(i, 0);
            ++i;
        }
        _blocks.push_back(std::vector<int>(1, 0));
    }
    _done = _support.size();
}


void VoicingMono::revoice_online()
{
    // voice next block
    for (size_t i = _done; i < _support.size(); i++)
    {
        assert(_support.voice(i) == VOICE_UNDEF);
        _support.set_voice(i, 0);
    }
    _done = _support.size();
    _blocks.push_back(std::vector<int>(1, 0));
}



/// @todo STUB
//void VoicingMono::revoice(size_t i)
//{
//    ERROR("revoice(bar) = STUB");
//}

//
//  Interval.hpp
//  qparse
//
//  Created by Florent Jacquemard on 06/02/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#ifndef Interval_hpp
#define Interval_hpp

#include <stdio.h>

#include <assert.h>
#include <vector>
#include <map>
#include <limits.h>

#include "hash.hpp"
#include "RTU.hpp"
#include "MTU.hpp"

//#include "InputSegment.hpp"
//#include "IntervalHeap.hpp"


class InputSegment;


/// @brief an Interval in an input segment
/// with realtime bounds (seconds)
/// and musical bounds (fraction of bars).
class Interval
{
    friend class IntervalHeap;
    
public:
    
    //Interval();
    
    /// Time interval with the real-time (RTU) bounds of an input segment,
    /// and musical-time (MTU) bounds resp. 0 and the given end MTU date.
    /// @param s input segment. the RTU bounds of its support interval are
    /// copied.
    /// @param mend end date in musical time (number of bars),
    /// MTU::UNBOUNDED when unknown.
    Interval(const InputSegment* s, mtu_t mend=MTU::UNBOUNDED);

    // @todo TBR, replace by sub
    // Interval of the given input segment s
    // starting at the (real) begin date of s
    // of real duration the given value rdur
    // of musical duration 1 bar
    // bar of given number (default 0)
    // interval must fit in the given input segment
    //Interval(const InputSegment* s, rtu_t len, size_t bar=0);

    // next inside-bar interval in input segment.
    // musical duration is 1 bar
    // real duration is the same as the given interval
    // real begin bound is the real end bound of the given interval
    // the given interval must be inside-bar
    // interval must fit in the given input segment
    //Interval(const InputSegment*, const Interval&);

    /// interval next to the given interval, inside given input segment,
    /// and of real-time (RTU) and musical-time (MTU) duration the factor of
    /// the duration of the given interval.
    /// @param s input segment.
    /// @param p previous interval. must not be unbounded (MTU end).
    /// its bounds must be inside the input segment s.
    /// @param factor multiplicative factor to compute the duration of the
    /// constructed interval from the duration of p.
    Interval(const InputSegment* s, const Interval& p, Rational factor);
    
    /// @brief copy.
    Interval(const Interval& p);

    /// @brief used for copy of downcasted IntervalTree.
    Interval(Interval* p);
    
    /// @warning do not deallocate the segment here.
    ~Interval() {}
    
    virtual Interval& operator= (const Interval&);
    
    /// @brief for using Interval as key in map.
    virtual bool operator==(const Interval&) const;
    
    // number of bars (in input segment)
    // before the begining of this interval
    // @todo TBR
    //size_t bars;
    
    /// @brief start date of this interval in real-time (seconds).
    /// i.e. real-time date aligned with the musical date bars + begin
    inline rtu_t rbegin() const { return _rbegin; }
    
    /// @brief end date of this interval in real-time (seconds).
    /// i.e. real date aligned with he musical date bars + end.
    /// must be > rbegin.
    inline rtu_t rend() const  { return _rend; }
    
    /// @brief duration of this interval in real-time (seconds).
    rtu_t rduration() const;
    
    /// @brief start date of this interval in musical-time (number of bars).
    /// relatively (shift) to current bar start.
    inline mtu_t mbegin() const { return _mbegin; }
    
    /// @brief end date of this interval in musical-time (number of bars).
    /// TBR relatively (shift) to current bar start.
    /// TBR 0 for meta interval (in this case begin must be 0).
    /// @warning must be >= begin.
    inline mtu_t mend() const { return _mend; }
    
    /// augment the musical time end by the given duration.
    inline void extend_mend(mtu_t d) { _mend += d; }
    
    /// @brief duration of this interval in musical-time (number of bars).
    mtu_t mduration() const;
    
    /// real-time interval is right-open
    /// i.e. real-time right bound is +infinity.
    /// Useful when end date of input flow is unknown apriori,
    /// e.g. for processing online MIDI input.
    inline bool ropen() const { return (_rend == RTU::UNBOUNDED); }

    /// musical-time interval is right-open
    /// i.e. musical-time right bound is +infinity.
    /// Useful when number of bars is unknown apriori.
    inline bool mopen() const { return (_mend == MTU::UNBOUNDED); }
    
    //bool insideBar() const;
    
    friend std::ostream& operator<<(std::ostream&, const Interval&);
    
protected:
    
    /// start date of interval in real-time (seconds).
    rtu_t _rbegin;
    
    /// end date of interval in real-time (seconds).
    rtu_t _rend;
    
    /// duration of interval in real-time (seconds).
    // rtu_t _rduration;
    
    /// start date of interval in musical-time (number of bars).
    mtu_t _mbegin;

    /// end date of interval in musical-time (number of bars).
    mtu_t _mend;

    /// duration of interval in musical-time (number of bars).
    // mtu_t _mduration;
    
    /// @brief build an interval with
    /// musical-time bounds [mbegin, mbegin+mdur[ and
    /// real-time bounds [rbegin, rbegin+rdur[.
    /// @param rbegin left bound in real-time (seconds).
    /// @param rend right bound in real-time (seconds).
    /// @param mbegin left bound in musical-time (number of bars).
    /// @param mend right bound in musical-time (number of bars).
    /// @warning not aligned.
    Interval(rtu_t rbegin, rtu_t rend,
             mtu_t mbegin, mtu_t mend);


    /// @brief build an interval with
    /// musical-time bounds [mbegin, mbegin+mdur[ and
    /// real-time bounds [rbegin, rbegin+rdur[
    /// for the input segment s.
    /// @param rbegin left bound in real-time (seconds).
    /// @param rend right bound in real-time (seconds).
    /// @param mbegin left bound in musical-time (number of bars).
    /// @param mend right bound in musical-time (number of bars).
    /// @warning not aligned.
    Interval(const InputSegment* s,
             rtu_t rbegin, rtu_t rend,
             mtu_t mbegin, mtu_t mend);
    
private:
    bool sanity_check(const InputSegment*);
    bool sanity_check();

};



/// @brief hash function for using interval as key in a unordered map.
namespace std
{
    template <> struct hash<Interval>
    {
        public:
        size_t operator()(const Interval& p) const
        {
            using std::hash;

            // definition in hash.hpp
            std::size_t h = HASH_SEED;
            h = REHASH(h, hash<rtu_t>()(p.rbegin()));
            h = REHASH(h, hash<rtu_t>()(p.rduration()));
            //h = REHASH(h, hash<rtu_t>()(p.rend));
            h = REHASH(h, hash<mtu_t>()(p.mbegin()));
            h = REHASH(h, hash<mtu_t>()(p.mduration()));
            //h = REHASH(h, hash<mtu_t>()(p.mend));
            return POSTHASH(h);
        }
    };
}


#endif /* Interval_hpp */


/// @}

//
//  InputSegmentMono.cpp
//  qparse
//
//  Created by Florent Jacquemard on 31/05/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//
// special treatment for simultaneous events



#include "InputSegmentMono.hpp"


InputSegmentMono::InputSegmentMono(const InputSegment& seg):
InputSegment(seg.rbegin(), seg.rend()) // initialy empty list of events
{
    // create a new _events vector for this segment by copy of given seg.

    // pointer to currently sounding event of seg
    // = linked event with link not met yet
    // there is at most one
    MusPoint* sounding = NULL;
    
    // FIFO of successive note-on from seg with the same realtime date,
    // together with their resp. index in seg.
    // initially empty.
    //bool(*fn_comp)(long, long) = point_comp;
    //std::priority_queue<long, std::vector<long>, bool(*)(long, long)>
    //heap(fn_comp);
    //std::queue<std::pair<const MusPoint&, double>> simulton();

    // number of links to each element of seg
    // std::vector<size_t> links_events;
    // std::vector<size_t> links_heap;
    // links_events.resize(seg.size());
    // TODO define as a class linkscount in InputSegment.h
    
    
    // iterator to the event of seg currently read
    std::vector<MusPoint>::const_iterator seg_it=seg.cbegin();
    // index of seg_it
    long seg_index = 0;
    
    // pointer to previous event inserted in events of this segment
    // MusPoint* prev = NULL;
        
    while (seg_it != seg.cend())
    {
        // current point in segment
        const MusPoint* current = &(*seg_it);
        
        // index in seg of current
        long index = seg_index;
        long first_index = index;
        
        // max realtime duratrion of points in simulton
        double maxrdur = rduration(*seg_it);
        // index in seg of point in simulton with max duration.
        // long simult_maxindex = 0;
        
        // process all event simultaneous with current
        while ((seg_it+1 != seg.cend()) &&
               ((seg_it+1)->rdate() == seg_it->rdate()))
        {
            seg_it++;
            seg_index++;
            double nextrdur = rduration(*(seg_it));
            if (nextrdur > maxrdur)
            {
                current = &(*seg_it);
                index = seg_index;
                maxrdur = nextrdur;
            }
        }

        assert(current);

        if (sounding != NULL && sounding->linked == index)
        {
            
        }
            // seg_it is the noteoff of sounding note
            if (sounding != NULL && sounding->linked == seg_index)
            {
                
            }
        
        if (current->noteon())
        {
            
            //sounding = current;
        }
        
    }
}

//
//  MTU.cpp
//  squant
//
//  Created by Florent Jacquemard on 06/01/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "MTU.hpp"

bool MTU::defined(mtu_t t)
{
    return (t != MTU::UNDEF);
}


bool MTU::undefined(mtu_t t)
{
    return (t == MTU::UNDEF);
}


bool MTU::finite(mtu_t t)
{
    return ((t != MTU::UNDEF) && (t != MTU::UNBOUNDED));
}


bool MTU::infinite(mtu_t t)
{
    return (t == MTU::UNBOUNDED);
}


bool MTU::null(mtu_t t)
{
    return (t == Rational(0));
}


bool MTU::positive(mtu_t t)
{
    return (defined(t) && (t > Rational(0)));    
}


mtu_t MTU::make(long b, long n, long d)
{
    assert(b >= 0);
    assert(n >= 0);
    assert(d > 0);
    assert(n < d);
    return Rational(b) + Rational(n, d);
}


mtu_t MTU::mult(mtu_t t, const Rational& factor)
{
    return (t * factor);
}


long MTU::measure(mtu_t t)
{
    assert(t >= 0);
    return floor(t);
}


Rational MTU::inside(mtu_t t)
{
    assert(t >= 0);
    return (t - Rational(measure(t)));
}


mtu_t MTU::of_int(long b)
{
    return make(b, 0, 1);
}


Rational MTU::to_Rational(mtu_t t)
{
    return t;
}

mtu_t MTU::invert(mtu_t t)
{
    return ((Rational) t).Rational::invert();
}



std::string MTU::to_string(mtu_t t)
{
    if (t.integral())
    {
        return std::to_string(t.numerator());
    }
    else if (t < 1)
    {
        return std::to_string(t.numerator())+"/"+std::to_string(t.denominator());
    }
    else
    {
        long i = measure(t);
        mtu_t rem = t - Rational(i);
        assert(rem > 0);
        return std::to_string(i)+"+"+MTU::to_string(rem);
    }
}

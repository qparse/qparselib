//
//  IntervalHeap.cpp
//  qparse
//
//  Created by Florent Jacquemard on 06/02/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#include "IntervalHeap.hpp"


IntervalHeap::IntervalHeap():
_interval_heap(),
_added(0),
_found(0)
{ }


IntervalHeap::~IntervalHeap()
{
    TRACE("delete interval heap");
    INFO("delete iHeap: size={} interval added={} hit={}",
          _interval_heap.size(), _added, _found);

    for (IntervalSet::iterator it = _interval_heap.begin();
         it != _interval_heap.end(); ++it )
    {
        assert(*it);
        delete(*it); // already freed if Heap is associated to an IntervalTree
    }
         
    _interval_heap.clear();
}


bool IntervalHeap::empty() const
{
    return _interval_heap.empty();
}


size_t IntervalHeap::size() const
{
    return _interval_heap.size();
}


IntervalTree* const IntervalHeap::make(const InputSegment* s,
                                       mtu_t me,
                                       bool f_align,
                                       rtu_t rext)
{
    assert(s);
    assert(MTU::defined(me));
    assert(rext >= 0);

    // nont aligned
    IntervalTree* p = new IntervalTree(s,
                                       s->rbegin(),
                                       s->Interval::rduration()+rext,
                                       MTU::ZERO,
                                       me,
                                       0,
                                       s->size(),
                                       NULL, NULL);

    if (f_align)
        p->align(s);
    
    return push(p);
}


IntervalTree* const IntervalHeap::make(const InputSegment* s,
                                       rtu_t rb, rtu_t re,
                                       mtu_t mb, mtu_t me,
                                       size_t first,
                                       IntervalTree* par,
                                       IntervalTree* ps)
{
    IntervalTree* p = new IntervalTree(s,
                                       rb, re,
                                       mb, me,
                                       first,
                                       par, ps);
    
    return push(p);
}


IntervalTree* const IntervalHeap::make(const InputSegment* s,
                                       rtu_t rb, rtu_t re,
                                       mtu_t mb, mtu_t me,
                                       size_t first,
                                       size_t next,
                                       IntervalTree* par,
                                       IntervalTree* ps)
{
    IntervalTree* p = new IntervalTree(s,
                                       rb, re,
                                       mb, me,
                                       first,
                                       next,
                                       par, ps);
    
    return push(p);
}



IntervalTree* const IntervalHeap::push(IntervalTree* p)
{
    assert(p);
    
    std::pair<IntervalSet::iterator, bool> ret = _interval_heap.insert(p);
    
    // p inserted to heap
    if (ret.second)
    {
        assert(p == *(ret.first));
        TRACE("iHeap.push: new IntervalTree {}", *(p));
        _added++;   // stat
    }
    // p already in heap, not inserted and deleted
    else
    {
        TRACE("iHeap.push: found IntervalTree {}", **(ret.first));
        _found++;
        delete p;
    }
    assert(*(ret.first));
    return *(ret.first);
}


/// @}

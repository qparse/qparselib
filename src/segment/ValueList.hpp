//
//  ValueList.hpp
//  qparse
//
//  Created by Florent Jacquemard on 29/07/2017.
//  Copyright © 2017 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{



#ifndef ValueList_hpp
#define ValueList_hpp

#include <stdio.h>
#include <assert.h>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <list>
#include <map>
#include <vector>

#include "MTU.hpp"
#include "DuraList.hpp"


/// list of rational durations (musical time) as components of value states.
///
/// Each duration is either
/// positive (event w or wo continuations - ties)
/// or null (grace note).
///
/// a value list is made of 2 parts:
/// - _cont : initial duration (possibly null) tied to the previous duration list
/// - _main : main list of the other events (without ties)
/// it is represented by _cont[_main]
class ValueList
{
    friend class DurationTree;
    
public:
    
    /// empty duration list of given potential length.
    /// length cannot be changed afterwards.
    ValueList(const mtu_t l = MTU::ZERO);

    /// copy of DurationList
    ValueList(const DurationList& dl);
    
    /// copy constructor
    ValueList(const ValueList& vl);

    /// copy of a sublist in the given list,
    /// starting at given date and of given duration.
    /// @param vl a value list.
    /// @param d starting date of the sublist in vl. must be smaller that the
    /// length of vl.
    /// @param len duration of the sublist in vl. must be smaller that the
    /// length of vl minus starting date. if MTU::UNBOUNDED, return the sublist
    /// from date d until the end of given vl.
    ValueList(const ValueList& vl, const mtu_t d, const mtu_t len = MTU::UNBOUNDED);
    
    /// read value list from file
    /// one ratio per line
    /// if the first line is negative ratio, it is a continuation
    /// all other line must contain positive or null ratios
    /// the file must not be empty
    ValueList(std::string filename);
        
    ~ValueList() {}
    
    ValueList& operator=(const ValueList& vl);
    
    bool empty() const;
    
    size_t size() const { return (_main.size() + ((_continuation > 0)?1:0)); }
    
    /// total duration that the list can contain, including continuation
    inline mtu_t length() const { return _length; }
    
    /// value of the continuation
    inline mtu_t cont() const { return _continuation; }
    
    /// @brief return the final status for import.
    /// @return 0 if import or export worked well.
    /// @return error code > 0 otherwise.
    inline size_t status() const { return _status; }
    
    std::list<mtu_t>::const_iterator cbegin() const { return _main.cbegin(); }
    
    std::list<mtu_t>::const_iterator cend() const   { return _main.cend(); }
    
    /// the total duration of events added in list is the expected length.
    bool complete() const;

    /// one (non null) continuation and no event in the main list.
    bool single_continuation() const;
    
    /// no continuation and only one event in the main list.
    bool single_event() const;
    
    //bool unit() const;
    
    /// no continuation and some grace notes (dur=0) + one event (dur>0) in the main list.
    bool event() const;

    /// number of events, excl. continuation.
    inline size_t nbevent() const { return _nbevt; };

    /// number of grace note.
    /// must be an event()
    inline size_t nbgn() const { return _nbgn; };
    
    /// add the event at the end of the main list
    /// fail if event cannot be added (makes sum > 1)
    /// this list must not have have been summed with others
    void add(mtu_t);
    
    /// push a continuation value
    /// fail if cont cannot be added (makes sum > 1)
    /// this list must not have been summed with others
    void addcont(mtu_t);

    /// return first element of main event list
    /// main event list must not be empty.
    mtu_t front() const;
    
    /// remove front element of main event list,
    /// @return the front element removed.
    /// @warning event list must not be empty.
    /// @warning continuation must not be null.
    mtu_t pop();

    /// reset continuation.
    /// @return the former continuation value.
    mtu_t popcont();
    
    /// remove value from continuation.
    /// @warning the given value must be smaller than continuation.
    void popcont(mtu_t);
    
    
    friend std::ostream& operator<<(std::ostream&, const ValueList&);
    
    friend inline bool operator==(const ValueList&, const ValueList&);
    friend inline bool operator!=(const ValueList&, const ValueList&);
    
private:
    /// duration to tie when appending to other list
    /// - zero if there is no continuation
    /// - positive rational otherwise
    mtu_t _continuation;
    
    /// list of events (without ties)
    std::list<mtu_t> _main;
    
    /// total duration that the list can contain, including continuation
    mtu_t _length;
    
    /// sum of durations already added to list
    mtu_t _accu;

    /// number of events added (excl. continuation)
    size_t _nbevt;

    /// number of grace notes added
    size_t _nbgn;
    
    /// error number for import. 0 means no error.
    size_t _status;

};


// hash function
//struct ValueListHasher
//{
//    std::size_t operator()(const ValueList& dl) const
//    {
//        using std::size_t;
//        using std::hash;
//
//        size_t h = HASH_SEED;
//        h = h * HASH_FACTOR + hash<size_t>()(dl.size());
//        h = h * HASH_FACTOR + hash<rtu_t>()(toDouble(dl.cont()));
//        return h;
//    }
//};

namespace std {
    template <> struct hash<ValueList>
    {
        size_t operator()(const ValueList& d) const
        {
            using std::hash;
            
            // see constant.h
            std::size_t h = HASH_SEED;
            h = REHASH(h, hash<size_t>()(d.size()));
            h = REHASH(h, hash<mtu_t>()(d.cont()));
            return POSTHASH(h);
        }
    };
}


inline bool operator==(const ValueList& lhs, const ValueList& rhs)
{
    if ((lhs._continuation == rhs._continuation) &&
        // approx
        (lhs._length == rhs._length) &&
        (lhs._accu == rhs._accu) &&
        (lhs._nbevt == rhs._nbevt) &&
        (lhs._nbgn == rhs._nbgn) &&
        // exact
        (lhs._main.size() == rhs._main.size()))
    {
        std::list<mtu_t>::const_iterator il = lhs._main.begin();
        std::list<mtu_t>::const_iterator ir = rhs._main.begin();
        while (il != lhs._main.end())
        {
            assert (ir != lhs._main.end());
            if (*il != *ir) { return false; }
            il++;
            ir++;
        }
        return true;
    }
    else
        return false;
}

inline bool operator!=(const ValueList& lhs, const ValueList& rhs)
{
    return !operator==(lhs,rhs);
}


#endif /* ValueList_hpp */


/// @}

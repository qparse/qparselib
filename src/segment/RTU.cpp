//
//  RTU.cpp
//  qparse
//
//  Created by Florent Jacquemard on 06/01/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "RTU.hpp"

// rtu_t RTU::UNBOUNDED = std::numeric_limits<double>::max();


rtu_t RTU::of_Rational(const Rational& q)
{
    return toDouble(q);
}


rtu_t RTU::mult(rtu_t d, const Rational& q)
{
    return (d * double(q.numerator())) / q.denominator();
}

//
//  InputEnv.cpp
//  qparse
//
//  Created by Florent Jacquemard on 15/02/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#include "InputEnv.hpp"

InputEnv::InputEnv(InputSegment* is):
segment(is),
iheap((is == NULL)?NULL:new IntervalHeap())
{ }


InputEnv::InputEnv(const InputEnv& env):
segment(env.segment),
iheap(env.iheap)
{ }


InputEnv::~InputEnv()
{
    if (iheap)
    {
        TRACE("delete Input Env heap");
        delete iheap;
    }
    // do not delete WCFG and segment :
    // they must be deallocated externaly
}

/// @}

//
//  InputSegmentMono.cpp
//  qparse
//
//  Created by Florent Jacquemard on 31/05/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#include "InputSegmentMono.hpp"


// create a new _events vector for this segment by copy of given seg.
/// @todo norest not implemented
InputSegmentMono::InputSegmentMono(const InputSegment& seg, bool norest):
InputSegment(seg.rbegin(), seg.rend()) // initialy empty list of events
{
    bool init = true;
       
    // event currently sounding: last noteon added and not terminated by noteoff
    // as pair made of
    // - index in this (new) segment and
    // - MIDI key number in 0..127
    // INVARIANT: stack of size at most 1.
    std::stack<std::pair<long, unsigned int>> sounding;
    
    // stack conntaining the last note-off
    // added (if norest=false, and to be linked to the next note-on)
    // or not added (if norest).
    // INVARIANT: stack of size at most 1.
    std::stack<long> noteoff;
    
    for (MusPointSeq::const_iterator it_seg=seg.cbegin();
         it_seg != seg.cend(); ++it_seg)
    {
        // current point in segment
        const MusPoint& current = *it_seg;
        
        // note-on (beginning of new sounding note)
        if (current.onset())
        {
            // insert
            long it = this->add_back(current);
            NoteEvent* event = (NoteEvent*) current.event();
            assert(event);
            TRACE("{}: add note-on[{}]: event={}", current.rdate(), it, *event);

            // there was a rest
            if (sounding.empty())
            {
                if (noteoff.empty())
                {
                    assert(init == true);
                    init = false;
                }
                else
                {
                    assert(init == false);
                    assert(noteoff.size() == 1);
                    long lastoff = noteoff.top();
                    link(lastoff, it);
                    noteoff.pop();
                }
            }
            // there was a sounding note
            else
            {
                assert(init == false);
                assert(noteoff.empty());
                assert(sounding.size() == 1);
                std::pair<long, unsigned int> p = sounding.top();
                long laston = p.first;
                unsigned int lastkey = p.second;
                // no repeated note-on unless norest
                assert(norest || lastkey != event->pitch().midi());
                // the new note event is also the off of previous on.
                WARN("{}: note {} (key={}) shorten to remove overlap",
                     current.rdate(), laston, lastkey);
                link(laston, it);
                sounding.pop();
            }
            assert(sounding.empty());
            sounding.push(std::make_pair(it, event->pitch().midi()));
        }
        // note-off
        else
        {
            assert(current.offset());
            RestEvent* event = (RestEvent*) current.event();
            assert(event);

            if (sounding.empty())
            {
                TRACE("{}: ignored pending note-off {}",
                      current.rdate(), *event);
                continue;
            }
            
            std::pair<long, unsigned int> p = sounding.top();
            long laston = p.first;
            unsigned int onkey = p.second;
            
            // find the key closed by this note-off
            // index in seg (not this segment)
            long matchon = current.backlink;
            assert(matchon != POINTREF_NULL);
            const MusPoint& matchpoint = seg.point(matchon);
            NoteEvent* matchevent = (NoteEvent*) matchpoint.event();
            assert(matchevent);
        
            // we keep the current note-off when
                // this is the last event or
            if (((norest == true) && (it_seg+1) == seg.cend()) ||
                // the current note-off is matching the sounding note-on
                ((norest == false) && (onkey == matchevent->pitch().midi())))
            {
                // insert note-off
                long it = this->add_back(current);
                TRACE("{}: add note-off[{}]: event={}",
                      current.rdate(), it, *event);
                link(laston, it);
                sounding.pop();
                assert(noteoff.empty());
                noteoff.push(it);
            }
            // we do not end the sounding note
            // until a new note-on is met
            else if (norest == true)
            {
                WARN("{}: note prolongation (norest), ignore note-off key={} event={}",
                     current.rdate(),
                     matchevent->pitch().midi(), *event);
            }
            // pending note-off of note that has been shorten
            else
            {
                WARN("{}: ignore pending note-off of shorten note key={} event={}",
                     current.rdate(),
                     matchevent->pitch().midi(), *event);
            }
        }
    }
}


/// @}

//
//  InputSequence.tpp
//  squanty
//
//  Created by Florent Jacquemard on 11/03/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


template<class P, typename U>
InputSequence<P, U>::InputSequence():
_events(), // empty list of dated points
_heap()
{ }


template<class P, typename U>
InputSequence<P, U>::InputSequence(const InputSequence& s):
_events(s._events), // copy all points
_heap(s._heap)
{ }


template<class P, typename U>
InputSequence<P, U>::~InputSequence()
{
    _events.clear();
    _heap.clear();
}


template<class P, typename U>
size_t InputSequence<P, U>::size() const
{
    return _events.size();
}


template<class P, typename U>
typename std::vector<P>::iterator
InputSequence<P, U>::begin()
{
    return _events.begin();
}


template<class P, typename U>
typename std::vector<P>::iterator
InputSequence<P, U>::end()
{
    return _events.end();
}


template<class P, typename U>
typename std::vector<P>::const_iterator
InputSequence<P, U>::cbegin() const
{
    return _events.cbegin();
}


template<class P, typename U>
typename std::vector<P>::const_iterator
InputSequence<P, U>::cend() const
{
    return _events.cend();
}


template<class P, typename U>
bool InputSequence<P, U>::check_index(long i) const
{
    if (i == POINTREF_NULL)
        return true;
    else if ((0 <= i) && (i < _events.size()))
        return true;
    else if ((-_heap.size() <= i) && (i < 0))
        return true;
    else
        return false;
}


template<class P, typename U>
void InputSequence<P, U>::link(long i, long j)
{
    assert(check_index(i));
    assert(i != POINTREF_NULL);
    assert(check_index(j));
    // forward link
    // link set for the first time.
    if (point(i).link != POINTREF_NULL)
    {
        assert(j != POINTREF_NULL);
        WARN("link of point {} reset from {} to {}",
             i, point(i).link, j);
    }
    ncpoint(i).link = j;
    TRACE("new link {}({}) - {}({})",
          i, point(i).rdate(), j, point(j).rdate());

    // backward link
    if  (j != POINTREF_NULL)
    {
        assert(point(i).rdate() <= point(j).rdate());
        if (point(j).backlink != POINTREF_NULL)
        {
            WARN("back link of point {} reset from {} to {}",
                 j, point(j).backlink, i);
        }
        ncpoint(j).backlink = i;
        TRACE("new backlink {}({}) - {}({})",
              j, point(j).rdate(), i, point(i).rdate());
    }
}


template<class P, typename U>
long InputSequence<P, U>::add_back(MusEvent* e,
                              U date,
                              bool on,
                              long link,
                              long backlink)
{
    assert(e);
    assert(check_index(link));
    // preserve ordering
    assert(_events.empty() || _events.back().date() <= date);
    _events.emplace_back(e, date, on, link, backlink);
    return (_events.size() - 1);
}

           
template<class P, typename U>
long InputSequence<P, U>::add_back(const P& p)
{
    _events.emplace_back(p);
    return (_events.size() - 1);
}

           
template<class P, typename U>
long InputSequence<P, U>::add_floating(MusEvent* e,
                                  U date,
                                  bool on,
                                  long link,
                                  long backlink)
{
    assert(e);
    assert(check_index(link));
    assert(_events.empty() || _events.back().date() <= date);
    _heap.emplace_back(e, date, on, link, backlink);
    return (- _heap.size());
}

template<class P, typename U>
long InputSequence<P, U>::add_floating(const P& p)
{
    _heap.emplace_back(p);
    return (- _heap.size());
}

template<class P, typename U>
const P& InputSequence<P, U>::point(long i) const
{
    assert(i != POINTREF_NULL);
    if (i >= 0)
    {
        assert(i < _events.size());
        return _events[i];
    }
    else
    {
        long j = -(i+1);
        assert(0 <= j);
        assert(j < _heap.size());
        return _heap[j];
    }
}


// non-const version
template<class P, typename U>
P& InputSequence<P, U>::ncpoint(long i)
{
    assert(i != POINTREF_NULL);
    if (i >= 0)
    {
        assert(i < _events.size());
        return _events[i];
    }
    else
    {
        long j = -(i+1);
        assert(0 <= j);
        assert(j < _heap.size());
        return _heap[j];
    }
}

template<class P, typename U>
MusEvent* InputSequence<P, U>::event(long i) const
{
    return point(i).event();
}

template<class P, typename U>
U InputSequence<P, U>::date(long i) const
{
    return point(i).rdate();
}

template<class P, typename U>
U InputSequence<P, U>::duration(long i) const
{
    assert(check_index(i)); // true for POINTREF_NULL
    assert(i != POINTREF_NULL);
    const P& p = point(i);

    if (p.link == POINTREF_NULL)
    {
        ERROR("Input segment: link not set for Point[{}@{}] ({}),\
               cannot compute rduration",
               i, p.rdate(), (p.onset()?"ON":"OFF"));
        return 0;
    }
    const P& poff = point(p.link);
    assert(poff.rdate() >= p.rdate());
    return(poff.rdate() - p.rdate());
}

template<class P, typename U>
bool InputSequence<P, U>::onset(long i) const
{
    return event(i)->isNote();
    //return point(i).noteon();
}


/// @warning changed
template<class P, typename U>
bool InputSequence<P, U>::offset(long i) const
{
    return event(i)->isRest();
    //return point(i).noteoff();
}


/// @}

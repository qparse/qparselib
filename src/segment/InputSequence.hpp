//
//  InputSequence.hpp
//  squanty
//
//  Created by Florent Jacquemard on 11/03/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup segment
/// @{


#ifndef InputSequence_hpp
#define InputSequence_hpp

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <iostream>
#include <string>
#include <vector>

#include "MTU.hpp"
#include "MusEvent.hpp"
#include "MusPoint.hpp"
#include "Spiral.hpp"
#include "Label.hpp"
#include "Interval.hpp"

namespace Segment{

/// @brief abstract class specifying internal representations
/// of a timed sequences of musical events.
/// The events belongs to a class P (points),
/// each point has a publicly accessible date,
/// and the time unit for dates is U.
///
/// There are 2 containers of points in the InputSequence class:
/// - a list of points ordered by increasing date.
/// - an unordered heap of floating points (also of class P).
/// Every point is accessible by an index: a unique identifier of the point
/// in either of the two above structures.
///
/// The points are in relation by links:
/// if a point mark the beginning of a musical event,
/// its links marks the end (like for MIDI ON and OFF messages).
/// The links are stored as indexes, @see Point::linked
/// Backlinks are also stored.
///
/// All dates are accessible in the Time Unit type U.
/// The duration of a linked point is the difference
/// of dates (between the link and the point).
/// The duration of a point without link (with unknown link) is zero.
template<class P, typename U>
class InputSequence
{
public:
    
    typedef std::vector<P> PointSeq;

    /// @brief empty sequence
    InputSequence();
    
    InputSequence(const InputSequence& s);

    virtual ~InputSequence();
    
    /// @brief number of non-floating points in sequence.
    size_t size() const;

    /// @brief iterators to the sequence's contents.
    typename std::vector<P>::iterator begin();
    typename std::vector<P>::iterator end();
    typename std::vector<P>::const_iterator cbegin() const;
    typename std::vector<P>::const_iterator cend() const;
       
       /// @brief return a reference to the point at index i.
       /// - ith point in this input segment if 0 <= i < input segment size
       /// - or the -i-1th floating point    if heap size <= i < 0.
       /// @param i must be in the above range of values.
       const P& point(long i) const;
       
       /// @brief return the event associated to the point at index i.
       inline MusEvent* event(long i) const;
       
       /// @brief return the real-time date (in seconds)
       /// of the point of index i
       inline U date(long i) const;
       
       /// @brief return the real-time duration (in seconds)
       /// of the point of index i.
       /// @return 0 if the point i is an offset,
       /// the distance (in real time) to matching offset if
       /// point i is an onset.
       U duration(long i) const;
      
       /// @brief the point of index i correspond to an onset.
       bool onset(long i) const;

       /// @brief the point of index i correspond to an offset.
       /// @warning in the monophonic case, it implies that the event
       /// at the point of index i is a rest.
       bool offset(long i) const;
    
       /// @brief insert new timestamped muspoint created from the parameters,
       /// at the end of the segment.
       /// @warning The realtime dateof the point must be after
       /// the current last point of this segment.
       /// @return the index of the inserted point (can be used as link).
       long add_back(MusEvent* e,
                     U date,
                     bool on,
                     long link=POINTREF_NULL,
                     long backlink=POINTREF_NULL);
       
       long add_back(const P& p);

       /// @brief create new timestamped muspoint from the parameters,
       /// and add the the heap of floating points (not in segment).
       ///
       /// (allocated and freed by this segment)
       /// @return the index of the new point (can be used as link).
       long add_floating(MusEvent* e,
                         U date,
                         bool on,
                         long link=POINTREF_NULL,
                         long backlink=POINTREF_NULL);

       long add_floating(const P& p);


    
protected:
    
    /// @brief event list.
    ///
    /// polymorphic (mono or poly)
    std::vector<P> _events;

    /// @brief floating events.
    ///
    /// not in the list but can be linked by events on the list
    std::vector<P> _heap;

    /// @brief same as point but not const.
    P& ncpoint(long i);

    bool check_index(long i) const;

    
    /// @brief the point of index i is linked to the point of index j,
    ///        and j is backlinked to i.
    /// @param i must be a valid and not NULL index of an onset point.
    /// @param j must be a valid or NULL index.
    ///        if non NULL it must index an offset.
    /// @warning both i and j can be in heap (negative index).
    /// @warning the point at i and j must not be linked (NULL link index).
    /// @warning the realtime date of i must be <= realtime date of j
    /// (if j not NULL).
    void link(long i, long j);
    
    
};


// separated definition of template class
#include "InputSequence.tpp"

} // end namespace InputSequence

#endif /* InputSequence_hpp */


/// @}

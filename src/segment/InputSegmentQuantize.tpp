//
//  InputSegmentQuantize.tpp
//  qparse
//
//  Created by Florent Jacquemard on 19/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

// these template methods cannot be defined in the cpp
// see e.g. https://stackoverflow.com/questions/115703/storing-c-template-function-definitions-in-a-cpp-file


template <class K>
void InputSegment::quantize(Parsing::TableA<K>* table,
                            const K& k,
                            size_t n)
{
    assert(table);
    assert(n >= 1);
    quantize_run(table, k, table->best(k, n), 0);
}


template <class K>
size_t InputSegment::quantize_run(Parsing::TableA<K>* tab,
                                  const K& k,
                                  const Parsing::Run<K>* r,
                                  size_t b)
{
    assert(tab);
    assert(k.interval() != NULL);
    TRACE("quantize: point {} with {}", b, k);

    if (r == NULL)
    {
        ERROR("parse failure in input segment of {} (no run)", k);
        if (! k.interval()->aligned())
            k.interval()->align(this);
        return k.interval()->next();
    }
    assert(r->complete());
    if (r->terminal())
        return quantize_run_leaf(tab, k, r, b);
    else
        return quantize_run_inner(tab, k, r, b);
}


template <class K>
size_t InputSegment::quantize_run_leaf(Parsing::TableA<K>* tab,
                                       const K& k,
                                       const Parsing::Run<K>* r,
                                       size_t j)
{
    assert(r->terminal());
    //size_t j = b;
    label_t a = r->label();
    assert(k.interval() != NULL);
    assert(k.interval()->aligned());
    mtu_t onset = k.interval()->mbegin();
    mtu_t dur = k.interval()->mduration();
    assert(dur > MTU::make(0));

    // rest
    if (SymbLabel::rest(a))
    {
        if (j < this->size())
        {
            // offset event
            this->set_mdate(j, onset);
            this->set_mduration(j, dur);
            this->extend_mend(dur); // TBC dirty. must be set only once for j.
            return ++j;
        }
        else
        {
            ERROR("quantize: grace note {} out of input segment", j);
            return j;
        }
    }
    // grace notes + note
    else if (SymbLabel::note(a))
    {
        assert((j >= this->size()) || MTU::undefined(this->mdate(j)));
        assert((j >= this->size()) || MTU::undefined(this->mduration(j)));
        // grace notes
        for (size_t i = 0; i < SymbLabel::nbGraceNotes(a); i++)
        {
            if (j < this->size())
            {
                assert(MTU::undefined(this->mdate(j)));
                assert(MTU::undefined(this->mduration(j)));
                this->set_mdate(j, onset);
                this->set_mduration(j, MTU::ZERO);
            }
            else
            {
                ERROR("quantize: grace note {} out of input segment", j);
                return j;
            }
            ++j;
        }
        // note
        if (j < this->size())
        {
            assert(MTU::undefined(this->mdate(j)));
            assert(MTU::undefined(this->mduration(j)));
            this->set_mdate(j, onset);
            this->set_mduration(j, dur);
            this->extend_mend(dur); // TBC dirty. must be set only once for j.
            return ++j;
        }
        else
        {
            ERROR("quantize: note {} out of input segment", j);
            return j;
        }
    }
    // contiuation : accumulate in previous note
    else if (SymbLabel::continuation(a) || SymbLabel::dot(a))
    {
        // skip an initial continuation
        if (j == 0)
        {
            assert(MTU::undefined(this->mdate(j)));
            assert(MTU::undefined(this->mduration(j)));
            _mend += dur;
        }
        // continuation added to previous event
        else
        {
            assert(j-1 < this->size());
            assert(onset == this->mdate(j-1) + this->mduration(j-1));

            this->set_mduration(j-1, mduration(j-1)+dur);
            this->extend_mend(dur); // TBC dirty. must be set only once for j.
        }
        return j;
    }
    // double bar mark - do nothing
    else if (SymbLabel::bar(a))
    {
        assert(SymbLabel::arity(a) == 0); // not-single bar
        INFO("Double bar at point {}/{}", j, this->size());
        //assert(j >= this->size()); // all points quantified
        return j;
    }
    else if (SymbLabel::dummy(a))
    {
        WARN("quantize: dummy symbol at {}", j);
        return j;
    }
    else
    {
        ERROR("quantize: unexpected leaf symbol {}", a);
        return j;
    }
}


template <class K>
size_t InputSegment::quantize_run_inner(Parsing::TableA<K>* tab,
                                        const K& k,
                                        const Parsing::Run<K>* r,
                                        size_t b)
{
    assert(r);
    assert(r->inner());
    size_t j = b;
    size_t ar = r->arity();
    assert(ar > 0);
    label_t a = r->label();
    
    // tuple - beamed or not
    if (SymbLabel::tuple(a) || SymbLabel::bar(a))
    {
        for (size_t i = 0; i < ar; i++)
        {
            const K* ki = r->key(i);
            assert(ki);
            const Parsing::Run<K>* ri = r->subrun(i);
            j = quantize_run(tab, *ki, ri, j);
        }
        return j;
    }
    // single bar mark
    else if (SymbLabel::bar(a))
    {
        assert(SymbLabel::arity(a) == 2); // not double bar
        // first child is a bar
        const K* k0 = r->key(0);
        j = quantize_run(tab, *k0, r->subrun(0), j);
        // second child is the rest
        const K* k1 = r->key(1);
        return quantize_run(tab, *k1, r->subrun(1), j);
    }
    // grace notes
    else if (SymbLabel::grace(a))
    {
        WARN("quantize: grace note binary symbol at {}, not supported", j);
        return j;
    }
    // voice separation
    else if (SymbLabel::fork(a))
    {
        WARN("quantize: voice fork symbol at {}, not supported", j);
        return j;
    }
    else
    {
        ERROR("quantize: unexpected inner symbol {}", a);
        return j;
    }
}



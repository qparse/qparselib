//
//  Recordey.tpp
//  squanty
//
//  Created by Florent Jacquemard on 07/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//


// #include "RunCompare.hpp"


namespace Parsing{


template<class K>
Record<K>::Record(RunCompare<K> comp, const Run<K>* failrun):
_key(nullptr),
_comp(comp),
_fail(failrun),
_nb_cand(0),
_nb_best_rejected(0)
{
//    assert((k == nullptr) || k->complete());
//    if (k)
//        TRACE("new empty Record[{}]", *k);
    TRACE("new empty Record[UNSET key]");
}


template<class K>
Record<K>::~Record()
{
    if (_fail)
        delete _fail;
}


template<class K>
void Record<K>::setKey(const K* k)
{
    assert(k);
    // assert(k->complete()); // not for pool of parse table (best partial)
    assert(_key == nullptr);  // key can only be set once
    assert(this->empty()); // before proceeding (initial state)
    _key = k;
    TRACE("Record: set key {} to associated new empty record", *k);
}


template<class K>
void Record<K>::setFailRun(const Run<K>* r)
{
    assert(r);
    assert(_fail == nullptr); // set only once
    _fail = r;
    //_fail = std::shared_ptr<Run<K>>(r);
}


template<class K>
bool Record<K>::valid(const Run<K>* r)
{
    const K& thiskey = *(this->_key);

    if (r == nullptr)
    {
        ERROR("Record[{}]: NULL run not added", thiskey);
        return false;
    }
    if (r->weight().unknown())
    {
        WARN("Record[{}]: run {} with unknown weight not added", thiskey, *r);
        return false;
    }
    else if (r->weight().zero()) // max weight
    {
        ERROR("Record[{}]: run {} with zero weight not added", thiskey, *r);
        return false;
    }
    else
    {
        return true;
    }
}


} // end namespace Parsing

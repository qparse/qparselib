//
//  KeyS.hpp
//  squant
//
//  Created by Florent Jacquemard on 31/03/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{


#ifndef KeyS_hpp
#define KeyS_hpp

#include <stdio.h>
#include <assert.h>
#include <stack>          // std::stack
#include <queue>          // std::queue

#include "State.hpp"
#include "SymbLabel.hpp"
#include "Transition.hpp"
#include "ParsingEnv.hpp"
#include "Key.hpp"
#include "Ranked.hpp"

namespace Parsing{

/// Key made of one state of a base WTA (trivial augmented state).
/// no attributes.
/// extended with rank for k-best algo (not used for 1-best).
class KeyS : public Key<KeyS>
{
    
public:
    
    /// @brief top constructor.
    /// allocate an initial key
    /// which will serve to create the other keys.
    /// @param s initial state of the base WTA.
    /// @param env input environment.
    KeyS(state_t s, const ParsingEnv& env);
    //KeyS* top(state_t s, const ParsingEnv& env);

    
    /// @brief copy
    KeyS(const KeyS& k);

    
    /// @brief constructor for building first child.
    /// construct a partial key to be instanciated as the first child of parent
    /// in the body of an augmented transition.
    /// @param parent head key of the augmented transition.
    ///        partial or complete.
    /// @param tr WTA transition, augmented by the transition partially
    ///        constructed here. must be inner.
    /// @param env input environment.
    /// @see Key::firstChild
    KeyS(const KeyS& parent,
         const Transition& tr,
         const ParsingEnv& env);
    
    
    /// @brief constructor of next sibling.
    /// construct a partial key to be instanciated as sibling
    /// in the body of an augmented transition.
    /// @param previous previous sibling of this key in the body of the
    ///        augmented transition. must be complete.
    /// @param i position of the new sibling in body, in 1..arity-1
    ///        (0 is the position of the first sibling).
    /// @param parent head key of the augmented transition
    ///        partial or complete.
    /// @param tr SWTA transition, augmented by the transition partially
    ///        constructed here.
    /// @param env input environment.
    /// @see Key::nextSibling
    KeyS(const KeyS& previous,
         size_t i,
         const KeyS& parent,
         const Transition& tr,
         const ParsingEnv& env);
    
       
    /// @brief constructor of complete instance.
    /// construct a complete upgrade of the head key, by copy,
    /// to be the head of an augmented transition.
    /// @param head a key to be instanciated.
    ///        must be partial.
    /// @param a label of the augmented transition.
    /// @param lastchild last child in the body of the augmented transition.
    ///        must be complete.
    /// @param env input environment.
    /// @warning should not be called for this class (head must be partial).
    /// @see Key::instanciate
    KeyS(const KeyS& head,
         label_t a,
         const KeyS& lastchild,
         const ParsingEnv& env);

    
    /// @brief constructor of complete instance for a leaf.
    /// @param head a key to instanciate as leaf. must be partial
    /// @param a label of the augmented transition. must be terminal.
    /// @param env parsing environment.
    /// @warning should not be called on this class (head must be partial)
    /// @warning leaf for head, with same args, must return 1.
    KeyS(const KeyS& head,
         label_t a,
         const ParsingEnv& env);

        
protected:
        
    /// @brief constructor for internal use
    KeyS(state_t s);

    
public:
    
    // allocate and return a special Run representing a parse failure for this key.
    // @param env parsing environment.
    // @return a pointer to a newly allocated fail run, with
    // - transition with dummy symbol, empty body, weight one in the
    //   domain of the base wta in the passed environment.
    // - fail symbol
    // - weight one (in the domain of the base wta).
    // This returned run will answer succesfully to Run.fail().
    // Run<KeyS>* failRun(const ParsingEnv& env) const; // default

    /// @brief this key can be an inner node, target of tr, in a run.
    /// @param tr a SWTA transition, augmented by the transition partially
    ///        constructed here.
    /// @param env parsing environment.
    virtual bool inner(const Transition& tr,
                       const ParsingEnv& env) const;

    
    /// @brief number of leaf instances that can be obtained
    ///        from this key and the given args.
    /// @param tr a SWTA transition, augmented by the transition partially constructed here.
    ///        must be nullary (leaf, terminal).
    /// @param env parsing environment.
    /// @return - 0 if there no leaf instance compatible with tr.
    ///           if this key is complete, it means that it is not compatible with tr, a, and env.
    ///         - 1 if there is exactly one leaf instance of this key compatible with tr, a and env.
    ///           if this key is complete, it means that it is compatible with tr, a, and env.
    ///           if this key is partial, the leaf constructor can be use to built the compatible instance.
    ///           can be used instead of Key::leaf.
    ///         - 2 if there are 2 or more leaf instances compatible with tr.
    ///           it means that this key is partial, the compatible instances can be built with leaves.
    /// @see Key::leaf
    virtual size_t leaf(const Transition& tr,
                        const ParsingEnv& env) const;

    
    /// @brief completion of this partial leaf key to be the head
    ///        of a nullary (leaf) transition augmenting tr.
    /// Build and store in the given queue q
    /// all ther new complete instances of this key,
    /// using the parsing environment env.
    /// @param tr a SWTA transition, augmented by the transition partially
    /// constructed here. must be nullary (leaf, terminal).
    /// @param env parsing environment.
    /// @param q empty queue used to store the complete instances.
    /// @warning should not be called on this class (only for partial keys)
    /// @warning this key must be partial and leaf return at least 2.
     /// @see Key::leaves
     virtual void leaves(const Transition& tr,
                         const ParsingEnv& env,
                         std::queue<KeyS>& q) const;

    
    /// Best label of terminal transition augmenting tr targeted to this key.
    /// @param tr a SWTA transition, possibly unlabelled or partialy labelled
    /// @param env parsing environment.
    /// @return the label maximizing the weight function for this label, this transition, this env.
    /// @warning this key must be complete.
    virtual label_t label(const Transition& tr,
                          const ParsingEnv& env) const;
    

    /// @brief The weight of an augmented transition headed by an instance
    ///        of this Key (augmented state) and augmenting tr.
    /// @param a label for the augmented transition.
    /// @param tr SWTA transition, augmented by the transition partially
    ///        constructed here.
    /// @param env parsing environment.
    /// @return the weight of a transition headed by an instance of this key
    ///         and augmenting tr.
    /// @return a returned weight of zero (absorbing in the semiring)
    ///         indicates incompatibility between this key and the transition.
    /// @warning if tr is nullary (leaf), this key must be complete.
    /// @warning if tr is not nullary (inner), this key can be partial
    ///          or complete.
    /// @warning The weight of a run headed by the augmented transition
    ///          is the semiring product of this value with the product
    ///          of weights of the subruns.
    /// @see Key::weight
    virtual Weight weight(label_t a,
                          const Transition& tr,
                          const ParsingEnv& env) const;

    
    /// @brief return WTA transitions to be generalized
    ///        to augmented transitions targeted by this key or an instance.
    /// @param s a stack to be filled with WTA transitions to be augmented.
    /// @param env parsing environment, containing a wta.
        virtual void transitions(std::stack<const Transition*>& s,
                                 const ParsingEnv& env) const;
    
    
//    /// @return a constant iterator to the first base transition in given environment
//    /// with head state the state component of this Key.
//    /// @param env parsing environment.
//    /// @warning the base SWTA and the pool SWTA must be non-NULL in env.
//    /// @todo TBR replaced by transitions()
//    virtual typename Tlist<Transition>::const_iterator
//    cbegin(const ParsingEnv& env) const;
//    
//    
//    /// @return a constant iterator to the past-the-end base transition
//    /// in given environment
//    /// with head state the state component of this Key.
//    /// @param env parsing environment.
//    /// @warning the base SWTA and the pool SWTA must be non-NULL in env.
//    /// @todo TBR replaced by transitions()
//    virtual typename Tlist<Transition>::const_iterator
//    cend(const ParsingEnv& env) const;
    
    
    /// Key assignement.
    virtual KeyS& operator= (const KeyS& k);
    
    
    /// for use as key in a unordered_multimap.
    virtual bool operator==(const KeyS& k) const;
    virtual bool operator!=(const KeyS& k) const;

    
    /// return wether this Key is an instance of p.
    /// = equality in this case because KeyS are always complete.
    virtual bool instance(const KeyS& k) const;

    
    /// all attributes of the key are valued
    virtual bool complete() const;

    
    /// accessor
    inline state_t state() const { return _state; }

    friend std::ostream& operator<<(std::ostream& o, const KeyS& k);
    
protected:
    state_t _state;

    // The leaf instances are pushed to this queue.
    //    std::queue<K> leafq;
    
    // all the complete leaves have been computed for this key
    // initialized to false, this flag must be managed by derived classes.
    //bool all_leaves;
    
    //    virtual void init_leafq(label_t a,
    //                            const Transition& tr,
    //                            const ParsingEnv& env);
};



struct KeySHasher
{
    std::size_t operator()(const  KeyS& p) const
    {
        using std::size_t;
        using std::hash;
        return std::hash<state_t>()(p.state());
    }
};

} // end namespace Parsing


#endif /* KeyS_hpp */

/// @}

//
//  TableInstances.hpp
//  squant
//
//  Created by Florent Jacquemard on 05/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{


#ifndef TableInstances_hpp
#define TableInstances_hpp


#include <stdio.h>
#include <assert.h>
#include <unordered_map> // and unordered_multimap
#include <queue>

#include "trace.hpp"
#include "Runey.hpp"
//#include "ParsingEnv.hpp"

namespace Parsing{

/// needs equality K.operator== and hash function H.operator()
template<class K, class R, class H> using MapInstances =
    std::unordered_multimap<K,R*,H>;
// other option using K.operator< : std::multimap<K,K>;


/// table of instances:
/// undorered mapping associating to partial Keys of one or several complete instances.
/// @code multimap: Key -> Record* @endcode where
/// - Key of type K (partial key)
/// - Record* over K: pointers to records associated to complete instances of Key
/// - H = Hasher for K
/// - equal+to is op. == defined in K
///
/// Every record contains a pointer to the associated complete key.
///
/// Parameters:
/// - K = concrete key class (index rows in the table)
/// - R = record class (values in table)
/// - H = KeyHasher (for keys)
template<class K, class R, class H>
class TableInstances
{
public:
    
    /// @brief empty table of instances.
    TableInstances();
    
    /// the partial key (first components) are deleted.
    /// the records are not deleted
    /// (only pointers are stored in the instance table).
    ~TableInstances();
    

    /// @brief add a pair made of a partial key and a record
    ///        to the table of instances.
    /// @param kp key. must be partial.
    /// @param rec record associated to a complete key kc, instance of kp.
    /// @warning kc must be the key of the record it.
    ///          i.e. the key of the record it must be a complete instance of kp.
    /// @warning the record it must not be already associated with kp in
    ///          this intance table.
    void add(const K& kp, R* rec);
    //typename MapRecord<K, R, H>::iterator add(const K&, const K&);

    /// @brief return iterators to the registered instances for the given key.
    /// @param kp key. must be partial.
    /// @return a pair made of:
    /// - first component is an iterator to the first record of an instance of kp registered
    /// - second component is an iterator to the end of the range
    ///   of records associated to instances of kp.
    /// The recoreds of instances (of kp) range between these two iterators,
    /// including pair::first, but not pair::second.
    std::pair<typename MapInstances<K,R,H>::iterator,
              typename MapInstances<K,R,H>::iterator>
    get(const K& kp);

    
    /// @brief searches this table for elements whose key is kp
    ///        and returns the number of elements found.
    /// @param kp key. must be partial.
    /// @return the number of records rec associated to a complete key instance
    ///         of kp such that (kp, rec) is in this instance table.
    size_t count(const K& kp) const;

    
    /// test for the presence of the given pair of keys in this association table.
    /// @param kp key. must be partial.
    /// @param it pointer to record. it associated (embeded) key
    /// must be a complete instance of kp.
    /// @return whether the pair (kp, it) is registered in the instance table.
    bool exists(const K& kp, const R* it) const;
   
    
    inline bool empty() { return _instances.empty(); }

    
    /// nb of concrete keys registered in this table.
    inline size_t size() { return _instances.size(); }
    

    // DEBUG functions
    
    inline size_t nb_entries() { return _rows; }
    
    void dump() const;
    
    
private:
    
    /// associate to every registered partial ptr p
    /// an iterator to pairs (p', r)
    /// where p' is a complete instance of p assciated to r in _table
    MapInstances<K, R, H> _instances;
    
    /// stats.
    size_t _rows;

};

} // end namespace Parsing


// separated definition of template class
#include "TableInstances.tpp"


#endif /* TableInstances_hpp */

/// @}

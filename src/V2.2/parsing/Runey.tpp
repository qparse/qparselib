//
//  Runey.tpp
//  squant
//
//  Created by Florent Jacquemard on 28/03/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//


namespace Parsing{

template<class K>
Run<K>::Run(const Transition& tr):
_label(tr.label()),
_children(),      // no children yet
_weight(tr.weight()), // copy
_filter(tr.label(), tr.size()),     // initial duration list
_initweight(tr.weight()),   // [opt]
_subruns(),        // [opt]
_origin(tr)
{
    assert(! _weight.unknown());
}


template<class K>
Run<K>::Run(const Transition& tr, label_t lab, const Weight& w):
_label(lab),
_children(),      // no children yet
_weight(w),
_filter(lab, tr.size()),     // initial duration list
_initweight(w),   // [opt]
_subruns(),       // [opt]
_origin(tr)
{
    assert(! w.unknown());
    assert(SymbLabel::arity(lab) == tr.arity());
}


// for backward compatibility.
//template<class K>
//Run<K>::Run(size_t a, label_t lab, const Weight& w):
//Run(lab, w)
//{
//    assert(a == SymbLabel::arity(lab));
//}


// copy
template<class K>
Run<K>::Run(const Run<K>& r):
_label(r._label),
_children(r._children),       // copy
_weight(r._weight),
_filter(r._filter),
_initweight(r._initweight), // [opt]
_subruns(r._subruns),          // [opt]
_origin(r._origin)
{
    assert(! _weight.unknown());
    assert(size() <= arity());
}


template<class K>
void Run<K>::init_best(const Run<K>& r,
                       Record<K>* rec,
                       const Run<K>* best)
{
    assert(this->partial());
    assert(this->inner());

    assert(rec);
    assert(rec->key());
    assert(rec->key()->complete());
    _children.push_back(rec);
    
    if (best == nullptr)
    {
        _weight = Weight();
        _filter.reset(r._label,  _origin.size());
    }
    else
    {
        assert(best->complete());
        assert(! best->weight().unknown());
        if (best->weight().zero())
            ERROR("RunUpdate {}: zero-weight subrun {}", r, *best);
        _weight *= best->weight();
        _filter.update(best->_filter);
    }
    _subruns.push_back(best);  // [opt]
}


// copy/update
template<class K>
Run<K>::Run(const Run<K>& r,
            Record<K>* rec):
Run(r) // copy
{
    assert(rec);
    const Run<K>* best = rec->best(1);
    init_best(r, rec, best);
}


// copy/update
template<class K>
Run<K>::Run(const Run<K>& r,
            Record<K>* rec,
            const Run<K>* best):
Run(r) // copy
{
    assert(rec);
    init_best(r, rec, best);
}


// copy/replace
//template<class K>
//Run<K>::Run(const Run<K>& r,
//            size_t i,
//            const K& k,
//            const Run<K>& best):
//Run(r)     // copy
//{
//    assert(this->complete());
//    assert(this->inner());
//    assert(r.complete());
//    assert(0 <= i);
//    assert(i < arity());
//    assert(k.complete());
//    assert(best.complete());
//    assert(! best.weight().unknown());
//    
//    _children[i] = &k;   // copy
//    _subruns[i] = &best; // [opt]
//    
//    // weight and recomputation
//    // alt: divide by weight of former _children[i]
//    _weight = _initweight;
//    _filter.reset(_label);
//    // typename std::vector<const Run<K>*>::const_iterator
//    for (auto it = _subruns.cbegin();
//         it != _subruns.cend(); ++it)
//    {
//        assert(*it);
//        assert(! *it->weight().unknown());
//        _weight *= *it->weight();
//        _filter.update(*it->_filter);
//    }
//    
//    assert(complete());
//}


template<class K>
Run<K>::~Run()
{
    TRACE("delete Run {}", *this);
    _children.clear();
    _subruns.clear();
    //delete weight;
}


template<class K>
Run<K>& Run<K>::operator= (const Run<K>& rhs)
{
    if(this != &rhs)
    {
        _label = rhs._label;
        _children.resize(rhs._children.size());
        _children = rhs._children; // copy
        _weight = rhs._weight;
        _filter = rhs._filter;
        _initweight = rhs._initweight;
        _subruns.resize(rhs._subruns.size());
        _subruns = rhs._subruns;
    };
    
    return *this;
}


template<class K>
bool Run<K>::operator==(const Run<K>& rhs) const
{
    if (_label != rhs._label)
        return false;
    
    if (_children.size() != rhs._children.size())
        return false;
    
    typename std::vector<Record<K>*>::const_iterator li =
    _children.cbegin();
    typename std::vector<Record<K>*>::const_iterator ri =
    rhs._children.cbegin();
    
    while (li != _children.cend())
    {
        /// @warning comparison of pointers (to Records)
        if (*li != *ri)
            return false;
        ++li;
        // lists have same size
        assert(ri != rhs._children.cend());
        ++ri;
    }
    
    // if the runs are identical, their weights must be the same
    assert(_weight == rhs._weight);

    return true;
}


template<class K>
bool Run<K>::complete() const
{
    assert(size() <= arity());

    // a fail run is a particular case of complete run
    return (fail() || (size() == _origin.size()));
    // return (arity() == size());
}


template<class K>
bool Run<K>::filter() const
{
    assert(this->complete());
    return _filter.filter();
}


template<class K>
bool Run<K>::fail() const
{
    assert((! SymbLabel::fail(_label)) || (_origin.dummy()));
    return SymbLabel::fail(_label);
}


template<class K>
size_t Run<K>::skip() const
{
    assert(fail());
    return SymbLabel::nbEvents(_label);
}


template<class K>
const Run<K>* Run<K>::subrun(size_t i) const
{
    return subrun(i, 1);
}


template<class K>
const Run<K>* Run<K>::subrun(size_t i, size_t n) const
{
    assert(this->complete());
    assert(i < this->size());
    assert(0 < n);
    Record<K>* rec = _children[i];
    assert(rec);
    return rec->best(n);
}


template<class K>
const K* Run<K>::key(size_t i) const
{
    assert (i < _children.size());
    Record<K>* rec = _children[i];
    assert(rec);
    const K* k = rec->key();
    assert(k);
    assert(k->complete());
    return (k);
}


//template<class K>
//Record<K>* Run<K>::operator[](size_t i) const
//{
//    assert (i < _children.size());
//    Record<K>* rec = _children[i];
//    assert(rec);
//    return rec;
//}


} // end namespace Parsing


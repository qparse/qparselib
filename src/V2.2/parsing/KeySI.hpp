//
//  KeySI.hpp
//  squanty
//
//  Created by Florent Jacquemard on 04/06/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{

#ifndef KeySI_hpp
#define KeySI_hpp

#include <stdio.h>
#include <assert.h>

#include "util.hpp"
#include "hash.hpp"
#include "MTU.hpp"
#include "KeyS.hpp"
#include "Distance.hpp"
#include "GaussianDistance.hpp"
#include "IntervalTree.hpp"
#include "IntervalHeap.hpp"

namespace Parsing{

/// Key (augmented SWTA state) made of
/// - one state of a base WTA
/// - one interval (inherited attribute)
/// - rank for k-best algo (not used for 1-best).
class KeySI : public KeyS  // Key<KeySI>
{
public:
        
    /// @brief top constructor.
    /// allocate an initial key
    /// which will serve to create the other keys.
    /// @param barlen first estimated bar duration in rtu.
    /// @param env input environment.
    /// @param f_open flag true iff the initial MTU interval is right-open
    /// if false, the right bound is set to the duration of input segment.
    KeySI(rtu_t barlen, const ParsingEnv& env, bool f_open=true);
    

    /// @brief copy
    KeySI(const KeySI& k);
    
    
    /// @brief constructor of first child.
    /// construct a partial key to be instanciated as the first child of parent
    /// in the body of an augmented transition.
    /// @param parent head key of the augmented transition.
    ///        partial or complete.
    ///        inner must be true for parent key.
    /// @param tr WTA transition, augmented by the transition partially
    ///        constructed here. must be inner.
    /// @param env input environment.
    KeySI(const KeySI& parent,
          const Transition& tr,
          const ParsingEnv& env);
    
    
    /// @brief constructor for building next sibling.
    /// construct a partial key to be instanciated as sibling
    /// in the body of an augmented transition.
    /// @param previous previous sibling of this key in the body of the
    ///        augmented transition. must be complete.
    /// @param i position of the next sibling in body, in 1..arity-1
    ///        (0 is the position of the first child).
    /// @param parent head key of the augmented transition
    ///        partial or complete.
    /// @param tr SWTA transition, augmented by the transition partially
    ///        constructed here.
    /// @param env input environment.
    KeySI(const KeySI& previous,
          size_t i,
          const KeySI& parent,
          const Transition& tr,
          const ParsingEnv& env);
    

    /// @brief constructor of complete instance.
    /// construct a complete upgrade of the head key, by copy,
    /// to be the head of an augmented transition.
    /// @param head a key to be instanciated. must be partial.
    /// @param a label of the augmented transition.
    /// @param lastchild last child in the body of the augmented transition.
    ///        must be complete.
    /// @param env input environment.
    /// @warning should not be called for this class (head must be partial).
    /// @see Key::instanciate
    KeySI(const KeySI& head,
          label_t a,
          const KeySI& lastchild,
          const ParsingEnv& env);
    
    
    /// @brief constructor of complete instance for a leaf.
    /// @param head a key to instanciate as leaf. must be partial
    /// @param a label of the augmented transition. must be terminal.
    /// @param env parsing environment.
    /// @warning should not be called on this class (head must be partial)
    /// @warning leaf for head, with same args, must return 1.
    KeySI(const KeySI& head,
          label_t a,
          const ParsingEnv& env);
    
    
protected:
    
    /// @brief constructor for internal use
    KeySI(state_t s, IntervalTree* node, rtu_t barlen);

    
public:
    
    /// allocate and return a special Run representing a parse failure for this key.
    /// @param env parsing environment.
    /// @return a pointer to a newly allocated fail run, with
    /// - transition with dummy symbol, empty body, weight one in the
    ///   domain of the base wta in the passed environment.
    /// - fail symbol
    /// - weight one (in the domain of the base wta).
    /// This returned run will answer succesfully to Run.fail().
    Run<KeySI>* failRun(const ParsingEnv& env) const;
    
    /// @brief number of leaf instances that can be obtained
    ///        from this key and the given args.
    /// @param tr a SWTA transition, augmented by the transition partially
    /// constructed here. must be nullary (leaf, terminal).
    /// @param env parsing environment.
    /// @return - 0 if there no leaf instance compatible with tr.
    ///           if this key is complete, it means that it is not compatible
    ///           with tr, a, and env.
    ///         - 1 if there is exactly one leaf instance of this key compatible
    ///           with tr, a and env.
    ///           if this key is complete, it means that it is compatible
    ///           with tr, a, and env.
    ///           if this key is partial, the leaf constructor can be use
    ///           to built the compatible instance.
    ///           can be used instead of Key::leaf.
    ///         - 2 if there are 2 or more leaf instances compatible with tr.
    ///           it means that this key is partial, the compatible instances
    ///           can be built with leaves.
    virtual size_t leaf(const Transition& tr,
                        const ParsingEnv& env) const;
    
//    /// @brief completion of this partial leaf key to be the head
//    ///        of a nullary (leaf) transition augmenting tr.
//    /// Build and store in the given queue q
//    /// all ther new complete instances of this key,
//    /// using the parsing environment env.
//    /// @param tr a SWTA transition, augmented by the transition partially
//    /// constructed here. must be nullary (leaf, terminal).
//    /// @param env parsing environment.
//    /// @param q empty queue used to store the complete instances.
//    /// @warning this Key must be partial.
    virtual void leaves(const Transition& tr,
                        const ParsingEnv& env,
                        std::queue<KeySI>& q) const;
    
    /// Best label of terminal transition augmenting tr targeted to this key.
    /// @param tr a SWTA transition, possibly unlabelled or partialy labelled
    /// @param env parsing environment.
    /// @return the label maximizing the weight function for this label,
    /// this transition, this env.
    /// @warning this key must be complete.
    virtual label_t label(const Transition& tr,
                          const ParsingEnv& env) const;
    
    
    /// @brief The weight of an augmented transition headed by an instance
    ///        of this Key (augmented state) and augmenting tr.
    /// @param a label for the augmented transition.
    /// @param tr SWTA transition, augmented by the transition partially
    ///        constructed here.
    /// @param env parsing environment.
    /// @return the weight of a transition headed by an instance of this key
    ///         and augmenting tr.
    /// @return a returned weight of zero (absorbing in the semiring)
    ///         indicates incompatibility between this key and the transition.
    /// @warning if tr is nullary (leaf), this key must be complete.
    /// @warning if tr is not nullary (inner), this key can be partial or complete.
    /// @warning The weight of a run headed by the augmented transition
    ///          is the semiring product of this value with the product
    ///          of weights of the subruns.
    /// @todo complete this stub with Distance
    /// @see Key::weight
    virtual Weight weight(label_t a,
                          const Transition& tr,
                          const ParsingEnv& env) const;

    /// @brief return WTA transitions to be generalized
    ///        to augmented transitions targeted by this key or an instance.
    /// @param s a stack to be filled with WTA transitions to be augmented.
    /// @param env parsing environment, containing a wta
    /// and pool of multi-bar transitions. can be modified.
    virtual void transitions(std::stack<const Transition*>& s,
                             ParsingEnv& env) const;
    
    /// @brief Key assignement.
    virtual KeySI& operator= (const KeySI& p);
    
    /// @brief for use as key in a unordered_multimap.
    virtual bool operator==(const KeySI& p) const;
    virtual bool operator!=(const KeySI& p) const;
    
    /// @brief return wether this Key is an instance of p.
    /// = equality in this case because KeyS are always complete.
    virtual bool instance(const KeySI& p) const;
   
    /// @brief all attributes of the key are valued
    virtual bool complete() const;
  
    /// @brief accessor
    virtual IntervalTree* interval() const { return _interval; }
    
    /// @brief printer
    friend std::ostream& operator<<(std::ostream& o, const KeySI& p);
    
protected:
    rtu_t _barlen; // current bar length. useful only for meta keys.

    IntervalTree* _interval;  // should not be modified
       
    /// @brief accessor
    virtual size_t size() const;

    bool equal_interval(const KeySI&) const;
    
    /// @brief weight for the case of a and tr inner.
    virtual Weight innerWeight(label_t a,
                               const Transition& tr,
                               const ParsingEnv& env) const;
    
    /// @brief weight for the case of a and tr terminal.
    virtual Weight terminalWeight(label_t a,
                                  const Transition& tr,
                                  const ParsingEnv& env) const;
       
    /// @param s target state of the build transition
    /// @param env parsing environment, containing a pool
    /// of multi-bar transitions. can be modified.
    /// @return reference of the transition added.
    virtual Transition& addNullaryBarTransition(state_t s,
                                                ParsingEnv& env) const;

    /// @param s target state of the build transition
    /// @param env parsing environment, containing a pool
    /// of multi-bar transitions. can be modified.
    /// @return reference of the transition added.
    virtual Transition& addBinaryBarTransition(state_t s,
                                               ParsingEnv& env) const;
    
    virtual void transitionsWTA(std::stack<const Transition*>& st,
                                ParsingEnv& env) const;

    virtual void transitionsBar(std::stack<const Transition*>& st,
                                ParsingEnv& env) const;

    
    /// @brief compatibility of this key with the given label symbol.
    /// @param a a label. must be inner.
    /// @param env parsing environment.
    /// @return whether the given symbol a can label
    ///         a transition targeted by this key.
    virtual bool filterInnerLabel(const label_t& a,
                                  const ParsingEnv& env) const;

    /// @brief compatibility of this key with the given transition.
    /// @param tr a wta transition. must be inner.
    /// @param env parsing environment.
    /// @return whether the given transition tr can be generalized
    ///         to a transition targeted by this key.
    virtual bool filterInnerTransition(const Transition& tr,
                                       const ParsingEnv& env) const;
    
    /// @brief number of points to read in the input segment
    /// in order to make a transition to this Key state.
    /// @todo RM : replaced by size()
    virtual inline size_t points() const { return _interval->lsize(); }

    // @brief number of input point to read for transition from the given
    // label symbol to this key.
    // @param a a label. must be terminal.
    // @param env parsing environment.
    // @return -1 if the given symbol a cannot label a transition targeted by
    // this key, or, otherwise, the number of points of the interval
    // to read (parse) when making the transition from symbol a to this Key.
    //virtual long pointsTerminalLabel(const label_t& a,
    //                                 const ParsingEnv& env) const;
    
    /// @brief compatibility of this key with the given label symbol.
    /// @param a a label. must be terminal.
    /// @param env parsing environment.
    /// @return whether the given symbol a can label
    ///         a transition targeting to this key.
    virtual bool filterTerminalLabel(const label_t& a,
                                     const ParsingEnv& env) const;
    
    /// @brief compatibility of this key with the given transition.
    /// @param tr a wta transition. must be terminal.
    /// @param env parsing environment.
    /// @return whether the given transition tr can be generalized
    ///         to a transition targeted by this key.
    virtual bool filterTerminalTransition(const Transition& tr,
                                          const ParsingEnv& env) const;

};


/// hash function for using as key in a table.
/// rank is ignoreds : same as SpointerHasher
struct KeySIHasher
{
    std::size_t operator()(const KeySI& k) const
    {
        using std::size_t;
        using std::hash;
        assert(k.interval());
        
        // see constant.h
        std::size_t h = HASH_SEED;
        h += hash<state_t>()(k.state());
        h = h * HASH_FACTOR + hash<rtu_t>()(k.interval()->rbegin());
        h = h * HASH_FACTOR + hash<rtu_t>()(k.interval()->rduration());
        h = h * HASH_FACTOR + hash<mtu_t>()(k.interval()->mbegin());
        h = h * HASH_FACTOR + hash<mtu_t>()(k.interval()->mduration());
        return h;
    }
};

} // end namespace Parsing


#endif /* KeySI_hpp */

/// @}



/// allocate a partial key to be instanciated as the first child
/// of this key in the body of a transition augmenting tr,
/// and for an input environment env.
/// i.e. this key is the head key of the augmented transition,
/// it can be partial or complete.
/// @param tr WTA transition, augmented by the transition partially
///        constructed here. must be inner.
/// @param env input environment.
//    KeySI* firstChild(const Transition& tr,
//                      const ParsingEnv& env);


/// allocate a partial key to be instanciated as the
/// next sibling of this key
/// in the body of a transition augmenting tr,
/// and for an input environment env.
/// @param i position of the returned new sibling in body.
/// @param parent head key of the augmented transition
///        partial or complete.
/// @param tr SWTA transition, augmented by the transition partially
///        constructed here.
/// @param env input environment.
/// @warning this key must be complete.
//    KeySI* nextSibling(size_t i,
//                       const KeySI* parent,
//                       const Transition& tr,
//                       const ParsingEnv& env);

/// allocate a complete upgrade of this key, by copy,
/// to be the head of an augmented transition,
/// using the input environment env.
/// @param a label of the augmented transition.
/// @param lastchild last child in the body of the augmented transition.
///        must be complete.
/// @param env input environment.
/// @warning this Key must be partial: should not be called for this class.
///
//    KeySI* instanciate(label_t a,
//                       const KeySI* lastchild,
//                       const ParsingEnv& env);

/// allocate and return new cmplete updates of this key, by copy,
/// to be the head of a leaf augmented transition,
/// using the input environment env.
/// @param a label of the augmented transition.
/// @param tr SWTA transition, augmented by the transition partially
///        constructed here.
/// @param env input environment.
/// @warning this Key must be partial: should not be called for this class.
//    std::queue<KeySI*> leaf(label_t a,
//                            const Transition& tr,
//                            const ParsingEnv& env);

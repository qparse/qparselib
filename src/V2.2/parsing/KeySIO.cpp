//
//  KeySIO.cpp
//  squant2
//
//  Created by Florent Jacquemard on 02/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "KeySIO.hpp"


namespace Parsing{


// top constructor (top state = first bar)
KeySIO::KeySIO(rtu_t len, const ParsingEnv& env, bool f_open,
               pre_t pre, pre_t preon, bool prelaston):
KeySI(len, env, f_open),
_pre(pre),
_pre_on(preon),
_pre_laston(prelaston),
_post(P_UNKNOWN),
_post_on(P_UNKNOWN),
_post_laston(false)
{
    assert(P_KNOWN(pre));
    assert(P_KNOWN(preon));
}


// internal use
KeySIO::KeySIO(state_t s, IntervalTree* node, rtu_t len,
               pre_t pre, pre_t pre_on,  bool pre_laston,
               pre_t post, pre_t post_on,  bool post_laston):
KeySI(s, node, len),
_pre(pre),
_pre_on(pre_on),
_pre_laston(pre_laston),
_post(post),
_post_on(post_on),
_post_laston(post_laston)
{ }


// copy
KeySIO::KeySIO(const KeySIO& k):
KeySI(k),
_pre(k._pre),
_post(k._post)
{ }




// firstChild
KeySIO::KeySIO(const KeySIO& parent,
               const Transition& tr,
               const ParsingEnv& env):
KeySI(parent, tr, env),
_pre(parent._pre),
_pre_on(parent._pre_on),
_pre_laston(parent._pre_laston),
_post(P_UNKNOWN),
_post_on(P_UNKNOWN),
_post_laston(false)
{
    assert(P_KNOWN(_pre));
    assert(P_KNOWN(_pre_on));
    // construction of sub-interval is the same as in KeySI
    /// @todo cases grace and fork
}


// nextSibling
KeySIO::KeySIO(const KeySIO& previous,
             size_t i,
             const KeySIO& parent,
             const Transition& tr,
             const ParsingEnv& env):
KeySI(previous, i, parent, tr, env),
_pre(previous._post),
_pre_on(previous._post_on),
_pre_laston(previous._post_laston),
_post(P_UNKNOWN),
_post_on(P_UNKNOWN),
_post_laston(false)
{
    assert(previous.complete());
    // construction of sub-interval is the same as in KeySI
    /// @todo cases grace and fork
}


// instanciate: copy and up-propagation of post
KeySIO::KeySIO(const KeySIO& head,
               label_t a,
               const KeySIO& lastchild,
               const ParsingEnv& env):
KeySI(head, a, lastchild, env),
_pre(head._pre),
_pre_on(head._pre_on),
_pre_laston(head._pre_laston),
_post(lastchild._post),
_post_on(lastchild._post_on),
_post_laston(lastchild._post_laston)
{
    assert(head.partial());
    assert(lastchild.complete());
    // this key is now complete
    assert(P_KNOWN(this->_pre));
    assert(P_KNOWN(this->_pre_on));
    assert(P_KNOWN(this->_post));
    assert(P_KNOWN(this->_post_on));
}


// complete leaf: copy and computation of post
KeySIO::KeySIO(const KeySIO& head,
               label_t a,
               const ParsingEnv& env):
KeySI(head, a, env),
_pre(head._pre),
_pre_on(head._pre_on),
_pre_laston(head._pre_laston)
{
    assert(SymbLabel::terminal(a));
    assert(_interval);
    assert(P_KNOWN(_pre));
    assert(P_KNOWN(_pre_on));

    if (P_KNOWN(head._post))
    {
        assert(P_KNOWN(head._post_on));
        ERROR("KeySIO : instance as leaf of complete ptr {}", head);
        _post = head._post;
        assert(_post == _interval->rsize());
        assert(_post_on == _interval->ronsets());
        assert(_post_laston == laston(env));
    }
    else
    {
        if (! _interval->aligned())
        {
            // case of terminal bar symbol (double bar)
            assert(SymbLabel::bar(a));
            assert(env.segment);
            _interval->align(env.segment);
        }
        _post = _interval->rsize();
        _post_on = _interval->ronsets();
        _post_laston = laston(env);
    }
    assert(P_KNOWN(_post));
    assert(P_KNOWN(_post_on));
}


size_t KeySIO::size() const
{
    assert(_interval);
    return _pre + _interval->lsize();
}


Run<KeySIO>* KeySIO::failRun(const ParsingEnv& env) const
{
    assert(env.wta);
    const Transition& t1 = env.dummyTransition_one();
    assert(t1.weight().one());
    if (! _interval->aligned())
    {
        assert(env.segment);
        _interval->align(env.segment);
    }
    label_t a = SymbLabel::make_fail(this->size());

    return new Run<KeySIO>(t1, a, t1.weight());
}


void KeySIO::leaves(const Transition& tr,
                    const ParsingEnv& env,
                    std::queue<KeySIO>& q) const
{
    assert(this->partial());
    assert(q.empty());
    size_t nbl = leaf(tr, env);
    
    if (nbl == 0)
    {
        ERROR("KeySIO: leaves called for 0 leaf");
    }
    else if (nbl == 1)
    {
        ERROR("KeySIO: leaves called for 1 leaf, call cstr instead");
        label_t a = tr.label();
        q.emplace(*this, a, env);
    }
}

// KeySIO::innerWeight = KeySI::innerWeight

Weight KeySIO::terminalWeight(label_t a,
                              const Transition& tr,
                              const ParsingEnv& env) const
{
    assert(tr.terminal());
    assert(SymbLabel::terminal(a));
    assert(this->complete());
    assert(this->leaf(tr, env) != 0);

    // tweight = transition weight = complexity
    Weight tweight = tr.weight(); // copy
    
    assert(! tweight.unknown());

    // terminal bar interval:
    // there are no points hence no need to compute the distance
    if (SymbLabel::bar(tr.label()) && (SymbLabel::arity(tr.label())) == 0)
    {
        TRACE("KeySIO: terminal bar interval: {} ({}aligned) for transition {}",
              *(_interval), ((! _interval->aligned())?"not ":""), tr);
        assert(tr.label() == a);
        assert(! _interval->inhabited());
        assert(_interval->rduration() < _barlen);
    }
    // combine transition weight with distance weight
    // CST_WEIGHT_TYPE == WeightDom::STOCHASTIC
    else if (tweight.hasType("ViterbiWeight"))
    {
        assert(env.segment);
        assert(_interval);
        TRACE("KeySIO: new bar interval: {} ({}aligned) for transition {}",
              *(_interval), ((! _interval->aligned())?"not ":""), tr);
        //assert(_pre != PP_UNKNOWN);          // csq complete
        //assert(_post != PP_UNKNOWN);         // csq complete
        //assert(_post == _interval->rsize()); // csq filterTerminal by leaf
        tweight *= Weight(new GaussianDistance(env.segment, _interval));
    }

    // CST_WEIGHT_TYPE == WeightDom::PENALTY
    else if (tweight.hasType("TropicalWeight"))
    {
        assert(_interval);
        // compute distance to input segment
        // ALPHA applied to dist only (not complexity)
        Weight dist = Weight(new Distance(env.segment, _interval));
        TRACE("KeySIO: tropical distance for inter={} tr={} dist={}",
              *(this), tr, dist);

        // new weight = (1 - alpha) . former transition weight * alpha . distance
        // update: alpha applied to dist only (not to complexity)
        tweight *= dist; // tropical weight product is a sum
    }
    else
    {
        ERROR("KeySIO.weight: unexpected weight type");
        // tweight will be UNDEF
    }
    
    return tweight;
}


// KeySIO::filterInnerLabel: same as for KeySI


bool KeySIO::checkPost(const ParsingEnv& env) const
{
    return (_post == P_UNKNOWN) ||
            ((_post == _interval->rsize()) &&
             (_post_on == _interval->ronsets()) &&
             (_post_laston == laston(env)));
}


bool KeySIO::laston(const ParsingEnv& env) const
{
    assert(_interval);
    assert(_interval->aligned());
    if (_interval->rsize() == 0) return false;
    
    // index of last point in right half of interval
    size_t last = _interval->rfirst();
    last += (_interval->rsize() - 1);
    assert(last < _interval->next());
    assert(env.segment);
    const MusPoint& p = env.segment->point(last);
    return p.onset();
}


//bool KeySIO::filterTerminalLabel(const label_t& a,
//                                 const ParsingEnv& env) const
//{
//    assert(_interval);
//    assert(_interval->aligned());
//    long res = pointsTerminalLabel(a, env);
//    INFO("FILTER {}..{}|{}-{} : {} = {}  {}   pre={} lsize={} lfirst={}",
//         // first in pre
//         ((_pre > 0)?((_interval->lsize() > 0)?
//                      std::to_string(_interval->lfirst() - _pre):
//                      std::to_string(_interval->next() - _pre - 1)):"  "),
//         // last in pre (optional)
//         ((_pre > 0)?((_interval->lsize() == 0)?
//                      std::to_string(_interval->next()-1):""):""),
//         // first in left hand
//         ((_interval->lsize() > 0)?std::to_string(_interval->lfirst()):"  "),
//         // last in left hand
//         ((_interval->lsize() > 0)?
//          std::to_string(_interval->lfirst()+_interval->lsize()-1):"  "),
//         // symbol
//         SymbLabel::to_string(a),
//         // result of filter
//         ((res >= 0)?std::to_string(res):"NOT"),
//         // details
//         *(_interval),
//         _pre, _interval->lsize(), _interval->lfirst());
//
//
//    return (pointsTerminalLabel(a, env) >= 0);
//}


bool KeySIO::filterTerminalLabel(const label_t& a,
                                 const ParsingEnv& env) const
{
    assert(SymbLabel::terminal(a));
    assert(_interval);
    if (! _interval->aligned())
    {
        WARN("KeySIO: must align interval {} for filtering {}",
             *(_interval), SymbLabel::to_string(a));
        assert(env.segment);
        _interval->align(env.segment);
    }

    /// double bar (end of score)
    if (SymbLabel::bar(a))
    {
        assert(_interval);
        // && (DBL_APPROX_EQUAL(_interval->rend(), _interval->rbegin()))
        return ((_interval->rduration() < _barlen) &&
                (! _interval->inhabited()));
        // 0 points to read in interval or filtered out
    }
    else if (SymbLabel::dummy(a))
    {
        WARN("KeySIO: filter dummy symbol");
        return false;
    }
    else if (SymbLabel::continuation(a) || SymbLabel::dot(a))
    {
        assert(checkPost(env)); // not related to a
        return (_pre + _interval->lsize() == 0);
        // 0 points to read in interval or filtered out
    }
    /// @todo missing case: rest preceeded by grace notes.
    /// here it is excluded. we consider only one rest alone.

    /// exactly 1 event in pre + left-half of interval and it is offset
    else if (SymbLabel::rest(a))
    {
        // assert(checkPost(env)); // should be unknown
        // 1 offset in pre
        if ((_pre == 1) && (_interval->lsize() == 0) && (! _pre_laston))
        {
            return true;
            // 1 points to read in interval
        }
        // 1 offset in left-half of interval
        else if ((_pre == 0) && (_interval->lsize() == 1))
        {
            // last point = first point
            size_t last = _interval->lfirst();
            assert(env.segment);
            return (env.segment->point(last).offset());
            // 1 points to read in interval or filtered out
        }
        // special case of performance beginning with a gap (not an event).
        else if ((_pre == 0) &&
                 (_interval->lsize() == 0) &&
                 DBL_APPROX_EQUAL(_interval->rbegin(), 0))
                 // (_interval->rbegin() == 0)
        {
            WARN("KeySIR: first event in performance is not at 0, padding with a rest");
            return true;  // 1 points to read
        }
        else
            return false;
    }
    /// last event in pre + left-half of interval is an onset
    else if (SymbLabel::note(a))
    {
        assert(checkPost(env)); // should be unknown
        assert(_pre != P_UNKNOWN);
        assert(_pre_on != P_UNKNOWN);
        if (_interval->lsize() == 0)
        {
            return ((_pre_laston) && // false if pre empty
                     // nb of g.n. + 1 note = nb of onsets
                    (SymbLabel::nbEvents(a) == _pre_on));
            // _pre points to read in interval (onsets and offsets)
            // or filtered out
        }
        else
        {
            // last point is onset?
            size_t last = _interval->lfirst();
            last += (_interval->lsize() - 1);
            assert(last < _interval->rfirst());
            assert(last < _interval->next());
            assert(env.segment);
            return (env.segment->point(last).onset() &&
                    // nb of g.n. + 1 note = nb of onsets
                    (SymbLabel::nbEvents(a) ==
                     (_pre_on + _interval->lonsets())));
            // (_pre + _interval->lsize()) points to read (onsets and offsets)
            // or filtered out
        }
    }
    else
    {
        ERROR("KeySIO: unexpected terminal symbol {}", SymbLabel::to_string(a));
        return false;
    }
}


KeySIO& KeySIO::operator= (const KeySIO& p)
{
    if(this != &p)
    {
        KeySI::operator=(p);
        _pre = p._pre;
        _pre_on = p._pre_on;
        _pre_laston = p._pre_laston;
        _post = p._post;
        _post_on = p._post_on;
        _post_laston = p._post_laston;
    };
    return *this;
}


//bool KeySIO::operator==(const KeySIO& p) const
//{
//    if (KeySI::operator!=(p))
//        return false;
//    else
//        return ((_pre == p._pre) && (_post == p._post));
//}


bool KeySIO::operator==(const KeySIO& p) const
{
    return (KeySI::operator==(p) &&
            (_pre == p._pre) &&
            (_pre_on == p._pre_on) &&
            (_pre_laston == p._pre_laston) &&
            (_post == p._post) &&
            (_post_on == p._post_on) &&
            (_post_laston == p._post_laston));
//            this->equal_interval(p));
}


bool KeySIO::operator!=(const KeySIO& p) const
{
    return (! (*this == p));
}


bool KeySIO::instance(const KeySIO& p) const
{
    assert(_pre != P_UNKNOWN);
    assert(_pre_on != P_UNKNOWN);
    assert(p._pre != P_UNKNOWN);
    assert(p._pre_on != P_UNKNOWN);
    return (KeySI::instance(p) &&
            ((p._post == P_UNKNOWN) || ((_post == p._post) &&
                                        (p._post_on != P_UNKNOWN) &&
                                        (_post_on == p._post_on) &&
                                        (_post_laston == p._post_laston))));
}


// always complete
bool KeySIO::complete() const
{
    assert(_pre != P_UNKNOWN);
    assert((_post != P_UNKNOWN) || (_post_on == P_UNKNOWN));
    assert((_post_on != P_UNKNOWN) || (_post == P_UNKNOWN));
    return (KeySI::complete() && (_post != P_UNKNOWN));
}


std::ostream& operator<<(std::ostream& o, const KeySIO& p)
{
    if (p._interval == NULL)
    {
        o << KEY_LPAR << p.state() << KEY_RPAR;
        return o;
    }
    
    o << KEY_LPAR;
    o << " " << p.state() << " ";
    
    // short version
    if (p._interval)
    {
        o << *(p._interval);
    }
    else
    {
        o << "[]";
    }
    assert(p._pre != P_UNKNOWN);
    o << " pre=" << p._pre;
    o << " pre_on=" << p._pre_on;
    o << " pre_laston=" << p._pre_laston;

    if (p._post == P_UNKNOWN)
        o << " post=UNK";
    else
    {
        o << " post=" << p._post;
        o << " post_on=" << p._post_on;
        o << " post_laston=" << p._post_laston;
    }
    o << KEY_RPAR;    
    return o;
}


} // end namespace Parsing

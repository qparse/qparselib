//
//  Recordey.hpp
//  squanty
//
//  Created by Florent Jacquemard on 07/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{

#ifndef Recordey_hpp
#define Recordey_hpp

#include <stdio.h>
#include <assert.h>

#include "trace.hpp"
#include "Runey.hpp"
#include "RunCompare.hpp"


namespace Parsing{

//template<class K> class Run;
//template<class K> class RunCompare;
//template<class K, class R, class H> class TableParse;


/// @brief abstract class describing the basic functionalities of a record.
///
/// each record is associated with a Key (target of transitions)
/// and contains bodies of transition.
/// It can be filled with add
/// and can be interrogating with best,
/// for retrieving the best runs for the associated Key.
///
/// When uncomplete runs are added to the record
/// (either by the record or from outside)
/// their weight must be computed using a table.
///
/// [update] the runs with weight zero (still invalid) can be added to records
/// but an error message is displayed (for debugging).
template<class K>
class Record
{
public:
    
    Record() { assert(false); }

    /// @brief empty record associated to k.
    /// @param comp function for comparing Runs over K.
    /// @param failrun fixed run returned by best when there is no best run.
    /// @warning the key associated to the record must
    ///          be set later to a non-NULL ptr with setKey.
    Record(RunCompare<K> comp, const Run<K>* failrun);
    
    virtual ~Record();
    
    /// @brief add a run to the record.
    virtual size_t add(const Run<K>* r) = 0;
    
    /// @brief n-th best run of the record.
    /// @param table may be completed with new runs (lazy evaluation of candidates).
    /// @param n (as in n-best) is the rank of the run with target the key of the record.
    /// @return the n-th best run of the record or, if there is none,
    /// the failRun set or NULL when the failRun was not set with setFailRun.
    /// @warning the record can be modified.
    virtual const Run<K>* best(size_t n=1) = 0;
    //    virtual Run<K>* best(TableI<K>* table, size_t n=1) = 0;

    /// @brief key associated to the record.
    inline const K* key() const { return _key; }
    
    /// @brief set key associated to the record.
    /// @param k new key for this record. must be complete.
    /// @warning the current key of this record must be NULL.
    /// @warning this record must be empty.
    void setKey(const K* k);
    
    /// @brief set the fail Run associated to the record.
    /// @param r the fail run to be set.
    /// @warning the current fail Run of this record must be NULL.
    void setFailRun(const Run<K>* r);
   
    /// at least one run has been added succesfully.
    virtual bool empty() const = 0;
    
//    /// state - possible values:
//    /// 0 : empty (record just created no run was stored)
//    /// 1 : add(_key) was not called
//    ///     but add(p) was called for p partial and subsuming _key
//    /// 2 : add(_key) was called
//    /// 3 : some run has been stored
//    ///     but we are not in 1 or 2. should not happen.
//    ///
//    /// The state is not changed inside the Record class.
//    /// It is changed by callers (table.add).
//    /// @todo TBR (obsolete)
//    unsigned int state;
    
protected:
    
    /// copy of the key associated to the record.
    const K* _key;

    /// comparison function.
    RunCompare<K> _comp;
    
    /// generic fail run associated to the key.
    /// will be returned in case of parse faillure.
    const Run<K>* _fail;
    /// @todo shared_ptr
    // std::shared_ptr<Run<K>> _fail;
    
    /// stats: number of candidate bests constructed.
    size_t _nb_cand;
    
    /// stats: number of best not added to the list because of optimization filters.
    size_t _nb_best_rejected;
    
    /// @brief sanity check before adding run to a record.
    /// There are 3 kinds of Run* that should not be added in record:
    /// TBC should not be returned by best on the record ?
    /// - NULL ptr to Run
    /// - ptr to Run with unknown weight (i.e. weight with NULL letter).
    /// - ptr to Run with weight == zero (acc. to test zero()).
    /// a Run not in these 3 cases is called valid.
    bool valid(const Run<K>*);
    
};


} // end namespace Parsing


// separated definition of template class
#include "Recordey.tpp"


#endif /* Recordey_hpp */

/// @}

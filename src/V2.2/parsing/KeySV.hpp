//
//  KeySV.hpp
//  equiv2
//
//  Created by Florent Jacquemard on 22/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{


#ifndef KeySV_hpp
#define KeySV_hpp

#include <stdio.h>
#include <assert.h>
#include <utility>      // std::pair, std::make_pair
#include <memory>

#include "util.hpp"
#include "hash.hpp"
#include "MTU.hpp"
#include "ValueList.hpp"
#include "KeyS.hpp"
//#include "Distance.hpp"
//#include "Runey.hpp"

namespace Parsing{

//template<class K> class Run;


/// Key (augmented SWTA state) made of
/// - one state of a base WTA
/// - one quantified interval = sequence of rationals (inherited attribute)
class KeySV : public KeyS  // Key<KeySI>
{
    friend class KeySVHasher;

public:
    
    /// @brief top constructor.
    /// allocate an initial key which will serve to create the other keys.
    /// @param seg initial IOI interval, to be decomposed into bars.
    /// @param env input environment.
    KeySV(const ValueList& seg, const ParsingEnv& env);

    // internal
    // @param s initial state of the base WTA.
    // KeySV(state_t s, const ValueList& seg, const ParsingEnv& env);
    
    /// @brief copy constructor.
    KeySV(const KeySV& k);
    
    /// @brief Key assignement.
    virtual KeySV& operator= (const KeySV& k);
           
    /// @brief constructor of first child.
    /// construct a partial key to be instanciated as the first child of parent
    /// in the body of an augmented transition.
    /// @param parent head key of the augmented transition.
    ///        partial or complete.
    ///        inner must be true for parent key.
    /// @param tr WTA transition, augmented by the transition partially
    ///        constructed here. must be inner.
    /// @param env input environment.
    KeySV(const KeySV& parent,
         const Transition& tr,
         const ParsingEnv& env);
    
    /// @brief constructor for building next sibling.
    /// construct a partial key to be instanciated as sibling
    /// in the body of an augmented transition.
    /// @param previous previous sibling of this key in the body of the
    ///        augmented transition. must be complete.
    /// @param i position of the next sibling in body, in 1..arity-1
    ///        (0 is the position of the first child).
    /// @param parent head key of the augmented transition
    ///        partial or complete.
    /// @param tr SWTA transition, augmented by the transition partially
    ///        constructed here.
    /// @param env input environment.
    KeySV(const KeySV& previous,
         size_t i,
         const KeySV& parent,
         const Transition& tr,
         const ParsingEnv& env);

    /// @brief constructor of complete instance.
    /// construct a complete upgrade of the head key, by copy,
    /// to be the head of an augmented transition.
    /// @param head a key to be instanciated. must be partial.
    /// @param a label of the augmented transition.
    /// @param lastchild last child in the body of the augmented transition.
    ///        must be complete.
    /// @param env input environment.
    /// @warning should not be called for this class (head must be partial).
    /// @see Key::instanciate
    KeySV(const KeySV& head,
         label_t a,
         const KeySV& lastchild,
         const ParsingEnv& env);
    
    /// @brief constructor of complete instance for a leaf.
    /// @param head a key to instanciate as leaf. must be partial
    /// @param a label of the augmented transition. must be terminal.
    /// @param env parsing environment.
    /// @warning should not be called on this class (head must be partial)
    /// @warning leaf for head, with same args, must return 1.
    KeySV(const KeySV& head,
         label_t a,
         const ParsingEnv& env);

    /// destructor
    virtual ~KeySV();

    /// allocate and return a special Run representing a parse failure for this key.
    /// @param env parsing environment.
    /// @return a pointer to a newly allocated fail run, with
    /// - transition with dummy symbol, empty body, weight one in the
    ///   domain of the base wta in the passed environment.
    /// - fail symbol
    /// - weight one (in the domain of the base wta).
    /// This returned run will answer succesfully to Run.fail().
    Run<KeySV>* failRun(const ParsingEnv& env) const;
    // std::shared_ptr<Run<KeySV>> failRun_one(const ParsingEnv& env) const;

    /// @brief number of leaf instances that can be obtained
    ///        from this key and the given args.
    /// @param tr a SWTA transition, augmented by the transition partially
    /// constructed here. must be nullary (leaf, terminal).
    /// @param env parsing environment.
    /// @return - 0 if there no leaf instance compatible with tr.
    ///           if this key is complete, it means that it is not compatible
    ///           with tr, a, and env.
    ///         - 1 if there is exactly one leaf instance of this key compatible
    ///           with tr, a and env.
    ///           if this key is complete, it means that it is compatible
    ///           with tr, a, and env.
    ///           if this key is partial, the leaf constructor can be use
    ///           to built the compatible instance.
    ///           can be used instead of Key::leaf.
    ///         - 2 if there are 2 or more leaf instances compatible with tr.
    ///           it means that this key is partial, the compatible instances
    ///           can be built with leaves.
    virtual size_t leaf(const Transition& tr,
                        const ParsingEnv& env) const;
    
//    /// @brief completion of this partial leaf key to be the head
//    ///        of a nullary (leaf) transition augmenting tr.
//    /// Build and store in the given queue q
//    /// all ther new complete instances of this key,
//    /// using the parsing environment env.
//    /// @param tr a SWTA transition, augmented by the transition partially
//    /// constructed here. must be nullary (leaf, terminal).
//    /// @param env parsing environment.
//    /// @param q empty queue used to store the complete instances.
//    /// @warning this Key must be partial.
    virtual void leaves(const Transition& tr,
                        const ParsingEnv& env,
                        std::queue<KeySV>& q) const;
    
    /// Best label of terminal transition augmenting tr targeted to this key.
    /// @param tr a SWTA transition, possibly unlabelled or partialy labelled
    /// @param env parsing environment.
    /// @return the label maximizing the weight function for this label,
    /// this transition, this env.
    /// @warning this key must be complete.
    virtual label_t label(const Transition& tr,
                          const ParsingEnv& env) const;
    
    /// @brief The weight of an augmented transition headed by an instance
    ///        of this Key (augmented state) and augmenting tr.
    /// @param a label for the augmented transition.
    /// @param tr SWTA transition, augmented by the transition partially
    ///        constructed here.
    /// @param env parsing environment.
    /// @return the weight of a transition headed by an instance of this key
    ///         and augmenting tr.
    /// @return a returned weight of zero (absorbing in the semiring)
    ///         indicates incompatibility between this key and the transition.
    /// @warning if tr is nullary (leaf), this key must be complete.
    /// @warning if tr is not nullary (inner), this key can be partial or complete.
    /// @warning The weight of a run headed by the augmented transition
    ///          is the semiring product of this value with the product
    ///          of weights of the subruns.
    /// @todo complete this stub with Distance
    /// @see Key::weight
    virtual Weight weight(label_t a,
                          const Transition& tr,
                          const ParsingEnv& env) const;

    /// @brief return WTA transitions to be generalized
    ///        to augmented transitions targeted by this key or an instance.
    /// @param s a stack to be filled with WTA transitions to be augmented.
    /// @param env parsing environment, containing a wta
    /// and pool of multi-bar transitions. can be modified.
    virtual void transitions(std::stack<const Transition*>& s,
                             ParsingEnv& env) const;
       
    /// @brief for use as key in a unordered_multimap.
    virtual bool operator==(const KeySV& p) const;
    virtual bool operator!=(const KeySV& p) const;
        
    /// @brief return wether this Key is an instance of p.
    /// = equality in this case because KeyS are always complete.
    virtual bool instance(const KeySV& p) const;
    
    /// @brief all attributes of the key are valued
    virtual bool complete() const;
    
    /// @brief accessor
    /// sequence of IOI (rationals) associated to this state.
    virtual const ValueList& value() const { return _list; }

    /// @brief printer
    friend std::ostream& operator<<(std::ostream& o, const KeySV& k);

protected:
    
    /// sequence of IOI (rationals) associated to this state.
    ValueList _list;

    /// start date in parent list.
    /// convenience to avoid recomputation. ignored for key equality.
    mtu_t _start;
    
    /// is fierst child
    /// @todo RM (debug)
    bool _fs;
    
    // @brief constructor for internal use
    // KeySV(state_t s, const ValueList& seg);
    
    /// @brief accessor
    virtual size_t size() const;
    
    /// @brief number of points to read in the input segment
    /// in order to make a transition to this Key state.
    /// @todo RM : replaced by size()
    virtual size_t points() const;
       
    /// @brief weight for the case of a and tr inner.
    /// @todo case zero redundant with filterInnerLabel ?
    virtual Weight innerWeight(label_t a,
                               const Transition& tr,
                               const ParsingEnv& env) const;
    
    /// @brief weight for the case of a and tr terminal.
    /// @todo case zero redundant with filterTerminalLabel ?
    virtual Weight terminalWeight(label_t a,
                                  const Transition& tr,
                                  const ParsingEnv& env) const;
       
    /// @param s target state of the build transition
    /// @param env parsing environment, containing a pool
    /// of multi-bar transitions. can be modified.
    /// @return reference of the transition added.
    virtual Transition& addNullaryBarTransition(state_t s,
                                                ParsingEnv& env) const;

    /// @param s target state of the build transition
    /// @param env parsing environment, containing a pool
    /// of multi-bar transitions. can be modified.
    /// @return reference of the transition added.
    virtual Transition& addBinaryBarTransition(state_t s,
                                               ParsingEnv& env) const;
    
    virtual void transitionsWTA(std::stack<const Transition*>& st,
                                ParsingEnv& env) const;

    virtual void transitionsBar(std::stack<const Transition*>& st,
                                ParsingEnv& env) const;

    
    /// @brief compatibility of this key with the given label symbol.
    /// @param a a label. must be inner.
    /// @param env parsing environment.
    /// @return whether the given symbol a can label
    ///         a transition targeted by this key.

    /// @todo redundant with innerWeight = zero ?
    virtual bool filterInnerLabel(const label_t& a,
                                  const ParsingEnv& env) const;

    /// @brief compatibility of this key with the given transition.
    /// @param tr a wta transition. must be inner.
    /// @param env parsing environment.
    /// @return whether the given transition tr can be generalized
    ///         to a transition targeted by this key.
    virtual bool filterInnerTransition(const Transition& tr,
                                       const ParsingEnv& env) const;
    
    // @brief number of input point to read for transition from the given
    // label symbol to this key.
    // @param a a label. must be terminal.
    // @param env parsing environment.
    // @return -1 if the given symbol a cannot label a transition targeted by
    // this key, or, otherwise, the number of points of the interval
    // to read (parse) when making the transition from symbol a to this Key.
    //virtual long pointsTerminalLabel(const label_t& a,
    //                                 const ParsingEnv& env) const;
    
    /// @brief compatibility of this key with the given label symbol.
    /// @param a a label. must be terminal.
    /// @param env parsing environment.
    /// @return whether the given symbol a can label
    ///         a transition targeting to this key.
    /// @todo redundant with terminalWeight = zero ?
    virtual bool filterTerminalLabel(const label_t& a,
                                     const ParsingEnv& env) const;
    
    /// @brief compatibility of this key with the given transition.
    /// @param tr a wta transition. must be terminal.
    /// @param env parsing environment.
    /// @return whether the given transition tr can be generalized
    ///         to a transition targeted by this key.
    virtual bool filterTerminalTransition(const Transition& tr,
                                          const ParsingEnv& env) const;
    
private:

    /// this key can be used as body of terminal transition with given label.
    /// @param label must be terminal (arity = 0) and insider the bar symbol.
    bool compatible(label_t label) const;

};


/// hash function for using as key in a table.
/// rank is ignored.
/// @see also ValueStateHasher.
struct KeySVHasher
{
    std::size_t operator()(const KeySV& k) const
    {
        using std::size_t;
        using std::hash;
        
        // see constant.h
        std::size_t h = HASH_SEED;
        h += hash<state_t>()(k.state());
        h = h * HASH_FACTOR + hash<bool>()(k._fs);
        h = h * HASH_FACTOR + hash<mtu_t>()(k._start);
        h = h * HASH_FACTOR + hash<ValueList>()(k.value());
        return h;
    }
};


} // end namespace Parsing


#endif /* KeySV_hpp */

/// @}


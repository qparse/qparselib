//
//  KeyS.cpp
//  squant
//
//  Created by Florent Jacquemard on 31/03/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include "KeyS.hpp"

namespace Parsing{


// rank default to 1 at creation
KeyS::KeyS(state_t s):
_state(s)
{ }


KeyS::KeyS(state_t s, const ParsingEnv& env):
_state(s)
{ }


// copy
KeyS::KeyS(const KeyS& k):
_state(k._state)
{ }


// firstChild
KeyS::KeyS(const KeyS& parent,
           const Transition& tr,
           const ParsingEnv& env)
{
    assert(tr.inner());
    assert(parent.inner(tr, env));
    _state = tr.state(0);
}


// nextSibling
// i = pos. of nextSibling
KeyS::KeyS(const KeyS& previous,
           size_t i,
           const KeyS& parent,
           const Transition& tr,
           const ParsingEnv& env)
{
    assert(previous.complete());
    assert(tr.inner());
    assert(1 <= i);
    assert(i < tr.size());
    assert(previous.state() == tr.state(i-1));
    _state = tr.state(i);
}


// instanciate = copy
KeyS::KeyS(const KeyS& head,
           label_t a,
           const KeyS& lastchild,
           const ParsingEnv& env)
{
    assert(head.partial()); // will fail for KeyS
    assert(lastchild.complete());
    assert(SymbLabel::inner(a));
    _state = head.state();
}


// complete leaf = copy in this case
KeyS::KeyS(const KeyS& head,
           label_t a,
           const ParsingEnv& env):
KeyS(head)
{
    assert(head.partial()); // will fail for KeyS
}


bool KeyS::inner(const Transition& tr,
                 const ParsingEnv& env) const
{
    return true;
}


size_t KeyS::leaf(const Transition& tr,
                  const ParsingEnv& env) const
{
    return 1;
}


void KeyS::leaves(const Transition& tr,
                  const ParsingEnv& env,
                  std::queue<KeyS>& q) const
{
    assert(this->partial()); // will fail
}


label_t KeyS::label(const Transition& tr,
                    const ParsingEnv& env) const
{
    return tr.label();
}


Weight KeyS::weight(label_t a,
                    const Transition& tr,
                    const ParsingEnv& env) const
{
    return tr.weight();
}


void KeyS::transitions(std::stack<const Transition*>& st,
                       const ParsingEnv& env) const
{
    assert(State::isWTA(_state));
    assert(env.wta);
    assert(env.wta->isRegistered(_state));
    
    for (Tlist<Transition>::const_iterator
         i = env.wta->cbegin(this->state());
         i != env.wta->cend(this->state()); i++)
    {
        const Transition* t = &(*i);
        st.push(t);
    }
}
    
    
//typename Tlist<Transition>::const_iterator
//KeyS::cbegin(const ParsingEnv& env) const
//{
//    assert(State::isWTA(_state));
//    assert(env.wta);
//    assert(env.wta->isRegistered(_state));
//    return (env.wta->cbegin(this->state()));
//}
//
//
//typename Tlist<Transition>::const_iterator
//KeyS::cend(const ParsingEnv& env) const
//{
//    assert(State::isWTA(_state));
//    assert(env.wta);
//    return (env.wta->cend(this->state()));
//}


KeyS& KeyS::operator= (const KeyS& k)
{
    if(this != &k)
    {
        _state = k._state;
    };
    return *this;
}


bool KeyS::operator==(const KeyS& k) const
{
    return (k._state == _state);
}

bool KeyS::operator!=(const KeyS& k) const
{
    return (! (*this == k));
}


// always complete
bool KeyS::instance(const KeyS& k) const
{
    return (k._state == _state);
}


bool KeyS::complete() const
{
    return true;    
}


//void KeyS::init_leafq(label_t a,
//                      const Transition& tr,
//                      const ParsingEnv& env)
//{
//    assert(f_leafq == false);
//    assert(leafq.empty());
//    // no instance
//    f_leafq = true;
//}


std::ostream& operator<<(std::ostream& o, const KeyS& k)
{
    o << KEY_LPAR << k._state << KEY_RPAR;
    return o;
}

} // end namespace Parsing

//
//  ParsingEnv.cpp
//  squant
//
//  Created by Florent Jacquemard on 06/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include "ParsingEnv.hpp"

namespace Parsing{


ParsingEnv::ParsingEnv(SWTA* a, InputSegment* s):
InputEnv(s),
wta(a),
pool((a == NULL)?NULL:(new SWTA(wta->weight_one()))), // empty
_dt0((a == NULL)?Transition():Transition(SymbLabel::make_dummy(), a->weight_zero())),
_dt1((a == NULL)?Transition():Transition(SymbLabel::make_dummy(), a->weight_one()))
{
    TRACE("Creating new Parsing Env");
    //init_pool();
}


ParsingEnv::ParsingEnv(const InputEnv& env, SWTA* a):
InputEnv(env), // copy
wta(a),
pool((a == NULL)?NULL:(new SWTA(wta->weight_one()))),
_dt0((a == NULL)?Transition():Transition(SymbLabel::make_dummy(), a->weight_zero())),
_dt1((a == NULL)?Transition():Transition(SymbLabel::make_dummy(), a->weight_one()))
{
    TRACE("Creating Parsing Env from Input Env");
    //init_pool();
}


ParsingEnv::ParsingEnv(const ParsingEnv& env):
InputEnv(env),
wta(env.wta),
pool(env.pool),
_dt0(env._dt0),
_dt1(env._dt1)
{ }


ParsingEnv::~ParsingEnv()
{
    if (pool)
    {
        TRACE("delete Parsing Env pool");
        delete pool;
//        pool = NULL;
    }
}


inline Weight ParsingEnv::weight_zero() const
{
    assert(wta);
    return wta->weight_zero();
}


inline Weight ParsingEnv::weight_one() const
{
    assert(wta);
    return wta->weight_one();
}


const Transition& ParsingEnv::dummyTransition_zero() const
{
    assert(wta);
    assert(! _dt0.weight().unknown());
    assert(_dt0.weight().zero());
    return _dt0;
}


const Transition& ParsingEnv::dummyTransition_one() const
{
    assert(wta);
    assert(! _dt1.weight().unknown());
    assert(_dt1.weight().one());
    return _dt1;
}




//void ParsingEnv::init_pool()
//{
//    if (wta != NULL)
//    {
//        if (pool == NULL)
//            pool = new SWTA(wta->weight_one());
//    }
//    else
//    {
//        pool = NULL;
//    }
//}


} // end namespace Parsing

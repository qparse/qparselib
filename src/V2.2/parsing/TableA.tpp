//
//  TableA.tpp
//  qparse
//
//  Created by Florent Jacquemard on 07/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

//#include "RunCompare.hpp"


namespace Parsing{


template <class K>
TableA<K>::TableA(ParsingEnv* env, RunCompare<K> comp):
_env(env),
_comparer(comp)
{
    assert(env);
};


template <class K>
TableA<K>::~TableA()
{
    assert(_env);    
}


} // end namespace Parsing

//
//  KeySIP.hpp
//  squanty
//
//  Created by Florent Jacquemard on 29/10/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{


#ifndef KeySIP_hpp
#define KeySIP_hpp

#include <stdio.h>
#include <assert.h>

#include "util.hpp"
#include "hash.hpp"
#include "MTU.hpp"
#include "KeySI.hpp"
#include "Distance.hpp"
#include "GaussianDistance.hpp"
#include "IntervalTree.hpp"
#include "IntervalHeap.hpp"


namespace Parsing{

/// type for pre post values in augmented states
typedef long pre_t;
static pre_t P_NULL = 0;
static pre_t P_UNKNOWN = -1;
static bool P_KNOWN(pre_t x) { return (x >= 0); }


/// extension of KeySI with pre and post values (int), made of
/// - one state of a base WTA
/// - one interval (inherited attribute)
/// - rank for k-best algo (not used for 1-best).
/// - pre value
/// - post value can be unknown (making the key partial)
class KeySIP : public KeySI  // Key<KeySI>
{
public:
    
    /// @brief top constructor.
    /// allocate an initial key
    /// which will serve to create the other keys.
    /// @param barlen first estimated bar duration in rtu.
    /// @param env input environment.
    /// @param f_open flag true iff the initial MTU interval is right-open.
    /// @param pre intial pre value (for chainig with other segment).
    /// must not be UNKNOWN.
    /// if false, the right bound is set to the duration of input segment.
    KeySIP(rtu_t barlen, const ParsingEnv& env, bool f_open=true,
           pre_t pre=0, pre_t post=P_UNKNOWN);
    

    /// @brief copy
    KeySIP(const KeySIP& k);
    
    
    /// @brief constructor of first child.
    /// construct a partial key to be instanciated as the first child of parent
    /// in the body of an augmented transition.
    /// @param parent head key of the augmented transition.
    ///        partial or complete.
    ///        inner must be true for parent key.
    /// @param tr WTA transition, augmented by the transition partially
    ///        constructed here. must be inner.
    /// @param env input environment.
    KeySIP(const KeySIP& parent,
           const Transition& tr,
           const ParsingEnv& env);
    
    
    /// @brief constructor for building next sibling.
    /// construct a partial key to be instanciated as sibling
    /// in the body of an augmented transition.
    /// @param previous previous sibling of this key in the body of the
    ///        augmented transition. must be complete.
    /// @param i position of the next sibling in body, in 1..arity-1
    ///        (0 is the position of the first child).
    /// @param parent head key of the augmented transition
    ///        partial or complete.
    /// @param tr SWTA transition, augmented by the transition partially
    ///        constructed here.
    /// @param env input environment.
    KeySIP(const KeySIP& previous,
           size_t i,
           const KeySIP& parent,
           const Transition& tr,
           const ParsingEnv& env);
    

    /// @brief constructor of complete instance.
    /// construct a complete upgrade of the head key, by copy,
    /// to be the head of an augmented transition.
    /// @param head a key to be instanciated. must be partial.
    /// @param a label of the augmented transition.
    /// @param lastchild last child in the body of the augmented transition.
    ///        must be complete.
    /// @param env input environment.
    /// @warning should not be called for this class (head must be partial).
    /// @see Key::instanciate
    KeySIP(const KeySIP& head,
           label_t a,
           const KeySIP& lastchild,
           const ParsingEnv& env);
    
    
    /// @brief constructor of complete instance for a leaf.
    /// @param head a key to instanciate as leaf. must be partial
    /// @param a label of the augmented transition. must be terminal.
    /// @param env parsing environment.
    /// @warning should not be called on this class (head must be partial)
    /// @warning leaf for head, with same args, must return 1.
    KeySIP(const KeySIP& head,
           label_t a,
           const ParsingEnv& env);
    
    
protected:
    
    /// @brief constructor for internal use
    KeySIP(state_t s, IntervalTree* node, rtu_t barlen, pre_t pre, pre_t post);

    
public:
        
    /// allocate and return a special Run representing a parse failure for this key.
    /// @param env parsing environment.
    /// @return a pointer to a newly allocated fail run, with
    /// - transition with dummy symbol, empty body, weight one in the
    ///   domain of the base wta in the passed environment.
    /// - fail symbol
    /// - weight one (in the domain of the base wta).
    /// This returned run will answer succesfully to Run.fail().
    Run<KeySIP>* failRun(const ParsingEnv& env) const;
    
    /// @brief completion of this partial leaf key to be the head
    ///        of a nullary (leaf) transition augmenting tr.
    /// Build and store in the given queue q
    /// all ther new complete instances of this key,
    /// using the parsing environment env.
    /// @param tr a SWTA transition, augmented by the transition partially
    /// constructed here. must be nullary (leaf, terminal).
    /// @param env parsing environment.
    /// @param q empty queue used to store the complete instances.
    /// @warning this Key must be partial.
    virtual void leaves(const Transition& tr,
                        const ParsingEnv& env,
                        std::queue<KeySIP>& q) const;
    
    /// @brief Key assignement.
    virtual KeySIP& operator= (const KeySIP& p);
    
    /// @brief for use as key in a unordered_multimap.
    virtual bool operator==(const KeySIP& p) const;
    virtual bool operator!=(const KeySIP& p) const;
    
    /// @brief return wether this Key is an instance of p.
    /// = equality in this case because KeyS are always complete.
    virtual bool instance(const KeySIP& p) const;

    
    /// @brief all attributes of the key are valued
    virtual bool complete() const;

    /// @brief accessor
    inline pre_t pre() const { return _pre; }

    /// @brief accessor
    inline pre_t post() const { return _post; }
    
    
    /// @brief printer
    friend std::ostream& operator<<(std::ostream& o, const KeySIP& p);

    
protected:
    pre_t _pre;
    pre_t _post;

    /// @brief accessor
    virtual size_t size() const;
    
    /// @brief weight for the case of a and tr terminal.
    virtual Weight terminalWeight(label_t a,
                                  const Transition& tr,
                                  const ParsingEnv& env) const;
       
    /// @brief number of points to read in the input segment
    /// in order to make a transition to this Key state.
    /// @todo RM : replaced by size()
    virtual inline size_t points() const { return _pre + _interval->lsize(); }

    // @brief number of input point to read for transition from the given
    // label symbol to this key.
    // @param a a label. must be terminal.
    // @param env parsing environment.
    // @return -1 if the given symbol a cannot label a transition targeted by
    // this key, or, otherwise, the number of points of the interval
    // to read (parse) when making the transition from symbol a to this Key.
    // virtual long pointsTerminalLabel(const label_t& a,
    //                                  const ParsingEnv& env) const;

    /// @brief compatibility of this key with the given label symbol.
    /// @param a a label. must be terminal.
    /// @param env parsing environment.
    /// @return whether the given symbol a can label
    ///         a transition targeted by this key.
    virtual bool filterTerminalLabel(const label_t& a,
                                     const ParsingEnv& env) const;

};


/// hash function for using as key in a table.
/// rank is ignoreds : same as SpointerHasher
struct KeySIPHasher
{
    std::size_t operator()(const KeySIP& k) const
    {
        using std::size_t;
        using std::hash;
        assert(k.interval());
        
        // see constant.h
        std::size_t h = HASH_SEED;
        h += hash<state_t>()(k.state());
        h = h * HASH_FACTOR + hash<rtu_t>()(k.interval()->rbegin());
        h = h * HASH_FACTOR + hash<rtu_t>()(k.interval()->rduration());
        h = h * HASH_FACTOR + hash<mtu_t>()(k.interval()->mbegin());
        h = h * HASH_FACTOR + hash<mtu_t>()(k.interval()->mduration());
        h = h * HASH_FACTOR + hash<pre_t>()(k.pre());
        h = h * HASH_FACTOR + hash<pre_t>()(k.post());
        return h;
    }
};

} // end namespace Parsing


#endif /* KeySIP_hpp */

/// @}

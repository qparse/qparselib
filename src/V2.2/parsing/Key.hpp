//
//  Key.hpp
//  squanty
//
//  Created by Florent Jacquemard on 04/06/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{


#ifndef Key_hpp
#define Key_hpp

#define KEY_LPAR '<'
#define KEY_RPAR '>'

#include <stdio.h>
#include <assert.h>
#include <queue>
#include <stack>


#include "trace.hpp"
#include "Weight.hpp"
#include "WeightDom.hpp"
#include "Tlist.hpp"
#include "Transition.hpp"
#include "ParsingEnv.hpp"

namespace Parsing{

/// @brief template for keys.
/// a key is an augmented state,
/// acting as index for best runs in a parse table.
///
/// It is made of:
/// - one state of a base SWTA,
/// - extended with attributes taking values in different domains,
/// and of two kinds:
///   - synthesized attributes are propagated bottom-up,
///   - inherited attributes are propagated top-down.
///
/// - A key is partial if only its inherited attributes are valued.
/// - A key is complete if its all inherited and synthesized attributes are
///   valued
///   (keys without synthesized attributes are always complete).
///
/// A augmented transition has the form
/// a(K1, ..., Kn) -> K0
/// where
/// - a is a label of arity n.
/// - (K1, ..., Kn) is a tuple of complete keys called body of the transition.
/// - K0 is a complete key called head of the transition.
/// K0, K1, ..., Kn are abject of the same concrete class K derived
/// from this class.
///
/// concrete transitions are built with the following functions:
/// - initial augmented states can be built with `top`.
/// - from a given a partial augmented state `K0` for the head
///   of an inner augmented transition,
///   one can built a partial first child by calling `firstChild` on `K0`.
/// - then every other child `Ki+1` can be built by calling `nextSibling`
///   on sibling `Ki`, for `i > 0`.
/// - from a given a partial augmented state `K0` for the head
///   of a terminal (leaf) augmented transition,
///   one can built possible complete instances by calling `leaf`.
/// - the (partial) states for siblings are instanciated
///   by repeating from step 3 down to leaves.
/// - once all the children augmented states are complete,
///   the head 'K0' can be instanciated by calling `instanciate` on `K0`
///   (with the last child complete state in argument).
///
/// Some synthesized attribute contain weight values.
/// The weight of a key is computed from the attributes and an input symbol.
template<class K>
class Key
{
public:
    
    ~Key();

    /// @brief wheter this key has a rank (for k-best computations)
    /// @todo TBR
    virtual bool ranked() const { return false; }
    
    /// @brief computation of first child key in augmented transition.
    /// allocate and return a partial key to be instanciated as the first child
    /// of this key in the body of a transition augmenting tr,
    /// and for an parsing environment env.
    /// i.e. this key is the head key of the augmented transition,
    /// it can be partial or complete.
    /// @param tr WTA transition, augmented by the transition partially
    ///        constructed here. must be inner.
    /// @param env parsing environment.
    inline K* firstChild(const Transition& tr,
                         const ParsingEnv& env) const;
    
    
    /// @brief computation of next sibling key in augmented transition.
    /// allocate and return a partial key to be instanciated as the
    /// next sibling of this key
    /// in the body of a transition augmenting tr,
    /// and for an parsing environment env.
    /// @param i position of the returned new sibling in body.
    ///        between 1 and arity. position of first sibling is 0.
    /// @param parent head key of the augmented transition
    ///        partial or complete.
    /// @param tr SWTA transition, augmented by the transition partially
    ///        constructed here.
    /// @param env parsing environment.
    /// @warning this key must be complete.
    inline K* nextSibling(size_t i,
                          const K& parent,
                          const Transition& tr,
                          const ParsingEnv& env) const;
    
    
    /// @brief completion of a partial inner key heading an augmented transition.
    /// allocate and return a complete upgrade of this key, by copy,
    /// to be the head of an augmented transition,
    /// using the parsing environment env.
    /// @param a label of the augmented transition. must be inner.
    /// @param lastchild last child in the body of the augmented transition.
    ///        must be complete.
    /// @param env parsing environment.
    /// @warning this Key must be partial.
    inline K* instanciate(label_t a,
                          const K& lastchild,
                          const ParsingEnv& env) const;

    
    /// @brief this key can be an inner node, target of tr, in a run.
    /// @param tr a SWTA transition, augmented by the transition partially
    ///        constructed here.
    /// @param env parsing environment.
    virtual bool inner(const Transition& tr,
                       const ParsingEnv& env) const = 0;
    
    
    /// @brief number of leaf instances that can be obtained
    ///        from this key and the given args.
    /// @param tr a SWTA transition, augmented by the transition partially
    /// constructed here. must be nullary (leaf, terminal).
    /// @param env parsing environment.
    /// @return - 0 if there no leaf instance compatible with tr.
    ///           if this key is complete, it means that it is not compatible with tr, a, and env.
    ///         - 1 if there is exactly one leaf instance of this key compatible with tr, a and env.
    ///           if this key is complete, it means that it is compatible with tr, a, and env.
    ///           if this key is partial, the leaf constructor can be use to built the compatible instance.
    ///           can be used instead of Key::leaf.
    ///         - 2 if there are 2 or more leaf instances compatible with tr.
    ///           it means that this key is partial, the compatible instances can be built with leaves.
    virtual size_t leaf(const Transition& tr,
                        const ParsingEnv& env) const = 0;

        
    /// @brief completion of this partial leaf key to be the head
    ///        of a nullary (leaf) transition augmenting tr.
    /// Build and store in the given queue q
    /// all ther new complete instances of this key,
    /// using the parsing environment env.
    /// @param tr a SWTA transition, augmented by the transition partially
    /// constructed here. must be nullary (leaf, terminal).
    /// @param env parsing environment.
    /// @param q empty queue used to store the complete instances.
    /// @warning this Key must be partial.
    virtual void leaves(const Transition& tr,
                        const ParsingEnv& env,
                        std::queue<K>& q) const;

        
    /// Best label of terminal transition augmenting tr targeted to this key.
    /// @param tr a SWTA transition, possibly unlabelled or partialy labelled
    /// @param env parsing environment.
    /// @return the label maximizing the weight function for this label, this transition, this env.
    /// @warning this key must be complete.
    virtual label_t label(const Transition& tr,
                          const ParsingEnv& env) const = 0;

        
    /// @brief The weight of an augmented transition headed by an instance
    ///        of this Key (augmented state) and augmenting tr.
    /// @param a label for the augmented transition.
    /// @param tr SWTA transition, augmented by the transition partially
    ///        constructed here.
    /// @param env parsing environment.
    /// @return the weight of a transition headed by an instance of this key
    ///         and augmenting tr.
    /// @return a returned weight of zero (absorbing in the semiring)
    ///         indicates incompatibility between this key and the transition.
    /// @warning if tr is nullary (leaf), this key must be complete.
    /// @warning if tr is not nullary (inner), this key can be partial or complete.
    /// @warning The weight of a run headed by the augmented transition
    ///          is the semiring product of this value with the product
    ///          of weights of the subruns.
    virtual Weight weight(label_t a,
                          const Transition& tr,
                          const ParsingEnv& env) const = 0;
    // default to assert(! tr.weight().unknown()); return tr.weight(); ?


    /// @brief push in given stack the WTA transitions to be generalized
    ///        in order to augmented transitions targeted by this key,
    ///        or an instance.
    /// @param s a stack to be filled with WTA transitions to be augmented.
    /// @param env parsing environment, containing a wta.
        virtual void transitions(std::stack<const Transition*>& s,
                                 const ParsingEnv& env) const = 0;

    
//    /// return an constant iterator to the first base transition in given environment
//    /// with head state the state component of this Key.
//    /// @param env parsing environment.
//    /// @warning the base SWTA and the pool SWTA must be non-NULL in env.
//    /// @todo TBR replaced by transitions()
//    virtual typename Tlist<Transition>::const_iterator
//    cbegin(const ParsingEnv& env) const = 0;
//    
//    
//    /// return an constant iterator to the past-the-end base transition in given environment
//    /// with head state the state component of this Key.
//    /// @param env parsing environment.
//    /// @warning the base SWTA and the pool SWTA must be non-NULL in env.
//    /// @todo TBR replaced by transitions()
//    virtual typename Tlist<Transition>::const_iterator
//    cend(const ParsingEnv& env) const = 0;

    
    /// Key assignement.
    virtual K& operator= (const K& p) = 0;
        
    /// for use as key in a unordered_multimap.
    virtual bool operator==(const K& p) const = 0;
    
    inline bool operator!=(const K& p) const
    { return (! (*this == p)); }
        
    /// return wether this Key is an instance of p.
    /// if p is complete, then it is equality.
    virtual bool instance(const K& p) const = 0;
        
    /// inverse of instance
    /// return wether p is an instance of this Key.
    /// if this Key is complete, then it is equality.
    bool subsume(const K& p) const
    { return p.instance(*this); }
    
    /// all attributes of the key are valued
    virtual bool complete() const = 0;
    
    /// one atribute at least of the key is not valued
    inline bool partial() const
    { return(! this->complete()); }

    /// allocate and return a special Run representing a parse failure for this key.
    /// @param env parsing environment.
    /// @return a pointer to a newly allocated fail run, with fail symbol.
    /// This returned run will answer succesfully to Run.fail().
    Run<K>* failRun(const ParsingEnv& env) const;
    
    /// @brief time interval represented by this key,
    /// or NULL if there is none.
    virtual IntervalTree* interval() const { return NULL; }
    
protected:

    /// @brief number of input events represented by this key.
    virtual size_t size() const { return 0; }
    
private:
    
    /// trick of Curiously Recurring Template Pattern
    /// to force a derived class K to inherit Key<K>
    friend K;
    
    Key();
    
};


} // end namespace Parsing


// separated definition of template class
#include "Key.tpp"


#endif /* Key_hpp */

/// @}

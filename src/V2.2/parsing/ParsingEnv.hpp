//
//  ParsingEnv.hpp
//  squant
//
//  Created by Florent Jacquemard on 06/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{

#ifndef ParsingEnv_hpp
#define ParsingEnv_hpp

#include <stdio.h>

#include "trace.hpp"
#include "InputEnv.hpp"
#include "Weight.hpp"
#include "Transition.hpp"
#include "SWTA.hpp"


namespace Parsing{

/// @brief a Parsing Environment extends the elements of
/// an Input Environment with two SWTAs:
/// - the base SWTA augmented for parsing
/// - an auxiliary SWTA, initialy empty,
///
class ParsingEnv : public InputEnv
{
public:
    
    /// base SWTA
    SWTA* wta;
    
    /// auxiliary SWTA = pool of extra transitions for over-the-bars processing
    /// will be empty if there is no wta
    SWTA* pool;

    /// @param seg an input segment. Can be NULL when not needed.
    /// @param wta base SWTA. Can be NULL when not needed.
    ParsingEnv(SWTA* wta = NULL, InputSegment* seg = NULL);
    
    /// copy
     ParsingEnv(const ParsingEnv& env);
    
    /// upgrade
     ParsingEnv(const InputEnv& env, SWTA* a = NULL);

    /// @brief delete the pool.
    /// @warning the base SWTA and segment must be delete externaly.
    virtual ~ParsingEnv();

    /// @brief return the 0 value in the weight domains
    /// shared by both SWTA of this environment.
    /// shortcuts to members of the base SWTA.
    /// @warning the base SWTA must be set and non-NULL in this environment.
    Weight weight_zero() const;
    
    /// @brief return the 1 value in the weight domains
    /// shared by both SWTA of this environment.
    /// shortcuts to members of the base SWTA.
    /// @warning the base SWTA must be set and non-NULL in this environment.
    Weight weight_one() const;

    /// Transition with dummy label, empty body
    /// and weight zero of the domain of the wta of this environment.
    /// @warning the base SWTA must be set and non-NULL in this environment.
    const Transition& dummyTransition_zero() const;

    /// Transition of dummy label, empty body
    /// and weight one of the domain of the wta of this environment.
    /// @warning the base SWTA must be set and non-NULL in this environment.
    const Transition& dummyTransition_one() const;
    
private:
    //void init_pool();
    
    /// dummy transition.
    /// with weight one of the domain of the wta of this environment.
    /// @warning the base SWTA must be set and non-NULL in this environment.
    const Transition _dt0;
    const Transition _dt1;

};

} // end namespace Parsing

#endif /* ParsingEnv_hpp */

/// @}

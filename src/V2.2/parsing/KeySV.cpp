//
//  KeySV.cpp
//  equiv2
//
//  Created by Florent Jacquemard on 22/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#include "KeySV.hpp"

namespace Parsing{


// internal use
//KeySV::KeySV(state_t s, const ValueList& seg, bool fs):
//KeyS(s),
//_list(seg), // copy
//_start(MTU::ZERO)
//_fs(fs)
//{ }


// top constructor
KeySV::KeySV(const ValueList& seg, const ParsingEnv& env):
KeyS(-1, env), // first bar
_list(seg),    // copy
_start(MTU::ZERO),
_fs(true)
{
    assert(env.iheap == NULL);
    assert(env.segment == NULL);
}

// copy
KeySV::KeySV(const KeySV& k):
KeyS(k),
_list(k._list),
_start(k._start),
_fs(k._fs)
{ }


// first child
KeySV::KeySV(const KeySV& parent,
             const Transition& tr,
             const ParsingEnv& env):
KeyS(parent, tr, env),
_list(), // will be replaced
_start(MTU::ZERO),
_fs(true)
{
    label_t a = tr.label();
    size_t ar = tr.arity();
    assert(ar > 0);
    if (SymbLabel::tuple(a)) //  || SymbLabel::rest(a))
    {
        mtu_t dur = ((parent._list.length() / ar) * tr.multiplicity(0));
        _list = ValueList(parent._list, MTU::ZERO, dur);
    }
    else if (SymbLabel::bar(a))
    {
        // rdur = _barlen, mdur=1
        assert(ar == 2);
        _list = ValueList(parent._list, MTU::ZERO, MTU::ONE);
    }
    else
    {
        ERROR("KeySV.FirstChild: unexpected symbol in tr: _state -> {}", tr);
    }
    /// @todo cases grace and fork
    assert(_start.null());
}


// next sibling
KeySV::KeySV(const KeySV& previous,
             size_t i,
             const KeySV& parent,
             const Transition& tr,
             const ParsingEnv& env):
KeyS(previous, i, parent, tr, env),
_list(), // will be replaced
_start(previous._start + previous._list.length()),
_fs(false)
{
    label_t a = tr.label();
    size_t ar = tr.arity();
    assert(ar > 0);
    size_t nbc = tr.size(); // number fo children
    assert(nbc <= ar);
    assert(0 < i); // not first child
    assert(i < nbc);
    assert((i > 1) || previous._fs); // if previous is first child
    // start of this sibling list included in parent
    assert(0 <= _start);
    assert(_start <= parent._list.length());

    if (SymbLabel::tuple(a)) //  || SymbLabel::rest(a))
    {
        mtu_t dur = ((parent._list.length() / ar) * tr.multiplicity(i));
        // end of this sibling list included in parent
        assert(0 <= _start + dur);
        assert(_start + dur <= parent._list.length());
        _list = ValueList(parent._list, _start, dur);
    }
    else if (SymbLabel::bar(a))
    {
        // rdur = _barlen, mdur=1
        assert(ar == 2);
        assert(i == 1); // second sibling
        // start = end of previous bar
        // dur = infinity
        _list = ValueList(parent._list, MTU::ONE);
    }
    else
    {
        ERROR("KeySI.NextSibling: unexpected symbol in tr: _state -> {}", tr);
    }
    /// @todo cases grace and fork
}


// up: instanciate = copy
KeySV::KeySV(const KeySV& head,
             label_t a,
             const KeySV& lastchild,
             const ParsingEnv& env):
KeyS(head, a, lastchild, env),
_list(head._list),
_start(head._start),
_fs(head._fs)
{ }


// leaf instanciate = copy
KeySV::KeySV(const KeySV& head,
           label_t a,
           const ParsingEnv& env):
KeyS(head, a, env),
_list(head._list),
_start(head._start),
_fs(head._fs)
{ }


KeySV::~KeySV()
{ }


KeySV& KeySV::operator= (const KeySV& k)
{
    if(this != &k)
    {
        KeyS::operator=(k); // _state = k._state;
        _list  = k._list;   // copy
        _start = k._start;
        _fs = k._fs;
    };
    return *this;
}


// for use as key in a unordered_multimap.
bool KeySV::operator==(const KeySV& k) const
{
    return (KeyS::operator==(k) &&
            (_list == k._list) &&
            (_start == k._start) &&
            (_fs == k._fs));
}


// for use as key in a unordered_multimap.
bool KeySV::operator!=(const KeySV& k) const
{
    return (! (*this == k));
}


size_t KeySV::size() const
{
    mtu_t c = _list.cont();
    return (_list.size() - ((c > 0)?1:0));
}


// std::shared_ptr<Run<KeySV>> KeySV::failRun_one(const ParsingEnv& env) const
Run<KeySV>* KeySV::failRun(const ParsingEnv& env) const
{
    assert(env.wta);
    const Transition& t1 = env.dummyTransition_one();
    assert(t1.weight().one());
    label_t a = SymbLabel::make_fail(this->size());
    return new Run<KeySV>(t1, a, t1.weight());
    // return std::shared_ptr<Run<KeySV>>(new Run<KeySV>(t1, a, t1.weight()));
}


// number of leaf instances (0, 1 or 2)
size_t KeySV::leaf(const Transition& tr,
                   const ParsingEnv& env) const
{
    if (filterTerminalTransition(tr, env))
        return 1;
    else
        return 0;
}


// add leaf instances in q
// should not be called
void KeySV::leaves(const Transition& tr,
                   const ParsingEnv& env,
                   std::queue<KeySV>& q) const
{
    assert(this->partial()); // will fail
}


label_t KeySV::label(const Transition& tr,
                     const ParsingEnv& env) const
{
    return tr.label();
    /// @todo if label has undefined info,
    /// get info (nb grace notes) from _interval
}


// this Key is always complete
bool KeySV::instance(const KeySV& k) const
{
    return (k == *this);
}

// all attributes of this key are valued
bool KeySV::complete() const
{
    return true;
}
   

Weight KeySV::weight(label_t a,
                     const Transition& tr,
                     const ParsingEnv& env) const
{
    if (tr.inner())
        return innerWeight(a, tr, env);
    else
        return terminalWeight(a, tr, env);
}


// weight for the case of a and tr inner.
Weight KeySV::innerWeight(label_t a,
                          const Transition& tr,
                          const ParsingEnv& env) const
{
    assert(tr.inner());
    assert(SymbLabel::inner(a));
    assert(this->inner(tr, env));
    assert(! tr.weight().unknown());

    // new bar
    if (SymbLabel::bar(tr.label()))
    {
        DEBUGU("KeySV: binary bar interval: {} for transition {}", _list, tr);
        assert(SymbLabel::arity(tr.label()) == 2);
        assert(tr.label() == a);
        return tr.weight();
    }
    // empty or terminal value list : cannot be divided (by inner transition)
    else if (_list.empty() ||
        _list.single_continuation() ||
        _list.single_event() ||
        _list.event())
    {
        return tr.weight().get_zero(); // exclude transition
    }
    // divisible value list
    else
    {
        return tr.weight();
    }
}


bool KeySV::compatible(label_t a) const
{
    assert(SymbLabel::inside(a));
    assert(SymbLabel::arity(a) == 0);
    // case of a continuation (tie or dot)
    if (SymbLabel::continuation(a) || SymbLabel::dot(a))
        return _list.single_continuation();
    // case of single note (no grace notes, no continuation)
    else if ((SymbLabel::note(a) || SymbLabel::rest(a)) &&
             (SymbLabel::nbGraceNotes(a) == 0))
        return _list.single_event();
    // case of 1 note and grace notes (no continuation)
    else if (SymbLabel::note(a) && (SymbLabel::nbGraceNotes(a) == _list.nbgn()))
        return _list.event();
    // shoul not happen
    else
    {
        ERROR("KeySV: compatible: unexpected case: {}",
              SymbLabel::to_string(a));
        return false;
    }
}


// weight for the case of a and tr terminal.
Weight KeySV::terminalWeight(label_t a,
                             const Transition& tr,
                             const ParsingEnv& env) const
{
    assert(tr.terminal());
    assert(SymbLabel::terminal(a));
    assert(this->complete());
    assert(this->leaf(tr, env) != 0);
    assert(! tr.weight().unknown());

    // terminal bar interval:
    // there are no points hence no need to compute the distance
    if (SymbLabel::bar(tr.label()))
    {
        DEBUGU("KeySV: terminal bar interval: {} for transition {}", _list, tr);
        assert(SymbLabel::arity(tr.label()) == 0);
        assert(tr.label() == a);
        assert(_list.empty());
        return tr.weight();
    }
    // combine transition weight with distance weight
    else if (compatible(a))
    {
        TRACE("KeySV: {} -> <{}> {} {} {}   compatible, add terminal tr.",
              _state, SymbLabel::to_string(a),
              (_list.single_continuation())?"(single cont)":"",
              (_list.single_event())?"(single event)":"",
              (_list.event())?"(event)":"");
        return tr.weight();
    }
    else
    {
        TRACE("KeySV: {} -> <{}> {} {} {} incompatible, ignore terminal tr.",
              _state, SymbLabel::to_string(a),
              (_list.single_continuation())?"(single cont)":"",
              (_list.single_event())?"(single event)":"",
              (_list.event())?"(event)":"");
        return tr.weight().get_zero(); // exclude transition
    }
}


// compatibility of this key with the given label symbol.
bool KeySV::filterInnerLabel(const label_t& a,
                             const ParsingEnv& env) const
{
    assert(SymbLabel::inner(a));

    if (SymbLabel::bar(a))
    {
        assert(SymbLabel::arity(a) == 2);
        return true;
    }
    else if (SymbLabel::tuple(a))
    {
        return (! _list.empty());
    }
    else if (SymbLabel::grace(a))
    {
        assert(SymbLabel::arity(a) == 2);
        WARN("KeySV: outside g.n. not supported, label={}",
             SymbLabel::to_string(a));
        return false;
    }
    else if (SymbLabel::fork(a))
    {
        assert(SymbLabel::arity(a) == 2);
        WARN("KeySV: voice fork not supported, label={}",
             SymbLabel::to_string(a));
        return false;
    }
    else
    {
        ERROR("KeySV: unexpected inner symbol {}", SymbLabel::to_string(a));
        return false;
    }

}


// compatibility of this key with the given transition.
bool KeySV::filterInnerTransition(const Transition& tr,
                                  const ParsingEnv& env) const
{
    assert(tr.inner());
    // TRACE("KeySV.filterTransition {}", tr);
    return filterInnerLabel(tr.label(), env);
}


// compatibility of this key with the given label symbol.
bool KeySV::filterTerminalLabel(const label_t& a,
                                const ParsingEnv& env) const
{
    assert(SymbLabel::terminal(a));

    // double bar (end of score)
    if (SymbLabel::bar(a))
    {
        return (_list.length().null() && _list.empty());
    }
    else if (SymbLabel::dummy(a))
    {
        WARN("KeySV: dummy symbol in tr.");
        return false; // -1 points
    }
    // continuation: tie or dot
    else if (SymbLabel::continuation(a) || SymbLabel::dot(a))
    {
        return _list.single_continuation();
    }
    // case of single event (no grace notes, no continuation)
    else if (SymbLabel::rest(a) ||
            (SymbLabel::note(a) && (SymbLabel::nbGraceNotes(a) == 0)))
    {
        return _list.single_event();
    }
    // case of 1 note and grace notes (no continuation)
    else if (SymbLabel::rest(a) || SymbLabel::note(a))
    {
        assert(! SymbLabel::continuation(a));
        return (_list.event() && (SymbLabel::nbGraceNotes(a) == _list.nbgn()));
    }
    else
    {
        ERROR("KeySV: unexpected terminal symbol {}", SymbLabel::to_string(a));
        return false;
    }
}


// compatibility of this key with the given transition.
bool KeySV::filterTerminalTransition(const Transition& tr,
                                     const ParsingEnv& env) const
{
    assert(tr.terminal());
    // TRACE("KeySV.filterTransition {}", tr);
    return filterTerminalLabel(tr.label(), env);
}


// add base wta transition targting this key in s
void KeySV::transitions(std::stack<const Transition*>& st,
                        ParsingEnv& env) const
{
    if (State::isWTA(_state))
        transitionsWTA(st, env);

    if (State::isMeta(_state))
        transitionsBar(st, env);
}


void KeySV::transitionsWTA(std::stack<const Transition*>& st,
                           ParsingEnv& env) const
{
    assert(State::isWTA(_state));
    assert(env.wta);
    assert(env.wta->isRegistered(_state));
        
    for (Tlist<Transition>::const_iterator
            i = env.wta->cbegin(this->state());
            i != env.wta->cend(this->state()); i++)
    {
        const Transition* t = &(*i);
        if ((t->terminal() && filterTerminalTransition(*t, env)) ||
            (t->inner() && filterInnerTransition(*t, env)))
            st.push(t);
    }
}


void KeySV::transitionsBar(std::stack<const Transition*>& st,
                           ParsingEnv& env) const
{
    assert(State::isMeta(_state));
    assert(env.wta);
    assert(env.pool);
    const Transition* t[2] = { NULL, NULL}; // 1 NT and 1 Term transition
    
    if (env.pool->isRegistered(_state))
    {
        // we assume that there are exactly 1 nullary and 1 binary
        // transitions targeting _state in env. pool.
        for (Tlist<Transition>::const_iterator
             i = env.pool->cbegin(_state);
             i != env.pool->cend(_state); i++)
        {
            if (i->arity() == 0)
            {
                assert(t[0] == NULL);
                t[0] = &(*i);
                TRACE("KeySV: found BAR0 transition in pool: {} <- {}",
                      _state, *i);
            }
            else if (i->arity() == 2)
            {
                assert(t[1] == NULL);
                t[1] = &(*i);
                TRACE("KeySV: found BAR2 transition in pool: {} <- {}",
                       _state, *i);
            }
            else
                ERROR("transition of unexpected arity {} in pool: {}",
                      i->arity(), (*i));
        }
    }
    else
    {
        t[0] = &(addNullaryBarTransition(_state, env));
        t[1] = &(addBinaryBarTransition(_state, env));
    }

    // the arity of the transition in the stack will determine
    // if we continue processing the rest of the segment or stop.
        
    // remaining interval is smaller than a bar and not inhabited
    // double bar mark (end of processing for bar sequence)
    if (_list.length() < MTU::ONE)
    {
        assert(_list.length().null());
        assert(_list.empty());
        assert(t[0]);
        st.push(t[0]);
    }
    // remaining interval is larger than a bar:
    // single bar mark (next bar)
    else
    {
        assert(t[1]);
        st.push(t[1]);
    }
}


Transition& KeySV::addNullaryBarTransition(state_t s,
                                           ParsingEnv& env) const
{
    assert(State::isMeta(s));
    assert(env.pool);
    const Weight tone = env.pool->weight_one();
    Tlist<Transition>& tl = env.pool->add(s, (_state == -1));
    tl.emplace(SymbLabel::make_bar(0), tone);
    assert(! tl.empty());
    Transition& t = tl.last();
    TRACE("KeySV: add BAR0 transition to pool: {} <- {}", _state, t);
    return t;

}


Transition& KeySV::addBinaryBarTransition(state_t s,
                                         ParsingEnv& env) const
{
    assert(State::isMeta(s));
    assert(env.pool);
    const Weight tone = env.pool->weight_one();
    Tlist<Transition>& tl = env.pool->add(s, (_state == -1));
    tl.emplace(SymbLabel::make_bar(2), tone);
    assert(! tl.empty());
    Transition& t = tl.last();
    assert(env.wta);
    t.push(env.wta->initial());
    t.push(_state - 1);
    TRACE("KeySV: add BAR2 transition to pool: {} <- {}", _state, t);
    return t;

}


// number of points to read in the input segment
/// @todo RM : replaced by size()
size_t KeySV::points() const
{
    return _list.size();
}


std::ostream& operator<<(std::ostream& o, const KeySV& k)
{
    o << KEY_LPAR;
    o << " " << k.state() << " ";
    o << k._list;    // short version
    o << KEY_RPAR;
    
    return o;
}


} // end namespace Parsing

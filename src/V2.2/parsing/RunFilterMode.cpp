//
//  RunFilterMode.cpp
//  squanty
//
//  Created by Florent Jacquemard on 13/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "RunFilterMode.hpp"


/// default values for optimisation flags

bool Parsing::OPT_RUN_UNIT = true;

bool Parsing::OPT_RUN_CONT = true;

bool Parsing::OPT_RUN_DUR = false;

bool Parsing::OPT_RUN_STRICT = false;

//
//  Quantizer.hpp
//  squanty
//
//  Created by Florent Jacquemard on 24/03/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{


#ifndef Quantizer_hpp
#define Quantizer_hpp

#include <stdio.h>

#include "trace.hpp"
#include "InputSegment.hpp"
#include "TableA.hpp"


namespace Parsing{


/// quantization:
/// methods to set musical time dates of points in an input segment
/// according to a parse table.
/// K = concrete key class (keys in table)
template<class K>
class Quantizer
{

public:

    /// a quantizer is attached to a unique table,
    /// that associated table cannot be changed.
    /// @param tab associated parse table for quantization.
    /// table content can be modified (for best computation).
    Quantizer(TableA<K>& tab);
 
    ~Quantizer();
    
            
    /// @brief set the musical time date and duration of events
    /// in given input segment,
    /// according to the n-best run for k in current table.
    /// @param seg an input segment. must not be NULL. will be modified.
    /// @param k a row in the parse table.
    /// @param n (as in n-best) is the rank of the run with target the key
    ///        of the record. must be larger or equal to 1 (default).
    /// @warning ptr type K must have interval.
    /// @warning all the musical date of events must be unknown in seg.
    /// @warning all the musical durations of events must be unknown in seg.
    /// @warning unfailing strategy: if there is a NULL subrun in the n-best
    /// run for k in tab (parse fail),
    /// the corresponding point in this input segment will keep unknown
    /// musical dates.
    /// @version 2
    void quantize(InputSegment& seg, const K& k, size_t n=1);
             
        
    // @brief quantization has been applied at least once.
    // inline bool quantized(InputSegment* seg) const
    // { return (seg->Interval::mduration() != MTU::UNBOUNDED); }
            

    
private:

    /// parse table used for quantization
    /// one quantizer is attached to a table.
    /// the table can be modified during quantization (computation of best).
    TableA<K>& _table;
    
    /// internal interval of the Key for the previous leaf met.
    /// @warning useful only for the K that use aligned intervals
    /// to align segment's points at closest bound, like KeySIP, KeySIR.
    IntervalTree* _previnterval;

    /// @brief set the musical time date and duration of events
    /// in given input segment, according to the run r of target state k.
    /// @param seg an input segment. must not be NULL. will be modified.
    /// @param k target state or run r
    /// @param r a run (body of transition rule of the augmented automaton)
    /// @param b starting point in this input segment.
    size_t quantize_run(InputSegment& seg,
                        const K& k,
                        const Parsing::Run<K>* r,
                        size_t b);


    /// subcase of quantize_run
    size_t quantize_run_inner(InputSegment& seg,
                              const K& k,
                              const Parsing::Run<K>* r,
                              size_t b);

    /// subcase of quantize_run
    size_t quantize_run_leaf(InputSegment& seg,
                             const K& k,
                             const Parsing::Run<K>* r,
                             size_t b);
    
};


// separated definition of template class
#include "Quantizer.tpp"

} // end namespace Parsing

#endif /* Quantizer_hpp */

/// @}



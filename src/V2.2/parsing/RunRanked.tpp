//
//  RunRanked.tpp
//  squant
//
//  Created by Florent Jacquemard on 07/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//



namespace Parsing{


//template<class K>
//RunRanked<K>::RunRanked(label_t lab, const Weight& w):
//Run<K>(lab, w),
//_ranks()
//{
//
//}

// /// if r is ranked, make a simple copy,
// /// otherwise, copy r and upgrade to RunRankedby setting all the ranks to 1.
//RunRanked<K>::RunRanked(const Run<K>& r):
//Run<K>(r),
//_ranks(r.size(), 1)
//{
//    assert(r.complete());
//    if (r.ranked())
//    {
//        RunRanked{K}& rr = dynamic_cast<RunRanked<K>&> r;
//        _ranks = std::vector<size_t>(r._ranks);
//    }
//    else
//    {
//        _ranks = std::vector<size_t>(r.size(), 1);
//    }
//}

template<class K>
RunRanked<K>::RunRanked(const RunRanked<K>& r):
Run<K>(r),
_ranks(r._ranks)
{
    assert(r.ranked());
    assert(r.complete());
    assert(r.size() == r._ranks.size());
}


template<class K>
RunRanked<K>::RunRanked(const Run<K>& r):
Run<K>(r),
_ranks(r.size(), 1)
{
    assert(! r.ranked());
    assert(r.complete());
    _ranks = std::vector<size_t>(r.size(), 1);
}


// copy and increment
/// @todo optimisation for recomputing weight in semirings with division?
template<class K>
RunRanked<K>::RunRanked(const RunRanked<K>& r, size_t i):
Run<K>(r),
_ranks(r._ranks)
{
    assert(r.ranked());
    assert(r.complete());
    assert(r.size() == r._ranks.size());
    assert(i < r.size());
    this->weightUnknown(); // set weight to unknown 
    ++_ranks[i];
    this->filterReset();   // filter in initial state
}


template<class K>
void RunRanked<K>::reset()
{
    assert(this->complete());
    this->weightReset();
    assert(! this->weight().unknown());
    if (this->weight().zero())
        WARN("RunR.reset: init weight=zero for {}", *this);
    
    this->filterReset();
    
    for (size_t i = 0; i < this->size(); i++)
    {
        const Run<K>* sub0 = subrun(i);
        const RunRanked<K>* sub =
            dynamic_cast<const RunRanked*>(sub0);
        // dynamic cast did not fail
        assert((sub0 == NULL) || (sub != NULL));
        
        if (sub == NULL)
        {
            this->weightUnknown(); // set weight to unknown
            return;
        }
        else
        {
            Weight wi = sub->weight();
            assert(! wi.unknown());
            if (wi.zero())
                WARN("RunR.reset: zero weight subrun {} of {}",
                     *sub, *this);
            this->weightUpdate(wi);
            this->filterUpdate(sub->_filter);
        }
    }
    assert(! this->weight().unknown());
    assert(this->_filter.complete());
}


template<class K>
const Run<K>* RunRanked<K>::subrun(size_t i) const
{
    assert(_ranks.size() == this->size()); // optional
    assert(i < _ranks.size());
    size_t n = _ranks[i];
    assert(n >= 1);

    return Run<K>::subrun(i, n);
}


} // end namespace Parsing

//
//  RunFiltery.cpp
//  squant
//
//  Created by Florent Jacquemard on 28/03/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @todo the recursive construction of DuraLists
/// is not suitable to deal with FORK symbols (voice split).
/// i.e. the durations values are meaningless in the case of nested voices
/// (concatenation of children & normalization is not possible
///  when there is a voice split)
/// but the detection of unit and cont is still accurate (inside a voice).

#include "RunFilter.hpp"

namespace Parsing{


RunFilter::RunFilter(label_t a, size_t size):
duration(),     // empty list
_label(a),
_size(size),
_state_unit(0), // default to trash state : ignore
_state_cont(0),
_done(0)
{
    _init_states();
    assert(_size <= SymbLabel::arity(_label));
}


RunFilter::RunFilter(const RunFilter& f):
duration(f.duration),     // copy list
_label(f._label),
_size(f._size),
_state_unit(f._state_unit),
_state_cont(f._state_cont),
_done(f._done)
{
    assert(_size <= SymbLabel::arity(_label));
}


RunFilter::~RunFilter()
{ }


RunFilter& RunFilter::operator=(const RunFilter& f)
{
    if(this != &f)
    {
        duration = f.duration;     // copy list
        _label = f._label;
        _size = f._size;
        _state_unit = f._state_unit;
        _state_cont = f._state_cont;
        _done = f._done;
    };
    return *this;
}


void RunFilter::_init_states()
{
    assert(_done == 0);
    
    if (OPT_RUN_UNIT && update_flag())
    {
        _init_state_unit();
    }
    
    // we must detect continuations for unit detection
    if ((OPT_RUN_CONT || OPT_RUN_UNIT) && update_flag())
    {
        _init_state_cont();
    }
    
    if ((OPT_RUN_CONT || OPT_RUN_UNIT || OPT_RUN_DUR) && update_flag())
    {
        duration = DurationList(_label);
    }
}


void RunFilter::_init_state_unit()
{
    label_t a = _label;

    // arity 0 (singleton run)
    if (SymbLabel::terminal(a))
    {
        assert(! SymbLabel::undef(a));
        //assert(! SymbLabel::bar(a));
        
        // continuation by tie
        if (SymbLabel::continuation(a))
        {
            assert(SymbLabel::nbEvents(a) == 0);
            _state_unit = 3; // not-unit, filtered
        }
        // continuation by dot
        else if (SymbLabel::dot(a))
        {
            WARN("RunFilter.initunit: unexpected dot symbol");
            _state_unit = 4; // error
        }
        // event (including rest)
        else if (SymbLabel::note(a) || SymbLabel::rest(a))
        {
            assert(SymbLabel::nbEvents(a) > 0);
            _state_unit = 2; // unit, filtered
        }
        // bar delimitor
        // should not happen update_flag = false
        else if (SymbLabel::bar(a))
        {
            ERROR("RunFilter.initunit: unexpected double bar symbol");
            _state_unit = 3;  // not-unit, filtered.
        }
        else if (SymbLabel::space(a))
        {
            TRACE("RunFilter.initunit: symbol SPACE");
            _state_unit = 2;  // unit, filtered.
        }
        else if (SymbLabel::fail(a))
        {
            TRACE("RunFilter.initunit: symbol FAIL({})", SymbLabel::nbEvents(a));
            _state_unit = 2;  // unit, filtered.
        }
        else if (SymbLabel::dummy(a))
        {
            ERROR("RunFilter.initunit: unexpected DUMMY symbol");
            _state_unit = 4;  // error
        }
        else
        {
            ERROR("RunFilter.initunit: unexpected Terminal symbol {}",
                  SymbLabel::to_string(a));
            _state_unit = 4;  // error
        }
    }
    // special grace constructor: treated as leaf with g.n.
    else if (SymbLabel::grace(a))
    {
        _state_unit = 2; // unit, filtered
    }
    // case of inner run (initialy empty)
    else if (SymbLabel::tuple(a))
    {
        assert(SymbLabel::arity(a) > 0);
        _state_unit = 0; // init
    }
    // case of voice split
    else if (SymbLabel::fork(a))
    {
        ERROR("RunFilter.initunit: unexpected fork symbol");
        _state_unit = 3;  // not-unit, filtered.
    }
    // bar delimitor
    else if (SymbLabel::bar(a))
    {
        ERROR("RunFilter.initunit: unexpected bar symbol");
        _state_unit = 3;  // not-unit, filtered.
    }
    // case of inner run (initialy empty), special binary symbol
    else
    {
        ERROR("RunFilter: unexpected NT symbol {}", SymbLabel::to_string(a));
        _state_unit = 3;  // not-unit, filtered.
    }
}


void RunFilter::_init_state_cont()
{
    label_t a = _label;
    
    // arity 0 (singleton run)
    if (SymbLabel::terminal(a))
    {
        assert(! SymbLabel::undef(a));
        //assert(! SymbLabel::bar(a));
        // continuation by tie
        if (SymbLabel::continuation(a))
        {
            assert(SymbLabel::nbEvents(a) == 0);
            _state_cont = 2; // cont, filtered
        }
        // continuation by dot
        else if (SymbLabel::dot(a))
        {
            WARN("RunFilter.initcont: unexpected dot symbol");
            _state_cont = 3; // not-cont, filtered.
        }
        // event (including rest)
        else if (SymbLabel::note(a) || SymbLabel::rest(a))
        {
            assert(SymbLabel::nbEvents(a) > 0);
            _state_cont = 3; // not-cont, filtered
        }
        // bar delimitor
        // should not happen update_flag = false
        else if (SymbLabel::bar(a))
        {
            ERROR("RunFilter.initcont: unexpected double bar symbol");
            _state_cont = 4; // error
        }
        else if (SymbLabel::space(a))
        {
            TRACE("RunFilter.initcont: symbol SPACE");
            _state_cont = 3;  // not-cont, filtered.
        }
        else if (SymbLabel::fail(a))
        {
            TRACE("RunFilter.initcont: symbol FAIL({})", SymbLabel::nbEvents(a));
            _state_cont = 3;  // not-cont, filtered.
        }
        else if (SymbLabel::dummy(a))
        {
            ERROR("RunFilter.initcont: unexpected DUMMY symbol");
            _state_cont = 4; // error
        }
        else
        {
            ERROR("RunFilter.initcont: unexpected Term symbol {}",
                  SymbLabel::to_string(a));
            _state_cont = 4; // error
        }
    }
    // special grace constructor: treated as leaf with g.n.
    else if (SymbLabel::grace(a))
    {
        _state_cont = 3; // not-cont, filtered. like event.
    }
    // case of inner run (initialy empty)
    else if (SymbLabel::tuple(a))
    {
        assert(SymbLabel::arity(a) > 0);
        _state_cont = 0; // init
    }
    // case of voice split
    else if (SymbLabel::fork(a))
    {
        ERROR("RunFilter.initcont: unexpected fork symbol");
        _state_cont = 3; // not-cont, filtered
    }
    // bar delimitor
    else if (SymbLabel::bar(a))
    {
        ERROR("RunFilter.initcont: unexpected bar symbol");
        _state_cont = 3; // not-cont, filtered
    }
    // case of inner run (initialy empty), special binary symbol
    else
    {
        ERROR("RunFilter: unexpected NT symbol {}", SymbLabel::to_string(a));
        _state_cont = 3; // not-cont, filtered
    }
}
    

void RunFilter::reset(label_t a, size_t s)
{
    duration = DurationList(a);
    _label = a;
    _size = s;
    _state_unit = 0;
    _state_cont = 0;
    _done = 0;
    _init_states();
}


size_t RunFilter::arity() const
{
    if (update_flag())
        return SymbLabel::arity(_label);
    else
        return 0;
}


size_t RunFilter::size() const
{
    if (update_flag())
        return _size;
    else
        return 0;
}


bool RunFilter::complete() const
{
    return (_done == size());
}


bool RunFilter::filter() const
{
    assert(this->complete());
    bool pass = true;
    
    if (OPT_RUN_UNIT && update_flag())
    {
        assert((_state_unit == 1) || (_state_unit == 2) || (_state_unit == 3));
        pass = pass && ((_state_unit == 2) || (_state_unit == 3));
    }

    if (OPT_RUN_CONT && update_flag())
    {
        assert((_state_cont == 1) || (_state_cont == 2) || (_state_cont == 3));
        pass = pass && ((_state_cont == 2) || (_state_cont == 3));
    }

    // OPT_RUN_DUR not used for filtering
    return pass;
}


bool RunFilter::unit() const
{
    assert(OPT_RUN_UNIT);
    
    if (update_flag())
    {
        assert(this->complete());
        assert((_state_unit == 1) || (_state_unit == 2) || (_state_unit == 3));
        return ((_state_unit == 1) || (_state_unit == 2));
    }
    else
    {
        WARN("RunFilter.unit test for unfiltered symbol {}",
              SymbLabel::to_string(_label));
        return false;
    }
}


bool RunFilter::continuation() const
{
    assert(OPT_RUN_UNIT || OPT_RUN_CONT);
    if (update_flag())
    {
        assert(this->complete());
        assert((_state_cont == 1) || (_state_cont == 2) || (_state_cont == 3));
        return ((_state_cont == 1) || (_state_cont == 2));
    }
    else
    {
        WARN("RunFilter.cont test for unfiltered symbol {}",
             SymbLabel::to_string(_label));
        return false;
    }
}


bool RunFilter::update_flag() const
{
    return (SymbLabel::inside(_label));
}


/// @todo extend update to FORK symbols (voice split)
void RunFilter::update(const RunFilter& fil) // arg. mult
{
    if (! update_flag())
        return;

    label_t a = _label;

    if (SymbLabel::terminal(a) || SymbLabel::grace(a) ||
        SymbLabel::fork(a) || SymbLabel::bar(a))
    {
        WARN("RunFilter.update: unexpected symbol {}",
             SymbLabel::to_string(a));
        return;
    }

    TRACE("Filter update {}({}/{})",
          SymbLabel::to_string(_label), _done, arity());

    assert(! this->complete());
    assert(fil.complete());
    // assert(fil.filter());
            
    if (OPT_RUN_UNIT && SymbLabel::tuple(a))
    {
        switch (_state_unit)
        {
            // first child
            case 0:
                if (fil.unit()) // state 1 or 2
                    _state_unit = 2;
                else
                    _state_unit = 3;
                break;

            // next childs
            case 1:
            case 2:
                if (fil.continuation())
                    _state_unit = 1;
                else
                    _state_unit = 3;
                break;

            // next childs
            case 3:
                _state_unit = 3; // trash
                break;

            default:
            {
                ERROR("filter update: unextected state {}", _state_unit);
                assert(false);
            }
        }
    }
    
    // we must detect continuations for unit detection
    if ((OPT_RUN_CONT || OPT_RUN_UNIT) && SymbLabel::tuple(a))
    {
        switch (_state_cont)
        {
            // first child
            case 0:
                if (fil.continuation())
                    _state_cont = 2;
                else
                    _state_cont = 3;
                break;

            case 1:
            case 2:
                if (fil.continuation())
                    _state_cont = 1;
                else
                    _state_cont = 3;
                break;
                
                // next childs
            case 3:
                _state_cont = 3;
                break;
                
            default:
            {
                ERROR("filter update: unextected state {}", _state_unit);
                assert(false);
            }
        }
    }
    
    if (OPT_RUN_DUR && SymbLabel::tuple(a))
    {
        duration += fil.duration; // duration with mult

        // filter of last subrun
        if (_done == size() - 1)
            duration.normalize();
    }

    _done++;  // _done += mult
}


} // end namespace Parsing

//
//  KeySI.cpp
//  squanty
//
//  Created by Florent Jacquemard on 04/06/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include "KeySI.hpp"

namespace Parsing{


// internal use
KeySI::KeySI(state_t s, IntervalTree* node, rtu_t len):
KeyS(s),
_barlen(len),
_interval(node)
{
    assert(node);
}


KeySI::KeySI(const KeySI& k):
KeyS(k),
_barlen(k._barlen),
_interval(k._interval)
{
    assert(k._interval);
}


// top constructor
KeySI::KeySI(rtu_t len, const ParsingEnv& env, bool f_open):
KeyS(-1, env), // first bar
_barlen(len)
{
    assert(env.iheap);
    assert(env.segment);

    if (f_open)
        _interval = env.iheap->make(env.segment, MTU::UNBOUNDED, true);
    else
        _interval = env.iheap->make(env.segment, env.segment->mend(), true);
    // _interval is aligned
    assert(_interval);
    assert(_interval->aligned());
}


// firstChild
KeySI::KeySI(const KeySI& parent,
             const Transition& tr,
             const ParsingEnv& env):
KeyS(parent, tr, env),
_barlen(parent._barlen)
{
    assert(env.segment);
    assert(env.iheap);
    assert(parent._interval);
    // assert(tr.inner());
    label_t a = tr.label();
    size_t ar = tr.arity();
    assert(ar > 0);
    // assert(parent.inner(tr, env));
    
    if (SymbLabel::tuple(a)) //  || SymbLabel::rest(a))
    {
        _interval = parent._interval->sub(env.segment, env.iheap, ar, 1);
        assert(_interval);
        assert(_interval->aligned());
    }
    else if (SymbLabel::bar(a))
    {
        // rdur = _barlen, mdur=1
        assert(ar == 2);
        _interval = parent._interval->split(env.segment, env.iheap,
                                            _barlen, MTU::ONE, 1);
        assert(_interval);
        assert(_interval->aligned());
    }
    else
    {
        ERROR("KeySI.FirstChild: unexpected symbol in tr: _state -> {}", tr);
    }
    /// @todo cases grace and fork
}


// nextSibling
KeySI::KeySI(const KeySI& previous,
             size_t i,
             const KeySI& parent,
             const Transition& tr,
             const ParsingEnv& env):
KeyS(previous, i, parent, tr, env),
_barlen(previous._barlen)
{
    //assert(previous.complete());
    assert(parent._interval);
    assert(parent.inner(tr, env));
    // assert(tr.inner());
    assert(tr.arity() == SymbLabel::arity(tr.label()));
    label_t a = tr.label();
    size_t ar = tr.arity();
    assert(ar > 0);
    assert(1 <= i);
    assert(i < ar);
    assert(env.segment);
    assert(env.iheap);
    if (SymbLabel::tuple(a))
    {
        _interval = parent._interval->sub(env.segment, env.iheap, ar, i+1);
        assert(_interval);
        assert(_interval->aligned());
    }
    else if (SymbLabel::bar(a))
    {
        assert(ar == 2);
        assert(i == 1); // second sibling
        // rdur = _barlen, mdur=1
        _interval = parent._interval->split(env.segment, env.iheap,
                                            _barlen, MTU::ONE, i+1);
        assert(_interval); // not aligned
    }
    else
    {
        ERROR("KeySI.NextSibling: unexpected symbol in tr: _state -> {}", tr);
    }
    /// @todo cases grace and fork
}


// instanciate = copy
KeySI::KeySI(const KeySI& head,
             label_t a,
             const KeySI& lastchild,
             const ParsingEnv& env):
KeyS(head, a, lastchild, env), // will fail for KeySI
_barlen(head._barlen),
_interval(head._interval)
{
    // assert(head.partial());
    // assert(lastchild.complete());
    // assert(SymbLabel::inner(a));
    assert(head._interval);
}


// complete leaf = copy
KeySI::KeySI(const KeySI& head,
             label_t a,
             const ParsingEnv& env):
KeyS(head, a, env), // will fail for KeySI
_barlen(head._barlen),
_interval(head._interval)   // no clone (share intervals)
{
    assert(SymbLabel::arity(a) == 0);
    // case of terminal bar symbol (double bar)
}


Run<KeySI>* KeySI::failRun(const ParsingEnv& env) const
{
    assert(env.wta);
    const Transition& t1 = env.dummyTransition_one();
    assert(t1.weight().one());
    if (! _interval->aligned())
    {
        assert(env.segment);
        _interval->align(env.segment);
    }
    label_t a = SymbLabel::make_fail(this->size());

    return new Run<KeySI>(t1, a, t1.weight());
}


size_t KeySI::leaf(const Transition& tr,
                   const ParsingEnv& env) const
{
    if (filterTerminalTransition(tr, env))
        return 1;
    else
        return 0;
}


size_t KeySI::size() const
{
    assert(_interval);
    assert(_interval->aligned());
    return _interval->size();
}


// leaves should not be called
void KeySI::leaves(const Transition& tr,
                   const ParsingEnv& env,
                   std::queue<KeySI>& q) const
{
    assert(this->partial()); // will fail
}


label_t KeySI::label(const Transition& tr,
                      const ParsingEnv& env) const
{
    return tr.label();
    /// @todo if label has undefined info,
    /// get info (nb grace notes) from _interval
}


Weight KeySI::weight(label_t a,
                     const Transition& tr,
                     const ParsingEnv& env) const
{

    if (tr.inner())
        return innerWeight(a, tr, env);
    else
        return terminalWeight(a, tr, env);
}


Weight KeySI::innerWeight(label_t a,
                          const Transition& tr,
                          const ParsingEnv& env) const
{
    assert(tr.inner());
    assert(SymbLabel::inner(a));
    assert(this->inner(tr, env));
    assert(! tr.weight().unknown());
    
    if (_interval->inhabited())
        return tr.weight();
    else
        return tr.weight().get_zero();
}


Weight KeySI::terminalWeight(label_t a,
                             const Transition& tr,
                             const ParsingEnv& env) const
{
    assert(tr.terminal());
    assert(SymbLabel::terminal(a));
    assert(this->complete());
    assert(this->leaf(tr, env) != 0);

    // tweight = transition weight = complexity
    Weight tweight = tr.weight(); // copy
    assert(! tweight.unknown());

    // terminal bar interval:
    // there are no points hence no need to compute the distance
    if (SymbLabel::bar(tr.label()) && (SymbLabel::arity(tr.label()) == 0))
    {
        TRACE("KeySI: terminal bar interval: {} ({}aligned) for transition {}",
              *(_interval),
              ((! _interval->aligned())?"not ":""),
              tr);
        assert(tr.label() == a);
        assert(! _interval->inhabited());
        assert(_interval->rduration() < _barlen);
        //assert(env.segment);
        //_interval->align(env.segment);
    }
    // combine transition weight with distance weight
    // CST_WEIGHT_TYPE == WeightDom::STOCHASTIC
    else if (tweight.hasType("ViterbiWeight"))
    {
        assert(env.segment);
        assert(_interval);
        tweight *= Weight(new GaussianDistance(env.segment, _interval));
    }

    // CST_WEIGHT_TYPE == WeightDom::PENALTY
    else if (tweight.hasType("TropicalWeight"))
    {
        assert(env.segment);
        assert(_interval);
        // compute distance to input segment
        Weight dist = Weight(new Distance(env.segment, _interval));
        TRACE("KeySI: tropical distance for inter={} tr={} dist={}",
              *(this), tr, dist);

        // new weight = (1 - alpha) . former transition weight * alpha . distance
        // update: alpha applied to dist only (not to complexity)
        tweight *= dist; // tropical weight product is a sum
    }
    else
    {
        ERROR("KeySI.weight: unxepected weight type");
    }
    
    return tweight;
}


void KeySI::transitions(std::stack<const Transition*>& st,
                        ParsingEnv& env) const
{
    if (State::isWTA(_state))
        transitionsWTA(st, env);

    if (State::isMeta(_state))
        transitionsBar(st, env);
}


void KeySI::transitionsWTA(std::stack<const Transition*>& st,
                           ParsingEnv& env) const
{
    assert(State::isWTA(_state));
    assert(env.wta);
    assert(env.wta->isRegistered(_state));
        
    for (Tlist<Transition>::const_iterator
            i = env.wta->cbegin(this->state());
            i != env.wta->cend(this->state()); i++)
    {
        const Transition* t = &(*i);
        if ((t->terminal() && filterTerminalTransition(*t, env)) ||
            (t->inner() && filterInnerTransition(*t, env)))
        {
            st.push(t);
        }
    }
}


void KeySI::transitionsBar(std::stack<const Transition*>& st,
                           ParsingEnv& env) const
{
    assert(State::isMeta(_state));
    assert(env.wta);
    assert(env.pool);
    const Transition* t[2] = { NULL, NULL};
    if (env.pool->isRegistered(_state))
    {
        // we assume that there are exactly 1 nullary and 1 binary
        // transitions targeting _state in env. pool.
        for (Tlist<Transition>::const_iterator
             i = env.pool->cbegin(_state);
             i != env.pool->cend(_state); i++)
        {
            if (i->arity() == 0)
            {
                assert(t[0] == NULL);
                t[0] = &(*i);
            }
            else if (i->arity() == 2)
            {
                assert(t[1] == NULL);
                t[1] = &(*i);
            }
            else
                ERROR("transition of unexpected arity {} in pool: {}",
                      i->arity(), (*i));
        }
    }
    else
    {
        t[0] = &(addNullaryBarTransition(_state, env));
        t[1] = &(addBinaryBarTransition(_state, env));
    }

    // the arity of the transition in the stack will determine
    // if we continue processing the rest of the segment or stop.
        
    // remaining interval is smaller than a bar and not inhabited
    // double bar mark (end of processing for bar sequence)
    if ((_interval->rduration() < _barlen) && (! _interval->inhabited()))
    {
        assert(t[0]);
        st.push(t[0]);
    }
    // remaining interval is larger than a bar:
    // single bar mark (next bar)
    else
    {
        assert(t[1]);
        st.push(t[1]);
    }
}


Transition& KeySI::addNullaryBarTransition(state_t s,
                                           ParsingEnv& env) const
{
    assert(State::isMeta(s));
    assert(env.pool);
    const Weight tone = env.pool->weight_one();
    Tlist<Transition>& tl = env.pool->add(s, (_state == -1));
    tl.emplace(SymbLabel::make_bar(0), tone);
    assert(! tl.empty());
    Transition& t = tl.last();
    TRACE("KeySI: add BAR0 transition to pool: {} -> {}", _state, t);
    return t;
}


Transition& KeySI::addBinaryBarTransition(state_t s,
                                          ParsingEnv& env) const
{
    assert(State::isMeta(s));
    assert(env.pool);
    const Weight tone = env.pool->weight_one();
    Tlist<Transition>& tl = env.pool->add(s, (_state == -1));
    tl.emplace(SymbLabel::make_bar(2), tone);
    assert(! tl.empty());
    Transition& t = tl.last();
    assert(env.wta);
    t.push(env.wta->initial());
    t.push(_state - 1);
    TRACE("KeySI: add BAR2 transition to pool: {} -> {}", _state, t);
    return t;
}


bool KeySI::filterInnerLabel(const label_t& a,
                             const ParsingEnv& env) const
{
    assert(SymbLabel::inner(a));

    if (SymbLabel::bar(a))
    {
        assert(SymbLabel::arity(a) == 2);
        return true;
    }
    else if (SymbLabel::tuple(a))
    {
        assert(_interval);
        return (_interval->inhabited());
    }
    else if (SymbLabel::grace(a))
    {
        assert(SymbLabel::arity(a) == 2);
        WARN("KeySI: outside g.n. not supported, label={}",
             SymbLabel::to_string(a));
        return false;
    }
    else if (SymbLabel::fork(a))
    {
        assert(SymbLabel::arity(a) == 2);
        WARN("KeySI: voice fork not supported, label={}",
             SymbLabel::to_string(a));
        return false;
    }
    else
    {
        ERROR("KeySI: unexpected inner symbol {}", SymbLabel::to_string(a));
        return false;
    }
}

bool KeySI::filterInnerTransition(const Transition& tr,
                                  const ParsingEnv& env) const
{
    assert(tr.inner());
    TRACE("KeySI.filterTransition {}", tr);
    return filterInnerLabel(tr.label(), env);
}


//bool KeySI::filterTerminalLabel(const label_t& a,
//                                const ParsingEnv& env) const
//{
//    return (pointsTerminalLabel(a, env) >= 0);
//}


bool KeySI::filterTerminalLabel(const label_t& a,
                                const ParsingEnv& env) const
{
    assert(SymbLabel::terminal(a));
    assert(_interval);
    if (! _interval->aligned())
    {
        WARN("KeySI: unaligned interval {} for filtering {}",
             *(_interval), SymbLabel::to_string(a));
        assert(env.segment);
        _interval->align(env.segment);
    }

    /// double bar (end of score)
    if (SymbLabel::bar(a))
    {
        return ((_interval->rduration() < _barlen) &&
                (! _interval->inhabited()));
        // 0 or -1 point to read in interval
    }
    else if (SymbLabel::dummy(a))
    {
        WARN("KeySI: dummy symbol in tr.");
        return false; // -1 points
    }
    else if (SymbLabel::continuation(a) || SymbLabel::dot(a))
    {
        return ((_interval->lsize() == 0) && (_interval->rsize() == 0));
        // 0 or -1 point to read in interval
    }
    /// one single offset point aligned to left bound of interval
    /// @todo add case of 1 rest preceeded by grace notes (here 0 grace notes)
    else if (SymbLabel::rest(a))
    {
        return ((_interval->lsize() == 1) &&
                (_interval->lsize() == _interval->loffsets()) &&
                (_interval->rsize() == 0));
        // 0 or -1 points to read
    }
    /// all points aligned to left bound of interval
    /// grace rests are discounted
    else if (SymbLabel::note(a))
    {
        return ((SymbLabel::nbEvents(a) == _interval->lonsets()) &&
                (_interval->rsize() == 0));
        // _interval->lsize() = gn + grace-rests + 1 notev or -1 points to read
    }
    else
    {
        ERROR("KeySI: unexpected terminal symbol {}", SymbLabel::to_string(a));
        return false;
    }
}


bool KeySI::filterTerminalTransition(const Transition& tr,
                                     const ParsingEnv& env) const
{
    assert(tr.terminal());
    TRACE("KeySI.filterTransition {}", tr);
    return filterTerminalLabel(tr.label(), env);
}


// if (DBL_APPROX_EQUAL(_interval->rend(), _interval->rbegin()))


KeySI& KeySI::operator= (const KeySI& p)
{
    if(this != &p)
    {
        KeyS::operator=(p); // _state = p._state;
        _interval  = p._interval;   // no clone
    };
    return *this;
}


bool KeySI::operator==(const KeySI& p) const
{
    return (KeyS::operator==(p) &&
            this->equal_interval(p));
//    return ((_state == p._state) &&
//            this->equal_interval(p));
}


bool KeySI::operator!=(const KeySI& k) const
{
    return (! (*this == k));
}
    

// always complete
bool KeySI::instance(const KeySI& p) const
{
    return (p == *this);
}


// always complete
bool KeySI::complete() const
{
    assert(_interval != NULL);
    return true;
}


bool KeySI::equal_interval(const KeySI& p) const
{
    if ((_interval == NULL) && (p._interval == NULL))
        return true;
    
    if ((_interval == NULL) || (p._interval == NULL))
    {
        return false;
    }
    else // both _interval non null
    {
        return ((_interval->rbegin() == p._interval->rbegin()) &&
                (_interval->rend()   == p._interval->rend())   &&
                (_interval->mbegin() == p._interval->mbegin())  &&
                (_interval->mend()   == p._interval->mend()));
    }
    //  ( *((Interval*) _interval) == *((Interval*) p._interval)) &&
}


std::ostream& operator<<(std::ostream& o, const KeySI& p)
{
    if (p._interval == NULL)
    {
        o << KEY_LPAR << p.state() << KEY_RPAR;
        return o;
    }
    
    o << KEY_LPAR;
    o << " " << p.state() << " ";
    
    // short version
    if (p._interval)
    {
        o << *(p._interval);
    }
    else
    {
        o << "[] ";
    }
    o << KEY_RPAR;
    
    return o;
}


} // end namespace Parsing

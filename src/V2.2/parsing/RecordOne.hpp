//
//  RecordOne.hpp
//  squanty
//
//  Created by Florent Jacquemard on 07/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{

#ifndef RecordOne_hpp
#define RecordOne_hpp

#include <stdio.h>
#include <assert.h>

#include "trace.hpp"
#include "Recordey.hpp"

namespace Parsing{

//template<class K, class R, class H> class TableParse;

/// @brief record associated to Key for one-best algorithms.
template<class K>
class RecordOne : public Record<K>
{
public:
    
    /// @brief empty record associated to k.
    /// @param comp function for comparing Runs over K.
    /// @param failrun fixed run returned by best when there is no best run.
    /// @warning the key associated to the record must
    ///          be set later to a non-NULL ptr with setKey.
    RecordOne(RunCompare<K> comp, const Run<K>* failrun);
    
    /// deleting a RecordOne frees the best run contained.
    virtual ~RecordOne();
    
    /// @brief add a run to the record or delete.
    /// @param r candidate run to add.
    /// @warning the key of this record must be set.
    /// @warning if r is not added, it is deleted.
    virtual size_t add(const Run<K>* r);
    
    /// @brief returns the 1-th best run of the record.
    /// @param n rank, must be 1 for RecordOne.
    /// @return the n-th best run of the record or, if there is none,
    /// the failRun set or NULL when the failRun was not set with setFailRun.
    virtual const Run<K>* best(size_t n=1);
    //virtual Run<K>* best(TableParse<K,Record<K,H>,H>* table, size_t n=1);
    
    virtual bool empty() const;
    
protected:
    
    /// best run for the associated state.
    const Run<K>* _best;
    
};


} // end namespace Parsing


// separated definition of template class
#include "RecordOne.tpp"


#endif /* RecordOne_hpp */

/// @}






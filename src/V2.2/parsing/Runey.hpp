//
//  Runey.hpp
//  squant
//
//  Created by Florent Jacquemard on 26/03/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
// runs where children (pointer to subruns)
// are pointers to record
// (every record empbed the associated complete key in parse table).
//
// advantage: efficiency: no needed lookup for the key in parse table
// to get the associated record hence best run.
//
/// @addtogroup parsing
/// @{

#ifndef RuneyTemplate_hpp
#define RuneyTemplate_hpp

#include <stdio.h>
#include <assert.h>
#include <vector>

#include "trace.hpp"
#include "SymbLabel.hpp" // symbolic labels
#include "Weight.hpp"
#include "Transition.hpp" 
//#include "Recordey.hpp"
#include "RunFilter.hpp"


namespace Parsing{

template<class K> class Record;
//const Run<K>* Record<K>::best(size_t);

/// a run is represented as a concrete transition, with
/// - a label
/// - an arity value (number of children expected)
/// - a list of children.
///   every child is represented by a complete key
///   (pointer to a row in parse table)
/// - a weight (updatable)
/// - a list of relative durations
///
/// Runs are compact representations of parse trees,
/// as a tuple of pointers to subruns.
///
/// A run is complete when the length of the list of children is the arity.
/// It is partial otherwise.
/// param K = class of Key in parse table
template<class K>
class Run
{
public:
    
    // null run.
    // unknown weight, empty children list.
    // Run(size_t a = 0);
        
    /// run with empty children list initialized with a transition of the base wta.
    /// @param tr origin transition used to build the run.
    /// It gives label and initial weight.
    ///  cannot be changed afterwards.
    Run(const Transition& tr);
    
    /// run with empty children list, initialized  a transition of the base wta
    /// and some label and weight values which may differ from this transition.
    /// @param lab given label. cannot be changed afterwards.
    /// @param w weight must not be unknown. can be updated afterwards.
    /// The arity of the run is the arity of given transition.
    /// run is complete if arity == 0 (terminal run), it is partial otherwise.
    Run(const Transition& tr, label_t lab, const Weight& w);
    
    // run with empty children list - for backward compatibility.
    // it is complete if a == 0 (terminal run), it is partial otherwise.
    // @param a positive arity value. must be the arity of lab.
    // @param lab given label.  cannot be changed afterwards. arity of label must be a.
    // @param w weight must not be unknown. can be updated afterwards.
    // @@todo TBR. for backward compatibility.
    // Run(size_t a, label_t lab, const Weight& w);
     
    
    /// copy construtor.
    Run(const Run<K>& r);
       
    /// @brief copy/update constructor with push back of a child (record).
    /// @param r run must be partial.
    /// @param rec record, must have a complete key.
    /// copy r, and in the copy:
    /// - add rec at the end of children list.
    /// - multiply current weight by the weight of the 1-best run for rec.
    /// - update current filter with the filter of the 1-best run for rec.
    /// @warning if rec has no 1-best run, the weight of the copy
    /// is set to unknown.
    Run(const Run<K>& r, Record<K>* rec);
        
    /// @brief copy/update constructor with push back of a new best subrun.
    /// @param r run must be a partial.
    /// @param rec must be a complete key.
    /// @param best is a pointer to the best run in rec. must be non-NULL.
    /// the pointed run must be complete.
    /// copy r, and in the copy:
    /// - add rec at the end of children list.
    /// - multiply current weight by the weight of best.
    /// - update current filter with the filter of best.
    Run(const Run<K>& r, Record<K>* rec, const Run<K>* best);
    
    // copy/replace constructor
    // @param r must be a complete run.
    // @param i is the index of a children.
    // must be between 0 (included) and the arity (excluded).
    // @param k must be a complete key.
    // @param best is the best run for k. run must be complete.
    // copy r and in the copy
    // - replace the key number i by k.
    // - reset the weight of this new run to w.
    // - reset the duration list to dl.
    // @todo useful?
//    Run(const Run<K,H>& r,
//        size_t i,
//        const K& k,
//        const Run<K,H>& best);
    
    /// deleting a run does not free the records
    /// pointed in the list of children (this list contains only pointers).
    ~Run();
    
    Run<K>& operator= (const Run<K>& rhs);
    
    bool operator==(const Run<K>& rhs) const;
    
    // @return number of children expected for this run.
    /// @return out degree expected for this run.
    /// @warning it may not be the current number of childen
    /// when this run is not complete or when children have multiple edges.
    inline size_t arity() const { return SymbLabel::arity(_label); }

    /// @return current number of children for this run.
    /// it is the arity - multiplicity if this run is complete.
    /// it is < arity - multiplicity if this run is partial.
    inline size_t size() const { return _children.size(); }
    
    /// @return the multiplicity of the ith children.
    /// = number of edges between the top of this run into its ith subrun.
    inline size_t multiplicity(size_t i) const { return _origin.multiplicity(i); }
    
    /// @return label of this run.
    inline label_t label() const { return _label; }
    
    /// current weight.
    inline const Weight& weight() const { return _weight; }
    
    /// Transition of the base WTA used to build this run.
    inline const Transition& origin() const { return _origin; }
    
    /// list of relative durations.
    inline const DurationList duration() const
    { return _filter.duration; }

    /// this run is terminal (leaf).
    inline bool terminal() const { return (arity() == 0); }
    
    /// this run is inner.
    inline bool inner() const { return (arity() > 0); }
    
    /// a run is complete when the length of the list of children is the arity.
    bool complete() const;
    
    /// not complete
    inline bool partial() const { return (! complete()); }

    /// this run passes successfully the filter.
    bool filter() const;
    
    virtual bool ranked() const { return false; }
    
    /// this run is a fail run.
    bool fail() const;

    /// number to input event for this fail run.
    /// Warning this run must be a fail run.
    size_t skip() const;

    /// @param i the index of the subrun, between 0 and arity()-1.
    /// @return the ith subrun or NULL if there is none.
    /// @warning this run must be complete.
    virtual const Run<K>* subrun(size_t i) const;
        
    /// return the ith key of this run.
    /// @param i index of subrun, must be between 0 and run size - 1.
    inline const K* key(size_t i) const;

private:
    
    /// @param best subrun to add can be NULL
    void init_best(const Run<K>& r, Record<K>* rec, const Run<K>* best);
    
protected:
    /// fixed symbol.
    label_t _label;
    /// @todo obtain from _origin ?

    /// pointers (keys) to children sub-runs.
    typename std::vector<Record<K>*> _children;
    
    /// current weight.
    Weight _weight;
    
    /// filter for optimization
    RunFilter _filter;
    
    /// initial weight at construction of run.
    /// needed when recomputing the run's weight.
    Weight _initweight;
    
    /// [opt] data for optimization.
    /// backup of the best runs used for construction.
    /// @todo TBR. not used.
    typename std::vector<const Run<K>*> _subruns;
    
    /// Transition of the base WTA used to build this run.
    const Transition& _origin;
    // gives _label, _initweight -> protected functions

    
    /// make a copy and in copy, reset the weight to w and reset the filter.
    Run(const Run<K>& r, Weight w);
        
    /// @param i the index of the subrun, between 0 and arity-1.
    /// @param n the rank for n-best.
    ///        must be equal to 1 for unranked runs.
    ///        must be larger or equal to 1 for ranked runs.
    /// @return the ith subrun or NULL if there is none.
    /// @warning this run must be complete.
    virtual const Run<K>* subrun(size_t i, size_t n) const;
    
    
    // return the ith record of this run.
    // @param i index of subrun, between 0 and arity-1.
    // Record<K>* operator[](size_t i) const;
    
    
    // inline const RunFilter& filter() const { return _filter; }
    
    /// @brief reset the filter of this run to its initial state.
    /// utility for internal use
    inline void filterReset()
    { _filter.reset(_label, _origin.size()); }

    /// @brief update state of this run's filter.
    /// @param f filter of sub-run.
    /// @warning this run must be inner.
    /// utility for internal use
    inline void filterUpdate(const RunFilter& f)
    { _filter.update(f); }

    /// @brief reset the weight of this runn to its initial weight.
    /// utility for internal use
    inline void weightReset()
    { _weight = _initweight; }
    
    /// @brief reset the weight of this run to an unknown weight.
    /// utility for internal use
     inline void weightUnknown()
    { _weight = Weight(); }

    /// @brief update the weight of this run
    /// by multiplying it with the given weight.
    /// @param w weight of a subrun. must not be unknown.
    /// @warning the weight of this run must not be unknown.
    /// utility for internal use
    inline void weightUpdate(const Weight& w)
    { assert(! _weight.unknown()); assert(! w.unknown()); _weight *= w; }

    
public:

    friend std::ostream& operator<<(std::ostream& o, const Run<K>& r)
    {
        SymbLabel::print(r._label, o);

        if (r._children.empty())
        {
            o << "()";
        }
        else
        {
            o << "(";
            typename std::vector<Record<K>*>::const_iterator
                i  = r._children.cbegin();
            assert(i != r._children.cend());
            o << *((*i)->key());
            ++i;
            for(; i != r._children.cend(); ++i)
            {
                o << " " << *((*i)->key());
            }
            o << ")";
        }
        o << " : " << r._weight;
        return o;
    }
};

} // end namespace Parsing


// to solve circular dependencies
#include "Recordey.hpp"

// separated definition of template class
#include "Runey.tpp"


//#include "RunCompare.hpp"


#endif /* RuneyTemplate_hpp */


/// @}

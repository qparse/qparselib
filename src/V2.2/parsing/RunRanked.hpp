//
//  RunRanked.hpp
//  squant
//
//  Created by Florent Jacquemard on 07/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{

#ifndef RunRanked_hpp
#define RunRanked_hpp

#include <stdio.h>
#include <assert.h>
#include <vector>


#include "trace.hpp"
#include "Runey.hpp"

namespace Parsing{

//template<class K> class Run;

/// extension of class Run with a rank value for each children.
/// for k-best computation with the algo of Huang & Chiang 2005
template<class K>
class RunRanked : public Run<K>
{

public:

    // @brief run with empty children list.
    // arity a of run is the arity of given label.
    // run is complete if a == 0 (terminal run), it is partial otherwise.
    // @param lab given label.  cannot be changed afterwards.
    // @param w weight must not be unknown. can be updated afterwards.
    //RunRanked(label_t lab, const Weight& w);


    /// @brief copy.
    /// @warning. r must be complete.
    RunRanked(const RunRanked<K>& r);

    
    /// @brief copy and upgrade.
    /// @param r a run. must be unranked (base class).
    /// @warning. r must be complete.
    RunRanked(const Run<K>& r);

    
    /// @brief copy and increment.
    /// @param r ranked run to copy.
    /// @param i index of the children whose rank will be upgraded in the copy.
    /// @warning the weight of the copy is deleted and set as unknown.
    /// @warning the filter of the copy is reset.
    RunRanked(const RunRanked<K>& r, size_t i);

    
    /// @brief recompute the weight and filter of this run.
    /// - set the weight of this run to its initial weight,
    ///   and filter to its initial state.
    /// - multiply by the the weight of each subrun,
    ///   and update filter for each subrun.
    /// If one of the subrun is NULL, the weight is set to unknown
    /// (filter is left uncomplete in this case).
    /// @warning the weight of every (non-NULL) subrun must not be unknown.
    /// @warning this run must be complete.
    void reset();
    
    
    virtual bool ranked() const { return true; }

    
    /// @param i the index of the subrun, between 0 and arity()-1.
    /// @return the ith subrun or NULL if there is none.
    /// @warning this run must be complete.
    virtual const Run<K>* subrun(size_t i) const;

    
protected:
    
    /// ranks of children. mutable.
    typename std::vector<size_t> _ranks;
    
    
public:
    
    friend std::ostream& operator<<(std::ostream& o, const RunRanked<K>& r)
    {
        SymbLabel::print(r._label, o);
        if (r._children.empty())
        {
            o << "()";
        }
        else
        {
            assert(r.size() == r._ranks.size());
            o << "(";
            typename std::vector<Record<K>*>::const_iterator
                i  = r._children.cbegin();
            assert(i != r._children.cend());
            o << *((*i)->key()) << "." << r._ranks[0];
            ++i;
            for(size_t j = 1; i != r._children.cend(); ++i)
            {
                o  << " " << *((*i)->key()) << "." << r._ranks[j] ;
                ++j;
            }
            o << ") ";
        }
        o << " w=" << r._weight;
        return o;
    }
    
};


} // end namespace Parsing


// separated definition of template class
#include "RunRanked.tpp"

            
#endif /* RunRanked_hpp */

/// @}

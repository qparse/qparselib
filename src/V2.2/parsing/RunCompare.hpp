//
//  RunCompare.hpp
//  squanty
//
//  Created by Florent Jacquemard on 18/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{


#ifndef RunCompare_hpp
#define RunCompare_hpp

#include <stdio.h>
#include <assert.h>

#include "trace.hpp"
#include "Weight.hpp"
//#include "Runey.hpp"

namespace Parsing
{

/// orderings for ordered containers
/// used in k-best procedures.
template<class K> using RunCompare =
std::function<bool(const Run<K>*, const Run<K>*)>;


/// one ordering for k-best to rank Runs
/// from best weight to worse weight,
/// where partial run or run with unknown weight
/// is considered to be the best
/// (highest priority for unevaluated candidates)
template<class K>
RunCompare<K> weightMax =
[](const Run<K>* lhs, const Run<K>* rhs)
{
    assert (lhs);
    assert (rhs);
    assert (lhs->complete());
    assert (rhs->complete());
    return (lhs->weight() < rhs->weight());
//    if (rhs->partial() || rhs->weight().unknown())
//        return false;
//    else if (lhs->partial() || lhs->weight().unknown())
//        return true;
//    else
//        return (lhs->weight() > rhs->weight());
};


/// one ordering for k-best to rank Runs
/// from worse weight to best weight,
/// where partial runs and runs with unknown weight
/// is considered to be the worst.
/// (highest priority for unevaluated candidates)
template<class K>
RunCompare<K> weightMin =
[](const Run<K>* lhs, const Run<K>* rhs)
{
    assert (lhs);
    assert (rhs);
    assert (lhs->complete());
    assert (rhs->complete());
    return (lhs->weight() > rhs->weight());
//    if (lhs->partial() || lhs->weight().unknown())
//        return false;
//    // if rhs == 0 then lhs == rhs
//    // if rhs > 0 then lhs > rhs;
//    else if (rhs->partial() || rhs->weight().unknown())
//        return true;
//    else
//        return (lhs->weight() < rhs->weight());
};

} // end namespace

#endif /* RunCompare_hpp */

/// @}

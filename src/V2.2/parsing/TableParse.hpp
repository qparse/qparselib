//
//  TableParse.hpp
//  qparse
//
//  Created by Florent Jacquemard on 08/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{


#ifndef TableParse_hpp
#define TableParse_hpp

#include <stdio.h>
#include <assert.h>
#include <utility>       // std::pair
#include <unordered_map> // and unordered_multimap
#include <queue>

#include "trace.hpp"
#include "State.hpp"
#include "Transition.hpp"
#include "Runey.hpp"
#include "Recordey.hpp"
#include "RecordQueue.hpp"
#include "TableInstances.hpp"
#include "ParsingEnv.hpp"
#include "TableA.hpp"

namespace Parsing{

template<class K, class R, class H> using MapRecord =
    std::unordered_map<K,R,H>;

template<class K, class R, class H> using MapIterator =
    typename MapRecord<K,R,H>::iterator;

template<class K, class R, class H> using Entry =
    std::pair<const K*, R*>;


/// parse table.
/// mapping associating to a complete Key one Record
/// containing bests runs targetting this Key.
///
/// form of the table:
/// @code map key -> value @endcode where
/// - key of type K = Key
/// - value of type R = R* (stores some Run<K>*)
/// - H = Hasher for K
/// - equal+to is op. == defined in K
///
/// The parse table also embeds an Instance Table for convenience.
///
/// Parameters:
/// - K = concrete key class (index rows in the table)
/// - R = record class (values in table)
/// - H = PointerHasher (for keys)
template<class K, class R, class H>
class TableParse : public TableA<K>
{
public:
    
    /// concrete table.
    /// @param env the parsing environment must not be NULL
    /// @param comp binary function for comparing runs
    TableParse(ParsingEnv* env, RunCompare<K> comp);
    
    /// delete all the complete keys and associated records
    /// stored in the table.
    /// @warning they must not be freed elsewhere.
    virtual ~TableParse();

    bool empty() const;

    
    /// @brief n-th best run targeting the key (augmented state) k.
    /// @param k key can be partial or complete.
    /// @param n (as in n-best) is the rank of the run with target the key
    ///        of the record. must be larger or equal to 1 (default).
    /// @return if k is complete: the n-th best run targeting k or NULL if there is none.
    ///         if k is  partial: the n-th best run targeting one complete instance of k
    ///         reachable by the augmented automaton, or a failRun if it does not exist.
    /// @warning the table can be modified (lazy addition of needed entries)
    virtual const Run<K>* best(const K& k, size_t n=1);
       
    virtual size_t nb_entries() const { return _nb_rows; }
    virtual size_t nb_runs() const { return _nb_runs; }
   
    
private: // data
    
//    const ParsingEnv* _env;
//
//    RunCompare<K> _comparer;
    
    MapRecord<K,R,H> _table;
    
    /// associate to every registered partial key k
    /// an iterator to pairs (k', r)
    /// where k' is a complete instance of k assciated to r in _table
    TableInstances<K,R,H>* _instances;

    /// pool of best runs for instance of a partial k.
    /// for best partial
    RecordQueue<K>* _pool;
    
    /// stats.
    size_t _nb_rows;
    
    /// stats.
    size_t _nb_runs;
    
    
private: // table access and construction

    /// @brief find or create an entry in the table.
    /// @param k complete key, key of the entry.
    /// @param newrec flag set to true iff the returned record
    ///        has been created and added to table in this call.
    /// @return a iterator to the entry in table
    ///         associated with key k.
    MapIterator<K,R,H> addemplace(const K& k,
                                  bool& newrec);
    
    
    /// @brief find or create an entry in the table.
    /// @param head arg. of constructor of complete instance. must be partial.
    /// @param a arg. of constructor of complete instance.
    /// @param lastchild arg. of constructor of complete instance. must be complete.
    /// @return an iterator to the entry in table associated with
    /// the instance of head build by the constructor of complete instance
    /// called with the given arguments (+ env).
    MapIterator<K,R,H> addemplace(const K& head,
                                  label_t a,
                                  const K& lastchild,
                                  bool& newrec);
    
    
    /// @brief find or create an entry in the table.
    /// @param head arg. of constructor of leaf instance. must be partial.
    /// @param a arg. of constructor of leaf instance. must be terminal.
    /// @return an iterator to the entry in table associated with
    /// the instance of head build by the constructor of complete instance
    /// for a leaf. called with the given arguments (+ env).
    MapIterator<K,R,H> addemplace(const K& head,
                                  label_t a,
                                  bool& newrec);
  
    /// compute a non-null fail run associated to the given key,
    /// to register in a Record for the case of failure to compute a best run.
    /// @param k key constructing to the fail run.
    const Run<K>* failRun(const K& k) const;

    /// compute a non-null fail run to register in a Record
    /// for the case of failure to compute a best run.
    /// @param head arg. of constructor of complete instance. must be partial.
    /// @param a arg. of constructor of complete instance.
    /// @param lastchild arg. of constructor of complete instance. must be complete.
    /// @return fail run associated to the key instance of head build by the
    /// constructor of complete instance called with the given arguments (+ env).
    const Run<K>* failRun(const K& head, label_t a, const K& lastchild) const;

    /// compute a non-null fail run associated to the given key,
    /// to register in a Record for the case of failure to compute a best run.
    /// @param head arg. of constructor of leaf instance. must be partial.
    /// @param a arg. of constructor of leaf instance. must be terminal.
    /// @return fail run associated to the key instance of head build by
    /// the constructor of complete instance for a leaf. called with
    /// the given arguments (+ env).
    const Run<K>* failRun(const K& head, label_t a) const;
    
    /// @param k key must be complete.
    R* at(const K& k);
    R* at(const K& k, bool& newrec);

    
//    /// @brief record in the table associated to the key k.
//    /// @param k key must be complete.
//    /// @return a pair with the following components:
//    /// - the first component is the record with key k in the table
//    ///   (pointers to the real addresses of record and key() in table).
//    ///   It is created if not found (new empty record).
//    /// - the second component is true iff the entry was created in this call
//    ///   (hence empty).
//    std::pair<R*, bool> getRecord(const K& k);

    
    /// @brief initialize the given record with the given complete k
    /// @param k key, must be complete, must be associated to rec in table.
    /// @param rec record that will be associated to k. its key must be NULL.
    void initRecord(const K* k, R* rec);
    

    /// best in particular case where k is complete.
    /// @param k key must be complete.
    const Run<K>* bestComplete(const K& k, size_t n);


    /// best in particular case where k is partial.
    /// @param k key must be partial.
    const Run<K>* bestPartial(const K& k, size_t n);


    /// add to the table all the runs with target k,
    /// or do nothing if they are already in the table.
    /// @param k key, must be complete.
    /// @return a pointer to the record (in table)
    /// where the runs have been added.
    R* addComplete(const K& k);
    
    
    /// the record rec is filled with all runs with target k
    /// and can be asked for best run (or k-best runs).
    /// @param k key, must be complete.
    /// @param rec pointer to record to fill with runs targeting k.
    /// it must be nun-NULL, and of key k.
    /// @return the number of runs added to the table
    size_t addComplete(const K& k,
                       R* rec);
    
    /// the table of instances is updated with all runs
    /// for all the complete instances of k.
    /// @param k key must be partial.
    /// @warning this must the first call to addPartial for k.
    /// @return the number of runs added to the table
    size_t addPartial(const K& k);

    
    /// Compute and add all complete runs with target k
    /// (or complete instances of k if k is partial)
    /// to records associated to k
    /// (associated to all complete instances of k if k is partial),
    /// using transitions returned by k to compute these runs.
    /// @param k key can be complete or partial.
    /// @param rec if k is complete then rec must be a pointer
    /// to the record associated to k in table,
    /// otherwise rec is NULL.
    /// @warning must be called by addComplete or addPartial.
    /// @return the number of runs added to the table
    size_t add(const K& k,
               R* rec);

    
    /// add to table all possible runs r
    /// based on an augmented version of transition tr.
    /// @param k key can be complete or partial.
    /// @param rec if k is complete then rec must be a pointer
    /// to the record associated to k in table, otherwise it is NULL.
    /// @param tr transition of base SWATA.
    /// @return the number of runs added to the table
    size_t add(const K& k,
               R* rec,
               const Transition& tr);
    
    
    // @brief particular case of add(k, rec, tr)
    // @param k key,
    //        - if tr is nullary (leaf) then k must be complete
    //        - otherwise k can be partial or complete.
    // @param rec if k is complete then rec must be a pointer
    //        to the record associated to k in table,
    //        otherwise rec is NULL.
    // @param tr transition of base SWATA.
    // @return the number of runs added to the table
//    size_t addTransition(const K& k,
//                         R* rec,
//                         const Transition& tr);

    
    /// add possible instances of run r
    /// to the entries in table corresponding to possible instances for k.
    /// dispatch to the four following functions, according to k and r:
    /// - addTable: k complete, r complete
    /// - add Instanciaite: k partial, r complete
    /// - addUpdate: k complete or partial, r partial
    /// @param k key can be complete or partial.
    /// @param rec if k is complete then rec must be a pointer
    /// to the record associated to k in table, otherwise it is NULL.
    /// @param r run can be complete or partial. must be non-NULL.
    /// @param tr transition of base SWATA.
    /// @return the number of runs added to the table
    size_t add(const K& k,
               R* rec,
               Run<K>* r,
               const Transition& tr);
    

    /// particular case of add(k, rec, r, tr)
    /// @param k key can be partial or complete.
    /// @param rec if k is complete then it must be a pointer
    ///        to the record associated to k in table,
    ///        otherwise it is NULL.
    /// @param r run must be partial and inner. must be non-NULL.
    /// @param tr transition of base SWATA.
    /// @return the number of runs added to the table
    size_t addUpdate(const K& k,
                     R* rec,
                     Run<K>* r,
                     const Transition& tr);
    
    
    /// dispatching to addUpdatePartial or addUpdateComplete.
    size_t addUpdateNext(const K& k,
                         R* rec,
                         Run<K>* r,
                         const Transition& tr,
                         const K& next);
    

    /// particular case of add(k, rec, r, tr)
    /// @param k key can be partial or complete.
    /// @param rec if k is complete then it must be a pointer
    ///        to the record associated to k in table,
    ///        otherwise it is NULL.
    /// @param r run must be partial and inner. must be non-NULL.
    /// @param tr transition of base SWATA.
    /// @param next key to be added as next argument of r.
    ///        must be partial.
    /// @return the number of runs added to the table
    size_t addUpdatePartial(const K& k,
                            R* rec,
                            Run<K>* r,
                            const Transition& tr,
                            const K& next);

    
    /// particular case of add(k, rec, r, tr)
    /// @param k key can be partial or complete.
    /// @param rec if k is complete then it must be a pointer
    ///        to the record associated to k in table,
    ///        otherwise it is NULL.
    /// @param r run must be partial and inner. must be non-NULL.
    /// @param tr transition of base SWATA.
    /// @param next key to be added as next argument of r.
    ///        must be complete.
    /// @return the number of runs added to the table
    size_t addUpdateComplete(const K& k,
                             R* rec,
                             Run<K>* r,
                             const Transition& tr,
                             const K& next);

    
    /// particular case of add(k, rec, r, tr)
    /// @param k key can be partial or complete.
    /// @param rec if k is complete then it must be a pointer
    ///        to the record associated to k in table,
    ///        otherwise it is NULL.
    /// @param r run must be partial and inner. must be non-NULL.
    /// @param tr transition of base SWATA.
    /// @param nextarg record, next argument for r. must not be NULL.
    /// @return the number of runs added to the table
    size_t addUpdateFinal(const K& k,
                          R* rec,
                          Run<K>* r,
                          const Transition& tr,
                          R* nextarg);

    
    /// particular case of add(k, rec, r, tr)
    /// create the complete instance k' of k defined by r,
    /// add r to the entry for k' in the map table and
    /// add the pair (k, k') to the instance table.
    /// @param k key must be partial.
    /// @param r run must be complete and inner. must be non-NULL.
    /// @param tr transition of base SWATA.
    /// @return the number of runs added to the table
    size_t addInstanciate(const K& k,
                          Run<K>* r,
                          const Transition& tr);

    
    /// particular case of add(k, rec, tr).
    /// @param k key must be partial.
    /// @param tr nullary transition of base SWATA.
    /// @return the number of runs added to the table
    size_t addLeafPartial(const K& k,
                          const Transition& tr);


    /// @param k key must be complete (entry in table).
    /// @param rec pointer to the record associated to k in table.
    /// @param tr nullary transition of base SWATA.
    /// @return the number of runs added to the table
    size_t addLeafComplete(const K& k,
                    R* rec,
                    const Transition& tr);

    
    // particular case of add(k, r, it)
    // create the instances k' of k defined by r,
    // add r to the entry for k' in the map table and
    // add the pair (k, k') to the instance table.
    // @param k key must be partial.
    // @param r run must be complete and empty.
    // size_t addLeaf(const K& k, Run<K>* r);
    
    
    /// particular case of add(k, i, r):
    /// add r to the record in the given entry i for k in the map table.
    /// @param k key must be complete.
    /// @param rec must be a pointer to the entry for k in table.
    /// @param r run must be complete. must be non-NULL.
    /// @return the number of runs added to the table
    size_t addTable(const K& k,
                  R* rec,
                  Run<K>* r);
    
    
    // the record pointed by i (second param) is updated
    // (filled with all runs with target k)
    // and can be asked for best run (or k-best runs).
    // @param k target key, must be complete,
    // @param i record to fill with runs rageting k.
    // i must point to the record associated to k in table.
    // size_t addComplete(const K& k, R* i);

    
    // table of instances is updated with all possible instances of k, and
    // map table is updated with all runs for all these instances of k.
    // @param k must be partial.
    // @warning this must the first call to addPartial for k.
    // size_t addPartial(const K& k);   

};


} // end namespace Parsing


// separated definition of template class
#include "TableParse.tpp"


#endif /* TableParse_hpp */

/// @}

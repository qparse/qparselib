//
//  TableParse.tpp
//  qparse
//
//  Created by Florent Jacquemard on 08/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//


namespace Parsing{


template<class K, class R, class H>
TableParse<K,R,H>::TableParse(ParsingEnv* env, RunCompare<K> comp):
TableA<K>(env, comp),
_table(), // empty
_pool(new RecordQueue<K>(comp, nullptr)), /// @todo make local var. in bestPartial
_instances(new TableInstances<K,R,H>()), // empty
_nb_rows(0),
_nb_runs(0)
{
    assert(env);
};


template<class K, class R, class H>
TableParse<K,R,H>::~TableParse()
{
    assert(_nb_rows == _table.size());
    INFO("delete Parse Table");
    TRACE("TableParse: nb of entries (complete keys)={}", _nb_rows);
    TRACE("TableParse: size={}", _table.size());
    TRACE("TableParse: nb of runs={}", _nb_runs);
    TRACE("TableParse: size of instances map:  {}", _instances->size());

//    for (auto it = _table.begin(); it != _table.end(); ++it)
//    {
//        K& k = it->first;
//        R& rec = it->second;
//    }
    
    delete _pool;
    delete _instances;
    _table.clear();
}


template<class K, class R, class H>
bool TableParse<K,R,H>::empty() const
{
    assert(_instances);
    
    return (_table.empty() && _instances->empty());
}


template<class K, class R, class H>
const Run<K>* TableParse<K,R,H>::best(const K& k, size_t n)
{
    assert(n >= 1);
    const Run<K>* r;
    if (k.partial())
    {
        r = bestPartial(k, n);
    }
    else
    {
        assert(k.complete());
        r = bestComplete(k, n);
    }
    assert(r); // no NULL Run, even in case of parse fail.
    assert(r->fail() || r->complete());
    assert(r->fail() || (! r->weight().unknown()));

    if (r->fail())
        TRACE("TableParse[{}].{}-best = FAIL", k, n);
    else
        TRACE("TableParse[{}].{}-best = {}", k, n, *r);

    return r;
}


template <class K, class R, class H>
const Run<K>* TableParse<K,R,H>::bestComplete(const K& k, size_t n)
{
    assert(k.complete());
    R* rec = addComplete(k);
    assert(rec);
    const Run<K>* r = rec->best(n); // best or fail run or nullptr
    assert(r); // the fail run of rec must be set to non-NULL.
    
//    // fail Run is not set in the record
//    if (r == nullptr)
//    {
//        assert(this->_env);
//        ParsingEnv& env = *(this->_env);
//        const Run<K>* failrun = k.failRun_one(env);
//        rec->setFailRun(failrun);
//        return failrun;
//    }
//    // best or fail run
//    else
//    {
//        return r;
//    }
    
    return r;
}


template <class K, class R, class H>
const Run<K>* TableParse<K,R,H>::bestPartial(const K& k, size_t n)
{
    assert(k.partial());
    assert(_pool);
    assert(this->_env);
    ParsingEnv& env = *(this->_env);
    const K* pk = _pool->key();

    // was already called for another partial key
    // recreate a new pool
    /// @todo make _pool a local var.?
    if (pk != nullptr)
    {
        if (k != *pk)
        {
            delete(_pool);
            _pool = new RecordQueue<K>(this->_comparer, k.failRun(env)); // empty pool
            _pool->setKey(new K(k));
        }
    }
    else
    {
       _pool->setKey(new K(k));
       _pool->setFailRun(k.failRun(env));
    }
        
    size_t ret = addPartial(k); // fill table
    if (ret > 0) { TRACE("added {} partial instances of {}", ret, k); }
    
    assert(_instances);
    // find complete instances of k in instance table
    std::pair<typename MapInstances<K,R,H>::iterator,
    typename MapInstances<K,R,H>::iterator> range =
    _instances->get(k);
    
    size_t l = 1;
    // for all record rec associated to a complete instance k' of k in instance table
    for(typename MapInstances<K,R,H>::iterator rit = range.first;
         rit != range.second; ++rit)
     {
         assert(rit->first == k);
         R* rec = rit->second;
         assert(rec);
         assert(rec->key());
         assert(rec->key()->complete());
         assert(rec->key()->instance(k));
         // dirty trick: set key of pool as first instance of k
         // (the pool needs a key)
         //if (rit == range.first) { pool.setKey(rec->key()); }
         // add rec.best(1) to rec.best(n) to the pool
         for (size_t i = 1; i <= n; i++)
         {
             const Run<K>* rc = rec->best(i);
             assert(rc);
             if (! rc->fail())
             {
                 TRACE("bestPartial: add {}-best of {}-th instance {} of {} to pool",
                       i, l, *(rec->key()), k);
                 assert(rc->complete());
                 _pool->add(rc);
            }
         }
         l++;
     }
    //TRACE("bestPartial: pool total size = {}", pool.size());
    return _pool->best(n);
}


// addComplete(k)
// k complete.
template <class K, class R, class H>
R* TableParse<K,R,H>::addComplete(const K& k)
{
    assert(k.complete());
    bool newrec = false;
    MapIterator<K,R,H> it = addemplace(k, newrec);
    R* rec = &(it->second);
    assert(it->first == k);
//    bool newrec = false;
//    R* rec = at(k, newrec);
    assert(rec);
    assert(rec->key() != nullptr);
    assert(*(rec->key()) == k);

    if (newrec)  // record rec is new and empty and has no key
    {
        size_t ret = addComplete(k, rec);
        TRACE("TableParse[{}].addComplete: {} runs added", k, ret);
    }
    else          // k and record already done, skip
    {
        TRACE("TableParse[{}].addComplete: complete key already in table,\
               skip", k);
    }
    return rec;
}


// addComplete(kc, rec)
// kc must be complete.
// rec must point to the record associated to kc.
template <class K, class R, class H>
size_t TableParse<K,R,H>::addComplete(const K& kc,
                                      R* rec)
{
    assert(kc.complete());
    assert(rec);
    assert(*(rec->key()) == kc);
    assert(rec->empty());
    TRACE("TableParse[{}]: addComplete", kc);
    return add(kc, rec);
}


// addPartial(kp)
// kp must be partial.
template <class K, class R, class H>
size_t TableParse<K,R,H>::addPartial(const K& kp)
{
    assert(kp.partial());
    assert(_instances);
    if (_instances->count(kp) == 0) // never called for kp
    {
        TRACE("TableParse: add partial key {}", kp);
        return add(kp, nullptr);
    }
    else        // ignore (kp already treated)
    {
        TRACE("TableParse: partial key already instanciated, skip: {}", kp);
        return 0;
    }
}


// add(k, rec)
// k is partial or complete.
// if k partial then rec must be NULL.
// if k complete then rec must point ot the associated record in table.
template <class K, class R, class H>
size_t TableParse<K,R,H>::add(const K& k,
                              R* rec)
{
    // TRACE("TableParse: add entry {} ({})", k, ((k.partial())?"partial":"complete"));
    assert(k.partial()  || (rec != nullptr));
    assert(k.complete() || (rec == nullptr));
    assert(rec == nullptr  || rec->empty());
    assert(rec == nullptr  || *(rec->key()) == k);

    //TRACE("TableParse: add {} to rec", k);
    assert(this->_env);
    ParsingEnv& env = *(this->_env);
    size_t ret = 0;
    // creation of runs of target k or one of its instances
    std::stack<const Transition*> st; // empty stack
    k.transitions(st, env);
    TRACE("TableParse: add {}: {} transitions", k, (st.size()));
    for (; (! st.empty()); st.pop())
    {
        ret += add(k, rec, *(st.top()));
    }
    return ret;
}


// add(k, rec, tr)
// k is partial or complete.
// if k partial then rec must be NULL.
// if k complete then rec must point ot the associated record in table.
// tr is a transition of base SWTA.
//template <class K, class R, class H>
//size_t TableParse<K,R,H>::add(const K& k,
//                              R* rec,
//                              const Transition& tr)
//{
//    TRACE("TableParse[{}]: transition {}", k, tr);
//
//    if (tr.terminal())
//    {
//        if (k.partial())
//        {
//            assert(rec == nullptr);
//            return addLeafPartial(k, tr);
//        }
//        else
//        {
//            assert(k.complete());
//            assert(rec != nullptr);
//            return addTransition(k, rec, tr);
//        }
//    }
//    else
//    {
//        assert(tr.inner());
//        assert(k.partial()  || (rec != nullptr));
//        assert(k.complete() || (rec == nullptr));
//        return addTransition(k, rec, tr);
//    }
//}


// addTransition(k, rec, tr)
// tr leaf and k complete
// or tr inner and k partial or complete.
// if k partial then rec must be NULL.
// if k complete then rec must point ot the associated record in table.
//template <class K, class R, class H>
//size_t TableParse<K,R,H>::addTransition(const K& k,
//                                        R* rec,
//                                        const Transition& tr)
//{
//    assert(tr.inner() || k.complete());
//    assert(k.complete() || (rec == nullptr));
//    assert(k.partial()  || (rec != nullptr));
//    label_t a = tr.label();
//    assert(this->_env);
//    const ParsingEnv& env = *(this->_env);
//    assert(tr.size() == tr.arity());
//    assert(tr.arity() == SymbLabel::arity(a));
//    const Weight& w = k.weight(a, tr, env);
//    assert(! w.unknown());
//
//    if (w.zero())
//    {
//        // incompatibility betweem k and tr. skip.
//        WARN("TableParse[{}]: weight zero (base transition {}), skip",
//              k, tr);
//        return 0;
//    }
//    else
//    {
//        Run<K>* r = new Run<K>(a, w);
//        assert(r->arity() == tr.arity());
//        assert(r->size() == 0);
//        assert(r->label() == a);
//
//        return add(k, rec, r, tr);
//    }
//}


// add(k, rec, tr)
// k is partial or complete.
// if k partial then rec must be NULL.
// if k complete then rec must point ot the associated record in table.
template <class K, class R, class H>
size_t TableParse<K,R,H>::add(const K& k,
                              R* rec,
                              const Transition& tr)
{
    assert(k.complete() || (rec == nullptr));
    assert(k.partial()  || (rec != nullptr));
    TRACE("TableParse[{}]: add transition {} -> {}", k, k.state(), tr);
    
    if (tr.terminal() && k.partial())
        return addLeafPartial(k, tr);
    
    // in the other cases, we can compute the weight
    assert(tr.inner() || k.complete());
    assert(tr.size() <= tr.arity()); // may differ in case of multiplicity
    assert(this->_env);
    const ParsingEnv& env = *(this->_env);
    label_t a;
    if (tr.inner())
    {
        a = tr.label();
    }
    else
    {
        assert(tr.terminal());
        a = k.label(tr, env);
    }
    
    assert(tr.arity() == SymbLabel::arity(a));
    const Weight& w = k.weight(a, tr, env);
    assert(! w.unknown());

    if (w.zero())
    {
        // incompatibility betweem k and tr. skip.
        WARN("TableParse[{}]: weight zero (base transition {}), skip",
              k, tr);
        return 0;
    }
    else
    {
        Run<K>* r = new Run<K>(tr, a, w); // w = initial weight
        TRACE("NEW run {} for {}", *r, tr);
        assert(r->arity() == tr.arity());
        assert(r->size() == 0);
        assert(r->label() == a);
        
        return add(k, rec, r, tr);
    }
}


// add(k, r, rec)
// k can be partial or complete
// if k is complete,
// then rec must point to a Record,
// otherwise rec must be NULL.
// r can be partial or complete
template <class K, class R, class H>
size_t TableParse<K,R,H>::add(const K& k,
                              R* rec,
                              Run<K>* r,
                              const Transition& tr)
{
    TRACE("TableParse[{}]: add run {}", k, *r);
    assert (r);
    assert (! r->weight().unknown());
    assert (r->inner() || r->terminal());
    
    if (r->complete())
    {
        assert(r->size() == tr.size()); // def complete
        if (k.complete())
        {
            assert(rec != nullptr);
            return addTable(k, rec, r);
        }
        else
        {
            // k partial (no other case)
            assert(k.partial());
            assert(rec == nullptr);
            // addLeafPartial(k, rec, r) was called earlier for case terminal
            assert(r->inner());
            return addInstanciate(k, r, tr);
        }
    }
    // r partial
    else
    {
        assert(r->partial());
        assert(r->inner());
        return addUpdate(k, rec, r, tr);
    }
}


// addTable(k, r, rec)
// k complete
// r complete
template <class K, class R, class H>
size_t TableParse<K,R,H>::addTable(const K& k,
                                   R* rec,
                                   Run<K>* r)
{
    assert(k.complete());
    assert(rec);
    assert(rec->key() != nullptr);
    assert(*(rec->key()) == k);
    //assert((rec->state == 1) || (rec->state == 2));
    assert(r);
    assert(r->complete());
    assert(! r->weight().unknown());
    
    size_t ret = rec->add(r); // will check that weight is known
    if (ret == 0)
    {
        TRACE("TableParse[{}]: added run {}", k, *r);
        _nb_runs++;  // stats
        return 1;
    }
    else
    {
        TRACE("TableParse[{}]: run {} not added (error code {})",
              k, *r, ret);
        return 0;
    }
};


// addUpdate(k, rec, r, tr)
// k complete or partial
// r partial
template <class K, class R, class H>
size_t TableParse<K,R,H>::addUpdate(const K& k,
                                    R* rec,
                                    Run<K>* r,
                                    const Transition& tr)
{
    assert(k.complete() || (rec == nullptr));
    assert(k.partial()  || (rec != nullptr));
    assert(r);
    assert(r->partial());
    assert(r->size() < r->arity());
    assert(r->size() < tr.size()); // partial
    assert(r->arity() >= 1);
    assert(r->inner());
    TRACE("TableParse[{}]: addUpdate run {}", k, *r);

    assert(this->_env);
    const ParsingEnv& env = *(this->_env);
    
    if (r->size() == 0) // first child
    {
        const K fs = K(k, tr, env);
        return addUpdateNext(k, rec, r, tr, fs);
    }
    else // next sibling
    {
        const K* previous = r->key(r->size() - 1);
        assert(previous);
        assert(previous->complete());
        assert(tr.arity() == r->arity());
        assert(1 <= r->size());
        assert(r->size() < tr.size());
        const K ns = K(*previous, r->size(), k, tr, env);
        return addUpdateNext(k, rec, r, tr, ns);
    }
}


template <class K, class R, class H>
size_t TableParse<K,R,H>::addUpdateNext(const K& k,
                                        R* rec,
                                        Run<K>* r,
                                        const Transition& tr,
                                        const K& next)
{
    // we construct instances of the run r, replacing pi by complete instances
    if (next.partial())
    {
        return addUpdatePartial(k, rec, r, tr, next);
    }
    // we update the weight of the run r
    else
    {
        return addUpdateComplete(k, rec, r, tr, next);
    }
}


// addUpdatePartial(k, rec, r, tr, next)
// k complete or partial
// r partial
// next partial
template <class K, class R, class H>
size_t TableParse<K,R,H>::addUpdatePartial(const K& k,
                                           R* rec,
                                           Run<K>* r,
                                           const Transition& tr,
                                           const K& next)
{
    assert(next.partial());
    assert(r);
    TRACE("TableParse[{}]: addUpdate run {} with partial {}", k, *r, next);

    // compute the runs for next child only if that was not done before
    size_t ret = addPartial(next);
    
    // find complete instances of k added in instance table
    assert(_instances);
    std::pair<typename MapInstances<K,R,H>::iterator,
    typename MapInstances<K,R,H>::iterator> range =
    _instances->get(next);
    
    // BUGFIX: the table _instances may be completed during the loop
    // (when the new run rinst is added)
    // and the loop will re-iterate over new elements of _instances,
    // which is not appropriate.
    // To avoid this problem,
    // we store in advance the instances of next in a FIFO .
    std::queue<R*> completes;
    for(typename MapInstances<K,R,H>::iterator rit = range.first;
        rit != range.second; ++rit)
    {
        assert(rit->first == next);
        R* rec = rit->second;
        assert(rec);
        assert(rec->key());
        assert(rec->key()->complete());
        assert(rec->key()->instance(next));
        completes.push(rec);
    }
    
    size_t l = 1;
    size_t total = completes.size();
    
    if (total == 0)
    {
        TRACE("TableParse[{}].addUpdate: no instances of {}", k, next);
    }
    else
    {
        TRACE("TableParse[{}].addUpdate: {} complete instances of {}",
              k, total, next);
    }
    
    while (! completes.empty())
    {
        R* nextrec = completes.front();
        TRACE("TableParse[{}].addUpdate: instance {}/{} of arg {}",
              k, l, total, next);
        ret += addUpdateFinal(k, rec, r, tr, nextrec);
        completes.pop();
        l++;
    }
    
    // all possible instances of r have been computed,
    // the partial r is not useful anymore.
    delete(r);
    return ret;
}


// addUpdateComplete(k, rec, r, tr, next)
// k complete or partial
// r partial
// next complete
template <class K, class R, class H>
size_t TableParse<K,R,H>::addUpdateComplete(const K& k,
                                            R* rec,
                                            Run<K>* r,
                                            const Transition& tr,
                                            const K& next)
{
    assert(next.complete());
    assert(r);
    TRACE("TableParse[{}]: addUpdate run {} with complete {}", k, *r, next);
    R* nextrec = addComplete(next);
    size_t ret = addUpdateFinal(k, rec, r, tr, nextrec);
    delete r;
    return ret;
}


// addUpdate(k, rec, r, tr, nextrec)
// k complete or partial
// r partial
template <class K, class R, class H>
size_t TableParse<K,R,H>::addUpdateFinal(const K& k,
                                         R* rec,
                                         Run<K>* r,
                                         const Transition& tr,
                                         R* nextrec)
{
    assert(r);
    assert(nextrec);
    const Run<K>* nextbest = nextrec->best(1);
    assert(nextbest);
    
    if (nextbest->fail())
    {
        WARN("TableParse[{}].addUpdate: fail sibling in {}", k, *r);
    }

    assert(nextbest->complete());
    assert(! nextbest->weight().unknown());
    if (nextbest->weight().zero())
    {
        WARN("TableParse[{}].addUpdate: child {} of run with zero weight",
                k, *nextbest);
    }
    // instance of r
    Run<K>* rinst = new Run<K>(*r, nextrec, nextbest);
    TRACE("TableParse[{}].addUpdate: updated run = {}", k, *rinst);
    // rinst can be partial or complete
    return add(k, rec, rinst, tr);
}


// addInstanciate(k, r)
// k partial
// r complete
// creation (from r) and registration of a new instance of p,
template <class K, class R, class H>
size_t TableParse<K,R,H>::addInstanciate(const K& k,
                                         Run<K>* r,
                                         const Transition& tr)
{
    assert(k.partial());
    assert(r);
    TRACE("TableParse[{}].addIntanciate run {}", k, *r);
    assert(r->inner());
    assert(r->complete());
    assert(! r->weight().unknown());
    assert(! r->weight().zero());
    const K* last = r->key(r->size() - 1);
    assert(last);
    assert(last->complete());
    assert(r->arity() == tr.arity());
    assert(r->size() == tr.size()); // r complete
    label_t a = tr.label();
//  assert(this->_env);
//  const ParsingEnv& env = *(this->_env);

    bool newrec = false;
    MapIterator<K,R,H> it = addemplace(k, a, *last, newrec);
    const K& kc = it->first;
    R* rec = &(it->second);
//  const K kc(k, a, *last,  env);
//  R* rec = at(kc);
    assert(kc.complete());
    assert(kc.instance(k));
    assert(rec);
    assert(rec->key() != nullptr);
    assert(*(rec->key()) == kc);

    assert(_instances);
    _instances->add(k, rec);
    return addTable(kc, rec, r);
}


// addLeafPartial(k, tr)
// k partial
// tr nullary
// creation (from r) and registration of a new instance of p,
template <class K, class R, class H>
size_t TableParse<K,R,H>::addLeafPartial(const K& k,
                                         const Transition& tr)
{
    assert(k.partial());
    assert(tr.terminal());
    assert(_instances);
    assert(this->_env);
    const ParsingEnv& env = *(this->_env);
    size_t nbleaves = k.leaf(tr, env);
    size_t ret = 0;
    
    if (nbleaves == 0)
    {
        TRACE("TableParse: key {} has no instance for tr={}", k, tr);
        ret = 0;
    }
    else if (nbleaves == 1)
    {
        label_t a = k.label(tr, env);
        bool newrec = false;
        MapIterator<K,R,H> it = addemplace(k, a, newrec);
        const K& kc = it->first;
        R* rec = &(it->second);
//      const K kc(k, a, env);
//      R* rec = at(kc);
        assert(kc.complete());
        assert(kc.instance(k));
        assert(rec);
        assert(rec->key() != nullptr);
        assert(*(rec->key()) == kc);
        assert(_instances);
        _instances->add(k, rec);
        ret = addLeafComplete(kc, rec, tr);
    }
    else
    {
        assert(nbleaves >= 2);
        std::queue<K> q = std::queue<K>();
        k.leaves(tr, env, q);
        // assert(q.size() > 1);
        while (! q.empty())
        {
            K& kc = q.front();
            assert(kc.complete());
            assert(kc.instance(k));
            bool newrec = false;
            MapIterator<K,R,H> it = addemplace(kc, newrec);
            R* rec = &(it->second);
            assert(it->first == kc);
//          R* rec = at(kc);
            assert(rec);
            assert(rec->key() != nullptr);
            assert(*(rec->key()) == kc);
            assert(_instances);
            _instances->add(k, rec);
            ret += addLeafComplete(kc, rec, tr);
            q.pop();
        }
    }
    return ret;
}


//template <class K, class R, class H>
//size_t TableParse<K,R,H>::addLeafComplete(const K& kp,
//                                          const K& kc,
//                                          R* rec,
//                                          bool flag,
//                                          const Transition& tr)
//{
//    assert(kp.partial());
//    assert(kc.complete());
//
//    // add entry using piecewise constructor
//    std::pair<typename MapRecord<K,R,H>::iterator,bool> pret =
//    _table.emplace(std::piecewise_construct,
//                   // Key copy
//                   std::forward_as_tuple(kc),
//                   // Record
//                   std::forward_as_tuple(this->_comparer));
//
//    R* rec = &(pret.first->second);
//    assert(k == pret.first->first);
//    assert(rec);
//
//    if (pret.second)  // record rec is new and empty and has no key
//    {
//        initRecord(&(pret.first->first), rec);
//        // assert(&(pret.first->first) == rec->key());
//        size_t ret = addComplete(k, rec);
//        TRACE("TableParse[{}].addComplete: {} runs added", k, ret);
//    }
//    else              // k and record already done, skip
//    {
//        assert(rec->key() != nullptr);
//        TRACE("TableParse[{}].addComplete: complete key already in table,\
//               skip", k);
//    }
//}


// addLeafComplete(k, rec, flag, tr)
// k complete
// tr nullary
template <class K, class R, class H>
size_t TableParse<K,R,H>::addLeafComplete(const K& k,
                                          R* rec,
                                          const Transition& tr)
{
    assert(k.complete());
    assert(tr.terminal());
    assert(this->_env);
    const ParsingEnv& env = *(this->_env);

    label_t a = k.label(tr, env);
    assert(tr.size() <= tr.arity()); // < in case of multiplicities > 1
    assert(SymbLabel::arity(a) == 0);
    const Weight& w = k.weight(a, tr, env);
    assert(! w.unknown());
    
    // incompatibility betweem k and tr.
    // should not happen with nbLeaves and leaf().
    if (w.zero())
    {
        ERROR("TableParse[{}]: weight zero leaf (base transition {}), skip",
              k, tr);
        return 0;
    }
    else
    {
        assert(rec);
        // create and add run
        Run<K>* r = new Run<K>(tr, a, w);
        assert(r->arity() == tr.arity());
        assert(r->arity() == 0);
        assert(r->size() == 0);
        assert(r->complete());
        assert(r->label() == a);
        
        return addTable(k, rec, r);
    }
}


//template<class K, class R, class H>
//std::pair<R*, bool>
//TableParse<K,R,H>::getRecord(const K& k)
//{
//    assert(k.complete());
//
//    // using piecewise constructor
//    std::pair<typename MapRecord<K,R,H>::iterator,bool> pret =
//    _table.emplace(std::piecewise_construct,
//                   // Key copy
//                   std::forward_as_tuple(k),
//                   // Record
//                   std::forward_as_tuple(this->_comparer));
//
//    R* rec = &(pret.first->second);
//    assert(k == pret.first->first);
//
//    // a new entry for was created in table (k had no Record )
//    if (pret.second)
//        initRecord(&(pret.first->first), rec); //  k == pret.first->first;
//
//    return std::make_pair(rec, pret.second); // pointer to new record, flag
//}


// run for the case of failure to compute best
template<class K, class R, class H>
const Run<K>* TableParse<K,R,H>::failRun(const K& k) const
{
    assert(this->_env);
    ParsingEnv& env = *(this->_env);
    const Run<K>* failrun = k.failRun(env);
    assert(failrun);
    assert(failrun->fail());         // recognized as fail run
    assert(failrun->weight().one()); // weight is one
    return failrun;
}


template<class K, class R, class H>
const Run<K>* TableParse<K,R,H>::failRun(const K& head,
                                         label_t a,
                                         const K& lastchild) const
{
    assert(this->_env);
    ParsingEnv& env = *(this->_env);
    const K k = K(head, a, lastchild, env);
    return failRun(k);
}


template<class K, class R, class H>
const Run<K>* TableParse<K,R,H>::failRun(const K& head,
                                         label_t a) const
{
    assert(this->_env);
    ParsingEnv& env = *(this->_env);
    const K k = K(head, a, env);
    return failRun(k);
}


template<class K, class R, class H>
typename MapRecord<K,R,H>::iterator TableParse<K,R,H>::addemplace(const K& k,
                                                                  bool& newrec)
{
    assert(k.complete());
    assert(this->_env);

    std::pair<typename MapRecord<K,R,H>::iterator,bool> pret =
    _table.emplace(std::piecewise_construct,
                   // Leaf constructor (Key copy)
                   std::forward_as_tuple(k),
                   // empty Record
                   std::forward_as_tuple(this->_comparer, failRun(k)));

    assert(k == pret.first->first);
    newrec = pret.second;
    // record rec is new and empty and has no key
    // init with components of the entry in table
    if (newrec)
        initRecord(&(pret.first->first), &(pret.first->second));
    
    return pret.first;
}


template<class K, class R, class H>
MapIterator<K,R,H> TableParse<K,R,H>::addemplace(const K& head,
                                                 label_t a,
                                                 const K& lastchild,
                                                 bool& newrec)
{
    assert(head.partial());
    assert(lastchild.complete());
    assert(this->_env);
    const ParsingEnv& env = *(this->_env);
    std::pair<typename MapRecord<K,R,H>::iterator,bool> pret =
    _table.emplace(std::piecewise_construct,
                   // instanciate cstr
                   std::forward_as_tuple(head, a, lastchild, env),
                   // empty Record
                   std::forward_as_tuple(this->_comparer,
                                         failRun(head, a, lastchild)));

    newrec = pret.second;
    // record rec is new and empty and has no key
    // init with components of the entry in table
    if (newrec)
        initRecord(&(pret.first->first), &(pret.first->second));
    
    return pret.first;
}


template<class K, class R, class H>
MapIterator<K,R,H> TableParse<K,R,H>::addemplace(const K& head,
                                                 label_t a,
                                                 bool& newrec)
{
    assert(head.partial());
    assert(SymbLabel::terminal(a));
    assert(this->_env);
    const ParsingEnv& env = *(this->_env);
    std::pair<typename MapRecord<K,R,H>::iterator,bool> pret =
    _table.emplace(std::piecewise_construct,
                   // instanciate cstr
                   std::forward_as_tuple(head, a, env),
                   // empty Record
                   std::forward_as_tuple(this->_comparer,
                                         failRun(head, a)));

    newrec = pret.second;
    // record rec is new and empty and has no key
    // init with components of the entry in table
    if (newrec)
        initRecord(&(pret.first->first), &(pret.first->second));

    return pret.first;
}


// inefficient but emplace not working
template<class K, class R, class H>
R* TableParse<K,R,H>::at(const K& k, bool& newrec)
{
    assert(k.complete());
    MapIterator<K, R, H> it = _table.find(k);
    // found
    if (it != _table.end())
    {
        R* rec = &(it->second);
        assert(rec->key() != nullptr);
        assert(*(rec->key()) == k);
        newrec = false;
        return rec;
    }
    // insert
    else
    {
        std::pair<typename MapRecord<K,R,H>::iterator,bool> pret =
        _table.emplace(k, this->_comparer);
        assert(pret.second);
        const K* kc = &(pret.first->first);
        assert(*kc == k);
        R* rec = &(pret.first->second);
        initRecord(kc, rec);
        newrec = true;
        return rec;
    }
}


template<class K, class R, class H>
R* TableParse<K,R,H>::at(const K& k)
{
    bool bidon = true;
    return at(k, bidon);
}


//template<class K, class R, class H>
//MapIterator<K,R,H> TableParse<K,R,H>::addinsert(const K& head,
//                                                label_t a,
//                                                bool& newrec)
//{
//    INFO("** addinsert {} {}", head, a);
//    assert(head.partial());
//    assert(SymbLabel::terminal(a));
//    assert(this->_env);
//    const ParsingEnv& env = *(this->_env);
//    const K kc(head, a, env);
//    MapIterator<K, R, H> it = _table.find(kc);
//    // found
//    if (it == _table.end())
//    {
//        newrec = false;
//        const K* kk = &(it->first);
//        INFO("** it: < {}, rec>", kk);
//        return it;
//    }
//    // insert it
//    else
//    {
//        std::pair<typename MapRecord<K,R,H>::iterator,bool> pret =
//        _table.insert(std::make_pair(kc, R(this->_comparer)));
//        assert(pret.second == true);
//        newrec = pret.second;
//        // record rec is new and empty and has no key
//        // init with components of the entry in table
//        initRecord(&(pret.first->first), &(pret.first->second));
//        INFO("** prit: < {}, rec>", pret.first->first);
//        return pret.first;
//    }
//}


template<class K, class R, class H>
void TableParse<K,R,H>::initRecord(const K* k, R* rec)
{
    assert(k);
    assert(k->complete());
    assert(rec);
    rec->setKey(k);
    TRACE("TableParse: new entry of key {}", *k);
    assert(rec->key() != nullptr);
    assert(rec->key()->complete());
    _nb_rows++;
}


} // end namespace Parsing

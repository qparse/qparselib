//
//  RecordQueue.tpp
//  qparse
//
//  Created by Florent Jacquemard on 12/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//


namespace Parsing{


template<class K>
RecordQueue<K>::RecordQueue(RunCompare<K> comp, const Run<K>* failrun):
Record<K>(comp, failrun), // base class constructor with parameter
_cand(comp),     // empty candidate list
_best()          // empty best list
{ }


template<class K>
RecordQueue<K>::~RecordQueue()
{
    // deallocate all runs in the _cand priority queue
    if (this->_key)
        TRACE("delete RecordQueue[{}] candidate queue (size={})",
              *(this->_key), _cand.size());
    else
        TRACE("delete RecordQueue[NULL] candidate queue (size={})",
              _cand.size());

    while (! _cand.empty())
    {
        Run<K>* r = _cand.top();
        _cand.pop();
        assert (r);
        delete r;
    }
    
    // no clear() for priority_queues
    _cand = std::priority_queue<RunRanked<K>*, std::vector<RunRanked<K>*>, RunCompare<K>>();
    //    std::priority_queue<RunRanked<K>*,
    //    vector<RunRanked<K>*>,
    //    std::function<bool(const Run<K>*, const Run<K>*)>>().swap(_cand);
    
    // deallocate all runs in the _best list
    if (this->_key)
        TRACE("delete RecordQueue[{}] best list (size={})", *(this->_key), _best.size());
    else
        TRACE("delete RecordQueue[NULL] best list (size={})",
              _best.size());

    for (typename std::vector<RunRanked<K>*>::iterator i = _best.begin();
         i != _best.end(); ++i)
    {
        assert(*i);
        delete (*i);
    }
    
    _best.clear();
}


template<class K>
bool RecordQueue<K>::empty() const
{
    return (_cand.empty() && _best.empty());
}


template<class K>
const Run<K>* RecordQueue<K>::best(size_t n)
{
    assert(n > 0);
    const K* key = this->_key;
    assert(key);

    // n-best run already computed for this record
    if (sizeBest() >= n)
    {
        return getBest(n);
    }
    
    // otherwise, we construct the next best run
    
    // cannot construct best (all runs constructed in best table)
    if (emptyCand())
    {
        TRACE("RecordQueue[{}]: no best {}", *key, n);
        const Run<K>* fail = this->_fail;
        assert((fail == nullptr) || fail->fail());

        // return a fail Run
        if (fail == nullptr)
        {
            WARN("RecordQueue[{}].best: failRun is NULL", *key);
        }
        return fail;
    }
    
    // otherwise, process the best candidate r
    /// @warning runs with unevaluated weight must always be on top
    RunRanked<K>* r = popCand();
    assert(r);
    // candidate's weight not evaluated
    if (r->weight().unknown())
    {
        // re-evaluate the weight and filter of the run r
        r->reset();

        // one subrun does not exist,
        // so doesn't the run.
        // the evaluation of weight fails and
        // the run is not added to the best list.
        // we do not push this run back to the heap of candidates
        // we do not push next runs because they can neither be evaluated.
        if (r->weight().unknown())
        {
            TRACE("RecordQueue.best: ignored UKN candidate {}.", *r);
        }
        else if (r->weight().zero())
        {
            WARN("RecordQueue.best: ignore zero weight run {}", *r);
        }
        else
        {
            // re-insert (and filter) run to candidate queue
            addCand(r);
        }
        
        // tail recursive call to extract and
        // evaluate the other candidates
        return best(n);
    }
    // all candidates have been evaluated
    // because unknown weight has priority
    // r is the best run
    else
    {
        // add the best run to the best table
        addBest(r);
        
        // compute next candidate if run is not terminal
        if (r->inner())
        {
            addNext(r);
        }
        
        // tail recursive call, either:
        // return the run that was just added (if place in best list matches)
        // or try to construct more best runs if needed.
        return best(n);
    }
}


//template<class K>
//void BRecord<K>::addCand(const Kroduction& t)
//{
//    TRACE("BRecord[{}].addTr {}", _key, t);
//    _nb_cand++;
//    add(new Run(t));
//}


// external add
template<class K>
size_t RecordQueue<K>::add(const Run<K>* r)
{
    assert(this->_key);
    const K& thiskey = *(this->_key);

    if (r == nullptr)
    {
        TRACE("RecordQueue[{}].add: ignore NULL run {}", thiskey);
        return 2;
    }
    
    assert(r->complete());
    
    // ignore invalid runs
    if (this->valid(r))
    {
        assert (r->inner() || r->terminal());
        assert(r);
        // add 'best-index'=1 to all children
        RunRanked<K>* rr = new RunRanked<K>(*r);
        assert(this->valid(rr));
        return addCand(rr);
    }
    else
    {
        TRACE("RecordQueue[{}].add: ignore run with unknown or zero weight {}",
        thiskey, *r);
        delete r;
        return 3;
    }
}


// add run to the heap of candidates
// accept unknown weight
template<class K>
size_t RecordQueue<K>::addCand(RunRanked<K>* r)
{
    assert(r);
    assert(r->complete());
    // assert(! r->weight().zero());
    assert(this->_key);
    const K& thiskey = *(this->_key);
    
    if (r->weight().unknown() || r->filter())
    {
        TRACE("RecordQueue[{}] push candidate {} run {}",
              thiskey,
              (r->weight().unknown()?"unevaluated":""),
              *r);
        this->_cand.push(r);
        this->_nb_cand++;
        return 1;
    }
    else
    {
        TRACE("RecordQueue[{}] reject filtered-out candidate run {}",
              thiskey, *r);
        this->_nb_best_rejected++;
        delete r;
        return 0;
    }
}


// extract run to the heap of candidates
template<class K>
RunRanked<K>* RecordQueue<K>::popCand()
{
    assert(this->_key);
    const K& thiskey = *(this->_key);

    RunRanked<K>* r = _cand.top();
    assert(r); // should be no NULL runs in candidate list
    assert(r->inner() || r->terminal());
    assert(r->complete());  // no missing child
    TRACE("RecordQueue[{}] pop cand run {}", thiskey, *r);
    _cand.pop();
    return r;
}


template<class K>
bool RecordQueue<K>::emptyCand() const
{
    return _cand.empty();
}


// add run to the list of bests
template<class K>
void RecordQueue<K>::addBest(RunRanked<K>* r)
{
    assert (r);
    assert(! r->weight().unknown());
    assert(r->inner() || r->terminal());
    assert(r->complete()); // weight is evaluated
    assert(this->_key);
    const K& thiskey = *(this->_key);
    
    if (r->filter())
    {
        TRACE("RecordQueue[{}] add best run {}", thiskey, *r);
        _best.push_back(r);
    }
    else
    {
        TRACE("RecordQueue[{}] rejected run filtered-out {}", thiskey, *r);
        this->_nb_best_rejected++;
        delete r;
    }
}


template<class K>
Run<K>* RecordQueue<K>::getBest(size_t n)
{
    assert(n > 0);
    assert(_best.size() >= n);
    Run<K>* best = _best[n-1];
    assert(best);
    assert(best->complete());
    assert(! best->weight().unknown());
    
    return best;
}


template<class K>
size_t RecordQueue<K>::sizeBest() const
{
    return _best.size();
}


// template<class K>
// bool RecordQueue<K>::bestFilter(const Run<K>* r)
// {
//     assert (r);
//     assert(r->inner() || r->terminal());
//
//    // filter acc. to optimization flags
//     if (OPT_RUN_UNIT)
//     {
//         assert (OPT_RUN_DUR);
//         if (r->duration.unit() && r->inner())
//             return false;
//     }
//     return true;
// }


template<class K>
void RecordQueue<K>::addNext(RunRanked<K>* r)
{
    assert(r);
    assert(r->complete());
    assert(r->inner());
    
    // add next candidates
    for (int i = 0; i < r->arity(); i++)
    {
        // copy and increment rank
        // @warning the run added (to candidates) has unknown weight
        // the evaluation is delayed to next call to best (lazy eval)
        addCand(new RunRanked<K>(*r, i));
        
        // do not delete r : it has been placed in best list!
    }
}


} // end namespace Parsing

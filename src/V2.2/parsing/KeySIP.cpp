//
//  KeySIP.cpp
//  squanty
//
//  Created by Florent Jacquemard on 29/10/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include "KeySIP.hpp"


namespace Parsing{


// static
//pre_t KeySIP::P_UNKNOWN = -1;

// static
//pre_t KeySIP::P_NULL = 0;

// static
//bool KeySIP::P_KNOWN(pre_t x)
//{
//    return (x >= 0);
//}


// internal use
KeySIP::KeySIP(state_t s, IntervalTree* node, rtu_t len,
               pre_t pre, pre_t post):
KeySI(s, node, len),
_pre(pre),
_post(post)
{ }


// copy
KeySIP::KeySIP(const KeySIP& k):
KeySI(k),
_pre(k._pre),
_post(k._post)
{ }


// top constructor (top state = first bar)
KeySIP::KeySIP(rtu_t len, const ParsingEnv& env, bool f_open,
               pre_t pre, pre_t post):
KeySI(len, env, f_open),
_pre(pre),
_post(post)
{
    assert(P_KNOWN(pre));
}


// firstChild
KeySIP::KeySIP(const KeySIP& parent,
               const Transition& tr,
               const ParsingEnv& env):
KeySI(parent, tr, env),
_pre(parent._pre),
_post(P_UNKNOWN)
{
    assert(P_KNOWN(_pre));
    // construction of sub-interval is the same as in KeySI
    /// @todo cases grace and fork
}


// nextSibling
KeySIP::KeySIP(const KeySIP& previous,
             size_t i,
             const KeySIP& parent,
             const Transition& tr,
             const ParsingEnv& env):
KeySI(previous, i, parent, tr, env),
_pre(previous._post),
_post(P_UNKNOWN)
{
    assert(previous.complete());
    // construction of sub-interval is the same as in KeySI
    /// @todo cases grace and fork
}


// instanciate: copy and propagation of post
KeySIP::KeySIP(const KeySIP& head,
             label_t a,
             const KeySIP& lastchild,
             const ParsingEnv& env):
KeySI(head, a, lastchild, env),
_pre(head._pre),
_post(lastchild._post)
{
    assert(head.partial());
    assert(lastchild.complete());
    // this key is now complete
    assert(P_KNOWN(this->_pre));
    assert(P_KNOWN(this->_post));
}


// complete leaf: copy and computation of post
KeySIP::KeySIP(const KeySIP& head,
               label_t a,
               const ParsingEnv& env):
KeySI(head, a, env),
_pre(head._pre)
{
    assert(SymbLabel::terminal(a));
    assert(_interval);
    // case of terminal bar symbol (double bar)

    assert(P_KNOWN(_pre));

    if (P_KNOWN(head._post))
    {
        ERROR("KeySIP : instance as leaf of complete ptr {}", head);
        _post = head._post;
        assert(_post == _interval->rsize());
    }
    else
    {
        if (! _interval->aligned())
        {
            assert(SymbLabel::bar(a));
            assert(env.segment);
            _interval->align(env.segment);
        }
        _post = _interval->rsize();
    }
    assert(P_KNOWN(_post));
}


Run<KeySIP>* KeySIP::failRun(const ParsingEnv& env) const
{
    assert(env.wta);
    const Transition& t1 = env.dummyTransition_one();
    assert(t1.weight().one());
    if (! _interval->aligned())
    {
        assert(env.segment);
        _interval->align(env.segment);
    }
    label_t a = SymbLabel::make_fail(this->size());

    return new Run<KeySIP>(t1, a, t1.weight());
}


size_t KeySIP::size() const
{
    assert(_interval);
    return _pre + _interval->lsize();
}


void KeySIP::leaves(const Transition& tr,
                    const ParsingEnv& env,
                    std::queue<KeySIP>& q) const
{
    assert(this->partial());
    assert(q.empty());
    size_t nbl = leaf(tr, env);
    
    if (nbl == 0)
    {
        ERROR("KeySIP: leaves called for 0 leaf");
    }
    else if (nbl == 1)
    {
        ERROR("KeySIP: leaves called for 1 leaf, call cstr instead");
        label_t a = tr.label();
        q.emplace(*this, a, env);
    }
}

// KeySIP::innerWeight = KeySI::innerWeight

Weight KeySIP::terminalWeight(label_t a,
                              const Transition& tr,
                              const ParsingEnv& env) const
{
    assert(tr.terminal());
    assert(SymbLabel::terminal(a));
    assert(this->complete());
    assert(this->leaf(tr, env) != 0);

    // tweight = transition weight = complexity
    Weight tweight = tr.weight(); // copy
    
    assert(! tweight.unknown());

    // terminal bar interval:
    // there are no points hence no need to compute the distance
    if (SymbLabel::bar(tr.label()) && (SymbLabel::arity(tr.label())) == 0)
    {
        TRACE("KeySIP: terminal bar interval: {} ({}aligned) for transition {}",
              *(_interval),
              ((! _interval->aligned())?"not ":""),
              tr);
        assert(tr.label() == a);
        assert(! _interval->inhabited());
        assert(_interval->rduration() < _barlen);
    }
    // combine transition weight with distance weight
    // CST_WEIGHT_TYPE == WeightDom::STOCHASTIC
    else if (tweight.hasType("ViterbiWeight"))
    {
        assert(env.segment);
        assert(_interval);
//        assert(_pre != PP_UNKNOWN);          // csq complete
//        assert(_post != PP_UNKNOWN);         // csq complete
//        assert(_post == _interval->rsize()); // csq filterTerminal by leaf
        tweight *= Weight(new GaussianDistance(env.segment, _interval));
    }

    // CST_WEIGHT_TYPE == WeightDom::PENALTY
    else if (tweight.hasType("TropicalWeight"))
    {
        assert(_interval);
        // compute distance to input segment
        // ALPHA applied to dist only (not complexity)
        Weight dist = Weight(new Distance(env.segment, _interval));
        TRACE("KeySIP: tropical distance for inter={} tr={} dist={}",
              *(this), tr, dist);

        // new weight = (1 - alpha) . former transition weight * alpha . distance
        // update: alpha applied to dist only (not to complexity)
        tweight *= dist; // tropical weight product is a sum
    }
    else
    {
        ERROR("KeySIP.weight: unexpected weight type");
        // tweight will be UNDEF
    }
    
    return tweight;
}


// KeySIP::filterInnerLabel: same as for KeySI


/// we do not filter out grace rests
bool KeySIP::filterTerminalLabel(const label_t& a,
                                 const ParsingEnv& env) const
{
    assert(SymbLabel::terminal(a));
    assert(_interval);
    if (! _interval->aligned())
    {
        WARN("KeySIP: unaligned interval {} for filtering {}",
             *(_interval), SymbLabel::to_string(a));
        assert(env.segment);
        _interval->align(env.segment);
    }

    /// double bar (end of score)
    if (SymbLabel::bar(a))
    {
        assert(_interval);
        // && (DBL_APPROX_EQUAL(_interval->rend(), _interval->rbegin()))
        return ((_interval->rduration() < _barlen) &&
                (! _interval->inhabited()));
        // 0 or -1 point to read in interval
    }
    else if (SymbLabel::dummy(a))
    {
        WARN("KeySIP: filter dummy symbol");
        return false;
    }
    else if (SymbLabel::note(a) || SymbLabel::rest(a) ||
             SymbLabel::continuation(a) || SymbLabel::dot(a))
    {
        if ((_post != P_UNKNOWN) && (_post != _interval->rsize()))
            return false;
        
        assert(_pre != P_UNKNOWN);
        size_t n = SymbLabel::nbEvents(a);
        return (n == _pre + _interval->lsize());
        // n points = grace-notes + grace-rests + 1 note or -1
    }
    else
    {
        ERROR("KeySIP: unexpected terminal symbol {}", SymbLabel::to_string(a));
        return false;
    }
}


//bool KeySIP::filterTerminalLabel(const label_t& a,
//                                 const ParsingEnv& env) const
//{
//    return (pointsTerminalLabel(a, env) >= 0);
//}


KeySIP& KeySIP::operator= (const KeySIP& p)
{
    if(this != &p)
    {
        KeySI::operator=(p);
        _pre = p._pre;
        _post = p._post;
    };
    return *this;
}


//bool KeySIP::operator==(const KeySIP& p) const
//{
//    if (KeySI::operator!=(p))
//        return false;
//    else
//        return ((_pre == p._pre) && (_post == p._post));
//}


bool KeySIP::operator==(const KeySIP& p) const
{
    return (KeySI::operator==(p) &&
            (_pre == p._pre) &&
            (_post == p._post));    
//    return ((_state == p._state) &&
//            (_pre == p._pre) &&
//            (_post == p._post) &&
//            this->equal_interval(p));
}


bool KeySIP::operator!=(const KeySIP& p) const
{
    return (! (*this == p));
}


bool KeySIP::instance(const KeySIP& p) const
{
    assert(_pre != P_UNKNOWN);
    assert(p._pre != P_UNKNOWN);
    return (KeySI::instance(p) && ((p._post == P_UNKNOWN) ||
                                   (_post == p._post)));
}


// always complete
bool KeySIP::complete() const
{
    assert(_pre != P_UNKNOWN);
    return (KeySI::complete() && (_post != P_UNKNOWN));
}


std::ostream& operator<<(std::ostream& o, const KeySIP& p)
{
    if (p._interval == NULL)
    {
        o << KEY_LPAR << p.state() << KEY_RPAR;
        return o;
    }
    
    o << KEY_LPAR;
    o << " " << p.state() << " ";
    
    // short version
    if (p._interval)
    {
        o << *(p._interval);
    }
    else
    {
        o << "[]";
    }
    assert(p._pre != P_UNKNOWN);
    o << " pre=";
    o << p._pre;
    o << " post=";
    if (p._post == P_UNKNOWN)
        o << "UnK";
    else
        o << p._post;
    o << KEY_RPAR;
    
    return o;
}


} // end namespace Parsing

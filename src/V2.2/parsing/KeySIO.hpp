//
//  KeySIO.hpp
//  squant2
//
//  Created by Florent Jacquemard on 02/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{

#ifndef KeySIO_hpp
#define KeySIO_hpp

#include <stdio.h>
#include <assert.h>
#include <utility>      // std::pair, std::make_pair

#include "util.hpp"
#include "hash.hpp"
#include "trace.hpp"
#include "RTU.hpp"
#include "MTU.hpp"
#include "KeySI.hpp"
#include "KeySIP.hpp"  // definition of pre_t etc
#include "Distance.hpp"
#include "GaussianDistance.hpp"
#include "IntervalTree.hpp"
#include "IntervalHeap.hpp"



namespace Parsing{

/// type for pre post values in augmented states
/// same as KeySIP.hpp
//typedef long pre_t;
//static pre_t P_NULL = 0;
//static pre_t P_UNKNOWN = -1;
//static bool P_KNOWN(pre_t x) { return (x >= 0); }


/// extension of KeySI with values for dealing with rests (offset points).
/// It is made of:
/// components of KeySI:
/// - one state of a base WTA
/// - one interval (inherited attribute)
/// - rank for k-best algo (not used for 1-best)
/// new components:
/// - (pre) number of points in second half of interval of previous leaf
/// - (preon) number of onset points in second half of interval of previous leaf
/// - flag described below for second half of interval of previous leaf
/// - (post)  number of points in second half of this interval
///   can be unknown (making the KeySIO partial)
/// - (poston) number of onset points in second half of this interval
/// - flag described below for this interval.
///
/// The flag (called laston) for an interval is :
/// - true iff the last point in the interval is an onset
/// - false iff the last point in the interval is an offset or
///   the interval contains no point.
class KeySIO : public KeySI  // Key<KeySI>
{
public:
        
    /// @brief top constructor.
    /// allocate an initial key
    /// which will serve to create the other keys.
    /// @param barlen first estimated bar duration in rtu.
    /// @param env input environment.
    /// @param f_open flag true iff the initial MTU interval is right-open,
    /// if false, the right bound is set to the duration of input segment.
    /// @param pre intial pre value (for chainig with other segment).
    /// must not be P_UNKNOWN.
    KeySIO(rtu_t barlen, const ParsingEnv& env, bool f_open=true,
           pre_t pre        = P_NULL,
           pre_t preon      = P_NULL,
           bool  prelaston  = false);
//           pre_t post       = P_UNKNOWN,
//           pre_t poston     = P_UNKNOWN,
//           bool  postlaston = false);

    /// @brief copy
    KeySIO(const KeySIO& k);
    
    /// @brief constructor of first child.
    /// construct a partial key to be instanciated as the first child of parent
    /// in the body of an augmented transition.
    /// @param parent head key of the augmented transition.
    ///        partial or complete.
    ///        inner must be true for parent key.
    /// @param tr WTA transition, augmented by the transition partially
    ///        constructed here. must be inner.
    /// @param env input environment.
    KeySIO(const KeySIO& parent,
           const Transition& tr,
           const ParsingEnv& env);
        
    /// @brief constructor for building next sibling.
    /// construct a partial key to be instanciated as sibling
    /// in the body of an augmented transition.
    /// @param previous previous sibling of this key in the body of the
    ///        augmented transition. must be complete.
    /// @param i position of the next sibling in body, in 1..arity-1
    ///        (0 is the position of the first child).
    /// @param parent head key of the augmented transition
    ///        partial or complete.
    /// @param tr SWTA transition, augmented by the transition partially
    ///        constructed here.
    /// @param env input environment.
    KeySIO(const KeySIO& previous,
           size_t i,
           const KeySIO& parent,
           const Transition& tr,
           const ParsingEnv& env);
    
    /// @brief constructor of complete instance.
    /// construct a complete upgrade of the head key, by copy,
    /// to be the head of an augmented transition.
    /// @param head a key to be instanciated. must be partial.
    /// @param a label of the augmented transition.
    /// @param lastchild last child in the body of the augmented transition.
    ///        must be complete.
    /// @param env input environment.
    /// @warning should not be called for this class (head must be partial).
    /// @see Key::instanciate
    KeySIO(const KeySIO& head,
           label_t a,
           const KeySIO& lastchild,
           const ParsingEnv& env);
        
    /// @brief constructor of complete instance for a leaf.
    /// @param head a key to instanciate as leaf. must be partial
    /// @param a label of the augmented transition. must be terminal.
    /// @param env parsing environment.
    /// @warning should not be called on this class (head must be partial)
    /// @warning leaf for head, with same args, must return 1.
    KeySIO(const KeySIO& head,
           label_t a,
           const ParsingEnv& env);
            

    /// allocate and return a special Run representing a parse failure for this key.
    /// @param env parsing environment.
    /// @return a pointer to a newly allocated fail run, with
    /// - transition with dummy symbol, empty body, weight one in the
    ///   domain of the base wta in the passed environment.
    /// - fail symbol
    /// - weight one (in the domain of the base wta).
    /// This returned run will answer succesfully to Run.fail().
    Run<KeySIO>* failRun(const ParsingEnv& env) const;

    
    /// @brief completion of this partial leaf key to be the head
    ///        of a nullary (leaf) transition augmenting tr.
    /// Build and store in the given queue q
    /// all ther new complete instances of this key,
    /// using the parsing environment env.
    /// @param tr a SWTA transition, augmented by the transition partially
    /// constructed here. must be nullary (leaf, terminal).
    /// @param env parsing environment.
    /// @param q empty queue used to store the complete instances.
    /// @warning this Key must be partial.
    virtual void leaves(const Transition& tr,
                        const ParsingEnv& env,
                        std::queue<KeySIO>& q) const;

    /// @brief Key assignement.
    virtual KeySIO& operator= (const KeySIO& p);
        
    /// @brief for use as key in a unordered_multimap.
    virtual bool operator==(const KeySIO& p) const;
    virtual bool operator!=(const KeySIO& p) const;
    
    /// @brief return wether this Key is an instance of p.
    /// = equality in this case because KeyS are always complete.
    virtual bool instance(const KeySIO& p) const;

    /// @brief all attributes of the key are valued
    virtual bool complete() const;
    
    /// @brief accessor
    inline pre_t pre() const { return _pre; }

    /// @brief accessor
    inline pre_t pre_on() const { return _pre_on; }

    /// @brief accessor
    inline bool pre_laston() const { return _pre_laston; }

    /// @brief accessor
    inline pre_t post() const { return _post; }

    /// @brief accessor
    inline pre_t post_on() const { return _post_on; }

    /// @brief accessor
    inline bool post_laston() const { return _post_laston; }
    
    /// @brief number of input to read for this KeySIO state
    /// @todo RM : replaced by size()
    virtual inline size_t points() const { return _pre + _interval->lsize(); }

    // @brief number of input points to read for making a transition
    // to this key, with the given label symbol.
    // @param a a label. must be terminal.
    // @param env parsing environment.
    // @return -1 if the given symbol a cannot label a transition targeting
    // this key, or, otherwise, the number of points of the interval
    // to read (parse) when making the transition to this Key with symbol a.
    // virtual long pointsTerminalLabel(const label_t& a,
    //                                  const ParsingEnv& env) const;
    
    /// @brief printer
    friend std::ostream& operator<<(std::ostream& o, const KeySIO& p);
    
protected:
    
    /// number of onset and offset points in the second half
    /// of the interval of the previous leaf.
    /// They will be aligned to the left bound of current interval, or ignored.
    pre_t _pre;

    /// number of onset points among the points described in _pre
    pre_t _pre_on;

    /// flag true if the last point in second half of the interval of
    /// the previous leaf is an onset, and
    /// false if the last point in second half is an offset or
    /// if the second half contains no point.
    bool _pre_laston;

    /// number of onset and offset points in the second half of current interval.
    /// They will be aligned to the right bound of current interval, or ignored.
    pre_t _post;

    /// number of offset points among the points described in _post
    pre_t _post_on;

    /// flag true if the last point in second half of the current interval
    /// is an onset, and false if the last point in second half is an offset or
    /// if the second half contains no point.
    bool _post_laston;

    /// @brief accessor
    virtual size_t size() const;
    
    /// @brief weight for the case of a and tr terminal.
    virtual Weight terminalWeight(label_t a,
                                  const Transition& tr,
                                  const ParsingEnv& env) const;
           
    /// @brief compatibility of this key with the given label symbol.
    /// @param a a label. must be terminal.
    /// @param env parsing environment.
    /// @return whether the given symbol a can label
    ///         a transition targeted by this key.
    virtual bool filterTerminalLabel(const label_t& a,
                                     const ParsingEnv& env) const;

private:
    
    /// @brief constructor for internal use
    KeySIO(state_t s, IntervalTree* node, rtu_t barlen,
           pre_t pre, pre_t pre_on,  bool pre_laston,
           pre_t post, pre_t post_on,  bool post_laston);

    /// check that the post values are as expected.
    /// @warning this key must be terminal.
    bool checkPost(const ParsingEnv& env) const;
    
    /// compute the laston flag for the interval of this key.
    /// @warning  the interval of this key must be aligned.
    bool laston(const ParsingEnv& env) const;
};


/// hash function for using as key in a table.
/// rank is ignoreds : same as SpointerHasher
struct KeySIOHasher
{
    std::size_t operator()(const KeySIO& k) const
    {
        using std::size_t;
        using std::hash;
        assert(k.interval());
        
        // see constant.h
        std::size_t h = HASH_SEED;
        h += hash<state_t>()(k.state());
        h = h * HASH_FACTOR + hash<rtu_t>()(k.interval()->rbegin());
        h = h * HASH_FACTOR + hash<rtu_t>()(k.interval()->rduration());
        h = h * HASH_FACTOR + hash<mtu_t>()(k.interval()->mbegin());
        h = h * HASH_FACTOR + hash<mtu_t>()(k.interval()->mduration());
        h = h * HASH_FACTOR + hash<pre_t>()(k.pre());
        h = h * HASH_FACTOR + hash<pre_t>()(k.pre_on());
        h = h * HASH_FACTOR + hash<bool>()(k.pre_laston());
        h = h * HASH_FACTOR + hash<pre_t>()(k.post());
        h = h * HASH_FACTOR + hash<pre_t>()(k.post_on());
        h = h * HASH_FACTOR + hash<bool>()(k.post_laston());
        return h;
    }
    
};

} // end namespace Parsing


#endif /* KeySIO_hpp */

/// @}

//
//  RecordOne.tpp
//  squanty
//
//  Created by Florent Jacquemard on 07/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//


namespace Parsing{

template<class K>
RecordOne<K>::RecordOne(RunCompare<K> comp, const Run<K>* failrun):
Record<K>(comp, failrun), // base class constructor with parameter
_best(nullptr) // initialize with worst run
{ }


template<class K>
RecordOne<K>::~RecordOne()
{
    if (this->_key)
        TRACE("delete RecordOne[{}]", *(this->_key));
    else
        TRACE("delete RecordOne[NULL]");

    if(_best)
    {
        if (this->_key)
            TRACE("delete best run {} of RecordOne[{}]", *_best, *(this->_key));
        else
            TRACE("delete run {} of RecordOne[NULL]", *_best);
        delete(_best);
    }
}


//Run<K>* RecordOne<K>::best(TableParse<K,Record<K,H>,H>* table, size_t n)
template<class K>
const Run<K>* RecordOne<K>::best(size_t n)
{
    assert(n == 1);
    assert ((_best == nullptr) || _best->complete());
    assert ((_best == nullptr) || (! _best->weight().unknown()));
    const K* key = this->_key;
    assert(key);

    // k-best run _best is already computed at construction
    // return a fail Run when there exist no best
    if (_best == nullptr)
    {
        TRACE("RecordOne[{}].best : no best run", *key);
        const Run<K>* fail = this->_fail;
        assert((fail == nullptr) || fail->fail());

        if (fail == nullptr)
        {
            WARN("RecordOne[{}].best: failRun is not set", *key);
        }
        return fail;
    }
    
    return _best;
}


//template<class K>
//void RecordOne<K>::addCand(const Production& t)
//{
//    TRACE("RecordOne[{}].addTr {}", _key, t);
//    _nb_cand++;
//    add(new Run(t));
//}


template<class K>
size_t RecordOne<K>::add(const Run<K>* r)
{
    assert(this->_key);
    const K& thiskey = *(this->_key);
    this->_nb_cand++;

    // ignore invalid runs
    if (! this->valid(r))
    {
        if (r == nullptr)
        {
            WARN("RecordOne[{}].add: reject NULL run", thiskey);
            return 2;
        }
        else
        {
            WARN("RecordOne[{}].add: reject invalid run {}", thiskey, *r);
            delete r;
            return 3;
        }
    }
    
    assert(r);
    assert (r->inner() || r->terminal());
    assert ((_best == nullptr) || _best->complete());
    assert ((_best == nullptr) || (! _best->weight().unknown()));
    
    if (r->partial())
    {
        ERROR("RecordOne[{}].add: reject partial run {}", thiskey, *r);
        delete r;
        return 5;
    }
    else if ((_best == nullptr) || this->_comp(_best, r))
    {
        assert(r->complete());
        // r is the new best
        if( r->filter() )
        {
            if (_best)
            {
                delete _best;
            }
            _best = r;
            TRACE("RecordOne[{}].add: add best run {}", thiskey, *r);
            assert (_best->complete());
            assert (! _best->weight().unknown());
            return 0;
        }
        else
        {
            TRACE("RecordOne[{}].add: reject filtered-out run {} {}",
                  thiskey, *r, r->duration());
            this->_nb_best_rejected++;
            delete r;
            return 6;
        }
    }
    else
    {
        TRACE("RecordOne[{}].add: ignore run {} (not better)", thiskey, *r);
        delete r;
        return 1;
    }
}


template<class K>
bool RecordOne<K>::empty() const
{
    return (_best == nullptr);
}


} // end namespace Parsing

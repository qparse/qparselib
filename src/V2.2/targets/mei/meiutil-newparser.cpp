//
//  main.cpp
//  squant
//
//  Created by Florent Jacquemard on 10/12/2017.
//  Copyright © 2017 Florent Jacquemard. All rights reserved.
//
// command line for quantization of input segment according to schema
// 6 scenarios, by combination of the following options
//
// 3 options for WTA:
// - SCHEMA_COMBO
//   build combo WTA from input segment and schema
//   enumerate trees of combo WTA
// - SCHEMA_PRE (not stable)
//   build pre WTA from input segment and schema
//   parse input segment against pre WTA
// - SCHEMA_WTA
//   parse input segment against given (schema) WTA
//
// 2 options for parsing algo
// - PARSING_VITERBI
//   1-best
// - PARSING_KBEST
//   k-best


#include <stdio.h>
#include <iostream>
#include <time.h>
#include <string>
#include <vector>

#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>     // handling commandline options
#include <assert.h>

#include "constant.h"

using namespace std;
namespace ScoreModel{};
namespace sm = ScoreModel;

#include "MEI.hpp"

#include "Score.hpp"
#include "Event.hpp"
#include "Measure.hpp"
#include "Note.hpp"

// compile options
#ifdef QP_WEIGHT
#undef QP_WEIGHT
#endif
#define QP_WEIGHT WEIGHT_STOCHASTIC  // alt WEIGHT_PENALTY

#ifdef QP_SCHEMA
#undef QP_SCHEMA
#endif
#define QP_SCHEMA SCHEMA_WTA  // alt SCHEMA_COMBO

#ifdef QP_PARSING
#undef QP_PARSING
#endif
#define QP_PARSING PARSING_VITERBI // 1-best parsing, alt   PARSING_KBEST

// check compile flags
#include "QPconfig.h"

// switch options for WTA weight
#include "FloatWeight.hpp"
#if QP_WEIGHT == WEIGHT_PENALTY
#include "TropicalWeight.hpp"
#elif QP_WEIGHT == WEIGHT_STOCHASTIC
#include "ViterbiWeight.hpp"
#endif

#include "WTA.hpp"
#include "Schema.hpp"
#include "DurationList.hpp"

// output
#include "RT.hpp"




// ==========================================================================
// Option handling with getop_long (stdlib)

static int verbosity = 0;

// number of K-best to compute   //was #define TEST_K 1
static size_t _k = 1;

// file name for reading input segment
static string _input_file;
// input file is text or midi
static bool f_input = false;
// input file is midi
static bool f_midi = false;
// input segment
static InputSegment* _iseg;

// ignore rests in input midi file
static int f_norest = false;

// start date option is present
static int f_start = false;
// argument of start date option
static double d_start;

// end date option is present
static int f_end = false;
// argument of end date option
static double d_end;

// output file option is present
static bool f_output = false;
// text file name for writing
static string _output_file;

// file name for reading schema
static string _schema_file;
// schema file is present
static bool f_schema = false;
// input schema
static Schema* _schema;

// flag: clean WTA
static int f_clean = false;

// flag: number of bars specified (otherwise default = 1)
static int f_bars = false;
// number of bars in input
static size_t _bars = 1;

// flag: pre value
static int f_pre = false;
// argument of pre option
static size_t _pre = 0;

// flag: post value
static int f_post = false;
// argument of post option
static size_t _post = 0;

// weight type for WTA
static int f_penalty = false;
static int f_count = false;
static int f_proba = false;

static struct option long_options[] =
{
    // Options with an abbreviation
    // we dont use flags here, we set them after calling getopt instead
    
    /*  0 */ { "help",        no_argument,       NULL, 'h' },
    /*  1 */ { "version",     no_argument,       NULL, 'V' },
    
    /*  2 */ { "verbosity",   required_argument, NULL, 'v' },
    /*  3 */ { "debug",       no_argument,       NULL, 'd' },
    /*  4 */ { "trace",       no_argument,       NULL, 't' },
    /*  5 */ { "quiet",       no_argument,       NULL, 'q' },
    
    /*  6 */ { "kbest",       required_argument, NULL, 'k' },
    
    /*  7 */ { "input",       required_argument, NULL, 'i' },
    /*  8 */ { "midi",        required_argument, NULL, 'm' },
    /*  9 */ { "schema",      required_argument, NULL, 'a' },
    /* 10 */ { "output",      required_argument, NULL, 'o' },

    // Options with no abbreviation
    
    /* 11 */ { "penalty",     no_argument,       &f_penalty, true},
    /* 12 */ { "counting",    no_argument,       &f_count,   true },
    /* 13 */ { "proba",       no_argument,       &f_proba,   true },
    /* 14 */ { "probability", no_argument,       &f_proba,   true },
    /* 15 */ { "stochastic",  no_argument,       &f_proba,   true },
    /* 16 */ { "clean",       no_argument,       &f_clean,   true },
    /* 17 */ { "norest",      no_argument,       &f_norest,  true },
    /* 18 */ { "start",       required_argument, &f_start,   true },
    /* 19 */ { "end",         required_argument, &f_end,     true },
    /* 20 */ { "bars",        required_argument, &f_bars,    true },
    /* 21 */ { "pre",         required_argument, &f_pre,     true },
    /* 22 */ { "post",        required_argument, &f_post,    true },
    /* last */ { NULL, 0, NULL, 0 }
};

double duration(clock_t start)
{
    return ((double)(clock() - start)/CLOCKS_PER_SEC * 1000);
}



// ==========================================================================
// Definition of demo

//#include "Inputless1best.hpp"
//#include "InputlessKbest.hpp"
//#include "1bar1bestSIP.hpp"
//#include "1barKbestSIP.hpp"
#include "Multibar1bestSIPBU.hpp"


// ==========================================================================
// Option handling with getop_long (stdlib)

#define check_index_is(NAME) \
assert(0 == strcmp(long_options[option_index].name, NAME))

static void setOptionArgs(int option_index)
{
    TRACE("setOptionArgs: {}", option_index);
    
    // long options without abbreviation and with argument
    switch(option_index)
    {
        case 18:
            check_index_is("start");
            assert (f_start);
            d_start = strtod(optarg, NULL);
            TRACE("setOptionArgs {}: {}", long_options[option_index].name, d_start);
            break;
            
        case 19:
            check_index_is("end");
            assert (f_end);
            d_end = strtod(optarg, NULL);
            TRACE("setOptionArgs {}: {}", long_options[option_index].name, d_end);
            break;
            
        case 20:
            check_index_is("bars");
            assert (f_bars);
            _bars = strtol(optarg, NULL, 10);
            TRACE("setOptionArgs {}: {}",
                  long_options[option_index].name, _bars);
            break;
            
        case 21:
            check_index_is("pre");
            assert (f_pre);
            _pre = strtol(optarg, NULL, 10);
            TRACE("setOptionArgs {}: {}", long_options[option_index].name, _pre);
            break;
            
        case 22:
            check_index_is("post");
            assert (f_post);
            _post = strtol(optarg, NULL, 10);
            TRACE("setOptionArgs {}: {}",
                  long_options[option_index].name,
                  _post);
            break;

            
        default:
            ; // ignore
    }
}

bool checkOptions()
{
    bool error = false;
    
    if (f_penalty || f_count || f_proba)
    {
        if ((f_penalty && f_count) || ((f_penalty || f_count) == f_proba))
        {
            ERROR("options conflict: more than one schema file type");
            error = true;
        }
    }
    else
    {
        ERROR("missing option: schema file type");
        error = true;
    }
    
    if (f_input == f_midi)
    {
        TRACE("f_input={}, f_midi={}", f_input, f_midi);
        if (f_midi)
        {
            ERROR("options conflict: 2 input files");
            error = true;
        }
        else
        {
            ERROR("missing option: input file");
            error = true;
        }
    }
    
    if (! f_schema)
    {
        ERROR("missing option: schema file");
        error = true;
    }
    
    if (f_start && f_end)
    {
        if (d_end <= d_start)
        {
            ERROR("start date must be before end date");
            error = true;
        }
    }
    
    return error;
}

template<class P>
void print_best(const P& p,
                Run<P>* r,
                WTA* schema,
                Atable<P>* cont)
{
    assert (cont);
    assert (p.complete());
    assert(schema);
    assert (r);
    assert (! r->null());
    assert(p.has_S());

    // one-bar run
    if (isWTA(p.state()))
    {
        print_tree(p, r, schema, cont);
        return;
    }

    // multiple bars run, print each bar
    assert(isMeta(p.state()));

    bool vazy = true;
    Run<P>* rtail = r;
    Run<P>* rhead = NULL;

    while (vazy)
    {
        assert(rtail);
        assert(rtail->inner());
        assert(rtail->arity() == 2);
        
        const P& phead = (*rtail)[0];
        assert(phead.has_S());
        assert(isWTA(phead.state()));
        rhead = cont->best(phead);
        assert(rhead);
        print_tree(phead, rhead, schema, cont);
        std::cout << " | ";

        const P& ptail = (*rtail)[1];
        assert(ptail.has_S());
        assert(isMeta(ptail.state()));
        rtail = cont->best(ptail);

        vazy = ptail.divisible();
    }
}


#if QP_SCHEMA == SCHEMA_COMBO
ComboWTA* build_combowta(const InputSegment& seg, const WTA& schema)
{
    clock_t time_start;

    // option for single bar processing
    assert(_bars == 1); // default value for option

    INFO("==== Construction ComboWTA from schema and input");
    time_start = clock();
    
    // bloc number = 0
    // pre is given as option (default 0)
    ComboWTA* combowta = new ComboWTA(seg, 0, schema, _pre);
    INFO("ComboWTA:");
    if (verbosity >= 4) // info
    {
        std::cout << *combowta;
        combowta->print(std::cout);
    }
    INFO("time to compute ComboWTA : {}ms", duration(time_start));
    
    return combowta;
}
#endif


#if QP_SCHEMA == SCHEMA_PRE
PreWTA* build_prewta(const WTA& schema)
{
    clock_t time_start;
    
    INFO("==== Construction PreWTA from schema");
    
    time_start = clock();
    PreWTA* prewta = new PreWTA(schema);
    INFO("PreWTA:");
    if (verbosity >= 4) // info
    {
        std::cout << *prewta;
        prewta->print(std::cout);
    }
    INFO("time to compute PreWTA : {}ms", duration(time_start));
    
    return prewta;
}
#endif


void usage()
{
    INFO("Usage: quant [options...]");
    INFO("  -help -h");
    INFO("  -version -V");
    INFO("  -verbosity level -v level : level=0..6  (default is 0)");
    INFO("             levels: 0=trace, 1=debug, 2=info, 3=warn, \
         4=err,   5=critical, 6=off");
    INFO("  -trace -t : same as -verbosity 6");
    INFO("  -debug -d : same as -verbosity 5");
    INFO("  -quiet -q : same as -verbosity 0");
    INFO("  -kbest N -k N : compute the N best trees");
    INFO("input files:");
    INFO("  -input filename -i filename : \
filename is either a text file or midifile \
(characterized by extension .mid or .midi)");
    INFO("  -midi midifilename -m midifilename : filename is a midi file");
    INFO("  -schema filename -a filename : \
         filename contains the text description of a WTA");
    INFO("  -output filename -o filename : output score file");
    INFO("  ");
    INFO("schema weight model (mutually exclusive options):");
    INFO("  -counting");
    INFO("  -penalty");
    INFO("  -probability -stochastic");
    INFO("  ");
    INFO("  -clean  : clean input schema");
    INFO("  -norest  : ignore rests in input midifile");
    INFO("  -start D : set start date for input segment");
    INFO("  -end D   : set end date for input segment");
    INFO("  -pre I : number of notes added (as grace notes)\
at the begining of the segment to quantize,\
e.g. originating from another precedent segment");
    INFO("  -post I : number of notes that will be aligned to right bound\
in quantization solutions");
    INFO("   -bar I : number of bars in the input segment (default 1)");
    INFO(" ");
    INFO("-input and -midi are mutualy exclusive");
    INFO("-probability -stochastic are equivalent");
    INFO("-counting -penalty -probability -stochastic are mutualy exclusive");
    INFO("-trace has priority over -verbosity");
}

void version()
{
    INFO("meiutil: version 0.1");
}













int main(int argc, char** argv)
{
    clock_t time_start;     // chrono
    //clock_t time_end;
    
    // set for tracing the parsing of options
    //
    // log levels:
    // trace = 0,
    // debug = 1,
    // info = 2,
    // warn = 3,
    // err = 4,
    // critical = 5,
    // off = 6
    spd::set_level(spd::level::debug);
    
    // index of the current option
    //int index = 0;
    
    
    //
    // read command line options
    //
    
    // character of option
    int c;
    
    while(1)
    {
        /* getopt_long stores the option index here. */
        int option_index = 0;
        
        c = getopt_long_only(argc, argv, "hVv:dtk:i:m:a:",
                             long_options, &option_index);
        
        
        // end of the options
        if (c == -1)
            break;
        
        switch (c)
        {
                // long option with flag pointer
            case 0:
                setOptionArgs(option_index);
                break;
                
            case 'h':
                usage();
                exit(0);
                
            case 'V':
                version();
                exit(0);
                
            case 'v':
                if (optarg)
                    //verbosity = strtol(optarg, NULL, 10);
                    verbosity = atoi(optarg);
                else
                    verbosity = 4;
                TRACE("Option {}: {}", long_options[option_index].name,
                      verbosity);
                break;
                
            case 'd':
                verbosity = 5;
                TRACE("Option {}: verbosity={}",
                      long_options[option_index].name,
                      verbosity);
                break;
                
            case 't':
                verbosity = 6;
                TRACE("Option {}: verbosity={}",
                      long_options[option_index].name,
                      verbosity);
                break;
                
            case 'q':
                verbosity = 0;
                TRACE("Option {}: verbosity={}",
                      long_options[option_index].name,
                      verbosity);
                break;
                
            case 'k':
                //k = strtoul(optarg, NULL, 10);
                _k = atoi(optarg);
                TRACE("Option {} : {}",
                      long_options[option_index].name, _k);
                if (_k == 0)
                {
                    ERROR("option error: k={} must be > 0", _k);
                    exit(1);
                }
                break;
                
            case 'i':
                f_input = true;
                _input_file = std::string(optarg);
                TRACE("Option {}: {}",
                      long_options[option_index].name, _input_file);
                break;
                
            case 'o':
                f_output = true;
                _output_file = std::string(optarg);
                TRACE("Option {}: {}", long_options[option_index].name,
                      _output_file);
                break;
                
            case 'm':
                f_midi = true;
                _input_file = std::string(optarg);
                TRACE("Option {}: {}",
                      long_options[option_index].name, _input_file);
                break;
                
            case 'a':
                f_schema = true;
                _schema_file = std::string(optarg);
                TRACE("Option {}: {}",
                      long_options[option_index].name, _schema_file);
                break;
                
                //  missing option argument. Error already signaled by getopt
            case ':':
                ERROR("Missing option argument");
                break;
                
                // unknown or ambiguous option  Error already signaled by getopt
            case '?':
                WARN("unkown or ambiguous option argument");
                break;
                
                // Do nothing for the options that are not flag-managed but without arguments
            default:
                break;
        }
    }
    
    
    if (checkOptions())
    {
        ERROR("option error. exit");
        std::cerr << "option error. exit";
        std::exit(1);
    }
    
    INFO("verbosity level = {}", verbosity);
    switch (verbosity)
    {
        case 0: spd::set_level(spd::level::off); break;
        case 1: spd::set_level(spd::level::critical); break;
        case 2: spd::set_level(spd::level::err); break;
        case 3: spd::set_level(spd::level::warn); break;
        case 4: spd::set_level(spd::level::info); break;
        case 5: spd::set_level(spd::level::debug); break;
        case 6: spd::set_level(spd::level::trace); break;
        default: spd::set_level(spd::level::trace);
    }
    
#if   QP_WEIGHT == WEIGHT_STOCHASTIC
    INFO("stochastic weight model");
#elif QP_WEIGHT == WEIGHT_PENALTY
    INFO("penalty weight model (alpha={})", CST_ALPHA);
#else
#error ERROR unknown weight model
#endif
    
    INFO("input file: {}", _input_file);
    INFO("schema file: {} ({} weight model)",
         _schema_file,
         (f_count?"counting":(f_proba?"stochastic":(f_penalty?"penalty":"???"))));
    INFO("output file: {}", _output_file);
    
    // initialize running environment from
    // WTA file
    // TODO: remove normalize flag
    // was read_schema(schema_file, f_clean, true);

    // input schema (global)
    // read from file
    _schema = new Schema(_schema_file,
                         f_count,
                         f_penalty,
                         f_proba);
    
    // TBC remove 0 weighted transitions
    if (f_clean)
    {
        INFO("Cleaning schema");
        _schema->clean();
    }
    
    // TBC normalization flag ?


    if (_schema == NULL)
    {
        ERROR("error importing schema, exit");
        exit (1);
    }
    else
    {
        INFO("Schema (after casting and cleaning):");
        if (verbosity >= 4) // info
        {
            std::cout << *(_schema);
            _schema->print(cout);
        }
    }
    
    // input segment (global)
    _iseg = NULL;

    if (f_input)
    {
        _iseg = new InputSegment(_input_file,
                                f_midi,
                                true, // mono
                                _bars,
                                f_norest);
    }
    
    if (f_start)
    {
        assert(_iseg);
        _iseg->set_begin(d_start);
    }
    
    if (f_end)
    {
        assert(_iseg);
        _iseg->set_end(d_end);
    }
    
    if (_iseg->size() == 0)
    {
        ERROR("empty segment, stopping");
        exit(2);
    }
    else if (verbosity >= 4) // info
    {
        INFO("segment : {} events", _iseg->size());
        _iseg->print(std::cout);
    }

    //Environment* env = new Environment(_schema, _iseg);
    // resolution is in environment


    // TBC
    // cf. demo file "Multibar1bestSIPBU.hpp"
    
    demo();
    
    std::vector<const PointedRhythmTree*> quantizedbars =
    std::vector<const PointedRhythmTree*>(); // result of parsing
    
    // 1-best parsing
    Run<PtrType>* r = cont->best(pre, post, (bars == 1));
    
    // fill results vector with bars obtained by parsing
    if (r)
    {
        PtrType p(cont->environment, pre, post, (bars == 1));
        
        // one-bar run
        if (isWTA(p.state()))
        {
            RhythmTree* t = cont->bestTree(p);
            quantizedbars.push_back(new PointedRhythmTree(t, seg, 0));
            delete t;
        }
        // multiple bars run, print each bar
        else
        {
            assert(isMeta(p.state()));
            bool vazy = true;
            Run<PtrType>* rtail = r;
            Run<PtrType>* rhead = NULL;
            size_t next = 0;
            
            while (vazy)
            {
                assert(rtail);
                assert(rtail->inner());
                assert(rtail->arity() == 2);
                const PtrType& phead = (*rtail)[0];
                assert(phead.has_S());
                assert(isWTA(phead.state()));
                rhead = cont->best(phead);
                assert(rhead);
                RhythmTree* t = cont->bestTree(phead);
                PointedRhythmTree* pt = new PointedRhythmTree(t, seg, next);
                quantizedbars.push_back(pt);
                delete t;
                
                next = pt->next();
                const PtrType& ptail = (*rtail)[1];
                assert(ptail.has_S());
                assert(isMeta(ptail.state()));
                rtail = cont->best(ptail);
                vazy = ptail.divisible();
            }
        }
    }
    
    
    // score construction

    cout << "Create the symbolic score from the best tree" << std::endl;

    ScoreModel::ScoreMeter sMeter = ScoreModel::ScoreMeter(2, 4);
    ScoreModel::Score myScore = ScoreModel::Score("test", sMeter);
    ScoreModel::Voice* voice = myScore.getVoice("test", "test");

	for (const PointedRhythmTree* pointedRT : quantizedbars) {
	    // First measure
		ScoreModel::Measure measure = myScore.addMeasure();
		cout << endl <<  "Found a pointedRTree. Add measure  " << measure.getId() << endl;
	   voice->addFromRhythmTree(measure, pointedRT, measure.getDuration());
	}


   cout << std::endl << "Serialize in MEI and store in" << _output_file << std::endl;

    MEI meiProcessor = MEI();
    meiProcessor.createFromScore(myScore);
    meiProcessor.writeInFile(_output_file);

    if (_schema)
        delete _schema;
    
    if (_iseg)
        delete _iseg;
    
    exit(0);
}



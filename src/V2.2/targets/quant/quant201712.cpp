//
//  test_rearchi.cpp
//  qparse
//
//  Created by Florent Jacquemard on 12/06/17.
//  Copyright © 2017 Florent Jacquemard. All rights reserved.
//

#include <stdio.h>
#include <time.h>
#include <iostream>
#include <string>
#include <vector>

#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>     // handling commandline options
#include <assert.h>

#include "constant.h"

#include "FloatWeight.hpp"
#include "TropicalWeight.hpp"
#include "ViterbiWeight.hpp"
#include "WTA.hpp"
#if QP_TESTSCHEMA == TESTSCHEMA_PRE
#include "PreWTA.hpp"
#endif
#if QP_TESTSCHEMA == TESTSCHEMA_COMBO
#include "ComboWTA.hpp"
#endif
#include "DurationList.hpp"
#include "Stable.hpp"
#include "SItable.hpp"
#include "SIPtable.hpp"
#include "SKtable.hpp"
#include "SKItable.hpp"
#include "SKItree.hpp"
#include "SKIPtable.hpp"
#include "RT.hpp"



// ==========================================================================
// Option handling with getop_long (stdlib)

static int verbosity = 0;

// number of K-best to compute   //was #define TEST_K 1
static size_t k = 1;

// TODO set as options
// toplevel PRE value #define TEST_PRE    0
static const size_t pre = 0;

// toplevel POST value #define TEST_POST   0
static const size_t post = 0;

// file name for reading input segment
static string input_file;
// input file is text or midi
static bool f_input = false;
// input file is midi
static bool f_midi = false;

// ignore rests in input midi file
static int f_norest = false;

// start date option is present
static int f_start = false;
// argument of start date option
static double d_start;

// end date option is present
static int f_end = false;
// argument of end date option
static double d_end;

// file name for reading schema
static string schema_file;
// schema file is present
static bool f_schema = false;

static int f_clean = false;
static int f_penalty = false;
static int f_count = false;
static int f_proba = false;

static struct option long_options[] =
{
    // Options with an abbreviation
    // we dont use flags here, we set them after calling getopt instead
    
    /*  0 */ { "help",        no_argument,       NULL, 'h' },
    /*  1 */ { "version",     no_argument,       NULL, 'V' },
    
    /*  2 */ { "verbosity",   required_argument, NULL, 'v' },
    /*  3 */ { "debug",       no_argument,       NULL, 'd' },
    /*  4 */ { "trace",       no_argument,       NULL, 't' },
    /*  5 */ { "quiet",       no_argument,       NULL, 'q' },

    /*  6 */ { "kbest",       required_argument, NULL, 'k' },
    
    /*  7 */ { "input",       required_argument, NULL, 'i' },
    /*  8 */ { "midi",        required_argument, NULL, 'm' },
    /*  9 */ { "schema",      required_argument, NULL, 'a' },
    
    // Options with no abbreviation
    
    /* 10 */ { "penalty",     no_argument,       &f_penalty, true},
    /* 11 */ { "counting",    no_argument,       &f_count,   true },
    /* 12 */ { "proba",       no_argument,       &f_proba,   true },
    /* 13 */ { "probability", no_argument,       &f_proba,   true },
    /* 14 */ { "stochastic",  no_argument,       &f_proba,   true },
    /* 15 */ { "clean",       no_argument,       &f_clean,   true },
    /* 16 */ { "norest",      no_argument,       &f_norest,  true },
    /* 17 */ { "start",       required_argument, &f_start,   true },
    /* 18 */ { "end",         required_argument, &f_end,     true },
    
    // last
    { NULL, 0, NULL, 0 }
};

#define check_index_is(NAME) \
assert(0 == strcmp(long_options[option_index].name, NAME))

static void setOptionArgs(int option_index)
{
    TRACE("setOptionArgs: {}", option_index);

    // long options without abbreviation and with argument
    switch(option_index)
    {
        case 17:
            check_index_is("start");
            assert (f_start);
            d_start = strtod(optarg, NULL);
            TRACE("setOptionArgs {}: {}", long_options[option_index].name, d_start);
            break;
            
        case 18:
            check_index_is("end");
            assert (f_end);
            d_end = strtod(optarg, NULL);
            TRACE("setOptionArgs {}: {}", long_options[option_index].name, d_end);
            break;

            
        default:
            ; // ignore
    }
}

bool checkOptions()
{
    bool error = false;
    
    if (f_penalty || f_count || f_proba)
    {
        if ((f_penalty && f_count) || ((f_penalty || f_count) == f_proba))
        {
            ERROR("options conflict: more than one schema file type");
            error = true;
        }
    }
    else
    {
        ERROR("missing option: schema file type");
        error = true;
    }
    
    if (f_input == f_midi)
    {
        TRACE("f_input={}, f_midi={}", f_input, f_midi);
        if (f_midi)
        {
            ERROR("options conflict: 2 input files");
            error = true;
        }
        else
        {
            ERROR("missing option: input file");
            error = true;
        }
    }
    
    if (! f_schema)
    {
        ERROR("missing option: schema file");
        error = true;
    }
    
    if (f_start && f_end)
    {
        if (d_end <= d_start)
        {
            ERROR("start date must be before end date");
            error = true;
        }
    }
    
    return error;
}


double duration(clock_t start)
{
    return ((double)(clock() - start)/CLOCKS_PER_SEC * 1000);
}


void print_best(Run* r,
                WTA* schema,
                Acontainer* cont,
                SKpointer* p,
                size_t cpre=0)
{
    assert (cont);
    assert (p);
    assert(schema);
    
    //Run r = cont->best(p, cpre);
//    Run* r = p->best(cont, cpre);
    
    assert (r);
    if (! r->unknown())
    {
        RhythmTree* t = p->newRhythmTree(cont, cpre);
        assert (t);
        Weight cpty = schema->eval(*t);
#if QP_WEIGHT == WEIGHT_STOCHASTIC
        INFO("{}-best: {} (weight={}, cpty={}, dist={}) {} {}",
             p->rank(),
             t->to_string(),
             r.weight,
             cpty,
             (r.weight.norm()/cpty.norm()),
             r.duration,
             (t->reducible())?"REDUCIBLE\n":"\n");
#elif QP_WEIGHT == WEIGHT_PENALTY
        INFO("{}-best: {} (weight={}, cpty={}, dist={},) {} {}",
             p->rank(),
             t->to_string(),
             r->weight,
             cpty,
             ((r->weight.norm() - (1.0 - CST_ALPHA) * cpty.norm())/CST_ALPHA),
             r->duration,
             (t->reducible())?"REDUCIBLE\n":"\n");
#else
#error ERROR unknown weight model
#endif
        delete t;
    }
    else
    {
        // INFO ("{}-best for {}: none", p->rank(), p->state());
    }
}


WTA* read_schema(const string filename,
                 bool clean_flag=true,
                 bool norm_flag=true)
{
    clock_t time_start;

    INFO("==== Read schema WTA from {}", filename);
    Weight seed;
    
    if (f_penalty)
        seed = TropicalWeight::make_zero();
    else if (f_count)
        seed = FloatWeight::make_zero();
    else if (f_proba)
        seed = ViterbiWeight::make_zero();

    assert(! seed.unknown());
    
    WTA* schema = new WTA(seed, filename);
    std::cout << *schema;
    schema->print(cout);

    // TBC remove 0 weighted transitions
    if (clean_flag)
    {
        INFO("==== Cleaning schema");
        time_start = clock();
        schema->clean();
        //time_end = clock();
        INFO("time to clean WTA : {} ms", duration(time_start));
        INFO("clean schema:");
        schema->print(cout);
    }
    
#if QP_WEIGHT == WEIGHT_STOCHASTIC
    assert(schema);
    if (f_penalty)
    {
        WARN("This version is compiled for stochastic weight models.");
        WARN("Casting weights from penalty into stochastic model, via counting model.");
        WARN("The outcome is not guaranteed.");
        TRACE("Casting weights from penalty into counting model.");
        schema->PenaltytoCounting();
        TRACE("Casting weights from counting into stochastic model.");
        schema->CountingtoStochastic();
        return schema;
    }
    else if (f_count)
    {
        WARN("Casting weights from counting into stochastic model.");
        schema->CountingtoStochastic();
        return schema;
    }
    else if (f_proba)
    {
        return schema;
    }
#elif QP_WEIGHT == WEIGHT_PENALTY
    if (f_penalty)
    {
        return schema;
    }
    else if (f_count)
    {
        WARN("Casting weights from counting into penalty model.");
        schema->CountingtoPenalty();
        return schema;
    }
    else if (f_proba)
    {
        ERROR("This version is compiled for penalty weight models.");
        ERROR("Impossible to cast weights from stochastic into penalty model. Abort");
        return NULL;
    }
#else
#error ERROR unknown weight model
#endif

    return schema;
}


size_t compute_resolution(WTA* schema)
{
    assert (schema);
    clock_t time_start;

    INFO("====  Compute Resolution:");
    time_start = clock();
    size_t res = schema->resolution();
    INFO("resolution = {}", res);
    INFO("time to compute resolution : {}ms", duration(time_start));

    return res;
}


InputSegment read_segment(const string filename, size_t res, bool norest)
{
    InputSegment seg;

    assert (res > 0);
    assert(filename.size() > 0);
    INFO("==== Read input segment from {} ", filename);
    if (norest)
        INFO("ignore rests");
    
    if (f_midi)
    {
        MidiFile  midifile;
        int status = midifile.read(filename);
        if (status == 0)
        {
            ERROR("Error in MIDI file: {}", filename);
        }
        else
        {
            seg = InputSegment(midifile, true, 1, 1, norest);
        }
    }
    else
    {
        assert(f_input);
        // monophonic, resolution=res, 1 bloc
        seg = InputSegment(filename, true, 1, norest);
    }

    seg.set_resolution(res);

    INFO("segment: size = {}", seg.size());
    INFO("segment :");
    seg.print(cout);
    
    return seg;
}

         
#if QP_TESTSCHEMA == TESTSCHEMA_COMBO
ComboWTA* build_combowta(const InputSegment& seg, const WTA& schema, size_t pre)
{
    clock_t time_start;

    INFO("==== Construction ComboWTA from schema and input");
    time_start = clock();
    
    // bloc number = 0, pre = 0
    ComboWTA* combowta = new ComboWTA(seg, 0, schema, 0);
    INFO("ComboWTA:");
    std::cout << *combowta;
    combowta->print(cout);
    INFO("time to compute ComboWTA : {}ms", duration(time_start));
    
    return combowta;
}
#endif

#if QP_TESTSCHEMA == TESTSCHEMA_PRE
PreWTA* build_prewta(const WTA& schema)
{
    clock_t time_start;

    INFO("==== Construction PreWTA from schema");
    
    time_start = clock();
    PreWTA* prewta = new PreWTA(schema);
    INFO("PreWTA:");
    std::cout << *prewta;
    prewta->print(cout);
    INFO("time to compute PreWTA : {}ms", duration(time_start));
    
    return prewta;
}
#endif


void usage()
{
    INFO("Usage: quant [options...]");
    INFO("  -help -h");
    INFO("  -version -V");
    INFO("  -verbosity level -v level : level=0..6  (default is 0)");
    INFO("             levels: 6=trace, 5=debug, 4=info, 3=warn, \
0=err,   1=critical, 0=off");
    INFO("  -trace -t : same as -verbosity 6");
    INFO("  -debug -d : same as -verbosity 5");
    INFO("  -quiet -q : same as -verbosity 0");
    INFO("  -kbest N -k N : compute the N best trees");
    INFO("input files:");
    INFO("  -input filename -i filename : \
         filename is either a text file or midifile \
         (characterized by extension .mid or .midi)");
    INFO("  -midi midifilename -m midifilename : filename is a midi file");
    INFO("  -schema filename -a filename : \
         filename contains the text description of a WTA");
    INFO("  -clean  : clean input schema");
    INFO("  -norest  : ignore rests in input midifile");
    INFO("  -start D : set start date for input segment");
    INFO("  -end D   : set end date for input segment");
    INFO("schema weight model (mutually exclusive options):");
    INFO("  -counting");
    INFO("  -penalty");
    INFO("  -probability --stochastic");
    INFO(" ");
    INFO("-input and -midi are mutualy exclusive");
    INFO("-probability -stochastic are equivalent");
    INFO("-counting -penalty -probability -stochastic are mutualy exclusive");
    INFO("-trace has priority over -verbosity");
}

void version()
{
    INFO("qparse: debug version");
}

















int main(int argc, char** argv)
{
    // set for tracing the parsing of options
    //
    // log levels:
    // trace = 0,
    // debug = 1,
    // info = 2,
    // warn = 3,
    // err = 4,
    // critical = 5,
    // off = 6
    spd::set_level(spd::level::debug);
    
    // index of the current option
    //int index = 0;
    
    // character of option
    int c;
    
    while(1)
    {
        /* getopt_long stores the option index here. */
        int option_index = 0;
        
        c = getopt_long_only(argc, argv, "hVv:dtk:i:m:a:",
                             long_options, &option_index);
    
        
        // end of the options
        if (c == -1)
            break;

        switch (c)
        {
            // long option with flag pointer
            case 0:
                setOptionArgs(option_index);
                break;
                
            case 'h':
                usage();
                exit(0);
                
            case 'V':
                version();
                exit(0);

            case 'v':
                if (optarg)
                    //verbosity = strtol(optarg, NULL, 10);
                    verbosity = atoi(optarg);
                else
                    verbosity = 4;
                TRACE("Option {}: {}", long_options[option_index].name,
                      verbosity);
                break;

            case 'd':
                verbosity = 5;
                TRACE("Option {}: verbosity={}",
                      long_options[option_index].name,
                      verbosity);
                break;

            case 't':
                verbosity = 6;
                TRACE("Option {}: verbosity={}",
                      long_options[option_index].name,
                      verbosity);
                break;

            case 'q':
                verbosity = 0;
                TRACE("Option {}: verbosity={}",
                      long_options[option_index].name,
                      verbosity);
                break;

            case 'k':
                //k = strtoul(optarg, NULL, 10);
                k = atoi(optarg);
                TRACE("Option {} : {}",
                      long_options[option_index].name, k);
                if (k == 0)
                {
                    ERROR("option error: k={} must be > 0", k);
                    exit(1);
                }
                break;
                
            case 'i':
                f_input = true;
                input_file = std::string(optarg);
                TRACE("Option {}: {}",
                      long_options[option_index].name, input_file);
                break;

            case 'm':
                f_midi = true;
                input_file = std::string(optarg);
                TRACE("Option {}: {}",
                      long_options[option_index].name, input_file);
                break;

            case 'a':
                f_schema = true;
                schema_file = std::string(optarg);
                TRACE("Option {}: {}",
                      long_options[option_index].name, schema_file);
                break;
                
            //  missing option argument. Error already signaled by getopt
            case ':':
                ERROR("Missing option argument");
                break;
                
            // unknown or ambiguous option  Error already signaled by getopt
            case '?':
                WARN("unkown or ambiguous option argument");
                break;
                
            // Do nothing for the options that are not flag-managed but without arguments
            default:
                break;
        }
    }
        
        
    if (checkOptions())
    {
        ERROR("option error. exit");
        std::cerr << "option error. exit";
        std::exit(1);
    }
    
    INFO("verbosity level = {}", verbosity);
    switch (verbosity)
    {
        case 0: spd::set_level(spd::level::off); break;
        case 1: spd::set_level(spd::level::critical); break;
        case 2: spd::set_level(spd::level::err); break;
        case 3: spd::set_level(spd::level::warn); break;
        case 4: spd::set_level(spd::level::info); break;
        case 5: spd::set_level(spd::level::debug); break;
        case 6: spd::set_level(spd::level::trace); break;
        default: spd::set_level(spd::level::trace);
    }
    
    
#if   QP_WEIGHT == WEIGHT_STOCHASTIC
    INFO("qparse quantization with stochastic weight model");
#elif QP_WEIGHT == WEIGHT_PENALTY
    INFO("qparse quantization with penalty weight model (alpha={})", CST_ALPHA);
#else
#error ERROR unknown weight model
#endif
    
    INFO("input file: {}", input_file);
    INFO("schema file: {} ({} weight model)",
          schema_file,
          (f_count?"counting":(f_proba?"stochastic":(f_penalty?"penalty":"???"))));
    
    clock_t time_start;
    //clock_t time_end;
    
    // WTA file
    // TODO: remove normalize flag
    WTA* schema = read_schema(schema_file, f_clean, true);
    if (schema == NULL)
    {
        ERROR("error importing schema, exit");
        exit (1);
    }
    else
    {
        INFO("Schema (after cleaning and casting):");
        std::cout << *schema;
        schema->print(cout);
    }
    
    // resolution
    size_t res = compute_resolution(schema);
    
    // read input segment
    InputSegment seg = read_segment(input_file, res, f_norest);
    
    if (seg.size() == 0)
    {
        ERROR("empty segment, stopping");
        exit(2);
    }

    if (f_start)
    {
        seg.set_begin(d_start);
    }
    
    if (f_end)
    {
        seg.set_end(d_end);
    }
    
    // ComboWTA construction
#if QP_TESTSCHEMA == TESTSCHEMA_COMBO
    ComboWTA* combowta = build_combowta(seg, *schema, 0);
#endif
    
    // PreWTA construction
#if QP_TESTSCHEMA == TESTSCHEMA_PRE
    size_t max_pre = 0;
    size_t max_post = MAX_GRACE;

    PreWTA* prewta = build_prewta(*schema);
#endif
    
#if   QP_WEIGHT == WEIGHT_STOCHASTIC
    auto weightOrdering = weightMax;
#elif QP_WEIGHT == WEIGHT_PENALTY
    auto weightOrdering = weightMin;
#else
#error ERROR unknown weight model
#endif
    
    
// 2 cases effective (in test)
#if QP_TESTSCHEMA == TESTSCHEMA_COMBO
    assert (combowta);
    assert (combowta->hasType("ComboWTA"));
    //    ComboWTA* combowta = dynamic_cast<ComboWTA*>(wta);
    Acontainer* cont;
    SKpointer* p;

    time_start = clock();
    
    // 1-best for comboWTA with Stable
    if ( k == 1 )
    {
        INFO("test Stable: {}-best for pre={} post={}", k, pre, post);
        // TODO move #if to definition of weightMax / Min in Run
        cont = new Stable(combowta, weightOrdering);
    }
    // k-best for comboWTA with SKtable
    else
    {
        INFO("test SKtable: {}-best for pre={} post={}", k, pre, post);
        cont = new SKtable(combowta, weightOrdering);
    }
    assert (cont);
    State init = combowta->initial(pre, post);

    for (size_t i = 1; i <= k; i++)
    {
        p = new SKpointer(init, i);
        //Run* r = p->best(cont, cpre);
        Run* r = cont->best(p, pre);
        print_best(r, schema, cont, p, pre);
        delete p;
    }
    
    INFO("time to compute {} bests with ComboWTA : {}ms", k, duration(time_start));
    
    delete cont;
#endif

    

// 2 cases not effective yet. must go debug.
#if QP_TESTSCHEMA == TESTSCHEMA_PRE
    assert (prewta);
    assert (prewta->hasType("PreWTA"));
    
    Acontainer* cont;
    SKIpointer* p;

    time_start = clock();

    // 1-best for preWTA with SItable
    if ( k == 1 )
    {
        INFO("test SItable: {}-best for pre={} post={}", k, pre, post);
        cont = new SItable(prewta, weightOrdering, seg);
    }
    // k-best for preWTA with SKItable
    else
    {
        INFO("test SKItable: {}-best for pre={} post={}", k, pre, post);
        cont = new SKItable(prewta, weightOrdering, seg);
        // TODO k-best for preWTA with SKItree
    }

    assert (cont);
    State init = prewta->initial(pre, post);
    
    AlignmentTree* root = cont->top();
    for (size_t i = 1; i <= k; i++)
    {
        p = new SKIpointer(init, i, root);
        test_best(cont, p, pre);
        delete p;
    }
    
    INFO("time to compute {} bests with PreWTA : {}ms", k, duration(time_start));
#endif

    
    
// 2 cases not effective yet. must go debug.
#if QP_TESTSCHEMA == TESTSCHEMA_WTA
    assert (schema);
    assert (schema->hasType("WTA"));
    
    SKIPpointer* p;
    State init = schema->initial(pre, post);

    time_start = clock();

   // 1-best for WTA with SIPtable
    if ( k == 1 )
    {
        INFO("test SIPtable: {}-best for pre={} post={}", k, pre, post);
        // TODO move #if to definition of weightMax / Min in Run
        SIPtable* table = new SIPtable(schema, weightOrdering, seg);
        AlignmentTree* root = table->top();
        
        p = new SKIPpointer(init, 1, root);
        Run* r = table->best(p, pre);
        print_best(r, schema, table, p, pre);
        delete p;
        delete table;
    }

    // k-best for WTA with SKIPtable
    else
    {
        INFO("test SKIPtable: {}-best for pre={} post={}", k, pre, post);
        SKIPtable* table = new SKIPtable(schema, weightOrdering, seg);
        AlignmentTree* root = table->top();

        for (size_t i = 1; i <= k; i++)
        {
            p = new SKIPpointer(init, i, root);
            Run* r = table->best(p, pre);
            print_best(r, schema, table, p, pre);
            delete p;
        }
        delete table;
    }
    
    INFO("time to compute {} bests with ComboWTA : {}ms", k, duration(time_start));
#endif
    
}

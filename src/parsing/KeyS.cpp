//
//  KeyS.cpp
//  squant
//
//  Created by Florent Jacquemard on 31/03/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include "KeyS.hpp"


namespace parsing{


// rank default to 1 at creation
KeyS::KeyS(state_t s):
_state(s)
{ }


KeyS::KeyS(state_t s, const ParsingEnv& env):
_state(s)
{ }


// successor key (fs or ns or instance)
KeyS::KeyS(const KeyS& k,
           RunInner<KeyS>& r,
           const ParsingEnv& env)
{
    assert(r.inner());

    // first child
    if (r.empty())
    {
        assert(r.partial());
        const Transition& t = r.origin();
        assert(t.inner());
        assert(t.size() > 0);
        _state = t.state(0);
    }
    // instanciate head (inner case)
    else if (r.complete())
    {
        _state = k._state; // simple copy
    }
    // next sibling
    else
    {
        assert(r.partial());
        size_t i = r.size(); // index of next sibling in r
        const Transition& t = r.origin();
        assert(t.inner());
        assert(0 < i);
        assert(i < t.size());
        _state = t.state(i);
    }
}


// copy
KeyS::KeyS(const KeyS& k):
_state(k._state)
{ }


KeyS::~KeyS()
{
    TRACE("delete KeyS");
}


KeyS& KeyS::operator= (const KeyS& k)
{
    if(this != &k)
    {
        _state = k._state;
    };
    return *this;
}


bool KeyS::operator==(const KeyS& k) const
{
    return (k._state == _state);
}


bool KeyS::operator!=(const KeyS& k) const
{
    return (! (*this == k));
}


// KeyS always complete
bool KeyS::instance(const KeyS& k) const
{
    return (k._state == _state);
}


bool KeyS::complete() const
{
    return true;
}


// std::stack<RunInner<KeyS>*>& si,
// std::stack<std::pair<const KeyS*, RunTerm*>>& st,
void KeyS::runs(StackInner<KeyS>& si,
                StackTerm<KeyS>& st,
                ParsingEnv& env) const
{
    assert(si.empty());
    assert(st.empty());
    RunFactory<KeyS>::runs(*this, si, st, env);
}


//std::shared_ptr<RunTerm> KeyS::failRun(const ParsingEnv& env) const
//{
//    return RunFactory<KeyS>::failRun(*this, env);
//}


// protected
void KeyS::RunsTerminal(const Transition& t,
                        StackTerm<KeyS>& st,
                        //std::stack<std::pair<const KeyS*, RunTerm*>>& st,
                        const ParsingEnv& env) const
{
    // RunTerm* r = new RunTerm(t, 0);
    std::shared_ptr<RunTerm> r = std::make_shared<RunTerm>(t, 0); // length = 0
    
    TRACE("NEW terminal run {} -> {} for {}", *r, *this, t);
    st.emplace(*this, r); // this key is complete
}


// protected
void KeyS::RunsInner(const Transition& t,
                     StackInner<KeyS>& si,
                     // std::stack<RunInner<KeyS>*>& si,
                     const ParsingEnv& env) const

{
    assert(State::isWTA(_state));
    // generic method
    RunFactory<KeyS>::RunInnermk(*this, t, si);
}


//void KeyS::runsWTA(std::stack<RunInner<KeyS>*>& si,
//                   std::stack<std::pair<KeyS, RunTerm*>>& st,
//                   const ParsingEnv& env) const
//{
//    assert(State::isWTA(_state)); // no treament of bars for this class of key
//    RunFactory<KeyS>::runsWTA(*this, si, st, env);
//}


//void KeyS::runsBar(std::stack<RunInner<KeyS>*>& si,
//                   std::stack<std::pair<KeyS, RunTerm*>>& st,
//                   ParsingEnv& env) const
//{
//    RunFactory<KeyS>::runsBar(*this, si, st, env);
//}


//const Transition& KeyS::barTransition0(ParsingEnv& env) const
//{
//    RunFactory<KeyS>::barTransition0(*this, env);
//}
//
//
//const Transition& KeyS::barTransition2(ParsingEnv& env) const
//{
//    RunFactory<KeyS>::barTransition2(*this, env);
//}


std::ostream& operator<<(std::ostream& o, const KeyS& k)
{
    o << KEY_LPAR << k._state << KEY_RPAR;
    return o;
}


} // end namespace parsing

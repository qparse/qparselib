//
//  Runey.cpp
//  squant
//
//  Created by Florent Jacquemard on 28/03/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include "Runey.hpp"
#include "Recordey.hpp"


namespace parsing{


//template<class K>
Run::Run(const Transition& tr):
_label(tr.label()),
_weight(tr.weight()), // copy
_filter(tr.label(), tr.size()),     // initial duration list
_initweight(tr.weight()),   // [opt]
_origin(tr)
{
    assert(! _weight.unknown());
}


//template<class K>
Run::Run(const Transition& tr, label_t a, const Weight& w):
_label(a),
_weight(w),
_filter(a, tr.size()),     // initial duration list
_initweight(w),   // [opt]
_origin(tr)
{
    assert(! w.unknown());
    assert(SymbLabel::arity(a) == tr.arity());
}


// for backward compatibility.
//template<class K>
//Run::Run(size_t a, label_t lab, const Weight& w):
//Run(lab, w)
//{
//    assert(a == SymbLabel::arity(lab));
//}


// copy and update weight
//template<class K>
//Run::Run(const Run& r, Weight w):
//_label(r._label),
//_weight(w),
//_filter(r._filter),
//_initweight(r._initweight), // [opt]
//_origin(r._origin)
//{
//    assert(! _weight.unknown());
//    _filter.reset(_label, _origin.size());
//}


// copy
//template<class K>
Run::Run(const Run& r):
_label(r._label),
_weight(r._weight),
_filter(r._filter),
_initweight(r._initweight), // [opt]
_origin(r._origin)
{
    assert(! _weight.unknown());
}


//template<class K>
Run::~Run()
{
    TRACE("delete Run <ABS> {}", *this);
    // delete weight;
}


//template<class K>
Run& Run::operator= (const Run& rhs)
{
    if(this != &rhs)
    {
        _label = rhs._label;
        _weight = rhs._weight;
        _filter = rhs._filter;
        _initweight = rhs._initweight;
    };
    
    return *this;
}


//template<class K>
bool Run::operator==(const Run& rhs) const
{
    if (_label != rhs._label)
        return false;
        
    if (_origin.id() != rhs._origin.id()) // id of transition
        return false;
    
    // if the runs are identical, their weights must be the same
    assert(_weight == rhs._weight);

    return true;
}


//template<class K>
size_t Run::arity() const
{
    return _origin.arity();
}


//bool Run::complete() const
//{
//    ERROR("Run (abstract class): complete() called (origin={})",
//          SymbLabel::to_string(_origin.label()));
//    return false;
//}


//template<class K>
bool Run::filtered() const
{
    assert(this->complete());
    return _filter.filter();
}


//template<class K>
bool Run::fail() const
{
    assert((! SymbLabel::fail(_label)) || (_origin.dummy()));
    return SymbLabel::fail(_label);
}


//template<class K>
void Run::weightReset()
{
    _weight = _initweight;    
}


//template<class K>
void Run::weightUnknown()
{
    _weight = Weight();
}


//template<class K>
void Run::weightUpdate(const Weight& w)
{
    assert(! _weight.unknown());
    assert(! w.unknown());
    _weight *= w;
}


//template<class K>
void Run::filterReset()
{
    _filter.reset(_label, _origin.size());
}


//template<class K>
void Run::filterUpdate(const RunFilter& f)
{
    _filter.update(f);
}


void Run::print(std::ostream& o) const
{
    SymbLabel::print(_label, o);
    o << " : " << _weight;
}


std::ostream& operator<<(std::ostream& o, const Run& r)
{
    r.print(o);
    return o;
}


} // end namespace parsing


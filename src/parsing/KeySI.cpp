//
//  KeySI.cpp
//  squanty
//
//  Created by Florent Jacquemard on 04/06/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include "KeySI.hpp"

namespace parsing{


// internal use
KeySI::KeySI(state_t s, IntervalTree* node, rtu_t len):
KeyS(s),
_barlen(len),
_interval(node)
{
    assert(node);
}


// top constructor
KeySI::KeySI(rtu_t len, const ParsingEnv& env, bool f_open):
KeyS(-1, env), // first bar
_barlen(len)
{
    assert(env.iheap);
    assert(env.segment);

    if (f_open)
        _interval = env.iheap->make(env.segment, MTU::UNBOUNDED, true);
    else
        _interval = env.iheap->make(env.segment, env.segment->mend(), true);
    // _interval is aligned
    assert(_interval);
    assert(_interval->aligned());
}


// successor key (fs or ns or instance)
KeySI::KeySI(const KeySI& k, RunInner<KeySI>& r, const ParsingEnv& env):
KeyS(k, (RunInner<KeyS>&) r, env),
_barlen(k._barlen) // k is the parent (fs, ns) or matcher (instanciate)
{
    assert(r.inner());
    const Transition& t = r.origin();
    assert(t.inner());
    assert(0 < t.size());
    assert(t.size() <= t.arity());
    assert((! SymbLabel::bar(t.label())) || (t.arity() == 2));
    assert((! SymbLabel::bar(t.label())) || (t.multiplicity(0) == 1));

    // first child, k is parent
    if (r.empty())
    {
        assert(r.partial());
        assert(_state == t.state(0)); // check downcast of KeyS
        init_firstchild(k, t, env);
    }
    // instanciate head (inner case)
    else if (r.complete())
    {
        assert(r.size() > 0);
        assert(r.size() == t.size());
        assert(_state == k._state); // check downcast of KeyS
        const KeySI& last = r.key(r.size()-1); // last sibling
        // instanciate k with last key in r
        init_instanciate(k, last, t, env);
    }
    // next sibling
    else
    {
        assert(r.partial());
        size_t i = r.size();  // index of next sibling in r
        assert(0 < i);        // there is at least one previous sibling
        assert(i < t.size()); // r partial
        assert(_state == t.state(i)); // check downcast of KeyS
        const KeySI& last = r.key(i-1); // last sibling
        init_nextsibling(k, last, t, i, env);
    }
}


void KeySI::init_firstchild(const KeySI& k,
                            const Transition& t,
                            const ParsingEnv& env)
{
    label_t a = t.label();
    size_t ar = t.arity();
    assert(env.segment);
    assert(env.iheap);
    assert(k._interval);
    size_t m = t.multiplicity(0);

    if (SymbLabel::tuple(a)) //  || SymbLabel::rest(a))
    {
        _interval = k._interval->subm(env.segment, env.iheap, ar, 1, m);
    }
    else if (SymbLabel::bar(a))
    {
        // rdur = _barlen, mdur=1
        assert(ar == 2);
        assert(m == 1);
        // first sibling in bar constructor (of length 1 bar)
        _interval = k._interval->split(env.segment, env.iheap,
                                       _barlen, MTU::ONE, 1);
    }
    else
    {
        ERROR("KeySI.firstChild: unexpected symbol in tr: _state -> {}", t);
    }
    /// @todo cases grace and fork
    assert(_interval);
    assert(_interval->aligned());
}


void KeySI::init_nextsibling(const KeySI& parent,
                             const KeySI& prev,
                             const Transition& t,
                             size_t i, // index of next
                             const ParsingEnv& env)
{
    assert(t.inner());
    assert(prev.complete());
    label_t a = t.label();
    size_t ar = t.arity();
    assert(env.segment);
    assert(env.iheap);
    size_t m = t.multiplicity(0);

    if (SymbLabel::tuple(a))
    {
        if (m == 1)  // (t.nomult(i))
        {
            assert(parent._interval);
            assert(i < ar);
            _interval = parent._interval->sub(env.segment, env.iheap, ar, i+1);
        }
        else
        {
            assert(prev._interval);
            assert(0 < i);
            Rational factor = Rational(m, t.multiplicity(i-1));
            _interval = prev._interval->nexti(env.segment, env.iheap, factor);
        }
    }
    else if (SymbLabel::bar(a))
    {
        // rdur = _barlen, mdur=1
        assert(ar == 2);
        assert(m == 1);
        // second (and last) sibling in bar constructor
        _interval = parent._interval->split(env.segment, env.iheap,
                                            _barlen, MTU::ONE, 2);
    }
    else
    {
        ERROR("KeySI.nextSibling: unexpected symbol in tr: _state -> {}", t);
    }
    /// @todo cases grace and fork
    assert(_interval);
    // assert(_interval->aligned());
}


void KeySI::init_instanciate(const KeySI& head,
                             const KeySI& last,
                             const Transition& t,
                             const ParsingEnv& env)
{
    assert(last.complete());
    _interval = head._interval;
    // ead is already complete in the case of KeySI
    // assert(this->complete());
}


//copy
KeySI::KeySI(const KeySI& k):
KeyS(k),
_barlen(k._barlen),
_interval(k._interval)
{
    assert(k._interval);
}


KeySI::~KeySI()
{
    TRACE("delete KeySI {}", *this);
    // interval tree is deleted with the Parsing Env.
}


KeySI& KeySI::operator= (const KeySI& p)
{
    if(this != &p)
    {
        KeyS::operator=(p); // _state = p._state;
        _interval  = p._interval;   // no clone
    };
    return *this;
}


bool KeySI::operator==(const KeySI& p) const
{
    return (KeyS::operator==(p) &&
            this->equal_interval(p));
//    return ((_state == p._state) &&
//            this->equal_interval(p));
}


bool KeySI::operator!=(const KeySI& k) const
{
    return (! (*this == k));
}
    

bool KeySI::equal_interval(const KeySI& p) const
{
    if ((_interval == NULL) && (p._interval == NULL))
        return true;
    
    if ((_interval == NULL) || (p._interval == NULL))
    {
        return false;
    }
    else // both _interval non null
    {
        return ((_interval->rbegin() == p._interval->rbegin()) &&
                (_interval->rend()   == p._interval->rend())   &&
                (_interval->mbegin() == p._interval->mbegin())  &&
                (_interval->mend()   == p._interval->mend()));
    }
    //  ( *((Interval*) _interval) == *((Interval*) p._interval)) &&
}


// always complete
bool KeySI::instance(const KeySI& p) const
{
    return (p == *this);
}


// always complete
bool KeySI::complete() const
{
    assert(_interval != NULL);
    return true;
}


size_t KeySI::size() const
{
    assert(_interval);
    assert(_interval->aligned());
    return _interval->size();
}


void KeySI::runs(StackInner<KeySI>& si,
                 StackTerm<KeySI>& st,
                 //std::stack<RunInner<KeySI>*>& si,
                 //std::stack<std::pair<const KeySI*, RunTerm*>>& st,
                 ParsingEnv& env) const
{
    RunFactory<KeySI>::runs(*this, si, st, env);
}


//void KeySI::runsWTA(std::stack<RunInner<KeySI>*>& si,
//                    std::stack<std::pair<KeySI, RunTerm*>>& st,
//                    ParsingEnv& env) const
//{
//    RunFactory<KeySI>::runsWTA(*this, si, st, env);
//}


//void KeySI::runsBar(std::stack<RunInner<KeySI>*>& si,
//                    std::stack<std::pair<KeySI, RunTerm*>>& st,
//                    ParsingEnv& env) const
//{
//    RunFactory<KeySI>::runsBar(*this, si, st, env);
//}


// not the default of RunFactory (consider interval)
void KeySI::RunsTerminal(const Transition& t,
                         StackTerm<KeySI>& st,
                         // std::stack<std::pair<const KeySI*, RunTerm*>>& st,
                         const ParsingEnv& env) const
{
    const label_t& a = t.label();
    
    assert(SymbLabel::terminal(a));
    assert(_interval);
    if (! _interval->aligned())
    {
        WARN("KeySI: unaligned interval {} for filtering {}",
             *(_interval), SymbLabel::to_string(a));
        assert(env.segment);
        _interval->align(env.segment);
    }

    if (SymbLabel::continuation(a) || SymbLabel::dot(a))
    {
        if ((_interval->lsize() == 0) && (_interval->rsize() == 0))
        {
            // 0 or -1 point to read in interval
            Weight w = terminalWeight(a, t, env);
            size_t len = 0;
            RunFactory<KeySI>::RunTermmk(*this, t, a, w, len, st);
        }
    }
    /// one single offset point aligned to left bound of interval
    /// @todo add case of 1 rest preceeded by grace notes (here 0 grace notes)
    else if (SymbLabel::rest(a))
    {
        if ((_interval->lsize() == 1) &&
            (_interval->lsize() == _interval->loffsets()) &&
            (_interval->rsize() == 0))
        {
            // 0 or -1 points to read
            Weight w = terminalWeight(a, t, env);
            size_t len = _interval->lsize();;
            RunFactory<KeySI>::RunTermmk(*this, t, a, w, len, st);
        }
    }
    /// all points aligned to left bound of interval
    /// grace rests are discounted
    else if (SymbLabel::note(a))
    {
        if ((SymbLabel::nbEvents(a) == _interval->lonsets()) &&
            (_interval->rsize() == 0))
        {
            // _interval->lsize() = gn + grace-rests + 1 notev or -1 points to read
            Weight w = terminalWeight(a, t, env);
            size_t len = _interval->lsize();
            RunFactory<KeySI>::RunTermmk(*this, t, a, w, len, st);
        }
    }
    /// double bar (end of score)
    else if (SymbLabel::bar(a) && (SymbLabel::arity(a) == 0))
    {
        // condition of termination : end of input segment reached
        if ((_interval->rduration() < _barlen) && (! _interval->inhabited()))
        {
            // 0 or -1 point to read in interval
            Weight w = terminalWeight(a, t, env);
            assert(! w.unknown());
            RunFactory<KeySI>::RunTermmk(*this, t, a, w, 0, st);
        }
    }
    else if (SymbLabel::dummy(a))
    {
        ERROR("KeySI: dummy symbol in tr.");
    }
    else
    {
        ERROR("KeySI: unexpected terminal symbol {}", SymbLabel::to_string(a));
    }    
}


// not the default of RunFactory (filter out not inhabited intervals)
void KeySI::RunsInner(const Transition& t,
                      StackInner<KeySI>& si,
                      // std::stack<RunInner<KeySI>*>& si,
                      const ParsingEnv& env) const
{
    const label_t& a = t.label();
    assert(SymbLabel::inner(a));
    
    if (SymbLabel::tuple(a))
    {
        assert(_interval);
        // we do not divide empty intervals
        if (_interval->inhabited())
            RunFactory<KeySI>::RunInnermk(*this, t, si);
    }
    else if (SymbLabel::grace(a))
    {
        assert(SymbLabel::arity(a) == 2);
        ERROR("KeySI: outside g.n. not supported, label={}",
              SymbLabel::to_string(a));
    }
    else if (SymbLabel::fork(a))
    {
        assert(SymbLabel::arity(a) == 2);
        ERROR("KeySI: voice fork not supported, label={}",
              SymbLabel::to_string(a));
    }
    else if (SymbLabel::bar(a))
    {
        assert(SymbLabel::arity(a) == 2);
        // end of input segment not reached (condition of termination)
        if ((_interval->rduration() >= _barlen) || _interval->inhabited())
            RunFactory<KeySI>::RunInnermk(*this, t, si);
    }
    else
    {
        ERROR("KeySI: unexpected inner symbol {}", SymbLabel::to_string(a));
    }
}


// not the default of RunFactory
std::shared_ptr<RunTerm> KeySI::failRun(const ParsingEnv& env) const
{
    assert(env.wta);
    const Transition& t0 = env.dummyTransition_zero();
    assert(t0.weight().is_zero());
    assert(_interval);
    if (! _interval->aligned())
    {
        assert(env.segment);
        _interval->align(env.segment);
    }
    label_t a = SymbLabel::make_fail(this->size());
    size_t length = _interval->size();
    std::shared_ptr<RunTerm> r =
        std::make_shared<RunTerm>(t0, a, t0.weight(), length);
    return r;
}


Weight KeySI::terminalWeight(label_t a,
                             const Transition& t,
                             const ParsingEnv& env) const
{
    assert(t.terminal());
    assert(SymbLabel::terminal(a));
    assert(this->complete());

    // tweight = transition weight = complexity
    Weight tweight = t.weight(); // copy
    assert(! tweight.unknown());

    // terminal bar interval:
    // there are no points hence no need to compute the distance
    if (SymbLabel::bar(t.label()) && (SymbLabel::arity(t.label()) == 0))
    {
        TRACE("KeySI: terminal bar interval: {} ({}aligned) for transition {}",
              *(_interval), ((! _interval->aligned())?"not ":""), t);
        assert(t.label() == a);
        assert(! _interval->inhabited());
        assert(_interval->rduration() < _barlen);
        //assert(env.segment);
        //_interval->align(env.segment);
    }
    // combine transition weight with distance weight
    // CST_WEIGHT_TYPE == WeightDom::STOCHASTIC
    else if (tweight.hasType("ViterbiWeight"))
    {
        assert(env.segment);
        assert(_interval);
        Weight dist = Weight(new GaussianDistance(env.segment, _interval));
        TRACE("KeySI: Viterbi distance for inter={} tr={} dist={}",
              *(this), t, dist);
        tweight *= dist;
    }

    // CST_WEIGHT_TYPE == WeightDom::PENALTY
    else if (tweight.hasType("TropicalWeight"))
    {
        assert(env.segment);
        assert(_interval);
        // compute distance to input segment
        Weight dist = Weight(new Distance(env.segment, _interval));
        TRACE("KeySI: tropical distance for inter={} tr={} dist={}",
              *(this), t, dist);
        // new weight = (1 - alpha) . former transition weight * alpha . distance
        // update: alpha applied to dist only (not to complexity)
        tweight *= dist; // tropical weight product is a sum
    }
    else
    {
        ERROR("KeySI.weight: unxepected weight type");
    }
    
    return tweight;
}


std::ostream& operator<<(std::ostream& o, const KeySI& p)
{
    if (p._interval == NULL)
    {
        o << KEY_LPAR << p.state() << KEY_RPAR;
        return o;
    }
    
    o << KEY_LPAR;
    o << " " << p.state() << " ";
    
    // short version
    if (p._interval)
    {
        o << *(p._interval);
    }
    else
    {
        o << "[] ";
    }
    o << KEY_RPAR;
    
    return o;
}


//void KeySI::runsBar(std::stack<RunInner<KeySI>*>& si,
//                    std::stack<std::pair<KeySI, RunTerm*>>& st,
//                    ParsingEnv& env) const
//{
//    assert(State::isMeta(_state));
//    assert(env.wta);
//    assert(env.pool);
//    const Transition* t[2] = { NULL, NULL};
//    if (env.pool->isRegistered(_state))
//    {
//        // we assume that there are exactly 1 nullary and 1 binary
//        // transitions targeting _state in env. pool.
//        for (Tlist<Transition>::const_iterator
//             i = env.pool->cbegin(_state);
//             i != env.pool->cend(_state); i++)
//        {
//            if (i->arity() == 0)
//            {
//                assert(t[0] == NULL);
//                t[0] = &(*i);
//            }
//            else if (i->arity() == 2)
//            {
//                assert(t[1] == NULL);
//                t[1] = &(*i);
//            }
//            else
//                ERROR("transition of unexpected arity {} in pool: {}",
//                      i->arity(), (*i));
//        }
//    }
//    else
//    {
//        t[0] = &(addBarTransition0(_state, env));
//        t[1] = &(addBarTransition2(_state, env));
//    }
//
//    // the arity of the transition in nthe stack will determine
//    // if we continue processing the rest of the segment or stop.
//
//    // remaining interval is smaller than a bar and not inhabited
//    // double bar mark (end of processing for bar sequence)
//    if ((_interval->rduration() < _barlen) && (! _interval->inhabited()))
//    {
//        assert(t[0]);
//        st.push(t[0]);
//    }
//    // remaining interval is larger than a bar:
//    // single bar mark (next bar)
//    else
//    {
//        assert(t[1]);
//        RunInner<KeySI>* r = new RunInner<KeySI>(*(t[1]));
//        si.push(r);
//    }
//}


//Transition& KeySI::addBarTransition0(state_t s, ParsingEnv& env) const
//{
//    assert(State::isMeta(s));
//    assert(env.pool);
//    const Weight tone = env.pool->weight_one();
//    Tlist<Transition>& tl = env.pool->add(s, (_state == -1));
//    tl.emplace(SymbLabel::make_bar(0), tone);
//    assert(! tl.empty());
//    Transition& t = tl.last();
//    TRACE("KeySI: add BAR0 transition to pool: {} -> {}", _state, t);
//    return t;
//}


//Transition& KeySI::addBarTransition2(state_t s, ParsingEnv& env) const
//{
//    assert(State::isMeta(s));
//    assert(env.pool);
//    const Weight tone = env.pool->weight_one();
//    Tlist<Transition>& tl = env.pool->add(s, (_state == -1));
//    tl.emplace(SymbLabel::make_bar(2), tone);
//    assert(! tl.empty());
//
//    Transition& t = tl.last();
//    assert(env.wta);
//    t.push(env.wta->initial());
//    t.push(_state - 1);
//    TRACE("KeySI: add BAR2 transition to pool: {} -> {}", _state, t);
//    return t;
//}



// if (DBL_APPROX_EQUAL(_interval->rend(), _interval->rbegin()))



} // end namespace parsing

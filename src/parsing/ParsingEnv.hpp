//
//  ParsingEnv.hpp
//  squant
//
//  Created by Florent Jacquemard on 06/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{

#ifndef ParsingEnv_hpp
#define ParsingEnv_hpp

#include <stdio.h>

#include "trace.hpp"
#include "InputEnv.hpp"
#include "Weight.hpp"
#include "Transition.hpp"
#include "SWTA.hpp"
#include "RunOrder.hpp"
#include "RunTerm.hpp"


namespace parsing{

/// @brief environment for a n-best parsing procedure.
/// It extends the elements of an Input Environment with two SWTAs:
/// - the base SWTA augmented for parsing,
/// - an auxiliary SWTA, initialy empty,
/// - the definition of an ordering for the numeration of runs.
class ParsingEnv : public InputEnv
{
public:

    /// @param seg an input segment. Can be NULL when not needed.
    /// @param wta base SWTA. Can be NULL when not needed.
    /// @param crescendo true if the runs will be enumerated from best to worst,
    ///                  false if the runs will be enumerated from worst to best.
    ParsingEnv(SWTA* wta = NULL,
               InputSegment* seg = NULL,
               bool crescendo = true);
    
    /// upgrade of input env
    /// @param env an input environment to be upgraded to parsing env.
    /// @param wta base SWTA. Can be NULL when not needed.
    /// @param crescendo true if the runs will be enumerated from best to worst,
    ///                  false if the runs will be enumerated from worst to best.
     ParsingEnv(const InputEnv& env,
                SWTA* wta = NULL,
                bool crescendo = true);

    /// copy
     ParsingEnv(const ParsingEnv& env);
    
    /// @brief delete the pool.
    /// @warning the base SWTA and segment must be delete externaly.
    virtual ~ParsingEnv();

    /// @brief return the 0 value in the weight domain of the SWTAs
    /// in this environment.
    /// shortcuts to members of the base SWTA.
    /// @warning the base SWTA must be set and non-NULL in this environment.
    Weight weight_zero() const;
    
    /// @brief return the 1 value in the weight domains
    /// shared by both SWTA of this environment.
    /// shortcuts to members of the base SWTA.
    /// @warning the base SWTA must be set and non-NULL in this environment.
    Weight weight_one() const;

    /// function to compare runs
    const RunOrder::Compare& compare() const;
   
    /// Constant run used for expressing parse failure.
    /// @warning the base SWTA must be set and non-NULL in this environment.
    std::shared_ptr<const Run> failrun() const;

    /// object containing the comparison function and failrun and generic weights
    inline const RunOrder& comparer() const { return runenum; }
    
    /// Transition with dummy label, empty body
    /// and weight zero of the domain of the wta of this environment.
    /// @warning the base SWTA must be set and non-NULL in this environment.
    const Transition& dummyTransition_zero() const;

    /// Transition of dummy label, empty body
    /// and weight one of the domain of the wta of this environment.
    /// @warning the base SWTA must be set and non-NULL in this environment.
    const Transition& dummyTransition_one() const;
    
private: // data
    
    /// dummy transition with weight zero in the domain of the wta of this environment.
    /// @warning the base SWTA must be set and non-NULL in this environment.
    const Transition _dt0;

    /// dummy transition with weight one in the domain of the wta of this environment.
    /// @warning the base SWTA must be set and non-NULL in this environment.
    const Transition _dt1;
    
    // last run in enumeration.
    // const RunTerm _failrun;

public: // data
    
    /// base SWTA
    SWTA* wta;
    
    /// auxiliary SWTA = pool of extra transitions for over-the-bars processing.
    /// initially empty.
    /// will always be empty if there is no base SWTA.
    SWTA* pool;
    
    /// enumeration ordering for the runs.
    const RunOrder runenum;

private: // functions

    /// whether the weight domain of the base WTA is a positive semiring or not.
    bool positive() const;

    //void init_pool();
    
    
};

} // end namespace parsing

#endif /* ParsingEnv_hpp */

/// @}



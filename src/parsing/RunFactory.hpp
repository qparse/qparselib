//
//  RunFactory.hpp
//  qparse
//
//  Created by Florent Jacquemard on 26/03/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{


#ifndef RunFactory_hpp
#define RunFactory_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <assert.h>
#include <stack>
//#include <queue>

#include "State.hpp"
#include "SymbLabel.hpp"
#include "Transition.hpp"
#include "SWTA.hpp"
#include "ParsingEnv.hpp"
#include "Runey.hpp"
#include "RunInner.hpp"
#include "RunTerm.hpp"
#include "RunStack.hpp"

namespace parsing{

/// methods for the generation of runs common to several key classes KeyX.
/// Every Run object represents an augmented transition built by extension of
/// - a transitios of a base WTA, for the parsing inside a bar, or
/// - a generic transition for parsing over the bars, built in this factory.
///
/// The form of the parse tree is the following:
/// ```
///        B2
///       /  \
///    m1     B2
///          /  \
///        m2    ...
///                B2
///               /  \
///             mk    B0
/// ```
/// where B0, B2 are nullary (final) and binary bar symbols
/// and m1,...,mk are the parse trees for measures 1 to k.
template<class K>
class RunFactory
{

public:

    // RunFactory(const K& k);
    
    /// @brief construct runs targeting complete instances of given key.
    /// @param k target key of the run.
    /// @param si a stack to be filled with inner runs. must be empty.
    /// @param st a stack to be filled with pairs containing terminal runs.
    /// must be empty.
    /// @param env parsing environment, containing a wta.
    // @return how many runs have been added to the stacks.
    /// @warning the runs constructed are added to the given stacks.
    /// - si will contain inner runs, partial or complete.
    /// - st will contains pairs made of
    ///     - a complete instance k' of k if k is partial, and NULL otherwise.
    ///     - a complete terminal run targeting r'
    /// All the runs in stacks must be deallocate elsewhere.
    static void runs(const K& k,
                     StackInner<K>& si,
                     StackTerm<K>& st,
                     ParsingEnv& env);
    
    /// add to the stacks runs corresponding to the extension
    /// of a base WTA transition from the given parsing environment.
    /// @param si a stack to be filled with inner runs.
    /// @param st a stack to be filled with pairs containing terminal runs.
    /// @param env parsing environment, containing a wta.
    static void runsWTA(const K& k,
                        StackInner<K>& si,
                        StackTerm<K>& st,
                        const ParsingEnv& env);
    
    /// construct base "over-the-bar transitions" (nullary and binary),
    /// extend them and add new associated runs to the stacks.
    static void runsBar(const K& k,
                        StackInner<K>& si,
                        StackTerm<K>& st,
                        ParsingEnv& env);
    
    /// generic method adding to the given stack one terminal run
    /// built from the params.
    /// @param k target of the run added. must be complete.
    /// @param t transition to build terminal runs. must be terminal and complete.
    /// @param lab given label. see RunTerm.
    /// @param w weight of run. must not be unknown. see RunTerm.
    /// @param len number of input points (events) parsed by this run.
    /// see RunTerm.
    /// @param st a stack to be filled with pairs containing terminal runs.
    /// @warning default case only for complete keys.
     static void RunTermmk(const K& k,
                           const Transition& t,
                           label_t lab,
                           const Weight& w,
                           size_t len,
                           StackTerm<K>& st);

    /// generic method adding to the given stack one inner runs built from the
    /// given transition.
    /// The run added is partial (empty), and is built from the transition
    /// solely (same label and weight as the transition).
    /// @param t transition to build inner runs. must be inner and complete.
    /// @param si a stack to be filled with inner runs.
    /// @warning the runs constructed are added to the given stack.
    /// They must be deallocate elsewhere.
    static void RunInnermk(const K& k,
                           const Transition& t,
                           StackInner<K>& si);

    /// unique outside-the-bar base transition or given arity
    /// targeting the embedded state.
    /// @param state a SWTA state, base state of a target key.
    /// @param arity arity of the transition. must be 0 or 2.
    /// @param env parsing environment. may be modified.
    static const Transition& barTransition(state_t state,
                                           size_t arity,
                                           ParsingEnv& env);

    
    /// unique nullary outside-the-bar base transition targeting the embedded state.
    /// @param env parsing environment. may be modified.
    /// @todo delete, replaced by barTransition
    static const Transition& barTransition0(state_t state, ParsingEnv& env);
    
    /// unique binary outside-the-bar base transition targeting the embedded state.
    /// @param env parsing environment. may be modified.
    /// @todo delete, replaced by barTransition
    static const Transition& barTransition2(state_t state, ParsingEnv& env);

    // allocate and return a special Run representing a parse failure for this key.
    // @param env parsing environment.
    // @return a pointer to a newly allocated FAIL run, with
    // - transition with dummy symbol, empty body, weight zero in the
    //   domain of the base wta in the passed environment.
    // - fail symbol
    // - weight zero (in the domain of the base wta).
    // This returned run will answer succesfully to Run.fail().
    // static std::shared_ptr<RunTerm> failRun(const ParsingEnv& env);

private:
    
//    const K& _key;
//    
//    inline const K& key() { return _key; }
//    inline const state_t state() { return _key.state(); }

};

} // end namespace parsing

// separated definition of template class
#include "RunFactory.tpp"

#endif /* RunFactory_hpp */

/// @}

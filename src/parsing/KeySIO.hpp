//
//  KeySIO.hpp
//  squant2
//
//  Created by Florent Jacquemard on 02/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{

#ifndef KeySIO_hpp
#define KeySIO_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <assert.h>
#include <utility>      // std::pair, std::make_pair

#include "util.hpp"
#include "hash.hpp"
#include "trace.hpp"
#include "RTU.hpp"
#include "MTU.hpp"
#include "KeySI.hpp"
#include "KeySIP.hpp"  // definition of pre_t etc
#include "Distance.hpp"
#include "GaussianDistance.hpp"
#include "IntervalTree.hpp"
#include "IntervalHeap.hpp"



namespace parsing{

/// type for pre post values in augmented states
/// same as KeySIP.hpp
//typedef long pre_t;
//static pre_t P_NULL = 0;
//static pre_t P_UNKNOWN = -1;
//static bool P_KNOWN(pre_t x) { return (x >= 0); }


/// extension of KeySI with values for dealing with rests (offset points).
/// It is made of:
/// components of KeySI:
/// - one state of a base WTA
/// - one interval (inherited attribute)
/// - rank for k-best algo (not used for 1-best)
/// new components:
/// - (pre) number of points in second half of interval of previous leaf
/// - (preon) number of onset points in second half of interval of previous leaf
/// - flag described below for second half of interval of previous leaf
/// - (post)  number of points in second half of this interval
///   can be unknown (making the KeySIO partial)
/// - (poston) number of onset points in second half of this interval
/// - flag described below for this interval.
///
/// The flag (called laston) for an interval is :
/// - true iff the last point in the interval is an onset
/// - false iff the last point in the interval is an offset or
///   the interval contains no point.
class KeySIO : public KeySI  // Key<KeySI>
{
    
    template<typename> friend class RunFactory;

public:
        
    /// @brief top constructor.
    /// allocate an initial key
    /// which will serve to create the other keys.
    /// @param barlen first estimated bar duration in rtu.
    /// @param env input environment.
    /// @param f_open flag true iff the initial MTU interval is right-open,
    /// if false, the right bound is set to the duration of input segment.
    /// @param pre intial pre value (for chainig with other segment).
    /// must not be P_UNKNOWN.
    KeySIO(rtu_t barlen, const ParsingEnv& env, bool f_open=true,
           pre_t pre        = P_NULL,
           pre_t preon      = P_NULL,
           bool  prelaston  = false);
//           pre_t post       = P_UNKNOWN,
//           pre_t poston     = P_UNKNOWN,
//           bool  postlaston = false);

    /// @brief constructor of a successor of a key to complete an inner run.
    /// @param k target key for the run. can be partial.
    /// @param r an inner run to be completed with the key constructed.
    /// @param env input environment.
    /// There are 4 cases of construction:
    /// - if r is partial and empty, construct the first child k0 to add to r.
    ///   k is the parent of k0 in this case.
    /// - if r is partial but not empty, construct the next sibling kn to add
    ///   to r. k is the parent of kn in this case.
    /// - if r is inner and complete, construct a complete instance of k
    ///   that will be the (complete) target of run r.
    KeySIO(const KeySIO& k, RunInner<KeySIO>& r, const ParsingEnv& env);
    
    /// @brief copy
    KeySIO(const KeySIO& k);
    
    /// destructor
    virtual ~KeySIO();

    /// @brief Key assignement.
    virtual KeySIO& operator= (const KeySIO& p);
        
    /// @brief for use as key in a unordered_multimap.
    virtual bool operator==(const KeySIO& p) const;
    virtual bool operator!=(const KeySIO& p) const;
    
    /// @brief return wether this Key is an instance of p.
    /// = equality in this case because KeyS are always complete.
    virtual bool instance(const KeySIO& p) const;

    /// @brief all attributes of the key are valued
    virtual bool complete() const;

    /// @todo revision needed
    /// the 8 following public members are specific to this K class
    /// PROBLEM FOR USING IN A TEMPLATE<K> like TableParse
    
    /// @brief accessor
    inline pre_t pre() const { return _pre; }

    /// @brief accessor
    inline pre_t pre_on() const { return _pre_on; }

    /// @brief accessor
    inline bool pre_laston() const { return _pre_laston; }

    /// @brief accessor
    inline pre_t post() const { return _post; }

    /// @brief accessor
    inline pre_t post_on() const { return _post_on; }

    /// @brief accessor
    inline bool post_laston() const { return _post_laston; }
    
    /// @brief construct (augmented) runs targeting complete instances of
    /// this key.
    /// @param si a stack to be filled with inner runs. must be empty.
    /// @param st a stack to be filled with pairs containing terminal runs.
    /// must be empty.
    /// @param env parsing environment, containing a wta.
    // @return how many runs have been added to the stacks.
    /// @warning the runs constructed are added to the given stacks.
    /// - si will contain inner runs, partial or complete.
    /// - st will contains pairs made of
    ///     - a complete instance k' of k if k is partial,
    ///       or k itself otherwise.
    ///     - a complete terminal run targeting r'
    /// All the runs in stacks must be deallocate elsewhere.
    void runs(StackInner<KeySIO>& si,
              StackTerm<KeySIO>& st,
              ParsingEnv& env) const;
    
    /// allocate and return a special Run representing a parse failure for this key.
    /// @param env parsing environment.
    /// @return a pointer to a newly allocated fail run, with
    /// - transition with dummy symbol, empty body, weight one in the
    ///   domain of the base wta in the passed environment.
    /// - fail symbol
    /// - weight one (in the domain of the base wta).
    /// This returned run will answer succesfully to Run.fail().
    // Run<KeySIO>* failRun(const ParsingEnv& env) const;
    
    /// @brief printer
    friend std::ostream& operator<<(std::ostream& o, const KeySIO& p);
    
protected:
    
    /// number of onset and offset points in the second half
    /// of the interval of the previous leaf.
    /// They will be aligned to the left bound of current interval, or ignored.
    pre_t _pre;

    /// number of onset points among the points described in _pre
    pre_t _pre_on;

    /// flag true if the last point in second half of the interval of
    /// the previous leaf is an onset, and
    /// false if the last point in second half is an offset or
    /// if the second half contains no point.
    bool _pre_laston;

    /// number of onset and offset points in the second half of current interval.
    /// They will be aligned to the right bound of current interval, or ignored.
    pre_t _post;

    /// number of offset points among the points described in _post
    pre_t _post_on;

    /// flag true if the last point in second half of the current interval
    /// is an onset, and false if the last point in second half is an offset or
    /// if the second half contains no point.
    bool _post_laston;

    /// @param k parent of this key
    /// @param t base wta transition to be augmented with the constructed key.
    /// @param env parsing environment, contain an input segment,
    /// and interval tree and heap.
    void init_firstchild(const KeySIO& k,
                         const Transition& t,
                         const ParsingEnv& env);
    
    /// @param parent parent of this key (target of the run it belongs to).
    /// @param prev previous sibling of this key (in the run it belongs to).
    /// @param t base wta transition to be augmented with the constructed key.
    /// @param i index of the next sibling in run (and transition)
    /// @param env parsing environment, contain an input segment,
    /// and interval tree and heap.
    void init_nextsibling(const KeySIO& parent,
                          const KeySIO& prev,
                          const Transition& t,
                          size_t i,
                          const ParsingEnv& env);
    
    /// @param head key to be instanciated (i.e. this key will be an instance of k).
    /// @param last complete key, last child of this key.
    /// @param t base wta transition to be augmented, with the constructed target key.
    /// @param env parsing environment, contain an input segment,
    /// and interval tree and heap.
    void init_instanciate(const KeySIO& head,
                          const KeySIO& last,
                          const Transition& t,
                          const ParsingEnv& env);
    
    /// @brief accessor
    virtual size_t size() const;
    
    /// add to the given stack some terminal runs built from the given transition.
    /// @param t transition to build terminal runs. must be terminal and complete.
    /// @param st a stack to be filled with pairs containing terminal runs.
    virtual void RunsTerminal(const Transition& t,
                              StackTerm<KeySIO>& st,
                              const ParsingEnv& env) const;

    /// add to the given stack some inner runs built from the given transition.
    /// @param t transition to build inner runs. must be inner and complete.
    /// @param si a stack to be filled with inner runs.
    /// @warning the runs constructed are added to the given stack.
    /// They can be partial or complete.
    /// They must be deallocate elsewhere.
    virtual void RunsInner(const Transition& t,
                           StackInner<KeySIO>& si,
                           const ParsingEnv& env) const;
    
    /// @brief weight for the case of a and tr terminal.
    virtual Weight terminalWeight(label_t a,
                                  const Transition& tr,
                                  const ParsingEnv& env) const;

private:
    
    /// @brief constructor for internal use
    KeySIO(state_t s, IntervalTree* node, rtu_t barlen,
           pre_t pre,  pre_t pre_on,   bool pre_laston,
           pre_t post, pre_t post_on,  bool post_laston);

    /// @brief copy and instanciate. (up-propagation of post values)
    /// for internal use.
    KeySIO(const KeySIO& k, pre_t post, pre_t post_on,  bool post_laston);
    
    /// check that the post values are as expected.
    /// @warning this key must be terminal.
    bool checkPost(const ParsingEnv& env) const;
    
    /// compute the laston flag for the interval of this key.
    /// @warning  the interval of this key must be aligned.
    bool laston(const ParsingEnv& env) const;
        
    /// add to the given stack one run built from the given transition,
    /// the given number of points and an weight computed from the env.
    /// this key is the target of the run built. it must be complete.
    // @param k key that the run is targeting. must be complete.
    /// @param n number of points in interval for the run added
    ///        = grace-notes + grace-rests + 1 note or -1.
    /// @param t transition to build terminal runs. must be terminal and complete.
    /// @param st a stack to be filled with pairs containing terminal runs.
    /// @param env parsing environment, contain an input segment,
    void addTerminalRun(//const KeySIO& k,
                        size_t n, // nb points
                        const Transition& t,
                        StackTerm<KeySIO>& st,
                        const ParsingEnv& env) const;

};


/// hash function for using as key in a table.
/// rank is ignoreds : same as SpointerHasher
struct KeySIOHasher
{
    std::size_t operator()(const KeySIO& k) const
    {
        using std::size_t;
        using std::hash;
        assert(k.interval());
        
        // see constant.h
        std::size_t h = HASH_SEED;
        h += hash<state_t>()(k.state());
        h = h * HASH_FACTOR + hash<rtu_t>()(k.interval()->rbegin());
        h = h * HASH_FACTOR + hash<rtu_t>()(k.interval()->rduration());
        h = h * HASH_FACTOR + hash<mtu_t>()(k.interval()->mbegin());
        h = h * HASH_FACTOR + hash<mtu_t>()(k.interval()->mduration());
        h = h * HASH_FACTOR + hash<pre_t>()(k.pre());
        h = h * HASH_FACTOR + hash<pre_t>()(k.pre_on());
        h = h * HASH_FACTOR + hash<bool>()(k.pre_laston());
        h = h * HASH_FACTOR + hash<pre_t>()(k.post());
        h = h * HASH_FACTOR + hash<pre_t>()(k.post_on());
        h = h * HASH_FACTOR + hash<bool>()(k.post_laston());
        return h;
    }
    
};

} // end namespace parsing


#endif /* KeySIO_hpp */

/// @}

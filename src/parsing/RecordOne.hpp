//
//  RecordOne.hpp
//  squanty
//
//  Created by Florent Jacquemard on 07/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{

#ifndef RecordOne_hpp
#define RecordOne_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <assert.h>

#include "trace.hpp"
#include "Recordey.hpp"


namespace parsing{

//template<class K, class R, class H> class TableParse;

/// @brief record (container of runs) associated to Key for one-best algorithms.
template<class K>
class RecordOne : public Record<K>
{
public:
    
    /// @brief empty record associated to given key.
    /// @param k key associated to the record
    /// @param comp function for comparing Runs over K.
    // @warning the key associated to the record must
    //          be set later to a non-NULL ptr with setKey.
    RecordOne(std::shared_ptr<const K> k,
              std::shared_ptr<const RunOrder> comp);
    
    /// copy
    RecordOne(const RecordOne&);
    
    /// deleting a RecordOne frees the best run contained.
    virtual ~RecordOne();
    
    /// @brief assignement.
    virtual RecordOne<K>& operator=(const RecordOne<K>& rec);
    
    virtual std::shared_ptr<Recordo> clone() const;
    
    /// @brief returns the 1-th best run of the record.
    /// @param n rank, must be 1 for RecordOne.
    /// @return the n-th best run of the record or, if there is none,
    /// the failRun set or NULL when the failRun was not set with setFailRun.
    virtual std::shared_ptr<const Run> best(size_t n=1);

    /// @brief returns the 1-th best run of the record.
    virtual bool empty() const;
    
    /// @brief add a run to the record or delete.
    /// Visitor of Runs dispatching over two Run types.
    /// @param r candidate run to add.
    /// @warning the key of this record must be set.
    /// @warning if r is not added, it is deleted.
    virtual size_t addTerm(std::shared_ptr<const RunTerm> r);
    virtual size_t addInner(std::shared_ptr<const RunInnero> r);
    virtual size_t addInner(std::shared_ptr<const RunInner<K>> r);

protected:
    
    /// best run for the associated state.
    std::shared_ptr<const Run> _best;
 
private:
    
    /// Common code for addTerm and addInner
    /// In this class of Recoreds, there is no distinction between the 2 types
    /// of Runs.
    size_t generic_add(std::shared_ptr<const Run> r);

};


} // end namespace parsing


// separated definition of template class
#include "RecordOne.tpp"


#endif /* RecordOne_hpp */

/// @}

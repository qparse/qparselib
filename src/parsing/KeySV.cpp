//
//  KeySV.cpp
//  equiv2
//
//  Created by Florent Jacquemard on 22/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#include "KeySV.hpp"

namespace parsing{


// internal use
//KeySV::KeySV(state_t s, const ValueList& seg, bool fs):
//KeyS(s),
//_list(seg), // copy
//_start(MTU::ZERO)
//_fs(fs)
//{ }


// top constructor
KeySV::KeySV(const ValueList& seg,
             const ParsingEnv& env):
KeyS(-1, env), // first bar
_list(seg),    // copy
_start(MTU::ZERO),
_fs(true)
{
    assert(env.iheap == NULL);
    assert(env.segment == NULL);
}


KeySV::KeySV(const KeySV& k,
             RunInner<KeySV>& r,
             const ParsingEnv& env):
KeyS(k, (RunInner<KeyS>&) r, env),
_list(),
_start(MTU::ZERO),
_fs(true)
{
    init(k, r, env);
}


void KeySV::init(const KeySV& k,
                 RunInner<KeySV>& r,
                 const ParsingEnv& env)
{
    assert(r.inner());
    const Transition& t = r.origin();
    assert(t.inner());
    assert(0 < t.size());
    assert(t.size() <= t.arity());
    assert((! SymbLabel::bar(t.label())) || (t.arity() == 2));
    assert((! SymbLabel::bar(t.label())) || (t.multiplicity(0) == 1));

    // first child, k is parent
    if (r.empty())
    {
        assert(r.partial());
        assert(_state == t.state(0)); // check downcast of KeyS
        init_firstchild(k, t, env);
    }
    // instanciate head (inner case)
    else if (r.complete())
    {
        assert(r.size() == t.size());
        assert(_state == k._state); // check downcast of KeyS
        assert(r.size() > 0);
        assert(r.key(r.size()-1)); // last sibling
        init_instanciate(k, *(r.key(r.size()-1)), t, env);
    }
    // next sibling
    else
    {
        assert(r.partial());
        size_t i = r.size();  // index of next sibling in r
        assert(0 < i);        // there is at least one previous sibling
        assert(i < t.size()); // r partial
        assert(_state == t.state(i)); // check downcast of KeyS
        assert(r.key(i-1)); // last sibling
        init_nextsibling(k, *(r.key(i-1)), t, i, env);
    }
}


void KeySV::init_firstchild(const KeySV& parent,
                            const Transition& t,
                            const ParsingEnv& env)
{
    assert(t.inner());
    label_t a = t.label();
    size_t ar = t.arity();
    assert(ar > 0);
    size_t mult = t.multiplicity(0);
    
    _start = MTU::ZERO;
    _fs = true;
    
    if (SymbLabel::tuple(a)) //  || SymbLabel::rest(a))
    {
        mtu_t dur = ((parent._list.length() / ar) * mult);
        _list = ValueList(parent._list, MTU::ZERO, dur);
    }
    else if (SymbLabel::bar(a))
    {
        // rdur = _barlen, mdur=1
        assert(ar == 2);
        assert(mult == 1);
        _list = ValueList(parent._list, MTU::ZERO, MTU::ONE);
    }
    else
    {
        ERROR("KeySV.FirstChild: unexpected symbol in tr: _state -> {}", t);
    }
    /// @todo cases grace and fork
    assert(_start.null());
}


void KeySV::init_nextsibling(const KeySV& parent,
                             const KeySV& previous,
                             const Transition& tr,
                             size_t i, // index of next
                             const ParsingEnv& env)
{
    assert(tr.inner());
    assert(previous.complete());
    label_t a = tr.label();
    size_t ar = tr.arity();
    // number fo children
    size_t nbc = tr.size();
    assert(nbc <= ar);
    assert(0 < i); // not first child
    assert(i < nbc);
    assert((i > 1) || previous._fs); // if previous is first child
    size_t mult = tr.multiplicity(i);
    assert(mult >= 1);

    // start time for this sibling list
    _start = previous._start + previous._list.length();
    assert(0 <= _start); // start date inside parent
    assert(_start <= parent._list.length());
    _fs = false;

    if (SymbLabel::tuple(a)) //  || SymbLabel::rest(a))
    {
        mtu_t dur = ((parent._list.length() / ar) * mult);
        // end of this sibling list included in parent
        assert(0 <= _start + dur);
        assert(_start + dur <= parent._list.length());
        _list = ValueList(parent._list, _start, dur);
    }
    else if (SymbLabel::bar(a))
    {
        // rdur = _barlen, mdur=1
        assert(ar == 2);
        assert(i == 1); // second sibling
        assert(mult == 1);
        // start = end of previous bar
        // dur = infinity
        _list = ValueList(parent._list, MTU::ONE);
    }
    else
    {
        ERROR("KeySV.NextSibling: unexpected symbol in tr: _state -> {}", tr);
    }
    /// @todo cases grace and fork
}


void KeySV::init_instanciate(const KeySV& head,
                             const KeySV& last,
                             const Transition& t,
                             const ParsingEnv& env)
{
    assert(last.complete());
    // simply copy. nothing to change (head is already complete)
    _list = head._list;
    _start = head._start;
    _fs = head._fs;
    assert(this->complete());
}


// copy
KeySV::KeySV(const KeySV& k):
KeyS(k),
_list(k._list),
_start(k._start),
_fs(k._fs)
{ }



KeySV::~KeySV()
{
    TRACE("delete KeySI {}", *this);
}


KeySV& KeySV::operator= (const KeySV& k)
{
    if(this != &k)
    {
        KeyS::operator=(k); // _state = k._state;
        _list  = k._list;   // copy
        _start = k._start;
        _fs = k._fs;
    };
    return *this;
}


// for use as key in a unordered_multimap.
bool KeySV::operator==(const KeySV& k) const
{
    return (KeyS::operator==(k) &&
            (_list == k._list) &&
            (_start == k._start) &&
            (_fs == k._fs));
}


// for use as key in a unordered_multimap.
bool KeySV::operator!=(const KeySV& k) const
{
    return (! (*this == k));
}


// this Key is always complete
bool KeySV::instance(const KeySV& k) const
{
    return (k == *this);
}

// all attributes of this key are valued
bool KeySV::complete() const
{
    return true;
}
   

void KeySV::runs(StackInner<KeySV>& si,
                 //std::stack<RunInner<KeySV>*>& si,
                 StackTerm<KeySV>& st,
                 //std::stack<std::pair<const KeySV*, RunTerm*>>& st,
                 ParsingEnv& env) const
{
    RunFactory<KeySV>::runs(*this, si, st, env);
}


// not the default of RunFactory (consider interval)
void KeySV::RunsTerminal(const Transition& t,
                         StackTerm<KeySV>& st,
                         //std::stack<std::pair<const KeySV*, RunTerm*>>& st,
                         const ParsingEnv& env) const
{
    const label_t& a = t.label();
    assert(SymbLabel::arity(a) == 0);
    
    // continuation: tie or dot
    if (SymbLabel::continuation(a) || SymbLabel::dot(a))
    {
        if (_list.single_continuation())
        {
            TRACE("KeySV: {} -> <{}> {} {} {}   compatible, add terminal tr.",
                  _state, SymbLabel::to_string(a),
                  (_list.single_continuation())?"(single cont)":"",
                  (_list.single_event())?"(single event)":"",
                  (_list.event())?"(event)":"");
            Weight w = t.weight();
            size_t len = 0;
            RunFactory<KeySV>::RunTermmk(*this, t, a, w, len, st);
        }
    }
    // case of single event (no grace notes, no continuation)
    else if (SymbLabel::rest(a) ||
            (SymbLabel::note(a) && (SymbLabel::nbGraceNotes(a) == 0)))
    {
        if (_list.single_event())
        {
            TRACE("KeySV: {} -> <{}> {} {} {}   compatible, add terminal tr.",
                  _state, SymbLabel::to_string(a),
                  (_list.single_continuation())?"(single cont)":"",
                  (_list.single_event())?"(single event)":"",
                  (_list.event())?"(event)":"");
            Weight w = t.weight();
            size_t len = 1;
            RunFactory<KeySV>::RunTermmk(*this, t, a, w, len, st);
        }
    }
    // case of 1 note and grace notes (no continuation)
    else if (SymbLabel::note(a) && (SymbLabel::nbGraceNotes(a) == _list.nbgn()))
    {
        assert(! SymbLabel::continuation(a));
        if (_list.event())
        {
            TRACE("KeySV: {} -> <{}> {} {} {}   compatible, add terminal tr.",
                  _state, SymbLabel::to_string(a),
                  (_list.single_continuation())?"(single cont)":"",
                  (_list.single_event())?"(single event)":"",
                  (_list.event())?"(event)":"");
            Weight w = t.weight();
            size_t len = _list.nbevent();
            RunFactory<KeySV>::RunTermmk(*this, t, a, w, len, st);
        }
    }
    // case of double bar (end of score)
    else if (SymbLabel::bar(a) && (SymbLabel::arity(a) == 0))
    {
        // condition of termination : end of input segment reached
        if (_list.length().null() && _list.empty())
        {
            DEBUGU("KeySV: terminal bar interval: {} for transition {}",
                   _list, t);
            Weight w = t.weight();
            size_t len = 0;
            RunFactory<KeySV>::RunTermmk(*this, t, a, w, len, st);
        }
    }
    else if (SymbLabel::dummy(a))
    {
        ERROR("KeySV: unexpected dummy symbol in transition {}", t);
    }
    else
    {
        TRACE("KeySV: {} -> <{}> {} {} {} ignore incompatible terminal tr.",
              _state, SymbLabel::to_string(a),
              (_list.single_continuation())?"(single cont)":"",
              (_list.single_event())?"(single event)":"",
              (_list.event())?"(event)":"");
    }
}


// not the default of RunFactory (filter out not inhabited intervals)
void KeySV::RunsInner(const Transition& t,
                      StackInner<KeySV>& si,
                      //std::stack<RunInner<KeySV>*>& si,
                      const ParsingEnv& env) const
{
    const label_t& a = t.label();
    assert(SymbLabel::inner(a));
    
    if (SymbLabel::tuple(a))
    {
        // we do not divide empty intervals
        if (! _list.empty())
            RunFactory<KeySV>::RunInnermk(*this, t, si);
    }
    else if (SymbLabel::grace(a))
    {
        assert(SymbLabel::arity(a) == 2);
        ERROR("KeySV: outside g.n. not supported, label={}",
              SymbLabel::to_string(a));
    }
    else if (SymbLabel::fork(a))
    {
        assert(SymbLabel::arity(a) == 2);
        ERROR("KeySV: voice fork not supported, label={}",
              SymbLabel::to_string(a));
    }
    else if (SymbLabel::bar(a) && SymbLabel::arity(a) == 2)
    {
        // end of input not reached (condition of termination)
        if (! _list.empty())
        {
            DEBUGU("KeySV: binary bar interval: {} for transition {}",
                   _list, t);
            RunFactory<KeySV>::RunInnermk(*this, t, si);
        }
    }
    else
    {
        ERROR("KeySV: unexpected inner symbol {}", SymbLabel::to_string(a));
    }
}


size_t KeySV::size() const
{
    mtu_t c = _list.cont();
    return (_list.size() - ((c > 0)?1:0));
}


// number of points to read in the input segment
/// @todo RM : replaced by size()
size_t KeySV::points() const
{
    return _list.size();
}


// not the default of RunFactory (use specific size of this class)
std:shared_ptr<RunTerm> KeySV::failRun(const ParsingEnv& env) const
{
    //return RunFactory<KeySV>::failRun(*this, env);
    assert(env.wta);
    const Transition& t0 = env.dummyTransition_zero();
    assert(t0.weight().is_zero());
    size_t length = this->size(); // number of input symbols parsed
    label_t a = SymbLabel::make_fail(length);
    std:shared_ptr<RunTerm> r =
        std:make_shared<RunTerm>(t0, a, t0.weight(), length);
    return r;
}


std::ostream& operator<<(std::ostream& o, const KeySV& k)
{
    o << KEY_LPAR;
    o << " " << k.state() << " ";
    o << k._list;    // short version
    o << KEY_RPAR;
    
    return o;
}








} // end namespace parsing

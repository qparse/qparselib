//
//  Quantizer.tpp
//  squanty
//
//  Created by Florent Jacquemard on 24/03/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

//#define PATCH_TRSISSUE3_GUESS
#define PATCH_TRSISSUE3_INTERVAL


template <class K>
Quantizer<K>::Quantizer(TableA<K>& tab):
_table(tab),
_previnterval(NULL)
{ }


template <class K>
Quantizer<K>::~Quantizer()
{ }


template <class K>
void Quantizer<K>::quantize(InputSegment& seg,
                            const K& k,
                            size_t n)
{
    assert(n >= 1);
    quantize_run(seg, k, _table.best(k, n), 0);
}


template <class K>
size_t Quantizer<K>::quantize_run(InputSegment& seg,
                                  const K& k,
                                  std::shared_ptr<const parsing::Run> r,
                                  size_t b)
{
    assert(k.interval() != NULL);
    TRACE("quantize[{}]: point {} with {}", b, b, k);

    if (r == NULL)
    {
        ERROR("parse failure in input segment of {} (no run)", k);
        if (! k.interval()->aligned())
        {
            k.interval()->align(&seg);
        }

        return k.interval()->next();
    }
    assert(r->complete());
    if (r->terminal())
    {
        std::shared_ptr<const parsing::RunTerm> rt =
        std::dynamic_pointer_cast<const parsing::RunTerm, const parsing::Run>(r);
        return quantize_run_term(seg, k, rt, b);
    }
    else
    {
        std::shared_ptr<const parsing::RunInner<K>> ri =
        std::dynamic_pointer_cast<const parsing::RunInner<K>, const parsing::Run>(r);
        return quantize_run_inner(seg, k, ri, b);
    }
}


template <class K>
size_t Quantizer<K>::quantize_run_inner(InputSegment& seg,
                                        const K& k,
                                        std::shared_ptr<const parsing::RunInner<K>> r,
                                        size_t b)
{
    assert(r);
    assert(r->inner());
    size_t j = b;
    size_t ar = r->arity();
    assert(ar > 0);
    label_t a = r->label();
    
    // tuple - beamed or not
    if (SymbLabel::tuple(a))
    {
        for (size_t i = 0; i < ar; i++)
        {
            const K& ki = r->key(i);
            std::shared_ptr<const Run> ri = r->subrun(i);
            j = quantize_run(seg, ki, ri, j);
        }
        return j;
    }
    // single bar mark
    else if (SymbLabel::bar(a))
    {
        assert(SymbLabel::arity(a) == 2); // not double bar
        // first child is a bar
        const K& k0 = r->key(0);
        j = quantize_run(seg, k0, r->subrun(0), j);
        // second child is the rest
        const K& k1 = r->key(1);
        return quantize_run(seg, k1, r->subrun(1), j);
    }
    // grace notes
    else if (SymbLabel::grace(a))
    {
        WARN("quantize[{}]: grace note binary symbol, not supported", j);
        return j;
    }
    // voice separation
    else if (SymbLabel::fork(a))
    {
        WARN("quantize[{}]: voice fork symbol, not supported", j);
        return j;
    }
    else
    {
        ERROR("quantize[{}]: unexpected inner symbol {}", j, a);
        return j;
    }
}


#ifdef PATCH_TRSISSUE3_INTERVAL
/// this patched function
/// uses the info on alignments in intervals contained in Key.
/// @warning It works only for Key.s that align points at closest
/// of its interval, like KeySIP, KeySIR.

template <class K>
size_t Quantizer<K>::quantize_run_term(InputSegment& seg,
                                       const K& k,
                                       std::shared_ptr<const parsing::RunTerm> r,
                                       size_t b)
{
    assert(r->terminal());
    size_t j = b;
    label_t a = r->label();
    assert(k.interval() != NULL);
    assert(k.interval()->aligned());
    mtu_t onset = k.interval()->mbegin();
    mtu_t dur = k.interval()->mduration();
    assert(dur > MTU::ZERO);
    size_t rsize =
        (_previnterval == NULL)?0:_previnterval->rsize();
    size_t lsize = k.interval()->lsize();
    size_t first =
        (rsize == 0)?k.interval()->lfirst():_previnterval->rfirst();
    // if rsize != 0 then _previnterval is not NULL
    DEBUGU("quantize[{}]: new event, first(interval)={}", j, first);
    assert((first == seg.size()) || (first == j));
    
    // rest
    if (SymbLabel::rest(a))
    {
        DEBUGU("quantize[{}]: REST mdate={} mdur={}  rsize={} lsize={}",
               j, onset, dur, rsize, lsize);
        // assume that there were no grace notes before that rest
        assert(rsize+lsize == 1);
        assert(j < seg.size());
        assert(seg.offset(j));         // offset event
        seg.set_mdate(j, onset);
        if (j == seg.size()-1) // last event
        {
            seg.set_mduration(j, MTU::ZERO);
        }
        else
        {
            seg.set_mduration(j, dur);
            seg.extend_mend(dur); // TBC dirty.should be set only once for j.
        }
        _previnterval = k.interval();
        return ++j;
    }
    // grace notes + note
    else if (SymbLabel::note(a))
    {
        DEBUGU("quantize[{}]: GN/N mdate={} mdur={}  rsize={} lsize={}",
               j, onset, dur, rsize, lsize);
        //assert((j >= seg.size()) || MTU::undefined(seg.mdate(j)));
        //assert((j >= seg.size()) || MTU::undefined(seg.mduration(j)));

        // grace notes
        assert(rsize+lsize >= 1);
        assert(SymbLabel::nbGraceNotes(a) < rsize+lsize);
        /// @todo change lsize to lsize + pre
        for (size_t i = 0; i < rsize+lsize-1; i++)
        {
            DEBUGU("quantize[{}]: G.N. mdate={} mdur={}", j, onset, dur);
            assert(j < seg.size());
            // normal case of grace note
            assert(MTU::undefined(seg.mdate(j)));
            assert(MTU::undefined(seg.mduration(j)));
            seg.set_mdate(j, onset);
            seg.set_mduration(j, MTU::ZERO);
            ++j;
        }
        // note
        DEBUGU("quantize[{}]: NOTE mdate={} mdur={}", j, onset, dur);
        assert(j < seg.size());
        assert(MTU::undefined(seg.mdate(j)));
        assert(MTU::undefined(seg.mduration(j)));
        //assert((rsize+lsize == 1) || seg.onset(j)); // should not be a rest
        if (seg.offset(j))
        {
            ERROR("quantize[{}]: NOTE expected, REST found");
        }
        seg.set_mdate(j, onset);
        seg.set_mduration(j, dur);
        seg.extend_mend(dur); // TBC dirty should be set only once for j.
        _previnterval = k.interval();
        return ++j;
    }
    // contiuation : accumulate in previous note
    else if (SymbLabel::continuation(a) || SymbLabel::dot(a))
    {
        // skip an initial continuation
        if (j == 0)
        {
            DEBUGU("quantize[{}]: CONT SKIP mdate={} mdur={}", j, onset, dur);
            assert(MTU::undefined(seg.mdate(j)));
            assert(MTU::undefined(seg.mduration(j)));
            seg.extend_mend(dur);
        }
        // continuation added to previous event
        else
        {
            DEBUGU("quantize[{}]: CONT mdate={} mdur={}; prev mdate={} mdur={}; rsize={} lsize={}", j, onset, dur, seg.mdate(j-1), seg.mduration(j-1),
                   rsize, lsize);
            assert(j-1 < seg.size());
            assert(rsize == 0);
            assert(lsize == 0); // no event in interval
            assert((seg.offset(j-1) && (j == seg.size())) ||
                   (onset == seg.mdate(j-1) + seg.mduration(j-1)));
            seg.set_mduration(j-1, seg.mduration(j-1)+dur);
            seg.extend_mend(dur); // TBC dirty should be set only once for j.
        }
        _previnterval = k.interval();
        return j;
    }
    // double bar mark - do nothing
    else if (SymbLabel::bar(a))
    {
        assert(SymbLabel::arity(a) == 0); // not-single bar
        INFO("quantize[{}]: Double bar at point {}/{}", j, j, seg.size());
        //assert(j >= seg->size()); // all points quantified
        return j;
    }
    else if (SymbLabel::dummy(a))
    {
        WARN("quantize[{}]: dummy symbol", j);
        return j;
    }
    else
    {
        ERROR("quantize[{}]: unexpected leaf symbol {}", j, a);
        return j;
    }
}

#endif



#ifdef PATCH_TRSISSUE3_GUESS
/// this patched function tries to guess the offset that have been
/// skip because they correspond to grace rests.
/// @warning Its behaviour must be tested.
template <class K>
size_t Quantizer<K>::quantize_run_term(InputSegment& seg,
                                       const K& k,
                                       std::shared_ptr<const parsing::Run> r,
                                       size_t b)
{
    assert(r->terminal());
    size_t j = b;
    label_t a = r->label();
    assert(k.interval() != NULL);
    assert(k.interval()->aligned());
    mtu_t onset = k.interval()->mbegin();
    mtu_t dur = k.interval()->mduration();
    assert(dur > MTU::ZERO);

    // rest
    if (SymbLabel::rest(a))
    {
        if (j < seg.size())
        {
            // offset event
            DEBUGU("quantize[{}]: REST mdate={} mdur={}", j, onset, dur);
            assert(seg.offset(j));
            seg.set_mdate(j, onset);
            seg.set_mduration(j, dur);
            seg.extend_mend(dur); // TBC dirty.should be set only once for j.
            return ++j;
        }
        else
        {
            ERROR("quantize[{}]: rest out of input segment", j);
            return j;
        }
    }
    // grace notes + note
    else if (SymbLabel::note(a))
    {
        assert((j >= seg.size()) || MTU::undefined(seg.mdate(j)));
        assert((j >= seg.size()) || MTU::undefined(seg.mduration(j)));
        // grace notes
        // some g.n. can be ignored e.g. grace rests in KeySIR
        // grace notes
        for (size_t i = 0; i < SymbLabel::nbGraceNotes(a); i++)
        {
            if (j < seg.size())
            {
                // DIRTY. we guess that a grace rest was ignored.
                if (seg.offset(j)) // grace rest
                {
                    DEBUGU("quantize[{}]: G.R. ignored", j);
                }
                // normal case of grace note
                else
                {
                    assert(MTU::undefined(seg.mdate(j)));
                    assert(MTU::undefined(seg.mduration(j)));
                    DEBUGU("quantize[{}]: G.N. mdate={} mdur={}",
                           j, onset, dur);
                    seg.set_mdate(j, onset);
                    seg.set_mduration(j, MTU::ZERO);
                }
            }
            else
            {
                ERROR("quantize: grace note {} out of input segment", j);
                return j;
            }
            ++j;
        }
        // note
        if (j < seg.size())
        {
            assert(MTU::undefined(seg.mdate(j)));
            assert(MTU::undefined(seg.mduration(j)));

            // DIRTY. we guess that a grace rest was ignored.
            if (seg.offset(j)) // grace rest
            {
                DEBUGU("quantize[{}]: G.R. ignored", j);
                // try with the next point
                return quantize_run_term(seg, k, r, ++j);
            }
            // normal case of a note
            else
            {
                assert(seg.onset(j));
                DEBUGU("quantize[{}]: NOTE mdate={} mdur={}", j, onset, dur);
                seg.set_mdate(j, onset);
                seg.set_mduration(j, dur);
                seg.extend_mend(dur); // TBC dirty should be set only once for j.
                return ++j;
            }
        }
        else
        {
            ERROR("quantize: note {} out of input segment", j);
            return j;
        }
    }
    // contiuation : accumulate in previous note
    else if (SymbLabel::continuation(a) || SymbLabel::dot(a))
    {
        // skip an initial continuation
        if (j == 0)
        {
            assert(MTU::undefined(seg.mdate(j)));
            assert(MTU::undefined(seg.mduration(j)));
            DEBUGU("quantize[{}]: CONT SKIP mdate={} mdur={}", j, onset, dur);
            seg.extend_mend(dur);
        }
        // continuation added to previous event
        else
        {
            assert(j-1 < seg.size());
            assert(onset == seg.mdate(j-1) + seg.mduration(j-1));
            DEBUGU("quantize[{}]: CONT mdate={} mdur={}", j, onset, dur);
            seg.set_mduration(j-1, seg.mduration(j-1)+dur);
            seg.extend_mend(dur); // TBC dirty should be set only once for j.
        }
        return j;
    }
    // double bar mark - do nothing
    else if (SymbLabel::bar(a))
    {
        assert(SymbLabel::arity(a) == 0); // not-single bar
        INFO("Double bar at point {}/{}", j, seg.size());
        //assert(j >= seg->size()); // all points quantified
        return j;
    }
    else if (SymbLabel::dummy(a))
    {
        WARN("quantize: dummy symbol at {}", j);
        return j;
    }
    else
    {
        ERROR("quantize: unexpected leaf symbol {}", a);
        return j;
    }
}

#endif


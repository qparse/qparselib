//
//  Recordey.hpp
//  squanty
//
//  Created by Florent Jacquemard on 07/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{

#ifndef Recordey_hpp
#define Recordey_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <assert.h>

#include "trace.hpp"
#include "Runey.hpp"
//#include "RunTerm.hpp"
//#include "RunInner.hpp"
//#include "RunCompare.hpp"
#include "RunOrder.hpp"
#include "Recordo.hpp"


namespace parsing{

//template<class K> class Run;
//template<class K> class RunCompare;
//template<class K, class R, class H> class TableParse;

class RunTerm;
class RunInnero;
template<class K> class RunInner;


/// @brief abstract class describing the basic functionalities
/// of a record, which is a container of runs.
///
/// each record is associated with a Key (target of transitions)
/// and contains bodies of transition.
/// It can be filled with add
/// and can be interrogating with best,
/// for retrieving the best runs for the associated Key.
///
/// When uncomplete runs are added to the record
/// (either by the record or from outside)
/// their weight must be computed using a table.
///
/// [update] the runs with weight zero (still invalid) can be added to records
/// but an error message is displayed (for debugging).
template<class K>
class Record : public Recordo
{
       
public:
    
    Record() { assert(false); }

    // @brief empty record associated to a key of type K.
    // @param comp function for comparing Runs over K.
    // @param failrun fixed run returned by best when there is no best run.
    // @todo delete failrun if not necessary (replaced by method)
    // @warning the key associated to the record must
    //          be set later to a non-NULL ptr with setKey.
    // Record(RunCompare comp, const RunTerm* failrun);

    /// @brief empty record associated to a key of type K.
    /// @param k the key associated to the record
    /// @param comp function for comparing Runs over K.
    /// @todo delete failrun if not necessary (replaced by method)
    Record(std::shared_ptr<const K> k,
           std::shared_ptr<const RunOrder> comp);

    /// copy
    Record(const Record&);
    
    /// @brief assignement.
    virtual Record<K>& operator=(const Record<K>& rec);

    virtual ~Record();
    
    /// @brief n-th best run of the record.
    // @param table may be completed with new runs (lazy evaluation of candidates).
    /// @param n (as in n-best) is the rank of the run with target the key of the record.
    /// @return the n-th best run of the record or, if there is none,
    /// the failRun set or NULL when the failRun was not set with setFailRun.
    /// @warning the record can be modified.
    virtual std::shared_ptr<const Run> best(size_t n=1) = 0;
    //    virtual Run* best(TableI<K>* table, size_t n=1) = 0;

    /// @brief key associated to the record.
    const K& key() const;
    
    /// function to compare runs
    const RunOrder::Compare& compare() const;
    
    /// generic run returned in case of parse failure.
    std::shared_ptr<const Run> failrun() const;
    
    // @brief set key associated to the record.
    // @param k new key for this record. must be complete.
    // @warning the current key of this record must be NULL.
    // @warning this record must be empty.
    // void setKey(const K& k);
    
    // @brief set the fail Run associated to the record.
    // @param r the fail run to be set.
    // @warning the current fail Run of this record must be NULL.
    // void setFailRun(std::shared_ptr<const RunTerm> r);
   
    /// at least one run has been added succesfully.
    virtual bool empty() const = 0;
    
//    /// state - possible values:
//    /// 0 : empty (record just created no run was stored)
//    /// 1 : add(_key) was not called
//    ///     but add(p) was called for p partial and subsuming _key
//    /// 2 : add(_key) was called
//    /// 3 : some run has been stored
//    ///     but we are not in 1 or 2. should not happen.
//    ///
//    /// The state is not changed inside the Record class.
//    /// It is changed by callers (table.add).
//    /// @todo TBR (obsolete)
//    unsigned int state;
    
protected:
    
    /// key associated to the record.
    std::shared_ptr<const K> _key;

    /// function for the comparison of runs.
    std::shared_ptr<const RunOrder> _comp;
    
    // generic fail run associated to the key.
    // will be returned in case of parse failure.
    // @todo delete if not necessary
    // std::shared_ptr<const RunTerm> _fail;
    
    /// stats: number of candidate bests constructed.
    size_t _nb_cand;
    
    /// stats: number of best not added to the list because of optimization filters.
    size_t _nb_best_rejected;
    
    /// @brief sanity check before adding run to a record.
    /// There are 3 kinds of Run* that should not be added in record:
    /// TBC should not be returned by best on the record ?
    /// - NULL ptr to Run
    /// - ptr to Run with unknown weight (i.e. weight with NULL letter).
    /// - ptr to Run with weight == zero (acc. to test is_zero()).
    /// a Run not in these 3 cases is called valid.
    bool valid(std::shared_ptr<const Run>);
    
    // Fail run associated with the key.
    // will be returned in case of parse failure.
    // const Run* fail() const;
    
};


} // end namespace parsing


// separated definition of template class
#include "Recordey.tpp"


#endif /* Recordey_hpp */

/// @}

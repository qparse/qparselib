//
//  KeyS.hpp
//  squant
//
//  Created by Florent Jacquemard on 31/03/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{


#ifndef KeyS_hpp
#define KeyS_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <assert.h>
#include <stack>          // std::stack
#include <queue>          // std::queue

#include "State.hpp"
#include "SymbLabel.hpp"
#include "Transition.hpp"
#include "ParsingEnv.hpp"
#include "Runey.hpp"
#include "RunInner.hpp"
#include "RunTerm.hpp"
#include "RunFactory.hpp"


namespace parsing{

// for printing the keys
#define KEY_LPAR '<'
#define KEY_RPAR '>'


/// @brief generalities on the Key classes.
/// A key is an augmented state,
/// acting as index for best runs in a parse table.
///
/// It is made of:
/// - one state of a base SWTA,
/// - extended with attributes taking values in different domains,
/// and of two kinds:
///   - synthesized attributes are propagated bottom-up,
///   - inherited attributes are propagated top-down.
///
/// - A key is partial if only its inherited attributes are valued.
/// - A key is complete if its all inherited and synthesized attributes are
///   valued
///   (keys without synthesized attributes are always complete).
///
/// A augmented transition has the form
/// a(K1, ..., Kn) -> K0
/// where
/// - a is a label of arity n.
/// - (K1, ..., Kn) is a tuple of complete keys called body of the transition.
/// - K0 is a complete key called head of the transition.
/// K0, K1, ..., Kn are abject of the same concrete class K derived
/// from this class.
///
/// concrete transitions are built with the following functions:
/// - initial augmented states can be built with `top`.
/// - from a given a partial augmented state `K0` for the head
///   of an inner augmented transition,
///   one can built a partial first child by calling `firstChild` on `K0`.
/// - then every other child `Ki+1` can be built by calling `nextSibling`
///   on sibling `Ki`, for `i > 0`.
/// - from a given a partial augmented state `K0` for the head
///   of a terminal (leaf) augmented transition,
///   one can built possible complete instances by calling `leaf`.
/// - the (partial) states for siblings are instanciated
///   by repeating from step 3 down to leaves.
/// - once all the children augmented states are complete,
///   the head 'K0' can be instanciated by calling `instanciate` on `K0`
///   (with the last child complete state in argument).
///
/// Some synthesized attribute contain weight values.
/// The weight of a key is computed from the attributes and an input symbol.

/// Key made of one state of a base WTA (trivial augmented state).
/// no attributes.
/// extended with rank for k-best algo (not used for 1-best).
class KeyS
{
    
    /// @todo use Curiously Recurring Template Pattern instead?
    template<typename> friend class RunFactory;
    
public:
    
    /// @brief top constructor.
    /// allocate an initial key
    /// which will serve to create the other keys.
    /// @param s initial state of the base WTA.
    /// @param env input environment.
    KeyS(state_t s, const ParsingEnv& env);
    
    /// @brief constructor of a successor of a key to complete an inner run.
    /// @param k target key for the run. can be partial.
    /// @param r an inner run to be completed with the key constructed.
    /// @param env input environment.
    /// There are 4 cases of construction:
    /// - if r is partial and empty, construct the first child k0 to add to r.
    ///   k is the parent of k0 in this case.
    /// - if r is partial but not empty, construct the next sibling kn to add
    ///   to r. k is the parent of kn in this case.
    /// - if r is complete, construct a complete instance of k
    ///   that will be the (complete) target of run r.
    KeyS(const KeyS& k, RunInner<KeyS>& r, const ParsingEnv& env);

    /// @brief constructor of an instance of a key targeting a terminal run.
    /// @param k target key for the run in 2 cases below. must be partial.
    /// @param r a complete terminal run targeting the key constructed.
    /// @param env input environment.
    /// Construct a complete instance of k that will be the target of run r.
    KeyS(const KeyS& k, const RunTerm& r, const ParsingEnv& env);
    
    /// @brief copy
    KeyS(const KeyS& k);
    
    /// destructor
    virtual ~KeyS();
            
    /// Key assignement.
    virtual KeyS& operator= (const KeyS& k);
        
    /// for use as key in a unordered_multimap.
    virtual bool operator==(const KeyS& k) const;
    virtual bool operator!=(const KeyS& k) const;
    
    /// return wether this Key is an instance of k.
    /// = equality in this case because KeyS are always complete.
    virtual bool instance(const KeyS& k) const;
    
    /// all attributes of the key are valued
    virtual bool complete() const;

    /// one atribute at least of the key is not valued
    inline bool partial() const { return(! this->complete()); }
    
    /// accessor
    inline state_t state() const { return _state; }

    /// @brief construct (augmented) runs targeting complete instances
    /// of this key.
    /// @param si a stack to be filled with inner runs. must be empty.
    /// @param st a stack to be filled with pairs containing terminal runs.
    /// must be empty.
    /// @param env parsing environment, containing a wta.
    // @return how many runs have been added to the stacks.
    /// @warning the runs constructed are added to the given stacks.
    /// - si will contain inner runs, partial or complete.
    /// - st will contains pairs made of
    ///     - a complete instance k' of k if k is partial,
    ///       or k itself otherwise.
    ///     - a complete terminal run targeting r'
    /// All the runs in stacks must be deallocate elsewhere.
    virtual void runs(StackInner<KeyS>& si,
                      StackTerm<KeyS>& st,
                      ParsingEnv& env) const;
     
    /// allocate and return a special Run representing a parse failure for this key.
    /// @param env parsing environment.
    /// @return a pointer to a newly allocated fail run, with
    /// - transition with dummy symbol, empty body, weight zero in the
    ///   domain of the base wta in the passed environment.
    // - fail symbol
    // - weight zero (in the domain of the base wta).
    // This returned run will answer succesfully to Run.fail().
    // std::shared_ptr<RunTerm> failRun(const ParsingEnv& env) const;
    
    friend std::ostream& operator<<(std::ostream& o, const KeyS& k);
    
protected:

    /// state of the base WTA
    state_t _state;
        
    /// @brief constructor for internal use
    KeyS(state_t s);
    

    // special case of runs(si, st, env).
    // @param si a stack to be filled with inner runs.
    // @param st a stack to be filled with pairs containing terminal runs.
    // @param env parsing environment, containing a wta.
    // virtual void runsWTA(std::stack<RunInner<KeyS>*>& si,
    //                      std::stack<std::pair<KeyS, RunTerm*>>& st,
    //                      const ParsingEnv& env) const;
    
    
    // construct and add to the stacks (augmented) runs targeting
    // complete instances of this key.
    // void runsBar(std::stack<RunInner<KeyS>*>& si,
    //              std::stack<std::pair<KeyS, RunTerm*>>& st,
    //              ParsingEnv& env) const;

    
    /// add to the given stack some terminal runs built from the given transition.
    /// @param t transition to build terminal runs. must be terminal and complete.
    /// @param st a stack to be filled with pairs containing terminal runs.
    /// @param env parsing environment, containing a wta and pool.
    void RunsTerminal(const Transition& t,
                      StackTerm<KeyS>& st,
                      const ParsingEnv& env) const;

    /// add to the given stack some inner runs built from the given transition.
    /// @param t transition to build inner runs. must be inner and complete.
    /// @param si a stack to be filled with inner runs.
    /// @param env parsing environment, containing a wta and pool.
    /// @warning the runs constructed are added to the given stack.
    /// They can be partial or complete.
    /// They must be deallocate elsewhere.
    void RunsInner(const Transition& t,
                   StackInner<KeyS>& si,
                   const ParsingEnv& env) const;
       
};



struct KeySHasher
{
    std::size_t operator()(const  KeyS& p) const
    {
        using std::size_t;
        using std::hash;
        return std::hash<state_t>()(p.state());
    }
};


} // end namespace parsing


// separated definition of template class
// #include "KeyS.tpp"


#endif /* KeyS_hpp */

/// @}

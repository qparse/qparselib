//
//  KeySIO.cpp
//  squant2
//
//  Created by Florent Jacquemard on 02/12/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "KeySIO.hpp"


namespace parsing{


// top constructor (top state = first bar)
KeySIO::KeySIO(rtu_t len, const ParsingEnv& env, bool f_open,
               pre_t pre, pre_t preon, bool prelaston):
KeySI(len, env, f_open),
_pre(pre),
_pre_on(preon),
_pre_laston(prelaston),
_post(P_UNKNOWN),
_post_on(P_UNKNOWN),
_post_laston(false)
{
    assert(P_KNOWN(pre));
    assert(P_KNOWN(preon));
}


// internal use
KeySIO::KeySIO(state_t s, IntervalTree* node, rtu_t len,
               pre_t pre, pre_t pre_on,  bool pre_laston,
               pre_t post, pre_t post_on,  bool post_laston):
KeySI(s, node, len),
_pre(pre),
_pre_on(pre_on),
_pre_laston(pre_laston),
_post(post),
_post_on(post_on),
_post_laston(post_laston)
{ }


// successor key in run (fs or ns or instance)
KeySIO::KeySIO(const KeySIO& k,
               RunInner<KeySIO>& r,
               const ParsingEnv& env):
KeySI(k, (RunInner<KeySI>&) r, env),
_pre(k._pre), // k is the parent (fs, ns) or matcher (instanciate)
_pre_on(k._pre_on),
_pre_laston(k._pre_laston),
_post(P_UNKNOWN),
_post_on(P_UNKNOWN),
_post_laston(false)
{
    assert(r.inner());
    const Transition& t = r.origin();
    assert(t.inner());
    assert(0 < t.size());
    assert(t.size() <= t.arity());

    
    // dispatch
    // the asserts ALREADY in KeySI::KeySI
    
    // first child, k is parent
    if (r.empty())
    {
        assert(r.partial());
        assert(_state == t.state(0)); // check downcast of KeyS
        init_firstchild(k, t, env);
    }
    // instanciate k (inner case) with last key in r
    else if (r.complete())
    {
        assert(r.size() == t.size());
        assert(_state == k._state); // check downcast of KeyS
        const KeySIO& last = r.key(r.size()-1); // last sibling
        init_instanciate(k, last, t, env);
    }
    // next sibling
    else
    {
        assert(r.partial());
        size_t i = r.size();  // index of next sibling in r
        assert(0 < i);        // there is at least one previous sibling
        assert(i < t.size()); // r partial
        assert(_state == t.state(i)); // check downcast of KeyS
        const KeySIO& last = r.key(i-1); // previous sibling
        init_nextsibling(k, last, t, i, env);
    }
}


void KeySIO::init_firstchild(const KeySIO& parent,
                             const Transition& t,
                             const ParsingEnv& env)
{
    assert(P_KNOWN(parent._pre));
    assert(P_KNOWN(parent._pre_on));
    label_t a = t.label();

    if (SymbLabel::tuple(a) || SymbLabel::bar(a)) //  || SymbLabel::rest(a))
    {
        _pre = parent._pre;
        _pre_on = parent._pre_on;
        _pre_laston = parent._pre_laston;
        _post = P_UNKNOWN;
        _post_on = P_UNKNOWN;
        _post_laston = false;
    }
    else  /// @todo cases grace and fork
    {
        ERROR("KeySIO.firstChild: unexpected symbol in tr: _state -> {}", t);
    }
    assert(P_KNOWN(_pre));
    assert(P_KNOWN(_pre_on));
}


void KeySIO::init_nextsibling(const KeySIO& parent,
                              const KeySIO& previous,
                              const Transition& t,
                              size_t i, // index of next
                              const ParsingEnv& env)
{
    assert(previous.complete());
    label_t a = t.label();

    if (SymbLabel::tuple(a) || SymbLabel::bar(a))
    {
        _pre = previous._post;
        _pre_on = previous._post_on;
        _pre_laston = previous._post_laston;
        _post = P_UNKNOWN;
        _post_on = P_UNKNOWN;
        _post_laston = false;
    }
    else     /// @todo cases grace and fork
    {
        ERROR("KeySIO.nextSibling: unexpected symbol in tr: _state -> {}", t);
    }
    assert(P_KNOWN(_pre));
    assert(P_KNOWN(_pre_on));
}


void KeySIO::init_instanciate(const KeySIO& head,
                              const KeySIO& last, // last sibling below head
                              const Transition& t,
                              const ParsingEnv& env)
{
    assert(head.partial()); // will be copied and completed with last
    assert(last.complete());

    assert(P_KNOWN(head._pre));
    _pre = head._pre;       // copy
    assert(P_KNOWN(head._pre_on));
    _pre_on = head._pre_on;
    _pre_laston = head._pre_laston;

    assert(P_KNOWN(last._post));
    _post = last._post;     // up-propagation of post
    assert(P_KNOWN(last._post_on));
    _post_on = last._post_on;
    _post_laston = last._post_laston;

    // this key is now complete
    assert(P_KNOWN(this->_pre));
    assert(P_KNOWN(this->_pre_on));
    assert(P_KNOWN(this->_post));
    assert(P_KNOWN(this->_post_on));
    assert(this->complete());
}


// copy
KeySIO::KeySIO(const KeySIO& k):
KeySI(k),
_pre(k._pre),
_pre_on(k._pre_on),
_pre_laston(k._pre_laston),
_post(k._post),
_post_on(k._post_on),
_post_laston(k._post_laston)
{ }


// copy and instanciate (up-propagation of post)
KeySIO::KeySIO(const KeySIO& k,
               pre_t post,
               pre_t post_on,
               bool  post_laston):
KeySI(k),
_pre(k._pre),
_pre_on(k._pre_on),
_pre_laston(k._pre_laston),
_post(post),
_post_on(post_on),
_post_laston(post_laston)
{
    assert(k.partial());
    // this key is now complete
    assert(P_KNOWN(this->_pre));
    assert(P_KNOWN(this->_pre_on));
    assert(P_KNOWN(this->_post));
    assert(P_KNOWN(this->_post_on));
    assert(this->complete());
}


KeySIO::~KeySIO()
{ }


KeySIO& KeySIO::operator= (const KeySIO& p)
{
    if(this != &p)
    {
        KeySI::operator=(p);
        _pre = p._pre;
        _pre_on = p._pre_on;
        _pre_laston = p._pre_laston;
        _post = p._post;
        _post_on = p._post_on;
        _post_laston = p._post_laston;
    };
    return *this;
}


bool KeySIO::operator==(const KeySIO& p) const
{
    return (KeySI::operator==(p) &&
            (_pre == p._pre) &&
            (_pre_on == p._pre_on) &&
            (_pre_laston == p._pre_laston) &&
            (_post == p._post) &&
            (_post_on == p._post_on) &&
            (_post_laston == p._post_laston));
}


bool KeySIO::operator!=(const KeySIO& p) const
{
    return (! (*this == p));
}


bool KeySIO::instance(const KeySIO& p) const
{
    assert(_pre != P_UNKNOWN);
    assert(_pre_on != P_UNKNOWN);
    assert(p._pre != P_UNKNOWN);
    assert(p._pre_on != P_UNKNOWN);
    return (KeySI::instance(p) &&
            ((p._post == P_UNKNOWN) || ((_post == p._post) &&
                                        (p._post_on != P_UNKNOWN) &&
                                        (_post_on == p._post_on) &&
                                        (_post_laston == p._post_laston))));
}


// always complete
bool KeySIO::complete() const
{
    assert(_pre != P_UNKNOWN);
    assert(_pre_on != P_UNKNOWN);
    assert((_post != P_UNKNOWN) || (_post_on == P_UNKNOWN));
    assert((_post_on != P_UNKNOWN) || (_post == P_UNKNOWN));
    return (KeySI::complete() &&
            (_post != P_UNKNOWN) &&
            (_post_on != P_UNKNOWN));
}


size_t KeySIO::size() const
{
    assert(_pre != P_UNKNOWN);
    assert(_pre_on != P_UNKNOWN);
    assert(_interval);
    assert(_interval->aligned());
    return _pre + _interval->lsize();
}


void KeySIO::runs(StackInner<KeySIO>& si,
                  // std::stack<RunInner<KeySIO>*>& si,
                  StackTerm<KeySIO>& st,
                  // std::stack<std::pair<const KeySIO*, RunTerm*>>& st,
                  ParsingEnv& env) const
{
    RunFactory<KeySIO>::runs(*this, si, st, env);
}


void KeySIO::RunsTerminal(const Transition& t,
                          StackTerm<KeySIO>& st,
                          //std::stack<std::pair<const KeySIO*, RunTerm*>>& st,
                          const ParsingEnv& env) const
{
    assert(P_KNOWN(_pre));
    assert(_interval);
    const label_t& a = t.label();
    assert(SymbLabel::terminal(a));
    const KeySIO* knew = nullptr;
      
    if (! _interval->aligned())
    {
        WARN("KeySIO RunsTerminal: unaligned interval {} for {}",
             *(_interval), SymbLabel::to_string(a));
        assert(env.segment);
        _interval->align(env.segment);
    }

    if (P_KNOWN(_post))
    {
        assert(P_KNOWN(_post_on));
        WARN("KeySIO : instance as leaf of complete key {}", *this);
        if ((_post == _interval->rsize()) &&
            (_post_on == _interval->ronsets()) &&
            (_post_laston == laston(env)))
        {
            knew = this;
        }
        else
        {
            return; // add nothing to queue
        }
    }
    else
    {
        //copy and update post values
        knew = new KeySIO(*this,
                          _interval->rsize(),   // post
                          _interval->ronsets(), // _post_on
                           laston(env));        // _post_laston
    }
    assert(knew);
    assert(knew->complete());
    //assert(P_KNOWN(knew->_post));
    //assert(P_KNOWN(knew->_post_on));

    /// no points in pre + left-half of interval
    if (SymbLabel::continuation(a) || SymbLabel::dot(a))
    {
        assert(checkPost(env)); // not related to a
        assert(_pre != P_UNKNOWN);
        if (_pre + _interval->lsize() == 0)
        {
            knew->addTerminalRun(0, t, st, env); // run with 0 points
            // 0 points to read in interval or
        } // otherwise : do not update stack
    }
    /// exactly 1 event in pre + left-half of interval and it is an offset
    /// @todo missing case: rest preceeded by grace notes (g.n. are then ignored)
    /// here it is excluded. we consider only one rest alone.
    else if (SymbLabel::rest(a))
    {
        assert(checkPost(env)); // should be unknown
        assert(env.segment);
        TRACE("KeySIO: terminal rest interval: {} ({}aligned) for transition {}",
              *(_interval), ((! _interval->aligned())?"not ":""), t);

        // 1 offset in pre (and nothing in left-half of interval)
        if ((_pre == 1) && (_interval->lsize() == 0) && (! _pre_laston))
        {
            knew->addTerminalRun(1, t, st, env); // 1 point to read in interval
        }
        // 1 offset in left-half of interval (and nothing in pre)
        else if ((_pre == 0) && (_interval->lsize() == 1))
        {
            // last point = first point
            size_t last = _interval->lfirst();
            assert(env.segment);
            if (env.segment->point(last).offset())
                knew->addTerminalRun(1, t, st, env); // 1 point to read
        }
        // special case of performance beginning with a gap (not an event).
        else if ((_pre == 0) &&
                 (_interval->lsize() == 0) &&
                 DBL_APPROX_EQUAL(_interval->rbegin(), 0))
                 // (_interval->rbegin() == 0)
        {
            WARN("KeySIR: first event in performance is not at 0, padding with a rest");
            knew->addTerminalRun(0, t, st, env);  // 0 point to read
        }
        // otherwise : do not update stack
    }
    /// last event in pre + left-half of interval is an onset
    else if (SymbLabel::note(a))
    {
        assert(checkPost(env)); // should be unknown
        assert(_pre != P_UNKNOWN);
        assert(_pre_on != P_UNKNOWN);
        TRACE("KeySIO: terminal note interval: {} ({}aligned) for transition {}",
              *(_interval), ((! _interval->aligned())?"not ":""), t);

        if (_interval->lsize() == 0)
        {
            if (_pre_laston && // false if pre empty
                // nb of g.n. + 1 note = nb of onsets
                (SymbLabel::nbEvents(a) == _pre_on))
            {
                knew->addTerminalRun(_pre, t, st, env);
                // _pre points to read in interval (onsets and offsets)
            }
        }
        else
        {
            // last point is onset?
            size_t last = _interval->lfirst();
            last += (_interval->lsize() - 1);
            assert(last < _interval->rfirst());
            assert(last < _interval->next());
            assert(env.segment);
            if (env.segment->point(last).onset() &&
                    // nb of g.n. + 1 note = nb of onsets
                    (SymbLabel::nbEvents(a) ==
                     (_pre_on + _interval->lonsets())))
            {
                size_t n = _pre + _interval->lsize();
                knew->addTerminalRun(n, t, st, env);
                // (_pre + _interval->lsize()) points to read (onsets and offsets)
            }
        }
    }
    /// double bar (end of score)
    else if (SymbLabel::bar(a) && (SymbLabel::arity(a) == 0))
    {
        TRACE("KeySIO: terminal bar interval: {} ({}aligned) for transition {}",
              *(_interval), ((! _interval->aligned())?"not ":""), t);
        // && (DBL_APPROX_EQUAL(_interval->rend(), _interval->rbegin()))
        if ((_interval->rduration() < _barlen) && (! _interval->inhabited()))
        {
            knew->addTerminalRun(0, t, st, env);
            // 0 or -1 point to read in interval
        } // otherwise : do not update stack
    }
    else if (SymbLabel::dummy(a))
    {
        ERROR("KeySIO: unexpected dummy symbol");
    }
    else
    {
        ERROR("KeySIO: unexpected terminal symbol {}", SymbLabel::to_string(a));
    }
}


void KeySIO::addTerminalRun(//const KeySIO& k,
                            size_t n, // nb points
                            const Transition& t,
                            StackTerm<KeySIO>& st,
                            // std::stack<std::pair<const KeySIO*, RunTerm*>>& st,
                            const ParsingEnv& env) const
{
    assert(this->complete());
    const label_t& a = t.label();
    Weight w = terminalWeight(a, t, env);
    assert(! w.unknown());
    RunFactory<KeySIO>::RunTermmk(*this, t, a, w, n, st);
}


bool KeySIO::checkPost(const ParsingEnv& env) const
{
    return (_post == P_UNKNOWN) ||
            ((_post == _interval->rsize()) &&
             (_post_on == _interval->ronsets()) &&
             (_post_laston == laston(env)));
}


bool KeySIO::laston(const ParsingEnv& env) const
{
    assert(_interval);
    assert(_interval->aligned());
    if (_interval->rsize() == 0) return false;
    
    // index of last point in right half of interval
    size_t last = _interval->rfirst();
    last += (_interval->rsize() - 1);
    assert(last < _interval->next());
    assert(env.segment);
    const MusPoint& p = env.segment->point(last);
    return p.onset();
}


Weight KeySIO::terminalWeight(label_t a,
                              const Transition& tr,
                              const ParsingEnv& env) const
{
    assert(tr.terminal());
    assert(SymbLabel::terminal(a));
    assert(this->complete());

    // tweight = transition weight = complexity
    Weight tweight = tr.weight(); // copy
    
    assert(! tweight.unknown());

    // terminal bar interval:
    // there are no points hence no need to compute the distance
    if (SymbLabel::bar(tr.label()) && (SymbLabel::arity(tr.label())) == 0)
    {
        TRACE("KeySIO: terminal bar interval: {} ({}aligned) for transition {}",
              *(_interval), ((! _interval->aligned())?"not ":""), tr);
        assert(tr.label() == a);
        assert(! _interval->inhabited());
        assert(_interval->rduration() < _barlen);
    }
    // combine transition weight with distance weight
    // CST_WEIGHT_TYPE == WeightDom::STOCHASTIC
    else if (tweight.hasType("ViterbiWeight"))
    {
        assert(env.segment);
        assert(_interval);
        TRACE("KeySIO: new bar interval: {} ({}aligned) for transition {}",
              *(_interval), ((! _interval->aligned())?"not ":""), tr);
        //assert(_pre != PP_UNKNOWN);          // csq complete
        //assert(_post != PP_UNKNOWN);         // csq complete
        //assert(_post == _interval->rsize()); // csq filterTerminal by leaf
        tweight *= Weight(new GaussianDistance(env.segment, _interval));
    }

    // CST_WEIGHT_TYPE == WeightDom::PENALTY
    else if (tweight.hasType("TropicalWeight"))
    {
        assert(_interval);
        // compute distance to input segment
        // ALPHA applied to dist only (not complexity)
        Weight dist = Weight(new Distance(env.segment, _interval));
        TRACE("KeySIO: tropical distance for inter={} tr={} dist={}",
              *(this), tr, dist);

        // new weight = (1 - alpha) . former transition weight * alpha . distance
        // update: alpha applied to dist only (not to complexity)
        tweight *= dist; // tropical weight product is a sum
    }
    else
    {
        ERROR("KeySIO.weight: unexpected weight type");
        // tweight will be UNDEF
    }
    
    return tweight;
}


void KeySIO::RunsInner(const Transition& t,
                       StackInner<KeySIO>& si,
                       // std::stack<RunInner<KeySIO>*>& si,
                       const ParsingEnv& env) const
{
    const label_t& a = t.label();
    assert(SymbLabel::inner(a));
    
    if (SymbLabel::tuple(a))
    {
        assert(_interval);
        // we do not divide empty intervals
        if (_interval->inhabited())
            RunFactory<KeySIO>::RunInnermk(*this, t, si);
        else
            TRACE("RunsInner: empy interval, do not divide {}", *this);
    }
    else if (SymbLabel::grace(a))
    {
        assert(SymbLabel::arity(a) == 2);
        ERROR("KeySIO: outside g.n. not supported, label={}",
              SymbLabel::to_string(a));
    }
    else if (SymbLabel::fork(a))
    {
        assert(SymbLabel::arity(a) == 2);
        ERROR("KeySIO: voice fork not supported, label={}",
              SymbLabel::to_string(a));
    }
    else if (SymbLabel::bar(a))
    {
        assert(SymbLabel::arity(a) == 2);
        assert(_interval);
        if ((_interval->rduration() >= _barlen) || _interval->inhabited())
            RunFactory<KeySIO>::RunInnermk(*this, t, si);
        else
            TRACE("RunsInner: do not add bar (fine) {}", *this);
    }
    else
    {
        ERROR("KeySIO: unexpected inner symbol {}", SymbLabel::to_string(a));
    }
}


std::ostream& operator<<(std::ostream& o, const KeySIO& p)
{
    if (p._interval == NULL)
    {
        o << KEY_LPAR << p.state() << KEY_RPAR;
        return o;
    }
    
    o << KEY_LPAR;
    o << " " << p.state() << " ";
    
    // short version
    if (p._interval)
    {
        o << *(p._interval);
    }
    else
    {
        o << "[]";
    }
    assert(p._pre != P_UNKNOWN);
    o << " pre=" << p._pre;
    o << " pre_on=" << p._pre_on;
    o << " pre_laston=" << p._pre_laston;

    if (p._post == P_UNKNOWN)
        o << " post=UNK";
    else
    {
        o << " post=" << p._post;
        o << " post_on=" << p._post_on;
        o << " post_laston=" << p._post_laston;
    }
    o << KEY_RPAR;
    return o;
}



} // end namespace parsing

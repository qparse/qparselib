//
//  RunInner.hpp
//  GTests
//
//  Created by Florent Jacquemard on 19/03/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{

#ifndef RunInner_hpp
#define RunInner_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <assert.h>
#include <vector>


#include "trace.hpp"
#include "SymbLabel.hpp" // symbolic labels
#include "Weight.hpp"
#include "Transition.hpp"
#include "Runey.hpp"
#include "RunInnero.hpp"
#include "Recordey.hpp"


namespace parsing{

//template<class K>
//class RunInner;

//template<class K>
//std::ostream& operator<<(std::ostream& o, const RunInner<K>& r);


/// An inner run is a particular case of run with at least one subrun.
/// An inner run is complete when the length of the list of children is the arity.
/// It is partial otherwise.
/// param K = class of Key in parse table
template<class K>
class RunInner : public RunInnero, public std::enable_shared_from_this<RunInner<K>>
{
public:

    /// inner run initialized with a transition of the base wta.
    /// @param tr origin transition used to build the run. must be inner.
    /// tr gives label and initial weight. It cannot be changed afterwards.
    /// The constructed run is partial (empty list of subruns).
    RunInner(const Transition& tr);
    
    /// inner run initialized  a transition of the base wta
    /// and some label and weight values which may differ from this transition.
    /// @param tr origin transition used to build the run. must be inner.
    /// @param lab given label. cannot be changed afterwards.
    /// @param w weight must not be unknown. can be updated afterwards.
    /// The constructed run is partial  (empty list of subruns).
    RunInner(const Transition& tr, label_t lab, const Weight& w);

    /// partial copy of terminal run.
    /// empty list of children. 
    RunInner(const RunTerm& r);
    
    /// copy constructor
    RunInner(const RunInner<K>& r);
    
    /// @brief copy/update constructor with push back of a subrun (record).
    /// @param r run must be partial.
    /// @param rec record, must have a complete key.
    /// copy r, and in the copy:
    /// - add rec at the end of children list.
    /// - multiply current weight by the weight of the 1-best run for rec.
    /// - update current filter with the filter of the 1-best run for rec.
    /// @warning if rec has no 1-best run, the weight of the copy
    /// is set to unknown.
    RunInner(const RunInner<K>& r, std::shared_ptr<Record<K>> rec);
        
    /// @brief copy/update constructor with push back of a new best subrun.
    /// @param r run must be a partial.
    /// @param rec must be a complete key.
    /// @param best is a pointer to the best run in rec. must be non-NULL.
    /// the pointed run must be complete.
    /// copy r, and in the copy:
    /// - add rec at the end of children list.
    /// - multiply current weight by the weight of best.
    /// - update current filter with the filter of best.
    RunInner(const RunInner<K>& r,
             std::shared_ptr<Record<K>> rec,
             std::shared_ptr<const Run> best);

    /// destructor
    virtual ~RunInner();
    
    virtual std::shared_ptr<RunInnero> clone() const;
    
    RunInner<K>& operator= (const RunInner<K>& rhs);
    
    bool operator==(const RunInner<K>& rhs) const;
    
    /// @return current number of children for this run.
    /// it can be smaller than the arity in case of multiplicities.
    /// it can be smaller than the number of expected children when this run is partial.
    virtual size_t size() const;

    /// @return this run has currently no children.
    bool empty() const;

    /// an inner run is complete when its list of children has the expected size.
    virtual bool complete() const;
    
    /// @return the multiplicity of the ith children.
    /// = number of edges between the top of this run into its ith subrun.
    size_t multiplicity(size_t i) const;
    
    /// @param i the index of the subrun, between 0 and size()-1.
    /// @return the ith subrun or NULL if there is none.
    /// @warning this run must be complete.
    std::shared_ptr<const Run> subrun(size_t i) const;

    /// return the ith key of this run.
    /// @param i index of subrun, must be between 0 and run size - 1.
    const K& key(size_t i) const;
    
    /// Visitor
    virtual size_t addto(Recordo& rec) const;
    //virtual size_t addto(std::shared_ptr<Recordo> rec) const;

    void print(std::ostream& o) const;
       
protected:
    
    /// pointers (keys) to children sub-runs.
    typename std::vector<std::shared_ptr<Record<K>>> _children;
    
    /// [opt] data for optimization.
    /// backup of the best runs used for construction.
    /// @todo TBR. not used.
    typename std::vector<std::shared_ptr<const Run>> _subruns;
    
    /// @param i the index of the subrun, between 0 and arity-1.
    /// @param n the rank for n-best.
    ///        must be equal to 1 for unranked runs.
    ///        must be larger or equal to 1 for ranked runs.
    /// @return the ith subrun or NULL if there is none.
    /// @warning this run must be complete.
    std::shared_ptr<const Run> subrun(size_t i, size_t n) const;

    // return the ith record of this run.
    // @param i index of subrun, between 0 and arity-1.
    // Record<K>* operator[](size_t i) const;
            
private:
    
    /// add the given record as new subrun and update the weight of this run
    /// with the weight of the given best subrun.
    /// @param rec record added as new subrun. must not be NULL and have a key.
    /// @param best subrun to add. must not be NULL.
    void init_best(std::shared_ptr<Record<K>> rec,
                   std::shared_ptr<const Run> best);
    
};


template<class K>
std::ostream& operator<<(std::ostream& o, const RunInner<K>& r)
{
    r.print(o);
    return o;
}


} // end namespace parsing

// separated definition of template class
#include "RunInner.tpp"

#endif /* RunInner_hpp */

/// @}

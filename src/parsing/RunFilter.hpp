//
//  RunFilter.hpp
//  squant
//
//  Created by Florent Jacquemard on 28/03/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{


#ifndef RunFiltery_hpp
#define RunFiltery_hpp

#include <stdio.h>

#include "SymbLabel.hpp"
#include "DuraList.hpp"
#include "RunFilterMode.hpp"

namespace parsing{

/// structure associated to a run, with information for filtering the run,
/// according to optimisation flags which are static constants of the class.
///
/// the runs not filtered are simplifiable to a smaller run
/// (e.g. a single event or a continuation).
/// This property is checked incrementaly, with a FSM.
struct RunFilter
{
public:
       
    /// @brief list of relative durations.
    /// empty at initialization or when OPT_RUN_DUR unset.
    DurationList duration;
    
    /// @brief initial filter.
    /// @param lab given label. cannot be changed.
    /// @param size number of subruns (without counting multiplicities).
    RunFilter(label_t lab, size_t size);

    /// @brief copy constructor.
    RunFilter(const RunFilter&);

    virtual ~RunFilter();
    
    /// @brief assignement.
    virtual RunFilter& operator=(const RunFilter&);

    /// number of expected subruns counting multiplities.
    size_t arity() const;

    /// number of expected subruns without counting multiplities.
    size_t size() const;

    /// @brief the filter has been completely evaluated.
    bool complete() const;

    /// @brief the containing run is filtered,
    /// according to the OPT_RUN_XXX options.
    /// @warning this filter must be complete.
    bool filter() const;

    /// @brief the associated run is a unit
    ///        (see OPT_RUN_UNIT for definition).
    /// @warning this filter must be complete.
    /// @warning OPT_RUN_UNIT must be set
    bool unit() const;
    
    /// @brief the associated run is a continuation
    ///        (see OPT_RUN_CONT for definition).
    /// @warning this filter must be complete.
    /// @warning OPT_RUN_UNIT or OPT_RUN_CONT must be set
    bool continuation() const;

    /// @brief reset filter to its initial state.
    /// @param lab given label like in constructor.
    /// @param s number of subruns (without counting multiplicities).
    void reset(label_t lab, size_t s);

    /// @brief update states of filter with new sub-run (of inner run).
    /// - the state state_unit is updated only if the flag OPT_RUN_UNIT is set
    /// - the state state_cont is updated only if the flag OPT_RUN_CONT is set
    /// @param f must be a complete filter (for sub-run).
    /// @warning this filter must be uncomplete.
    void update(const RunFilter& f);
    
private:

    
    label_t _label;
    
    /// number of children.
    /// @warning it may differ from label's arity (out degree) which is size
    /// times multiplicity of each children.
    size_t _size;
    
    /// state can have the following values (1 = unit, - = continuation):
    /// see RunFilterMode.hpp : OPT_RUN_UNIT for definition of unit
    /// - 0 : empty - initial state (uncomplete)
    /// - 1 : unit and unfiltered   (may be uncomplete) [1 - ... -]
    /// - 2 : unit and filtered     (may be uncomplete) [1]
    /// - 3 : not-unit and filtered (may be uncomplete)
    /// - 4 : error
    unsigned char _state_unit;

    /// can have the following values  (- = continuation):
    /// see RunFilterMode.hpp : OPT_RUN_CONT for definition of cont
    /// - 0 : empty - initial state (uncomplete)
    /// - 1 : cont and unfiltered   (may be uncomplete) [- ... -]
    /// - 2 : cont and filtered     (may be uncomplete) [-]
    /// - 3 : not-cont and filtered (may be uncomplete)
    /// - 4 : error
    unsigned char _state_cont;
    
    /// number of subruns evaluated.
    size_t _done;
        
    /// the flag must be updated.
    bool update_flag() const;
    
    void _init_states();
    void _init_state_unit();
    void _init_state_cont();

};

} // end namespace parsing


#endif /* RunFilterys_hpp */


/// @}

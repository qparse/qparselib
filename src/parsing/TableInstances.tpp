//
//  TableInstances.tpp
//  squant
//
//  Created by Florent Jacquemard on 05/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

namespace parsing{


template <class K, class R, class H>
TableInstances<K,R,H>::TableInstances():
_instances(),
_rows(0)
{ };


template <class K, class R, class H>
TableInstances<K,R,H>::~TableInstances()
{
    TRACE("delete table of instances");
    TRACE("Instance Table: nb of entries (partial keys): {}", _rows);
    TRACE("Instance Table: size   (nb of complete keys): {}", _instances.size());
    
    _instances.clear();
}


template <class K, class R, class H>
void TableInstances<K,R,H>::add(const K& kp, std::shared_ptr<R> rec)
{
    assert(kp.partial());
    assert(rec);

    const K& kc = rec->key();
    assert(kc.complete());
    assert(kc.instance(kp));
    
    if (count(kp) == 0)
    {
        TRACE("Instance Table: new key {}", kp);
        ++_rows;
    }
    
    // kc may have been registered to table (by former add(kp, kc)
    if (exists(kp, rec))
    {
        TRACE("Instance Table: pair already registered, skip: {}, {}", kp, kc);
    }
    else
    {
        TRACE("Instance Table: add pair {}, {}", kp, kc);
        // return iterator to the inserted element
        _instances.emplace(kp, rec);
        // calling constructors
//      _instances.emplace(std::piecewise_construct,
//                         std::forward_as_tuple(kp),   // copy of kp
//                         std::forward_as_tuple(rec));
    }
}


template <class K, class R, class H>
std::pair<typename MapInstances<K,R,H>::iterator,
          typename MapInstances<K,R,H>::iterator>
TableInstances<K,R,H>::get(const K& kp)
{
    assert(kp.partial());
    
    return _instances.equal_range(kp);
}


template <class K, class R, class H>
size_t TableInstances<K,R,H>::count(const K& kp) const
{
    assert(kp.partial());

    return _instances.count(kp);
}


template <class K, class R, class H>
bool TableInstances<K,R,H>::exists(const K& kp,
                                   std::shared_ptr<const R> it) const
{
    assert(kp.partial());
    assert(it);
    assert(it->key().complete());
    assert(it->key().instance(kp));
    
    std::pair<typename MapInstances<K,R,H>::const_iterator,
              typename MapInstances<K,R,H>::const_iterator> pit =
        _instances.equal_range(kp);
    
    for (typename MapInstances<K,R,H>::const_iterator i = pit.first;
         i != pit.second; i++)
    {
        assert(i->first == kp);
        if (i->second->key() == it->key())
            return true;
    }
    return false;
}


template <class K, class R, class H>
void TableInstances<K, R, H>::dump() const
{
    DEBUGU("Instance Table: nb of entries (partial keys): {}", _rows);
    DEBUGU("Instance Table: size   (nb of complete keys): {}", _instances.size());

    for (typename MapInstances<K,R,H>::const_iterator i=_instances.cbegin();
         i != _instances.cend(); i++)
    {
        const K& kp = i->first;  // key
        // assert(kp.partial());
        assert(i->second);       // record
        K& kc = i->second->key();
        // assert(kc.complete());
        // assert(kc.instance(kp));

        DEBUGU("Instance Table[{}] = {}", kp, kc);
    }
}


} // end namespace parsing

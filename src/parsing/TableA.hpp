//
//  TableA.hpp
//  qparse
//
//  Created by Florent Jacquemard on 07/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{

#ifndef TableA_hpp
#define TableA_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <assert.h>

#include "trace.hpp"
#include "Runey.hpp"
//#include "RunCompare.hpp"
#include "RunOrder.hpp"
//#include "ParsingEnv.hpp"
#include "LRT.hpp"

namespace parsing{

class ParsingEnv;

/// abstract interface to Parsing Table
/// as template with one single argument
/// K = concrete key class (keys in table)
template<class K>
class TableA
{
        
public:
   
    /// @param comp binary function for comparing runs
    /// @param env the parsing environment.
    TableA(ParsingEnv& env, const RunOrder& comp);
    
    virtual ~TableA();
    
    /// @brief n-th best run targeting the key (augmented state) k.
    /// @param k key can be partial or complete.
    /// @param n (as in n-best) is the rank of the run with target the key
    ///        of the record.
    /// @return if k is complete: the n-th best run targeting k
    ///         or a special fail run if there is none,
    ///         if k is partial: the n-th best run targeting one complete instance of k
    ///         reachable by the augmented automaton,
    ///         or  a special fail run if it does not exist.
    virtual std::shared_ptr<const Run> best(const K& k, size_t n=1) = 0;
    
    // rhythm tree associated (by projection) to the n-best run.
    // @param k key can be partial or complete.
    // @param n (as in n-best) is the rank of the run with target the key
    //        of the record. must be larger or equal to 1 (default).
    // @return an allocated best tree associated to the best run best(k, n)
    //         (must be deleted)
    //         or NULL if there is none.
    // virtual LabeledRhythmTree* bestTree(const K& k, size_t n=1) = 0;

    // Rhythm Tree associated (by projection) to a run.
    // @param r run in table.can be NULL.
    // @return an allocated best tree associated to the given run
    //         or NULL if run is NULL.
    // virtual LabeledRhythmTree* bestTree(const Run* r) = 0;
    
    // add possible instances of run r
    // to the entries in table for corresp. to possible instances for p.
    // dispatch to the four functions below according to p and r.
    // @param k key can be complete or partial.
    // @param r run can be complete or partial.
    // @param i record
    // if k is complete, then i must point to the entry for k in table,
    // otherwise (k partial), i is table.end()  NULL.
    //virtual size_t add(const K& k, Run* r, Record<K>* i) = 0;
    
    virtual size_t nb_entries() const = 0;
    virtual size_t nb_runs() const = 0;
    
protected:
    
    /// parsing environment (tree automaton and input)
    /// not const: contains a pool of transition that may be modified.
    ParsingEnv& _env;

    /// function for the comparison of runs.
    const RunOrder& _comparer;
    
};

} // end namespace parsing

// separated definition of template class
#include "TableA.tpp"

#endif /* TableA_hpp */

/// @}

//
//  RunFilterMode.hpp
//  qparse
//
//  Created by Florent Jacquemard on 31/05/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{


#ifndef RunFilterMode_hpp
#define RunFilterMode_hpp

namespace parsing{


/// Run Filter Modes
/// optimization flags:
/// modes for Run filtering
//namespace RFM
//{
    
    /// optimization flag:
    /// prevent to add non-terminal runs called *unit*,
    /// which consist in grace notes followed by 1 note
    /// followed by continuations (ties),
    /// i.e. runs with duration sequences of the form `[0…0, 1]`.
    /// Such a run can be simplified into a terminal (leaf) run
    /// containing grace-notes + 1 note.
    ///
    /// formaly, a *unit* run is either
    /// - an inside-the-bar terminal symbol with one event at least.
    /// - a run of the form a(unit, cont,...,cont)
    ///   where a is an inside-the-bar inner (arity>0) symbol.
    /// the first case is filtered.
    /// the second case is not filtered.
    /// all other (non-unit) runs are filtered.
    ///
    /// the value of this flag is specified in ini file or default: true
    extern bool OPT_RUN_UNIT;
    
    /// optimization flag:
    /// do not add non-terminal runs called *continuation*
    /// which consist in a sequence of continuations (tie).
    /// Such a run can be simplified into a terminal (leaf) run
    /// containing a single continuation.
    ///
    /// formaly a *continuation* run is either
    /// - an inside-the-bar terminal symbol with zero event.
    /// - a run of the form a(cont,...,cont)
    ///   where a is an inside-the-bar inner (arity>0) symbol.
    /// the first case is filtered.
    /// the second case is not filtered.
    /// all other (non-unit) runs are filtered.
    ///
    /// value specified in ini file or default: true
    extern bool OPT_RUN_CONT;
    
    /// optimization flag:
    /// compute the duration list of the run associated to this structure.
    ///
    /// value specified in ini file or default: false
    /// @warning if unset, `OPT_RUN_STRICT` must be automatically unset
    extern bool OPT_RUN_DUR;
    
    /// optimization flag
    /// compute at most one best run for a duration sequence in each record.
    /// it is the first best run added,
    /// i.e. the best with that duration sequence.
    ///
    /// value specified in ini file or default: false
    /// @warning `OPT_RUN_DUR` must be set.
    /// @todo OPT_RUN_STRICT is not implemented.
    extern bool OPT_RUN_STRICT;

//};

} // end namespace parsing


#endif /* RunFilterMode_hpp */


/// @}


//
//  RunRanked.hpp
//  squant
//
//  Created by Florent Jacquemard on 07/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{

#ifndef RunRanked_hpp
#define RunRanked_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <assert.h>
#include <vector>


#include "trace.hpp"
#include "Runey.hpp"
#include "RunInner.hpp"


namespace parsing{

//template<class K> class Run;

/// extension of the inner Run with a rank value for each children.
/// for k-best computation with the algo of Huang & Chiang 2005.
template<class K>
class RunRanked : public RunInner<K>
{

public:

    // @brief run with empty children list.
    // arity a of run is the arity of given label.
    // run is complete if a == 0 (terminal run), it is partial otherwise.
    // @param lab given label.  cannot be changed afterwards.
    // @param w weight must not be unknown. can be updated afterwards.
    //RunRanked(label_t lab, const Weight& w);

    /// @brief copy and simulate.
    /// @param r a run. must be unranked (base class) and complete.
    RunRanked(const RunTerm& r);

    /// @brief copy and upgrade.
    /// @param r a run. must be unranked (base class) and complete.
    RunRanked(const RunInner<K>& r);

    /// @brief copy.
    /// @warning. r must be complete.
    RunRanked(const RunRanked<K>& r);
        
    /// @brief copy and increment.
    /// @param r ranked run to copy.
    /// @param i index of the children whose rank will be incremented in the copy.
    /// @warning the weight of the copy is deleted and set as unknown.
    /// @warning the filter of the copy is reset.
    RunRanked(const RunRanked<K>& r, size_t i);
    
    /// @brief recompute the weight and filter of this run.
    /// - set the weight of this run to its initial weight,
    ///   and filter to its initial state.
    /// - multiply by the the weight of each subrun,
    ///   and update filter for each subrun.
    /// If one of the subrun is NULL, the weight is set to unknown
    /// (filter is left uncomplete in this case).
    /// @warning the weight of every (non-NULL) subrun must not be unknown.
    /// @warning this run must be complete.
    void reset();
        
    virtual bool ranked() const { return true; }
    
    inline size_t length() const { return _length; }
    
    /// @param i the index of the subrun, between 0 and size()-1.
    /// @return the ith subrun or NULL if there is none.
    /// @warning this run must be complete.
    virtual std::shared_ptr<const Run> subrun(size_t i) const;

    void print(std::ostream& o) const;
    
protected:
    
    /// ranks of children. mutable. must have the same size as _children.
    typename std::vector<size_t> _ranks;
       
    /// for the copy of RunTerm (the run is complete).
    bool _complete;
    
    /// for the copy of RunTerm.
    /// number of input points (events) parsed by this run.
    size_t _length;

};


template<class K>
std::ostream& operator<<(std::ostream& o, const RunRanked<K>& r)
{
    r.print(o);
    return o;
}


} // end namespace parsing


// separated definition of template class
#include "RunRanked.tpp"

            
#endif /* RunRanked_hpp */

/// @}

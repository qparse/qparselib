//
//  RecordQueue.hpp
//  qparse
//
//  Created by Florent Jacquemard on 12/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{

#ifndef RecordQueue_hpp
#define RecordQueue_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <queue>          // std::priority_queue
#include <vector>         // std::vector

#include "Recordey.hpp"
#include "RunRanked.hpp"

namespace parsing{

/// @brief record associated to Key for k-best algorithms.
template<class K>
class RecordQueue : public Record<K>
{
public:
    /// @brief empty record associated to given key.
    /// @param k key associated to the record
    /// @param comp function for comparing Runs over K.
    RecordQueue(std::shared_ptr<const K> k,
                std::shared_ptr<const RunOrder> comp);
    
    /// copy
    RecordQueue(const RecordQueue&);

    /// deleting a RecordQueue frees all runs contained
    /// in the candidate queue and the best list.
    virtual ~RecordQueue();
    
    /// @brief assignement.
    virtual RecordQueue<K>& operator= (const RecordQueue<K>& rec);

    virtual std::shared_ptr<Recordo> clone() const;
    
    /// @brief n-th best run of the record.
    /// @param n (as in n-best) is the rank of the run
    ///        with target to the key of the record.
    /// @return the n-th best run of the record or, if there is none,
    /// the failRun set or NULL when the failRun was not set with setFailRun.
    /// Fill the list of best runs up to (at most) n if necessary.
    /// If less than k best can be constructed (table is complete),
    /// return a fail run or NULL run (weight unknown),
    /// otherwise, the weight of the returned run is known.
    virtual std::shared_ptr<const Run> best(size_t n=1);

    virtual bool empty() const;

    /// @brief add a run to the record or delete.
    /// Visitor of Runs dispatching over two Run types.
    /// @param r candidate run to add.  must be complete.
    /// r is ignored (not added) if it is NULL,
    /// its weight is unknown, or zero. @see Record::valid
    /// @warning the key of this record must be set.
    /// @warning if r is not added, it is deleted.
    virtual size_t addTerm(std::shared_ptr<const RunTerm> r);
    virtual size_t addInner(const std::shared_ptr<const RunInnero> r);
    virtual size_t addInner(std::shared_ptr<const RunInner<K>> r);
        
protected:
    /// heap of candidate runs for the associated state.
    /// it is empty iff no more k-best can be added
    std::priority_queue<std::shared_ptr<RunRanked<K>>,
                        std::vector<std::shared_ptr<RunRanked<K>>>,
                        RunOrder::Compare> _cand;
    
    /// ordered list of best runs for the associated state.
    std::vector<std::shared_ptr<const Run>> _best;
    // std::vector<RunRanked<K>*> _best;
    
    /// add Run r to the queue of candidates
    /// after some filtering based on optimisation flags.
    /// @param r given Run, must not be NULL,
    ///        must be complete,
    ///        its weight can be unknown.
    /// @return the number of runs added.
    /// @warning if the run is filtered out, then it is deleted.
    size_t addCand(std::shared_ptr<RunRanked<K>> r);

    /// extract one run from the queue of candidates,
    /// optimal in the sense of _comp function.
    std::shared_ptr<RunRanked<K>> popCand();

    /// there are no more candidate.
    bool emptyCand() const;
    
    /// add Run r at the end of the list of best runs after filtering.
    /// record the given run r as one of the best runs of the record.
    /// @param r run must not be NULL,
    ///        must be complete,
    ///        its weight must not be unknown and not be zero.
    /// @warning runs filtered out will be deleted.
    void addBest(std::shared_ptr<RunRanked<K>> r);

    /// @return the n-th best run.
    /// @param n index of wanted best run. must be between 1 and
    /// the number of best runs computed (@see bestSize()).
    std::shared_ptr<const Run> getBest(size_t n);

    /// @brief number of best runs computed.
    size_t sizeBest() const;
    
    // bool bestFilter(const Run* r);
    
    /// add the candidates following Run r (lexico order for ranks)
    /// to the heap of candidates.
    /// @param r must not be NULL, must be complete,
    ///        its weight must not be unknown or zero.
    /// the nexts Run (new candidates) will be of unknown weight
    /// and with incomplete filters (initial state).
    void addNext(std::shared_ptr<RunRanked<K>> r);
    
private:
    
    /// common code to addTerm and addInner.
    size_t generic_add(std::shared_ptr<RunRanked<K>> r);
    
};
   

} // end namespace parsing


// separated definition of template class
#include "RecordQueue.tpp"


#endif /* RecordQueue_hpp */

/// @}

//
//  Runey.hpp
//  squant
//
//  Created by Florent Jacquemard on 26/03/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
// runs where children (pointer to subruns)
// are pointers to record
// (every record empbed the associated complete key in parse table).
//
// advantage: efficiency: no needed lookup for the key in parse table
// to get the associated record hence best run.
//
/// @addtogroup parsing
/// @{

#ifndef RuneyTemplate_hpp
#define RuneyTemplate_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <assert.h>
#include <vector>

#include "trace.hpp"
#include "SymbLabel.hpp" // symbolic labels
#include "Weight.hpp"
#include "Transition.hpp" 
//#include "Recordey.hpp"
#include "RunFilter.hpp"
//#include "Recordey.hpp"


namespace parsing{

class Recordo;
template<class K> class Record;


/// abstract class for runs,
/// which are compact representations of parse trees,
/// as a tuple of pointers to subruns.
///
/// a run is associated with as an augmented transition, with
/// - a label
/// - an arity value (number of children expected)
/// - a list of children.
///   every child is represented by a complete key
///   (pointer to a row in parse table)
/// - a weight (updatable)
/// - a list of relative durations
class Run
{
public:
    
    // null run.
    // unknown weight, empty children list.
    // Run(size_t a = 0);
        
    /// run with empty children list initialized with a transition of the base wta.
    /// @param tr origin transition used to build the run.
    /// tr gives label and initial weight. It cannot be changed afterwards.
    Run(const Transition& tr);
    
    /// run with empty children list, initialized  a transition of the base wta
    /// and some label and weight values which may differ from this transition.
    /// @param lab given label. cannot be changed afterwards.
    /// @param w weight must not be unknown. can be updated afterwards.
    /// The arity of the run is the arity of given transition.
    /// run is complete if arity == 0 (terminal run), it is partial otherwise.
    Run(const Transition& tr, label_t lab, const Weight& w);
    
    // run with empty children list - for backward compatibility.
    // it is complete if a == 0 (terminal run), it is partial otherwise.
    // @param a positive arity value. must be the arity of lab.
    // @param lab given label.  cannot be changed afterwards. arity of label must be a.
    // @param w weight must not be unknown. can be updated afterwards.
    // @@todo TBR. for backward compatibility.
    // Run(size_t a, label_t lab, const Weight& w);
     
    /// copy construtor.
    Run(const Run& r);
          
    /// deleting a run does not free the records
    /// pointed in the list of children (this list contains only8u p;
    virtual ~Run();
    
    Run& operator= (const Run& rhs);
    
    bool operator==(const Run& rhs) const;
    
    /// @return label of this run.
    inline label_t label() const { return _label; }
    
    /// current weight.
    inline const Weight& weight() const { return _weight; }
    
    /// Transition of the base WTA used to build this run.
    inline const Transition& origin() const { return _origin; }
    
    inline const RunFilter& filter() const { return _filter; }

    /// @return out degree expected for this run.
    /// @warning it may not be the current number of childen
    /// when this run is not complete or when children have multiple edges.
    size_t arity() const; // { return SymbLabel::arity(_label); }

    /// @return actual number of children for this run.
    /// it can be smaller than the arity in case of multiplicities.
    /// it can be smaller than the number of expected children when this run is partial.
    virtual size_t size() const = 0;
    
    /// list of relative durations.
    inline const DurationList duration() const { return _filter.duration; }

    /// this run is considered as complete.
    virtual bool complete() const = 0;
    
    /// this run is not considered as complete.
    inline bool partial() const { return (! complete()); }

    /// this run passes successfully its filter.
    bool filtered() const;

    /// this run is terminal (leaf).
    virtual bool terminal() const = 0;
    
    /// this run is inner.
    virtual bool inner() const = 0;
       
    /// this run is a ranked run
    virtual bool ranked() const { return false; }
    
    /// this run is a fail run.
    bool fail() const;
    
    /// call back visitor for dispatch.
    //virtual size_t addto(std::shared_ptr<Recordo> rec) const = 0;
    virtual size_t addto(Recordo& rec) const = 0;

    /// should not be called for this abstract class
    void print(std::ostream& o) const;
    
protected:
    /// fixed symbol.
    label_t _label;
    /// @todo obtain from _origin ?

    /// current weight.
    Weight _weight;
    
    /// filter for optimization
    RunFilter _filter;
    
    /// initial weight at construction of run.
    /// needed when recomputing the run's weight.
    /// @warning may be different from _origin.weight()
    Weight _initweight;
       
    /// Transition of the base WTA used to build this run.
    const Transition& _origin;
    // gives _label, _initweight -> protected functions

    // make a copy and in copy, reset the weight to w and reset the filter.
    // Run(const Run& r, Weight w);>>>>>>> termirun
        
    // inline const RunFilter& filter() const { return _filter; }
    
    /// @brief reset the weight of this run to its initial weight.
    /// utility for internal use
    void weightReset();
    
    /// @brief reset the weight of this run to an unknown weight.
    /// utility for internal use
    void weightUnknown();

    /// @brief update the weight of this run
    /// by multiplying it with the given weight.
    /// @param w weight of a subrun. must not be unknown.
    /// @warning the weight of this run must not be unknown.
    /// utility for internal use
    void weightUpdate(const Weight& w);
   
    /// @brief reset the filter of this run to its initial state.
    /// utility for internal use
    void filterReset();

    /// @brief update state of this run's filter.
    /// @param f filter of sub-run.
    /// @warning this run must be inner.
    /// utility for internal use
    void filterUpdate(const RunFilter& f);

};

std::ostream& operator<<(std::ostream& o, const Run& r);

} // end namespace parsing


// to solve circular dependencies
//#include "Recordey.hpp"

// separated definition of template class
//#include "Runey.tpp"

//#include "RunCompare.hpp"

#endif /* RuneyTemplate_hpp */

/// @}

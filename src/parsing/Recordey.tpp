//
//  Recordey.tpp
//  squanty
//
//  Created by Florent Jacquemard on 07/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//


// #include "RunCompare.hpp"


namespace parsing{


template<class K>
Record<K>::Record(std::shared_ptr<const K> k,
                  std::shared_ptr<const RunOrder> comp):
_key(k),
_comp(comp),
//_fail(failrun),
_nb_cand(0),
_nb_best_rejected(0)
{
    assert(k);
    //assert(k->complete());
    assert(comp);
    TRACE("new empty Record[{}]", *k);
}


template<class K>
Record<K>::Record(const Record& rec):
_key(rec._key),
_comp(rec._comp),
//_fail(rec._fail),
_nb_cand(rec._nb_cand),
_nb_best_rejected(rec._nb_best_rejected)
{ }


template<class K>
Record<K>::~Record()
{
    TRACE("delete Record");
//    if (_fail) delete _fail;
}


template<class K>
Record<K>& Record<K>::operator= (const Record<K>& rec)
{
    if(this != &rec)
    {
        _key = rec._key;   // share ptr
        _comp = rec._comp; // share ptr
        _nb_cand = rec._nb_cand;
        _nb_best_rejected = rec._nb_best_rejected;
    };
    return *this;
}


template<class K>
const K& Record<K>::key() const
{
    assert(_key != nullptr);
    return *_key;
}


template<class K>
const RunOrder::Compare& Record<K>::compare() const
{
    assert(_comp != nullptr);
    return _comp->compare;
}


template<class K>
std::shared_ptr<const Run> Record<K>::failrun() const
{
    assert(_comp != nullptr);
    assert(_comp->failrun());
    assert(_comp->failrun()->fail());
    return _comp->failrun();
}


//template<class K>
//void Record<K>::setKey(const K& k)
//{
//    //assert(k);
//    // assert(k->complete()); // not for pool of parse table (best partial)
//    //assert(_key == nullptr);  // key can only be set once
//    assert(this->empty()); // before proceeding (initial state)
//    _key = k;
//    TRACE("Record: set key {} to associated new empty record", k);
//}


//template<class K>
//void Record<K>::setFailRun(const RunTerm* r)
//{
//    assert(r);
//    assert(_fail == nullptr); // set only once
//    _fail = r;
//    //_fail = std::shared_ptr<Run>(r);
//}


template<class K>
bool Record<K>::valid(std::shared_ptr<const Run> r)
{
    assert(this->_key != nullptr);
    const K& thiskey = *(this->_key);

    if (r == nullptr)
    {
        ERROR("Record[{}]: NULL run not added", thiskey);
        return false;
    }
    if (r->weight().unknown())
    {
        WARN("Record[{}]: run {} with unknown weight not added", thiskey, *r);
        return false;
    }
    else if (r->weight().is_zero()) // max weight
    {
        ERROR("Record[{}]: run {} with zero weight not added", thiskey, *r);
        return false;
    }
    else
    {
        return true;
    }
}


} // end namespace parsing

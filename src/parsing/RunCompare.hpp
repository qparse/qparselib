//
//  RunCompare.hpp
//  squanty
//
//  Created by Florent Jacquemard on 18/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{


#ifndef RunCompare_hpp
#define RunCompare_hpp

#include <stdio.h>
#include <assert.h>

#include "trace.hpp"
#include "Weight.hpp"
#include "Runey.hpp"

namespace parsing
{

/// orderings for ordered containers
/// used in k-best procedures.
/// @todo OBSOLETE. replaced by RunOrder
using RunCompare =
    std::function<bool(std::shared_ptr<const Run>, std::shared_ptr<const Run>)>;

/// weightMax is one ordering for k-best to rank Runs
/// from best weight to worse weight,
/// where partial run or run with unknown weight
/// is considered to be the best
/// (highest priority for unevaluated candidates)
/// @todo OBSOLETE. replaced by RunOrder
extern RunCompare weightMax;

/// weightMin is one ordering for k-best to rank Runs
/// from worse weight to best weight,
/// where partial runs and runs with unknown weight
/// is considered to be the worst.
/// (highest priority for unevaluated candidates)
/// @todo OBSOLETE. replaced by RunOrder
extern RunCompare weightMin;

} // end namespace

#endif /* RunCompare_hpp */

/// @}

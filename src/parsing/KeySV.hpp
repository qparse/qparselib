//
//  KeySV.hpp
//  equiv2
//
//  Created by Florent Jacquemard on 22/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{


#ifndef KeySV_hpp
#define KeySV_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <assert.h>
#include <utility>      // std::pair, std::make_pair
#include <memory>

#include "util.hpp"
#include "hash.hpp"
#include "MTU.hpp"
#include "ValueList.hpp"
#include "KeyS.hpp"
//#include "Distance.hpp"
//#include "Runey.hpp"

namespace parsing{

//template<class K> class Run;

/// Key (augmented SWTA state) made of
/// - one state of a base WTA
/// - one quantified interval = sequence of rationals (inherited attribute)
class KeySV : public KeyS
{
    friend class KeySVHasher;
    template<typename> friend class RunFactory;

public:
    
    /// @brief top constructor.
    /// allocate an initial key which will serve to create the other keys.
    /// @param seg initial IOI interval, to be decomposed into bars.
    /// @param env input environment.
    KeySV(const ValueList& seg, const ParsingEnv& env);

    /// @brief constructor of a successor of a key to complete an inner run.
    /// @param k target key for the run. can be partial.
    /// @param r an inner run to be completed with the key constructed.
    /// @param env input environment.
    /// There are 4 cases of construction:
    /// - if r is partial and empty, construct the first child k0 to add to r.
    ///   k is the parent of k0 in this case.
    /// - if r is partial but not empty, construct the next sibling kn to add
    ///   to r. k is the parent of kn in this case.
    /// - if r is inner and complete, construct a complete instance of k
    ///   that will be the (complete) target of run r.
    KeySV(const KeySV& k, RunInner<KeySV>& r, const ParsingEnv& env);
    
    // internal
    // @param s initial state of the base WTA.
    // KeySV(state_t s, const ValueList& seg, const ParsingEnv& env);
    
    /// @brief copy constructor.
    KeySV(const KeySV& k);
    
    /// destructor
    virtual ~KeySV();
    
    /// @brief Key assignement.
    virtual KeySV& operator= (const KeySV& k);
    
     /// @brief for use as key in a unordered_multimap.
     virtual bool operator==(const KeySV& p) const;
     virtual bool operator!=(const KeySV& p) const;
         
     /// @brief return wether this Key is an instance of p.
     /// It is equality in this case because KeyS are always complete.
     virtual bool instance(const KeySV& p) const;
     
     /// @brief all attributes of the key are valued
     virtual bool complete() const;
     
     /// @brief accessor
     /// sequence of IOI (rationals) associated to this state.
     virtual const ValueList& value() const { return _list; }

    /// @brief construct (augmented) runs targeting complete instances of
    /// this key.
    /// @param si a stack to be filled with inner runs. must be empty.
    /// @param st a stack to be filled with pairs containing terminal runs.
    /// must be empty.
    /// @param env parsing environment, containing a wta.
    // @return how many runs have been added to the stacks.
    /// @warning the runs constructed are added to the given stacks.
    /// - si will contain inner runs, partial or complete.
    /// - st will contains pairs made of
    ///     - a complete instance k' of k if k is partial, and NULL otherwise.
    ///     - a complete terminal run targeting r'
    /// All the runs in stacks must be deallocate elsewhere.
    void runs(StackInner<KeySV>& si,
              StackTerm<KeySV>& st,
              ParsingEnv& env) const;
    
    /// allocate and return a special Run representing a parse failure for this key.
    /// @param env parsing environment.
    /// @return a pointer to a newly allocated fail run, with
    /// - transition with dummy symbol, empty body, weight one in the
    ///   domain of the base wta in the passed environment.
    /// - fail symbol
    /// - weight one (in the domain of the base wta).
    /// This returned run will answer succesfully to Run.fail().
    std:shared_ptr<RunTerm> failRun(const ParsingEnv& env) const;
    
        /// @return the weight of a transition headed by an instance of this key
    ///         and augmenting tr.
    ///         a returned weight of zero (absorbing in the semiring)
    ///         indicates incompatibility between this key and the transition.
    /// @warning if tr is nullary (leaf), this key must be complete.
    /// @warning if tr is not nullary (inner), this key can be partial or complete.
    /// @warning The weight of a run headed by the augmented transition
    ///          is the semiring product of this value with the product
    ///          of weights of the subruns.
    /// @todo complete this stub with Distance
    /// @see Key::weight
    virtual Weight weight(label_t a,
                          const Transition& tr,
                          const ParsingEnv& env) const;

    /// @brief return WTA transitions to be generalized
    ///        to augmented transitions targeted by this key or an instance.
    /// @param s a stack to be filled with WTA transitions to be augmented.
    /// @param env parsing environment, containing a wta
    /// and pool of multi-bar transitions. can be modified.
    virtual void transitions(std::stack<const Transition*>& s,
                             ParsingEnv& env) const;

    
    /// @todo revision needed
    /// the 2 following public members are specific to this K class
    /// PROBLEM FOR USING IN A TEMPLATE<K> like TableParse

    
    /// @brief printer
    friend std::ostream& operator<<(std::ostream& o, const KeySV& k);
    
protected:
    
    /// sequence of IOI (rationals) associated to this state.
    ValueList _list;

    /// start date in parent list.
    /// convenience to avoid recomputation. ignored for key equality.
    mtu_t _start;
    
    /// is first child
    /// @todo RM (debug)
    bool _fs;
    
    // @brief constructor for internal use
    // KeySV(state_t s, const ValueList& seg);
    
    /// @param k target key for the run. can be partial.
    /// @param r an inner run to be completed with the key initialized.
    /// @param env input environment.
    virtual void init(const KeySV& k,
                      RunInner<KeySV>& r,
                      const ParsingEnv& env);
    
    /// @param k parent of this key
    /// @param t base wta transition to be augmented with the constructed key.
    /// @param env parsing environment, contain an input segment,
    /// and interval tree and heap.
    void init_firstchild(const KeySV& k,
                         const Transition& t,
                         const ParsingEnv& env);
    
    /// @param parent parent of this key (target of the run it belongs to).
    /// @param prev previous sibling of this key (in the run it belongs to).
    /// @param t base wta transition to be augmented with the constructed key.
    /// @param i index of the next sibling in run (and transition)
    /// @param env parsing environment, contain an input segment,
    /// and interval tree and heap.
    void init_nextsibling(const KeySV& parent,
                          const KeySV& prev,
                          const Transition& t,
                          size_t i,
                          const ParsingEnv& env);
    
    /// @param head key to be instanciated (i.e. this key will be an instance of k).
    /// @param last complete key, last child of this key.
    /// @param t base wta transition to be augmented, with the constructed target key.
    /// @param env parsing environment, contain an input segment,
    /// and interval tree and heap.
    void init_instanciate(const KeySV& head,
                          const KeySV& last,
                          const Transition& t,
                          const ParsingEnv& env);
    
    /// @brief accessor
    virtual size_t size() const;
    
    /// @brief number of points to read in the input segment
    /// in order to make a transition to this Key state.
    /// @todo RM : replaced by size()
    virtual size_t points() const;
           
    /// add to the given stack some terminal runs built from the given transition.
    /// @param t transition to build terminal runs. must be terminal and complete.
    /// @param st a stack to be filled with pairs containing terminal runs.
    virtual void RunsTerminal(const Transition& t,
                              StackTerm<KeySV>& st,
                              const ParsingEnv& env) const;

    /// add to the given stack some inner runs built from the given transition.
    /// @param t transition to build inner runs. must be inner and complete.
    /// @param si a stack to be filled with inner runs.
    /// @warning the runs constructed are added to the given stack.
    /// They can be partial or complete.
    /// They must be deallocate elsewhere.
    virtual void RunsInner(const Transition& t,
                           StackInner<KeySV>& si,
                           const ParsingEnv& env) const;
    
           
private:

    /// this key can be used as body of terminal transition with given label.
    /// @param label must be terminal (arity = 0) and insider the bar symbol.
    bool compatible(label_t label) const;
    
    


};


/// hash function for using as key in a table.
/// rank is ignored.
/// @see also ValueStateHasher.
struct KeySVHasher
{
    std::size_t operator()(const KeySV& k) const
    {
        using std::size_t;
        using std::hash;
        
        // see constant.h
        std::size_t h = HASH_SEED;
        h += hash<state_t>()(k.state());
        h = h * HASH_FACTOR + hash<bool>()(k._fs);
        h = h * HASH_FACTOR + hash<mtu_t>()(k._start);
        h = h * HASH_FACTOR + hash<ValueList>()(k.value());
        return h;
    }
};


} // end namespace parsing


#endif /* KeySV_hpp */

/// @}


//
//  RunRanked.tpp
//  squant
//
//  Created by Florent Jacquemard on 07/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//



namespace parsing{


//template<class K>
//RunRanked<K>::RunRanked(label_t lab, const Weight& w):
//Run<K>(lab, w),
//_ranks()
//{
//
//}

// /// if r is ranked, make a simple copy,
// /// otherwise, copy r and upgrade to RunRankedby setting all the ranks to 1.
//RunRanked<K>::RunRanked(const Run<K>& r):
//Run<K>(r),
//_ranks(r.size(), 1)
//{
//    assert(r.complete());
//    if (r.ranked())
//    {
//        RunRanked{K}& rr = dynamic_cast<RunRanked<K>&> r;
//        _ranks = std::vector<size_t>(r._ranks);
//    }
//    else
//    {
//        _ranks = std::vector<size_t>(r.size(), 1);
//    }
//}

template<class K>
RunRanked<K>::RunRanked(const RunTerm& r):
RunInner<K>(r),
_ranks(),  // empty children list
_complete(true),
_length(r.length())
{
    assert(! r.ranked());
    assert(r.complete());
    // _ranks = std::vector<size_t>(r.size(), 1);
}


template<class K>
RunRanked<K>::RunRanked(const RunInner<K>& r):
RunInner<K>(r),
_ranks(r.size(), 1),
_complete(true),
_length(0)
{
    assert(! r.ranked());
    assert(r.complete());
    // _ranks = std::vector<size_t>(r.size(), 1);
}


template<class K>
RunRanked<K>::RunRanked(const RunRanked<K>& r):
Run(r),
_ranks(r._ranks)
{
    assert(r.ranked());
    assert(r.complete());
    assert(r.size() == r._ranks.size());
}


// copy and increment
/// @todo optimisation for recomputing weight in semirings with division?
template<class K>
RunRanked<K>::RunRanked(const RunRanked<K>& r, size_t i):
RunInner<K>(r),
_ranks(r._ranks)
{
    assert(r.ranked());
    assert(r.complete());
    assert(r.size() == r._ranks.size());
    assert(i < r.size());
    this->weightUnknown(); // set weight to unknown 
    ++_ranks[i];
    this->filterReset();   // filter in initial state
}


template<class K>
void RunRanked<K>::reset()
{
    assert(this->complete());
    this->weightReset();
    assert(! this->weight().unknown());
    if (this->weight().is_zero())
        WARN("RunR.reset: init weight=zero for {}", *this);
    
    this->filterReset();
    
    for (size_t i = 0; i < this->size(); i++)
    {
        std::shared_ptr<const Run> sub0 = subrun(i);
        std::shared_ptr<const RunRanked<K>> sub =
            std::dynamic_pointer_cast<const RunRanked<K>>(sub0);
        // dynamic cast did not fail
        assert((sub0 == nullptr) || (sub != nullptr));
        
        if (sub == nullptr)
        {
            this->weightUnknown(); // set weight to unknown
            return;
        }
        else
        {
            Weight wi = sub->weight();
            assert(! wi.unknown());
            if (wi.is_zero())
                WARN("RunR.reset: zero weight subrun {} of {}",
                     *sub, *this);
            this->weightUpdate(wi);
            this->filterUpdate(sub->_filter);
        }
    }
    assert(! this->weight().unknown());
    assert(this->_filter.complete());
}


template<class K>
std::shared_ptr<const Run> RunRanked<K>::subrun(size_t i) const
{
    assert(_ranks.size() == this->size()); // optional
    assert(i < _ranks.size());
    size_t n = _ranks[i];
    assert(n >= 1);

    return RunInner<K>::subrun(i, n);
}


template<class K>
void RunRanked<K>::print(std::ostream& o) const
{
    SymbLabel::print(this->_label, o);
    if (this->size() == 0)
    {
        o << "()";
    }
    else
    {
        assert(this->size() == _ranks.size());
        o << "(";
        // typename std::vector<std::shared_ptr<Record<K>>>::const_iterator i  =
        auto i = this->_children.cbegin();
        assert(i != this->_children.cend());
        assert(*i);
        o << (*i)->key() << "." << _ranks[0];
        ++i;
        for(size_t j = 1; i != this->_children.cend(); ++i)
        {
            assert(*i);
            o  << " " << (*i)->key() << "." << _ranks[j] ;
            ++j;
        }
        o << ") ";
    }
    o << " w=" << this->_weight;
}


} // end namespace parsing

//
//  KeySIP.hpp
//  squanty
//
//  Created by Florent Jacquemard on 29/10/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{


#ifndef KeySIP_hpp
#define KeySIP_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <assert.h>

#include "util.hpp"
#include "hash.hpp"
#include "MTU.hpp"
#include "KeySI.hpp"
#include "Distance.hpp"
#include "GaussianDistance.hpp"
#include "IntervalTree.hpp"
#include "IntervalHeap.hpp"


namespace parsing{

/// type for pre post values in augmented states
typedef long pre_t;
static pre_t P_NULL = 0;
static pre_t P_UNKNOWN = -1;
static bool P_KNOWN(pre_t x) { return (x >= 0); }


/// extension of KeySI with pre and post values (int), made of
/// - one state of a base WTA
/// - one interval (inherited attribute)
/// - rank for k-best algo (not used for 1-best).
/// - pre value
/// - post value can be unknown (making the key partial)
class KeySIP : public KeySI  // Key<KeySI>
{
    
    template<typename> friend class RunFactory;
    
public:
    
    /// @brief top constructor.
    /// allocate an initial key
    /// which will serve to create the other keys.
    /// @param barlen first estimated bar duration in rtu.
    /// @param env input environment.
    /// @param f_open flag true iff the initial MTU interval is right-open.
    /// @param pre intial pre value (for chainig with other segment).
    /// must not be UNKNOWN.
    /// if false, the right bound is set to the duration of input segment.
    KeySIP(rtu_t barlen, const ParsingEnv& env, bool f_open=true,
           pre_t pre=0, pre_t post=P_UNKNOWN);
    
    /// @brief constructor of a successor of a key to complete an inner run.
    /// @param k target key for the run. can be partial.
    /// @param r an inner run to be completed with the key constructed.
    /// @param env input environment.
    /// There are 4 cases of construction:
    /// - if r is partial and empty, construct the first child k0 to add to r.
    ///   k is the parent of k0 in this case.
    /// - if r is partial but not empty, construct the next sibling kn to add
    ///   to r. k is the parent of kn in this case.
    /// - if r is inner and complete, construct a complete instance of k
    ///   that will be the (complete) target of run r.
    KeySIP(const KeySIP& k, RunInner<KeySIP>& r, const ParsingEnv& env);
    
    /// @brief copy
    KeySIP(const KeySIP& k);
    
    /// destructor
    virtual ~KeySIP();
    
    
    /// @brief Key assignement.
    KeySIP& operator= (const KeySIP& p);
    
    /// @brief for use as key in a unordered_multimap.
    bool operator==(const KeySIP& p) const;
    bool operator!=(const KeySIP& p) const;
    
    /// @brief return wether this Key is an instance of p.
    /// = equality in this case because KeyS are always complete.
    bool instance(const KeySIP& p) const;
    
    /// @brief all attributes of the key are valued
    virtual bool complete() const;

    
    /// @todo revision needed
    /// the 3 following public members are specific to this K class
    /// PROBLEM FOR USING IN A TEMPLATE<K> like TableParse
    
    /// @brief accessor
    inline pre_t pre() const { return _pre; }

    /// @brief accessor
    inline pre_t post() const { return _post; }

    /// @brief construct (augmented) runs targeting complete instances of
    /// this key.
    /// @param si a stack to be filled with inner runs. must be empty.
    /// @param st a stack to be filled with pairs containing terminal runs.
    /// must be empty.
    /// @param env parsing environment, containing a wta.
    // @return how many runs have been added to the stacks.
    /// @warning the runs constructed are added to the given stacks.
    /// - si will contain inner runs, partial or complete.
    /// - st will contains pairs made of
    ///     - a complete instance k' of k if k is partial,
    ///       or k itself otherwise.
    ///     - a complete terminal run targeting r'
    /// All the runs in stacks must be deallocate elsewhere.
    void runs(StackInner<KeySIP>& si,
              StackTerm<KeySIP>& st,
              ParsingEnv& env) const;
        
    /// @brief printer
    friend std::ostream& operator<<(std::ostream& o, const KeySIP& p);

    
protected:

    /// number of input points in the first half of the interval of this key.
    pre_t _pre;

    /// number of input points in the second half of the interval of this key.
    pre_t _post;
       
    /// @param k parent of this key
    /// @param t base wta transition to be augmented with the constructed key.
    /// @param env parsing environment, contain an input segment,
    /// and interval tree and heap.
    void init_firstchild(const KeySIP& k,
                         const Transition& t,
                         const ParsingEnv& env);
    
    /// @param parent parent of this key (target of the run it belongs to).
    /// @param prev previous sibling of this key (in the run it belongs to).
    /// @param t base wta transition to be augmented with the constructed key.
    /// @param i index of the next sibling in run (and transition)
    /// @param env parsing environment, contain an input segment,
    /// and interval tree and heap.
    void init_nextsibling(const KeySIP& parent,
                          const KeySIP& prev,
                          const Transition& t,
                          size_t i,
                          const ParsingEnv& env);
    
    /// @param head key to be instanciated (i.e. this key will be an instance of k).
    /// @param last complete key, last child of this key.
    /// @param t base wta transition to be augmented, with the constructed target key.
    /// @param env parsing environment, contain an input segment,
    /// and interval tree and heap.
    void init_instanciate(const KeySIP& head,
                          const KeySIP& last,
                          const Transition& t,
                          const ParsingEnv& env);
    
    /// @brief accessor
    virtual size_t size() const;
    
    /// add to the given stack some terminal runs built from the given transition.
    /// @param t transition to build terminal runs. must be terminal and complete.
    /// @param st a stack to be filled with pairs containing terminal runs.
    virtual void RunsTerminal(const Transition& t,
                              StackTerm<KeySIP>& st,
                              const ParsingEnv& env) const;

    /// add to the given stack some inner runs built from the given transition.
    /// @param t transition to build inner runs. must be inner and complete.
    /// @param si a stack to be filled with inner runs.
    /// @warning the runs constructed are added to the given stack.
    /// They can be partial or complete.
    /// They must be deallocate elsewhere.
    virtual void RunsInner(const Transition& t,
                           StackInner<KeySIP>& si,
                           const ParsingEnv& env) const;
    
    /// @brief weight for the case of a and tr terminal.
    virtual Weight terminalWeight(label_t a,
                                  const Transition& tr,
                                  const ParsingEnv& env) const;
  
private:
    
    /// @brief constructor for internal use.
    KeySIP(state_t s, IntervalTree* node, rtu_t barlen, pre_t pre, pre_t post);

    /// @brief copy and instanciate. for internal use.
    KeySIP(const KeySIP& k, pre_t post);
    
    /// add to the given stack some terminal runs built from the given transition.
    /// @param t transition to build terminal runs. must be terminal and complete.
    /// @param st a stack to be filled with pairs containing terminal runs.
    /// @warning this key must be complete
    virtual void RunsTerminalComplete(const Transition& t,
                                      StackTerm<KeySIP>& st,
                                      const ParsingEnv& env) const;
    
};


/// hash function for using as key in a table.
/// rank is ignoreds : same as SpointerHasher
struct KeySIPHasher
{
    std::size_t operator()(const KeySIP& k) const
    {
        using std::size_t;
        using std::hash;
        assert(k.interval());
        
        // see constant.h
        std::size_t h = HASH_SEED;
        h += hash<state_t>()(k.state());
        h = h * HASH_FACTOR + hash<rtu_t>()(k.interval()->rbegin());
        h = h * HASH_FACTOR + hash<rtu_t>()(k.interval()->rduration());
        h = h * HASH_FACTOR + hash<mtu_t>()(k.interval()->mbegin());
        h = h * HASH_FACTOR + hash<mtu_t>()(k.interval()->mduration());
        h = h * HASH_FACTOR + hash<pre_t>()(k.pre());
        h = h * HASH_FACTOR + hash<pre_t>()(k.post());
        return h;
    }
};

} // end namespace parsing


#endif /* KeySIP_hpp */

/// @}

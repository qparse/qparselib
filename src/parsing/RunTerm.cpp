//
//  RunTerm.tpp
//  qparse
//
//  Created by Florent Jacquemard on 19/03/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//


#include "RunTerm.hpp"

namespace parsing{


//template<class K>
RunTerm::RunTerm(const Transition& tr, size_t len):
Run(tr),
_complete(true),
_length(len)
{
    assert(tr.terminal());
    assert(tr.arity() == 0);
    assert(! tr.weight().unknown());
    assert(SymbLabel::arity(tr.label()) == 0);
}


//template<class K>
RunTerm::RunTerm(const Transition& tr,
                    label_t a,
                    const Weight& w,
                    size_t len):
Run::Run(tr, a, w),
_complete(true),
_length(len)
{
    assert(tr.terminal());
    assert(tr.arity() == 0);
    assert(SymbLabel::arity(a) == tr.arity());
    assert(! w.unknown());
}


// copy
//template<class K>
RunTerm::RunTerm(const RunTerm& r):
Run(r),
_complete(r._complete),
_length(r._length)
{ }


//template<class K>
RunTerm::~RunTerm()
{
    TRACE("delete terminal Run {}", *this);
}


//template<class K>
RunTerm& RunTerm::operator= (const RunTerm& rhs)
{
    if(this != &rhs)
    {
        Run::operator=(rhs);
        _complete = rhs._complete;
        _length = rhs._length;
    };
    
    return *this;
}


//template<class K>
bool RunTerm::operator==(const RunTerm& rhs) const
{
    if (! Run::operator==(rhs))
        return false;
    
    if (_complete != rhs._complete)
        return false;
    
    if (_length != rhs._length)
        return false;

    return true;
}


size_t RunTerm::addto(Recordo& rec) const
{
    //assert(rec);
    return rec.addTerm(shared_from_this());
}


//template<class K>
size_t RunTerm::length() const
{
    return _length;
    // return SymbLabel::nbEvents(_label);
}


//template<class K>
size_t RunTerm::arity() const
{
    assert(this->_origin.arity() == 0); // it is really terminal
    return 0;
}


//template<class K>
size_t RunTerm::size() const
{
    return 0;
}


//template<class K>
bool RunTerm::complete() const
{
    return _complete;
}


bool RunTerm::terminal() const
{
    assert(arity() == 0);
    return true;
}


bool RunTerm::inner() const
{
    assert(arity() == 0);
    return false;
}


void RunTerm::print(std::ostream& o) const
{
    SymbLabel::print(_label, o);
    o << " : " << _weight;
    o << " (" << _length << " input points)";
}


std::ostream& operator<<(std::ostream& o, const RunTerm& r)
{
    r.print(o);
    return o;
}


} // end namespace parsing


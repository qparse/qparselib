//
//  RunInnero.cpp
//  squant2
//
//  Created by Florent Jacquemard on 03/04/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#include "RunInnero.hpp"



namespace parsing{


RunInnero::RunInnero(const Transition& tr):
Run(tr)
{ }


RunInnero::RunInnero(const Transition& tr, label_t lab, const Weight& w):
Run(tr, lab, w)
{ }


RunInnero::RunInnero(const RunTerm& r):
Run((const Run&) r) // downcast
{ }


RunInnero::RunInnero(const RunInnero& r):
Run(r)
{ }


RunInnero::~RunInnero()
{ }


bool RunInnero::terminal() const
{
    assert(arity() > 0);
    return false;
}


bool RunInnero::inner() const
{
    assert(arity() > 0);
    return true;
}



} // end namespace parsing


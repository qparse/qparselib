//
//  Recordo.hpp
//  squant2
//
//  Created by Florent Jacquemard on 03/04/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{


#ifndef Recordo_hpp
#define Recordo_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <assert.h>

#include "trace.hpp"
#include "Runey.hpp"
//#include "RunTerm.hpp"
//#include "RunInner.hpp"


namespace parsing{

//template<class K> class Run;
//template<class K> class RunCompare;
//template<class K, class R, class H> class TableParse;

class RunTerm;
class RunInnero;
template<class K> class RunInner;

/// Visitor of Runs
/// type erasure for a Record
class Recordo
{

public:
    
    virtual ~Recordo();
    
    virtual std::shared_ptr<Recordo> clone() const=0;
    
    /// @brief add a run to the record.
    /// Visitor of Runs dispatching over two Run types.
    virtual size_t addTerm(const std::shared_ptr<const RunTerm> r) = 0;
    virtual size_t addInner(const std::shared_ptr<const RunInnero> r) = 0;

};


} // end namespace parsing


#endif /* Recordo_hpp */


/// @}

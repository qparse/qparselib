//
//  RunTerm.hpp
//  qparse
//
//  Created by Florent Jacquemard on 19/03/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{

#ifndef RunTerm_hpp
#define RunTerm_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <assert.h>

#include "trace.hpp"
#include "SymbLabel.hpp" // symbolic labels
#include "Weight.hpp"
#include "Transition.hpp"
#include "Runey.hpp"
#include "Recordey.hpp"


namespace parsing{

/// A terminal run is a particular case of run without subrun.
/// An terminal run is complete when marked as such (flag).
/// It is partial otherwise.
/// The weight of a partial terminal run can be changed.
class RunTerm : public Run, public std::enable_shared_from_this<RunTerm>
{
public:

    /// terminal run initialized with a transition of the base wta.
    /// @param tr origin transition used to build the run. must be terminal.
    /// tr gives label and initial weight. It cannot be changed afterwards.
    /// @param len number of input points (events) parsed by this run.
    /// The constructed run is complete.
    RunTerm(const Transition& tr, size_t len);
    
    /// terminal run initialized  a transition of the base wta
    /// and some label and weight values which may differ from this transition.
    /// @param tr origin transition used to build the run. must be terminal.
    /// @param lab given label. cannot be changed afterwards.
    /// @param w weight of run. may differ from the run of tr.
    /// @param len number of input points (events) parsed by this run.
    /// must not be unknown. cannot be updated afterwards.
    /// The constructed run is complete.
    RunTerm(const Transition& tr, label_t lab, const Weight& w, size_t len);
    
    /// copy construtor.
    RunTerm(const RunTerm& r);
          
    /// deleting a run does not free the records
    /// pointed in the list of children (this list contains only pointers).
    virtual ~RunTerm();

    RunTerm& operator= (const RunTerm& rhs);
    
    bool operator==(const RunTerm& rhs) const;
    
    /// number of input symbols (events) parsed by this terminal run.
    size_t length() const;
    
    /// always 0 for terminal runs
    virtual size_t arity() const;

    /// @return actual number of children for this run.
    /// it is always 0 for terminal runs.
    virtual size_t size() const;

    /// a terminal run is complete
    virtual bool complete() const;
    
    /// this run is terminal (leaf).
    virtual bool terminal() const;
    
    /// this run is inner.
    virtual bool inner() const;
    
    /// Visitor
    virtual size_t addto(Recordo& rec) const;
    //virtual size_t addto(std::shared_ptr<Recordo> rec) const;
      
    void print(std::ostream& o) const;
    
protected:
    
    /// the run is complete
    /// @todo delete if useless (cannot be changed)
    bool _complete;
    
    /// number of input points (events) parsed by this run.
    size_t _length;
    
};

std::ostream& operator<<(std::ostream& o, const RunTerm& r);

} // end namespace parsing


// separated definition of template class
//#include "RunTerm.tpp"

#endif /* RunTerm_hpp */

/// @}

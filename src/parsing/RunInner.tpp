//
//  RunInner.tpp
//  GTests
//
//  Created by Florent Jacquemard on 19/03/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//


namespace parsing{


template<class K>
RunInner<K>::RunInner(const Transition& tr):
RunInnero(tr),
_children(),      // no children yet
_subruns()       // [opt]
{
    assert(tr.inner());
    assert(tr.arity() > 0);
}


template<class K>
RunInner<K>::RunInner(const Transition& tr, label_t a, const Weight& w):
RunInnero(tr, a, w),
_children(),      // no children yet
_subruns()        // [opt]
{
    assert(tr.inner());
    assert(SymbLabel::arity(a) == tr.arity());
    assert(SymbLabel::arity(a) > 0);
    assert(! w.unknown());
}


// partial copy
template<class K>
RunInner<K>::RunInner(const RunTerm& r):
RunInnero(r),
_children(),       // empty
_subruns()         // empty
{ }


// copy
template<class K>
RunInner<K>::RunInner(const RunInner<K>& r):
RunInnero(r),
_children(r._children),       // copy
_subruns(r._subruns)          // [opt]
{
    assert(size() <= this->arity());
}


// copy/update
template<class K>
RunInner<K>::RunInner(const RunInner<K>& r, std::shared_ptr<Record<K>> rec):
RunInner<K>(r) // copy
{
    assert(rec);
    std::shared_ptr<const Run> best = rec->best(1);
    init_best(rec, best); //    init_best(r, rec, best);
}


// copy/update
template<class K>
RunInner<K>::RunInner(const RunInner<K>& r,
                      std::shared_ptr<Record<K>> rec,
                      std::shared_ptr<const Run> best):
RunInner<K>(r) // copy
{
    assert(rec);
    init_best(rec, best);
}


template<class K>
void RunInner<K>::init_best(std::shared_ptr<Record<K>> rec,
                            std::shared_ptr<const Run> best)
{
    assert(this->partial());
    assert(this->inner());
    assert(rec);
    assert(rec->key().complete());
    
    // add new child
    _children.push_back(rec);
    
    // update weight and filter with the best's weight
    // const Transition& tr = this->_origin;
    assert(best != nullptr);
    assert(best->complete());
    filterUpdate(best->filter()); // handles the case of FAIL subrun
    if (best->fail())
    {
        // ignore weight of FAIL run
        // this->_weight = Weight();
        // this->_filter.reset(tr._label,  tr.size());
    }
    else
    {
        assert(! best->weight().unknown());
        if (best->weight().is_zero())
            WARN("RunUpdate {}: zero-weight subrun {}", *(this), *best);
        this->_weight *= best->weight();
    }
    _subruns.push_back(best);  // [opt]
}


template<class K>
RunInner<K>::~RunInner()
{
    TRACE("delete inner Run {}", *this);
    _children.clear();
    _subruns.clear();
    //delete weight;
}


template<class K>
std::shared_ptr<RunInnero> RunInner<K>::clone() const
{
    return std::make_shared<RunInner<K>>(*this);
}


template<class K>
RunInner<K>& RunInner<K>::operator= (const RunInner<K>& rhs)
{
    if(this != &rhs)
    {
        Run::operator=(rhs);
        _children.resize(rhs._children.size());
        _children = rhs._children; // copy
        _subruns.resize(rhs._subruns.size());
        _subruns = rhs._subruns;
    };
    
    return *this;
}


template<class K>
bool RunInner<K>::operator==(const RunInner<K>& rhs) const
{
    if (! Run::operator==(rhs))
        return false;
    
    if (_children.size() != rhs._children.size())
        return false;
    
    typename std::vector<Record<K>*>::const_iterator li =
    _children.cbegin();
    typename std::vector<Record<K>*>::const_iterator ri =
    rhs._children.cbegin();
    
    while (li != _children.cend())
    {
        /// @warning comparison of pointers (to Records)
        if (*li != *ri)
            return false;
        ++li;
        // lists have same size
        assert(ri != rhs._children.cend());
        ++ri;
    }
    
    // if the runs are identical, their weights must be the same
    assert(this->_weight == rhs._weight);

    return true;
}


template<class K>
size_t RunInner<K>::addto(Recordo& rec) const
{
    //assert(rec);
    // this-> to find object of dependent type
    // see http://gcc.gnu.org/wiki/VerboseDiagnostics#dependent_base
    return rec.addInner(this->shared_from_this());
}


// actual number of children
template<class K>
size_t RunInner<K>::size() const
{
    assert(_children.size() <= this->arity());

    return (_children.size());
}


template<class K>
bool RunInner<K>::empty() const
{
    return (_children.empty());
}


template<class K>
size_t RunInner<K>::multiplicity(size_t i) const
{
    assert(i < this->_origin.size());
    return this->_origin.multiplicity(i);
}


template<class K>
bool RunInner<K>::complete() const
{
    assert(size() <= this->arity());

    // expected number of children
    return (size() == this->_origin.size());   // return (arity() == size());
}


template<class K>
std::shared_ptr<const Run> RunInner<K>::subrun(size_t i) const
{
    return subrun(i, 1);
}


template<class K>
std::shared_ptr<const Run> RunInner<K>::subrun(size_t i, size_t n) const
{
    assert(this->complete());
    assert(i < this->size());
    assert(0 < n);
    std::shared_ptr<Record<K>> rec = _children[i];
    assert(rec);
    return rec->best(n);
}


template<class K>
const K& RunInner<K>::key(size_t i) const
{
    assert (i < _children.size());
    std::shared_ptr<Record<K>> rec = _children[i];
    assert(rec != nullptr);
    const K& k = rec->key();
    assert(k.complete());
    return k;
}


//template<class K>
//Record<K>* RunInner<K>::operator[](size_t i) const
//{
//    assert (i < _children.size());
//    Record<K>* rec = _children[i];
//    assert(rec);
//    return rec;
//}


template<class K>
void RunInner<K>::print(std::ostream& o) const
{
    SymbLabel::print(this->label(), o);

    if (_children.empty())
    {
        o << "()";
    }
    else
    {
        o << "(";
        // typename std::vector<std::shared_ptr<Record<K>>>::const_iterator
        auto i  = _children.cbegin();
        assert(i != _children.cend());
        o << (*i)->key();
        ++i;
        for(; i != _children.cend(); ++i)
        {
            o << " " << (*i)->key();
        }
        o << ")";
    }
    o << " : " << this->weight();
    if (complete())
        o << " ( complete & " << (_filter.filter()?"filtered":"unfiltered") << " )";
}


} // end namespace parsing




//
//  KeySI.hpp
//  squanty
//
//  Created by Florent Jacquemard on 04/06/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{

#ifndef KeySI_hpp
#define KeySI_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <assert.h>

#include "util.hpp"
#include "hash.hpp"
#include "MTU.hpp"
#include "KeyS.hpp"
#include "Distance.hpp"
#include "GaussianDistance.hpp"
#include "IntervalTree.hpp"
#include "IntervalHeap.hpp"

namespace parsing{


/// Key (augmented SWTA state) made of
/// - one state of a base WTA
/// - one interval (inherited attribute)
/// - rank for k-best algo (not used for 1-best).
class KeySI : public KeyS
{
    
    template<typename> friend class RunFactory;
    
public:
        
    /// @brief top constructor.
    /// allocate an initial key
    /// which will serve to create the other keys.
    /// @param barlen first estimated bar duration in rtu.
    /// @param env input environment.
    /// @param f_open flag true iff the initial MTU interval is right-open
    /// if false, the right bound is set to the duration of input segment.
    KeySI(rtu_t barlen, const ParsingEnv& env, bool f_open=true);
    
    /// @brief constructor of a successor of a key to complete an inner run.
    /// @param k target key for the run. can be partial.
    /// @param r an inner run to be completed with the key constructed.
    /// @param env input environment.
    /// There are 4 cases of construction:
    /// - if r is partial and empty, construct the first child k0 to add to r.
    ///   k is the parent of k0 in this case.
    /// - if r is partial but not empty, construct the next sibling kn to add
    ///   to r. k is the parent of kn in this case.
    /// - if r is inner and complete, construct a complete instance of k
    ///   that will be the (complete) target of run r.
    KeySI(const KeySI& k, RunInner<KeySI>& r, const ParsingEnv& env);
        
    // @brief constructor of an instance of a key targeting a terminal run.
    // @param k target key for the run in 2 cases below. must be partial.
    // @param r a complete terminal run targeting the key constructed.
    // @param env input environment.
    // Construct a complete instance of k that will be the target of run r.
    // KeySI(const KeySI& k, const RunTerm& r, const ParsingEnv& env);
    
    /// @brief copy
    KeySI(const KeySI& k);
    
    /// destructor
    virtual ~KeySI();

    /// @brief Key assignement.
    KeySI& operator= (const KeySI& k);
    
    /// @brief for use as key in a unordered_multimap.
    bool operator==(const KeySI& k) const;
    bool operator!=(const KeySI& k) const;
    
    /// @brief return wether this Key is an instance of p.
    /// = equality in this case because KeyS are always complete.
    bool instance(const KeySI& k) const;
   
    /// @brief all attributes of the key are valued
    virtual bool complete() const;

    /// @todo revision needed
    /// the 2 following public members are specific to the K class with interval
    /// PROBLEM FOR USING IN A TEMPLATE<K> like TableParse
    
    /// @brief accessor
    inline IntervalTree* interval() const { return _interval; }

    /// @brief construct (augmented) runs targeting complete instances of
    /// this key.
    /// @param si a stack to be filled with inner runs. must be empty.
    /// @param st a stack to be filled with pairs containing terminal runs.
    /// must be empty.
    /// @param env parsing environment, containing a wta.
    // @return how many runs have been added to the stacks.
    /// @warning the runs constructed are added to the given stacks.
    /// - si will contain inner runs, partial or complete.
    /// - st will contains pairs made of
    ///     - a complete instance k' of k if k is partial,
    ///       or k itself otherwise.
    ///     - a complete terminal run targeting r'
    /// All the runs in stacks must be deallocate elsewhere.
    void runs(StackInner<KeySI>& si,
              StackTerm<KeySI>& st,
              ParsingEnv& env) const;

    
    /// allocate and return a special Run representing a parse failure for this key.
    /// @param env parsing environment.
    /// @return a pointer to a newly allocated fail run, with
    /// - transition with dummy symbol, empty body, weight one in the
    ///   domain of the base wta in the passed environment.
    /// - fail symbol
    /// - weight one (in the domain of the base wta).
    /// This returned run will answer succesfully to Run.fail().
    std::shared_ptr<RunTerm> failRun(const ParsingEnv& env) const;
      
    /// @brief printer
    friend std::ostream& operator<<(std::ostream& o, const KeySI& p);
        
protected:
        
    rtu_t _barlen; // current bar length. useful only for meta keys.

    IntervalTree* _interval;  // should not be modified
       
    /// @brief constructor for internal use
    KeySI(state_t s, IntervalTree* node, rtu_t barlen);
       
    /// @param k parent of this key
    /// @param t base wta transition to be augmented with the constructed key.
    /// @param env parsing environment, contain an input segment,
    /// and interval tree and heap.
    void init_firstchild(const KeySI& k,
                         const Transition& t,
                         const ParsingEnv& env);
    
    /// @param parent parent of this key (target of the run it belongs to).
    /// @param prev previous sibling of this key (in the run it belongs to).
    /// @param t base wta transition to be augmented with the constructed key.
    /// @param i index of the next sibling in run (and transition)
    /// @param env parsing environment, contain an input segment,
    /// and interval tree and heap.
    void init_nextsibling(const KeySI& parent,
                          const KeySI& prev,
                          const Transition& t,
                          size_t i,
                          const ParsingEnv& env);
    
    /// @param head key to be instanciated (i.e. this key will be an instance of k).
    /// @param last complete key, last child of this key.
    /// @param t base wta transition to be augmented, with the constructed target key.
    /// @param env parsing environment, contain an input segment,
    /// and interval tree and heap.
    void init_instanciate(const KeySI& head,
                          const KeySI& last,
                          const Transition& t,
                          const ParsingEnv& env);
    
    /// @brief accessor
    virtual size_t size() const;

    // @brief number of points to read in the input segment
    // in order to make a transition to this Key state.
    // @todo RM : replaced by size()
    // virtual inline size_t points() const { return _interval->lsize(); }

    bool equal_interval(const KeySI&) const;
        
    /// add to the given stack some terminal runs built from the given transition.
    /// @param t transition to build terminal runs. must be terminal and complete.
    /// @param st a stack to be filled with pairs containing terminal runs.
    void RunsTerminal(const Transition& t,
                      StackTerm<KeySI>& si,
                      // std::stack<std::pair<const KeySI*, RunTerm*>>& st,
                      const ParsingEnv& env) const;

    /// add to the given stack some inner runs built from the given transition.
    /// @param t transition to build inner runs. must be inner and complete.
    /// @param si a stack to be filled with inner runs.
    /// @warning the runs constructed are added to the given stack.
    /// They can be partial or complete.
    /// They must be deallocate elsewhere.
    void RunsInner(const Transition& t,
                   StackInner<KeySI>& si,
                   // std::stack<RunInner<KeySI>*>& si,
                   const ParsingEnv& env) const;

    /// @brief weight for the case of a and tr terminal.
    virtual Weight terminalWeight(label_t a,
                                  const Transition& tr,
                                  const ParsingEnv& env) const;
    
    // special case of runs.
    // @param si a stack to be filled with inner runs.
    // @param st a stack to be filled with pairs containing terminal runs.
    // @param env parsing environment, containing a wta.
    // virtual void runsWTA(std::stack<RunInner<KeySI>*>& si,
    //                      std::stack<std::pair<KeySI*, RunTerm*>>& st,
    //                      ParsingEnv& env) const;

    // construct and add to the stacks (augmented) runs targeting
    // complete instances of this key.
    // void runsBar(std::stack<RunInner<KeySI>*>& si,
    //              std::stack<std::pair<KeySI, RunTerm*>>& st,
    //              ParsingEnv& env) const;
    
private:
    
//    virtual void runsWTA(std::stack<RunInner<KeySI>*>& si,
//                         std::stack<std::pair<KeySI, RunTerm*>>& st,
//                         ParsingEnv& env) const;

//    virtual void runsBar(std::stack<RunInner<KeySI>*>& si,
//                         std::stack<std::pair<KeySI, RunTerm*>>& st,
//                         ParsingEnv& env) const;

};


/// hash function for using as key in a table.
/// rank is ignoreds : same as SpointerHasher
struct KeySIHasher
{
    std::size_t operator()(const KeySI& k) const
    {
        using std::size_t;
        using std::hash;
        assert(k.interval());
        
        // see constant.h
        std::size_t h = HASH_SEED;
        h += hash<state_t>()(k.state());
        h = h * HASH_FACTOR + hash<rtu_t>()(k.interval()->rbegin());
        h = h * HASH_FACTOR + hash<rtu_t>()(k.interval()->rduration());
        h = h * HASH_FACTOR + hash<mtu_t>()(k.interval()->mbegin());
        h = h * HASH_FACTOR + hash<mtu_t>()(k.interval()->mduration());
        return h;
    }
};

} // end namespace parsing


#endif /* KeySI_hpp */


/// @}



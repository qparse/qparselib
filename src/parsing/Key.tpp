//
//  Key.tpp
//  squanty
//
//  Created by Florent Jacquemard on 04/06/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

namespace parsing{


template<class K>
Key<K>::Key()
{ }


template<class K>
Key<K>::~Key()
{
    // clear queue
    // std::queue<int> empty;
    // std::swap(leafq, empty );
}


template<class K>
K* Key<K>::firstChild(const Transition& tr,
                      const ParsingEnv& env) const
{
    return new K(this, tr, env);
}


template<class K>
K* Key<K>::nextSibling(size_t i,
                       const K& parent,
                       const Transition& tr,
                       const ParsingEnv& env) const
{
    assert(this->complete());
    return new K(this, i, parent, tr, env);
}

//KeyS* KeyS::nextSibling(size_t i,
//                        const KeyS* parent,
//                        const Transition& tr,
//                        const ParsingEnv& env)
//{
//    assert(tr.inner());
//    assert(0 < i);
//    assert(i < tr.size());
//    assert(this->state() == tr.at(i-1));
//    return (new KeyS(tr.at(i)));
//}


// [ALT] define instanciate as pure virtual,
// to be omplemented in every derived
template<class K>
K* Key<K>::instanciate(label_t a,
                       const K& lastchild,
                       const ParsingEnv& env) const
{
    assert(this->partial());
    assert(lastchild.complete());
    return new K(this, a, lastchild, env);
}


template<class K>
void Key<K>::leaves(const Transition& tr,
                    const ParsingEnv& env,
                    std::queue<K>& q) const
{
    assert(this->partial()); 
}


template<class K>
Run* Key<K>::failRun(const ParsingEnv& env) const
{
    assert(env.wta);
    const Transition& t1 = env.dummyTransition_one();
    assert(t1.weight().is_one());
    label_t a = SymbLabel::make_fail(this->size());
    return new RunTerm(t1, a, t1.weight());
}


// template<class K>
// K* Key<K>::leaf(label_t a,
//                  const Transition& tr,
//                  const ParsingEnv& env)
// {
//     if (all_leaves)
//         return NULL;
//     else
//         return new K(this, a, tr, env);
// }


//template<class K>
//K* Key<K>::leaf(label_t a,
//                 const Transition& tr,
//                 const ParsingEnv& env)
//{
//    if (f_leafq == false)
//    {
//        assert(leafq.empty());
//        init_leafq(a, tr, env);
//    }
//    assert(f_leafq == true);
//
//    if (leafq.empty())
//    {
//        f_leafq = false;
//        return NULL;
//    }
//    else
//    {
//        K k = leafq.pop();
//        // remove from queue and allocate a copy
//        return (new K(k));
//    }
//}



} // end namespace parsing

//
//  ParsingEnv.cpp
//  squant
//
//  Created by Florent Jacquemard on 06/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include "ParsingEnv.hpp"

namespace parsing{


ParsingEnv::ParsingEnv(SWTA* a, InputSegment* s, bool crescendo):
InputEnv(s),
_dt0((a == nullptr)?Transition():
                    Transition(SymbLabel::make_dummy(),
                               a->weight_zero())),
_dt1((a == nullptr)?Transition():
                    Transition(SymbLabel::make_dummy(),
                               a->weight_one())),
wta(a),
pool((a == nullptr)?nullptr:(new SWTA(wta->weight_one()))), // empty
// condition = exclusive or. length (nb of points for run) = 0
runenum((positive() != crescendo)?
        std::make_shared<const RunTerm>(_dt0, SymbLabel::make_fail(0),
                                        _dt0.weight(), 0):
        std::make_shared<const RunTerm>(_dt1, SymbLabel::make_fail(0),
                                        _dt1.weight(), 0))
{
    TRACE("Creating new Parsing Env");
    assert(failrun() != nullptr);
    assert((wta == nullptr) || (! failrun()->weight().unknown()));
}


ParsingEnv::ParsingEnv(const InputEnv& env, SWTA* a, bool crescendo):
InputEnv(env), // copy
_dt0((a == nullptr)?Transition():
                    Transition(SymbLabel::make_dummy(), a->weight_zero())),
_dt1((a == nullptr)?Transition():
                    Transition(SymbLabel::make_dummy(), a->weight_one())),
wta(a),
pool((a == nullptr)?nullptr:(new SWTA(wta->weight_one()))),
runenum((positive() != crescendo)?
        std::make_shared<const RunTerm>(_dt0, SymbLabel::make_fail(0),
                                        _dt0.weight(), 0):
        std::make_shared<const RunTerm>(_dt1, SymbLabel::make_fail(0),
                                        _dt1.weight(), 0))
{
    TRACE("Creating Parsing Env from Input Env (for enum {})",
          crescendo?"cresc.":"dim.");
    assert(failrun() != nullptr);
    assert((wta == nullptr) || (! failrun()->weight().unknown()));
}


ParsingEnv::ParsingEnv(const ParsingEnv& env):
InputEnv(env),
_dt0(env._dt0),
_dt1(env._dt1),
wta(env.wta), // same pointer CAUTION
pool(env.pool),
runenum(env.runenum)
{ }


ParsingEnv::~ParsingEnv()
{
    if (pool)
    {
        TRACE("delete Parsing Env pool");
        delete pool;
//        pool = NULL;
    }
}


inline Weight ParsingEnv::weight_zero() const
{
    assert(wta);
    return wta->weight_zero();
}


inline Weight ParsingEnv::weight_one() const
{
    assert(wta);
    return wta->weight_one();
}


const RunOrder::Compare& ParsingEnv::compare() const
{
    return runenum.compare;
}


std::shared_ptr<const Run> ParsingEnv::failrun() const
{
    assert(wta);
    return runenum.failrun();
}


const Transition& ParsingEnv::dummyTransition_zero() const
{
    assert(wta);
    assert(! _dt0.weight().unknown());
    assert(_dt0.weight().is_zero());
    return _dt0;
}


const Transition& ParsingEnv::dummyTransition_one() const
{
    assert(wta);
    assert(! _dt1.weight().unknown());
    assert(_dt1.weight().is_one());
    return _dt1;
}


bool ParsingEnv::positive() const
{
    return (_dt0.weight() < _dt1.weight());
}


//void ParsingEnv::init_pool()
//{
//    if (wta != NULL)
//    {
//        if (pool == NULL)
//            pool = new SWTA(wta->weight_one());
//    }
//    else
//    {
//        pool = NULL;
//    }
//}


} // end namespace parsing

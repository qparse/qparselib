//
//  TableRecords.tpp
//  squant2
//
//  Created by Florent Jacquemard on 19/06/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//


namespace parsing{



template <class K, class R, class H>
TableRecords<K,R,H>::TableRecords(ParsingEnv& env):
_env(env),
_comparer(env.comparer()),
_table(), // empty
_nb_rows(0),
_nb_runs(0)
{ }

/// delete all the complete keys and associated records
/// stored in the table.
/// @warning they must not be freed elsewhere.
template <class K, class R, class H>
TableRecords<K,R,H>::~TableRecords()
{
    TRACE("delete main table of records");
    TRACE("Instance Table: nb of entries (partial keys): {} entries, {} runs",
          _nb_rows, _nb_runs);
    _table.clear();
}

template <class K, class R, class H>
bool TableRecords<K,R,H>::empty() const
{
    return _table.empty();
}

template <class K, class R, class H>
std::shared_ptr<const Run> TableRecords<K,R,H>::best(const K& k, size_t n)
{
    assert(k.complete());
    std::shared_ptr<R> rec = find(k);
    assert(rec);
    std::shared_ptr<const Run> r = rec->best(n);
    assert(r);
    if (r->fail())
        WARN("TableRecords.best[{}]: fail run", k);
    return r;
}

// addComplete(k)
// k complete.
template <class K, class R, class H>
std::shared_ptr<R> TableRecords<K,R,H>::find(const K& k)
{
    assert(k.complete());
    bool newrec = false;
    MapIterator<K,R,H> it = addemplace(k, newrec);
    std::shared_ptr<R> rec = it->second;
    assert(it->first == k);
    assert(rec->key() == k);

    if (newrec)  // record rec is new and empty and has no key
    {
        TRACE("TableRecords[{}].addComplete: new record in table for key {}", k);
        /// @todo move next 2 lines to TableParse
        // size_t ret = addComplete(k, rec);
        //TRACE("TableParse[{}].addComplete: {} runs added", k, ret);
    }
    else          // k and record already done, skip
    {
        TRACE("TableRecords[{}].addComplete: complete key already in table,\
               skip", k);
    }
    return rec;
}


template <class K, class R, class H>
size_t TableRecords<K,R,H>::add(std::shared_ptr<R> rec,
                                std::shared_ptr<Run> r)
{
    assert(rec);
    assert(rec->key().complete());
    assert(r);
    assert(r->terminal() || r->inner());
    assert(r->terminal() != r->inner());
    assert(r->complete());
    assert(! r->weight().unknown());
    
    // dispatch to run type and add
    // will check that weight is known
    size_t ret = r->addto(*rec); // rec->add(r);

    if (ret == 0)
    {
        TRACE("TableRec[{}]: added run {}", rec->key(), *r);
        _nb_runs++;  // stats
        return 1;
    }
    else
    {
        TRACE("TableRec[{}]: run {} not added (error code {})", rec->key(), *r, ret);
        return 0;
    }
}

template<class K, class R, class H>
void TableRecords<K,R,H>::initRecord(const K& k, std::shared_ptr<R> rec)
{
    assert(k.complete());
    assert(rec);
    assert(rec->key().complete());
    // rec->setKey(k);
    TRACE("TableRecords: new record in table for key {}", k);
    _nb_rows++;
}


template<class K, class R, class H>
MapIterator<K,R,H>
TableRecords<K,R,H>::addemplace(const K& k, bool& newrec)
{
    assert(k.complete());   // assert(this->_env);
    std::shared_ptr<const K> k_ptr = std::make_shared<const K>(k);
    //std::shared_ptr<const K> k_ptr(&k);
    std::shared_ptr<const RunOrder> comp_ptr =
        std::make_shared<const RunOrder>(_env.comparer());
    //std::shared_ptr<const RunOrder> comp_ptr(&(_env.comparer()));
    std::pair<MapIterator<K,R,H>, bool> pret =
    _table.emplace(std::piecewise_construct,
                   // Leaf constructor (Key copy)
                   std::forward_as_tuple(k),
                   // empty Record
                   std::forward_as_tuple(std::make_shared<R>(k_ptr, comp_ptr)));
    assert(k == pret.first->first);
    newrec = pret.second;
    // record rec is new and empty and has no key
    // init with components of the entry in table
    if (newrec)
    {
        TRACE("TableRecords: new record in table for key {}", k);
        _nb_rows++;
    }
    
    return pret.first;
}


template<class K, class R, class H>
MapIterator<K,R,H>
TableRecords<K,R,H>::addemplace(const K& k,
                                std::shared_ptr<RunInner<K>> r,
                                bool& newrec)
{
    assert(k.partial()); // assert(this->_env);
    assert(r);
    assert(r->inner());
    assert(r->complete());

    // complete instance of the key
    K kc(k, *r, _env);
    std::shared_ptr<const K> kc_ptr = std::make_shared<const K>(kc);
    std::shared_ptr<const RunOrder> comp_ptr =
        std::make_shared<const RunOrder>(_env.comparer());
    std::pair<MapIterator<K,R,H>, bool> pret =
    _table.emplace(std::piecewise_construct,
                   // instanciate cstr
                   std::forward_as_tuple(kc), // copy
                   // empty Record
                   std::forward_as_tuple(std::make_shared<R>(kc_ptr, comp_ptr)));

    newrec = pret.second;
    // record rec is new and empty and has no key
    // init with components of the entry in table
    if (newrec)
    {
        const K& key = pret.first->first;
        assert(key.complete());
        initRecord(pret.first->first, pret.first->second);
    }
    else
        TRACE("Table.addemplace: key {} already in table", k);

    return pret.first;
}


// inefficient but emplace not working
//template<class K, class R, class H>
//std::shared_ptr<R> TableRecords<K,R,H>::at(const K& k, bool& newrec)
//{
//    assert(k.complete());
//    MapIterator<K, R, H> it = _table.find(k);
//    // found
//    if (it != _table.end())
//    {
//        std::shared_ptr<R> rec = std::make_shared<R>(it->second);
//        //assert(rec->key());
//        assert(rec->key() == k);
//        newrec = false;
//        return rec;
//    }
//    // insert
//    else
//    {
//        std::pair<MapIterator<K,R,H>, bool> pret =
//        _table.emplace(k, this->_comparer);
//        assert(pret.second);
//        const K& kc = pret.first->first;
//        assert(kc == k);
//        std::shared_ptr<R> rec = std::make_shared<R>(pret.first->second);
//        initRecord(kc, rec);
//        newrec = true;
//        return rec;
//    }
//}



} // end namespace parsing

//
//  RunOrder.cpp
//  squant2
//
//  Created by Florent Jacquemard on 24/06/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#include "RunOrder.hpp"


namespace parsing{


//RunOrder::RunOrder(const ParsingEnv& env, bool flag):
//_min(flag),
//_zero(env.dummyTransition_zero().weight()),
//_one(env.dummyTransition_one().weight()),
//_failrun(flag?RunTerm(env.dummyTransition_one(),
//                      SymbLabel::make_fail(0),
//                      _one, 0):  // length (nb pts) = 0
//              RunTerm(env.dummyTransition_zero(),
//                      SymbLabel::make_fail(0),
//                      _zero, 0)),
//compare(flag?(RunOrder::min):(RunOrder::max))
//{
//    TRACE("create RunOrder");
//    assert(env.wta);
//}


RunOrder::RunOrder(std::shared_ptr<const Run> fail):
_zero(fail->weight().zero()),  // copy
_one(fail->weight().one()),
_crescendo(positive()?fail->weight().is_zero():
                      fail->weight().is_one()),
_failrun(fail),
compare(_crescendo?(RunOrder::min):(RunOrder::max))
{
    TRACE("create RunOrder");
    assert(! _failrun->weight().unknown());
    assert(_failrun->weight().is_zero() ||
           _failrun->weight().is_one());
}


RunOrder::~RunOrder()
{
    TRACE("delete RunOrder");
}


const Weight& RunOrder::best() const
{
    if (positive())
        return _zero;
    else
        return _one;
}


const Weight& RunOrder::worst() const
{
    if (positive())
        return _one;
    else
        return _zero;
}


bool RunOrder::positive() const
{
    return (_zero < _one);
}


RunOrder::Compare RunOrder::min =
[](std::shared_ptr<const Run> lhs, std::shared_ptr<const Run> rhs)
{
    assert (lhs);
    assert (rhs);
    assert (lhs->complete());
    assert (rhs->complete());
    return (lhs->weight() < rhs->weight());
//    if (rhs->partial() || rhs->weight().unknown())
//        return false;
//    else if (lhs->partial() || lhs->weight().unknown())
//        return true;
//    else
//        return (lhs->weight() > rhs->weight());
};


RunOrder::Compare RunOrder::max =
[](std::shared_ptr<const Run> lhs, std::shared_ptr<const Run> rhs)
{
    assert (lhs);
    assert (rhs);
    assert (lhs->complete());
    assert (rhs->complete());
    return (lhs->weight() > rhs->weight());
//    if (lhs->partial() || lhs->weight().unknown())
//        return false;
//    // if rhs == 0 then lhs == rhs
//    // if rhs > 0 then lhs > rhs;
//    else if (rhs->partial() || rhs->weight().unknown())
//        return true;
//    else
//        return (lhs->weight() < rhs->weight());
};



} // end namespace parsing

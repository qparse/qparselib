//
//  TableRecords.hpp
//  squant2
//
//  Created by Florent Jacquemard on 19/06/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{


#ifndef TableRecords_hpp
#define TableRecords_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <assert.h>
#include <unordered_map> // and unordered_multimap
#include <queue>

#include "trace.hpp"
#include "Runey.hpp"
//#include "ParsingEnv.hpp"


namespace parsing{

template<class K, class R, class H> using MapRecord =
    std::unordered_map<K, std::shared_ptr<R>, H>;

template<class K, class R, class H> using MapIterator =
    typename MapRecord<K, R, H>::iterator;

//template<class K, class R, class H> using Entry =
//    std::pair<const K*, R*>;


/// main table for parsing.
/// it is a mapping of keys into records
///
/// Parameters:
/// - K = concrete key class (index rows in the table)
/// - R = record class (values in table)
/// - H = KeyHasher (for keys)
template<class K, class R, class H>
class TableRecords
{
public:
    
    /// concrete table.
    /// @param env the parsing environment.
    /// It includes a binary function for comparing runs.
    TableRecords(ParsingEnv& env);
    
    /// delete all the complete keys and associated records
    /// stored in the table.
    /// @warning they must not be freed elsewhere.
    virtual ~TableRecords();

    bool empty() const;
    
    /// @brief n-th best run in the record associated to the key k.
    /// @param k key, must be complete, must be present in the table.
    /// @param n (as in n-best) is the rank of the run with target the key
    ///        of the record. must be larger or equal to 1 (default).
    virtual std::shared_ptr<const Run> best(const K& k, size_t n=1);

    /// get record associated to the given key,
    /// it is created if there was none.
    /// @param k a complete key.
    /// @return the record associated to the given key.
    std::shared_ptr<R> find(const K& k);

    /// add run r to the record rec.
    /// @param rec an entry in the map table.
    /// @param r complete run
    /// @return the number of runs added to the table
    size_t add(std::shared_ptr<R> rec,
               std::shared_ptr<Run> r);
   
    /// @brief find or create an entry in the table.
    /// @param k key of the entry. must be complete.
    /// @param newrec flag set to true iff the returned record
    ///        has been created and added to table in this call.
    /// @return a iterator to the entry in table
    ///         associated with key k.
    MapIterator<K,R,H> addemplace(const K& k, bool& newrec);
        
    /// @brief find or create an entry in the table.
    /// @param head arg. of constructor of complete instance. must be partial.
    /// @param r an inner run used to complete k. must ne non-null and complete.
    /// @return an iterator to the entry in table associated with
    /// the instance of head build by the constructor of complete instance
    /// called with the given arguments (+ env).
    /// @todo TBR deprecated
    MapIterator<K,R,H> addemplace(const K& head,
                                  std::shared_ptr<RunInner<K>> r,
                                  bool& newrec);
    
    /// number of records in the table
    inline size_t nb_entries() const { return _nb_rows; }

    /// number of records in the table
    inline size_t size() const { return _table.size(); }

    /// total number of runs to the records of the table
    inline size_t nb_runs() const { return _nb_runs; }
   
private: // data

    /// the parsing environment.
    ParsingEnv& _env;
    
    /// Function for the comparison of Runs in records
    const RunOrder& _comparer;
    
    /// main table
    MapRecord<K,R,H> _table;
    
    /// number of records in the table
    size_t _nb_rows;
    
    /// total number of runs to the records of the table
    size_t _nb_runs;
    
    
private: // table access and construction

    // @param k key must be complete.
    // @todo SUPPR (not used)
    // std::shared_ptr<R> at(const K& k);
    // std::shared_ptr<R> at(const K& k, bool& newrec);
    
//    /// @brief record in the table associated to the key k.
//    /// @param k key must be complete.
//    /// @return a pair with the following components:
//    /// - the first component is the record with key k in the table
//    ///   (pointers to the real addresses of record and key() in table).
//    ///   It is created if not found (new empty record).
//    /// - the second component is true iff the entry was created in this call
//    ///   (hence empty).
//    std::pair<R*, bool> getRecord(const K& k);
    
    /// @brief initialize the given record with the given complete k
    /// @param k key, must be complete, must be associated to rec in table.
    /// @param rec record that will be associated to k. its key must be NULL.
    void initRecord(const K& k, std::shared_ptr<R> rec);
        
};

} // end namespace parsing


// separated definitions of template class
#include "TableRecords.tpp"

#endif /* TableRecords_hpp */

/// @}

//
//  Ranked.tpp
//  squanty
//
//  Created by Florent Jacquemard on 04/06/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include "Ranked.hpp"


namespace Parsing{


Ranked::Ranked(size_t k):
_rank(k)
{ }


Ranked::Ranked(const Ranked& p, size_t i):
_rank(p._rank + i)
{ }


Ranked& Ranked::operator= (const Ranked& p)
{
    if(this != &p)
    {
        _rank = p._rank;
    };
    return *this;
}


bool Ranked::operator==(const Ranked& p) const
{
    return (p._rank == _rank);
}

} // end namespace Parsing

//
//  RunCompare.cpp
//  squant2
//
//  Created by Florent Jacquemard on 30/03/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#include "RunCompare.hpp"


namespace parsing{


RunCompare weightMax =
[](std::shared_ptr<const Run> lhs, std::shared_ptr<const Run> rhs)
{
    assert (lhs);
    assert (rhs);
    assert (lhs->complete());
    assert (rhs->complete());
    return (lhs->weight() < rhs->weight());
//    if (rhs->partial() || rhs->weight().unknown())
//        return false;
//    else if (lhs->partial() || lhs->weight().unknown())
//        return true;
//    else
//        return (lhs->weight() > rhs->weight());
};


RunCompare weightMin =
[](std::shared_ptr<const Run> lhs, std::shared_ptr<const Run> rhs)
{
    assert (lhs);
    assert (rhs);
    assert (lhs->complete());
    assert (rhs->complete());
    return (lhs->weight() > rhs->weight());
//    if (lhs->partial() || lhs->weight().unknown())
//        return false;
//    // if rhs == 0 then lhs == rhs
//    // if rhs > 0 then lhs > rhs;
//    else if (rhs->partial() || rhs->weight().unknown())
//        return true;
//    else
//        return (lhs->weight() < rhs->weight());
};


} // end namespace parsing

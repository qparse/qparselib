//
//  Ranked.hpp
//  squanty
//
//  Created by Florent Jacquemard on 04/06/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#ifndef Ranked_hpp
#define Ranked_hpp

#include <stdio.h>
#include <assert.h>
#include <queue>


#include "trace.hpp"
#include "Weight.hpp"
#include "Transition.hpp"
#include "ParsingEnv.hpp"


namespace parsing{


// K is a class of Key (unranked, for 1-best algo)
// KK is its associated ranked version (for k-best algo)
//template<class K, class KK>
class Ranked
{
public:
    
    /// @brief copy and increase rank
    Ranked(const Ranked& p, size_t i = 0);
    
    /// @return this rank
    inline size_t rank() const { return _rank; }
    
    // increment rank
    //inline void incr() { _rank++; }
    
    virtual Ranked& operator= (const Ranked& p);
    
    virtual bool operator==(const Ranked& p) const;
    
    inline bool operator!=(const Ranked& p) const
    { return (! (*this == p)); }
    
    
protected:
    
    size_t _rank;
    
    /// internal use
    Ranked(size_t k = 1);
};


} // end namespace parsing


#endif /* Ranked_hpp */

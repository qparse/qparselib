//
//  RunOrder.hpp
//  squant2
//
//  Created by Florent Jacquemard on 24/06/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{


#ifndef RunOrder_hpp
#define RunOrder_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <assert.h>

#include "trace.hpp"
#include "Weight.hpp"
#include "Runey.hpp"
//#include "ParsingEnv.hpp"


namespace parsing
{

/// some elements to compare runs:
/// - an relation defining an ordering for enumeration,
/// - one run that can be returned in case of fail.
///
/// The fail run is the last run in enumeration. it defines the enumeration order.
/// We assume that the weight domain is a bounded semiring with its natural ordering <=.
/// - if the weight domain is a positive bounded semiring (zero <= x <= one) e.g. max-plus
///   - if the fail run has weight zero, the enumeration goes best to worst (increasing)
///   - if the fail run has weight one, the enumeration goes worst to best (decreasing)
/// - if the weight domain is a positive bounded semiring (one <= x <= zero) e.g. tropical = min-plus
///   - if the fail run has weight one, the enumeration goes best to worst (increasing)
///   - if the fail run has weight zero, the enumeration goes worst to best (decreasing)
struct RunOrder
{
   
public:
    
    // @param env parsing environment. the weight domain is the one of of the
    // base wta in this environment (in part. zero and one).
    // @param flag : 0 if the best value is zero of the weight domain env,
    // 1 if the best value is one of the weight domain env.
    // RunOrder(const ParsingEnv& env, bool flag);

    /// @param failrun worst run for this ordering, i.e. a run whose
    /// weight if the worst weight value in the bounded weight domain.
    /// Its weight must not be unknown, and must be zero or one.
    RunOrder(std::shared_ptr<const Run> failrun);
    
    ~RunOrder();

    /// type of comparison function (ordering relation)
    typedef std::function<bool(std::shared_ptr<const Run>,
                               std::shared_ptr<const Run>)> Compare;
    
    /// ordering for Runs following the ordering of weight domain,
    /// i.e. min(r1, r2) iff weight r1 < weight r2
    /// i.e. runs are ordered from best weight to worse weight.
    /// partial runs and runs with unknown weight
    /// are considered to be the best.
    static Compare min;

    /// ordering for Runs following the inverse of the ordering of weight domain,
    /// i.e. max(r1, r2) iff weight r1 > weight r2
    /// i.e. runs are ordered from worse weight to best weight.
    /// partial runs and runs with unknown weight
    /// are considered to be the worst.
    static Compare max;

    /// worst value in weight domain (max wrt the semiring natural ordering)
    const Weight& worst() const;
    
    /// best value in weight domain (min wrt the semiring natural ordering)
    const Weight& best() const;

    /// worst run (bottom element)
    inline std::shared_ptr<const Run> failrun() const { return _failrun; }
   
private:
        
    /// zero (neutral for sum) of the weight semiring domain
    const Weight _zero;

    /// one (neutral for product) of the weight semiring domain
    const Weight _one;
   
    /// true if Runs will be enumerated of run by increasing weight (best to worse)
    /// false if Runs will be enumerated of run by decreasing weight  (worse to best).
    const bool _crescendo;
  
    /// last run in enumeration.
    std::shared_ptr<const Run> _failrun;
    
    /// whether the weight domain is a positive semiring or not.
    bool positive() const;

public: // data
    
    /// comparison function (ordering relation),
    /// min or max according to the flag of the constructor.
    Compare compare;
    
};

} // end namespace

#endif /* RunOrder_hpp */

/// @}

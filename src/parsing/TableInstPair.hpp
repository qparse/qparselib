//
//  TableInstPair.hpp
//  qparse
//
//  Created by Florent Jacquemard on 08/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#ifndef TableInstPair_hpp
#define TableInstPair_hpp

#include <stdio.h>
#include <assert.h>
#include <unordered_map> // and unordered_multimap
#include <queue>

#include "trace.hpp"
#include "Recordey.hpp"


namespace parsing{

/// pair
template<class K, class H>
class InstRec
{
    
};


// needs equality K.operator== and hash function H.operator()
template<class K, class H> using MapInstRec = std::unordered_multimap<K,K,H>;
// other option using K.operator< : std::multimap<K,K>;


/// table of instances:
/// undorered mapping associating to partial Keys of one or several complete instances.
/// @code multimap: key -> keys @endcode where
/// - key of type K (partial)
/// - keys of type K: instances of key
/// - H = Hasher for K
/// - equal+to is op. == defined in K
///
/// Parameters:
/// - K = concrete Key class (keys in table)
/// - H = KeyHasher (for keys)
template<class K, class H>
class TableInstances
{
public:
    
    /// @brief empty table of instances.
    TableInstances();
    
    ~TableInstances();
    
    
    /// @brief create a record it associated to pc in table and
    ///        add the pair <pp, it> to the table of instances.
    /// @param pp key. must be partial.
    /// @param pc key. must be complete.
    /// @warning pc must be an instance of pp.
    /// @warning pc must not be already associated with pp in this table.
    void add(const K& pp, const K& pc);
    //typename MapRecord<K, R, H>::iterator add(const K&, const K&);
    
    /// @brief return iterators to the registered instances for the given key.
    /// @param p key. must be partial.
    /// @return a pair made of:
    /// - first component is an iterator to the first instance of p registered
    /// - second component is an iterator to the end of the range
    ///   of instances of p.
    /// The instances range between these two iterators,
    /// including pair::first, but not pair::second.
    std::pair<typename MapInstances<K,H>::iterator,
    typename MapInstances<K,H>::iterator>
    get(const K& k);
    
    
    /// the given partial key has already been registered
    /// in this association table.
    /// searches this table for elements whose key is pp
    /// and returns the number of elements found.
    /// @param pp key. must be partial.
    /// @return the number of complete keys pc such that (pp, pc) is in this instance table.
    size_t count(const K& pp) const;
    
    
    /// test for the presence of the given pair of keys in this association table.
    /// @param pp key. must be partial.
    /// @param pc key. must be complete.
    /// @return whether the pair (pp, pc) is registered in the instance table.
    bool exists(const K& pp, const K& pc) const;
    
    
    /// nb of concrete keys registered in this table.
    inline size_t size() { return _instances.size(); }
    
    // DEBUG functions
    
    inline size_t nb_entries() { return _rows; }
    
    void dump() const;
    
    
private:
    
    /// associate to every registered partial ptr p
    /// an iterator to pairs (p', r)
    /// where p' is a complete instance of p assciated to r in _table
    MapInstances<K, H> _instances;
    
    /// stats.
    size_t _rows;
    
};

} // end namespace parsing


// separated definition of template class
#include "TableInstances.tpp"


#endif /* TableInstPair_hpp */

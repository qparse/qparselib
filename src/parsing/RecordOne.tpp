//
//  RecordOne.tpp
//  squanty
//
//  Created by Florent Jacquemard on 07/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//


namespace parsing{

template<class K>
RecordOne<K>::RecordOne(std::shared_ptr<const K> k,
                        std::shared_ptr<const RunOrder> comp):
Record<K>(k, comp),    // base class constructor with parameter
_best(comp->failrun()) // initialize with worst run
{ }


template<class K>
RecordOne<K>::RecordOne(const RecordOne<K>& rec):
Record<K>(rec),
_best(rec._best)
{ }


template<class K>
RecordOne<K>::~RecordOne()
{
        TRACE("delete RecordOne[{}]", *(this->_key));

    if(_best)
    {
        if (this->_key)
            TRACE("delete best run {} of RecordOne[{}]", *_best, *(this->_key));
        else
            TRACE("delete run {} of RecordOne[NULL]", *_best);
        // delete(_best);
    }
}


template<class K>
RecordOne<K>& RecordOne<K>::operator= (const RecordOne<K>& rec)
{
    if(this != &rec)
    {
        Record<K>::operator=(rec);
        _best = rec._best;
    };
    return *this;
}


template<class K>
std::shared_ptr<Recordo> RecordOne<K>::clone() const
{
    return std::make_shared<RecordOne<K>>(*this);
}


//Run* RecordOne<K>::best(TableParse<K,Record<K,H>,H>* table, size_t n)
template<class K>
std::shared_ptr<const Run> RecordOne<K>::best(size_t n)
{
    // this class is only for 1-best
    assert(n == 1);
    // std::string s = SymbLabel::to_string(_best->origin().label());
    assert(_best);
    assert (_best->fail() || _best->complete());
    assert (_best->fail() || (! _best->weight().unknown()));

    // k-best run _best is already computed at construction
    // return a fail Run when there exist no best
    if (_best->fail())
    {
        TRACE("RecordOne[{}].best : no best run", this->key());
    }
    
    return _best;
}


//template<class K>
//void RecordOne<K>::addCand(const Production& t)
//{
//    TRACE("RecordOne[{}].addTr {}", _key, t);
//    _nb_cand++;
//    add(new Run(t));
//}


template<class K>
size_t RecordOne<K>::addTerm(std::shared_ptr<const RunTerm> r)
{
    return generic_add(r);
}


template<class K>
size_t RecordOne<K>::addInner(std::shared_ptr<const RunInnero> r)
{
    return generic_add(r);
}


template<class K>
size_t RecordOne<K>::addInner(std::shared_ptr<const RunInner<K>> r)
{
    return generic_add(r);
}

template<class K>
size_t RecordOne<K>::generic_add(std::shared_ptr<const Run> r)
{
    const K& thiskey = this->key();
    this->_nb_cand++;

    // ignore invalid runs
    if (! this->valid(r))
    {
        if (r)
        {
            WARN("RecordOne[{}].add: reject invalid run {}", thiskey, *r);
            // delete r;
            return 3;
        }
        else
        {
            WARN("RecordOne[{}].add: reject NULL run", thiskey);
            return 2;
        }
    }
    
    assert(r);
    assert(r->inner() || r->terminal());
    assert(_best);
    assert(_best->fail() || _best->complete());
    assert(_best->fail() || (! _best->weight().unknown()));
    const RunOrder::Compare& thiscompare = this->compare();

    if (r->partial())
    {
        ERROR("RecordOne[{}].add: partial run {} rejected", thiskey, *r);
        // delete r;
        return 5;
    }
    else if (_best->fail() || thiscompare(_best, r))
    {
        assert(r->complete());
        // r is the new best
        if ( r->filtered() )
        {
            //if (_best)  delete _best;
           _best = r;
            TRACE("RecordOne[{}].add: add best run {}", thiskey, *r);
            assert(_best != nullptr);
            assert (_best->complete());
            assert (! _best->weight().unknown());
            return 0;
        }
        else
        {
            TRACE("RecordOne[{}].add: reject filtered-out run {} {}",
                  thiskey, *r, r->duration());
            this->_nb_best_rejected++;
            //delete r;
            return 6;
        }
    }
    else
    {
        TRACE("RecordOne[{}].add: ignore run {} (not better)", thiskey, *r);
        //delete r;
        return 1;
    }
}


template<class K>
bool RecordOne<K>::empty() const
{
    assert(_best != nullptr);
    return (_best->fail());
}


} // end namespace parsing

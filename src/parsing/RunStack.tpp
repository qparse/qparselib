//
//  RunStack.tpp
//  squant2
//
//  Created by Florent Jacquemard on 07/07/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//


namespace parsing{


template<class K>
RunTermTargeted<K>::RunTermTargeted(const K& k, std::shared_ptr<RunTerm> r):
_target(k),  // copy construct
_run(r)
{
    assert(r);
}



} // end namespace parsing

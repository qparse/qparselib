//
//  RecordQueue.tpp
//  qparse
//
//  Created by Florent Jacquemard on 12/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//


namespace parsing{


template<class K>
RecordQueue<K>::RecordQueue(std::shared_ptr<const K> k,
                            std::shared_ptr<const RunOrder> comp):
Record<K>(k, comp), // base class constructor with parameter
_cand(comp->compare),     // empty candidate list
_best() // empty best list
{ }


template<class K>
RecordQueue<K>::RecordQueue(const RecordQueue& rec):
Record<K>(rec),
_cand(rec._cand),   // copy candidate list
_best(rec._best)    // copy best list
{ }


template<class K>
RecordQueue<K>::~RecordQueue()
{
    // deallocate all runs in the _cand priority queue
    TRACE("delete RecordQueue[{}] candidate queue (size={})",
          this->_key, _cand.size());

    while (! _cand.empty())
    {
        std::shared_ptr<RunRanked<K>> r = _cand.top();
        _cand.pop();
        assert (r);    //delete r;
    }
    
    // no clear() for priority_queues
    _cand = std::priority_queue<std::shared_ptr<RunRanked<K>>,
                                std::vector<std::shared_ptr<RunRanked<K>>>,
                                RunOrder::Compare>();
    
    // deallocate all runs in the _best list
    TRACE("delete RecordQueue[{}] best list (size={})",
          this->_key, _best.size());

//    for (typename std::vector<std::shared_ptr<Run>>::iterator i = _best.begin();
//         i != _best.end(); ++i)
//    {
//        assert(*i);
//        delete (*i);
//    }
    
    _best.clear();
}


template<class K>
RecordQueue<K>& RecordQueue<K>::operator= (const RecordQueue<K>& rec)
{
    if(this != &rec)
    {
        Record<K>::operator=(rec);
        _best = rec._best;
    };
    return *this;
}


template<class K>
std::shared_ptr<Recordo> RecordQueue<K>::clone() const
{
    return std::make_shared<RecordQueue<K>>(*this);
}


template<class K>
bool RecordQueue<K>::empty() const
{
    return (_cand.empty() && _best.empty());
}


template<class K>
std::shared_ptr<const Run> RecordQueue<K>::best(size_t n)
{
    assert(n > 0);
    assert(this->_key);
    const K& key = *(this->_key);

    // n-best run already computed for this record
    if (sizeBest() >= n)
    {
        return getBest(n);
    }
    
    // otherwise, we construct the next best run
    
    // cannot construct best (all runs constructed in best table)
    // return a fail Run
    if (emptyCand())
    {
        TRACE("RecordQueue[{}]: no best {}", key, n);
        std::shared_ptr<const Run> fail = this->failrun();
        assert(fail);
        assert(fail->fail());
        return this->failrun();
    }
    
    // otherwise, process the best candidate r
    /// @warning runs with unevaluated weight must always be on top
    std::shared_ptr<RunRanked<K>> r = popCand();
    assert(r);
    // candidate's weight not evaluated
    if (r->weight().unknown())
    {
        // re-evaluate the weight and filter of the run r
        r->reset();

        // one subrun does not exist,
        // so doesn't the run.
        // the evaluation of weight fails and
        // the run is not added to the best list.
        // we do not push this run back to the heap of candidates
        // we do not push next runs because they can neither be evaluated.
        if (r->weight().unknown())
        {
            TRACE("RecordQueue.best: ignored UKN candidate {}.", *r);
        }
        else if (r->weight().is_zero())
        {
            WARN("RecordQueue.best: ignore zero weight run {}", *r);
        }
        else
        {
            // re-insert (and filter) run to candidate queue
            addCand(r);
        }
        
        // tail recursive call to extract and
        // evaluate the other candidates
        return best(n);
    }
    // all candidates have been evaluated
    // because unknown weight has priority
    // r is the best run
    else
    {
        // add the best run to the best table
        addBest(r);
        
        // compute next candidate if run is not terminal
        if (r->inner())
        {
            addNext(r);
        }
        
        // tail recursive call, either:
        // return the run that was just added (if place in best list matches)
        // or try to construct more best runs if needed.
        return best(n);
    }
}


//template<class K>
//void BRecord<K>::addCand(const Kroduction& t)
//{
//    TRACE("BRecord[{}].addTr {}", _key, t);
//    _nb_cand++;
//    add(new Run(t));
//}


template<class K>
size_t RecordQueue<K>::addTerm(std::shared_ptr<const RunTerm> r)
{
    if (r)
    {
        // cast to RunRanked
        std::shared_ptr<RunRanked<K>> rr = std::make_shared<RunRanked<K>>(*r);
        //delete r;
        return generic_add(rr);
    }
    else
    {
        //std::shared_ptr<RunRanked<K>> nr = std::make_shared<RunRanked<K>>(nullptr);
        return generic_add(nullptr);
    }
}


template<class K>
size_t RecordQueue<K>::addInner(const std::shared_ptr<const RunInnero> r)
{
    if (r)
    {
        const std::shared_ptr<const RunInnero>& r_ref = r;
        std::shared_ptr<const RunInner<K>> ri =
        std::dynamic_pointer_cast<const RunInner<K>, const RunInnero>(r_ref);
        assert(ri);
        std::shared_ptr<RunRanked<K>> rr = std::make_shared<RunRanked<K>>(*ri);
        //delete r;
        // if (ri) delete ri;
        return generic_add(rr);
    }
    else
    {
        //const std::shared_ptr<RunRanked<K>} nr = std::make_shared<RunRanked<K>>(nullptr);
        return generic_add(nullptr);
    }
}


template<class K>
size_t RecordQueue<K>::addInner(std::shared_ptr<const RunInner<K>> r)
{
    if (r)
    {
        std::shared_ptr<RunRanked<K>> rr = std::make_shared<RunRanked<K>>(*r);
        //delete r;
        return generic_add(rr);
    }
    else
    {
        //const std::shared_ptr<RunRanked<K>} nr = std::make_shared<RunRanked<K>>(nullptr);
        return generic_add(nullptr);
    }
}


// external add
template<class K>
size_t RecordQueue<K>::generic_add(std::shared_ptr<RunRanked<K>> r)
{
    const K& thiskey = *(this->_key);

    if (r == nullptr)
    {
        TRACE("RecordQueue[{}].add: ignore NULL run {}", thiskey);
        return 2;
    }
    
    assert(r->complete());
    
    // ignore invalid runs
    if (this->valid(r))
    {
        assert(r->inner() || r->terminal());
        // add 'best-index'=1 to all children
        return addCand(r);
    }
    else
    {
        TRACE("RecordQueue[{}].add: ignore invalid run {}", thiskey, *r);
        // delete r;
        return 3;
    }
}


// add run to the heap of candidates
// accept unknown weight
template<class K>
size_t RecordQueue<K>::addCand(std::shared_ptr<RunRanked<K>> r)
{
    assert(r);
    assert(r->complete());
    // assert(! r->weight().is_zero());
    assert(this->_key != nullptr);
    const K& thiskey = *(this->_key);
    
    if (r->weight().unknown() || r->filtered())
    {
        TRACE("RecordQueue[{}] push candidate {} run {}",
              thiskey, (r->weight().unknown()?"unevaluated":""), *r);
        this->_cand.push(r);
        this->_nb_cand++;
        return 1;
    }
    else
    {
        TRACE("RecordQueue[{}] reject filtered-out candidate run {}",
              thiskey, *r);
        this->_nb_best_rejected++;
        // delete r;
        return 0;
    }
}


// extract run to the heap of candidates
template<class K>
std::shared_ptr<RunRanked<K>> RecordQueue<K>::popCand()
{
    assert(this->_key != nullptr);
    const K& thiskey = *(this->_key);

    std::shared_ptr<RunRanked<K>> r = _cand.top();
    assert(r != nullptr); // should be no NULL runs in candidate list
    assert(r->inner() || r->terminal());
    assert(r->complete());  // no missing child
    TRACE("RecordQueue[{}] pop cand run {}", thiskey, *r);
    _cand.pop();
    return r;
}


template<class K>
bool RecordQueue<K>::emptyCand() const
{
    return _cand.empty();
}


// add run to the list of bests
template<class K>
void RecordQueue<K>::addBest(std::shared_ptr<RunRanked<K>> r)
{
    assert(r);
    assert(! r->weight().unknown());
    assert(r->inner() || r->terminal());
    assert(r->complete()); // weight is evaluated
    assert(this->_key != nullptr);
    const K& thiskey = *(this->_key);

    if (r->filtered())
    {
        TRACE("RecordQueue[{}] add best run {}", thiskey, *r);
        /// @todo
        /// if inner, push best
        /// if termimal, cast to RunTerm and push

        if (r->terminal())
        {
            // cast back to RunTerm
            std::shared_ptr<RunTerm> rt =
                std::make_shared<RunTerm>(r->origin(), r->length());
            //delete r;
            _best.push_back(rt);
        }
        else
        {
            assert(r->inner());
            _best.push_back(r);
        }
    }
    else
    {
        TRACE("RecordQueue[{}] rejected run filtered-out {}", thiskey, *r);
        this->_nb_best_rejected++;
        // delete r;
    }
}


template<class K>
std::shared_ptr<const Run> RecordQueue<K>::getBest(size_t n)
{
    assert(n > 0);
    assert(_best.size() >= n);
    std::shared_ptr<const Run> best = _best[n-1]; /// @todo RunRanked<K>*
    assert(best != nullptr);
    assert(best->complete());
    assert(! best->weight().unknown());
    
    /// @todo if inner, return best
    /// if termimal, cast to RunTerm and return
    return best;
}


template<class K>
size_t RecordQueue<K>::sizeBest() const
{
    return _best.size();
}


// template<class K>
// bool RecordQueue<K>::bestFilter(const Run* r)
// {
//     assert (r);
//     assert(r->inner() || r->terminal());
//
//    // filter acc. to optimization flags
//     if (OPT_RUN_UNIT)
//     {
//         assert (OPT_RUN_DUR);
//         if (r->duration.unit() && r->inner())
//             return false;
//     }
//     return true;
// }


template<class K>
void RecordQueue<K>::addNext(std::shared_ptr<RunRanked<K>> r)
{
    assert(r);
    assert(r->complete());
    assert(r->inner());
    
    // add next candidates
    for (int i = 0; i < r->arity(); i++)
    {
        // copy and increment rank
        // @warning the run added (to candidates) has unknown weight
        // the evaluation is delayed to next call to best (lazy eval)
        addCand(std::make_shared<RunRanked<K>>(*r, i));
        
        // do not delete r : it has been placed in best list!
    }
}


} // end namespace parsing

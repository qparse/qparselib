//
//  RunInnero.hpp
//  squant2
//
//  Created by Florent Jacquemard on 03/04/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{


#ifndef RunInnero_hpp
#define RunInnero_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <assert.h>
#include <vector>


#include "trace.hpp"
#include "SymbLabel.hpp" // symbolic labels
#include "Weight.hpp"
#include "Transition.hpp"
#include "Runey.hpp"
#include "RunTerm.hpp"
#include "Recordey.hpp"


namespace parsing{


/// Visitable by Records
/// type erasure for RunInner<K> (without template)
class RunInnero : public Run
{

public:
    
    RunInnero(const Transition& tr);
    
    RunInnero(const Transition& tr, label_t lab, const Weight& w);

    /// Partial cast.
    RunInnero(const RunTerm& r);
    
    RunInnero(const RunInnero& r);
    
    virtual ~RunInnero();
    
    virtual std::shared_ptr<RunInnero> clone() const=0;
    
    /// this run is terminal (leaf).
    virtual bool terminal() const;
    
    /// this run is inner.
    virtual bool inner() const;
    

};


} // end namespace parsing


#endif /* RunInnero_hpp */


/// @}

//
//  KeySIP.cpp
//  squanty
//
//  Created by Florent Jacquemard on 29/10/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include "KeySIP.hpp"


namespace parsing{


// static
//pre_t KeySIP::P_UNKNOWN = -1;

// static
//pre_t KeySIP::P_NULL = 0;

// static
//bool KeySIP::P_KNOWN(pre_t x)
//{
//    return (x >= 0);
//}


// top constructor (top state = first bar)
KeySIP::KeySIP(rtu_t barlen,
               const ParsingEnv& env,
               bool f_open,
               pre_t pre, pre_t post):
KeySI(barlen, env, f_open),
_pre(pre),
_post(post)
{
    assert(P_KNOWN(pre));
}


// internal use
KeySIP::KeySIP(state_t s,
               IntervalTree* node,
               rtu_t barlen,
               pre_t pre, pre_t post):
KeySI(s, node, barlen),
_pre(pre),
_post(post)
{
    assert(node);
}


// successor key (fs or ns or instance)
KeySIP::KeySIP(const KeySIP& k,
               RunInner<KeySIP>& r,
               const ParsingEnv& env):
KeySI(k, (RunInner<KeySI>&) r, env),
_pre(k._pre), // k is the parent (fs, ns) or matcher (instanciate)
_post(k._post)
{
    assert(r.inner());
    const Transition& t = r.origin();
    assert(t.inner());
    assert(0 < t.size());
    assert(t.size() <= t.arity());

    // first child, k is parent
    if (r.empty())
    {
        assert(r.partial());
        assert(_state == t.state(0)); // check downcast of KeyS
        init_firstchild(k, t, env);
    }
    // instanciate head (inner case)
    else if (r.complete())
    {
        assert(r.size() == t.size());
        assert(_state == k._state); // check downcast of KeyS
        assert(r.size() > 0);
        const KeySIP& last = r.key(r.size()-1);
        init_instanciate(k, last, t, env);
    }
    // next sibling
    else
    {
        assert(r.partial());
        size_t i = r.size();  // index of next sibling in r
        assert(0 < i);        // there is at least one previous sibling
        assert(i < t.size()); // r partial
        assert(_state == t.state(i)); // check downcast of KeyS
        const KeySIP& last = r.key(i-1);
        init_nextsibling(k, last, t, i, env);
    }
}

void KeySIP::init_firstchild(const KeySIP& parent,
                             const Transition& t,
                             const ParsingEnv& env)
{
    assert(P_KNOWN(parent._pre));
    label_t a = t.label();
    assert((! SymbLabel::bar(a)) || (t.arity() == 2));
    assert((! SymbLabel::bar(a)) || (t.multiplicity(0) == 1));

    if (SymbLabel::tuple(a) || SymbLabel::bar(a)) //  || SymbLabel::rest(a))
    {
        _pre = parent._pre;
        _post = P_UNKNOWN;
    }
    else  /// @todo cases grace and fork
    {
        ERROR("KeySIP.firstChild: unexpected symbol in tr: _state -> {}", t);
    }
    assert(P_KNOWN(_pre));
}


void KeySIP::init_nextsibling(const KeySIP& parent,
                              const KeySIP& previous,
                              const Transition& t,
                              size_t i, // index of next
                              const ParsingEnv& env)
{
    assert(t.inner());
    assert(previous.complete());
    label_t a = t.label();
    assert((! SymbLabel::bar(a)) || (t.arity() == 2));
    assert((! SymbLabel::bar(a)) || (t.multiplicity(0) == 1));

    if (SymbLabel::tuple(a) || SymbLabel::bar(a))
    {
        _pre = previous._post;
        _post = P_UNKNOWN;
    }
    /// @todo cases grace and fork
    else

    {
        ERROR("KeySIP.nextSibling: unexpected symbol in tr: _state -> {}", t);
    }
    assert(P_KNOWN(_pre));
}


void KeySIP::init_instanciate(const KeySIP& head,
                              const KeySIP& last,
                              const Transition& t,
                              const ParsingEnv& env)
{
    assert(head.partial()); // will be copied and completed
    assert(last.complete());
    assert(P_KNOWN(head._pre));
    _pre = head._pre;       // copy
    assert(P_KNOWN(last._post));
    _post = last._post;     // up-propagation of post
    assert(this->complete());
}


// copy
KeySIP::KeySIP(const KeySIP& k):
KeySI(k),
_pre(k._pre),
_post(k._post)
{ }


// copy and instanciate.
KeySIP::KeySIP(const KeySIP& k, pre_t post):
KeySI(k),
_pre(k._pre),
_post(post)
{
    assert(k.partial());
    // this key is now complete
    assert(P_KNOWN(post));
    assert(this->complete());
}


KeySIP::~KeySIP()
{
    TRACE("delete KeySIP");
}


KeySIP& KeySIP::operator= (const KeySIP& p)
{
    if(this != &p)
    {
        KeySI::operator=(p);
        _pre = p._pre;
        _post = p._post;
    };
    return *this;
}


bool KeySIP::operator==(const KeySIP& p) const
{
    return (KeySI::operator==(p) &&
            (_pre == p._pre) &&
            (_post == p._post));
}


bool KeySIP::operator!=(const KeySIP& p) const
{
    return (! (*this == p));
}


bool KeySIP::instance(const KeySIP& p) const
{
    assert(_pre != P_UNKNOWN);
    assert(p._pre != P_UNKNOWN);
    return (KeySI::instance(p) && ((p._post == P_UNKNOWN) ||
                                   (_post == p._post)));
}


bool KeySIP::complete() const
{
    assert(_pre != P_UNKNOWN);
    return (KeySI::complete() && (_post != P_UNKNOWN));
}


size_t KeySIP::size() const
{
    assert(_pre != P_UNKNOWN);
    assert(_interval);
    assert(_interval->aligned());
    return _pre + _interval->lsize();
}


void KeySIP::runs(StackInner<KeySIP>& si,
                  // std::stack<RunInner<KeySIP>*>& si,
                  StackTerm<KeySIP>& st,
                  // std::stack<std::pair<const KeySIP*, RunTerm*>>& st,
                  ParsingEnv& env) const
{
    RunFactory<KeySIP>::runs(*this, si, st, env);
}


void KeySIP::RunsTerminal(const Transition& t,
                          StackTerm<KeySIP>& st,
                          const ParsingEnv& env) const
{
    assert(P_KNOWN(_pre));
    assert(_interval);
    const label_t& a = t.label();
    assert(SymbLabel::terminal(a));
      
    if (! _interval->aligned())
    {
        assert(SymbLabel::bar(a));
        WARN("KeySIP RunsTerminal: unaligned interval {} for {}",
             *(_interval), SymbLabel::to_string(a));
        assert(env.segment);
        _interval->align(env.segment);
    }

    // this key is complete
    if (P_KNOWN(_post))
    {
        WARN("KeySIP : instance as leaf of complete key {}", *this);
        if (_post == _interval->rsize())
        {
            RunsTerminalComplete(t, st, env);
        }
        else
        {
            ERROR("KeySIP : error in complete key {}", *this);
            return;
        }
    }
    // this key is partial
    else
    {
        //copy with _post = _interval->rsize();
        KeySIP knew(*this, _interval->rsize());
        assert(knew.complete());
        knew.RunsTerminalComplete(t, st, env);
    }
}

void KeySIP::RunsTerminalComplete(const Transition& t,
                                  StackTerm<KeySIP>& st,
                                  const ParsingEnv& env) const
{
    assert(P_KNOWN(_pre));
    assert(P_KNOWN(_post));
    assert(_interval);

    const label_t& a = t.label();
    assert(SymbLabel::terminal(a));

        
    /// filter term label
    if (SymbLabel::note(a) || SymbLabel::rest(a) ||
        SymbLabel::continuation(a) || SymbLabel::dot(a))
    {
        if ((_post != P_UNKNOWN) && (_post != _interval->rsize()))
            return; // add nothing
        
        assert(_pre != P_UNKNOWN);
        size_t n = SymbLabel::nbEvents(a);
        if (n == _pre + _interval->lsize())
        {
            Weight w = terminalWeight(a, t, env);
            assert(! w.unknown());
            // n points = grace-notes + grace-rests + 1 note or -1
            RunFactory<KeySIP>::RunTermmk(*this, t, a, w, n, st);
        }
    }
    /// double bar (end of score)
    else if (SymbLabel::bar(a) && (SymbLabel::arity(a) == 0))
    {
        TRACE("KeySIP: terminal bar interval: {} ({}aligned) for transition {}",
              *(_interval), ((! _interval->aligned())?"not ":""), t);
        // && (DBL_APPROX_EQUAL(_interval->rend(), _interval->rbegin()))
        if ((_interval->rduration() < _barlen) && (! _interval->inhabited()))
        {
            Weight w = terminalWeight(a, t, env);
            assert(! w.unknown());
            // 0 or -1 point to read in interval
            RunFactory<KeySIP>::RunTermmk(*this, t, a, w, 0, st);
        }
    }
    else if (SymbLabel::dummy(a))
    {
        ERROR("KeySIP: unexpected dummy symbol");
    }
    else
    {
        ERROR("KeySIP: unexpected terminal symbol {}", SymbLabel::to_string(a));
    }
}


Weight KeySIP::terminalWeight(label_t a,
                              const Transition& tr,
                              const ParsingEnv& env) const
{
    assert(tr.terminal());
    assert(SymbLabel::terminal(a));
    assert(this->complete());
    // assert(this->leaf(tr, env) != 0);

    // tweight = transition weight = complexity
    Weight tweight = tr.weight(); // copy
    
    assert(! tweight.unknown());

    // terminal bar interval:
    // there are no points hence no need to compute the distance
    if (SymbLabel::bar(tr.label()) && (SymbLabel::arity(tr.label())) == 0)
    {
        TRACE("KeySIP: terminal bar interval: {} ({}aligned) for transition {}",
              *(_interval),
              ((! _interval->aligned())?"not ":""),
              tr);
        assert(tr.label() == a);
        assert(! _interval->inhabited());
        assert(_interval->rduration() < _barlen);
    }
    // combine transition weight with distance weight
    // CST_WEIGHT_TYPE == WeightDom::STOCHASTIC
    else if (tweight.hasType("ViterbiWeight"))
    {
        assert(env.segment);
        assert(_interval);
//        assert(_pre != PP_UNKNOWN);          // csq complete
//        assert(_post != PP_UNKNOWN);         // csq complete
//        assert(_post == _interval->rsize()); // csq filterTerminal by leaf
        tweight *= Weight(new GaussianDistance(env.segment, _interval));
    }

    // CST_WEIGHT_TYPE == WeightDom::PENALTY
    else if (tweight.hasType("TropicalWeight"))
    {
        assert(_interval);
        // compute distance to input segment
        // ALPHA applied to dist only (not complexity)
        Weight dist = Weight(new Distance(env.segment, _interval));
        TRACE("KeySIP: tropical distance for inter={} tr={} dist={}",
              *(this), tr, dist);

        // new weight = (1 - alpha) . former transition weight * alpha . distance
        // update: alpha applied to dist only (not to complexity)
        tweight *= dist; // tropical weight product is a sum
    }
    else
    {
        ERROR("KeySIP.weight: unexpected weight type");
        // tweight will be UNDEF
    }
    
    return tweight;
}


void KeySIP::RunsInner(const Transition& t,
                       StackInner<KeySIP>& si,
                       // std::stack<RunInner<KeySIP>*>& si,
                       const ParsingEnv& env) const
{
    const label_t& a = t.label();
    assert(SymbLabel::inner(a));
    
    if (SymbLabel::tuple(a))
    {
        assert(_interval);
        // we do not divide empty intervals
        if (_interval->inhabited())
            RunFactory<KeySIP>::RunInnermk(*this, t, si);
    }
    else if (SymbLabel::grace(a))
    {
        assert(SymbLabel::arity(a) == 2);
        ERROR("KeySIP: outside g.n. not supported, label={}",
              SymbLabel::to_string(a));
    }
    else if (SymbLabel::fork(a))
    {
        assert(SymbLabel::arity(a) == 2);
        ERROR("KeySIP: voice fork not supported, label={}",
              SymbLabel::to_string(a));
    }
    else if (SymbLabel::bar(a))
    {
        assert(SymbLabel::arity(a) == 2);
        assert(_interval);
        if ((_interval->rduration() >= _barlen) || _interval->inhabited())
            RunFactory<KeySIP>::RunInnermk(*this, t, si);
        else
            TRACE("RunsInner: do not add bar (fine) {}", *this);
    }
    else
    {
        ERROR("KeySIP: unexpected inner symbol {}", SymbLabel::to_string(a));
    }
}


std::ostream& operator<<(std::ostream& o, const KeySIP& p)
{
    if (p._interval == NULL)
    {
        o << KEY_LPAR << p.state() << KEY_RPAR;
        return o;
    }
    
    o << KEY_LPAR;
    o << " " << p.state() << " ";
    
    // short version
    if (p._interval)
    {
        o << *(p._interval);
    }
    else
    {
        o << "[]";
    }
    assert(p._pre != P_UNKNOWN);
    o << " pre=";
    o << p._pre;
    o << " post=";
    if (p._post == P_UNKNOWN)
        o << "UnK";
    else
        o << p._post;
    o << KEY_RPAR;
    
    return o;
}



} // end namespace parsing

//
//  KeySIR.cpp
//  squanty
//
//  Created by Florent Jacquemard on 09/12/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include "KeySIR.hpp"


namespace parsing{


// static
//ppre_t KeySIR::PP_UNKNOWN = std::make_pair<long, long>(-1, -1);

// static
//ppre_t KeySIR::PP_NULL = std::make_pair<long, long>(0, 0);

// static
//bool KeySIR::PP_KNOWN(ppre_t x)
//{
//    return ((x.first >= 0) && (x.second >= 0));
//}


// internal use
KeySIR::KeySIR(state_t s, IntervalTree* node, rtu_t len,
               ppre_t pre, ppre_t post):
KeySI(s, node, len),
_pre(pre),
_post(post)
{ }


// copy
KeySIR::KeySIR(const KeySIR& k):
KeySI(k),
_pre(k._pre),
_post(k._post)
{ }


// top constructor (top state = first bar)
KeySIR::KeySIR(rtu_t len, const ParsingEnv& env, bool f_open,
               ppre_t pre, ppre_t post):
KeySI(len, env, f_open),
_pre(pre),
_post(post)
{
    assert(PP_KNOWN(pre));
}


// firstChild
KeySIR::KeySIR(const KeySIR& parent,
               const Transition& tr,
               const ParsingEnv& env):
KeySI(parent, tr, env),
_pre(parent._pre),
_post(PP_UNKNOWN)
{
    assert(PP_KNOWN(_pre));
    // construction of sub-interval is the same as in KeySI
    /// @todo cases grace and fork
}


// nextSibling
KeySIR::KeySIR(const KeySIR& previous,
             size_t i,
             const KeySIR& parent,
             const Transition& tr,
             const ParsingEnv& env):
KeySI(previous, i, parent, tr, env),
_pre(previous._post),
_post(PP_UNKNOWN)
{
    assert(previous.complete());
    // construction of sub-interval is the same as in KeySI
    /// @todo cases grace and fork
}


// instanciate: copy and up-propagation of post
KeySIR::KeySIR(const KeySIR& head,
               label_t a,
               const KeySIR& lastchild,
               const ParsingEnv& env):
KeySI(head, a, lastchild, env),
_pre(head._pre),
_post(lastchild._post)
{
    assert(head.partial());
    assert(lastchild.complete());
    // this key is now complete
    assert(PP_KNOWN(this->_pre));
    assert(PP_KNOWN(this->_post));
}


// complete leaf: copy and computation of post
KeySIR::KeySIR(const KeySIR& head,
               label_t a,
               const ParsingEnv& env):
KeySI(head, a, env),
_pre(head._pre)
{
    assert(SymbLabel::terminal(a));
    assert(_interval);
    // case of terminal bar symbol (double bar)

    assert(PP_KNOWN(_pre));

    if (PP_KNOWN(head._post))
    {
        ERROR("KeySIR : instance as leaf of complete ptr {}", head);
        _post = head._post;
        assert(_post.first == _interval->rsize());
        assert(_post.second == _interval->roffsets());
    }
    else
    {
        if (! _interval->aligned())
        {
            assert(SymbLabel::bar(a));
            assert(env.segment);
            _interval->align(env.segment);
        }
        _post = std::make_pair(_interval->rsize(), _interval->roffsets());
    }
    assert(PP_KNOWN(_post));
}


KeySIR::~KeySIR()
{
    TRACE("delete KeySIR");
}


size_t KeySIR::size() const
{
    assert(_interval);
    return _pre.first + _interval->lsize();
}


Run<KeySIR>* KeySIR::failRun(const ParsingEnv& env) const
{
    assert(env.wta);
    const Transition& t1 = env.dummyTransition_one();
    assert(t1.weight().is_one());
    if (! _interval->aligned())
    {
        assert(env.segment);
        _interval->align(env.segment);
    }
    label_t a = SymbLabel::make_fail(this->size());

    return new Run<KeySIR>(t1, a, t1.weight());
}


void KeySIR::leaves(const Transition& tr,
                    const ParsingEnv& env,
                    std::queue<KeySIR>& q) const
{
    assert(this->partial());
    assert(q.empty());
    size_t nbl = leaf(tr, env);
    
    if (nbl == 0)
    {
        ERROR("KeySIR: leaves called for 0 leaf");
    }
    else if (nbl == 1)
    {
        ERROR("KeySIR: leaves called for 1 leaf, call cstr instead");
        label_t a = tr.label();
        q.emplace(*this, a, env);
    }
}

// KeySIR::innerWeight = KeySI::innerWeight

Weight KeySIR::terminalWeight(label_t a,
                              const Transition& tr,
                              const ParsingEnv& env) const
{
    assert(tr.terminal());
    assert(SymbLabel::terminal(a));
    assert(this->complete());
    assert(this->leaf(tr, env) != 0);

    // tweight = transition weight = complexity
    Weight tweight = tr.weight(); // copy
    
    assert(! tweight.unknown());

    // terminal bar interval:
    // there are no points hence no need to compute the distance
    if (SymbLabel::bar(tr.label()) && (SymbLabel::arity(tr.label())) == 0)
    {
        TRACE("KeySIR: terminal bar interval: {} ({}aligned) for transition {}",
              *(_interval), ((! _interval->aligned())?"not ":""), tr);
        assert(tr.label() == a);
        assert(! _interval->inhabited());
        assert(_interval->rduration() < _barlen);
    }
    // combine transition weight with distance weight
    // CST_WEIGHT_TYPE == WeightDom::STOCHASTIC
    else if (tweight.hasType("ViterbiWeight"))
    {
        assert(env.segment);
        assert(_interval);
        TRACE("KeySIR: new bar interval: {} ({}aligned) for transition {}",
              *(_interval), ((! _interval->aligned())?"not ":""), tr);
        //assert(_pre != PP_UNKNOWN);          // csq complete
        //assert(_post != PP_UNKNOWN);         // csq complete
        //assert(_post == _interval->rsize()); // csq filterTerminal by leaf
        tweight *= Weight(new GaussianDistance(env.segment, _interval));
    }

    // CST_WEIGHT_TYPE == WeightDom::PENALTY
    else if (tweight.hasType("TropicalWeight"))
    {
        assert(_interval);
        // compute distance to input segment
        // ALPHA applied to dist only (not complexity)
        Weight dist = Weight(new Distance(env.segment, _interval));
        TRACE("KeySIR: tropical distance for inter={} tr={} dist={}",
              *(this), tr, dist);

        // new weight =
        // (1 - alpha) . former transition weight * alpha . distance
        // update: alpha applied to dist only (not to complexity)
        tweight *= dist; // tropical weight product is a sum
    }
    else
    {
        ERROR("KeySIR.weight: unexpected weight type");
        // tweight will be UNDEF
    }
    
    return tweight;
}


// KeySIR::filterInnerLabel: same as for KeySI

//bool KeySIR::filterTerminalLabel(const label_t& a,
//                                 const ParsingEnv& env) const
//{
//    return (pointsTerminalLabel(a, env) >= 0);
//}


bool KeySIR::checkPost() const
{
    return (_post == PP_UNKNOWN) ||
            ((_post.first == _interval->rsize()) &&
             (_post.second == _interval->roffsets()));
}


bool KeySIR::filterTerminalLabel(const label_t& a,
                                 const ParsingEnv& env) const
{
    assert(SymbLabel::terminal(a));
    assert(_interval);
    if (! _interval->aligned())
    {
        WARN("KeySIR: unaligned interval {} for filtering {}",
             *(_interval), SymbLabel::to_string(a));
        assert(env.segment);
        _interval->align(env.segment);
    }

    /// double bar (end of score)
    if (SymbLabel::bar(a))
    {
        assert(_interval);
        // && (DBL_APPROX_EQUAL(_interval->rend(), _interval->rbegin()))
        return ((_interval->rduration() < _barlen) &&
                (! _interval->inhabited()));
        // 0 or -1 points to read in interval
    }
    else if (SymbLabel::dummy(a))
    {
        WARN("KeySIR: filter dummy symbol");
        return false;
    }
    else if (SymbLabel::continuation(a) || SymbLabel::dot(a))
    {
        return ((checkPost() && (_pre.first + _interval->lsize() == 0)));
        // 0 or -1 points to read in interval
    }
    /// @todo case of rest preceeded by grace notes.
    /// here we consider only one rest alone.
    else if (SymbLabel::rest(a))
    {
        size_t n = _interval->loffsets() + _pre.second; // always 1 ?
     
        if (checkPost() && (n > 0) &&
            (_pre.first == _pre.second) && // no onset, at most 1 offset
            (_interval->lsize() == _interval->loffsets()))
        {
            return true;  // n points to read
        }
        // special case of performance beginning with a gap (not an event).
        else if ((_pre.first == 0) &&
                 (_interval->lsize() == 0) &&
                 (_interval->rbegin() == 0))
        {
            WARN("KeySIR: first event in performance is not at 0, padding with a rest");
            return true;  // n points to read
        }
        else
            return false;
    }
    else if (SymbLabel::note(a))
    {
        // number of onsets
        size_t n = (_interval->lsize() - _interval->loffsets()) +
                   (_pre.first - _pre.second);
        return (checkPost() &&
                (n >= 1) &&
                (SymbLabel::nbEvents(a) == n));
                // (_pre.first + _interval->lsize()) or -1 points to read
    }
    // original code, to check
    else if (SymbLabel::note(a) || SymbLabel::rest(a) ||
             SymbLabel::continuation(a) || SymbLabel::dot(a))
    {
        ERROR("KeySIR: SHOULD NOT BE HERE");
        if ((_post != PP_UNKNOWN) &&
            ((_post.first != _interval->rsize()) ||
             (_post.second != _interval->roffsets())))
            return false; // -1 point ro read
        
        assert(_pre != PP_UNKNOWN);

    size_t eventn = _interval->lsize();
    size_t offsetn = _interval->loffsets();
	size_t nbEn = eventn - offsetn + _pre.first - _pre.second;

    if (nbEn > 1)
	  return (SymbLabel::nbEvents(a) == nbEn);
	else if (nbEn == 1)
    {
	  if (SymbLabel::note(a))
	    return (SymbLabel::nbEvents(a) == nbEn);
      else
	    return false;
	}
    else if (nbEn == 0 && (offsetn + _pre.second) > 0)
    {
	  if (SymbLabel::rest(a))
	    return (SymbLabel::nbEvents(a) == 1);
      else
	    return false;
	}
    else // nbEn == 0 && _interval->loffsets() == 0 && _pre.second == 0
        return (SymbLabel::nbEvents(a) == _pre.first + _interval->lsize());
    }
    else
    {
        ERROR("KeySIR: unexpected terminal symbol {}", SymbLabel::to_string(a));
        return false;
    }
}

//        size_t ll = _interval->lsize();
//        int offsetn = 0;
//        int eventn = 0;
//        if(ll>0){
//          size_t j=_interval->lfirst();
//          for(int i=0;i<ll;i++){
//            if(env.segment->offset(j)){
//              offsetn++;
//            }
//            j++;
//            eventn++;
//          }
//        }
        
    // or without loffsets():
    // size_t offsetn = 0;
    // size_t i0 = _interval->lfirst();
    // for(size_t i = 0; i < eventn; i++)
    //     if(env.segment->offset(i0 + i))
   //         offsetn++;
        
//    if(eventn - offsetn > 0 && offsetn > 0)
//        return (SymbLabel::nbEvents(a) == _pre + _interval->lsize() - offsetn);
//    else if(eventn - offsetn == 0 && offsetn > 1)
//        return (SymbLabel::nbEvents(a) == _pre + _interval->lsize() - offsetn + 1);
//    else
//        return (SymbLabel::nbEvents(a) == _pre + _interval->lsize());
//    }


KeySIR& KeySIR::operator= (const KeySIR& p)
{
    if(this != &p)
    {
        KeySI::operator=(p);
        _pre = p._pre;
        _post = p._post;
    };
    return *this;
}


//bool KeySIR::operator==(const KeySIR& p) const
//{
//    if (KeySI::operator!=(p))
//        return false;
//    else
//        return ((_pre == p._pre) && (_post == p._post));
//}


bool KeySIR::operator==(const KeySIR& p) const
{
    return (KeySI::operator==(p) &&
            (_pre == p._pre) &&
            (_post == p._post));
//    return ((_state == p._state) &&
//            (_pre == p._pre) &&
//            (_post == p._post) &&
//            this->equal_interval(p));
}


bool KeySIR::operator!=(const KeySIR& p) const
{
    return (! (*this == p));
}


bool KeySIR::instance(const KeySIR& p) const
{
    assert(_pre != PP_UNKNOWN);
    assert(p._pre != PP_UNKNOWN);
    return (KeySI::instance(p) && ((p._post == PP_UNKNOWN) ||
                                   (_post == p._post)));
}


// always complete
bool KeySIR::complete() const
{
    assert(_pre != PP_UNKNOWN);
    return (KeySI::complete() && (_post != PP_UNKNOWN));
}


std::ostream& operator<<(std::ostream& o, const KeySIR& p)
{
    if (p._interval == NULL)
    {
        o << KEY_LPAR << p.state() << KEY_RPAR;
        return o;
    }
    
    o << KEY_LPAR;
    o << " " << p.state() << " ";
    
    // short version
    if (p._interval)
    {
        o << *(p._interval);
    }
    else
    {
        o << "[]";
    }
    assert(p._pre != PP_UNKNOWN);
    o << " pre=<";
    o << p._pre.first;
    o << ", ";
    o << p._pre.second;
    o << ">";

    o << " post=";
    if (p._post == PP_UNKNOWN)
        o << "UnK";
    else
    {
        o << " <";
        o << p._post.first;
        o << ", ";
        o << p._post.second;
        o << ">";
    }
    o << KEY_RPAR;
    
    return o;
}


} // end namespace parsing

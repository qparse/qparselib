//
//  RunFactory.tpp
//  qparse
//
//  Created by Florent Jacquemard on 26/03/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

namespace parsing{


//template<class K>
//void RunFactory::RunFactory(const K& k):
//_key(k)
//{ }


template<class K>
void RunFactory<K>::runs(const K& k,
                         StackInner<K>& si,
                         StackTerm<K>& st,
                         // std::stack<RunInner<K>*>& si,
                         // std::stack<std::pair<const K*, RunTerm*>>& st,
                         ParsingEnv& env)
{
    assert(si.empty());
    assert(st.empty());
    
    if (State::isWTA(k.state()))
        runsWTA(k, si, st, env);
    else if (State::isMeta(k.state()))
        runsBar(k, si, st, env);
    else
    {
        ERROR("RunFactory: unexpected state {}", k.state());
    }
}


template<class K>
void RunFactory<K>::runsWTA(const K& k,
                            StackInner<K>& si,
                            StackTerm<K>& st,
                            // std::stack<RunInner<K>*>& si,
                            // std::stack<std::pair<const K*, RunTerm*>>& st,
                            const ParsingEnv& env)
{
    assert(State::isWTA(k.state()));
    assert(env.wta);
    assert(env.wta->isRegistered(k.state()));

    // read transitions of the base WTA.
    for (Tlist<Transition>::const_iterator
         i = env.wta->cbegin(k.state());
         i != env.wta->cend(k.state()); i++)
    {
        const Transition& t = *i;
        assert(t.size() <= t.arity()); // may differ in case of multiplicity

        // let the key class extend each transition and add extension to stack
        if (t.terminal())
        {
            k.RunsTerminal(t, st, env);
        }
        else if (t.inner())
        {
            k.RunsInner(t, si, env);
        }
        else // should not happen
        {
            ERROR("RunFactory: unexpected WTA transition type {}", t);
        }
    }
}


template<class K>
void RunFactory<K>::runsBar(const K& k,
                            StackInner<K>& si,
                            StackTerm<K>& st,
                            // std::stack<RunInner<K>*>& si,
                            // std::stack<std::pair<const K*, RunTerm*>>& st,
                            ParsingEnv& env)
{
    //assert(k.complete());
    assert(State::isMeta(k.state()));
    // create final bar nullary transition. it is added to the env pool.
    const Transition& t0 = barTransition(k.state(), 0, env);
    // let the key class extend this transition and add extension to stack
    k.RunsTerminal(t0, st, env);
    // RunTerm* r0 = new RunTerm(t0, 0); // empty interval
    // st.push(std::make_pair((K*) NULL, r0));  // only for complete keys
    
    // create bar binary transition. it is added to the env pool.
    const Transition& t2 = barTransition(k.state(), 2, env);
    // let the key class extend this transition and add extension to stack
    k.RunsInner(t2, si, env);
}


// default
template<class K>
void RunFactory<K>::RunTermmk(const K& k,
                              const Transition& t,
                              label_t a,
                              const Weight& w,
                              size_t len,
                              StackTerm<K>& st)
{
    assert(k.complete()); // ONLY FOR COMPLETE KEY !
    assert(t.arity() == SymbLabel::arity(a));
    //const Weight& w = t.weight(); // weight(a, t, env);
    if (w.is_zero()) // skip.
    {
        WARN("RunFactory: weight zero for term. transition {}, skip", t);
        return;
    }
    // new terminal run
    std::shared_ptr<RunTerm> r = std::make_shared<RunTerm>(t, a, w, len);
    TRACE("NEW terminal run {} -> {} for transition {}", *r, k, t);
    st.emplace(k, r); // key k is complete
}


template<class K>
void RunFactory<K>::RunInnermk(const K& k,
                               const Transition& t,
                               StackInner<K>& si)
                               //std::stack<RunInner<K>*>& si)
{
    assert(t.inner());
    assert(t.arity() == SymbLabel::arity(t.label()));
    std::shared_ptr<RunInner<K>> r = std::make_shared<RunInner<K>>(t);
    TRACE("NEW empty inner run {} -> {} for transition {}", *r, k, t);
    assert(r->arity() == t.arity());
    assert(r->size() == 0);
    assert(r->label() == t.label());
    si.push(r);
}


template<class K>
const Transition& RunFactory<K>::barTransition(state_t state,
                                               size_t arity,
                                               ParsingEnv& env)
{
    assert(State::isMeta(state));
    assert((arity == 0) || (arity == 2));
    assert(env.pool);
    
    if (env.pool->isRegistered(state))
    {
        // we assume that there are exactly 1 nullary and 1 binary
        // transitions targeting _state in env. pool.
        for (Tlist<Transition>::const_iterator
             i = env.pool->cbegin(state);
             i != env.pool->cend(state); i++)
        {
            if (i->arity() == arity)
                return *i;
        }
        ERROR("KeyFactory:barTransition0: transition not found in pool, state {}",
              state);
        throw std::runtime_error("barTransition0 not found\n");
    }
    else
    {
        // we construct and add both bar transition of arity 0 and 2 at once
        const Weight tone = env.pool->weight_one();
        Tlist<Transition>& tl = env.pool->add(state, (state == -1));

        // construct and add transition of arity 0
        tl.emplace(SymbLabel::make_bar(0), tone);
        assert(! tl.empty());
        Transition& t0 = tl.last();
        TRACE("KeyFactory: add BAR0 transition to pool: {} -> {}", state, t0);

        // construct and add transition of arity 2
        tl.emplace(SymbLabel::make_bar(2), tone);
        assert(! tl.empty());
        Transition& t2 = tl.last();
        assert(env.wta);
        t2.push(env.wta->initial());
        t2.push(state - 1); /// @todo  macro next Bar state
        TRACE("KeyFactory: add BAR2 transition to pool: {} -> {}", state, t2);

        if (arity == 0)
        {
            return t0;
        }
        else
        {
            assert(arity == 2);
            return t2;
        }
    }
}


/// @todo delete, replaced by barTransition
template<class K>
const Transition& RunFactory<K>::barTransition0(state_t state, ParsingEnv& env)
{
    assert(State::isMeta(state));
    assert(env.pool);
    
    if (env.pool->isRegistered(state))
    {
        // we assume that there are exactly 1 nullary and 1 binary
        // transitions targeting _state in env. pool.
        for (Tlist<Transition>::const_iterator
             i = env.pool->cbegin(state);
             i != env.pool->cend(state); i++)
        {
            if (i->arity() == 0)
                return *i;
        }
        ERROR("KeyFactory:barTransition0: transition not found in pool, state {}",
              state);
        throw std::runtime_error("barTransition0 not found\n");
    }
    else
    {
        const Weight tone = env.pool->weight_one();
        Tlist<Transition>& tl = env.pool->add(state, (state == -1));
        tl.emplace(SymbLabel::make_bar(0), tone);
        assert(! tl.empty());
        Transition& t = tl.last();
        TRACE("KeyFactory: add BAR0 transition to pool: {} -> {}", state, t);
        return t;
    }
}


/// @todo delete, replaced by barTransition
template<class K>
const Transition& RunFactory<K>::barTransition2(state_t state, ParsingEnv& env)
{
    assert(State::isMeta(state));
    assert(env.pool);
    
    if (env.pool->isRegistered(state))
    {
        // we assume that there are exactly 1 nullary and 1 binary
        // transitions targeting _state in env. pool.
        for (Tlist<Transition>::const_iterator
             i = env.pool->cbegin(state);
             i != env.pool->cend(state); i++)
        {
            if (i->arity() == 2)
                return *i;
        }
        ERROR("KeyFactory:barTransition2: transition not found in pool, state {}",
              state);
        throw std::runtime_error("barTransition2 not found\n");
    }
    else
    {
        const Weight tone = env.pool->weight_one();
        Tlist<Transition>& tl = env.pool->add(state, (state == -1));
        tl.emplace(SymbLabel::make_bar(2), tone);
        assert(! tl.empty());
        Transition& t = tl.last();
        assert(env.wta);
        t.push(env.wta->initial());
        t.push(state - 1); /// @todo  macro next Bar state
        TRACE("KeyFactory: add BAR2 transition to pool: {} -> {}", state, t);
        return t;
    }
}


//template<class K>
//std::shared_ptr<RunTerm> RunFactory<K>::failRun(const ParsingEnv& env)
//{
//    assert(env.wta);
//    const Transition& t0 = env.dummyTransition_zero();
//    assert(t0.weight().is_zero());
//    size_t length = 0; // number of input symbols parsed
//    label_t a = SymbLabel::make_fail(length);
//    std::shared_ptr<RunTerm> r =
//        std::make_shared<RunTerm>(t0, a, t0.weight(), length);
//    return r;
//}


} // end namespace parsing

//
//  TableA.tpp
//  qparse
//
//  Created by Florent Jacquemard on 07/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

//#include "RunCompare.hpp"


namespace parsing{


template <class K>
TableA<K>::TableA(ParsingEnv& env, const RunOrder& comp):
_env(env),
_comparer(comp)
{ };


template <class K>
TableA<K>::~TableA()
{
    // assert(_env);    
}


} // end namespace parsing

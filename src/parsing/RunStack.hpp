//
//  RunStack.hpp
//  squant2
//
//  Created by Florent Jacquemard on 07/07/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup parsing
/// @{


#ifndef RunStack_hpp
#define RunStack_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <assert.h>
#include <stack>
//#include <queue>

//#include "RunTerm.hpp"


namespace parsing{

class RunTerm;

/// pair made of a complete terminal run and its target,
/// a complete key of type K.
template<class K>
class RunTermTargeted
{
public:
    
    /// @param k target of run
    /// @param r pointer to run, must be non-null.
    RunTermTargeted(const K& k, std::shared_ptr<RunTerm> r);

    inline const K& target() const { return _target; }
    
    inline std::shared_ptr<RunTerm> run() const { return _run; }

private:

    const K _target;
    std::shared_ptr<RunTerm> _run;

};

/// type of temporary stack of terminal runs with key of type K
template<class K> using StackTerm =
    std::stack<RunTermTargeted<K>>;
//std::stack<std::pair<const K&, std::shared_ptr<RunTerm>>>;

/// type of temporary stack of inner runs with key of type K
template<class K> using StackInner = std::stack<std::shared_ptr<RunInner<K>>>;



} // end namespace parsing

// separated definition of template class
#include "RunStack.tpp"

#endif /* RunStack_hpp */

/// @}

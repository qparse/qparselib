//
//  Tempo.cpp
//  squant2
//
//  Created by Florent Jacquemard on 09/08/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#include "Tempo.hpp"


namespace tempo {


Tempo::Tempo(rtu_t spb):
_val(spb)
{
    assert(spb > 0);
}


Tempo::Tempo(int bpm, const ScoreModel::MeterSig& ts)
{
    assert(bpm > 0);
    assert(bpm < 1024);
    rtu_t bps = (rtu_t) bpm;
    // beat per second
    bps /= 60;
    // bar per second
    bps /= ts.beats();
    // second per bar
    assert(bps > 0);
    _val = 1/bps;
}


Tempo::Tempo(const Tempo& t):
_val(t._val)
{ }


Tempo::~Tempo()
{ }


Tempo& Tempo::operator=(const Tempo& rhs)
{
    if(this != &rhs)
    {
        _val = rhs._val;
    };
    return *this;
}


bool Tempo::operator==(const Tempo& rhs) const
{
    return (this->_val == rhs._val);
}


rtu_t Tempo::spb() const
{
    assert(_val > 0);
    return _val;
}


mtu_t Tempo::bps() const
{
    // beats per second
    rtu_t rbps = 1/_val;
    return toRational(rbps);
}
  

int Tempo::bpm(int bpb) const
{
    assert(bpb > 1);
    assert(_val > 0);

    // bar per second
    rtu_t bps = 1/_val;
    // beat per second
    bps *= bpb;
    // beat per minute
    return (int) (bps * 60);
}


int Tempo::bpm(const ScoreModel::MeterSig& ts) const
{
    return bpm(ts.beats());
}


int Tempo::barpm() const
{
    assert(_val > 0);

    // bar per second
    rtu_t bps = 1/_val;
    // bar per minute
    return (int) (bps * 60);
}


void Tempo::print(std::ostream& o) const
{
    assert(_val > 0);
    o << _val << " s/bar";
    o << "(" << barpm() << " bar/m)";
}


std::ostream& operator<<(std::ostream& o, const Tempo& t)
{
    t.print(o);
    return o;
}


} // end namespace tempo

//
//  Tempo.hpp
//  squant2
//
//  Created by Florent Jacquemard on 09/08/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @defgroup tempo Tempo module
///
/// @addtogroup tempo
/// @{


#ifndef Tempo_hpp
#define Tempo_hpp

#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <iostream>

#include "RTU.hpp"
#include "MTU.hpp"
#include "MeterSig.hpp"


namespace tempo {


/// @brief base class for tempo inference
/// one fixed tempo value and convertions
class Tempo
{
public:

    /// constant tempo value given inn second per bar
    /// @param spb number of second per bar
    Tempo(rtu_t spb);

    /// constant tempo value given in bpm
    /// @param bpm number of beats per minute
    Tempo(int bpm, const ScoreModel::MeterSig& ts);

    /// copy
    Tempo(const Tempo& t);
    
    ~Tempo();
       
    Tempo& operator=(const Tempo& rhs);

    bool operator==(const Tempo& rhs) const;
   
    /// number of second per bar
    rtu_t spb() const;

    /// number of bar per second
    mtu_t bps() const;
      
    /// rounded number of beats per minute
    /// @param bpb number of beat per bar
    int bpm(int bpb) const;

    /// rounded number of beats per minute
    /// @param ts a time signature
    int bpm(const ScoreModel::MeterSig& ts) const;
        
    void print(std::ostream& o) const;

protected:

    rtu_t _val;
    
private:

    /// reounded bar per minute
    int barpm() const;
   
};


std::ostream& operator<<(std::ostream&, const Tempo&);


} // end namespace tempo

#endif /* Tempo_hpp */

/// @}

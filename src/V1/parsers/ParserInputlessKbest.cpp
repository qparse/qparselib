//
//  ParserInputlessKbest.cpp
//  qparse
//
//  Created by Florent Jacquemard on 09/03/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//

#include "ParserInputlessKbest.hpp"


ParserInputlessKbest::ParserInputlessKbest(WCFG* a, bool ordering):
Parser<SKpointer>(a),
table(new Table<SKpointer,
                Krecord<SKpointer>,
                SKpointerHasher>(this,
                          ordering?weightMin<SKpointer>:weightMax<SKpointer>))
{ }


ParserInputlessKbest::~ParserInputlessKbest()
{
    assert(table);
    delete(table);
}


size_t ParserInputlessKbest::addRuns(Atable<SKpointer>* table,
                                     const SKpointer& p,
                                     Record<SKpointer>* it)
{
    return addWCFGRuns(table, p, it);
}


RhythmTree* ParserInputlessKbest::best(size_t k)
{
    assert(wcfg);
    assert(table);
    return(table->bestTree(SKpointer(wcfg->initial(), k)));
}


Weight ParserInputlessKbest::bestWeight(size_t k)
{
    assert(wcfg);
    assert(table);
    Run<SKpointer>* r = table->best(SKpointer(wcfg->initial(), k));
    
    if (r)
    {
        return r->weight;
    }
    else
    {
        return Weight(); // unknown weight
    }
}


void ParserInputlessKbest::printBest(std::ostream& o, size_t k) const
{
    assert(wcfg);
    assert(table);
    assert(k > 0);
    
    SKpointer p(wcfg->initial(), k);
    printobest(o, table, p);
}


size_t ParserInputlessKbest::demo(const std::string _schema_file, size_t _k)
{
    assert(wcfg);
    assert(table);
    INFO("compute {} best trees for {}", _k, _schema_file);
    
    clock_t time_start = clock();
    
    for (size_t i = 1; i <= _k; i++)
    {
        //parser->print_best(std::cout, i);
        
        std::cout << i << "-best: ";
        this->printBest(std::cout, i);
        std::cout << std::endl;
    }
    
    INFO("time to compute {} bests : {}ms", _k, util::duration(time_start));
    return 0;
}



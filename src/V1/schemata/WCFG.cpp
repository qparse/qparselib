//
//  WCFG.cpp
//  qparse
//
//  Created by Florent Jacquemard on 01/06/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include "WCFG.hpp"


WCFG::WCFG():
WFSM<Production>()
{ }


WCFG::WCFG(Weight seed, pre_t pre, pre_t post):
WFSM<Production>(seed),
_max_pre(pre),
_max_post(post)
{
    assert (! seed.unknown());
}


// TBR
void WCFG::abstract(bool flag)
{
    for (typename std::map<state_t,Tlist<Production>>::iterator i = _table.begin();
         i != _table.end(); ++i)
    {
        // state_t s = i->state;
        Tlist<Production>& tl = i->second;
        
        if (tl.empty())
        {
            break;
        }
        
        Weight w;
        state_t label = 0;
        // label_t label = 0; // for labelled Transitions
        double sum = 0;  // @todo USE SUM OP OF SEMIRING
        bool visited = false;
        
        // compute sum of weights of transitions to leaf values >= MAX_GRACE
        // remove transitions to leaf values > MAX_GRACE
        // tmax points to the transition of leaf value == MAX_GRACE
        Tlist<Production>::iterator j = tl.begin();
        while (j != tl.end())
        {
            if (j->terminal() &&
                (Label::nbEvents((*j).label()) >= CST_MAX_GRACE))
            {
                if (flag)
                {
                    sum += j->weight().norm();
                }
                else
                {
                    j->invert();
                    sum += j->weight().norm();
                }
                
                if ((Label::nbEvents(j->label()) == CST_MAX_GRACE) &&
                    (! visited))
                {
                    w = j->weight(); // copy
                    label = j->label();
                    visited = true;
                }
                
                tl.remove(j);
            }
            else
            {
                ++j;
            }
        }
        
        if (visited)
        {
            Production newt(w.make(sum));
            // Production newt(j->label(), w.make(sum)); // for labelled Transitions
            if (! flag) { newt.invert(); }
            tl.add(newt);
            _cpt_tr++;
            _cpt_size += (newt.arity() + 1);
            
        }
    }
}


Weight WCFG::eval(const RhythmTree& t) const
{
    return eval(t, _initial);
}


// TBC factor ALPHA ?
Weight WCFG::eval(const RhythmTree& t, state_t s) const
{
    size_t tar = t.arity();
    // convention: empty sum is zero
    Weight res = weight_zero();
    
    //TRACE("WFSM<T>::eval {}", t);
    
    // enumeration of transitions of target s
    for (Tlist<Production>::const_iterator i = cbegin(s); i != cend(s); ++i)
    {
        Weight  w = i->weight();
        
        if (i->arity() == tar)
        {
            // TBC treatment of grace-note (arb. number)
            if (i->terminal() && Label::abstract(i->label(), t.label()))
                //if ((*i).terminal() && (i->label() == t.label()))
            {
                res += w;
            }
            else if (i->inner())
            {
                for (size_t j = 0; j < tar; j++)
                {
                    w *= eval(*(t.child(j)), i->state(j));
                }
                res += w;
            }
        }
    }
    
    return res;
}


std::ostream& operator<<(std::ostream& o, const WCFG& a)
{
    for (typename std::map<state_t,Tlist<Production>>::const_iterator
         i = a._table.begin();
         i != a._table.end(); ++i)
    {
        state_t s = i->first;
        Tlist<Production> tl = i->second;
        for(typename Tlist<Production>::const_iterator j = tl.cbegin(); j != tl.cend(); j++)
        {
            const Production& t = *j;
            o << s << " := " << t << " \n";
        }
    }
    return o;
}


void WCFG::print(std::ostream& o) const
{
    o << '\n';
    o << this->countStates() << " states\n";
    o << this->countTransitions() << " production rules\n";
    o << this->countAll() << " total symbols\n\n";
    //o << *this;
}


void DepthMarking::mark_rec(state_t s, int d, const WCFG& wcfg)
{
    assert (d >= 0);
    mark(s, d);
    
    // enumerate the transitions pointing to state s
    for (Tlist<Production>::const_iterator i = wcfg.cbegin(s);
         i != wcfg.cend(s);
         i++)
    {
        // enumerate the states in the body of the current transition
        for (Production::Body::const_iterator j = i->cbegin();
             j != i->cend();
             j++)
        {
            // recursive marking
            mark_rec(j->state, d+1, wcfg);
        }
    }
}


DepthMarking::DepthMarking(const WCFG& wcfg)
{
    mark_rec(wcfg.initial(), 0, wcfg);
}


int DepthMarking::depth(state_t s) const
{
    std::map<state_t, int>::const_iterator it = _marks.find(s);
    
    if (it != _marks.end()) // found
    {
        assert (it->second >= 0);
        return it->second;
    }
    else
    {
        return -1;
    }
}


bool DepthMarking::multiple(state_t s) const
{
    std::map<state_t, bool>::const_iterator it = _multi.find(s);
    
    if (it != _multi.end()) // found
    {
        return it->second;
    }
    else
    {
        return false;
    }
}


int DepthMarking::mark(state_t s, int d)
{
    assert (d >= 0);
    std::pair<std::map<state_t,int>::iterator, bool> ret;
    
    // if s not marked, just add its mark
    // if s already marked, ret.first contains a iterator to mark
    ret = _marks.insert(std::pair<state_t,int>(s, d));
    
    // s already marked (found)
    if (ret.second == false)
    {
        // stored depth
        int ds = ret.first->second;
        
        assert (ds >= 0);
        if (ds == d)
        {
            // do nothing, keep depth value
            return d;
        }
        else
        {
            int max = std::max(d, ds);
            
            _multi[s] = true;
            if (ds > d)
            {
                // insert the max value in place of the old value
                _marks.insert(ret.first, std::pair<state_t,int>(s, max));
                return ds;
            }
            else
            {
                // keep the same value stored
                return d;
            }
        }
    }
    // s had not been marked before
    else
    {
        return d;
    }
}


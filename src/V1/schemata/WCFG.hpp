//
//  WCFG.hpp
//  qparse
//
//  Created by Florent Jacquemard on 01/06/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#ifndef WCFG_hpp
#define WCFG_hpp

#include <stdio.h>

#include "config1.hpp"
#include "trace.hpp"
#include "State.hpp"
#include "Tlist.hpp"
#include "Production.hpp"
#include "WFSM.hpp"
#include "RT.hpp"


/// class of weighted context-free grammars (schema)
///
/// state (and non-terminals): int
///
/// production rule table = map
/// state -> (production rule = state list), weight
///   state: head state
///   state list: see Production.hpp
///               body states if length > 1
///               label if length = 1
///
/// in other terms production rules have one of the forms
/// > s -> (s1,...,sk) w where k > 1, s, s1, ..., sk are states and w weight
/// > s -> (s1) w where s1 is an leaf label = int encoding
///
/// leaf label (terminals):
/// number of note + grace notes at (left of) current node
/// > 0 = continuation
/// > 1 = 1 note | rest      (au + 1 note)
/// > 2 = 1 grace notes + 1 note
/// > 3 = 2 grace notes + 1 note
/// >etc
/// @see Label for the functions managing these labels
//
/// transition table:
/// > head state -> vector of (state vector, weight)
class WCFG : public WFSM<class Production>
{
    
public:
    ///nullary constructor for cython
    WCFG();
    
    /// empty grammar
    WCFG(Weight seed, pre_t pre=0, pre_t post=0);
    
    virtual bool hasType(std::string code) const
    { return (code == "WCFG"); }
    
    inline pre_t max_pre() const { return _max_pre; }
    
    inline pre_t max_post() const { return _max_post; }
        
    /// abstract the leaf label values
    /// in domain [0..MAX_GRACE]
    /// every value > MAX_GRACE is casted to MAX_GRACE
    /// the weights are summed accordingly
    ///
    /// leaf labels in domain of Label (not PreLabel).
    /// @todo TBR unused
    void abstract(bool flag = false);
    
    /// evaluate the weight of the tree t for WFSM in initial state
    virtual Weight eval(const RhythmTree& t) const;
    
    // evaluate the weight of the tree t for WFSM in given state
    virtual Weight eval(const RhythmTree& t, state_t s) const;

    /// print sizes to output stream
    virtual void print(std::ostream&) const;

    /// write table content to output stream
    friend std::ostream& operator<<(std::ostream&, const WCFG&);
    
    
protected:
    
    /// used only in descendant classes
    pre_t _max_pre;
    pre_t _max_post;
  
};






/// marking of states of a CFG
/// with informations on the depth of their occurences
/// initialized with a CFG, can be interrogated afterwards
class DepthMarking
{
public:
    DepthMarking(const WCFG&);
    
    //    ~DepthMarking();
    
    /// return depth mark if given state marked
    /// return -1 otherwise
    int depth(state_t) const;
    
    /// return true if the given state can occur at multiple depths
    /// return false otherwise or if state not marked
    bool multiple(state_t) const;
    
    /// mark state using given depth and return new mark value
    /// can be the given depth or a greater depth with which the state
    /// had been already marked.
    int mark(state_t, int);
    
private:
    /// marks:
    /// -1 : unknown
    /// 0.. : depth
    std::map<state_t, int> _marks;
    
    /// whether the state can occur at multiuple depths
    std::map<state_t, bool> _multi;
    
    void mark_rec(state_t, int, const WCFG&);
};



#endif /* WCFG_hpp */

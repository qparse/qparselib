//
//  WCFGFileIn.cpp
//  qparse
//
//  Created by Florent Jacquemard on 08/03/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//
// @todo revoir _read... et read... pour eviter plusieurs ouvertures
// @todo revoir parcours du fichier


#include "WCFGFileIn.hpp"


WCFGFileIn::~WCFGFileIn()
{ }


WCFGFileIn::WCFGFileIn():WCFG()
{}


WCFGFileIn::WCFGFileIn(const std::string filename, WeightDom wt, pre_t pre, pre_t post):
WCFG(SchemaFileIn::seed(SchemaFileIn::choose(SchemaFileIn::read_weight_type(filename), wt)), pre, post)
{
    TRACE("load schema from {}", filename);
    
    // seed was read. use zero as new seed.
    if (_load(this->weight_zero(), filename) > 0)
    {
        ERROR("error reading grammar from {}, abort import", filename);
        return;
    }
    
    TRACE("force type {}", wt);
    
    CST_MAX_GRACE = SchemaFileIn::read_max_grace(filename);
    if (CST_MAX_GRACE > 0)
    {
        INFO("found max_grace_note {} declaration in {}",
             CST_MAX_GRACE, filename);
    }
    else
    {
        INFO("found no max_grace_note declaration in {}", filename);
    }
    
    if (wt == WeightDom::UNDEF)
    {
        // penalty
        if (this->hasWeightType("TropicalWeight"))
        {
            CST_WEIGHT_TYPE = WeightDom::PENALTY;
        }
        // counting to stochastic
        else if (this->hasWeightType("FloatWeight"))
        {
            INFO("Casting WCFG weights from counting into stochastic type.");
            this->CountingtoStochastic();
            CST_WEIGHT_TYPE = WeightDom::STOCHASTIC;
        }
        // stochastic
        else if (this->hasWeightType("ViterbiWeight"))
        {
            CST_WEIGHT_TYPE = WeightDom::STOCHASTIC;
        }
    }
    else if (wt == WeightDom::STOCHASTIC)
    {
        // penalty to stochastic
        if (this->hasWeightType("TropicalWeight"))
        {
            INFO("Casting WCFG weights from penalty into stochastic type, via counting model.");
            WARN("The outcome of this WCFG weight casting is not guaranteed.");
            INFO("Casting WCFG weights from penalty into counting type.");
            this->PenaltytoCounting();
            INFO("Casting WCFG weights from counting into stochastic type.");
            this->CountingtoStochastic();
        }
        // counting to stochastic
        else if (this->hasWeightType("FloatWeight"))
        {
            INFO("Casting WCFG weights from counting into stochastic type.");
            this->CountingtoStochastic();
        }
        // stochastic to stochastic
        else if (this->hasWeightType("ViterbiWeight"))
        {
            TRACE("No WCFG weight casting");
        }
        CST_WEIGHT_TYPE = WeightDom::STOCHASTIC;
    }
    else if (wt == WeightDom::PENALTY)
    {
        // penalty to penalty
        if (this->hasWeightType("TropicalWeight"))
        {
            TRACE("No WCFG weight casting");
        }
        // counting to penalty
        else if (this->hasWeightType("FloatWeight"))
        {
            INFO("Casting WCFG weights from counting into penalty type.");
            this->CountingtoPenalty();
        }
        // stochastic to penalty
        else if (this->hasWeightType("ViterbiWeight"))
        {
            INFO("Casting WCFG weights from stochastic into penalty type.");
            this->StochastictoPenalty();
        }
        CST_WEIGHT_TYPE = WeightDom::PENALTY;
    }
    else if (wt == WeightDom::COUNTING)
    {
        assert(this->hasWeightType("FloatWeight"));
        CST_WEIGHT_TYPE = WeightDom::COUNTING;
    }
}


size_t WCFGFileIn::_load(Weight seed, std::string filename)
{
    assert(! seed.unknown());
    assert(! filename.empty());
    std::ifstream file;
    
    file.open(filename, std::ios_base::in);
    if(!file.is_open())
    {
        ERROR("cannot open grammar file {}", filename);
        return 1; // WCFG is empty
    }
    
    bool initial_transition = true;
    int ln = 0;
    
    //read stream line by line
    for(std::string line; getline(file, line); )
    {
        ln++;
        // skip empty line
        if (line.size() == 0) continue;
        
        std::istringstream in(line);   //make a stream from the line
        
        // discard leading white spaces
        //in >> std::ws;
        // skip white spaces
        in >> std::skipws;
        
        // process 1 line of the form "s (s0[:m0] ... sn[:mn]) w"
        // extract s
        state_t s;
        if (!(in >> s))
        {
            TRACE("read {}: line {} ignored", filename, ln);
            continue; // not a transition (comment etc): skip line
        }
        
        //TRACE("state {}", s);
        // extract '('
        std::string buf;
        in >> buf;
        if (buf != "(")
        {
            WARN("parse error (body={}), line {} ignored", buf, ln);
            continue; // parse error: skip line
        }
        
        // extract s0 ... sn into vector body
        getline(in, buf, ')');
        std::istringstream ins(buf);
        std::vector<state_t> body;
        state_t q;
        while (ins >> q)
            body.push_back(q);
        /// @todo parse multiplicity > 1 if needed

        double val;
        if (!(in >> val))
        {
            WARN("error parsing weight value, line {} ignored", ln);
            continue; // parse error: ignore line
        }
        
        // one transition parsed from current line
        // add this transition to the table
        // copy content of vector body to new transition
        Production t = Production(seed.make(val));
        for (state_t q : body)
        {
            t.push(q, 1);  // multiplicity = 1
        }
        assert(t.inner() || t.terminal());
        add(s, t, initial_transition);
        // initial state is the first target state read in the file
        if (initial_transition)
            initial_transition = false;
        TRACE("add transition {}={}", s, t);
    }
    assert (! initial_transition); // at least one transition was parsed
    file.close();
    // @todo TBR
    // initials = { _initial };
    
    return 0;
}


// revoir : ajouter weight type, max grace etc
size_t WCFGFileIn::save(std::string filename)
{
    std::ofstream file;
    
    file.open(filename, std::ios_base::out);
    if (!file.is_open())
    {
        ERROR("cannot open file {} for writing schema", filename);
        return 1;
    }
    
    file << *this;
    
    file.close();
    return 0;
}

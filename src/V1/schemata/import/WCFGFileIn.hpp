//
//  WCFGFileIn.hpp
//  qparse V1
//
//  Created by Florent Jacquemard on 08/03/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//
/// @addtogroup input
/// @{


#ifndef WCFGFileIn_hpp
#define WCFGFileIn_hpp

#include <stdio.h>
#include <algorithm>
#include <string>

#include "config1.hpp"
#include "trace.hpp"
#include "Weight.hpp"
#include "WeightDom.hpp"
// no compile options for WCFG weight : load all classes
#include "FloatWeight.hpp"
#include "TropicalWeight.hpp"
#include "ViterbiWeight.hpp"
#include "Label.hpp" // global vars
#include "WCFG.hpp"
#include "SchemaFileIn.hpp"


/// @brief wrapper for constructing WCFG with various flags for weight type.
///
/// @ingroup input
class WCFGFileIn : public WCFG
{
public:
    /// @brief default constructor for cython.
    WCFGFileIn();
    
    // see https://stackoverflow.com/questions/308276/can-i-call-a-constructor-from-another-constructor-do-constructor-chaining-in-c
    // for definition
    // WCFGFileIn(const char* filename,
    //         WeightDom wt = WeightDom::UNDEF,
    //         pre_t pre=0, pre_t post=0);
    
    
    /// @brief read weight type and schema from file.
    ///
    /// @param filename input text file specifying the schema
    /// @param wt weight type (forced)
    ///
    /// if another weight type is given as argument (forced weight type)
    /// - use it as weight type for reading schema if no weight type found in file
    /// - use it to cast schema (force type).
    /// the WCFG can be empty in case of error.
    WCFGFileIn(const std::string filename,
            WeightDom wt = WeightDom::UNDEF,
            pre_t pre=0, pre_t post=0);
    
    
    /// @brief read schema from file
    /// @param filename input text file specifying the schema
    /// @param count_flag flag to determine the type of weights
    /// @param penalty_flag flag to determine the type of weights
    /// @param stochastic_flag flag to determine the type of weights
    ///
    /// casts weights according to compile options if needed.
    ///
    /// @todo TBR
    WCFGFileIn(const std::string filename,
               bool count_flag=false,
               bool penalty_flag=true,
               bool stochastic_flag=false);
    
    /// @brief same as WCFG destructor.
    ~WCFGFileIn();
    
    /// @brief save to file.
    size_t save(std::string filename);
        
private:
    // weight type found in schema file
    //WeightDom _filewt;
    
    // weight type forced by options
    //WeightDom _givenwt;
    
    
    /// @brief read WCFG from file
    ///
    /// create transition of same type as given sample weight
    /// the initial state set is set to singleton { 0 }
    /// in case of parse error, the WCFG is empty.
    /// @return error code 0 if file could be loaded into WCFG.
    size_t _load(Weight seed, std::string filename);
    
};

/// @}

#endif /* WCFGFileIn_hpp */

//
//  Symbol.hpp
//  squant
//
//  Created by Florent Jacquemard on 27/05/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup schemata
/// @{


#ifndef Symbol_hpp
#define Symbol_hpp

#include <stdio.h>

#include "trace.hpp"

/// replacement class for Label
/// Symbols for labeling concrete transitions of SWATA
///
/// A symbol is made of
/// - an arity
/// - a kind (in a finite set)
/// - a optional value (int)
class Symbol
{
public:

    enum SymbolKind
    {
        /// inside-the-bar symbol,
        /// if arity = 0: terminal symbol.
        /// if arity > 0: regular division of inside-the-bar time interval.
        STD = 0,
        /// symbol for addition of grace notes on left (binary symbol).
        /// left child = grace notes (duration 0), right child = event.s.
        GRACE,
        /// symbol for division of multi-bar time interval (binary symbol).
        /// division into first bar and the rest.
        BAR,
        /// symbol for concurrency (binary symbol).
        /// time interval duplication = voice split.
        FORK
    };
   
    /// @param ar arity must be in 0..255
    /// @param k a kind must be compatible with arity
    /// @param info extra information
    Symbol(unsigned char ar, SymbolKind k, unsigned int info = UNKNOWN_EXTRA);

    Symbol(const Symbol&);

    /// copy symbol s and
    /// @param s symbol to coy, must be unknown and of kind inside-the-bar
    /// @param info optional value for the copy of s (not unknown)
    Symbol(const Symbol& s, unsigned int info);

    virtual Symbol& operator= (const Symbol&);
    
    bool operator==(const Symbol&) const;

   
    inline size_t arity() const  { return _arity; }

    inline bool terminal() const { return (this->arity() == 0); }
    
    inline bool inner() const    { return (this->arity() > 0); }
    
    inline bool binary() const   { return (this->arity() == 2); }

    
    inline SymbolKind kind() const { return _kind; }
    
    inline bool inside() const    { return (this->kind() == STD); }

    inline bool bar() const       { return (this->kind() == BAR); }
    
    inline bool grace() const     { return (this->kind() == GRACE); }
    
    inline bool fork() const      { return (this->kind() == FORK); }

    /// inside-the-bar symbol with uknown extra value
    bool unknown() const;

    
    /// @return the number of grace node encoded in given leaf label
    /// @warning this symbol must have arity 0
    /// @warning this symbol must be of kind inside-the-bar
    /// @warning this symbol must not be unknown
    size_t nbGraceNotes() const;
    
    /// @return the number of note + grace notes encoded in given leaf label
    /// @warning this symbol must have arity 0
    /// @warning this symbol must be of kind inside-the-bar
    /// @warning this symbol must not be unknown
    size_t nbEvents() const;
    
    /// @return whether the given leaf symbol is a continuation
    ///         (no event, no grace note)
    /// @warning this symbol must have arity 0
    /// @warning this symbol must be of kind inside-the-bar
    /// @warning this symbol must not be unknown
    bool continuation() const;

    void print(std::ostream& o) const;

private:

    size_t _arity;

    // deprecated (was Label.hpp)
    // unsigned long int _code;

    /// kind of symbol
    SymbolKind _kind;
    
    /// specific info
    /// for constant (arity 0) symbols of kind inside-the-bar, specific info
    /// is about the number of quantized input points in the current interval.
    ///  - -1 : unknown value in 0..255
    ///  - 0  : continuation (tie)
    ///  - 1  : single event
    ///  - 2..255 : grace notes followed by 1 event
    long int _extra;
    
    static const long int UNKNOWN_EXTRA =  -1;
    
};


#endif /* Symbol_hpp */

/// @}

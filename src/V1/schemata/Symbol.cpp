//
//  Symbol.cpp
//  squant
//
//  Created by Florent Jacquemard on 27/05/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup schemata
/// @{


#include <assert.h>

#include "Symbol.hpp"


Symbol::Symbol(unsigned char ar, SymbolKind k, unsigned int info):
_arity(ar),
_kind(k),
_extra(info)
{
    assert(0 <= ar);
    assert(ar <= 255);
}


Symbol::Symbol(const Symbol& s):
_arity(s._arity),
_kind(s._kind),
_extra(s._extra)
{
}


Symbol::Symbol(const Symbol& s, unsigned int info):
_arity(s._arity),
_kind(s._kind),
_extra(info)
{
    assert(s._kind == Symbol::STD);
    assert(s._extra == UNKNOWN_EXTRA);
    assert(info != UNKNOWN_EXTRA);
}


Symbol& Symbol::operator= (const Symbol& s)
{
    if(this != &s)
    {
        _arity = s._arity;
        _kind = s._kind;
        _extra = s._extra;
        
    };
    return *this;
}


bool Symbol::operator==(const Symbol& s) const
{
    return (_arity == s._arity) && (_kind == s._kind) && (_extra == s._extra);
}


bool Symbol::unknown() const
{
    return (this->inside() && (_extra == UNKNOWN_EXTRA));
}


size_t Symbol::nbGraceNotes() const
{
    assert(this->terminal());
    assert(this->inside());
    assert(! this->unknown());
    size_t e = _extra;

    if (e > 0)
        return e - 1;
    else
        return 0;
}


size_t Symbol::nbEvents() const
{
    assert(this->terminal());
    assert(this->inside());
    assert(! this->unknown());

    return _extra;
}


bool Symbol::continuation() const
{
    assert(this->terminal());
    assert(this->inside());
    assert(! this->unknown());

    return (_extra == 0);
}


void Symbol::print(std::ostream& o) const
{
    std::string info = (_extra == UNKNOWN_EXTRA)?"*":std::to_string(_extra);
    
    switch (_kind)
    {
        case Symbol::STD:
            if (_arity == 0)
            {
                if (_extra == UNKNOWN_EXTRA)
                    o << "E*" << info;
                else if (_extra == 0)
                    o << "C0";
                else if (_extra == 1)
                    o << "E1";
                else
                    o << "G" << (_extra - 1);
            }
            else if (_extra == UNKNOWN_EXTRA)
            {
                o << "D" << _arity;
            }
            else
            {
                o << "D" << _extra << ":" << _arity;
            }
            break;
            
        case Symbol::GRACE:
            assert (_arity == 2);
            assert (_extra == UNKNOWN_EXTRA);
            o << "G";
            break;
            
        case Symbol::BAR:
            assert (_extra == UNKNOWN_EXTRA);
            if (_arity == 0)
            {
                o << "B0";
            }
            else
            {
                assert (_arity == 2);
                o << "B";
            }
            break;
            
        case Symbol::FORK:
            assert (_extra == UNKNOWN_EXTRA);
            if (_arity == 0)
            {
                o << "F0";
            }
            else
            {
                assert (_arity == 2);
                o << "F";
            }
            break;
            
        default:
            assert (false); // should not happen
    }
    
}


/// @}

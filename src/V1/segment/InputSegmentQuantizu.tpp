//
//  InputSegmentQuantize.tpp
//  qparse
//
//  Created by Florent Jacquemard on 19/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

// these template methods cannot be defined in the cpp
// see e.g. https://stackoverflow.com/questions/115703/storing-c-template-function-definitions-in-a-cpp-file


//InputSegmentQuantize::InputSegmentQuantize(const InputSegment& s):
//InputSegment(s)
//{ }


//InputSegmentQuantize::InputSegmentQuantize(const InputSegmentQuantize& s):
//InputSegment(s)
//{ }


template<class P>
void InputSegmentQuantize::quantize(Atable<P>* table, const P& p)
{
    quantize_assert(table, p, 0);
}


/// @todo TBR
template <class P>
size_t InputSegmentQuantize::quantizu(Atable<P>* table, const P& p, size_t b)
{
    // TBC. dirty. quantize(table, p) is public, with b private
    assert(MTU::null(_mbegin));
    if (b == 0)
    {
        assert(this->size() > 0);
        // reset total musical length
        if (this->quantized())
        {
            WARN("reset musical length of input segment");
        }
        _mend = MTU::ZERO;
    }
    
    return quantize_assert(table, p, b);
}


template <class P>
size_t InputSegmentQuantize::quantize_assert(Atable<P>* table,
                                             const P& p,
                                             size_t b)
{
    assert(table);
    Run<P>* r = table->best(p);
    size_t j = b;
    
    if (r)
    {
        assert(! r->null());
        assert(r->complete());
        
        if (r->terminal())
        {
            label_t lab = r->label();
            assert(p.has_I());
            assert(p.interval());
            mtu_t onset = p.interval()->mbegin();
            mtu_t dur = p.interval()->mduration();
            assert(MTU::positive(dur));
            assert(j < this->size());
            
            // contiuation : accumulate in current note
            if (Label::continuation(lab))
            {
                // skip an initial continuation
                if (MTU::undefined(this->mdate(j)))
                {
                    assert(j == 0);
                    assert(MTU::undefined(this->mduration(j)));
                    //this->mdate(j) = onset;
                    //this->mduration(j) = dur;
                }
                // continuation added to previous event
                else
                {
                    assert(onset == this->mdate(j) + this->mduration(j));
                    this->mduration(j) += dur;
                    // TBC dirty. must be set only once for j.
                    _mend += dur;
                }
            }
            else // grace notes + note
            {
                // current event unset, process it
                if (MTU::undefined(this->mdate(j)))
                {
                    assert(MTU::undefined(this->mduration(j)));
                }
                // current event set, start processing the next one
                else
                {
                    assert(MTU::defined(this->mduration(j)));
                    j++;
                }
                
                // grace notes
                for (size_t i=0; i < Label::nbGraceNotes(lab); i++)
                {
                    if (j < this->size())
                    {
                        assert(MTU::undefined(this->mdate(j)));
                        assert(MTU::undefined(this->mduration(j)));
                        this->mdate(j) = onset;
                        this->mduration(j) = MTU::ZERO;
                    }
                    else
                    {
                        WARN("quantize: event {} (grace note) missing in input segment", j);
                    }
                    j++;
                }
                // note
                if (j < this->size())
                {
                    assert(MTU::undefined(this->mdate(j)));
                    assert(MTU::undefined(this->mduration(j)));
                    this->mdate(j) = onset;
                    this->mduration(j) = dur;
                    _mend += dur; // TBC dirty. must be set only once for j.
                }
                else
                {
                    WARN("quantize: event {} (note) missing in input segment", j);
                }
            }
        }
        else
        {
            assert(r->inner());
            size_t a = r->arity();
            assert(a > 0);
            
            for (size_t i = 0; i < a; i++)
            {
                j = quantize_assert(table, (*r)[i], j);
            }
        }
    }
    else
    {
        ERROR("cannot quantize input segment wrt {}: no run", p);
    }
    
    return j;
}




// used to manage heaps of MusPoint* in subclasses
//class InputSegmentMusPointcomp
//{
//public:
//    InputSegmentMusPointcomp(const bool& revparam=false)
//    {
//        reverse = revparam;
//    }
//
//    bool operator() (const MusPoint* lhs, const MusPoint* rhs) const
//    {
//        if (reverse)
//            return (MusPointcomp(rhs, lhs));
//        else
//            return (MusPointcomp(lhs, rhs));
//    }
//
//private:
//    bool reverse;
//
//    static bool MusPointcomp(const MusPoint* lhs, const MusPoint* rhs);
//};
//
//typedef
//std::priority_queue<MusPoint*, std::vector<MusPoint*>, InputSegmentMusPointcomp>
//MusPointHeap;


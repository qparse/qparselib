//
//  config1.cpp
//  squanty
//
//  Created by Florent Jacquemard on 13/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup general
/// @{


#include "config1.hpp"
#include "hash.hpp"
#include "Weight.hpp"
#include "Label.hpp"
#include "RunFilterMode.hpp"



pre_t PP_UNKNOWN = -1;
pre_t PP_NULL = 0;
bool PP_KNOWN(pre_t x) { return (x >= 0); }


/// default value
bool OPT_NOREST = false;



int read_config(const std::string  filename)
{
    assert(filename.size() > 0);
    
    INIReader reader(filename);
    
    if (reader.ParseError() < 0)
        return -1;
    
    parsing::OPT_RUN_DUR = reader.GetBoolean("options", "run_dur", true);
    parsing::OPT_RUN_UNIT = reader.GetBoolean("options", "run_unit", true);
    parsing::OPT_RUN_STRICT = reader.GetBoolean("options", "run_strict", false);
    if (parsing::OPT_RUN_DUR == false)
    {
        parsing::OPT_RUN_UNIT = false;
        parsing::OPT_RUN_STRICT = false;
    }
    parsing::OPT_RUN_STRICT = reader.GetBoolean("options", "norest", false);

//    CST_ALPHA = reader.GetReal("params", "alpha", 0.5);
    Weight::CST_ALPHA = reader.GetReal("params", "alpha", 1.0);
    Weight::CST_SIGMA2 = reader.GetReal("params", "sigma2", 0.5);
    Weight::COEF_OFFSET_DIST_LEFT = reader.GetReal("params", "coef_offset_dist_left", 1.0);
    Weight::COEF_OFFSET_DIST_RIGHT = reader.GetReal("params", "coef_offset_dist_right", 1.0);
    MAX_AR = reader.GetInteger("params", "maxarity", 11);
//  CST_PRECISION = reader.GetReal("params", "precision", 0.0000001);
    HASH_SEED = reader.GetInteger("hash", "seed", 1009);
    HASH_FACTOR = reader.GetInteger("hash", "factor", 9176);
    
    return reader.ParseError();
}


/// @}

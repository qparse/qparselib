//
//  CountingWCFG.hpp
//  schemas
//
//  Created by Florent Jacquemard on 28/11/2017.
//  Copyright © 2017 Florent Jacquemard. All rights reserved.
//
/// @addtogroup schemata
/// @{


#ifndef CountingWCFG_hpp
#define CountingWCFG_hpp

#include <stdio.h>
#include <assert.h>
#include <vector>
#include <map>
#include <set>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>


#include "trace.hpp"
#include "State.hpp"
#include "Label.hpp"
#include "Weight.hpp"
#include "CountingWeight.hpp"
#include "WCFG.hpp"
#include "RT.hpp"

/// transtition table ordered by transition's ids
typedef
std::set<std::pair<state_t, Production&>,
         bool(*)(std::pair<state_t, Production&>,
                 std::pair<state_t, Production&>)>
OTransitionTable;

/// copy of WCFG dedicated to corpus statistics.
///
/// for WCFG weight estimation and WCFG construction from corpus.
///
/// construction of WCFG with counting weights (unit vectors) from WCFG
/// and verbose tree evaluation with feedback.
///
/// @warning only for target SCHEMA
class CountingWCFG: public WCFG
{
public:
    ///default initializer for cython
    CountingWCFG();
    /// copy base WCFG
    /// reset weight values to counting weights (unit vectors)
    CountingWCFG(const WCFG& a);
    
    virtual bool hasType(std::string code) const
    { return (code == "CountingWCFG"); }

    /// special version of eval for CountingWeight
    /// with feedback in case of fail
    virtual Weight eval(const RhythmTree& t) const;

    /// it is important to enumerate in same order
    /// for printing and building unit weights!
    friend std::ostream& operator<<(std::ostream&, const CountingWCFG&);

protected:
    Weight evalCountingVerbose(const RhythmTree&, state_t, Position) const;

    /// the weight of this WCFG are replaced by "CountingWeight"
    /// unit vector of length dim (one unit per transition)
    void resetCounting(size_t dim);
    
    /// copy of transition table
    /// ordered according to the transition's ids (can be iterated).
    OTransitionTable _tableids;
    
    /// pointer to comparison functionå
    static bool(*_trcomp_ptr)(std::pair<state_t, Production&>,
                              std::pair<state_t, Production&>);
    
};

#endif /* CountingWCFG_hpp */


/// @}

//
//  equiv2.cpp
//  equiv1
//
//  Created by Florent Jacquemard on 15/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#include <stdio.h>
#include <time.h>
#include <iostream>
#include <string>
#include <vector>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>     // handling commandline options
#include <assert.h>

#include "config.hpp"   // check compile flags
#include "trace.hpp"
#include "Rational.hpp"
#include "util.hpp"

// input file
#include "MTU.hpp"
#include "ValueList.hpp"
#include "DuraList.hpp"

// switch options for WCFG weight
#include "Weight.hpp"
#include "WeightDom.hpp"
#include "FloatWeight.hpp"
#include "TropicalWeight.hpp"
#include "ViterbiWeight.hpp"

// automata
#include "Label.hpp"
#include "SymbLabel.hpp"
#include "State.hpp"
#include "Transition.hpp"
#include "WFSM.hpp"
#include "SWTA.hpp"
#include "SWTAFileIn.hpp"

// parsing
//#include "Key.hpp"
#include "KeyS.hpp"
// #include "KeySI.hpp"
// #include "KeySIP.hpp"
// #include "KeySIR.hpp"
#include "KeySV.hpp"
#include "Runey.hpp"
#include "RunTerm.hpp"
#include "RunInner.hpp"
#include "RunCompare.hpp"
//#include "Recordey.hpp"
#include "RecordOne.hpp"
#include "RecordQueue.hpp"
#include "TableA.hpp"
#include "TableParse.hpp"

// output
//#include "LRT.hpp"
#include "ScoringEnv.hpp"
#include "Score.hpp"
#include "MeterSig.hpp"
#include "Event.hpp"
#include "Measure.hpp"
#include "Note.hpp"
#include "PS0.hpp"
#include "TableMonoImporter.hpp"
#include "SMPrinter.hpp"
#include "SMMEI.hpp"
#include "LRTTableIn.hpp"

namespace ScoreModel {};
namespace sm = ScoreModel;


// ==========================================================================
// Option handling with getop_long (stdlib)

static int verbosity = 0;

/// number of K-best to compute
static size_t nbest = 1;

/// whether the best is the max (true) or the min (false) weight
static int f_max = true;

/// config file (aka ini) option is present
static bool f_config = false;
/// text file name for writing
static string _config_file;

/// input file option is present
static bool f_input = false;
static bool f_in_text = false;
/// input file is midi
static bool f_in_midi = false;
/// file name for reading quantized IOI list
static string _input_file;
// input segment
// static InputSegment* _iseg;

/// file name where reading schema
static string _schema_file;
/// schema file option is present
static bool f_schema = false;
/// input schema
static SWTAFileIn* _schema;
/// option: clean SWTA
static int f_clean = false;
// option: use the schema to do counting (grammar inference)
static int f_inference = false;

/// forced weight type for schema (given in command line options)
static int f_penalty = false;
static int f_count = false;
static int f_proba = false;
static WeightDom _weight_type = WeightDom::UNDEF;

// flag: clean WCFG
// static int f_clean = false;

// option: use the schema to do counting (grammar inference)
// static int f_inference = false;

/// output file option is present
static bool f_output = false;
/// output file is mei
static bool f_out_mei = false;
/// output file is txt
static bool f_out_txt = false;
/// output file name for writing
static string _output_file;

/// flag: time signature specified
static int f_timesig = false;
/// time signature
static sm::MeterSig _ts(4,4);
//static int _ts_num;
//static int _ts_den;


static struct option long_options[] =
{
    // Options with an abbreviation
    // we dont use flags here, we set them after calling getopt instead
    
    /*  0 */ { "help",        no_argument,       NULL, 'h' },
    /*  1 */ { "version",     no_argument,       NULL, 'V' },
    
    /*  2 */ { "verbosity",   required_argument, NULL, 'v' },
    /*  3 */ { "debug",       no_argument,       NULL, 'd' },
    /*  4 */ { "trace",       no_argument,       NULL, 't' },
    /*  5 */ { "quiet",       no_argument,       NULL, 'q' },

    /*  6 */ { "kbest",       required_argument, NULL, 'k' },
    
    /*  7 */ { "input",       required_argument, NULL, 'i' },
    /*  8 */ { "schema",      required_argument, NULL, 'a' },
    /*  9 */ { "output",      required_argument, NULL, 'o' },
    /* 10 */ { "config",      required_argument, NULL, 'c' },

    // Options with no abbreviation
    /* 11 */ { "penalty",     no_argument,       &f_penalty,   true},
    /* 12 */ { "counting",    no_argument,       &f_count,     true },
    /* 13 */ { "proba",       no_argument,       &f_proba,     true },
    /* 14 */ { "probability", no_argument,       &f_proba,     true },
    /* 15 */ { "stochastic",  no_argument,       &f_proba,     true },
    /* 16 */ { "inference",   no_argument,       &f_inference, true },
    /* 17 */ { "clean",       no_argument,       &f_clean,     true },
    /* 18 */ { "min",         no_argument,       &f_max,       false },
    /* 19 */ { "max",         no_argument,       &f_max,       true },
      
    // last
    { NULL, 0, NULL, 0 }
};



void usage()
{
    std::cout << "Usage: equiv2 [options...]";
    std::cout << std::endl;
    std::cout << "  -help -h";
    std::cout << std::endl;
    std::cout << "  -version -V";
    std::cout << std::endl;
    std::cout << "  -verbosity level -v level : level=0..6  (default is 0)";
    std::cout << std::endl;
    std::cout << "             levels: 0=off,  1=critical, 2=err, 3=warn";
    std::cout << std::endl;
    std::cout << "                     4=info, 5=debug,    6=trace";
    std::cout << std::endl;
    std::cout << "  -trace -t : same as -verbosity 6";
    std::cout << std::endl;
    std::cout << "  -debug -d : same as -verbosity 5";
    std::cout << std::endl;
    std::cout << "  -quiet -q : same as -verbosity 0";
    std::cout << std::endl;
    std::cout << "  --kbest N -k N : compute the N best trees";
    std::cout << std::endl;
    std::cout << "  -max : the best is the max weight";
    std::cout << std::endl;
    std::cout << "  -min : the best is the min weight";
    std::cout << std::endl;
    std::cout << "  -input filename -i filename : filename is a text file";
    std::cout << std::endl;
    std::cout << "           with one fraction (IOI) per line";
    std::cout << std::endl;
    std::cout << "  -midi midifilename -m midifilename : filename is a midi file";
    std::cout << std::endl;
    std::cout << "  -schema filename -a filename : filename contains \
    the text description of a SWTA";
    std::cout << std::endl;
    std::cout << "  -counting filename -g filename : filename contains \
    the text description of a SWTA for counting the best tree (grammatical inference)";
    std::cout << std::endl;
    std::cout << "  -output filename -o filename : text output in filename";
    std::cout << std::endl;
    std::cout << "  -config file.ini -c file.ini : configuration file";
    std::cout << std::endl;
    std::cout << "schema weight model (mutually exclusive options):";
    std::cout << std::endl;
    std::cout << "  -counting";
    std::cout << std::endl;
    std::cout << "  -penalty";
    std::cout << std::endl;
    std::cout << "  -probability -stochastic (equivalent)";
    std::cout << std::endl;
    std::cout << "  -noweight -nw : ignore weights in input file";
    std::cout << std::endl;
    std::cout << "-counting -penalty -probability -stochastic are mutualy exclusive";
    std::cout << std::endl;
    std::cout << "-trace has priority over -verbosity";
    std::cout << std::endl;
}


void version()
{
    INFO("equiv2: version 2.0");
}



// ==========================================================================
// Option handling with getop_long (stdlib)

#define check_index_is(NAME) \
assert(0 == strcmp(long_options[option_index].name, NAME))

static void setOptionArgs(int option_index)
{ }


/// @param c character of option
/// @return   0 : continue without error
///           1 : stop without error
///         > 1 : stop with error
static int getOption(int c, int option_index)
{
    // end of the options
    assert(c != -1);
        
    switch (c)
    {
            // long option with flag pointer
        case 0:
            setOptionArgs(option_index);
            break;
            
        case 'h':
            usage();
            return 1;
            
        case 'V':
            version();
            return 1;
            
        case 'v':
            if (optarg)
                verbosity = atoi(optarg);
            else
                verbosity = 4;
            TRACE("Option {}: {}", long_options[option_index].name,
                  verbosity);
            break;

        case 'd':
            verbosity = 5;
            TRACE("option {}: verbosity={}",
                  long_options[option_index].name,
                  verbosity);
            break;
            
        case 't':
            verbosity = 6;
            TRACE("Option {}: verbosity={}",
                  long_options[option_index].name,
                  verbosity);
            break;
            
        case 'q':
            verbosity = 0;
            TRACE("Option {}: verbosity={}",
                  long_options[option_index].name,
                  verbosity);
            break;

        case 'k':
            nbest = atoi(optarg);
            TRACE("Option {} : {}",
                  long_options[option_index].name, nbest);
            if (nbest == 0)
            {
                ERROR("option error: k={} must be > 0", nbest);
                return 2;
            }
            break;
            
        case 'i':
            f_input = true;
            _input_file = std::string(optarg);
            TRACE("Option {}: {}",
                    long_options[option_index].name, _input_file);
            // distinguish between MIDI and text input
            if (_input_file.size() > 0)
            {
                std::string suffix = util::suffix(_input_file);
                if (suffix == "MID" || suffix == "MIDI")
                {
                    INFO("option input: MIDI file import");
                    f_in_midi = true;
                }
                else if (suffix == "TXT")
                {
                    INFO("option input: plaintext file import");
                    f_in_text = true;
                }
                else
                {
                    WARN("option -i input file {}, \
                          unknown suffix, assume text file",
                          _input_file);
                    f_in_text = true;
                }
            }
            else
            {
                ERROR("option -i error: no output file name");
                return 2;
            }

            break;
            
        case 'm':
            f_input = true;
            f_in_midi = true;
            _input_file = std::string(optarg);
            TRACE("option {}: {}",
                  long_options[option_index].name, _input_file);
            break;
            
        case 'a':
            f_schema = true;
            _schema_file = std::string(optarg);
            TRACE("Option {}: {}",
                  long_options[option_index].name,
                  _schema_file);
            break;

        case 'o':
            f_output = true;
            _output_file = std::string(optarg);
            TRACE("option {}: {}", long_options[option_index].name,
                  _output_file);
            
            if (_output_file.size() > 0) // empty string : no output
            {
                string suffix = util::suffix(_output_file);
                
                // export only if output file has midi suffix
                if (suffix == "TXT")
                {
                    INFO("option output: TXT file export");
                    f_out_txt = true;
                }
                else if (suffix == "MEI")
                {
                    INFO("option output: MEI file export");
                    f_out_mei = true;
                }
                else
                {
                    ERROR("option -o error: ouput file {}, \
                           suffix must be .mei or .txt",
                          _output_file);
                    return 2;
                }
            }
            else
            {
                ERROR("option -o error: no output file name");
                return 2;
            }
            break;

        case 'c':
            f_config = true;
            _config_file = std::string(optarg);
            TRACE("Option {}: {}", long_options[option_index].name,
                  _config_file);
            break;
            
            //  missing option argument. Error already signaled by getopt
        case ':':
            ERROR("Missing option argument");
            break;
            
            // unknown or ambiguous option  Error already signaled by getopt
        case '?':
            WARN("unknown or ambiguous option argument");
            break;
            
            // Do nothing for the options that are not flag-managed but without arguments
        default:
            WARN("Unknown option {}. use -h for help.", c);
            return 2;
    }
    
    return 0;
}


/// control the presence of mandatory option and incompatibilities.
/// @return if check of option passed.
bool checkOptions()
{
    bool error = false;
 
    if (f_penalty || f_count || f_proba)
    {
        if ((f_penalty && f_count) || ((f_penalty || f_count) == f_proba))
        {
            ERROR("options conflict: more than one schema file type");
            error = true;
        }
    }
    else
    {
        TRACE("options: no schema file type");
    }
    
    // input file
    if (f_in_text == f_in_midi)
    {
        TRACE("options: f_input={}, f_in_midi={}", f_input, f_in_midi);
        if (f_in_midi)
        {
            ERROR("options conflict: 2 input files");
            error = true;
        }
        else
        {
            TRACE("options: no input file");
        }
    }

    
    // set forced weight type value
    if (f_penalty)
    {
        _weight_type = WeightDom::PENALTY;
    }
    else if (f_count)
    {
        _weight_type = WeightDom::COUNTING;
    }
    else if (f_proba)
    {
        _weight_type = WeightDom::STOCHASTIC;
    }
    else
    {
        _weight_type = WeightDom::UNDEF;
    }
    
    if (! f_schema)
    {
        ERROR("missing option: no schema file");
        error = true;
    }
    
    return error;
}

void setVerbosityLevel()
{
    switch (verbosity)
    {
        case 0: spd::set_level(spd::level::off); break;
        case 1: spd::set_level(spd::level::critical); break;
        case 2: spd::set_level(spd::level::err); break;
        case 3: spd::set_level(spd::level::warn); break;
        case 4: spd::set_level(spd::level::info); break;
        case 5: spd::set_level(spd::level::debug); break;
        case 6: spd::set_level(spd::level::trace); break;
        default:
        {
            WARN("verbosity level {} too high, set to 6 (trace).", verbosity);
            spd::set_level(spd::level::trace);
        }
    }
}


/// STUB for a parse fail at root (all bars)
size_t parse_fail(const std::string filename)
{
    std::ofstream file;
    
    file.open(filename, std::ios_base::out);
    if(!file.is_open())
    {
        ERROR("LRT export : cannot open {} for writing", filename);
        return 1;
    }

    file << "1. FAIL";
    
    file.close();
    return 0;
}


/// @param tab a parse table.
/// @param init initial key to start parsing.
/// @param n number of best trees to compute.
template<class K>
void printTrees(parsing::TableA<K>& tab,
                const K& init,
                size_t n)
{
    for (size_t i = 1; i <= nbest; i++)
    {
        const parsing::Run* best_run = tab.best(init, i);

        if (best_run == NULL) // should no more happen (changed to FAIL)
        {
            //assert(best_tree == NULL);
            INFO("{}-best: NULL", i);
        }
        else if (nbest == 1)
        {
            //LabeledRhythmTree* best_tree = tab->bestTree(best_run);
            LabeledRhythmTree* best_tree =
                LRTFactory<K>::projection(tab, best_run, 1);
            assert(best_tree);
            INFO("{}-best = {} weight = {}", i,
                 best_tree->to_string(), best_run->weight());
            delete best_tree;
        }
        else
        {
            const parsing::RunRanked<parsing::KeySV>* best_runr =
                dynamic_cast<const parsing::RunRanked<parsing::KeySV>*>(best_run);
            assert(best_runr);
            //LabeledRhythmTree* best_tree = tab->bestTree(best_runr);
            LabeledRhythmTree* best_tree =
                LRTFactory<K>::projection(tab, best_runr);
            assert(best_tree);
            INFO("{}-best = {} weight = {}", i,
                 best_tree->to_string(), best_run->weight());
            delete best_tree;
        }
    }

}



int main(int argc, char** argv)
{
    // set for tracing the parsing of options
    //
    // log levels:
    // trace = 0,
    // debug = 1,
    // info = 2,
    // warn = 3,
    // err = 4,
    // critical = 5,
    // off = 6
    spd::set_level(spd::level::trace);
    
    
    // character of option
    int c;
    
    while(1)
    {
        /* getopt_long stores the option index here. */
        int option_index = 0;
        c = getopt_long_only(argc, argv, "hVv:dtk:i:a:o:",
                             long_options, &option_index);
        
        // end of the options
        if (c == -1)
            break;

        int ret = getOption(c, option_index);
        if (ret == 1)
            return 0; // exit without error
        else if (ret > 1)
        {
            ERROR("error in options (code {}), exit", ret);
            return ret;
        }
    }
    
    if (checkOptions())
    {
        ERROR("option error. exit");
        std::cerr << "option error. exit";
        return 1;
    }
    
    INFO("verbosity level = {}", verbosity);
    setVerbosityLevel();
    
    if (f_input) { INFO("input file: {}", _input_file); }
    INFO("schema file: {} ({} weight model)",
         _schema_file,
         (f_count?"Counting":
                  (f_proba?"Stochastic":(f_penalty?"Penalty":"???"))));
    if (f_output) { INFO("output file: {}", _output_file); }
    if (f_config) { INFO("config file: {}", _config_file); }
    
    // initialize running environment from INI file
    if (f_config)
    {
        INFO("loading config. parameters from ini file {}", _config_file);
        int res = read_config(_config_file);
        if (res == 0)
        {
            INFO("reading config from {} OK", _config_file);
        }
        else if (res == -1)
        {
            ERROR("error opening config file {}", _config_file);
            return 1;
        }
        else
        {
            ERROR("parse error in config file {} line {}", _config_file, res);
            return 2;
        }
    }
   
    // input schema (global)
    // read from file
    INFO("Reading schema from {}", _schema_file);
    _schema = new SWTAFileIn(_schema_file, _weight_type);
    
    if (_schema == NULL || _schema->empty())
    {
        ERROR("error importing schema from {}, abort", _schema_file);
        return 2;
    }
    assert(_schema);
    
    if (_schema->seed().unknown())
    {
        ERROR("undef weight type in schema imported from {}, abort", _schema_file);
        return 2;
    }

    if (_schema->empty())
    {
        ERROR("empty schema imported from {}, abort", _schema_file);
        return 2;
    }

    if (f_timesig)
    {
        INFO("Time Signature (from command line option) {}/{}",
             _ts.getCount(), _ts.getUnit());
    }
    else
    {
        if (SchemaFileIn::readTimesignature(_schema_file, _ts))
        {
            INFO("Time Signature (from file {}) {}/{}",
                 _schema_file, _ts.getCount(), _ts.getUnit());
        }
        else
        {
            INFO("could not find time signature in {} or options, default to 4/4",
                 _schema_file);
        }
    }
    
    if (CST_WEIGHT_TYPE == WeightDom::UNDEF)
    {
        ERROR("no weight type found for {}, abort", _schema_file);
        return(3);
    }
    
    switch(CST_WEIGHT_TYPE)
    {
        case WeightDom::UNDEF:
            ERROR("weight type undef after importing automaton. abort.");
            return(4); break;
        case WeightDom::PENALTY:
            INFO("weight model: penalty (alpha = {})", Weight::CST_ALPHA);
            break;
        case WeightDom::STOCHASTIC:
            INFO("weight type: stochastic (sigma2 = {})", Weight::CST_SIGMA2);
            break;
        case WeightDom::COUNTING:
            ERROR("weight type counting not supported for quantization. abort.");
            return(4);
    }
    
// Schema cleaning
//    if (f_clean)
//    {
//        INFO("Cleaning schema");
//        _schema->clean();
//    }

    if (! _schema->isClean())
    {
        WARN("schema {} is not clean.", _schema_file);
        std::set<state_t> empty = _schema->emptyStates();
        for (std::set<state_t>::iterator i = empty.begin(); i != empty.end(); i++)
        {
            WARN("empty state : {}", *i);
        }
    }
   
    // resolution
    //size_t res = compute_resolution(schema);
    
    // read rhythmic value (2 options)
    ValueList seg;

    // one schema, no rythmic value
    if (! f_input)
    {
        INFO("no input, enumeration of schema");
    }
    // one schema, one rythmic value
    else
    {
        INFO("Read rhythmic value from {}", _input_file);
        // string valuename = string(argv[2])+".txt";
        seg = ValueList(_input_file);
        if (seg.status() != 0)
        {
            ERROR("abort (error {})", seg.status());
            return(2);
        }
        INFO("length rhythmic value: {}", seg.size());
        INFO("rhythmic value = {}", seg);
    }
    
    INFO("parsing");

    // prepare a parsing environment
    assert(_schema);
    parsing::ParsingEnv penv = parsing::ParsingEnv(_schema, NULL);
    parsing::RunCompare comp =
        (f_max)?parsing::weightMax:parsing::weightMin;

    // parse table
    parsing::TableA<parsing::KeySV>* tab = nullptr;      // empty table

    if (nbest == 1)     // 1-best
    {
        INFO("1-best computation");
        tab = new parsing::TableParse<parsing::KeySV,
                                      parsing::RecordOne<parsing::KeySV>,
                                      parsing::KeySVHasher>(penv, comp);
    }
    else             // k-best
    {
        INFO("{}-best computation", nbest);
        assert(nbest > 1); // cannot be 0
        tab = new parsing::TableParse<parsing::KeySV,
                                      parsing::RecordQueue<parsing::KeySV>,
                                      parsing::KeySVHasher>(penv, comp);
    }
    
    // initial augmented state
    const parsing::KeySV k0 = parsing::KeySV(seg, penv);

    // fill table up to n-best
    INFO("parsing IOI segment {} (dur={}bars)", seg, seg.size());
    clock_t time_start = clock();
    assert(tab);
    tab->best(k0, nbest); // fill table up to k-best
    INFO("time to parse : {}ms", util::duration(time_start));

    LabeledRhythmTree* t = LRTFactory<parsing::KeySV>::best(*tab, k0);
    assert(t);
    
    INFO("Ouput tree : {}", *t);
    
    if (f_output && (_output_file.size() == 0))
    {
        ERROR("empty output file name");
        return(1);
    }

    if (f_output)
    {
        size_t err = 0;
        
        if (t->fail())
        {
            // should not happen
            ERROR("Global parse fail");
            err = parse_fail(_output_file); // STUB
        }
        else
        {
            INFO("export parse tree to file {}", _output_file);
            err = t->write(_output_file);
        }
        
        if (err > 0)
        {
            ERROR("write error. failed to export to {}", _output_file);
        }
    }
    
    delete t;
    if (tab) delete tab;

    return 0;
}

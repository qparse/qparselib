//
//  Tlist.hpp
//  qparse
//
//  Created by Florent Jacquemard on 01/06/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#ifndef Tlist_hpp
#define Tlist_hpp

#include <stdio.h>
#include <list>
#include <set>

#include "trace.hpp"
#include "State.hpp"
#include "Weight.hpp"



/// container for grammar's production rules or automata's transitions.
template <typename T>
class Tlist
{
    
public:
    
    typedef typename std::list<T>::iterator iterator;
    
    typedef typename std::list<T>::const_iterator const_iterator;
    
    Tlist():_cpt_size(0) {}
    
    ~Tlist();
    
    /// @brief zero transition
    /// @return an empty transition
    bool empty() const;
    
    /// number of transitions.
    /// @return the number of transitions in this list
    size_t size() const;
    
    /// total size of transition list
    /// @return the sum of sizes of transitions in this list
    /// = number of occurences of states.
    size_t fullsize() const { return _cpt_size; }
    
    /// last transition in this list.
    T& last();
    
    /// add transition at the end of this list
    /// @param t transition to add
    void add(const T& t);

    /// construct and add transition at the end of this list
    /// @param args arguments forwarded to the constructor of the new transition.
    template <class... Args>
    void emplace(Args&&... args);
    
    /// remove the transition pointed by iterator in this transition list
    /// @return an iterator to the element that follows the last element removed
    /// or end if the last element was removed.
    Tlist<T>::iterator remove(Tlist<T>::iterator);
    
    /// remove all transitions of length > 1 in this transition list
    /// containing the given state in their body.
    /// do not remove length 1 transitions to terminal symbols
    void remove(state_t);
    
    void clear();
    
    /// constant iterator pointing to the first transition in this transition list.
    Tlist<T>::const_iterator cbegin() const { return _tlist.cbegin(); }
    
    /// constant iterator pointing to the end of this transition list.
    Tlist<T>::const_iterator cend() const { return _tlist.cend(); }
    
    /// iterator pointing to the first transition in this transition list.
    Tlist<T>::iterator begin() { return _tlist.begin(); }
    
    /// iterator pointing to the end of this transition list.
    Tlist<T>::iterator end() { return _tlist.end(); }
    
private:
    
    /// full size (number of occurences of states).
    size_t _cpt_size;
    
    /// transition list.
    std::list<T> _tlist;
    
    /// set of all states occuring in transitions of list (in head or body).
    std::set<state_t> allStates() const;
};


// separated definition of template class
#include "Tlist.tpp"

#endif /* Tlist_hpp */

//
//  CountingSWTA.hpp
//  schemas
//
//  Created by Florent Jacquemard on 29/09/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup schemata
/// @{


#ifndef CountingSWTA_hpp
#define CountingSWTA_hpp

#include <stdio.h>


#include <assert.h>
#include <vector>
#include <map>
#include <set>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>


#include "trace.hpp"
#include "State.hpp"
#include "Label.hpp"
#include "Weight.hpp"
#include "CountingWeight.hpp"
#include "SWTA.hpp"
#include "RT.hpp"

/// transtition table ordered by transition's ids
typedef
std::set<std::pair<state_t, Transition&>,
         bool(*)(std::pair<state_t, Transition&>,
                 std::pair<state_t, Transition&>)>
OTransitionTable;

/// copy of SWTA dedicated to corpus statistics.
///
/// for SWTA weight estimation and SWTA construction from corpus.
///
/// construction of SWTA with counting weights (unit vectors) from SWTA
/// and verbose tree evaluation with feedback.
///
/// @warning only for target SCHEMA
class CountingSWTA: public SWTA
{
public:
    ///default initializer for cython
    CountingSWTA();
    /// copy base SWTA
    /// reset weight values to counting weights (unit vectors)
    CountingSWTA(const SWTA& a);
    
    virtual bool hasType(std::string code) const
    { return (code == "CountingSWTA"); }

    /// special version of eval for CountingWeight
    /// with feedback in case of fail
    virtual Weight eval(const RhythmTree& t) const;

    /// it is important to enumerate in same order
    /// for printing and building unit weights!
    friend std::ostream& operator<<(std::ostream&, const CountingSWTA&);

protected:
    Weight evalCountingVerbose(const RhythmTree&, state_t, Position) const;

    /// the weight of this SWTA are replaced by "CountingWeight"
    /// unit vector of length dim (one unit per transition)
    void resetCounting(size_t dim);
    
    /// copy of transition table
    /// ordered according to the transition's ids (can be iterated).
    OTransitionTable _tableids;
    
    /// pointer to comparison functionå
    static bool(*_trcomp_ptr)(std::pair<state_t, Transition&>,
                              std::pair<state_t, Transition&>);
    
};


#endif /* CountingSWTA_hpp */

/// @}

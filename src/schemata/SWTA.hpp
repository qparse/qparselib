//
//  SWTA.hpp
//  qparse
//
//  Created by Florent Jacquemard on 02/06/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#ifndef SWTA_hpp
#define SWTA_hpp

#include <stdio.h>

#include "trace.hpp"
#include "State.hpp"
#include "WFSM.hpp"
#include "Transition.hpp"


/// class of symbolic weighted tree automata (schema)
/// replaces WCFG with labeled transitions instead of production rules.
///
/// state : same as WCFG (nt)
///
/// the transition table is a mapping
/// > state -> vector of Transition = (symbolic label, state list, weight)
/// - state = head state
/// - symbol = transition label
/// - state list = transition body. length = arity of label
///
/// @see SymbLabel for the functions managing the label
//
class SWTA : public WFSM<class Transition>
{
public:
    ///nullary constructor for cython
    SWTA();
    
    /// empty grammar
    SWTA(Weight seed);
    
    virtual bool hasType(std::string code) const
    { return (code == "SWTA"); }
    
    
    /// print sizes to output stream
    virtual void print(std::ostream&) const;
    
        
    /// write table content to output stream
    friend std::ostream& operator<<(std::ostream&, const SWTA&);
    
};


#endif /* SWTA_hpp */

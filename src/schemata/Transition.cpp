//
//  Transition.cpp
//  squanty
//
//  Created by Florent Jacquemard on 28/05/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup schemata
/// @{



#include "Transition.hpp"


Transition::Transition(label_t a):
Production(),
_label(a)
{ }


Transition::Transition(label_t a, const Weight& w):
Production(w),
_label(a)
{
    assert (_body.empty());
}


Transition::Transition(label_t a, LetterWeight* lw):
Production(lw),
_label(a)
{
    assert(lw);
    assert (_body.empty());
}


Transition::~Transition()
{
    _body.clear();
}


bool Transition::inner() const
{
    assert(SymbLabel::inner(_label) || _body.empty());
    return SymbLabel::inner(_label);
}


bool Transition::terminal() const
{
    // assert((! SymbLabel::terminal(_label)) || _body.empty());
    return SymbLabel::terminal(_label);
}


bool Transition::dummy() const
{
    assert((! SymbLabel::dummy(_label)) || _body.empty());
    return SymbLabel::dummy(_label);
}


// sifferent from Production
size_t Transition::size() const
{
    return (_body.size());
}


size_t Transition::arity() const
{
    // assert(_arity == SymbLabel::arity(_label));
    // return (SymbLabel::arity(_label));

    // cumulated sum of multiplicities of states in nbody
    return _arity;
}


std::ostream& operator<<(std::ostream& o, const Transition& t)
{
    SymbLabel::print(t.label(), o);
    if (t.terminal())
    {
        o << " : " << t.weight();
    }
    else if (t.inner())
    {
        o << "(";
        Production::Body::const_iterator i = t._body.cbegin();
        while(i != t._body.cend())
        {
            state_t s = i->state;
            size_t m = i->mult;
            assert(m > 0);
            o << s;
            if (m > 1) o << ":" << m;
            o << " ";
            ++i;
            if (i != t._body.cend())
                o << ", ";
        }
        
        o << ") : " << t.weight();
    }
    else
    {
        ERROR("transition not terminal nor inner");
    }
    return o;
}

/// @}

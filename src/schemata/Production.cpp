//
//  Production.cpp
//  qparse
//
//  Created by Florent Jacquemard on 25/01/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//
/// @addtogroup schemata
/// @{


#include "Production.hpp"



Production::BodyItem::BodyItem(state_t s, size_t u, size_t m):
state(s),
mult(m),
sum(u)
{
    assert(m > 0);
}


// initial counter value
size_t Production::_cpt = 0;


Production::Production():
_body(), // empty
_weight(),
_arity(0),
_id(_cpt++)
{ }


Production::Production(const Weight& w):
_body(), // empty
_weight(w),
_arity(0),
_id(_cpt++)
{
    assert (_body.empty());
}


Production::Production(LetterWeight* lw):
_body(), // empty
_weight(lw),
_arity(0),
_id(_cpt++)
{
    assert(lw);
    assert (_body.empty());
}


//Production::Production(std::vector<state_t> v, const Weight& w):
//_body(v), // copy
//_weight(w),
//_id(_cpt++)
//{
//    assert(v.size() > 0);
//    assert (! _body.empty());
//}


//Production::Production(std::vector<state_t> v, LetterWeight* lw):
//_body(v),
//_weight(lw),
//_id(_cpt++)
//{
//    assert(lw);
//    assert(v.size() > 0);
//    assert (! _body.empty());
//}


Production::Production(state_t s, const Weight& w):
_body(), // empty
_weight(w),
_arity(0),
_id(_cpt++)
{
    // assert (_body.empty());
    _body.emplace_back(s, 0); // multiplicity 0 only for this case
}


Production::Production(state_t s, LetterWeight* lw):
_body(), // empty
_weight(lw),
_arity(0),
_id(_cpt++)
{
    assert(lw);
    // assert (_body.empty());
    _body.emplace_back(s, 0); // multiplicity 0 only for this case
}


Production::~Production()
{
    _body.clear();
}


bool Production::inner() const
{
    //    assert(_weight);
    return ((_body.size() > 1) && (! _weight.unknown()));
}


bool Production::terminal() const
{
    //    assert(_weight);
    return ((_body.size() == 1) && (! _weight.unknown()));
}


void Production::setId(size_t id)
{
    _id = id;
}


label_t Production::label() const
{
    assert (terminal());
    return (_body[0].state);
}


void Production::scalar(double d)
{
    _weight.scalar(d);
}


void Production::invert()
{
    _weight.invert();
}


size_t Production::size() const
{
    if (terminal())
        return 0;

    assert (inner());
    return (_body.size());
}


size_t Production::arity() const
{
    if (terminal())
        return 0;

    assert(inner());
    return _arity; // (_body.size());
}


state_t Production::state(size_t i) const
{
    assert (0 <= i);
    assert (i < _body.size());
    return (_body[i].state);
}


size_t Production::multiplicity(size_t i) const
{
    assert (0 <= i);
    assert (i < _body.size());
    return (_body[i].mult);
}


size_t Production::multiplicities(size_t i) const
{
    assert (0 <= i);
    assert (i < _body.size());
    return (_body[i].sum);
}


void Production::push(state_t s, size_t m)
{
    assert(m > 0);
    _arity += m;
    _body.emplace_back(s, _arity, m); // _body.push_back(std::make_pair(s, m));
}


bool Production::member(state_t s) const
{
    if (terminal())
        return false;
    
    assert (inner());
    
    for (auto p : _body)
        if (p.state == s) return true;
    
    return false;
}


bool Production::allin(const std::set<state_t>& e) const
{
    //the singleton body of terminal transition contains terminal symbol
    if (terminal()) return false;
    assert (inner());
    
    //    for (vector<state_t>::const_iterator i = _body.begin(); i != _body.end(); ++i)
    for (auto p : _body)
    {
        if (e.count(p.state) == 0) return false;
    }
    return true;
}


bool Production::nonein(const std::set<state_t>& e) const
{
    //the singleton body of terminal transition contains terminal symbol
    if (terminal()) return true;
    assert (inner());
    
    //    for (vector<state_t>::const_iterator i = _body.begin(); i != _body.end(); ++i)
    for (auto p : _body)
    {
        if (e.count(p.state) != 0) return false;
    }
    return true;
}



std::ostream& operator<<(std::ostream& o, const Production& t)
{
    if (t.terminal())
    {
        o << "<" << t.label() << "> " << t.weight();
    }
    else if (t.inner())
    {
        o << "( ";
        for(Production::Body::const_iterator i = t._body.cbegin();
            i != t._body.cend();
            ++i)
        {
            state_t s = (*i).state;
            size_t m = (*i).mult;
            assert(m > 0);
            o << s;
            if (m > 1) o << ":" << m;
            o << " ";
        }
        
        o << ") " << t.weight();
    }
    else
    {
        ERROR("production rule not terminal nor inner");
    }
    return o;
}


/// @}

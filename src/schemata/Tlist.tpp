//
//  Tlist.tpp
//  qparse
//
//  Created by Florent Jacquemard on 01/06/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include "Tlist.hpp"


template<class T>
Tlist<T>::~Tlist()
{
    clear();
}


template<class T>
bool Tlist<T>::empty() const
{
    return (_tlist.empty());
}


template<class T>
size_t Tlist<T>::size() const
{
    return _tlist.size();
}


template<class T>
T& Tlist<T>::last()
{
    return _tlist.back();
}


template<class T>
void Tlist<T>::add(const T& t)
{
    assert(t.inner() || t.terminal());
    _tlist.push_back(t);
    _cpt_size += t.arity() + 1;
    
    //    assert (_parent);
    //    _parent->_cpt_tr++;
    //    _parent->_cpt_size += (t.arity() + 1);
}


template <class T>
template <class... Args>
void Tlist<T>::emplace(Args&&... args)
{
    _tlist.emplace_back(std::forward<Args>(args)...);
    assert(! _tlist.empty());
    const T& t = _tlist.back();
    _cpt_size += t.arity() + 1;
}


template<class T>
void Tlist<T>::clear()
{
    //for(vector<Production*>::iterator i = _tlist.begin(); i != _tlist.end(); ++i)
    //    delete *i;
    
    //_tlist.clear();
}


template<class T>
typename Tlist<T>::iterator
Tlist<T>::remove(Tlist<T>::iterator i)
{
    _cpt_size -= ((*i).size() + 1);
    return _tlist.erase(i);
}


template<class T>
void Tlist<T>::remove(state_t s)
{
    Tlist<T>::iterator i = _tlist.begin();
    while(i != _tlist.end())
    {
        T& t = *i;
        // we do not remove transition to terminal symbols
        if (t.inner() && (t.member(s)))
        {
            _cpt_size -= (t.size() + 1);
            TRACE("rm state {}: rm transition {}", s, t);
            // remove transition from vector (destructor called)
            i = _tlist.erase(i); // C++11
        }
        else
        {
            ++i;
        }
    }
}

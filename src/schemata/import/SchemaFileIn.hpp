//
//  SchemaFileIn.hpp
//  qparse
//
//  Created by Florent Jacquemard on 03/06/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @defgroup input Input module
///
/// The `input` module contains utilities for reading from and writing to
/// files the data given in input to the quantization by parsing algorithm
/// (schema and segment).



#ifndef SchemaFileIn_hpp
#define SchemaFileIn_hpp

#include <stdio.h>
#include <fstream>
#include <string>

#include "trace.hpp"
#include "Weight.hpp"
#include "WeightDom.hpp"
// no compile options for WCFG weight : load all classes
#include "FloatWeight.hpp"
#include "TropicalWeight.hpp"
#include "ViterbiWeight.hpp"
#include "MeterSig.hpp"
#include "Label.hpp" // global vars


/// @brief static functions for reading in schema files
///
/// @ingroup input
class SchemaFileIn
{
public:
    
    /// @brief read the weight type description found in schema file.
    /// @warning must be in the form "[weight type]" in the file.
    /// @param filename WCFG file read.
    /// @return the weight type description found in filename.
    /// @return WeightDom::UNDEF if not found in file.
    static WeightDom read_weight_type(const std::string filename);

    /// @brief read the specification for grace note abstraction
    /// found in filename.
    /// @warning must be in the form "[max_grace int]" in the file,
    /// with int > 0.
    /// @return 0 if not found in file.
    static size_t read_max_grace(const std::string filename);

    /// @brief search an option in input WCFG file
    ///
    /// @warning the option must be of one of the following forms
    /// - [option] if arg1 = arg2 = NULL or
    /// - [option int] if arg1 != NULL, arg2 = NULL or
    /// - [option int int] if arg1 != NULL, arg2 != NULL.
    ///   in the second and third cases, int are stored in arg1 and arg2.
    /// @return wether the search succeeded.
    static bool read_option(const std::string filename,
                             const std::string option,
                             int* arg1=NULL, int* arg2=NULL);
    
    /// @brief read time signature from schema file
    ///
    /// @return 0 if a time signature was found in filename
    /// in that case, the time signature is stored in the given ts,
    /// otherwise, ts is left unchanged.
    /// @warning must be in the form "[timesig int int]" in the file,
    /// with int > 0.
    static bool readTimesignature(const std::string filename,
                                  ScoreModel::MeterSig& ts);


    static WeightDom choose(WeightDom file_wt, WeightDom given_wt);

    /// @brief a weight value of a type corresponding
    /// to the given weight type description.
    static Weight seed(WeightDom);

    /// @todo TBR
    static Weight seed(bool count_flag,
                        bool penalty_flag,
                        bool stochastic_flag);

};

/// @}

#endif /* SchemaFileIn_hpp */

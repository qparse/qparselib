//
//  SWTAFileIn.cpp
//  squanty
//
//  Created by Florent Jacquemard on 29/05/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
//
// @todo revoir _read... et read... pour eviter plusieurs ouvertures
// @todo revoir parcours du fichier


#include "SWTAFileIn.hpp"

SWTAFileIn::SWTAFileIn():
SWTA()
{}


SWTAFileIn::SWTAFileIn(const Weight& seed):
SWTA(seed)
{}


SWTAFileIn::~SWTAFileIn()
{ }


SWTAFileIn::SWTAFileIn(const std::string filename, WeightDom wt):
SWTA(SchemaFileIn::seed(
     SchemaFileIn::choose(
     SchemaFileIn::read_weight_type(filename), wt))), // empty automaton
_filename(filename)
{
    INFO("SWTA import: read schema from {}", filename);
    
    // there was an error reading seed
    if (_seed.unknown())
    {
        ERROR("SWTA import: error reading weight seed of the automaton, abort");
        return;
    }
    else if (_filename.empty())
    {
        ERROR("SWTA import: empty filename for reading transitions, abort");
        return;
    }
    else if (this->load() > 0) // parse transitions from file
    {
        ERROR("SWTA import: failed reading transitions from {}, abort",
              filename);
        return;
    }
    // parsing successful
    INFO("SWTA import: force weight type {}", wt);
    
    CST_MAX_GRACE = SchemaFileIn::read_max_grace(filename);
    if (CST_MAX_GRACE > 0)
    {
        INFO("SWTA import: found max_grace_note {} declaration in {}",
             CST_MAX_GRACE, filename);
    }
    else
    {
        TRACE("SWTA import: found no max_grace_note declaration in {}",
              filename);
    }
    
    if (wt == WeightDom::UNDEF)
    {
        // penalty
        if (this->hasWeightType("TropicalWeight"))
        {
            CST_WEIGHT_TYPE = WeightDom::PENALTY;
        }
        // counting to stochastic
        else if (this->hasWeightType("FloatWeight"))
        {
            INFO("SWTA import: casting weights from counting into stochastic type.");
            this->CountingtoStochastic();
            CST_WEIGHT_TYPE = WeightDom::STOCHASTIC;
        }
        // stochastic
        else if (this->hasWeightType("ViterbiWeight"))
        {
            CST_WEIGHT_TYPE = WeightDom::STOCHASTIC;
        }
    }
    else if (wt == WeightDom::STOCHASTIC)
    {
        // penalty to stochastic
        if (this->hasWeightType("TropicalWeight"))
        {
            INFO("SWTA import: casting weights from penalty into stochastic type, via counting model.");
            WARN("SWTA import: The outcome of this WTA weight casting is not guaranteed.");
            INFO("SWTA import: casting WTA weights from penalty into counting type.");
            this->PenaltytoCounting();
            INFO("SWTA import: casting WTA weights from counting into stochastic type.");
            this->CountingtoStochastic();
        }
        // counting to stochastic
        else if (this->hasWeightType("FloatWeight"))
        {
            INFO("SWTA import: casting WTA weights from counting into stochastic type.");
            this->CountingtoStochastic();
        }
        // stochastic to stochastic
        else if (this->hasWeightType("ViterbiWeight"))
        {
            TRACE("SWTA import: no WTA weight casting");
        }
        CST_WEIGHT_TYPE = WeightDom::STOCHASTIC;
    }
    else if (wt == WeightDom::PENALTY)
    {
        // penalty to penalty
        if (this->hasWeightType("TropicalWeight"))
        {
            TRACE("SWTA import: no WTA weight casting");
        }
        // counting to penalty
        else if (this->hasWeightType("FloatWeight"))
        {
            INFO("SWTA import: casting WTA weights from counting into penalty type.");
            this->CountingtoPenalty();
        }
        // stochastic to penalty
        else if (this->hasWeightType("ViterbiWeight"))
        {
            INFO("SWTA import: casting WTA weights from stochastic into penalty type.");
            this->StochastictoPenalty();
        }
        CST_WEIGHT_TYPE = WeightDom::PENALTY;
    }
    else if (wt == WeightDom::COUNTING)
    {
        assert(this->hasWeightType("FloatWeight"));
        CST_WEIGHT_TYPE = WeightDom::COUNTING;
    }
}


size_t SWTAFileIn::load()
{
    assert(! _seed.unknown());
    assert(! _filename.empty());
    std::ifstream file;
    
    file.open(_filename, std::ios_base::in);
    if(!file.is_open())
    {
        ERROR("SWTA import: cannot open automata file {}", _filename);
        return 1; // WTA is empty
    }
    
    bool no_transition = true;
    bool parse_error = false;
    int ln = 0;
    
    //read stream line by line
    for(std::string line; getline(file, line); )
    {
        ln++;
        switch(read_line(line, no_transition))
        {
            case 0: // 1 transition parsed
            {
                TRACE("SWTA import: 1 transition parsed line {} of {}", ln, _filename);
                no_transition = false;
                break;
            }
            case 1:
            {
                TRACE("SWTA import: skip line {} of {} (comment)",
                      ln, _filename);
                break;
            }
            case 2:
            {
                ERROR("SWTA import: parse error in line {} of {}",
                     ln, _filename);
                parse_error = true;
                break;
            }
            case 3:
            {
                ERROR("SWTA import: arity error in line {} of {}",
                      ln, _filename);
                parse_error = true;
                break;
            }
            default:
            {
                ERROR("SWTA import: unexpected error in line {} of {}",
                      ln, _filename);
                parse_error = true;
                break;
            }
        }
        // we continue reading the lines even in case of parse error
    }
    size_t ret;
    
    if (no_transition)  // no transition was parsed
    {
        assert(this->empty());
        ERROR("SWTA import: zero transition read from {}", _filename);
        ret = 1;
    }
    else if (parse_error)
    {
        ERROR("SWTA import: parse errors in {}", _filename);
        ret = 2;
    }
    else
    {
        INFO("SWTA import: {} transitions succesfully parsed from {}",
              this->countTransitions(), _filename);
        // @todo TBR
        //initials = { _initial };
        ret = 0;
    }
    
    file.close();
    return ret;
}


size_t SWTAFileIn::read_line(const std::string& line,
                           bool initial)
{
    if (line.size() == 0)
    return 1; // skip empty line

    const std::string::const_iterator begin = line.begin();
    const std::string::const_iterator end = line.end();
    std::string::const_iterator it = begin;
    
    // discard leading spaces
    util::read_spaces(it, begin, end);
    assert((it == end) || (*it != ' '));
    
    // process 1 line of the form "s0 -> a(s1, ..., sn) w"
    
    // first parse s0
    state_t s0;

    if ((it == end) || (! isdigit(*it)))
        return 1; // skip line
    else
    {
        s0 = util::read_int(it, begin, end);
    }
    
    // ignore spaces
    util::read_spaces(it, begin, end);
    assert((it == end) || (*it != ' '));
    
    // parse "->"
    if ((! util::read_char('-', it, begin, end)) ||
        (! util::read_char('>', it, begin, end)))
        return 2; // parse error

    // ignore spaces
    util::read_spaces(it, begin, end);
    assert((it == end) || (*it != ' '));

    // parse the body a(s1, ..., sn)
    
    // parse symbol a
    label_t a = read_symbol(it, begin, end);
    if (SymbLabel::dummy(a))
        return 2; // parse error
    
    // ignore spaces
    util::read_spaces(it, begin, end);
    assert((it == end) || (*it != ' '));

    std::vector<state_t> body = std::vector<state_t>();
    std::queue<std::pair<state_t, size_t>> bodyq =
        std::queue<std::pair<state_t, size_t>>();
    
    if (SymbLabel::arity(a) > 0)
    {
        // parse opening par '('
        if (! util::read_char('(', it, begin, end))
            return 2; // parse error
        util::read_spaces(it, begin, end);
        assert((it == end) || (*it != ' '));
        do // at least one arg because ar > 0
        {
            std::pair<state_t, size_t> p = read_state(it, begin, end);
            bodyq.emplace(p);
            // util::read_spaces(it, begin, end);
        } while ((it != end) && util::read_char(',', it, begin, end));
        
        // parse closing par ')'
        if (! util::read_char(')', it, begin, end))
        {
            return 2; // parse error
        }
        
        // ignore spaces
        util::read_spaces(it, begin, end);
        assert((it == end) || (*it != ' '));
    }

    Weight w = Weight();

    // default weight is 1
    if ((it == end) || (*it == '/'))
    {
        if ((it == end) || read_comment(it, begin, end))
        {
            w = _seed.one();
        }
        else
            return 2;
    }
    // read explicit weight
    else if (isdigit(*it) || (*it == '.'))
    {
        rtu_t val = util::read_float(it, begin, end);
        w = _seed.make(val);
    }
    else
        return 2;


    // add transition
    Transition t = Transition(a, w);
    size_t outdegree = 0;
    while (! bodyq.empty())
    {
        std::pair<state_t, size_t> p = bodyq.front();
        t.push(p.first, p.second);
        outdegree += p.second;
        bodyq.pop();
    }
    // check out degree
    // (bodyq.size() != SymbLabel::arity(a)) if multiplicities
    if (outdegree != SymbLabel::arity(a))
        return 3;
    assert(t.terminal() || t.inner());
    this->add(s0, t, initial);
    TRACE("SWTA import: add transition {} -> {}", s0, t);
    
    return 0; // 1 transition successfuly parsed
}


bool SWTAFileIn::read_comment(std::string::const_iterator& it,
                            const std::string::const_iterator& beg,
                            const std::string::const_iterator& end)
{
    return (util::read_char('/', it, beg, end) &&
            util::read_char('/', it, beg, end));
}


label_t SWTAFileIn::read_symbol(std::string::const_iterator& it,
                              const std::string::const_iterator& beg,
                              const std::string::const_iterator& end)
{
    if ((*it == 'E') || (*it == 'e'))
    {
        it++;
        if (it != end && util::read_char('*',it,beg,end))
            return SymbLabel::SymbLabel::make_varnote(); // UNDEF
        else if (it != end && isdigit(*it))
        {
            unsigned short int info = util::read_int(it, beg, end);
            if (info == 1)
            {
                return SymbLabel::make_note();
            }
            else
            {
                return SymbLabel::make_appogiature(info);
            }
        }
        else
            return SymbLabel::make_dummy();
    }
    else if ((*it == 'C') || (*it == 'c'))
    {
        it++;
        if (it != end && util::read_char('0',it,beg,end))
            return SymbLabel::make_tie(); // continuation
        else
            return SymbLabel::make_dummy();
    }
    else if ((*it == 'D') || (*it == 'd'))
    {
        it++;
        if (it != end && util::read_char('*',it,beg,end))
            return SymbLabel::SymbLabel::make_vardots(); // UNDEF
        else if (it != end && isdigit(*it))
        {
            unsigned short int info = util::read_int(it, beg, end);
            if (info >= 0)
                return SymbLabel::make_dot();
            else
                return SymbLabel::make_dummy();
        }
        else
            return SymbLabel::make_dummy();
    }
    else if ((*it == 'T') || (*it == 't'))
    {
        it++;
        if (it != end && isdigit(*it))
        {
            unsigned char ar = util::read_int(it, beg, end);
            if (ar > 0)
                return SymbLabel::make_tuple(ar, true);
            else
                return SymbLabel::make_dummy();
        }
        else
            return SymbLabel::make_dummy();
    }
    else if ((*it == 'U') || (*it == 'u'))
    {
        it++;
        if (it != end && isdigit(*it))
        {
            unsigned char ar = util::read_int(it, beg, end);
            if (ar > 0)
                return SymbLabel::make_tuple(ar, false);
            else
                return SymbLabel::make_dummy();
        }
        else
            return SymbLabel::make_dummy();
    }
    else if ((*it == 'R') || (*it == 'r'))
    {
        it++;
        //assert(SymbLabel::rest(SymbLabel::make_rest()));
        return SymbLabel::make_rest();
    }
    else if ((*it == 'G') || (*it == 'g'))
    {
        it++;
        return SymbLabel::make_grace();
    }
    else if ((*it == 'B') || (*it == 'b'))
    {
        it++;
        if (it != end && util::read_char('0',it,beg,end))
            return SymbLabel::make_bar(0);
        else if (it != end && util::read_char('2',it,beg,end))
            return SymbLabel::make_bar(2);
        else
            return SymbLabel::make_dummy();
    }
    else if ((*it == 'F') || (*it == 'f'))
    {
        it++;
        return SymbLabel::make_fork();
    }
    else
    {
        return SymbLabel::make_dummy();
    }
}


std::pair<state_t, size_t>
SWTAFileIn::read_state(std::string::const_iterator& it,
                     const std::string::const_iterator& beg,
                     const std::string::const_iterator& end)
{
    size_t m = 1;
    util::read_spaces(it, beg, end);
    state_t q = util::read_int(it, beg, end);
    // ':mult' is optional
    if (util::read_char(':', it, beg, end)) // if (*it == ':')
    {
        m = util::read_int(it, beg, end);
    }
    util::read_spaces(it, beg, end);
    assert(m > 0);
    return std::make_pair(q, m);
}


// revoir : ajouter weight type, max grace etc
size_t SWTAFileIn::save(std::string filename)
{
    std::ofstream file;
    
    file.open(filename, std::ios_base::out);
    if (! file.is_open())
    {
        ERROR("cannot open file {} for writing schema", filename);
        return 1;
    }
    
    file << *this;
    
    file.close();
    return 0;
}


// old version. TBR.
size_t SWTAFileIn::read_line_stream(const std::string& line,
                                  const int& ln,
                                  bool& initial_transition)
{
    // skip empty line
    if (line.size() == 0)
        return 0; // continue
    
    std::istringstream in(line);   //make a stream from the line
    
    // discard leading white spaces
    //in >> std::ws;
    // skip white spaces
    in >> std::skipws;
    
    // process 1 line of the form "s (s0 ... sn) w"
    // extract s
    state_t s;
    if (!(in >> s))
    {
        TRACE("read {}: line {} ignored (comment?)", _filename, ln);
            return 0; // continue; // not a transition (comment etc): skip line
    }
    
    //TRACE("state {}", s);
    // extract '('
    std::string buf;
    in >> buf;
    if (buf != "(")
    {
        WARN("parse error (body={}), line {} ignored", buf, ln);
        return 2; //continue; // parse error: skip line
    }
    
    // extract s0 ... sn into vector body
    getline(in, buf, ')');
    std::istringstream ins(buf);
    std::vector<state_t> body;
    state_t q;
    while (ins >> q)
     body.push_back(q);
    
    double val;
    if (!(in >> val))
    {
        WARN("error parsing weight value, line {} ignored", ln);
            return 3;  //continue; // parse error: ignore line
    }
    
    // one transition parsed from current line
    // copy content of vector body to new transition
    // and add the transition to the table
    
    // terminal transition
    // in this format of WTA serialization
    if (body.size() == 1)
    {
        unsigned short int n = body[0];
        label_t a;
        if (n == 0)
            a = SymbLabel::make_tie();
        else if (n == 1)
            a = SymbLabel::make_note();
        else
            a = SymbLabel::make_appogiature(n);
        Transition t = Transition(a, _seed.make(val));
        assert(t.terminal() || t.inner());
        this->add(s, t, initial_transition);
        TRACE("add transition {} -> {}", s, t);
    }
    else
    {
        assert(body.size() > 1);
        label_t a = SymbLabel::make_tuple(body.size(), true);
        Transition t = Transition(a, _seed.make(val));
        for (state_t s : body)
        t.push(s);
        assert(t.terminal() || t.inner());
        add(s, t, initial_transition);
        TRACE("add transition {} -> {}", s, t);
    }
    // initial state is the first target state read in the file
    if (initial_transition)
    initial_transition = false;
    
    return 0;
}

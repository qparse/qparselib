//
//  SchemaFileIn.cpp
//  qparse
//
//  Created by Florent Jacquemard on 03/06/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include "SchemaFileIn.hpp"


// static
WeightDom SchemaFileIn::read_weight_type(const std::string filename)
{
    WeightDom res = WeightDom::UNDEF;
    
    assert(! filename.empty());
    std::ifstream file;
    file.open(filename, std::ios_base::in);
    if(!file.is_open())
    {
        ERROR("cannot open file {}", filename);
        return res;
    }
    
    int ln = 0;
    
    //read stream line by line
    for(std::string line; getline(file, line); )
    {
        ln++;
        
        // skip empty line
        if (line.size() == 0) continue;
        
        //make a stream from the line
        std::istringstream in(line);
        
        // discard leading white spaces
        //in >> std::ws;
        // skip white spaces
        in >> std::skipws;
        
        // first (non space) character of line
        int c = in.peek();
        
        // process 1 line of the form "[weight model]"
        // parse weight type
        if (c == '[')
        {
            // extract leading '['
            int a = in.get();
            assert(a == '[');
            // get the weight type descr in buf
            std::string buf;
            getline(in, buf, ']');
            std::transform(buf.begin(), buf.end(),buf.begin(), ::toupper);
            TRACE("declaration '{}' found in {}", buf, filename);
            if (buf == "PROBABILITY" ||
                buf == "PROBA" ||
                buf == "STOCHASTIC")
            {
                res = WeightDom::STOCHASTIC;
                break;
            }
            else if (buf == "PENALTY" ||
                     buf == "COST")
            {
                res = WeightDom::PENALTY;
                break;
            }
            else if (buf == "COUNTING" ||
                     buf == "COUNT")
            {
                res = WeightDom::COUNTING;
                break;
            }
            else
            {
                // ERROR("unknown weight type {} in {}", buf, filename);
                // not a weight declaration, ignore
                continue;
            }
        }
        else if (std::isdigit(c) || c == EOF)
        {
            WARN("no weight type declaration found in grammar file {}",
                 filename);
            break;
        }
        else
        {
            // comment etc: skip line
            TRACE("searching weight type decl. in grammar file {}: line {} ignored",
                  filename, ln);
            continue;
        }
    }
    
    file.close();
    
    // may be WeightDom::UNDEF
    INFO("found weight type {} in {}", res, filename);
    return res;
}


// static
bool SchemaFileIn::read_option(const std::string filename,
                            const std::string option,
                            int* arg1, int* arg2)
{
    bool ret = false;
    
    assert(! filename.empty());
    std::ifstream file;
    file.open(filename, std::ios_base::in);
    if(!file.is_open())
    {
        ERROR("[read_options] cannot open file {}", filename);
        return false;
    }
    
    int ln = 0;
    
    //read stream line by line
    for(std::string line; getline(file, line); )
    {
        ln++;
        
        // skip empty line
        if (line.size() == 0) continue;
        
        //make a stream from the line
        std::istringstream in(line);
        
        // skip white spaces
        in >> std::skipws;
        
        // first (non space) character of line
        int c = in.peek();
        
        // process 1 line of the form "[weight model]"
        // parse weight type
        if (c == '[')
        {
            // extract leading '['
            int a = in.get();
            assert(a == '[');
            // get the weight type descr in buf
            std::string buf;
            std::getline(in, buf, ' ');
            std::transform(buf.begin(), buf.end(),buf.begin(), ::toupper);
            if (buf == option)
            {
                // first argument
                if (arg1)
                {
                    if (!(in >> *arg1))
                    {
                        ERROR("grammar file {}:{} \
                              declaration {} should contain number. ignored",
                              filename, ln, option);
                        ret = false;
                    }
                    else if (arg2)
                    {
                        if (!(in >> *arg2))
                        {
                            ERROR("grammar file {}:{} declaration {} \
                                  should contain 2 numbers. ignored",
                                  filename, ln, option);
                            ret = false;
                        }
                        else
                        {
                            // two args
                            TRACE("grammar file {}:{}\
                                  declaration '{}' arg1={} arg2={}.",
                                  filename, ln, buf, *arg1, *arg2);
                            ret = true;
                        }
                    }
                    else
                    {
                        // only one arg
                        TRACE("grammar file {}:{} declaration '{}' arg1={}.",
                              filename, ln, buf, *arg1);
                        ret = true;
                    }
                }
                else
                {
                    // no argument
                    TRACE("grammar file {}:{} declaration '{}' (no arg).",
                          filename, ln, buf);
                    ret = true;
                }
                break;
            }
        }
        else if (std::isdigit(c) || c == EOF)
        {
            WARN("no declaration {} in grammar file {}", option, filename);
            ret = false;
            break;
        }
        else
        {
            // comment etc: skip line
            TRACE("searching declaration in grammar file {}: line {} ignored",
                  filename, ln);
            continue;
        }
    }
    
    file.close();
    
    return ret;
}


// static
bool SchemaFileIn::readTimesignature(const std::string filename,
                                   ScoreModel::MeterSig& ts)
{
    int num;
    int den;
    if (read_option(filename, "TIMESIG", &num, &den))
    {
        assert(num > 0);
        assert(den > 0);
        TRACE("time signature {}/{} found in {}", num, den, filename);
        ts = ScoreModel::MeterSig(num, den);
        return true;
    }
    else
    {
        TRACE("no time signature found in {}", filename);
        return false;
    }
}


size_t SchemaFileIn::read_max_grace(const std::string filename)
{
    int res;
    if (read_option(filename, "MAX_GRACE", &res))
    {
        return res;
    }
    else
    {
        return 0;
    }
}


// static
//size_t SchemaFileIn::read_max_grace(const std::string filename)
//{
//    size_t res = 0;
//
//    assert(! filename.empty());
//    ifstream file;
//    file.open(filename, ios_base::in);
//    if(!file.is_open())
//    {
//        ERROR("cannot open grammar file {}", filename);
//        return res;
//    }
//
//    int ln = 0;
//
//    //read stream line by line
//    for(string line; getline(file, line); )
//    {
//        ln++;
//
//        // skip empty line
//        if (line.size() == 0) continue;
//
//        //make a stream from the line
//        istringstream in(line);
//
//        // skip white spaces
//        in >> std::skipws;
//
//        // first (non space) character of line
//        int c = in.peek();
//
//        // process 1 line of the form "[weight model]"
//        // parse weight type
//        if (c == '[')
//        {
//            // extract leading '['
//            int a = in.get();
//            assert(a == '[');
//            // get the weight type descr in buf
//            std::string buf;
//            std::getline(in, buf, ' ');
//            std::transform(buf.begin(), buf.end(),buf.begin(), ::toupper);
//            TRACE("declaration '{}' found in {}", buf, filename);
//            if (buf == "MAX_GRACE")
//            {
//                if (!(in >> res))
//                {
//                    ERROR("declaration max_grace should contain number: {} line {}. ignored",
//                          filename, ln);
//                }
//            }
//        }
//        else if (std::isdigit(c) || c == EOF)
//        {
//            WARN("no  max_grace_note declaration in grammar file {}", filename);
//            break;
//        }
//        else
//        {
//            // comment etc: skip line
//            TRACE("searching declaration in grammar file {}: line {} ignored",
//                  filename, ln);
//            continue;
//        }
//    }
//
//    file.close();
//
//    return res;
//}


//static
WeightDom SchemaFileIn::choose(WeightDom file_wt, WeightDom given_wt)
{
    if (file_wt == WeightDom::UNDEF)
    {
        return given_wt;
    }
    else
    {
        return file_wt;
    }
}


// static
Weight SchemaFileIn::seed(WeightDom w)
{
    switch (w)
    {
        case WeightDom::PENALTY:
        {
            return TropicalWeight::make_zero();
            break;
        }
        case WeightDom::STOCHASTIC:
        {
            return ViterbiWeight::make_zero();
            break;
        }
        case WeightDom::COUNTING:
        {
            return FloatWeight::make_zero();
            break;
        }
        default:
        {
            ERROR("WTA Seed : weight type undef");
            return Weight();
            break;
        }
    }
}

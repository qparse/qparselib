//
//  SWTAFileIn.hpp
//  squanty
//  qparse V2
//
//  Created by Florent Jacquemard on 29/05/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup input
/// @{

#ifndef SWTAFileIn_hpp
#define SWTAFileIn_hpp

#include <stdio.h>
#include <algorithm>
#include <string>
#include <queue>

#include "trace.hpp"
#include "util.hpp" // parse string
#include "RTU.hpp"
#include "Weight.hpp"
#include "WeightDom.hpp"
// no compile options for WTA weight : load all classes
#include "FloatWeight.hpp"
#include "TropicalWeight.hpp"
#include "ViterbiWeight.hpp"
#include "Label.hpp" // global vars
#include "Production.hpp"
#include "Transition.hpp"
#include "SWTA.hpp"
//#include "ScoreMeter.hpp"
#include "SchemaFileIn.hpp"


/// @brief wrapper for building WTA with various flags for weight type.
/// parse WTA content from a file.
/// @ingroup input
///
/// syntax for file:
/// at most one transition per line, in the form
/// > s0 -> a w
/// where a is a constant (arity 0) symbol (label_t)
/// > s0 -> a(s1, ..., sn) w
/// where symbol a (label_t) has arity n > 0, and
/// - s0, s1, ..., sn are ints (state_t)
/// - w is a weight value (int or float)
/// the marker for comments is `//`.
///
/// the target of the first transition in thge file
/// is the initial state of the SWTA.
///
/// @see SymbLabel.hpp for the syntax of supported symbols
class SWTAFileIn : public SWTA
{
public:
    /// @brief default constructor for cython.
    SWTAFileIn();
    
    /// @brief constructor for testing.
    SWTAFileIn(const Weight& seed);
    
    
    /// @brief read weight type and schema from file.
    ///
    /// @param filename input text file specifying the schema
    /// @param wt weight type (forced)
    ///
    /// if another weight type is given as argument (forced weight type)
    /// - use it as weight type for reading schema if no weight type found in file
    /// - use it to cast schema (force type).
    /// the WTA can be empty in case of error.
    SWTAFileIn(const std::string filename,
             WeightDom wt = WeightDom::UNDEF);
    
    
    /// @brief read schema from file
    /// @param filename input text file specifying the schema
    /// @param count_flag flag to determine the type of weights
    /// @param penalty_flag flag to determine the type of weights
    /// @param stochastic_flag flag to determine the type of weights
    ///
    /// casts weights according to compile options if needed.
    ///
    /// @todo TBR
    SWTAFileIn(const std::string filename,
            bool count_flag=false,
            bool penalty_flag=true,
            bool stochastic_flag=false);
    
    /// @brief same as WTA destructor.
    ~SWTAFileIn();
    
    /// @brief save to file.
    size_t save(std::string filename);
    
    
private:
    
    const std::string _filename;
    
    // weight type found in schema file
    //WeightDom _filewt;
    
    // weight type forced by options
    //WeightDom _givenwt;
    
    
    /// @brief read SWTA from file.
    /// create transition of same type as given sample weight
    /// the initial state set is set to singleton { 0 }
    /// in case of parse error, the WTA is empty.
    /// @return error code 0 if file could be parsed and loaded into SWTA,
    ///         error code > 0 otherwise.
    size_t load();
    

    /// parse 1 transition in given line of SWTAFileIn::_filename
    /// @param line the string to parse.
    /// @param initial if a transition is parsed,
    ///        then its target is the initial state.
    /// @return 0: 1 transition parsed
    /// @return 1: skip line
    /// @return 2: parse error
    /// @return 3: arity error
    size_t read_line(const std::string& line,
                     bool initial);


    /// @return the symbol read at it
    /// or a dummy symbol in case of parse error.
    label_t read_symbol(std::string::const_iterator& it,
                        const std::string::const_iterator& beg,
                        const std::string::const_iterator& end);

    /// @return a pair made of a state and a multiplicity value at it
    /// or a dummy symbol in case of parse error.
    /// the string at it may contain a state (int) or a pattern state:mult.
    std::pair<state_t, size_t>
    read_state(std::string::const_iterator& it,
               const std::string::const_iterator& beg,
               const std::string::const_iterator& end);
    
    bool read_comment(std::string::const_iterator& it,
                      const std::string::const_iterator& beg,
                      const std::string::const_iterator& end);


    /// old version, TBR
    size_t read_line_stream(const std::string& line,
                            const int& ln,
                            bool& initial_transition);

};

/// @}

#endif /* SWTAFileIn_hpp */

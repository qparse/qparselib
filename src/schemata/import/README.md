# src/schemata/import

Facilities for reading a language model used by the transcription by parsing algorithm (CF grammar or tree automaton).

The automata or grammars can be read from a text file describing transition rules and some options (weight type, maximum number of grace notes). 
For details on the syntax of transition rules and options, see [top readme](../../README.md).

A grammar can can also be saved to a file.

//
//  SymbLabel.cpp
//  qparse
//
//  Created by Florent Jacquemard on 04/10/16.
//  Copyright © 2016 Florent Jacquemard. All rights reserved.
//
/// @addtogroup schemata
/// @{


#include <assert.h>

#include "SymbLabel.hpp"


// static function
label_t SymbLabel::make(unsigned char ar,
                        SymbLabel::Kind k,
                        unsigned short int mult)
{
    assert(0 <= ar);
    assert(ar <= 255);

    label_t a = ar;
    a += (k<<8);
    a += (mult<<22); // multiplicity = 1
    a += MASK_UNDEF; // set as undefined
    return a;
}


// static function
label_t SymbLabel::make(unsigned char ar,
                        SymbLabel::Kind k,
                        unsigned short int info1,
                        unsigned short int info2)
{
    assert(0 <= ar);
    assert(ar <= 255);
    assert(0 <= info1);
    assert(info1 < 512);
    assert(0 <= info2);
    assert(info2 < 512);

    label_t a = ar;
    a += (k<<8);
    a += (info1<<13);
    a += (info2<<22);
    return a;
}


// static function
//label_t SymbLabel::make_inside(unsigned char ar)
//{
//    return make(ar, SymbLabel::STD);
//}

// static function
// label_t SymbLabel::make_inside(unsigned char ar, unsigned short int info)
//{
//    return make(ar, SymbLabel::STD, info, 1);
//}


// static function
label_t SymbLabel::make_note(unsigned short int mult)
{
    return make(0, SymbLabel::STD, 1, mult);
}


// static function
label_t SymbLabel::make_varnote()
{
    return make(0, SymbLabel::STD, 1);
}


label_t SymbLabel::make_appogiature(unsigned short int nb,
                                    unsigned short int mult)
{
    return make(0, SymbLabel::STD, nb, mult);
}


// static function
label_t SymbLabel::make_rest(unsigned short int mult)
{
    return make(0, SymbLabel::REST, 1, mult);
}


// static function
label_t SymbLabel::make_tie(unsigned short int mult)
{
    return make(0, SymbLabel::CONT, 0, mult);
}


// static function
label_t SymbLabel::make_multi(unsigned short int mult)
{
    return make(0, SymbLabel::MULT, 0, mult);
}


// static function
label_t SymbLabel::make_dot(unsigned short int nb)
{
    return make(0, SymbLabel::CONT, nb, 1);
}


// static function
label_t SymbLabel::make_vardots()
{
    return make(0, SymbLabel::CONT, 1);
}


// static function
label_t SymbLabel::make_tuple(unsigned char ar, bool beamed,
                              unsigned short int mult)
{
    assert((mult == 0) || (mult == 1));
    return make(ar, SymbLabel::STD, beamed?(1):0, mult);
}


// static function
label_t SymbLabel::make_grace(unsigned short int mult)
{
    assert((mult == 0) || (mult == 1));
    return make(2, SymbLabel::GRACE, 0, mult);
}

// static function
label_t SymbLabel::make_bar(unsigned char ar, unsigned short int mult)
{
    assert((mult == 0) || (mult == 1));
    return make(ar, SymbLabel::BAR, 0, mult);
}

// static function
label_t SymbLabel::make_fork(unsigned short int mult)
{
    assert((mult == 0) || (mult == 1));
    return make(2, SymbLabel::FORK, 0, mult);
}

// static function
label_t SymbLabel::make_space()
{
    return make(0, SymbLabel::DUMMY, 1, 0);
}


// static function
label_t SymbLabel::make_fail(size_t n)
{
    return make(0, SymbLabel::DUMMY, 2, n);
}


// static function
label_t SymbLabel::make_dummy()
{
    return make(0, SymbLabel::DUMMY, 0, 0);
}


// static function
label_t SymbLabel::set_multiplicity(label_t a, unsigned short int m)
{
    label_t b = a;
    b &= ~MASK_INFO2; // reset multiplicity to 0
    b += (m<<22);
    return b;
}


// static function
size_t SymbLabel::arity(label_t a)
{
    return (a & MASK_ARITY);
}


// static function
bool SymbLabel::terminal(label_t a)
{
    // inside-the-bar or or rest or bar or dummy
    return (arity(a) == 0);
}


// static function
bool SymbLabel::inner(label_t a)
{
    return (arity(a) > 0);
}


// static function
bool SymbLabel::binary(label_t a)
{
    return (arity(a) == 2);
}


// static function
SymbLabel::Kind SymbLabel::kind(label_t a)
{
    return static_cast<SymbLabel::Kind>((a & MASK_KIND)>>8);
}


// static function
unsigned short int SymbLabel::specific(label_t a)
{
    assert(! undef(a));
    return ((a & MASK_INFO)>>13);
}


// static function
unsigned short int SymbLabel::specific2(label_t a)
{
    return ((a & MASK_INFO2)>>22);
}


// static function
unsigned short int SymbLabel::inmultiplicity(label_t a)
{
    // assert(! undef(a));
    return specific2(a);
}


// static function
/// @todo STUB. complete.
unsigned short int SymbLabel::outmultiplicity(label_t a, size_t i)
{
    return 1;
}


// static function
bool SymbLabel::undef(label_t a)
{
    return ((a & MASK_UNDEF) != 0);
}

// static function
bool SymbLabel::inside(label_t a)
{
    return ((kind(a) == STD) ||
            (kind(a) == REST) ||
            (kind(a) == CONT) ||
            (kind(a) == MULT) ||
            (kind(a) == GRACE) ||
             fail(a) ||
             space(a));
}


// static function
bool SymbLabel::rest(label_t a)
{
    return (kind(a) == REST);
}


// static function
bool SymbLabel::note(label_t a)
{
    return ((kind(a) == STD) && (arity(a) == 0));
}


// static function
size_t SymbLabel::nbEvents(label_t a)
{
    if (note(a))
    {
        assert(! undef(a));
        size_t e = specific(a);
        return e;
    }
    else if (rest(a))
        return 1;
    else if (fail(a))
        return specific2(a);
    else
        return 0;
}


// static function
size_t SymbLabel::nbGraceNotes(label_t a)
{
    size_t e = nbEvents(a);
    if (e > 0)
        return e - 1;
    else
        return 0;
}


// static function
bool SymbLabel::continuation(label_t a)
{
    if (kind(a) == CONT)
    {
        assert(arity(a) == 0);
        return ((! undef(a)) && (specific(a) == 0));
    }
    else
        return false;
}


// static function
bool SymbLabel::multi(label_t a)
{
    if (kind(a) == MULT)
    {
        assert(arity(a) == 0);
        assert(! undef(a));
        assert(specific(a) == 0);
        return true;
    }
    else
        return false;
}


// static function
bool SymbLabel::dot(label_t a)
{
    if (kind(a) == CONT)
    {
        assert(arity(a) == 0);
        return (undef(a) || (specific(a) > 0));
    }
    else
        return false;
}


// static function
size_t SymbLabel::nbDots(label_t a)
{
    assert(dot(a));
    assert(! undef(a));
    return (specific(a));
}


// static function
bool SymbLabel::tuple(label_t a)
{
    return ((kind(a) == STD) && (arity(a) > 0));
}


// static function
bool SymbLabel::beamed(label_t a)
{
    assert(tuple(a));
    assert(! undef(a));

    return (specific(a) != 0);
}


// static function
bool SymbLabel::grace(label_t a)
{
    return (kind(a) == GRACE);
}


// static function
bool SymbLabel::bar(label_t a)
{
    return (kind(a) == BAR);
}


// static function
bool SymbLabel::fork(label_t a)
{
    return (kind(a) == FORK);
}


// static function
bool SymbLabel::space(label_t a)
{
    assert((kind(a) != DUMMY) || (arity(a) == 0));
    return ((kind(a) == DUMMY) && (specific(a) == 1) && (specific2(a) == 0));
}


// static function
bool SymbLabel::fail(label_t a)
{
    assert((kind(a) != DUMMY) || (arity(a) == 0));
    return ((kind(a) == DUMMY) && (specific(a) == 2));
}


// static function
bool SymbLabel::dummy(label_t a)
{
    assert((kind(a) != DUMMY) || (arity(a) == 0));
    return ((kind(a) == DUMMY) && (specific(a) == 0) && (specific2(a) == 0));
}



// static function
label_t SymbLabel::setNb(label_t a, unsigned short int info)
{
    assert(note(a) || dot(a));
    assert(undef(a)); // can be done only once
    return make(arity(a), kind(a), info, inmultiplicity(a));
//    label_t b = a;
//    b &= (~MASK_UNDEF);            // unset the UNDEF bit
//    b &= (~MASK_INFO);             // erase specific info
//    b &= ((info<<13) & MASK_INFO); // replace by given info
//    assert(! undef(b));
//    return b;
}








// @todo TBR
bool SymbLabel::abstract(label_t a)
{
    assert(terminal(a));
    assert(! undef(a));
    size_t e = specific(a);
    return (CST_MAX_GRACE == 0) || (e <= CST_MAX_GRACE);
}


// @todo TBR
bool SymbLabel::abstract(label_t a, label_t n)
{
    assert(terminal(a));
    assert(! undef(a));

    size_t e = specific(a);

    // no abstraction
    if (CST_MAX_GRACE == 0)
    {
        return (e == n);
    }

    assert(CST_MAX_GRACE > 0);
    if (e < CST_MAX_GRACE)
    {
        return (e == n);
    }
    else
    {
        assert(e == CST_MAX_GRACE);
        return (e <= n);
    }
}


// @todo TBR
bool SymbLabel::leqabstract(label_t a, label_t n)
{
    assert(terminal(a));
    assert(! undef(a));

    size_t e = specific(a);

    // no abstraction
    if (CST_MAX_GRACE == 0)
    {
        return (n <= e);
    }
    
    assert(CST_MAX_GRACE > 0);
    
    if (e < CST_MAX_GRACE)
    {
        return (n <= e);
    }
    else
    {
        assert(e == CST_MAX_GRACE);
        // TBC      return (a <= n);
        // for all n, exists c such that n+c >= CST_MAX_GRACE
        return true;
    }
}


std::string SymbLabel::to_string(label_t a)
{
    size_t ar = SymbLabel::arity(a);
    std::stringstream o;
    
    switch (SymbLabel::kind(a))
    {
        case SymbLabel::STD:
            if (ar == 0)
            {
                if (undef(a))
                    o << "E*";
                else if (SymbLabel::specific(a) > 0)
                    o << "E" << SymbLabel::specific(a);
                else
                {
                    ERROR("SymbLabel: WRONG SYMBOL STD cst: {}", a);
                }
            }
            else if ((! undef(a)) && (SymbLabel::specific(a) == 1))
                o << "T" << ar; // beamed tuple
            else if ((! undef(a)) && (SymbLabel::specific(a) == 0))
                o << "U" << ar; // unbeamed tuple
            else
                ERROR("SymbLabel: WRONG SYMBOL STD ar={}: {}", ar, a);
            break;
            
        case SymbLabel::CONT:
            assert(ar == 0);
            if (undef(a))
                o << "D*";
            else if (SymbLabel::specific(a) == 0)
                o << "C0";
            else if (SymbLabel::specific(a) > 0)
                o << "D" << SymbLabel::specific(a);
            else
                ERROR("SymbLabel: WRONG SYMBOL CONT specific={}: {}",
                      SymbLabel::specific(a), a);

            break;

        case SymbLabel::REST:
            assert(ar == 0);
            assert(! undef(a));
            assert(SymbLabel::specific(a) == 1);
            o << "R";
        break;

        case SymbLabel::GRACE:
            assert(ar == 2);
            o << "G";
            break;

        case SymbLabel::BAR:
            assert((ar == 0) || (ar == 2));
            o << "B" << ar;
            break;

        case SymbLabel::FORK:
            assert(ar == 2);
                o << "F";
            break;

        case SymbLabel::DUMMY:
            assert(ar == 0);
            if (SymbLabel::specific(a) == 0)
                o << "DUMMY";
            else if (SymbLabel::specific(a) == 1)
                o << "SPACE";
            else if (SymbLabel::specific(a) == 2)
                o << "FAIL(" << SymbLabel::specific2(a) << ")";
            break;

        default:
            assert (false); // should not happen
    }
    
    return o.str();
}


// static
void SymbLabel::print(label_t a, std::ostream& o)
{
    o << SymbLabel::to_string(a);
}

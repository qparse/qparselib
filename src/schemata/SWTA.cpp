//
//  SWTA.cpp
//  qparse
//
//  Created by Florent Jacquemard on 02/06/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include "SWTA.hpp"


SWTA::SWTA():
WFSM<Transition>()
{ }


SWTA::SWTA(Weight seed):
WFSM<Transition>(seed)
{
    // assert (! seed.unknown());
}


void SWTA::print(std::ostream& o) const
{
    o << '\n';
    o << this->countStates() << " states\n";
    o << this->countTransitions() << " transitions\n";
    o << this->countAll() << " total symbols\n\n";
    //o << *this;
}


std::ostream& operator<<(std::ostream& o, const SWTA& a)
{
    for (typename std::map<state_t,Tlist<Transition>>::const_iterator
         i = a._table.begin();
         i != a._table.end(); ++i)
    {
        state_t s = i->first;
        Tlist<Transition> tl = i->second;
        for(typename Tlist<Transition>::const_iterator j = tl.cbegin(); j != tl.cend(); j++)
        {
            const Transition& t = *j;
            o << s << " -> " << t << " \n";
        }
    }
    return o;
}

//
//  WFSM.tpp
//  qparse
//
//  Created by Florent Jacquemard on 04/10/16.
//  Copyright © 2016 Florent Jacquemard. All rights reserved.
//
/// @addtogroup schemata
/// @{


//nullary constructor for cython
template<class T>
WFSM<T>::WFSM():
_cpt_tr(0),
_cpt_size(0),
_next_id(0)
{}


// empty automaton
template<class T>
WFSM<T>::WFSM(Weight seed):
_seed(seed),
_cpt_tr(0),
_cpt_size(0),
_next_id(0)
{
    // assert (! seed.unknown());
}


template<class T>
WFSM<T>::~WFSM()
{
    TRACE("delete WFSM transition table (size={})", _table.size());
    
    for (typename std::map<state_t,Tlist<T>>::iterator i = _table.begin();
         i != _table.end(); i++)
    {
        Tlist<T>& tl = i->second;
        tl.clear();
    }
    // TBC destroy the Tlist<T> contents?
    _table.clear();
}


template<class T>
size_t WFSM<T>::size() const
{
    return _table.size();
}


template<class T>
bool WFSM<T>::empty() const
{
    return _table.empty();
}


template<class T>
bool WFSM<T>::isRegistered(state_t s) const
{
    return (_table.count(s) > 0);
}


template<class T>
bool WFSM<T>::isInitial(state_t s) const
{
    // return (initials.count(s));
    return (s == _initial);
}


template<class T>
const T& WFSM<T>::rule(state_t s, size_t id) const
{
    typename std::map<state_t, Tlist<T>>::const_iterator i = _table.begin();
    assert(i != _table.end()); // not empty
    i += id;
    assert(i != _table.end()); // id is correct
    return *i;
}


template<class T>
Tlist<T>& WFSM<T>::add(state_t s, bool initial)
{
    // _table[s]:
    // if there is an entry for s, return it
    // if there is no entry for s, one is created with empty transition list
    // (see stl::map)
    Tlist<T>& tl = _table[s];
    // tl._parent = this;
    if (initial)
    {
        _initial = s;
        // initials.insert(s);
    }
    
    return(tl);
}


template<class T>
Tlist<T>& WFSM<T>::add(state_t s, const T& t, bool initial)
{
    assert(t.inner() || t.terminal());
    // updates the counters _cpt_tr and _cpt_size
    Tlist<T>& tl = add(s, initial);
    tl.add(t);
    // register all states in the body
    for (Production::Body::const_iterator i = t.cbegin(); i != t.cend(); i++)
    {
        state_t sub = i->state;
        add(sub, false);
    }
    _cpt_tr++;
    _cpt_size += (t.arity() + 1);
    return tl;
}


//template <class T>
//template <class... Args>
//Tlist<T>& WFSM<T>::add_emplace(state_t s, Args&&... args, bool initial)
//{
//    Tlist<T>& tl = add(s, initial);
//    tl.emplace(std::make_tuple(std::forward<Args>(args)...));
//    const T& t = tl.last();
//    
//    for (Body::const_iterator i = t.cbegin(); i != t.cend(); i++)
//    {
//        state_t sub = *i;
//        add(sub, false);
//    }
//    _cpt_tr++;
//    _cpt_size += (t.arity() + 1);
//
//    return tl;
//}


template<class T>
void WFSM<T>::remove(state_t s)
{
    TRACE("remove from WFSM all transitions containing state {}", s);
    
    // first traversal transition map:
    // to remove transition with s occurring in body
    // and to update size counters
    typename std::map<state_t, Tlist<T>>::iterator i = _table.begin();
    
    while (i != _table.end())
    {
        state_t q = i->first;
        Tlist<T>& tl = i->second;
        
        // list of transitions headed to s
        // remove the whole list (table entry)
        if (q == s)
        {
            // number of transitions headed to s
            _cpt_tr -= tl.size();;
            // sum of sizes of transitions headed to s
            _cpt_size -= tl.fullsize();
            TRACE("remove from WFSM all transitions headed with {}", q);
            i = _table.erase(i); // C++11
        }
        // delete the transitions containing s in the body (TransitionList of q)
        else
        {
            TRACE("DebugInsideElse");
            _cpt_tr -= tl.size();
            _cpt_size -= tl.fullsize();
            TRACE("remove from WFSM all transitions with {} in body", s);
            tl.remove(s);
            TRACE("DebugAfterTvRemove");
            _cpt_tr += tl.size();
            _cpt_size += tl.fullsize();
         
            // all transitions in the list have been removed
            if (tl.empty())
            {
                i = _table.erase(i); // C++11
            }
            else
            {
                ++i;
            }
        }
    }
    
    // the elements removed are destroyed,
    // destructor of transition list of s is called
    // _table.erase(s);  // should not be present
    // initials.erase(s);
}


template<class T>
typename Tlist<T>::const_iterator
WFSM<T>::cbegin(state_t s) const
{
    typename std::map<state_t,Tlist<T>>::const_iterator it = _table.find(s);
    assert(it != _table.end());
    return it->second.cbegin();
}


template<class T>
typename Tlist<T>::const_iterator WFSM<T>::cend(state_t s) const
{
    typename std::map<state_t,Tlist<T>>::const_iterator it = _table.find(s);
    assert(it != _table.end());
    return it->second.cend();
}


template<class T>
size_t WFSM<T>::countStates() const
{
    return _table.size();
}


template<class T>
size_t WFSM<T>::countTransitions() const
{
    return _cpt_tr;
}


template<class T>
size_t WFSM<T>::countAll() const
{
    return _cpt_size;
}


// over approx.
// could be optimized
template<class T>
size_t WFSM<T>::resolution() const
{
    // start with copy of initial state set
    // new std::set<state_t>(initials);
    std::set<state_t>* from = new std::set<state_t>();
    from->insert(_initial);
    // initialy empty
    std::set<state_t>* reach = new std::set<state_t>();
    
    size_t res = 1;
    
    while (! from->empty())
    {
        size_t res1 = 1;
        // for all state in reached set
        for (std::set<state_t>::iterator is = from->begin();
             is != from->end(); ++is)
        {
            state_t s = *is;
            // for all transition headed by the current state
            for (typename Tlist<T>::const_iterator it = cbegin(s);
                 it != cend(s); ++it)
            {
                const T& t = *it;
                size_t a = t.arity();
                if (t.inner()) // exclude leaf transitions (to terminal symbol)
                {
                    res1 = Rational::lcm(res1, a);
                    // add states in the body of the transition to reach set
                    for (typename Production::Body::const_iterator i = t.cbegin();
                         i != t.cend(); i++)
                    {
                        reach->insert(i->state);
                    }
                }
            }
        }
        std::set<state_t>* aux = from;
        from = reach;
        reach = aux;
        reach->clear();
        res *= res1;
    }
    delete from;
    delete reach;
    return res;
}


template<class T>
size_t WFSM<T>::oftarget(state_t s) const
{
    typename std::map<state_t,Tlist<T>>::const_iterator it = _table.find(s);
//    assert(it != _table.end());
    return (it->second.size());
}


//Transition* WFSM<T>::at(state_t s, size_t i) const
//{
//    map<state_t,Tlist<T>>::const_iterator it = _table.find(s);
//    assert(it != _table.end());
//    assert(i < it->second.size());
//    return (it->second.at(i));
//}


template<class T>
std::set<state_t> WFSM<T>::allStates() const
{
    std::set<state_t> res;
    
    for (typename std::map<state_t, Tlist<T>>::const_iterator
         i = _table.cbegin();
         i != _table.end(); ++i)
    {
        
        res.insert(i->first);
        for (typename Tlist<T>::const_iterator it = (i->second).cbegin();
             it != (i->second).cend(); ++it)
        {
            const T& t = *it;
            if (t.inner())
            {
                for (typename Production::Body::const_iterator is = t.cbegin();
                     is != t.cend(); is++)
                {
                    res.insert(is->state);
                }
            }
            // if t not inner its body is empty
        }
    }
    
    return res;
}


template<class T>
std::set<state_t> WFSM<T>::emptyStates() const
{
    // start with the set of all states
    std::set<state_t> empty = allStates();
    
    bool change = true;
    while(change)
    {
        change = false;
        // for all state s
        for (typename std::map<state_t,Tlist<T>>::const_iterator
             i = _table.cbegin();
             i != _table.end(); ++i)
        {
            state_t s = i->first;
            // the state is already marked nonempty
            if (empty.count(s) == 0) continue;
            // otherwise try to mark s
            // for all edges headed by s
            for (typename Tlist<T>::const_iterator it = (i->second).cbegin();
                 it != (i->second).cend(); ++it)
            {
                const T& t = *it;
                // transition from a body of all nonempty states
                if ((t.terminal()) || (t.nonein(empty)))
                {
                    empty.erase(s); // in this case s is not empty
                    change = true;
                }
            }
        }
    }
    
    return empty;
}


template<class T>
bool WFSM<T>::isClean() const
{
    std::set<state_t> empty = emptyStates();
    
    return empty.empty();
}


template<class T>
void WFSM<T>::clean()
{
    // compute the set of empty states
    std::set<state_t> empty = emptyStates();

    // erase empty states
    for (state_t s : empty){
        remove(s);
        //    for (std::set<state_t>::iterator i = empty.begin();
        //         i != empty.end(); ++i)
        //        remove(*i);
    }
}


template<class T>
std::set<state_t> WFSM<T>::step(const std::set<state_t>& sin)
{
    std::set<state_t> sout; // empty set
    assert (sout.empty());
    // for all state in given set
    for (state_t s : sin)
        //for (set<state_t>::iterator is = sin.begin(); is != sin.end(); ++is)
    {
        //state_t s = *is;
        // for all transition headed by the current state
        for (typename Tlist<T>::const_iterator it = cbegin(s);
             it != cend(s); ++it)
        {
            const T& t = *it;
            if (t.inner()) // exclude leaf transition (to terminal symbol)
            {
                // for all state in the body of the transition
                for (typename T::const_iterator i = t.cbegin();
                     i != t.cend(); i++)
                    sout.insert(*i);
            }
        }
    }
    return sout;
}


template<class T>
void WFSM<T>::CountingtoStochastic()
{
    assert (hasWeightType("FloatWeight"));
    Weight newseed = ViterbiWeight::make_zero();
    
    // for all state
    for (typename std::map<state_t,Tlist<T>>::iterator i = _table.begin();
         i != _table.end(); ++i)
    {
        state_t s = i->first;
        Tlist<T>& tl = i->second;
        TRACE("cast Counting-Stochastic: state {} ({}  transitions).",
              s, tl.size());
        
        // TBC : float sum ?
        // Weight sum = _seed.zero(); // 0 of FloatWeights
        // assert (sum.hasType("FloatWeight"));
        double sum = 0;
        
        // compute sum of weights
        // weight of first transition
        for (typename Tlist<T>::iterator j = tl.begin(); j != tl.end(); ++j)
        {
            sum += j->weight().norm();
        }
        
        double d;

        //assert (sum.hasType("FloatWeight"));
        if (sum == 0) //(sum.zero())
        {
            WARN("cast Counting-Stochastic: state {}, sum=0", s);
            d = sum;
        }
        else
        {
            d = 1/sum; // sum.invert();
        }
        // float d = sum.norm();
        
        for (typename Tlist<T>::iterator j = tl.begin(); j != tl.end(); ++j)
        {
            j->setWeight(newseed.make((*j).weight().norm() * d));
        }
    }
    
    _seed = newseed;
    assert(_seed.hasType("ViterbiWeight"));
}


template<class T>
void WFSM<T>::CountingtoPenalty()
{
    CountingtoStochastic();
    StochastictoPenalty();
}


template<class T>
void WFSM<T>::PenaltytoCounting()
{
    assert (hasWeightType("TropicalWeight"));
    Weight newseed = FloatWeight::make_zero();
    
    // for all state
    for (typename std::map<state_t,Tlist<T>>::iterator i = _table.begin();
         i != _table.end(); ++i)
    {
        state_t s = i->first;
        Tlist<T>& tl = i->second;

        TRACE("cast Penalty-Counting: state {} ({}  transitions).",
              s, tl.size());
        
        // for all transition heading to state
        for (typename Tlist<T>::iterator j = tl.begin(); j != tl.end(); ++j)
        {
            float v = (*j).weight().norm();
            if (v == 0)
            {
                WARN("cast Penalty-Counting: state {}, tr. with weight=0", s);
                j->setWeight(newseed.zero());
            }
            else
            {
                j->setWeight(newseed.make(1/v));
            }
            
            assert (j->weight().hasType("FloatWeight"));
        }
    }
    _seed = newseed;
    assert(_seed.hasType("FloatWeight"));
}


template<class T>
void WFSM<T>::StochastictoPenalty()
{
    assert (hasWeightType("ViterbiWeight"));
    Weight newseed = TropicalWeight::make_zero();
    
    // for all state
    for (typename std::map<state_t,Tlist<T>>::iterator i = _table.begin();
         i != _table.end(); ++i)
    {
        state_t s = i->first;
        Tlist<T>& tl = i->second;
        
        TRACE("cast Stochastic-Penalty: state {} ({}  transitions).",
              s, tl.size());
        
        for (typename Tlist<T>::iterator j = tl.begin(); j != tl.end(); ++j)
        {
            float v = j->weight().norm();
            assert(0 <= v);
            assert(v <= 1);
            if (v == 0)
            {
                WARN("cast Stochastic-Penalty: state {}, tr. with weight=0", s);
                j->setWeight(newseed.zero());
            }
//            else if (v == 1)
//            {
//                j->setWeight(newseed.make(0.00000000000001));
//            }
            else
            {
                j->setWeight(newseed.make(- log10(v)));
            }
            
            assert (j->weight().hasType("TropicalWeight"));
        }
    }
    _seed = newseed;
    assert(_seed.hasType("TropicalWeight"));
}


template<class T>
bool WFSM<T>::hasWeightType(std::string code) const
{
    return _seed.hasType(code);
}


template<class T>
Weight WFSM<T>::weight_zero() const
{
    assert (! _seed.unknown());
    return _seed.zero();
}


template<class T>
Weight WFSM<T>::weight_one() const
{
    assert (! _seed.unknown());
    return _seed.one();
}


/// @}

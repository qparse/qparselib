//
//  Transition.hpp
//  squanty
//
//  Created by Florent Jacquemard on 28/05/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup schemata
/// @{

#ifndef Transition_hpp
#define Transition_hpp


#include <stdio.h>
#include <assert.h>
#include <vector>
#include <set>
#include <string>
#include <iostream>

#include "trace.hpp"
#include "State.hpp"
#include "Production.hpp"
#include "SymbLabel.hpp"
#include "Weight.hpp"



// poor man's definition of iterators
// typedef std::vector<state_t>::iterator Transition_iterator;
// typedef std::vector<state_t>::const_iterator Transition_const_iterator;


/// @brief extention of the class Production with a symbolic label.

/// The arity of the transition is the arity of the label.
///
/// The encoding of the body (list of antecedent states differs from Production):
/// - inner transition:
///   the body has length > 1
///   the arity is the length of the body
/// - terminal (leaf) transition:
///   the body has length 0
///   the symbolic label is a leaf label (arity 0).
///
/// @see SymbLabel for the functions managing these labels
class Transition : public Production
{
    template<typename> friend class WFSM;

public:
    
    /// transition with unknown weight, empty body and given label.
    /// @param a label of the new transition.
    Transition(label_t a = SymbLabel::make_dummy());
        
    /// Transition(w) creates a transition
    /// with weight a copy of w and empty body.
    /// @param a label of the new transition.
    /// @param w weight copied in new transition.
    /// @warning the letter weight in the envelop w is cloned.
    Transition(label_t a, const Weight& w);
    
    /// Transition(lw) creates a transition with weight a wrapper of lw.
    /// @param a label of the new transition.
    /// @param lw letter weight, wrapped into a Weight. must be non null.
    Transition(label_t a, LetterWeight* lw);
    
    ~Transition();
    
    virtual bool terminal() const;

    /// non-terminal production rule.
    virtual bool inner() const;
    
    /// dummy transition (created by empty constructor)
    virtual bool dummy() const;
        
    /// return the label of this transition.
    virtual label_t label() const {  return _label; }
           
    /// size of body = current number of children in the body of this production.
    /// @warning it may differ from the arity, when some children have a
    /// multiplicity larger than 1.
    virtual size_t size() const;
    
    /// out degree = number of children times children multiplicity.
    /// arity of the label when transition is complete.
    virtual size_t arity() const;
    
    // write content of body and weight to output stream
    //void dump(ostream&);
    
    /// write content of body and weight to output stream.
    friend std::ostream& operator<<(std::ostream&, const Transition&);
    
protected:
    
    label_t _label;
        
};

#endif /* Transition_hpp */

/// @}

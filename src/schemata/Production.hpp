//
//  Production.hpp
//  qparse
//
//  Created by Florent Jacquemard on 25/01/2018.
//  Copyright © 2018 Florent Jacquemard. All rights reserved.
//
/// @addtogroup schemata
/// @{


#ifndef Production_hpp
#define Production_hpp

#include <stdio.h>
#include <assert.h>
#include <vector>
#include <set>
#include <string>
#include <iostream>

#include "trace.hpp"
#include "Label.hpp"
#include "State.hpp"
#include "Weight.hpp"


// poor man's definition of iterators
// typedef std::vector<state_t>::iterator Transition_iterator;
// typedef std::vector<state_t>::const_iterator Transition_const_iterator;


/// @brief a Production is defined by a sequence of antecedent states (body)
/// the weight must be not null
/// (null weight means a missing production rule).
///
/// a production rule can be of two kinds:
/// - inner production rule:
///   the body has length > 1
///   the arity is the length of the body
/// - terminal (leaf) production rule:
///   the body has length 1 and contains a leaf label
///   the arity is zero
///
/// leaf label (terminal production rules):
/// number of note + grace notes at (left of) current node
///  0 = continuation
///  1 = 1 note | rest      (au + 1 note)
///  2 = 1 grace notes + 1 note
///  3 = 2 grace notes + 1 note
/// etc
/// @see Label for the functions managing these labels
class Production
{
    template<typename> friend class WFSM;
    
protected:
    
    struct BodyItem
    {
    public:
        BodyItem(state_t s, size_t u, size_t m=1);
        
        /// state of body item
        state_t state;

        /// multiplicity for body item
        size_t mult;

        /// cumulated sum of multiplicities
        size_t sum;
    };
    
public:
    
    // typedef Body::iterator iterator;
    // typedef Body::const_iterator const_iterator;
    // typedef std::vector<state_t> Body;
    // typedef std::vector<std::pair<state_t, size_t>> Body;
    typedef std::vector<BodyItem> Body;
    
    /// production rule with unknown weight and empty body.
    Production();
    
    // to call by either
    // Production(new XXXWeight()) where XXXWeight is class derived of Weight
    // Production(Weight()) for UNKNOWN Weight
    
    /// Production(w) creates a production rule with given weight and empty body.
    /// @param  w weight object copied in new production rule.
    /// @warning the letter weight in the envelop w is cloned.
    Production(const Weight& w);
    
    /// Production(lw) creates a production rule with given weight and empty body.
    /// @param lw letter weight, wrapped into a Weight but not cloned.
    /// must be non null.
    Production(LetterWeight* lw);
    
    // Production(v, w) creates a production rule
    // with weight a copy of w
    // and body a copy of the vector v.
    // @warning the letter weight in the envelop w is cloned.
    // @todo TBR
    // Production(std::vector<state_t>, const Weight&);
    
    // Production(v, lw) creates a production rule
    // with weight a wrapper of the letter lw (must be non null)
    // and body a copy of the vector v.
    // @todo TBR
    // Production(std::vector<state_t>, LetterWeight*);
    
    /// Production(s, w) creates a terminal production rule
    /// with weight a copy of w
    /// and body (of size 1) the singleton (s) (terminal symbol).
    /// @warning the letter weight in the envelop w is cloned.
    /// @todo only for v1 (ValueWCFG)
    Production(state_t, const Weight&);
    
    /// Production(s, lw) creates a terminal production rule
    /// with weight a wrapper of the letter lw (must be non null)
    /// and body (of size 1) the singleton (s) (terminal symbol).
    /// @todo TBR
    Production(state_t, LetterWeight*);
    
    //  @todo lesquels sont utilisés?
    
    ~Production();
    
    virtual bool terminal() const;

    /// non-terminal production rule.
    virtual bool inner() const;

    inline size_t id() const { return _id; }

    /// replace the id of this production by the given value.
    /// @warning we do not check that id is not taken.
    void setId(size_t id);

    /// non terminal symbol of this terminal production rule.
    /// @warning this production rule must be terminal
    virtual label_t label() const;
    
    /// weight of this production rule.
    inline Weight weight() const { return _weight; }

    /// change weight of this production rule.
    inline void setWeight(const Weight& w) { _weight = w; };  // copy
    
    /// modify weight of this production rule.
    void scalar(double);

    /// modify weight of this production rule.
    void invert();
    
    /// returns the state of given index in the body of this production rule.
    /// @param i index of state. must be between 0 and size - 1.
    /// @return the ith state in the body.
    state_t state(size_t i) const;

    /// the multiplicity for the given index of children in the body
    /// of this production rule.
    /// @param i index of children. must be between 0 and size - 1.
    /// @return the multiplicity of the children i in the body of this rule.
    size_t multiplicity(size_t i) const;
    
    /// sum of the multiplicities in the body of this production rule
    /// up to the given index (included).
    /// @param i index of children. must be between 0 and size - 1.
    /// @return the sum of multiplicities up to the children i in the body of this rule.
    size_t multiplicities(size_t i) const;

    /// the multiplicity values of elements of the body up to i (included) is 1.
    /// @param i index of children. must be between 0 and size - 1.
    inline bool nomult(size_t i) const { return (multiplicities(i) == i+1); }

    /// add given state at the end of the body of this production rule,
    /// with given multiplicity.
    void push(state_t s, size_t mult = 1);
    
    /// size of body = current number of children in the body of this production.
    /// @warning it may differ from the arity, when some children have a
    /// multiplicity larger than 1.
    virtual size_t size() const;
    
    /// out degree = number of children times children multiplicity.
    virtual size_t arity() const;
    
    /// constant iterator pointing to the first state in the body of the production. rule.
    Body::const_iterator cbegin() const { return _body.cbegin(); }
    
    /// constant iterator pointing to the end of the body of the production rule.
    Body::const_iterator cend() const { return _body.cend(); }
    
    /// whether the given state belongs to the body of this production rule.
    bool member(state_t) const;
    
    /// every state of the body is in the given set.
    bool allin(const std::set<state_t>&) const;
    
    /// no state of the body is in the given set.
    bool nonein(const std::set<state_t>&) const;
    
    // write content of body and weight to output stream
    //void dump(ostream&);
    
    /// write content of body and weight to output stream.
    friend std::ostream& operator<<(std::ostream&, const Production&);
    
protected:
    
    /// antecedents (source states) of the production (with resp. multiplicities)
    Body _body;
    
    /// weight attached to the production rule
    Weight _weight;

    /// 0 for terminal production and
    /// sum of the multiplicities of the states in body for inner
    size_t _arity;
    
    /// the id of production rule is created automatically.
    /// it reflects the order of creation.
    /// @todo set id as param of constructor
    size_t _id;
    

    
private:
    /// counter for the creation of production rule's ids.
    static size_t _cpt;
        
};



#endif /* Production_hpp */


/// @}

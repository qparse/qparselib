//
//  SymbLabel.hpp
//  qparse
//
//  Created by Florent Jacquemard on 04/10/16.
//  Copyright © 2016 Florent Jacquemard. All rights reserved.
//
/// @addtogroup schemata
/// @{


#ifndef SymbLabel_hpp
#define SymbLabel_hpp

#include <stdio.h>
#include <assert.h>
#include <vector>
#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream, std::stringbuf

#include "trace.hpp"
#include "Label.hpp"


/// type for symbols labeling Symbolic WTA transitions
///
/// the symbols of the following table are currently used
///
///   syntax | kind  | arity | specific1 | spec2     | semantic
///  :------:|:-----:|:-----:|:---------:|:---------:|:------------------------
///   R      | REST  | 0     | 1         | m >= 0    | rest event
///   E1     | STD   | 0     | 1         | m >= 0    | single event (note...)
///   En     | STD   | 0     | n > 1     | m >= 0    | n-1 g.n. + 1 event
///   E*     | STD   | 0     | UNDEF     | 0         | variable nb of g.n.
///   C0     | CONT  | 0     | 0         | m >= 0    | continuation
///   D1     | CONT  | 0     | 1         | m >= 0    | 1 dot
///   Dn     | CONT  | 0     | n > 1     | m >= 0    | n dots
///   D*     | CONT  | 0     | UNDEF     | 0         | variable nb of dots
///   M      | MULT  | 0     | 0         | m >= 0    | multi
///   Tn     | STD   | n > 1 | 1         | m >= 0    | beamed tuple
///   Un     | STD   | n > 1 | 0         | m >= 0    | unbeamed tuple
///   G      | GRACE | 2     | 0         | 0, 1      | grace note constructor
///   B2     | BAR   | 2     | 0         | 0, 1      | bar seq. constructor
///   B0     | BAR   | 0     | 0         | 0, 1      | bar seq. constructor
///   F      | FORK  | 2     | 0         | 0, 1      | fork constructor
///   _      | DUMMY | 0     | 1         | 0         | blank space
///   FAIL   | DUMMY | 0     | 2         | n         | parse fail (n points skip)
///   U      | DUMMY | 0     | 0         | 0         | other (undef)
///   ------------------------------------------------------------------------
///
///
/// the symbols are encoded as follows:
///
///  ```
///  |1|    9    |    9    |  5  |   8   |
///  |f|specific2|specific1|kind | arity |
///   flag: spec1 is undef
///  ```
///
/// - last 8 bits: arity
/// - next 5 bits: kind of symbol @see SymbLabel::Kind
/// - next 9 bits: specific info1
///   - number of grace notes + events for STD nullary
///   - number of dots for continuations (0 for tie)
///   - beamed or not for tuples - STD not nullary
///   - can also be UNDEF
/// - next 9 bits: specific info2 = multiplicity of labeled node in tree
///   = number of edges between the node and its parent
///   - 0 for root node
///   - 1 by default
///   - 2 and more for multiple edges (mult. duration)
/// - next 1 bit: UNDEF flag (for specific info1)
///
/// @deprecated replaced by UNDEF falues in info1
/// we consider the abstract domain [0,...,MAX_GRACE]
/// for the number of g.n. + event
/// the meaning is
/// - 0: tie (no event)
/// - 1: 1 note
/// - 2: 1 gn + 1 note
/// - ...
/// - MAX_GRACE: >= MAX_GRACE-1 gn + 1 note (appogiature) = all other cases
/// an abstract label is a label in abstract domain.
/// a concrete label is a positive integer.
/// an abstract label a is an abstraction of a concrete label b if
/// - either b <= MAX_GRACE and a = b
/// - or b > MAX_GRACE and a = MAX_GRACE.
///
/// @todo temporary representation, to be replaced by hierarchy of classes
// typedef unsigned long int label_t;
// max value for abstract pre and post
//const label_t MAX_GRACE = 3;
/// static functions for int encoding of symbols
class SymbLabel
{
public:
    
    enum Kind
    {
        /// inside-the-bar symbol
        /// if arity = 0: note or chord event (terminal symbol).
        /// if arity > 0: tuplet: regular division of inside-the-bar time interval.
        STD = 0,
        /// explicit rest symbol (terminal symbol).
        REST,
        /// continuation: tie or dots (terminal symbol).
        CONT,
        /// multiplicity: add 1 to the duration of previous sibling
        /// (terminal symbol)
        /// @warning must not label the first sibling.
        MULT,
        /// binary symbol for ornament with grace notes + event.
        /// left child = grace notes (duration 0), right child = event.s.
        GRACE,
        /// bar constructor (binary or constant symbol).
        /// division of multi-bar time interval into first bar and the rest.
        BAR,
        /// binary symbol for concurrency
        /// time interval duplication = voice split.
        FORK,
        /// space or fail or dummy symbol (undef)
        DUMMY
    };
    
    // construct a new symbol of kind inside-the-bar,
    // with specific info undefined.
    // @param ar arity must be in 0..255
    // TBR
    // static label_t make_inside(unsigned char ar);

    // construct a new symbol of kind inside-the-bar,
    // with given specific info.
    // @param ar arity must be in 0..255
    // TBR
    // static label_t make_inside(unsigned char ar, unsigned short int info);
        
    /// construct a new symbol for single event: note or chord.
    /// @param mult multiplicity (of labeled node in tree).
    /// arity is 0.
    static label_t make_note(unsigned short int mult=1);

    /// construct a new symbol for event (note or chord)
    /// with variadic (unknown) number of grace-notes.
    /// for specification in WTA.
    /// arity is 0. multiplicity is 0.
    static label_t make_varnote();

    /// construct a new symbol for grace-notes + note (or chord).
    /// @param nb total number of events = number of grace-notes + 1
    /// @param mult multiplicity (of labeled note in tree).
    /// arity is 0.
    static label_t make_appogiature(unsigned short int nb,
                                    unsigned short int mult=1);
    
    /// appogiature with undef number of grace notes (for WTA spec.)
    /// arity is 0.
    /// multiplicity is 0.
    static label_t make_appogiature();
    
    /// construct a new symbol of kind rest.
    /// @param mult multiplicity (of labeled note in tree)
    /// arity is 0.
    /// specific info is UNDEF
    static label_t make_rest(unsigned short int mult=1);

    /// construct a new symbol for tied note (or chord).
    /// @param mult multiplicity (of labeled note in tree).
    /// arity is 0.
    /// specific info is 0.
    static label_t make_tie(unsigned short int mult=1);

    /// construct a new symbol for multi
    /// (extention of duration of previous sibling)
    /// @param mult multiplicity (of labeled note in tree).
    /// arity is 0.
    /// specific info is 0.
    static label_t make_multi(unsigned short int mult=1);
    
    /// construct a new symbol of dot(s).
    /// @param nb number of dots. if 0, the symbol is a tie.
    /// arity is 0. specific info is 0. multiplicity is 1.
    static label_t make_dot(unsigned short int nb=1);
    
    /// construct a new symbol for an arbitrary nb of dots.
    /// for specification in WTA.
    /// arity is 0. multiplicity is 0.
    static label_t make_vardots();

    /// construct a new tuple symbol.
    /// @param ar arity must be in 1..255
    /// @param beamed whether the elements of tuple are beamed.
    /// @param mult multiplicity (of labeled note in tree).
    static label_t make_tuple(unsigned char ar, bool beamed=true,
                              unsigned short int mult=1);

    /// construct a new binary symbol of kind grace.
    /// @param mult multiplicity (of labeled note in tree). must be 0 or 1.
    /// arity will be 2. specific info will be 0.
    static label_t make_grace(unsigned short int mult=1);

    /// construct a new symbol of kind bar, with given arity.
    /// @param ar arity must be in 0..255
    /// @param mult multiplicity (of labeled note in tree). must be 0 or 1.
    /// specific info will be 0.
    static label_t make_bar(unsigned char ar,
                            unsigned short int mult=1);

    /// construct a new binary symbol of kind fork.
    /// @param mult multiplicity (of labeled note in tree). must be 0 or 1.
    /// arity will be 2.
    /// specific info will be 0.
    static label_t make_fork(unsigned short int mult=1);

    /// construct a new symbol of whitespace.
    /// arity will be 0.
    /// specific info will be 1. multiplicity will be 0.
    static label_t make_space();

    /// construct a new symbol of parse fail.
    /// arity will be 0.
    /// specific info will be 2. multiplicity will be 0.
    /// @param n the number of input points skip. stored in specific2.
    static label_t make_fail(size_t n);
    
    /// construct a new dummy symbol.
    /// arity will be 0.
    /// specific info will be 0. multiplicity will be 0.
    static label_t make_dummy();

    /// return a copy of a with multiplicity value set to m.
    /// @param a a label.
    /// @param m new value of multiplicity.
    static label_t set_multiplicity(label_t a, unsigned short int m);
    
    /// @return arity of label
    static size_t arity(label_t);

    /// leaf symbol (arity = 0).
    static bool terminal(label_t);

    /// label for inner node (arity > 0, whatever the kind)
    static bool inner(label_t);

    static bool binary(label_t);

    /// @return the incoming multiplicity (number expected of incoming edges)
    /// for the given label.
    /// @param a label
    static unsigned short int inmultiplicity(label_t a);

    /// @return the outgoing multiplicity (number expected of outgoing edges)
    /// for the given label and its ith descendant.
    /// @param a label
    /// @param i number of descendant. must be smaller than arity. first is 0.
    /// @todo STUB. TBR. (it is in the Transition)
    static unsigned short int outmultiplicity(label_t a, size_t i);

    /// inside-the-bar symbol with unknown specific value
    static bool undef(label_t);
    
    /// inside-a-voicesymbol:
    /// note, grace-note, continuation, dot, rest or tuple symbol,
    /// not a voice split nor bar separator nor dummy.
    static bool inside(label_t);

    /// symbol is a rest
    static bool rest(label_t);

    /// @return whether the given leaf symbol is a note,
    /// with or without grace notes, undef of not.
    static bool note(label_t);

    /// @return the number of note + grace notes encoded in given label.
    /// @param a label. if it is a note, it must not be undef.
    /// if a is a rest, return 1.
    /// if a is neither a note nor a rest, return 0.
    /// @todo add the case of rest preceeded by grace notes.
    static size_t nbEvents(label_t a);

    /// @return the number of grace notes encoded in given label.
    /// @param a label. if it is a note, it must not be undef.
    static size_t nbGraceNotes(label_t a);

    /// @return whether the given leaf label is a continuation.
    /// (inside-the-bar and arity 0 and specific info = 0).
    /// @param a label
    static bool continuation(label_t a);
    static bool tie(label_t a) { return continuation(a); }

    /// @return whether the given leaf label is a  multi
    /// (inside-the-bar and arity 0 and specific info = 0).
    /// @param a label
    static bool multi(label_t a);

    /// @return whether the given leaf label is a dot.s symbol.
    /// @param a label.
    /// may be UNDEF (unknown number of dots, symbol `D*`) or not (symbol `Dn`).
    static bool dot(label_t a);

    /// @return the number of dots encoded in this label.
    /// @param a label. must be a dot symbol and not UNDEF.
    static size_t nbDots(label_t a);

    /// @return whether the given leaf label is a tuple symbol.
    /// @param a label.
    static bool tuple(label_t a);
    
    /// @return whether the given tuple symbol is beamed.
    /// @param a label. must be a tuple symbol.
    static bool beamed(label_t a);
    
    /// symbol of bar slicing
    static bool bar(label_t a);

    /// symbol for addition of grace notes on left (binary symbol)
    static bool grace(label_t a);

    /// symbol for concurrency (time interval duplication)
    static bool fork(label_t a);

    /// whitespace symbol
    static bool space(label_t a);

    /// parse error symbol
    static bool fail(label_t a);

    /// dummy symbol (undef)
    static bool dummy(label_t a);
    
    /// copy label a and set in copy the specific info to given info
    /// @param a a label to copy.
    /// must be a note or a dot. specific info must be UNDEF.
    /// @param nb value for new specific info (nb of g.n.+1 or nb of dots).
    static label_t setNb(label_t a, unsigned short int nb);

    /// @return whether the given leaf label has an abstract value.
    /// @param a label must have kind inside-the-bar and arity 0.
    /// and defined specific info.
    /// @todo TBR deprecated.
    static bool abstract(label_t a);
    
    /// @return whether the first value is an abstraction of the second one.
    /// @param a label must have kind inside-the-bar and arity 0
    /// and defined specific info.
    /// @todo TBR deprecated.
    static bool abstract(label_t a, label_t n);

    /// @return whether exists c >= 0 such that abstract(a, n+c)
    /// @param a label must have kind inside-the-bar and arity 0
    /// and defined specific info.
    /// @todo TBR deprecated.
    static bool leqabstract(label_t a, label_t n);
    
    static std::string to_string(label_t a);
    
    static void print(label_t a, std::ostream& o);

private:

    static const size_t MASK_ARITY  =  0b00000000000000000000000011111111;
    static const size_t MASK_KIND   =  0b00000000000000000001111100000000;
    static const size_t MASK_INFO   =  0b00000000001111111110000000000000;
    static const size_t MASK_INFO2  =  0b01111111110000000000000000000000;
    static const size_t MASK_UNDEF  =  0b10000000000000000000000000000000;
 // static const size_t MASK_INFO  = ~0b1111111111111;
    
    /// 16 bits int on the >= 19 bits allowed for specific info
    /// i.e. we assume that the specific info in always < a 16 bits limit
 // static const size_t SPECIFIC_UNDEF = (size_t)std::numeric_limits<unsigned short>::max();

    /// construct a new symbol encoding
    /// @param ar arity must be in 0..255
    /// @param k a kind must be compatible with arity
    /// @param info specific information
    /// @param mult multiplicity (of labeled note in tree).
    static label_t make(unsigned char ar,
                        SymbLabel::Kind k,
                        unsigned short int info,
                        unsigned short int mult);

    /// construct a new symbol encoding
    /// with specific information undefined.
    /// @param ar arity must be in 0..255
    /// @param k a kind must be compatible with arity
    /// @param mult multiplicity (of labeled note in tree).
    static label_t make(unsigned char ar,
                        SymbLabel::Kind k,
                        unsigned short int mult);

    /// return the kind of the given label
    /// @param a label
    static SymbLabel::Kind kind(label_t a);
    
    /// return specific info in given label
    /// @param a label must not have undefined specific info
    static unsigned short int specific(label_t a);

    /// return second specific info in given label
    /// @param a label.
    static unsigned short int specific2(label_t a);

};


#endif /* SymbLabel_hpp */


/// @}

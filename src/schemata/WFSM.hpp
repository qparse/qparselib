//
//  WFSM.hpp
//  qparse V2
//
//  Created by Florent Jacquemard on 04/10/16.
//  Copyright © 2016 Florent Jacquemard. All rights reserved.
//
/// @defgroup schemata Schemata module
///
/// The `schemata` module contains classes of weighted tree automata
/// used for parsing.

/// @addtogroup schemata
/// @{


#ifndef WFSM_hpp
#define WFSM_hpp

#include <stdio.h>
#include <assert.h>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <math.h> 

#include "trace.hpp"
#include "Rational.hpp"
#include "Label.hpp"
#include "State.hpp"
#include "Tlist.hpp"
#include "Production.hpp"
#include "Weight.hpp"
#include "FloatWeight.hpp"
#include "ViterbiWeight.hpp"
#include "TropicalWeight.hpp"
#include "CountingWeight.hpp"



/// Generic template for Weighted Finite State Machines,
/// to be instanciated into weighted grammars or tree automata.
/// = hypergraph with nodes of stype state_t and
/// edges with body of type T (sequence of state_t) and head of type state_t.
/// It is a mapping associated to each registered state
/// a Tlist of bodies of type T.
/// Every Tlist and body of type T can be iterated.
///
/// The type T will be instanciated either by
/// grammar's production rules or automata's transitions.
/// 
/// Weights are concrete weight values embedded in a Weight envelop
/// we consider 3 kinds of weights for WFSM serialized in file:
/// - counting model: weight = # of subresettrees in corpus parsed by rule
///   implemented as FloatWeight
/// - penalty model: weight = penalities to sum
///   implemented as TropicalWeight
///   e.g. inverse of counting model (normalized?)
/// - probabilistic model, fulfilling stochastic condition
///   (sum of weight of transition from a state = 1)
///   implemented as ViterbiWeight
///   e.g. (# of subtrees in corpus parsed by rule) / (# of subtrees matching lhs state)
template <typename T>
class WFSM
{

public:
    /// nullary constructor for cython.
    WFSM();

    /// empty machine.
    WFSM(Weight seed);
    
    ~WFSM();
    
    virtual bool hasType(std::string code) const
    { return (code == "WFSM"); }
    
    /// @brief FSM size.
    /// @return the number of states of this FSM.
    size_t size() const;
    
    /// @brief the FSM has no state and no transition.
    bool empty() const;
    
    /// @brief the given state is present in the FSM.
    /// @param s a state.
    bool isRegistered(state_t s) const;
    
    /// @brief the given state is an initial state.
    /// @param s a state.
    /// @todo TBR
    bool isInitial(state_t s) const;
    
    /// initial state
    /// @return the initial state.
    virtual state_t initial() const { return _initial; }

    // pre, port removed:
    // they are use for quantification and ignored in this version
    // (useless for schemas)
    // virtual state_t initial(pre_t pre=0, pre_t post=0) const { return _initial; }

    /// rule of the given target state and id in the transition table
    /// of this WFSM.
    /// @param s target state of the rule
    /// @param id identifier of one rule in this WFSM.
    /// @return the rule of identifier id.
    /// @warning there must be one rule in this WFSM with given id
    /// (in particular the WFSM must not be empty).
    /// @todo correct only when id is set automatically
    const T& rule(state_t s, size_t id) const;
    
    /// @brief register the given state (potential head of transitions).
    /// @param s state to register.
    /// @param initial : flag. s is set as initial if it is true.
    /// @return if s was already registered,
    /// return a reference to its transition list.
    /// otherwise, create state s with an empty transition list
    /// and returns a reference to it.
    /// @todo suppr. flag initial
    Tlist<T>& add(state_t s, bool initial=false);
    
    /// @brief add a new transition.
    /// @param s head state for new transition. if s was not registered, it is registered.
    /// @param t body of new transition.
    /// @param initial : flag. state s is set as initial if it is true.
    /// The transition t is added to the transition list of s.
    /// @return a reference to the transition list of s.
    /// @todo suppr. flag initial
    Tlist<T>& add(state_t s,
                  const T& t,
                  bool initial=false);
    
//    template <class ...Args>
//    Tlist<T>& add_emplace(state_t s,
//                          Args&& ...args,
//                          bool initial=false);
    
    /// @brief remove the entry for given state s in the transition table.
    /// Transitions removed:
    /// - all transitions targeted to s,
    /// - all the transitions with s in their body.
    /// @param s state. must be registered.
    /// @warning if s was the initial state, it remains the (empty) initial state.
    /// if s was in the initial set, it is also removed from this set.
    void remove(state_t s);
    
    /// @return an constant iterator pointing to the first transition
    /// with head state s.
    /// @param s state. must be registered.
    /// @warning not for modifying transition list of s.
    /// use add(...) methods for this.
    typename Tlist<T>::const_iterator cbegin(state_t s) const;

    /// @return an constant iterator pointing to the past-the-end transition
    /// with head state s.
    /// @param s state. must be registered.
    /// @warning not for modifying transition list of s.
    /// use add(...) methods for this.
    typename Tlist<T>::const_iterator cend(state_t s) const;
    
    /// @return the number of transitions of head state s.
    /// @param s state. must be registered.
    size_t oftarget(state_t s) const;

    /// the WFSM has no empty states.
    bool isClean() const;
    
    /// @return the set of all unreachable states in FSM.
    std::set<state_t> emptyStates() const;
    
    /// @brief remove states not inhabited and transitions containing these states.
    void clean();
    
    /// @brief tatum defined by this FSM.
    size_t resolution() const;
    
    /// return wether the weights in transitions have the type of the code
    /// (code of the letter weight if there is one or "UNKNOWN" otherwise).
    bool hasWeightType(std::string code) const;
    
    /// weight value of the weight type for this WFSM.
    inline const Weight& seed() const { return _seed; }
    
    /// @brief additive neutral value of the weight semiring.
    /// @return the 0 value in the weight domain in this WFSM.
    Weight weight_zero() const;
    
    /// @brief multiplicative neutral value of the weight semiring.
    /// @return the 1 value in the weight domain in this WFSM.
    Weight weight_one() const;    
   
    /// for all state q,
    /// for all transition tr to q (in the transition list TL(q) of q).
    /// recompute weights to get a probabilistic WFSM.
    /// @param flag
    /// - with flag = 0, we assume the current WFSM is a penalty model.
    /// the probability is then obtained by dividing (Weight.scalar)
    /// the inverse of the norm (Weight.norm) of the weight of the tr
    /// by the sum of inverses of norms of transitions in TL(q).
    /// - with flag = 1, we assume the current WFSM is a counting model.
    /// the probability is then obtained by dividing (Weight.scalar)
    /// the norm (Weight.norm) of the weight of the tr
    /// by the sum of norms of transitions in TL(q).
    void normalize(unsigned int flag = 0);
    
    // cast and recompute all weights values in WFSM transitions
    // from one Weight model to another.
    
    /// @brief cast weights in all transitions.
    /// @warning this WFSM must have Weight Type "FloatWeight".
    /// this WFSM is casted into Weight Type "ViterbiWeight"
    /// divide by sum for target state
    void CountingtoStochastic();

    /// @brief cast weights in all transitions.
    /// @warning this WFSM must have Weight Type "TropicalWeight".
    /// this WFSM is casted into Weight Type "FloatWeight"
    /// inverse
    void PenaltytoCounting();

    /// @brief cast weights in all transitions.
    /// @warning this WFSM must have Weight Type "ViterbiWeight".
    /// this WFSM is casted into Weight Type "TropicalWeight"
    void StochastictoPenalty();
    
    /// @brief cast weights in all transitions.
    /// @warning this WFSM must have Weight Type "FloatWeight".
    /// this WFSM is casted into Weight Type "TropicalWeight"
    /// composition of CountingtoStochastic and StochastictoPenalty
    void CountingtoPenalty();
    
    /// @return number of states.
    size_t countStates() const;
    
    /// @return number of transition.
    size_t countTransitions() const;
    
    /// @return number of symbols (state occurences).
    size_t countAll() const;
    
    // set of initial states
    // @todo SUPPR
    // std::set<state_t> initials;
        

protected:

    /// transition table
    std::map<state_t, Tlist<T>> _table;

    state_t _initial;
    
    ///  arbitrary (polymorphic) weight value.
    /// for generation of weights in same domain with get_zero, get_one
    Weight _seed;
    
    /// number of transitions
    size_t _cpt_tr;
    
    /// full size (number of occurences of states)
    size_t _cpt_size;
    
    /// returns the set of all states occuring in machine (in head or body)
    std::set<state_t> allStates() const;
    
    /// recompte the identifiers of rules for a correct access via rule(i)
    void reindex();
    
private:
    
    /// counter for creating transition ids
    size_t _next_id;
    
    /// step(s) returns the set of states reachable in one transition step
    /// by this WFSM from the given state set s.
    /// all the states in the set s must be registered.
    std::set<state_t> step(const std::set<state_t>&);
    
};


// separated definition of template class
#include "WFSM.tpp"



#endif /* WFSM_hpp */


/// @}

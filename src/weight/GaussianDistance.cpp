//
//  GaussianDistance.cpp
//  qparse
//
//  Created by Florent Jacquemard on 21/01/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup weight
/// @{

#include "GaussianDistance.hpp"


const double GaussianDistance::sq2 = 1.41421356237;
const double GaussianDistance::sqpi = 1.772453850905516;

double GaussianDistance::mu = 0.0;
double GaussianDistance::sigma2 = Weight::CST_SIGMA2;
double GaussianDistance::sigma = sqrt(GaussianDistance::sigma2);

double GaussianDistance::a = -4.0;
double GaussianDistance::b = 4.0;
double GaussianDistance::Z = sq2 * sqpi * sigma * erf(b/(sq2 * sigma));
//const double Z = sq2 * sqpi * sigma * (erf(b/(sq2 * sigma)) - erf(a/(sq2 * sigma)));


GaussianDistance::GaussianDistance(const InputSegment* s,
                         const AlignedInterval* p):
ViterbiWeight(1)
{
    assert(s);
    assert(p);
    double lbound = p->rbegin();
    double rbound = p->rend();
    assert (s->rbegin() <= lbound);
    //assert (lbound <= s->rend());  // can be out
    assert (s->rbegin() <= rbound);
    //assert (rbound <= s->rend());
    assert (lbound <= rbound);
    
    double scale = p->rduration() / (b - a);
    
    // p.l_size() points in the left half of the interval
    size_t i = p->lfirst();
    for (size_t j = 0; j < p->lsize(); j++)
    {
        double x = s->point(i+j).rdate();
        assert (lbound <= x);
        assert (x < rbound);
        // positive  (lbound <= x <= center)
        _val *= trnorm((x - lbound)/scale);
    }
    
    // p.r_size() points in the right half of the interval
    i = p->rfirst();
    for (size_t j = 0; j < p->rsize(); j++)
    {
        double x = s->point(i+j).rdate();
        assert (lbound <= x);
        assert (x < rbound);
        // negative  (center < x < rbound)
        _val *= trnorm((x - rbound) / scale);
    }

    // DEBUG
    if (_val == 0.0)
    {
        ERROR("perfo weight=1 rt=[{} {}] mt=[{} {}] size={}",
              lbound, rbound,
              p->mbegin(), p->mend(), p->size());
    }

    assert(0 < _val);
    assert(_val <= 1);
}


GaussianDistance& GaussianDistance::operator= (const GaussianDistance& rhs)
{
    if (this != &rhs)
    {
        _val = rhs._val;
    }
    return *this;
}

GaussianDistance& GaussianDistance::operator= (const LetterWeight& rhs)
{
    if(this != &rhs)
    {
        const GaussianDistance* perf_rhs =
        dynamic_cast<const GaussianDistance*>(&rhs);
        
        assert (perf_rhs);
        _val = perf_rhs->_val;
    };
    return *this;
}

GaussianDistance* GaussianDistance::GaussianDistance::clone() const
{
    return (new GaussianDistance(_val));
}


void GaussianDistance::set_sigma2(double s)
{
    assert (s > 0);
    GaussianDistance::sigma2 = s;
    GaussianDistance::sigma = sqrt(GaussianDistance::sigma2);
}


double GaussianDistance::trnorm(double x)
{
    // exp(- (x/sigma)**2 / 2) / Z
    return exp(-pow(x/sigma,2)/2) / Z;
}


/// @}

## CountingWeight.cpp only for target schema

add_library(weight STATIC
  Weight.cpp
  WeightDom.cpp
  FloatWeight.cpp
  Distance.cpp
  GaussianDistance.cpp
  ViterbiWeight.cpp
  TropicalWeight.cpp
  CountingWeight.cpp
  PerformanceModel.cpp
)


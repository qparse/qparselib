//
//  GaussianDistance.hpp
//  qparse
//
//  Created by Florent Jacquemard on 21/01/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup weight
/// @{

#ifndef GaussianDistance_hpp
#define GaussianDistance_hpp

#include <stdio.h>
#include <assert.h>
#include <iostream>
#include <math.h>       /* erf */

#include "trace.hpp"
#include "Weight.hpp"
#include "Interval.hpp"
#include "AlignedInterval.hpp"
#include "ViterbiWeight.hpp"


/// @brief extention of ViterbiWeight with a model of performance.
///
/// compute probabilities of alignement of input points to a score
/// following a truncated Gaussian distribution
/// with parameters mu (default 0) and sigma (default 1)
/// and is truncated on the interval [a,b] (values in samples)
/// and shifted.
class GaussianDistance : public ViterbiWeight
{
public:

    GaussianDistance(double v):ViterbiWeight(v) {}

    /// @brief probability of positions in the given alignement
    /// in the interval defined by the given path.
    ///
    /// = product of the probabilities for the points in the alignement,
    /// + the pre points on the left bound
    /// - the post rightmost points in the right half of the alignment
    GaussianDistance(const InputSegment* s,
                     const AlignedInterval* p);
    
    GaussianDistance& operator= (const GaussianDistance&);
    
    /// @param rhs must be a GaussianDistance
    GaussianDistance& operator= (const LetterWeight& rhs);
    

    ~GaussianDistance() { }
    
    GaussianDistance* clone() const;
    
    inline virtual Weight make(double v) const
    { return Weight(new GaussianDistance(v)); }

    /// @warning type code is still "ViterbiWeight"
    virtual bool hasType(std::string code) const
    { return (code == "GaussianDistance"); }
    
    static void set_sigma2(double);
    
    friend std::ostream& operator<<(std::ostream& o,
                                    const GaussianDistance& rhs)
    {
        o << rhs._val;
        return o;
    }
    
protected:
    
    // @todo perf. weight separated from _val
    
private:
    
    static const double sq2;     // sqrt(2)
    static const double sqpi;    // = sqrt(pi)
    
    static double mu;
    static double sigma2;
    static double sigma;
    static double a;
    static double b;
    static double Z;
    
    double scale = 1;
    
    /// @brief Gaussian pr. density distribution truncated to [a,b].
    static double trnorm(double x);    

};







#endif /* GaussianDistance_hpp */

/// @}

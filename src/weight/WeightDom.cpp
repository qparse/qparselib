//
//  WeightDom.cpp
//  squanty
//
//  Created by Florent Jacquemard on 13/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include "WeightDom.hpp"


WeightDom CST_WEIGHT_TYPE = WeightDom::UNDEF;


std::ostream& operator<<(std::ostream& o, const WeightDom& t)
{
    switch (t)
    {
        case WeightDom::UNDEF:
            o << "UNDEF";
            break;
        case WeightDom::PENALTY:
            o << "PENALTY";
            break;
        case WeightDom::STOCHASTIC:
            o << "STOCHASTIC";
            break;
        case WeightDom::COUNTING:
            o << "COUNTING";
            break;
        default:
            o << "?UNKNOWN?";
            break;
    }
    
    
    return o;
}


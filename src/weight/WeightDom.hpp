//
//  WeightDom.hpp
//  squanty
//
//  Created by Florent Jacquemard on 13/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup weight
/// @{


#ifndef WeightDom_hpp
#define WeightDom_hpp

#include <stdio.h>
#include <iostream>


/// @brief weight types for weighted FSM
enum class WeightDom
{
    UNDEF,       /// to be specified
    PENALTY,     /// tropical semiring
    STOCHASTIC,  /// Viterbi semiring
    COUNTING     /// int vectors for corpus stat
};

/// @brief weight type.
/// value specified in grammar file
/// or default value WeightDom::UNDEF
extern WeightDom CST_WEIGHT_TYPE;

std::ostream& operator<<(std::ostream&, const WeightDom&);


#endif /* WeightDom_hpp */


/// @}

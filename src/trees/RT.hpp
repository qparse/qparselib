//
//  RT.hpp
//  qparse
//
//  Created by Florent Jacquemard on 16/11/16.
//  Copyright © 2016 Florent Jacquemard. All rights reserved.
//

/// @defgroup trees Trees module
///
/// The `output` module contains representations for the output
/// of the parsing procedure and conversion into music transcription results.

/// @addtogroup trees
/// @{


#ifndef RT_hpp
#define RT_hpp

#include <stdio.h>
#include <assert.h>
#include <vector>
#include <string>
#include <ctype.h>
#include <iostream>

#include "util.hpp"
#include "Label.hpp"
#include "TreePosition.hpp"



/// Rhythm Trees.
/// leaves are labeled, inner nodes are not labelled.
/// for the value of leaf labels,
/// @see Label.hpp
/// @see WCFG.hpp
class RhythmTree
{
public:

    /// empty inner tree (not terminal)
    /// @warning the child list must be completed with add
    RhythmTree();
    
    /// single leaf rhythm tree
    /// (terminal tree)
    RhythmTree(label_t lab);

    /// extract RT from string description
    RhythmTree(const std::string&);
    
    ~RhythmTree();

    /// single node tree (no subtrees).
    virtual bool terminal() const;

    /// this RT has subtrees).
    inline bool inner() const { return (! terminal()); }
    
    /// @return arity of root node (0 for terminal tree)
    /// It is the number of children the root is supposed to have.
    virtual size_t arity() const;

    /// @return the actual number of children.
    /// It may be smaller than the arity when the tree is in construction.
    inline size_t degree() const {  return (_children.size()); }

    /// @return whether the actual number of children is the arity.
    /// i.e. that the construction of the tree is finished.
    inline bool complete() const { return (degree() == arity()); }
    
    /// @return number of nodes.
    size_t size() const;

    /// @brief subtree.
    /// @param i index of child subtree, in 0..arity-1.
    /// @return the ith child of this tree (may be NULL).
    /// @warning this tree must be inner (not terminal)
    RhythmTree* child(size_t i) const;
    
    /// @brief label for terminal node
    /// @warning this tree must be terminal.
    virtual label_t label() const;

    /// @brief label of terminal node is a continuation.
    /// @warning this tree must be terminal
    virtual bool continuation() const;
    
    /// @brief label of terminal node is a single event
    /// (1 note / rest, no grace note).
    /// @warning this tree must be terminal
    virtual bool single_event() const;

    /// @brief number of grace notes in this terminal node.
    /// @warning this tree must be terminal.
    virtual size_t nbgn() const;
    
    /// @brief this tree contains a subtree of the form:
    /// @code p(n, o,...,o) @endcode or
    /// @code p(o,...,o) @endcode
    bool reducible() const;
    
    /// @brief add a subtree at the end of subtree list.
    /// @param s subtree to add. can be NULL.
    /// @warning this tree must not be terminal.
    /// @warning the current size of children list must be strictly smaller
    ///          than the arity of tree.
    virtual void add(RhythmTree* s);

    /// @brief conversion to LilyPond format.
    std::string lily(size_t depth, bool tie=false) const;
    
    /// @brief conversion to LilyPond format with dots.
    std::string lilydot(size_t depth);
    
    /// global variable set if a dot is added in lilydot.
    static bool dot_flag;
    
    /// format for Tree Edit Distance Salzburg library.
    std::string APTED() const;
    
    virtual void print(std::ostream& o) const;
    virtual std::string to_string() const;
    
    friend std::ostream& operator<<(std::ostream&, const RhythmTree&);
    
protected:
    /// for leaves, it is the positive integer stored in the leaf;
    /// for inner tree, it is a negative integer.
    ///
    /// long int : for comparison with state_t = size_t = unsigned long
    /// (=label of terminal Run)
    label_t _label;

    std::vector<RhythmTree*> _children;

    /// inner and the children list is of the form.
    /// @code (_, o,...,o) @endcode
    bool tail_redex() const;

    /// inner and one of the children 1..a is reducible.
    bool tail_reducible() const;

    // utilities for LilyPond convert
    
    /// return whether this tree is binary.
    bool binary() const;

    /// return whether this tree is a continuation (a leaf).
    bool tie() const;
    
    /// return whether the leftmost innermost leaf is a tie (continuation).
    bool tied() const;

    /// return whether this tree is binary and the second child is tied.
    bool second_tied() const;
    
    /// return whether this tree is binary
    /// and the left son is a dot (continuation after the dotted note).
    bool dot_after() const;

    /// return whether this tree is binary
    /// and the right son is a dot (continuation before the dotted note).
    bool dot_before() const;
    
    /// LilyPond format with dots.
    std::string lilydot(size_t depth,
                   bool tie,
                   bool dot,
                   bool ignore_first,
                   bool ignore_second);
    
private:
    
    /// @brief parse Rhythm Tree from string
    void read_tree(std::string::const_iterator& it,
                   const std::string::const_iterator& beg,
                   const std::string::const_iterator& end);
    
    /// @brief parse Rhythm Tree list from string
    void read_list(std::string::const_iterator& it,
                   const std::string::const_iterator& beg,
                   const std::string::const_iterator& end);
};


#endif /* RT_hpp */


/// @}

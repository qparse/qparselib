//
//  MRT.hpp
//  squanty
//
//  Created by Florent Jacquemard on 06/11/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup trees
/// @{



#ifndef MRT_hpp
#define MRT_hpp

#include <stdio.h>
#include <vector>
#include <string>
#include <ctype.h>
#include <iostream>
#include <assert.h>

#include "LRT.hpp"

/// extension of Labeled Rhythm Trees with multiplicity coefficients for edges.
/// i.e. DAGs with subtree sharing restricted to sibling positions.
/// @warning OBSOLETE the multiplicity is integrated to LRT.
class MultipleRhythmTree : public LabeledRhythmTree
{
public:
    /// @brief empty tree with
    /// - root label a dummy symbol @see SymbLabel::dummy()
    /// - empty children list
    /// @warning the root label is not terminal
    /// @warning the child list must be completed with add
    MultipleRhythmTree();
    
    /// @brief single rhythm tree with root labeled a and empty children list.
    /// @param a root label
    /// @warning when a not nullary,
    ///          the children list must be completed with add
    MultipleRhythmTree(label_t a);

    /// @brief construct a multiple rhythm tree from a
    ///        labeled rhythm tree by detecting mutiplicities.
    /// @param t a labeled rhythm tree to cast
    MultipleRhythmTree(const LabeledRhythmTree& t);

    /// @brief copy
    MultipleRhythmTree(const MultipleRhythmTree& t);

    ~MultipleRhythmTree();
    
    /// this tree is a terminal tree labeled by multi mark.
    virtual bool multi() const;
    
//    virtual void add(MultipleRhythmTree* s);
    
    virtual void add(MultipleRhythmTree* s, unsigned int m);
    
//    virtual void print(std::ostream& o) const;
//    virtual std::string to_string() const;
//
//    friend std::ostream& operator<<(std::ostream&, const MultipleRhythmTree&);
    
protected:
    /// @brief mutiplicity of the incoming edge.
    size_t _mult;

    // @brief vector of mutiplicities for the edges to children.
    // @warning the sum of multiplicities must be the arity
    //          of the root symbol.
    // std::vector<unsigned int> _mult;

    /// sum of multiplicities.
    unsigned int _sum;
};


#endif /* MRT_hpp */

/// @}

//
//  TreePosition.hpp
//  qparse
//
//  Created by Florent Jacquemard on 27/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup trees
/// @{


#ifndef TreePosition_hpp
#define TreePosition_hpp

#include <stdio.h>
#include <vector>
#include <string>
#include <ctype.h>
#include <iostream>
#include <assert.h>


/// @brief position in a RT.
/// = sequence of integers to reach position from root.
class Position
{
public:
    /// empty sequence = root position
    Position();
    
    /// copy
    Position(const Position&);
    
    /// root position
    bool empty() const;

    size_t length() const;
    
    /// @brief concatenate given int at the end of this position.
    /// @param i int must be positive.
    void operator+=(size_t i);
    
    friend std::ostream& operator<<(std::ostream& o, const Position& pos);

    /// @brief construct the concatenation of given int at the end
    /// of given position.
    /// @param p position.
    /// @param i int must be positive.
    /// @return a copy of p with concatenation of i.
    friend Position operator+(const Position& p, const size_t& i);

    
protected:
    
    std::vector<size_t> _content;
    
    void print(std::ostream& o) const;
};


#endif /* TreePosition_hpp */

/// @}

//
//  InputSegmentQuantize.hpp
//  qparse
//
//  Created by Florent Jacquemard on 04/02/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup output
/// @{


#ifndef InputSegmentQuantize_hpp
#define InputSegmentQuantize_hpp

#include <stdio.h>
#include <assert.h>

#include "InputSegment.hpp"


/// addition of quantized (MTU) duration to input segment
/// according to a parse table.
class InputSegmentQuantize : public InputSegment
{
public:
    
    InputSegmentQuantize(const InputSegment&);
    
    // @brief musical total duration (in bars) of segment.
    //
    // will return MTU::UNDEF if segment was not quantized.
    //inline mtu_t mduration() const { return _mduration; }

    // resolution for quantization = number of samples in each bloc
    // @todo TBR (useless here)
    //inline size_t resolution() const { return _res; }

    /// @brief quantization has been applied at least once.
    virtual bool quantized() const
    { return (Interval::mduration() != MTU::UNBOUNDED); }
    
    /// @brief set the musical time date and duration of events
    /// in this given input segment,
    /// according to the best run for p in given table.
    ///
    /// @warning ptr type P must have interval.
    /// @warning all the musical dates and durations of events in this segment will be changed.
    template<class P>
    void quantize(Atable<P>* table, const P& p);
    
    /// @brief set the musical time date and duration of events
    /// in this given input segment,
    /// according to the best run for p in given table,
    /// starting from point number b in interval.
    ///
    /// @return the next point of input segment with musical date
    /// and duration yet unset after processing p.
    /// @warning ptr type P must have interval.
    /// @warning all the musical date of events must be unknown in seg.
    /// @warning all the musical durations of events must be unknown in seg.
    ///
    /// @todo TBR (replaced by quantize)
    template<class P>
    size_t quantizu(Atable<P>* table, const P& p, size_t b = 0);
    
    
protected:
    // resolution for quantization
    // = number of samples in each bloc
    // @todo TBR (useless here)
    //size_t _res;
    
    // @brief length (in bars) of segment.
    //
    //  is set at quantization
    //mtu_t _mduration;
    
private:
    template<class P>
    size_t quantize_assert(Atable<P>* table, const P& p, size_t b);
    
};



// separated definition of template methods
// (they  cannot be defined in the .cpp)
#include "InputSegmentQuantize.tpp"



#endif /* InputSegmentQuantize_hpp */


/// @}

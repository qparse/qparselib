//
//  LRT.cpp
//  qparse
//
//  Created by Florent Jacquemard on 27/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include "LRT.hpp"


LabeledRhythmTree::LabeledRhythmTree():
_label(SymbLabel::make_dummy()),
_children(), // empty children list
_mult(1),
_sum(0)
{ }


LabeledRhythmTree::LabeledRhythmTree(label_t lab, size_t m):
_label(lab),
_children(), // empty children list
_mult(m),
_sum(0)
{ }


LabeledRhythmTree::LabeledRhythmTree(const RhythmTree* t)
{
    assert(t);
    ERROR("casting RT into LRT is not implemented");
}


LabeledRhythmTree::LabeledRhythmTree(const LabeledRhythmTree& t):
_label(t._label),
_children(t._children), // copy
_mult(t._mult),
_sum(t._sum)
{ }


LabeledRhythmTree::~LabeledRhythmTree()
{
    // recursive deallocation of children
    for (std::vector<LabeledRhythmTree*>::iterator i = _children.begin();
         i != _children.end(); ++i)
    {
        if (*i)
            delete *i;
    }
    _children.clear(); //destroy the content
}


size_t LabeledRhythmTree::arity() const
{
    return SymbLabel::arity(_label);
}


size_t LabeledRhythmTree::multiplicity() const
{
    return _mult;
}


bool LabeledRhythmTree::terminal() const
{
    return(SymbLabel::terminal(_label));
}


bool LabeledRhythmTree::tie() const
{
    return (SymbLabel::continuation(_label));
}


bool LabeledRhythmTree::multi() const
{
    return (SymbLabel::multi(_label));
}


bool LabeledRhythmTree::fail() const
{
    return (SymbLabel::fail(_label));
}


bool LabeledRhythmTree::dummy() const
{
    return (SymbLabel::dummy(_label));
}


bool LabeledRhythmTree::single_event() const
{
    return (SymbLabel::nbEvents(_label) == 1);
}


size_t LabeledRhythmTree::nbgn() const
{
    return (SymbLabel::nbGraceNotes(_label));
}


size_t LabeledRhythmTree::size() const
{
    if (this->terminal())
        return 1;
    else
    {
        size_t ret = 1;
        for (LabeledRhythmTree* t : _children)
        {
            if (t != NULL)
                ret += t->size();
        }
        return ret;
    }
}


LabeledRhythmTree* LabeledRhythmTree::child(size_t i) const
{
    assert (this->inner());
    assert (i < _children.size());
    
    return _children[i];
}


void LabeledRhythmTree::add(LabeledRhythmTree* t)
{
    assert (this->inner());
    assert(t);
    size_t m = t->multiplicity();
    assert(m >= 1);
    _children.push_back(t);
    _sum += m;
    assert (_sum <= arity());
}


void LabeledRhythmTree::add(LabeledRhythmTree* t, unsigned int m)
{
    assert (this->inner());
    assert(m >= 1);
    assert (_children.size() + m <= arity());

    _children.push_back(t);
    for (size_t i = 1; i < m; ++i)
        _children.push_back(new LabeledRhythmTree(SymbLabel::make_multi()));
}


void LabeledRhythmTree::multicast(LabeledRhythmTree* t)
{
    multicast(t, 0);
}


size_t LabeledRhythmTree::multicast(LabeledRhythmTree* t, size_t state)
{
    if (t == NULL)
        return state;
    else if (t->inner())
    {
        size_t newstate = 0;
        assert(SymbLabel::arity(t->label()) >= _children.size());
        for (size_t i = 0; i < _children.size(); ++i)
            newstate = multicast(_children[i], newstate);

        return 0; // reset to initial state after non-nullary sibling
    }
    else
    {
        assert(t->terminal());
        label_t a = t->_label;
        assert(! SymbLabel::undef(a));
        assert((state == 0) || (state == 1));
        
        if (state == 0)
        {
            if(SymbLabel::note(a) ||
               SymbLabel::rest(a) ||
               SymbLabel::multi(a) ||
               SymbLabel::continuation(a))
                return 1;
            else if (SymbLabel::dot(a) ||
                     SymbLabel::bar(a))
                return 0;
            else
            {
                ERROR("multicast: unexpected symbol {}", a);
                return 0;
            }
        }
        else if (state == 1)
        {
            if(SymbLabel::note(a) ||
               SymbLabel::rest(a) ||
               SymbLabel::multi(a))
                return 1;
            else if (SymbLabel::continuation(a))
            {
                // change emplace tie with multi
                t->_label = SymbLabel::make_multi();
                return 1;
            }
            else if (SymbLabel::dot(a) ||
                     SymbLabel::bar(a))
                return 0;
        }
        else
        {
            ERROR("multicast: unexpected state value {}", state);
            return 0;
        }
    }
    return 0; // should not be reached
}


void LabeledRhythmTree::dotcast(LabeledRhythmTree* t)
{
    dotcast(t, 0);
}


size_t LabeledRhythmTree::dotcast(LabeledRhythmTree* t, size_t state)
{
    if (t == NULL)
        return state; // ignore
    else     // TBC
        return state;
}

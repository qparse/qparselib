//
//  LRT.hpp
//  qparse
//
//  Created by Florent Jacquemard on 27/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
/// @addtogroup trees
/// @{


#ifndef LRT_hpp
#define LRT_hpp

#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <ctype.h>
#include <assert.h>

#include "trace.hpp"
#include "util.hpp"
#include "SymbLabel.hpp"
#include "TableA.hpp"
#include "TreePosition.hpp"
#include "RT.hpp"


/// Rhythm Trees with Labeled nodes.
/// leaves and inner nodes are labelled by SymbLabel's
/// @see SWTA.hpp
/// @warning DEPRECATED: the evolution of LRT is the Node object of ethe
/// engraving score model (current maintained).
class LabeledRhythmTree
{
    //friend class LRTFactory;
    
public:

    /// empty tree with
    /// - root label a dummy symbol @see SymbLabel::dummy()
    /// - empty children list
    /// @warning the root label is not terminal
    /// @warning the child list must be completed with add
    LabeledRhythmTree();
    
    /// single rhythm tree with root labeled a and empty children list
    /// @param a root label
    /// @warning when a not nullary,
    ///          the children list must be completed with add
    LabeledRhythmTree(label_t a, size_t mult = 1);
    
    /// cast a RT (labeled in Label) into a LRT (labeled in SymbLabel)
    /// @param t RT to be casted
    /// @todo STUB rm
    LabeledRhythmTree(const RhythmTree* t);
   
    LabeledRhythmTree(const LabeledRhythmTree& t);

    ~LabeledRhythmTree();
    
    /// @return arity of root node's label.
    /// It is the number of children the root is supposed to have, counting multiplcities.
    virtual size_t arity() const;

    /// @return multiplicity of the root node
    /// It is the number of incoming edges.
    virtual size_t multiplicity() const;

    /// @return the actual number of children.
    /// @warning it may be smaller than the arity when the tree is in construction,
    /// and/or when some children have multiplicity > 1;
    inline size_t nbc() const {  return (_children.size()); }

    // @return whether the actual number of children is the arity.
    // i.e. that the construction of the tree is finished.
    // @todo remove (not correct with multiplicities)
    // inline bool complete() const { return (nbc() == arity()); }
        
    /// @brief label of the root node of this tree.
    /// @warning It is a SymbLabel::SymbLabel
    inline label_t label() const { return _label; };
    
    /// tree of arity 0 (no subtrees).
    virtual bool terminal() const;
       
    /// this LRT must have subtrees.
    inline bool inner() const { return (! terminal()); }
    
    /// @brief label of root node is a continuation.
    /// @warning this tree must be terminal (single node).
    virtual bool tie() const;

    /// @brief label of root node is a multi.
    virtual bool multi() const;

    /// @brief label of root node is an parse fail symbol.
    virtual bool fail() const;

    /// @brief label of root node is a dummy symbol.
    virtual bool dummy() const;
    
    /// @brief label of root node is a single event
    /// (1 note / rest, no grace note).
    /// @warning this tree must be terminal (single node).
    virtual bool single_event() const;
    
    /// @brief number of grace notes in this root node.
    virtual size_t nbgn() const;

    /// @return number of nodes.
    size_t size() const;

    /// @brief subtree.
    /// @param i index of child subtree, in 0..arity-1.
    /// @return the ith child of this tree (may be NULL).
    /// @warning this tree must be complete
    /// @warning this tree must be inner (not terminal)
    LabeledRhythmTree* child(size_t i) const;
    
    /// @brief add the given subtree at the end of subtree list.
    /// @param s subtree to add. must not be NULL.
    /// @warning this tree must not be terminal.
    /// @warning the current size of children list must be smaller
    ///          than the arity of tree minus m.
    virtual void add(LabeledRhythmTree* s);
    
    /// @brief add given subtree and a number of multi constant symbols
    /// at the end of subtree list.
    /// @param s subtree to add. can be NULL.
    /// @param m multiplicity of the added subtree = number of constant symbols.
    ///        must be larger than or equal to 1.
    /// @warning this tree must not be terminal.
    /// @warning the current size of children list must be smaller
    ///          than the arity of tree minus m.
    virtual void add(LabeledRhythmTree* s, unsigned int m);
   
    /// @brief convert the given tree to tree with multiplicities.
    /// @param t tree to cast. can be NULL.
    /// casting will convert every sequence of sibling of the form
    /// (event tie ... tie) of maximal length into the sequence of sibling
    /// (event multi ... multi) of the same length.
    /// @warning the tree pointers remain the same (objects modified in place).
    virtual void multicast(LabeledRhythmTree* t);
    
    /// @brief convert the given tree to tree with dots.
    /// @param t tree to cast. can be NULL.
    /// @warning the tree pointers remain the same (objects modified in place).
    virtual void dotcast(LabeledRhythmTree* t);
    
    /// pretty-print this tree in the given output stream
    virtual void print(std::ostream& o) const;
    virtual std::string to_string() const;
  
    /// raw print this tree in the given output stream, 1 line per bar.
    virtual void write(std::ostream& o) const;
    
    /// write a raw serial representation of this tree in the given file
    size_t write(const std::string filename) const;

    friend std::ostream& operator<<(std::ostream&, const LabeledRhythmTree&);
    
protected:
    /// label of root node ot this tree.
    /// @see SymbLabel
    label_t _label;

    /// subtrees of this tree
    std::vector<LabeledRhythmTree*> _children;
    
    /// @brief mutiplicity of the incoming edge.
    size_t _mult;

    // @brief vector of mutiplicities for the edges to children.
    // @warning the sum of multiplicities must be the arity
    //          of the root symbol.
    // std::vector<unsigned int> _mult;

    /// sum of multiplicities of children.
    unsigned int _sum;
       
    /// stateful cast to tree with multi symbols
    /// @param t tree to cast. can be NULL.
    /// @param state a state value after processing previous sibling in a seq.
    /// @return state value after processing this tree as sibling in a seq.
    /// state 1: a tie symbol must be casted to multi
    ///          (after note or rest or tie or multi)
    /// state 0: a tie symbol must not be casted to multi
    ///          (after inner or dot or double bar)
    virtual size_t multicast(LabeledRhythmTree* t, size_t state);
    
    /// stateful cast to tree with dot symbols
    /// @param t tree to cast. can be NULL.
    /// @param state a state value after processing previous sibling in a seq.
    /// @return state value after processing this tree as sibling in a seq.
    virtual size_t dotcast(LabeledRhythmTree* t, size_t state);
    
    /// pretty-print representation of the top label of this tree.
    /// specific to Labeled tree display.
    void printLabel(std::ostream& o) const;
    /// string representation of a label corresponding to printLabel.
    std::string label_to_string() const;

    /// pretty-print this tree when it is a bar
    /// @param i bar number
    void printBar(std::ostream& o, size_t i) const;
    std::string bar_to_string(size_t i) const;

    /// print a raw representation of the top label of this tree.
    /// @warning top of this tree must not be a bar symbol
    void writeLabel(std::ostream& o) const;
    
    /// raw print this tree when it is a bar
    /// @param i bar number
    void writeBar(std::ostream& o, size_t i) const;

};


#endif /* LRT_hpp */

/// @}

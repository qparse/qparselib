//
//  TreePosition.cpp
//  qparse
//
//  Created by Florent Jacquemard on 27/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include "TreePosition.hpp"


// empty sequence = root position
Position::Position():
_content()
{ }


Position::Position(const Position& p):
_content(p._content)
{ }


bool Position::empty() const
{
    return _content.empty();
}


size_t Position::length() const
{
    return _content.size();
}


// concatenate given int to this position
// int must be positive
void Position::operator+=(size_t a)
{
    assert (a >= 0);
    _content.push_back(a);
}


void Position::print(std::ostream& o) const
{
    o << "[";
    size_t len = _content.size();
    
    for (size_t i = 0; i < len; i++)
    {
        o << _content[i] << ", ";
    }
    // last element
    if (len > 0)
    {
        o << _content[len-1];
    }
    o << "]";
}


std::ostream& operator<<(std::ostream& o, const Position& pos)
{
    pos.print(o);
    return o;
}


Position operator+(const Position& p, const size_t& i)
{
    Position p0(p); // copy
    p0 += i;
    return p0;
}

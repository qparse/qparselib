//
//  LRTFileOut.cpp
//  qparse
//
//  Created by Florent Jacquemard on 09/03/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup trees
/// @{

#include <stdio.h>

#include "LRT.hpp"


size_t LabeledRhythmTree::write(const std::string filename) const
{
    std::ofstream file;
    
    file.open(filename, std::ios_base::out);
    if(!file.is_open())
    {
        ERROR("LRT export : cannot open {} for writing", filename);
        return 1;
    }

    write(file);
    
    file.close();
    return 0;
}


void LabeledRhythmTree::write(std::ostream& o) const
{
    label_t a = label();

    if (SymbLabel::inside(a))
    {
        size_t ar   = arity();
        size_t mult = multiplicity();
        writeLabel(o);
        if (mult > 1)
            o << ":" << mult;
        if (ar > 1) // inner
        {
            o << "(";
            for (size_t i = 0; i < _children.size(); i++)
            {
                if (_children[i] == NULL)
                    o << "FAIL";
                else
                    _children[i]->write(o);

                if (i < ar-1)
                    o << ", ";
            }
            o << ")";
        }
    }
    else if (SymbLabel::bar(a))
    {
        writeBar(o, 0);
    }
}


void LabeledRhythmTree::writeLabel(std::ostream& os) const
{
    label_t a = label();
    
    if (SymbLabel::inside(a) &&
        SymbLabel::terminal(a) &&
        (! SymbLabel::undef(a)))
    {
        if (SymbLabel::note(a))
        {
            
            for (int i = 0; i < SymbLabel::nbGraceNotes(a); ++i)
                os << "g.";  // grace note
            os << "E1";      // note/chord event
        }
        else if (SymbLabel::rest(a))
        {
            os << "R";      // rest
        }
        else if (SymbLabel::continuation(a))
        {
            os << "C";      // continuation
        }
        else if (SymbLabel::dot(a))
        {
            for (int i = 0; i < SymbLabel::nbDots(a); ++i)
                os << "D";  // dot
        }
        else if (SymbLabel::multi(a))
        {
            os << "+";
        }
        else if (SymbLabel::fail(a))
        {
            os << "FAIL";
        }
        else
        {
            os << "?";
        }
    }
    else if (SymbLabel::tuple(a))
    {
        assert(SymbLabel::arity(a) > 0);
        assert(! SymbLabel::undef(a));
        if (SymbLabel::beamed(a))
            os << "B";
        else
            os << "U";
        os << SymbLabel::arity(a);  // tuple arity
    }
    else if (SymbLabel::grace(a))
    {
         assert(SymbLabel::arity(a) == 2);
         os << "G"; // grace note (composed)
     }
    else if (SymbLabel::bar(a))
    {
        ERROR("LRT.write: unexpected bar symbol");
    }
    else if (SymbLabel::fork(a))
    {
        assert(SymbLabel::arity(a) == 2);
        os << "f";
    }
    else if (SymbLabel::dummy(a))
    {
        os << "_";
    }
    else
    {
        ERROR("LRT.write: unexpected symbol");
        os << a;
    }
}


void LabeledRhythmTree::writeBar(std::ostream& o, size_t i) const
{
    label_t a = this->label();
    assert(SymbLabel::bar(a));

    if (SymbLabel::arity(a) == 0)
    {
        return; // final bar
    }

    assert(SymbLabel::arity(a) == 2);
    // print bar number
    o << (i+1) << ". ";

        // print bar content
        LabeledRhythmTree* bar =(LabeledRhythmTree*) child(0);
        if (bar == NULL)
        {
            o << "FAILED_BAR ";
        }
        else
        {
            bar->write(o); // write content of the bar
            o << " ";
        }
        // next bars
        LabeledRhythmTree* next =(LabeledRhythmTree*) child(1);
        o << std::endl;
        if (next == NULL)
        {
            o << "FAILED_BARS"; // we cannot process next bars
        }
        else
        {
            next->writeBar(o, i+1);
        }
}


/// @}

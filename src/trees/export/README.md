# src/trees/export

facilities for exporting the parse trees (rhythm trees) obtained from the parsing procedure into other structures:
- OpenMusic Rhythm Trees
  see http://support.ircam.fr/docs/om/om6-manual/co/RT.html
- [LilyPond](http://lilypond.org) code
- input tree for the Tree Edit Distance library APTED by M. Pawlik and N. Augsten 
  see http://tree-edit-distance.dbresearch.uni-salzburg.at


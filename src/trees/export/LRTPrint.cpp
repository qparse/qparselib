//
//  LRTPrint.cpp
//  qparse
//
//  Created by Florent Jacquemard on 09/03/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup trees
/// @{

#include <stdio.h>

#include "LRT.hpp"


void LabeledRhythmTree::print(std::ostream& o) const
{
    label_t a = label();
    // std::string res = "";

    if (SymbLabel::inside(a))
    {
        size_t ar   = arity();
        size_t mult = multiplicity();
        printLabel(o); // res = label_to_string(a);
        if (mult > 1)
            o << ":" << mult;
        if (ar > 1) // inner
        {
            o << "(";  // res += "(";
            for (size_t i = 0; i < _children.size(); i++)
            {
                if (_children[i] == NULL)
                    o << "NULL";  // res += "NULL";
                else
                    _children[i]->print(o); // res += _children[i]->to_string();

                if (i < ar-1)
                    o << ", ";  // res += ", ";
            }
            o << ")";  // res += ")";
        }
    }
    else if (SymbLabel::bar(a))
    {
        printBar(o, 0);  // res += bar_to_string(0);
    }
    // return res;
}


std::string LabeledRhythmTree::to_string() const
{
    std::ostringstream os;
    print(os);
    return os.str();
}


void LabeledRhythmTree::printLabel(std::ostream& os) const
{
    label_t a = label();
    
    if (SymbLabel::inside(a) &&
        SymbLabel::terminal(a) &&
        (! SymbLabel::undef(a)))
    {
        if (SymbLabel::note(a))
        {
            
            for (int i = 0; i < SymbLabel::nbGraceNotes(a); ++i)
                os << "❛";  // grace note
            os << "●";      // note/chord event
        }
        else if (SymbLabel::rest(a))
        {
            os << "○";      // rest
        }
        else if (SymbLabel::continuation(a))
        {
            os << "⏑";      // continuation
        }
        else if (SymbLabel::dot(a))
        {
            for (int i = 0; i < SymbLabel::nbDots(a); ++i)
                os << "・";  // dot
        }
        else if (SymbLabel::multi(a))
        {
            os << "+";      // continuation
        }
        else if (SymbLabel::fail(a))
        {
            os << "FAIL";
        }
        else
        {
            os << "?";
        }
    }
    else if (SymbLabel::tuple(a))
    {
        assert(SymbLabel::arity(a) > 0);
        assert(! SymbLabel::undef(a));
        os << SymbLabel::arity(a);  // tuple arity
        if (SymbLabel::beamed(a))
            os << "\u0305";         // overline (beamed tuple)
    }
    else if (SymbLabel::grace(a))
    {
         assert(SymbLabel::arity(a) == 2);
         os << "G"; // grace note (composed)
     }
    else if (SymbLabel::bar(a))
    {
         if (SymbLabel::arity(a) == 2)
         {
             os << "|";  // single bar
         }
         else if (SymbLabel::arity(a) == 0)
         {
             os << " 𝄂"; // double bar
         }
         else
             ERROR("unexpected bar symbol arity {}", SymbLabel::arity(a));
    }
    else if (SymbLabel::fork(a))
    {
        assert(SymbLabel::arity(a) == 2);
        os << "f";
    }
    else if (SymbLabel::dummy(a))
    {
        os << "☺︎";
    }
    else
    {
        os << a;
    }
}


std::string LabeledRhythmTree::label_to_string() const
{
    std::ostringstream os;
    printLabel(os);
    return os.str();
}


void LabeledRhythmTree::printBar(std::ostream& o, size_t i) const
{
    label_t a = this->label();
    assert(SymbLabel::bar(a));
    printLabel(o);

    if (SymbLabel::arity(a) == 0)
    {
        // return res; // final bar
    }
    else
    {
        assert(SymbLabel::arity(a) == 2);
        // print bar number
        o << "(" << (i+1) << ") ";
        // res += "(";
        // res += std::to_string(i+1);
        // res += ") ";

        // print bar content
        LabeledRhythmTree* bar =(LabeledRhythmTree*) this->child(0);
        if (bar == NULL)
        {
            o << "FAILED_BAR ";  // res += "FAILED_BAR ";
        }
        else
        {
            bar->print(o); // res += bar->to_string();
            o << " ";  // res += " ";
        }
        // next bars
        LabeledRhythmTree* next =(LabeledRhythmTree*) this->child(1);
        if (next == NULL)
        {
            o << "FAILED_BARS"; // we cannot process next bars
            // res += "FAILED_BARS";
        }
        else
        {
            next->printBar(o, i+1); // res += next->bar_to_string(i+1);
        }
    }
    // return res;
}


std::string LabeledRhythmTree::bar_to_string(size_t i) const
{
    std::ostringstream os;
    printBar(os, i);
    return os.str();
}


std::ostream& operator<<(std::ostream& o, const LabeledRhythmTree& t)
{
    t.print(o);
    return o;
}

/// @}

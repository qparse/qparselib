//
//  RTOutput.cpp
//  qparse
//
//  Created by Florent Jacquemard on 09/03/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup trees
/// @{


#include <stdio.h>

#include "RT.hpp"


void RhythmTree::print(std::ostream& o) const
{
    if (terminal())
    {
        label_t a = this->label();
        size_t g = Label::nbGraceNotes(a);
        assert (a >= 0);

        if (Label::continuation(a)) // tie
        {
            o << "-";
            
        }
        else if (g == 0)            // single note
        {
            o << "●";
        }
        else                        // graces notes + note
        {
            //return (patch::to_string(Label::nbGraceNotes(a)+1)+"●");
            //wchar_t g = '\xF8';
            //const char& g = string("○").at(0);
            //string gn(Label::nbGraceNotes(a), g); // "●";
            // std::string gn = "";
            for (int i = 0; i < Label::nbGraceNotes(a); i++)
            {
                o << "○"; // gn += "○";
            }
            o << "●";  // gn += "●";
            // o << gn;
        }
    }
    else // inner()
    {
        size_t a = this->arity();
        assert (a > 1);
        // std::string res = util::to_string(a);
        o << a;
        o << "(";  // res += "(";
        for (size_t i = 0; i < a; i++)
        {
            if (_children[i] == NULL)
            {
                o << "NULL";  // res += "NULL";
            }
            else
            {
                _children[i]->print(o); // res += _children[i]->to_string();
            }

            if (i < a-1)
            {
                o << ", "; // res += ", ";
            }
        }
        o << ")"; //res += ")";
        // o << res;
    }
}


std::string RhythmTree::to_string() const
{
    std::ostringstream os;
    this->print(os); // os << *(this);
    return os.str();
}


std::ostream& operator<<(std::ostream& o, const RhythmTree& t)
{
    t.print(o);
    return o;
}


/// @}


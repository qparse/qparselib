//
//  RTofStringIn.cpp
//  qparse
//
//  Created by Florent Jacquemard on 08/03/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//
/// @addtogroup trees
/// @{

#include <stdio.h>
#include <sstream>

#include "RT.hpp"
#include "trace.hpp"
#include "util.hpp"


#define RT_PAR_OPEN '('
#define RT_PAR_CLOSE ')'
#define RT_SEP ','   // not used


RhythmTree::RhythmTree(const std::string& s):
_label(-1),
_children()
{
    if (s.empty())
        ERROR("RhythmTree from empty string");

    //std::istringstream in(s);   //make a stream from the string description
    std::string::const_iterator it = s.begin();
    std::string::const_iterator end = s.end();
    assert (it != end);
    read_tree(it, it, end);
}


void RhythmTree::read_tree(std::string::const_iterator& it,
                           const std::string::const_iterator& beg,
                           const std::string::const_iterator& end)
{
    if (it == end)
    {
        ERROR("read_tree: parse error(EOS)");
        throw "error parsing tree";
    }
    else if (std::isspace(*it))
    {
        util::read_spaces(it, beg, end);
        read_tree(it, beg, end);
    }
    else if (std::isdigit(*it))
    {
        _label = util::read_int(it, beg, end);
        assert (_label >= 0);
    }
    else if (*it == RT_PAR_OPEN)
    {
        it++;
        read_list(it, beg, end);
        util::read_spaces(it, beg, end);
        if (*it == RT_PAR_CLOSE)
            it++;
        else
        {
            ERROR("read_tree: error at {} (par)", std::distance(beg, it));
            throw "error parsing tree";
        }
    }
    else
    {
        ERROR("read_tree: parse error at {} (unexpected symbol)",
              std::distance(beg, it));
        throw "error parsing tree";
    }
}


void RhythmTree::read_list(std::string::const_iterator& it,
                           const std::string::const_iterator& beg,
                           const std::string::const_iterator& end)
{
    if (it == end)
    {
        ERROR("read_list: parse error (EOS)");
        throw "error parsing tree list";
    }
    else if (std::isspace(*it))
    {
        util::read_spaces(it, beg, end);
        read_list(it, beg, end);
    }
    else if (std::isdigit(*it) || (*it == RT_PAR_OPEN))
    {
        RhythmTree* t = new RhythmTree();
        t->read_tree(it, beg, end);
        _children.push_back(t);
        read_list(it, beg, end);
    }
    else if (*it == RT_PAR_CLOSE)
    {
        // end
    }
    else
    {
        ERROR("read_list: parse error at {} (unexpected symbol)",
              std::distance(beg, it));
        throw "error parsing tree list";
    }
}

/// @}

//
//  LRTTableIn.hpp
//  qparse
//
//  Created by Florent Jacquemard on 15/01/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//
/// @addtogroup trees
/// @{


#ifndef LRTTableIn_hpp
#define LRTTableIn_hpp

#include <stdio.h>
#include <iostream>
#include <memory> // shared_ptr
#include <vector>
#include <string>
#include <ctype.h>
#include <assert.h>

#include "Runey.hpp"
#include "TableA.hpp"
#include "LRT.hpp"


/// construction of Labeled Rhythm Tree by
/// extraction of runs from a parse table,
/// and projection.
///
/// @remark we define these tree constructor functions in a factory
/// because it is impossible to define template constructors.
/// @see https://stackoverflow.com/questions/3960849/c-template-constructor

template<class K>
class LRTFactory
{
public:
    
    /// allocate a tree associated (by projection) to the n-best run of
    /// target k in the given parse table tab.
    /// @param tab a parse table indexed by keys of type K
    /// @param k key (index of row in table) can be partial or complete.
    /// @param n (as in n-best) is the rank of the run with target the key
    ///        of the record. must be larger or equal to 1 (default).
    /// @param unfailing if true, return a FAIL tree
    /// iff there is a FAIL subrun in the n-best run for k in tab
    /// (bottom-up FAIL propagation).
    /// otherwise the tree may contain FAIL subtrees.
    /// @return the tree associated to the best run. should never be NULL.
    /// A parse error is signaled by a FAIL symbol.
    /// @warning even when the returned tree is not FAIL,
    /// it may contain FAIL subtrees,
    /// when parsing partially failed e.g. for 1 bar (FAIL subrun).
    static LabeledRhythmTree* best(parsing::TableA<K>& tab,
                                   const K& k,
                                   size_t n=1,
                                   bool unfailing=false);

    /// cast the given run into a tree (by projection).
    /// @param tab table containing all the parse trees
    /// @param r run to cast
    /// @param m multiplicity (number of incoming edges) for the tree constructed.
    /// @param unfailing wether must fail in case of FAIL run.
    /// @return the tree associated to the best run. should never be NULL.
    /// A parse error is signaled by a FAIL symbol.
    /// @remark defined as factory because one cannot define template constructors.
    static LabeledRhythmTree* projection(parsing::TableA<K>& tab,
                                         std::shared_ptr<const parsing::Run> r,
                                         size_t m=1,
                                         bool unfailing=false);
    
};

// separated definition of template class members
#include "LRTTableIn.tpp"


#endif /* LRTTableIn_hpp */


/// @}

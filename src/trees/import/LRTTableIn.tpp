//
//  LRTTableIn.tpp
//  qparse
//
//  Created by Florent Jacquemard on 15/01/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//


template <class K>
LabeledRhythmTree*
LRTFactory<K>::best(parsing::TableA<K>& tab,
                    const K& k,
                    size_t n,
                    bool unfailing)
{
    assert(n >= 1);
    const parsing::Run* r = tab.best(k, n); // should not be NULL
    return projection(tab, r, 0, unfailing); // multiplicity is 0 at top
}


template <class K>
LabeledRhythmTree*
LRTFactory<K>::projection(parsing::TableA<K>& tab,
                          std::shared_ptr<const parsing::Run> r,
                          size_t mult,
                          bool unfailing)
{
    assert(r);

    if (r->fail())
    {
        WARN("LRT::projection: FAIL run (partial fail)");
        assert(SymbLabel::fail(r->label()));
        return new LabeledRhythmTree(r->label());
    }
    
    assert (r->complete());
    size_t ar = r->arity();  // out degree
    size_t nbc = r->size();  // expected number of children
    assert(nbc <= ar);
    // size_t mult = r->multiplicity();
    label_t a = r->label();

    LabeledRhythmTree* t = new LabeledRhythmTree(a, mult);
    
    if (r->terminal())
    {
        assert (ar == 0);
        return t;
    }

    assert (r->inner());
    assert (ar > 0);
    std::shared_ptr<const parsing::RunInner<K>> ir =
    std::dynamic_pointer_cast<const parsing::RunInner<K>,const parsing::Run>(r);
    assert(ir);
    
    // inner RhythmTree, initialy empty.
        
    for (size_t i = 0; i < r->size(); i++)
    {
        std::shared_ptr<const parsing::Run> ri = ir->subrun(i);
        assert(ri);
        const size_t mi = ir->multiplicity(i);

        if ((! ri->fail()) || (unfailing == false))
        {
            LabeledRhythmTree* ti = projection(tab, ri, mi, unfailing);
            assert(ti);
            assert((! ri->fail()) || ti->fail());
            if (unfailing == false)
            {
                t->add(ti);
                continue;
            }
        }

        // case of FAIL subrun ri and unfailing = true
        assert(ri->fail());
        assert(unfailing == true);
        std::shared_ptr<const parsing::RunTerm> tr =
        std::dynamic_pointer_cast<const parsing::RunTerm,const parsing::Run>(r);
        assert(tr);

        WARN("LRT::projection: NULL tree (unfailing mode)");
        delete t;
        // read number of input points to skip in the FAIL run
        return new LabeledRhythmTree(SymbLabel::make_fail(tr->length()));
    }
    assert(t);
    assert(t->fail() || (t->nbc() == nbc));
    return t;
}

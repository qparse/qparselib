//
//  RT.cpp
//  qparse
//
//  Created by Florent Jacquemard on 16/11/16.
//  Copyright © 2016 Florent Jacquemard. All rights reserved.
//
/// @addtogroup trees
/// @{


#include "RT.hpp"


RhythmTree::RhythmTree():
_label(-1),
_children()
{ }


RhythmTree::RhythmTree(label_t lab):
_label(lab),
_children()
{
    assert (lab >= 0);
}


RhythmTree::~RhythmTree()
{
    // recursive destruction
    for (std::vector<RhythmTree*>::iterator i = _children.begin();
         i != _children.end(); ++i)
    {
        if (*i != NULL)
            delete *i;
    }
    _children.clear(); //destroy the content
}


bool RhythmTree::terminal() const
{
//    return _children.empty();
    return (_label >= 0);
}


label_t RhythmTree::label() const
{
    assert (this->terminal());
    return _label;
}


bool RhythmTree::continuation() const
{
    assert (this->terminal());
    return Label::continuation(label());
}


bool RhythmTree::single_event() const
{
    assert (this->terminal());
    return (Label::nbEvents(label()) == 1);
}


size_t RhythmTree::nbgn() const
{
    assert (this->terminal());
    return (Label::nbGraceNotes(label()));
}


size_t RhythmTree::arity() const
{
    if (this->terminal())
        return 0;
    else
        return (_children.size());
}


size_t RhythmTree::size() const
{
    if (this->terminal())
        return 1;
    else
    {
        size_t ret = 1;
        for (RhythmTree* t : _children)
        {
            if(t != NULL)
                ret += t->size();
        }
        return ret;
    }
}


RhythmTree* RhythmTree::child(size_t i) const
{
    assert (this->inner());
    assert (i < _children.size());
    
    return _children[i];
}


void RhythmTree::add(RhythmTree* t)
{
    assert (this->inner());
    //assert (t);

    _children.push_back(t);
}


bool RhythmTree::reducible() const
{
    if (terminal())
        return false;
    else
    {
        assert (inner());
        assert (_children.size() > 0);
        RhythmTree* head = _children[0];
        if (head == NULL)
            return false;
        else if (head->terminal())
            return (tail_redex() || tail_reducible());
        else if (head->reducible())
            return true;
        else
            return tail_reducible();
    }
}


bool RhythmTree::tail_redex() const
{
    assert (inner());
    size_t a = this->arity();
    assert (a > 1);
    
    for (size_t i = 1; i < a; i++)
    {
        RhythmTree* sub = _children[i];
        if (sub == NULL)
            return false;
        else if (sub->terminal() && sub->continuation())
            continue;
        else
            return false;
    }
    return true;
}


bool RhythmTree::tail_reducible() const
{
    assert (inner());
    size_t a = this->arity();
    assert (a > 1);

    for (size_t i = 1; i < a; i++)
    {
        if (_children[i] == NULL)
            return false;
        else if (_children[i]->reducible())
            return true;
    }
    return false;
}


bool RhythmTree::tie() const
{
    return ((this->terminal()) && (this->continuation()));
}


bool RhythmTree::tied() const
{
    if (terminal())
    {
        return (continuation());
    }
    else // inner()
    {
        assert (_children.size() > 0);
        if (_children[0] == NULL)
            return false;
        else
            return (_children[0]->tied());
    }
}

bool RhythmTree::binary() const
{
    return ((this->inner()) && (this->arity() == 2));
}

bool RhythmTree::second_tied() const
{
    if (! this->binary())
        return false;
    else if (_children[1] == NULL)
        return false;
    else
        return (_children[1]->tied());
}

bool RhythmTree::dot_after() const
{
    if (! this->binary())
        return false;
    else if (_children[0] == NULL)
        return false;
    else
        return (_children[0]->tie());
}

bool RhythmTree::dot_before() const
{
    return second_tied();
}





/// @}

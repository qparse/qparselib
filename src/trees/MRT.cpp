//
//  MRT.cpp
//  squanty
//
//  Created by Florent Jacquemard on 06/11/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include "MRT.hpp"

MultipleRhythmTree::MultipleRhythmTree():
LabeledRhythmTree()
{ }


MultipleRhythmTree::MultipleRhythmTree(label_t a):
LabeledRhythmTree(a)
{ }


/// @todo STUB
MultipleRhythmTree::MultipleRhythmTree(const LabeledRhythmTree& t)
{
    
}


/// @todo STUB
MultipleRhythmTree::MultipleRhythmTree(const MultipleRhythmTree& t)
{
    
}


bool MultipleRhythmTree::multi() const
{
    return (SymbLabel::multi(_label));
}


void MultipleRhythmTree::add(MultipleRhythmTree* t, unsigned int m)
{
    assert (this->inner());
    assert(m >= 1);
    assert (_children.size() + m <= arity());

    _children.push_back(t);
    for (size_t i = 1; i < m; ++i)
        _children.push_back(new MultipleRhythmTree(SymbLabel::make_multi()));
}

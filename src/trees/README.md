# src/trees

intermediate structures for the representation of the output of the parsing procedure (parse trees) 
before conversion into music transcription results (i.e. music notation).



Note that the Score Model (`src/scoremodel`) also includes a tree-structured representation of the engraving content of a score.


#  modules

The `input` module contains utilities for reading from and writing to files the data given in input
to the quantization by parsing algorithm (`schema` and `segment`).



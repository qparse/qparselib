var searchData=
[
  ['weight',['Weight',['../classWeight.html#ac33fb795eaee8c56d2e4179020a0d2eb',1,'Weight::Weight(LetterWeight *w=NULL)'],['../group__weight.html#ga9283a1d2c4b91605a0c60a2c7033cb2c',1,'Weight::Weight(const Weight &amp;w)']]],
  ['weight_5fone',['weight_one',['../group__schemata.html#gabfe48e6398d83c03161b664dadcf8fcd',1,'WTA']]],
  ['weight_5fzero',['weight_zero',['../group__schemata.html#ga762b5bbee46468815993417ca9b3d9c6',1,'WTA']]],
  ['writeinfile',['writeInFile',['../group__output.html#ga7f0c47ab091313feb0a7fba4d23e09da',1,'MEI']]],
  ['wta',['WTA',['../group__schemata.html#ga5636da911ebbe8772c5444172d56800d',1,'WTA::WTA()'],['../group__schemata.html#ga6eeb789b8c8aa8404f611a6fed61faca',1,'WTA::WTA(Weight seed, pre_t pre=0, pre_t post=0)']]],
  ['wtafile',['WTAFile',['../classWTAFile.html#afc385786a0b36382ab366c91a8b9ce0b',1,'WTAFile::WTAFile()'],['../classWTAFile.html#a0c65f030a8df8bdb8eed5c261908e6bb',1,'WTAFile::WTAFile(const std::string filename, WeightDom wt=WeightDom::UNDEF, pre_t pre=0, pre_t post=0)'],['../classWTAFile.html#a1d76575338801558fc75759aa90261b9',1,'WTAFile::WTAFile(const std::string filename, bool count_flag=false, bool penalty_flag=true, bool stochastic_flag=false)']]]
];

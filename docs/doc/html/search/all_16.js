var searchData=
[
  ['weight',['Weight',['../classWeight.html',1,'Weight'],['../classRun.html#a61e6a26fe0ae42b0e4d54babc8c46684',1,'Run::weight()'],['../classWeight.html#ac33fb795eaee8c56d2e4179020a0d2eb',1,'Weight::Weight(LetterWeight *w=NULL)'],['../group__weight.html#ga9283a1d2c4b91605a0c60a2c7033cb2c',1,'Weight::Weight(const Weight &amp;w)'],['../group__weight.html',1,'(Global Namespace)']]],
  ['weight_5fone',['weight_one',['../group__schemata.html#gabfe48e6398d83c03161b664dadcf8fcd',1,'WTA']]],
  ['weight_5fzero',['weight_zero',['../group__schemata.html#ga762b5bbee46468815993417ca9b3d9c6',1,'WTA']]],
  ['weightdom',['WeightDom',['../group__general.html#gae07bdce6f4896785ead3b815ca6ab560',1,'init.hpp']]],
  ['weightmax',['weightMax',['../group__table.html#ga27170c8c3df71e6854ca3becc034ea09',1,'Rune.hpp']]],
  ['weightmin',['weightMin',['../group__table.html#gac5d18d8c7a8e65bf58d4bc2446202f4f',1,'Rune.hpp']]],
  ['writeinfile',['writeInFile',['../group__output.html#ga7f0c47ab091313feb0a7fba4d23e09da',1,'MEI']]],
  ['wta',['WTA',['../classWTA.html',1,'WTA'],['../group__schemata.html#ga5636da911ebbe8772c5444172d56800d',1,'WTA::WTA()'],['../group__schemata.html#ga6eeb789b8c8aa8404f611a6fed61faca',1,'WTA::WTA(Weight seed, pre_t pre=0, pre_t post=0)']]],
  ['wtafile',['WTAFile',['../classWTAFile.html',1,'WTAFile'],['../classWTAFile.html#afc385786a0b36382ab366c91a8b9ce0b',1,'WTAFile::WTAFile()'],['../classWTAFile.html#a0c65f030a8df8bdb8eed5c261908e6bb',1,'WTAFile::WTAFile(const std::string filename, WeightDom wt=WeightDom::UNDEF, pre_t pre=0, pre_t post=0)'],['../classWTAFile.html#a1d76575338801558fc75759aa90261b9',1,'WTAFile::WTAFile(const std::string filename, bool count_flag=false, bool penalty_flag=true, bool stochastic_flag=false)']]]
];

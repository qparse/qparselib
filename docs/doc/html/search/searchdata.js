var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuvwz~",
  1: "abcdefhiklmnopqrstvw",
  2: "ps",
  3: "abcdefghilmnopqrstuvwz~",
  4: "_acdehilmnopqrstw",
  5: "lop",
  6: "w",
  7: "cps",
  8: "o",
  9: "giostw",
  10: "st"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "related",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Friends",
  9: "Modules",
  10: "Pages"
};


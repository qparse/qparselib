qparse \
\

src/general {#md_src_general_README}
===========

src/intput {#md_src_input_README}
==========

src/output {#md_src_output_README}
==========

src/parsers {#md_src_parsers_README}
===========

sources of the qparse library {#md_src_README}
=============================

src/schemata {#md_src_schemata_README}
============

src/scoremodel {#md_src_scoremodel_README}
==============

src/segment {#md_src_segment_README}
===========

src/table {#md_src_table_README}
=========

/src/targets {#md_src_targets_README}
============

src/weight {#md_src_weight_README}
==========

Todo List {#todo}
=========

Module Documentation
====================

Namespace Documentation
=======================

Class Documentation
===================

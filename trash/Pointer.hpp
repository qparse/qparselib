//
//  Pointer.hpp
//  squant
//
//  Created by Florent Jacquemard on 19/12/2017.
//  Copyright © 2017 Florent Jacquemard. All rights reserved.
//
// generic class of pointer to best runs
//
// there are two kind of pointers
// - state pointer : points to a state of a WTA
// - bar pointer : points to a solution for the n first bars in an input segment
//   = a sequence of n solutions pointing to the initial state of WTA.

#ifndef Pointer_hpp
#define Pointer_hpp

#include <stdio.h>
#include <assert.h>

#include "Weight.hpp"
#include "State.hpp"
#include "Table.hpp"
#include "Rune.hpp"


// P = concrete pointer class
// H = PointerHasher
template <class P, class H>
class Pointer
{
public:
    
    //////////////////////
    //// constructors ////
    //////////////////////

    Pointer(State s):_state(s) { }
    
    Pointer(const Pointer& p):_state(p._state) { }

    // every concrete subclass implements the following constructors
    //
    // - sub-pointer:
    //   P(const &P p, size_t a, size_t i, State s)
    //   pointer for the ith children of p, for a division in a children
    //
    // - head, tail
    //   P(const &P p, State s)
    //   component of a meta transition heading to p,
    //   with given state s.
    //   if s is a wta state, it is the first component.
    //   if s is a meta state, it is the second component.
    //   p must be a pointer to a meta-state.
    //
    // - copy
    //   P(const &P p)
    //
    // - next sibling
    //   P(const &P p1, const &P p2)
    //   where P1 is complete, P2 is partial, P1 and P2 are successive siblings
    //   construct a copy of P2 instanciated as the next sibling of P1.
    //
    // and
    // virtual bool operator==(const P&) const;
    // for using pointer as key in hash table

    
    virtual P& operator= (const P&) = 0;
    
    virtual bool operator==(const P&) const;

    
    //////////////////////////////////////////////////////////
    //// specific methods - defined in descendant classes ////
    //////////////////////////////////////////////////////////

    // the pointer is complete
    virtual bool complete() const = 0;

    // return wether it is worth descending (dividing) from this pointer
    // for instance it is not worth when the pointer corresponds to
    // an empty input sub-segment.
    // default true.
    virtual bool divisible() const { return true; }
    
    // instanciate this pointer (if incomplete) with content
    // of the given ptr.
    // default do nothing.
    // TBC USEFUL?
    virtual void instanciate(const P&) { }
    
    // instanciate this pointer (if incomplete)
    // as the next sibling of the given ptr.
    // default do nothing.
    virtual void instanciate_next(const P&) { }

    // instanciate this pointer (if incomplete)
    // as it is a leaf.
    // default do nothing.
    virtual void instanciate_terminal() { }

    // instanciate this pointer (if incomplete)
    // according to the sub-pointers in the given Run.
    // the given Run must be inner and complete.
    // default do nothing.
    virtual void instanciate_inner(const Run<P>*) { }

    // return wether this pointer is compatible with a
    // terminal transition of given label.
    // default true.
    virtual bool compatible(const State) { return true; }

    // return the weight for a terminal Run associated to the given Transition.
    // return weight 0 if this pointer is incompatible with the Transition.
    // The transition must be terminal.
    virtual Weight terminal_weight(const Transition&) = 0;

    
    //////////////////////////////////////////////////////
    //// generic methods - for all descendant classes ////
    //////////////////////////////////////////////////////

    // compute possible instances of this pointer
    // and store them in the given table.
    // Completion is performed by creatig runs heading to the complete
    // instances of this pointer, according to the transitions of the given WTA.
    // the complete runs are also stored in the table.
    // -> MV Table.add
    // void update(Atable<P, H>*, const WTA&);
    
    // compute possible instances the given run heading to
    // instances of this pointer
    // and store instances of runs and pointers in the given table.
    // -> MV Table.add
    // void update(Atable<P, H>*, const Run<P>*);
    
    
protected:
    // every pointer has an internal state
    // if >=0, it is a WTA State
    // if < 0, it is a number of bars processed
    State _state;
    
    // allocators
    // pure virtual methods to be defined in P
    // TBR: redefined as constructors
    
    // sub(a, i)
    // create a new pointer for the ith children amongst a of this pointer
    //virtual P sub(size_t, size_t, State) const = 0;

    // return a new pointer
    // for the first component of a meta transition
    // heading to this pointer
    // with given state
    //virtual P bar_head(State) const = 0;

    // return a new pointer
    // for the second component of a meta transition
    // heading to this pointer
    //virtual P bar_tail() const = 0;
    
    // succ_inst(p)
    // instanciate this pointer as the successor of p
    //virtual P succ_inst(const P&) const = 0;

};


// should not be called
template <class P, class H>
bool Pointer<P,H>::operator==(const P& p) const
{
    assert (false);
    return (p._state == _state);
}


#endif /* Pointer_hpp */

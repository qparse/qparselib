#!/bin/bash

declare PATH_TO_QPARSE=.
declare PATH_TO_EXEC=$PATH_TO_QPARSE/Build/Products/Debug
declare PATH_TO_SRC=$PATH_TO_QPARSE/test/GTests

export DYLD_FRAMEWORK_PATH=$DYLD_FRAMEWORK_PATH:$HOME/Code/googletest//

echo Starting Unit Tests of $PATH_TO_SRC
$PATH_TO_EXEC/GTests

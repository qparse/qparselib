//
//  TestTableParse.cpp
//  GTests
//
//  Created by Florent Jacquemard on 14/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include <stdio.h>
#include "gtest/gtest.h"

#include "TropicalWeight.hpp"
#include "SymbLabel.hpp"
#include "KeyS.hpp"
#include "Runey.hpp"
#include "ParsingEnv.hpp"
#include "RecordOne.hpp"
#include "TableParse.hpp"

using namespace parsing;

TEST(TestTableParse, TestEmpty)
{
    ParsingEnv* env = new ParsingEnv(); // empty, not used here

    TableParse<KeyS, RecordOne<KeyS>, KeySHasher>* tab0 =
    new TableParse<KeyS, RecordOne<KeyS>, KeySHasher>(*env);

    EXPECT_TRUE(tab0->empty());
    delete tab0;
    delete env;
}


//
//  TestRecordQueue.cpp
//  GTests
//
//  Created by Florent Jacquemard on 12/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include <stdio.h>
#include "gtest/gtest.h"

#include "TropicalWeight.hpp"
#include "SymbLabel.hpp"
#include "KeyS.hpp"
#include "ParsingEnv.hpp"
#include "RecordQueue.hpp"

using namespace parsing;

TEST(TestRecordQueue, TestEmpty)
{
    RecordQueue<KeyS> rec0 = RecordQueue<KeyS>(weightMax<KeyS>);
    EXPECT_TRUE(rec0.empty());
    state_t q0 = 0;
    ParsingEnv env = ParsingEnv(); // empty, not used here
    KeyS* k0 = new KeyS(q0, env);
    rec0.initKey(k0);
    EXPECT_TRUE(rec0.key() == k0);
    EXPECT_TRUE(rec0.best(1) == NULL);
    
    delete k0;
}

// TBR
TEST(TestRecordQueue, TestKey)
{
    state_t q0 = 0;
    ParsingEnv env = ParsingEnv(); // empty, not used here
    KeyS* k0 = new KeyS(q0, env);
    RecordQueue<KeyS> rec0 = RecordQueue<KeyS>(weightMax<KeyS>);
    rec0.initKey(k0);
    EXPECT_TRUE(rec0.empty());
    EXPECT_EQ(rec0.key(), k0);
    EXPECT_TRUE(rec0.best(1) == NULL);
    
    delete k0;
}


TEST(TestRecordQueue, TestSingle)
{
    RecordQueue<KeyS> rec0 = RecordQueue<KeyS>(weightMax<KeyS>);
    state_t q0 = 0;
    ParsingEnv env = ParsingEnv(); // empty, not used here
    KeyS* k0 = new KeyS(q0, env);
    rec0.initKey(k0);
    EXPECT_EQ(rec0.key(), k0);
    EXPECT_TRUE(rec0.empty());
    
    // nullary Run
    const label_t a  = SymbLabel::make_note();
    const Weight w = TropicalWeight::make_one().make(0.25);
    const Transition t = Transition(a, w);
    parsing::Run<KeyS>* r0 =
        new parsing::Run<KeyS>(t, a, TropicalWeight::make_one().make(0.25));
    rec0.add(r0);
    EXPECT_FALSE(rec0.empty());
    EXPECT_EQ(*(rec0.best(1)), *r0);
    
    // delete r0; // deleted with rec0
    delete k0;
}


// rec1 (key k1): r01 = a|0.25
// rec2 (key k2): r02 = a|0.75
// rec0 (key k0): r2  = b(r01,r02) | 8+0.25+0.75
TEST(TestRecordQueue, TestThreeMin)
{
    ParsingEnv env = ParsingEnv(); // empty, not used here
    
    // nullary Run r01 = a()|0.25 in record rec1[k1]
    const label_t a  = SymbLabel::make_note();
    const Weight w = TropicalWeight::make_one().make(0.25);
    const Transition t = Transition(a, w);
    const parsing::Run<KeyS>* r01 =
        new parsing::Run<KeyS>(t, a, TropicalWeight::make_one().make(0.25));
    state_t q1 = 1;
    KeyS* k1 = new KeyS(q1, env);
    RecordQueue<KeyS>* rec1 = new RecordQueue<KeyS>(weightMax<KeyS>);
    rec1->initKey(k1);
    rec1->add(r01);
    
    // nullary Run r02 = c()|0.75 in record rec2[k2]
    const label_t c  = SymbLabel::make_appogiature(2);
    const Weight w1 = TropicalWeight::make_one().make(0.75);
    const Transition t1 = Transition(c, w1);

    state_t q2 = 2;
    KeyS* k2 = new KeyS(q2, env);
    const parsing::Run<KeyS>* r02 =
        new parsing::Run<KeyS>(t1, c, TropicalWeight::make_one().make(0.75));
    RecordQueue<KeyS>* rec2 = new RecordQueue<KeyS>(weightMax<KeyS>);
    rec2->initKey(k2);
    rec2->add(r02);
    
    // binary run r2 = b(r01, r02)|9 in rec0
    const label_t b  = SymbLabel::make_tuple(2);
    // const Weight w0 = w1.make(1);
    Weight w2 = w1.make(8);
    const Transition t2 = Transition(b, w2);
    const parsing::Run<KeyS> r20 = parsing::Run<KeyS>(t2, b, w2);
    const parsing::Run<KeyS> r21 = parsing::Run<KeyS>(r20, rec1, r01); // copy/update
    w2 *= r01->weight();
    const parsing::Run<KeyS>* r2 = new parsing::Run<KeyS>(r21, rec2, r02); // 2d copy/update
    w2 *= r02->weight();
    state_t q0 = 0;
    KeyS* k0 = new KeyS(q0, env);
    RecordQueue<KeyS>* rec0 = new RecordQueue<KeyS>(weightMax<KeyS>);
    rec0->initKey(k0);
    EXPECT_TRUE(rec0->empty());
    rec0->add(r2);
    EXPECT_FALSE(rec0->empty());
    
    // add r03|0.25 (copy of r01) to rec0
    const parsing::Run<KeyS>* r03 = new parsing::Run<KeyS>(*r01); // copy
    rec0->add(r03);
    EXPECT_FALSE(rec0->empty());
    
    // add r04|0.75 (copy of r02) to rec0
    const parsing::Run<KeyS>* r04 = new parsing::Run<KeyS>(*r02); // copy
    rec0->add(r04); // r04 does NOT replace r03 as best (weights eq.)
    EXPECT_FALSE(rec0->empty());

    // weightMax: best penalty to worst
    EXPECT_TRUE(*(rec0->best(1)) == *r03); // 0.25
    EXPECT_TRUE(*(rec0->best(2)) == *r04); // 0.75
    EXPECT_TRUE(*(rec0->best(3)) == *r2);  // 9
    
    delete rec0;
    delete rec1;
    delete rec2;
    // delete r01; // deleted with rec1
    // delete r02; // deleted with rec2
    // delete r2;  // deleted with rec0
    delete k0;
    delete k1;
    delete k2;
}


TEST(TestRecordQueue, TestThreeMax)
{
    ParsingEnv env = ParsingEnv(); // empty, not used here
    
    // nullary Run r01 = a()|0.25 in record rec1[k1]
    const label_t a  = SymbLabel::make_note();
    const Weight w = TropicalWeight::make_one().make(0.25);
    const Transition t = Transition(a, w);

    const parsing::Run<KeyS>* r01 =
        new parsing::Run<KeyS>(t, a, TropicalWeight::make_one().make(0.25));
    state_t q1 = 1;
    KeyS* k1 = new KeyS(q1, env);
    RecordQueue<KeyS>* rec1 = new RecordQueue<KeyS>(weightMin<KeyS>);
    rec1->initKey(k1);
    rec1->add(r01);
    
    // nullary Run r02 = c()|0.75 in record rec2[k2]
    const label_t c  = SymbLabel::make_appogiature(2);
    const Weight w1 = TropicalWeight::make_one().make(0.25);
    const Transition t1 = Transition(c, w1);

    state_t q2 = 2;
    KeyS* k2 = new KeyS(q2, env);
    const parsing::Run<KeyS>* r02 =
        new parsing::Run<KeyS>(t1, c, TropicalWeight::make_one().make(0.75));
    RecordQueue<KeyS>* rec2 = new RecordQueue<KeyS>(weightMin<KeyS>);
    rec2->initKey(k2);
    rec2->add(r02);
    
    // binary run r2 = b(r01, r02)|9 in rec0
    const label_t b  = SymbLabel::make_tuple(2);
    // const Weight w0 = w1.make(1);
    Weight w2 = w1.make(8);
    const Transition t2 = Transition(b, w2);

    const parsing::Run<KeyS> r20 = parsing::Run<KeyS>(t2, b, w2);
    const parsing::Run<KeyS> r21 = parsing::Run<KeyS>(r20, rec1, r01); // copy/update
    w2 *= r01->weight();
    const parsing::Run<KeyS>* r2 = new parsing::Run<KeyS>(r21, rec2, r02); // 2d copy/update
    w2 *= r02->weight();
    state_t q0 = 0;
    KeyS* k0 = new KeyS(q0, env);
    RecordQueue<KeyS>* rec0 = new RecordQueue<KeyS>(weightMin<KeyS>);
    rec0->initKey(k0);
    EXPECT_TRUE(rec0->empty());
    rec0->add(r2);
    EXPECT_FALSE(rec0->empty());
    
    // add r03|0.25 (copy of r01) to rec0
    const parsing::Run<KeyS>* r03 = new parsing::Run<KeyS>(*r01); // copy
    rec0->add(r03);
    EXPECT_FALSE(rec0->empty());
    
    // add r04|0.75 (copy of r02) to rec0
    const parsing::Run<KeyS>* r04 = new parsing::Run<KeyS>(*r02); // copy
    rec0->add(r04); // r04 does NOT replace r03 as best (weights eq.)
    EXPECT_FALSE(rec0->empty());
    
    // weightMin: worst penalty to best
    EXPECT_TRUE(*(rec0->best(1)) == *r2);  // 9
    EXPECT_TRUE(*(rec0->best(2)) == *r04); // 0.75
    EXPECT_TRUE(*(rec0->best(3)) == *r03); // 0.25
    
    delete rec0;
    delete rec1;
    delete rec2;
    // delete r01; // deleted with rec1
    // delete r02; // deleted with rec2
    // delete r2;  // deleted with rec0
    delete k0;
    delete k1;
    delete k2;
}

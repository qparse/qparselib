//
//  TestKeyS.cpp
//  GTests
//
//  Created by Florent Jacquemard on 12/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include <stdio.h>
#include "gtest/gtest.h"

#include "TropicalWeight.hpp"
#include "SymbLabel.hpp"
#include "Transition.hpp"
#include "KeyS.hpp"
#include "ParsingEnv.hpp"

using namespace parsing;

TEST(TestKeyS, TestBase)
{
    state_t q0 = 0;
    ParsingEnv env = ParsingEnv(); // empty, not used here
    KeyS k0 = KeyS(q0, env);
    EXPECT_TRUE(k0.complete());
    EXPECT_FALSE(k0.partial());
    EXPECT_EQ(k0.state(), q0);
    KeyS k = k0;
    EXPECT_TRUE(k.complete());
    EXPECT_TRUE(k0 == k);
    EXPECT_TRUE(k.instance(k0));
}


TEST(TestKeyS, TestTerminal)
{
    state_t q0 = 0;
    ParsingEnv env = ParsingEnv(); // empty, not used here
    KeyS k0 = KeyS(q0, env);
    
    // constant symbol with specific info = 1 (note)
    const label_t a  = SymbLabel::make_note();
    // transition with unknown weight and empty body.
    const Weight w1 = TropicalWeight::make_one();
    Transition t0 = Transition(a, w1);
    
    EXPECT_TRUE(k0.weight(a, t0, env) == w1);
}


TEST(TestKeyS, TestBinary)
{
    state_t q0 = 0;
    ParsingEnv env = ParsingEnv(); // empty, not used here
    KeyS k0 = KeyS(q0, env);
    
    // binary symbol
    const label_t b  = SymbLabel::make_tuple(2);
    const Weight w2 = TropicalWeight::make_one().make(0.5);
    Transition t2 = Transition(b, w2);
    state_t q1 = 1;
    state_t q2 = 2;
    t2.push(q1);
    t2.push(q2);
    EXPECT_TRUE(k0.weight(b, t2, env) == w2);
    
    // first child
    KeyS k1 = KeyS(k0, t2, env);
    KeyS k11 = KeyS(q1, env);
    EXPECT_TRUE(k1.complete());
    EXPECT_FALSE(k1.partial());
    EXPECT_EQ(k1.state(), q1);
    EXPECT_TRUE(k1 == k11);
    EXPECT_TRUE(k1.instance(k11));
    
    // next sibling
    KeyS k2 = KeyS(k1, 1, k0, t2, env);
    KeyS k21 = KeyS(q2, env);
    EXPECT_TRUE(k2.complete());
    EXPECT_FALSE(k2.partial());
    EXPECT_EQ(k2.state(), q2);
    EXPECT_TRUE(k2 == k21);
    EXPECT_TRUE(k2.instance(k21));

    EXPECT_TRUE(k0.weight(b, t2, env) == w2);

    //    std::queue<KeyS*> qu = std::queue<KeyS*>(); // empty queue
    //    k0.leaf(a, t0, env, qu);
}

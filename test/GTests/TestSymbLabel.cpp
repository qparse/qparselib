//
//  TestSymbLabel.cpp
//  GTests
//
//  Created by Florent Jacquemard on 28/03/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//
// test class Label (static functions)

#include <stdio.h>
#include "gtest/gtest.h"

#include "SymbLabel.hpp"

// constant inside-the-bar symbols
TEST(TestSymbLabel, TestContinuation)
{
    // constant symbol of continuation
    label_t c  = SymbLabel::make_tie();
    EXPECT_EQ(SymbLabel::arity(c), 0);
    EXPECT_FALSE(SymbLabel::undef(c));
    EXPECT_TRUE(SymbLabel::terminal(c));
    EXPECT_FALSE(SymbLabel::inner(c));
    EXPECT_FALSE(SymbLabel::binary(c));
    EXPECT_TRUE(SymbLabel::nbGraceNotes(c) == 0);
    EXPECT_TRUE(SymbLabel::nbEvents(c) == 0);
    EXPECT_FALSE(SymbLabel::note(c));
    EXPECT_FALSE(SymbLabel::rest(c));
    EXPECT_TRUE(SymbLabel::continuation(c));
    EXPECT_FALSE(SymbLabel::dot(c));
    EXPECT_FALSE(SymbLabel::tuple(c));
    EXPECT_FALSE(SymbLabel::bar(c));
    EXPECT_FALSE(SymbLabel::grace(c));
    EXPECT_FALSE(SymbLabel::fork(c));
    EXPECT_TRUE(SymbLabel::inside(c));
    EXPECT_EQ(SymbLabel::inmultiplicity(c), 1); // default

    c  = SymbLabel::set_multiplicity(c, 2);
    EXPECT_EQ(SymbLabel::arity(c), 0);
    EXPECT_FALSE(SymbLabel::undef(c));
    EXPECT_TRUE(SymbLabel::terminal(c));
    EXPECT_FALSE(SymbLabel::inner(c));
    EXPECT_FALSE(SymbLabel::binary(c));
    EXPECT_TRUE(SymbLabel::nbGraceNotes(c) == 0);
    EXPECT_TRUE(SymbLabel::nbEvents(c) == 0);
    EXPECT_FALSE(SymbLabel::note(c));
    EXPECT_FALSE(SymbLabel::rest(c));
    EXPECT_TRUE(SymbLabel::continuation(c));
    EXPECT_FALSE(SymbLabel::dot(c));
    EXPECT_FALSE(SymbLabel::tuple(c));
    EXPECT_FALSE(SymbLabel::bar(c));
    EXPECT_FALSE(SymbLabel::grace(c));
    EXPECT_FALSE(SymbLabel::fork(c));
    EXPECT_TRUE(SymbLabel::inside(c));
    EXPECT_EQ(SymbLabel::inmultiplicity(c), 2);
}

TEST(TestSymbLabel, TestNote)
{
    // constant symbol of single note
    label_t a  = SymbLabel::make_note();
    EXPECT_EQ(SymbLabel::arity(a), 0);
    EXPECT_FALSE(SymbLabel::undef(a));
    EXPECT_TRUE(SymbLabel::terminal(a));
    EXPECT_FALSE(SymbLabel::inner(a));
    EXPECT_FALSE(SymbLabel::binary(a));
    EXPECT_TRUE(SymbLabel::nbGraceNotes(a) == 0);
    EXPECT_TRUE(SymbLabel::nbEvents(a) == 1);
    EXPECT_TRUE(SymbLabel::note(a));
    EXPECT_FALSE(SymbLabel::rest(a));
    EXPECT_FALSE(SymbLabel::continuation(a));
    EXPECT_FALSE(SymbLabel::dot(a));
    EXPECT_FALSE(SymbLabel::tuple(a));
    EXPECT_FALSE(SymbLabel::bar(a));
    EXPECT_FALSE(SymbLabel::grace(a));
    EXPECT_FALSE(SymbLabel::fork(a));
    EXPECT_TRUE(SymbLabel::inside(a));
    EXPECT_EQ(SymbLabel::inmultiplicity(a), 1); // default
}

TEST(TestSymbLabel, TestVarnote)
{
    // constant symbol of note with undef nb of g.n.
    label_t v  = SymbLabel::make_varnote();
    EXPECT_TRUE(SymbLabel::undef(v));
    EXPECT_EQ(SymbLabel::arity(v), 0);
    EXPECT_TRUE(SymbLabel::note(v));
    EXPECT_FALSE(SymbLabel::rest(v));
    EXPECT_FALSE(SymbLabel::continuation(v));
    EXPECT_FALSE(SymbLabel::dot(v));
    EXPECT_FALSE(SymbLabel::tuple(v));
    EXPECT_FALSE(SymbLabel::bar(v));
    EXPECT_FALSE(SymbLabel::grace(v));
    EXPECT_FALSE(SymbLabel::fork(v));
    EXPECT_TRUE(SymbLabel::inside(v));
    EXPECT_EQ(SymbLabel::inmultiplicity(v), 1); // default
    
    // instanciate
    label_t a = SymbLabel::setNb(v, 2);
    EXPECT_EQ(SymbLabel::nbGraceNotes(a), 1);
    EXPECT_EQ(SymbLabel::nbEvents(a), 2);
    EXPECT_EQ(SymbLabel::inmultiplicity(a), 1); // default
}
    
TEST(TestSymbLabel, TestAppogiature)
    {
    // constant symbol for 2 grace-notes + 1 note (or chord).
    label_t ga  = SymbLabel::make_appogiature(3);
    EXPECT_FALSE(SymbLabel::undef(ga));
    EXPECT_EQ(SymbLabel::arity(ga), 0);
    EXPECT_TRUE(SymbLabel::terminal(ga));
    EXPECT_FALSE(SymbLabel::inner(ga));
    EXPECT_TRUE(SymbLabel::nbGraceNotes(ga) == 2);
    EXPECT_TRUE(SymbLabel::nbEvents(ga) == 3);
    EXPECT_TRUE(SymbLabel::note(ga));
    EXPECT_FALSE(SymbLabel::rest(ga));
    EXPECT_FALSE(SymbLabel::continuation(ga));
    EXPECT_FALSE(SymbLabel::dot(ga));
    EXPECT_FALSE(SymbLabel::tuple(ga));
    EXPECT_FALSE(SymbLabel::bar(ga));
    EXPECT_FALSE(SymbLabel::grace(ga));
    EXPECT_FALSE(SymbLabel::fork(ga));
    EXPECT_TRUE(SymbLabel::inside(ga));
        EXPECT_EQ(SymbLabel::inmultiplicity(ga), 1); // default

    ga  = SymbLabel::set_multiplicity(ga, 0);
    EXPECT_FALSE(SymbLabel::undef(ga));
    EXPECT_EQ(SymbLabel::arity(ga), 0);
    EXPECT_TRUE(SymbLabel::terminal(ga));
    EXPECT_FALSE(SymbLabel::inner(ga));
    EXPECT_TRUE(SymbLabel::nbGraceNotes(ga) == 2);
    EXPECT_TRUE(SymbLabel::nbEvents(ga) == 3);
    EXPECT_TRUE(SymbLabel::note(ga));
    EXPECT_FALSE(SymbLabel::rest(ga));
    EXPECT_FALSE(SymbLabel::continuation(ga));
    EXPECT_FALSE(SymbLabel::dot(ga));
    EXPECT_FALSE(SymbLabel::tuple(ga));
    EXPECT_FALSE(SymbLabel::bar(ga));
    EXPECT_FALSE(SymbLabel::grace(ga));
    EXPECT_FALSE(SymbLabel::fork(ga));
    EXPECT_TRUE(SymbLabel::inside(ga));
        EXPECT_EQ(SymbLabel::inmultiplicity(ga), 0); // default
}


TEST(TestSymbLabel, TestRest)
{
    // constant symbol for 2 grace-notes + 1 note (or chord).
    label_t a  = SymbLabel::make_rest(2); // arg multiplicity
    EXPECT_FALSE(SymbLabel::undef(a));
    EXPECT_EQ(SymbLabel::arity(a), 0);
    EXPECT_TRUE(SymbLabel::terminal(a));
    EXPECT_FALSE(SymbLabel::inner(a));
    EXPECT_TRUE(SymbLabel::nbGraceNotes(a) == 0);
    EXPECT_TRUE(SymbLabel::nbEvents(a) == 1);
    EXPECT_FALSE(SymbLabel::note(a));
    EXPECT_TRUE(SymbLabel::rest(a));
    EXPECT_FALSE(SymbLabel::continuation(a));
    EXPECT_FALSE(SymbLabel::dot(a));
    EXPECT_FALSE(SymbLabel::tuple(a));
    EXPECT_FALSE(SymbLabel::bar(a));
    EXPECT_FALSE(SymbLabel::grace(a));
    EXPECT_FALSE(SymbLabel::fork(a));
    EXPECT_TRUE(SymbLabel::inside(a));
    EXPECT_EQ(SymbLabel::inmultiplicity(a), 2);
}

TEST(TestSymbLabel, TestDot)
{
    // constant symbol for dots
    label_t a  = SymbLabel::make_dot(2);
    EXPECT_FALSE(SymbLabel::undef(a));
    EXPECT_EQ(SymbLabel::arity(a), 0);
    EXPECT_TRUE(SymbLabel::terminal(a));
    EXPECT_FALSE(SymbLabel::inner(a));
    EXPECT_TRUE(SymbLabel::nbGraceNotes(a) == 0);
    EXPECT_TRUE(SymbLabel::nbEvents(a) == 0);
    EXPECT_FALSE(SymbLabel::note(a));
    EXPECT_FALSE(SymbLabel::rest(a));
    EXPECT_FALSE(SymbLabel::continuation(a));
    EXPECT_TRUE(SymbLabel::dot(a));
    EXPECT_TRUE(SymbLabel::nbDots(a) == 2);
    EXPECT_FALSE(SymbLabel::tuple(a));
    EXPECT_FALSE(SymbLabel::bar(a));
    EXPECT_FALSE(SymbLabel::grace(a));
    EXPECT_FALSE(SymbLabel::fork(a));
    EXPECT_TRUE(SymbLabel::inside(a));
    EXPECT_EQ(SymbLabel::inmultiplicity(a), 1); // default
    
    a  = SymbLabel::set_multiplicity(a, 0);
    EXPECT_FALSE(SymbLabel::note(a));
    EXPECT_FALSE(SymbLabel::rest(a));
    EXPECT_FALSE(SymbLabel::continuation(a));
    EXPECT_TRUE(SymbLabel::dot(a));
    EXPECT_TRUE(SymbLabel::nbDots(a) == 2);
    EXPECT_EQ(SymbLabel::inmultiplicity(a), 0); // default
}


// inner inside symbol (division)
TEST(TestSymbLabel, TestTuple)
{
    // triplet symbol, unbeamed
    label_t a  = SymbLabel::make_tuple(3, false, 1);
    EXPECT_EQ(SymbLabel::arity(a), 3);
    EXPECT_FALSE(SymbLabel::undef(a));
    EXPECT_FALSE(SymbLabel::terminal(a));
    EXPECT_TRUE(SymbLabel::inner(a));
    EXPECT_FALSE(SymbLabel::binary(a));
    EXPECT_FALSE(SymbLabel::note(a));
    EXPECT_FALSE(SymbLabel::rest(a));
    EXPECT_FALSE(SymbLabel::continuation(a));
    EXPECT_FALSE(SymbLabel::dot(a));
    EXPECT_TRUE(SymbLabel::tuple(a));
    EXPECT_FALSE(SymbLabel::beamed(a));
    EXPECT_FALSE(SymbLabel::bar(a));
    EXPECT_FALSE(SymbLabel::grace(a));
    EXPECT_FALSE(SymbLabel::fork(a));
    EXPECT_TRUE(SymbLabel::inside(a));
    EXPECT_EQ(SymbLabel::inmultiplicity(a), 1);

    // triplet symbol, beamed (default)
    a  = SymbLabel::make_tuple(3);
    EXPECT_EQ(SymbLabel::arity(a), 3);
    EXPECT_FALSE(SymbLabel::undef(a));
    EXPECT_FALSE(SymbLabel::terminal(a));
    EXPECT_TRUE(SymbLabel::inner(a));
    EXPECT_FALSE(SymbLabel::binary(a));
    EXPECT_FALSE(SymbLabel::note(a));
    EXPECT_FALSE(SymbLabel::rest(a));
    EXPECT_FALSE(SymbLabel::continuation(a));
    EXPECT_FALSE(SymbLabel::dot(a));
    EXPECT_TRUE(SymbLabel::tuple(a));
    EXPECT_TRUE(SymbLabel::beamed(a));        // default
    EXPECT_FALSE(SymbLabel::bar(a));
    EXPECT_FALSE(SymbLabel::grace(a));
    EXPECT_FALSE(SymbLabel::fork(a));
    EXPECT_TRUE(SymbLabel::inside(a));
    EXPECT_EQ(SymbLabel::inmultiplicity(a), 1); // default
}


// grace note symbol
TEST(TestSymbLabel, TestGraceNote)
{
    //label_t a  = SymbLabel::make(2, SymbLabel::GRACE, 29);
    label_t a  = SymbLabel::make_grace();
    EXPECT_EQ(SymbLabel::arity(a), 2);
    EXPECT_FALSE(SymbLabel::undef(a));
    EXPECT_FALSE(SymbLabel::terminal(a));
    EXPECT_TRUE(SymbLabel::inner(a));
    EXPECT_TRUE(SymbLabel::binary(a));
    EXPECT_FALSE(SymbLabel::note(a));
    EXPECT_FALSE(SymbLabel::rest(a));
    EXPECT_FALSE(SymbLabel::continuation(a));
    EXPECT_FALSE(SymbLabel::dot(a));
    EXPECT_FALSE(SymbLabel::tuple(a));
    EXPECT_FALSE(SymbLabel::bar(a));
    EXPECT_TRUE(SymbLabel::grace(a));
    EXPECT_FALSE(SymbLabel::fork(a));
    EXPECT_TRUE(SymbLabel::inside(a));
    EXPECT_EQ(SymbLabel::inmultiplicity(a), 1); // default
}


// bar symbol
TEST(TestSymbLabel, TestBar)
{
    //label_t a  = SymbLabel::make(2, SymbLabel::BAR, 2929);
    label_t a  = SymbLabel::make_bar(0, 0);
    EXPECT_EQ(SymbLabel::arity(a), 0);
    EXPECT_FALSE(SymbLabel::undef(a));
    EXPECT_TRUE(SymbLabel::terminal(a));
    EXPECT_FALSE(SymbLabel::inner(a));
    EXPECT_FALSE(SymbLabel::binary(a));
    EXPECT_FALSE(SymbLabel::note(a));
    EXPECT_FALSE(SymbLabel::rest(a));
    EXPECT_FALSE(SymbLabel::continuation(a));
    EXPECT_FALSE(SymbLabel::dot(a));
    EXPECT_FALSE(SymbLabel::tuple(a));
    EXPECT_TRUE(SymbLabel::bar(a));
    EXPECT_FALSE(SymbLabel::grace(a));
    EXPECT_FALSE(SymbLabel::fork(a));
    EXPECT_FALSE(SymbLabel::inside(a));
    EXPECT_EQ(SymbLabel::inmultiplicity(a), 0);
    
    label_t b  = SymbLabel::make_bar(2, 0);
    EXPECT_EQ(SymbLabel::arity(b), 2);
    EXPECT_FALSE(SymbLabel::terminal(b));
    EXPECT_TRUE(SymbLabel::inner(b));
    EXPECT_TRUE(SymbLabel::binary(b));
    EXPECT_FALSE(SymbLabel::note(b));
    EXPECT_FALSE(SymbLabel::rest(b));
    EXPECT_FALSE(SymbLabel::continuation(b));
    EXPECT_FALSE(SymbLabel::dot(b));
    EXPECT_FALSE(SymbLabel::tuple(b));
    EXPECT_TRUE(SymbLabel::bar(b));
    EXPECT_FALSE(SymbLabel::grace(b));
    EXPECT_FALSE(SymbLabel::fork(b));
    EXPECT_FALSE(SymbLabel::inside(b));
    EXPECT_EQ(SymbLabel::inmultiplicity(b), 0);
}

// fork symbol
TEST(TestSymbLabel, TestFork)
{
    //label_t a  = SymbLabel::make(2, SymbLabel::FORK, 21929);
    label_t a  = SymbLabel::make_fork();
    EXPECT_EQ(SymbLabel::arity(a), 2);
    EXPECT_FALSE(SymbLabel::terminal(a));
    EXPECT_TRUE(SymbLabel::inner(a));
    EXPECT_TRUE(SymbLabel::binary(a));
    EXPECT_FALSE(SymbLabel::inside(a));
    EXPECT_FALSE(SymbLabel::bar(a));
    EXPECT_FALSE(SymbLabel::grace(a));
    EXPECT_TRUE(SymbLabel::fork(a));
    EXPECT_EQ(SymbLabel::inmultiplicity(a), 1); // default
}


// dummy symbol
TEST(TestSymbLabel, TestDummy)
{
    label_t a  = SymbLabel::make_dummy();
    EXPECT_EQ(SymbLabel::arity(a), 0);
    EXPECT_TRUE(SymbLabel::terminal(a));
    EXPECT_FALSE(SymbLabel::inner(a));
    EXPECT_FALSE(SymbLabel::binary(a));
    EXPECT_FALSE(SymbLabel::inside(a));
    EXPECT_FALSE(SymbLabel::note(a));
    EXPECT_FALSE(SymbLabel::rest(a));
    EXPECT_FALSE(SymbLabel::continuation(a));
    EXPECT_FALSE(SymbLabel::dot(a));
    EXPECT_FALSE(SymbLabel::tuple(a));
    EXPECT_FALSE(SymbLabel::bar(a));
    EXPECT_FALSE(SymbLabel::grace(a));
    EXPECT_FALSE(SymbLabel::fork(a));
    EXPECT_TRUE(SymbLabel::dummy(a));
    EXPECT_FALSE(SymbLabel::undef(a));
    EXPECT_EQ(SymbLabel::inmultiplicity(a), 0);
}

//
//  TestWeight.cpp
//  GTests
//
//  Created by Florent Jacquemard on 11/02/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include <stdio.h>
#include "gtest/gtest.h"

#include "Weight.hpp"
#include "TropicalWeight.hpp"
#include "ViterbiWeight.hpp"


TEST(TestWeight, TestBaseWeight)
{
    Weight* w0 = new Weight();
    EXPECT_TRUE(w0->unknown());
    EXPECT_FALSE(w0->is_zero());
    EXPECT_FALSE(w0->is_one());
}


TEST(TestWeight, TestTropicalWeight)
{
    // zero, one
    const Weight c0 = TropicalWeight::make_zero();
    const Weight c1 = TropicalWeight::make_one();

    EXPECT_FALSE(c0.unknown());
    EXPECT_TRUE(c0.is_zero());
    EXPECT_FALSE(c1.unknown());
    EXPECT_TRUE(c1.is_one());

    // copy
    Weight t0 = c0;
    Weight t1 = c1;

    EXPECT_FALSE(t0.unknown());
    EXPECT_TRUE(t0.is_zero());
    EXPECT_FALSE(t1.unknown());
    EXPECT_TRUE(t1.is_one());

    // make
    const Weight c01  = c0.make(0.1);
    const Weight c001 = c1.make(0.01);
    Weight t01 = c1.make(0.1);
    Weight t02 = c1.make(0.2);
    
    EXPECT_FALSE(c01.unknown());
    EXPECT_FALSE(c001.unknown());
    EXPECT_FALSE(t01.unknown());
    EXPECT_FALSE(t02.unknown());
    EXPECT_FALSE(t01.is_one());
    EXPECT_FALSE(t01.is_zero());
    EXPECT_TRUE(c01 == t01);
    EXPECT_TRUE(t01 != t02);

    // plus
    t01 *= c01;
    EXPECT_TRUE(t01 == t02);
    EXPECT_TRUE(t01 != c001);

    // min
    t01 += c001;
    EXPECT_TRUE(t01 == c001);

    // zero
    t01 += c0;
    EXPECT_FALSE(t01.unknown());
    EXPECT_FALSE(t01.is_zero());
    EXPECT_TRUE(t01 == c001);

    t02 *= c0;
    EXPECT_FALSE(t01.is_zero());
    EXPECT_FALSE(t02.unknown());
    EXPECT_TRUE(t02.is_zero());

    // one
    t01 *= c1;
    EXPECT_TRUE(t01 == c001);

}


TEST(TestWeight, TestViterbiWeight)
{
    // zero, one
    const Weight c0 = ViterbiWeight::make_zero();
    const Weight c1 = ViterbiWeight::make_one();
    
    EXPECT_FALSE(c0.unknown());
    EXPECT_TRUE(c0.is_zero());
    EXPECT_FALSE(c1.unknown());
    EXPECT_TRUE(c1.is_one());
    
    // copy
    Weight t0 = c0;
    Weight t1 = c1;
    
    EXPECT_FALSE(t0.unknown());
    EXPECT_TRUE(t0.is_zero());
    EXPECT_FALSE(t1.unknown());
    EXPECT_TRUE(t1.is_one());
    
    // make
    const Weight c01  = c0.make(0.1);
    const Weight c001 = c1.make(0.01);
    Weight t01 = c1.make(0.1);
    Weight t02 = c1.make(0.2);
    
    EXPECT_FALSE(c01.unknown());
    EXPECT_FALSE(c001.unknown());
    EXPECT_FALSE(t01.unknown());
    EXPECT_FALSE(t02.unknown());
    EXPECT_FALSE(t01.is_one());
    EXPECT_FALSE(t01.is_zero());
    EXPECT_TRUE(c01 == t01);
    EXPECT_TRUE(t01 != t02);
    
    // mult
    t01 *= c01;
    EXPECT_TRUE(t01 != t02);
    EXPECT_TRUE(t01 == c001);
    
    // max
    t01 += c01;
    EXPECT_TRUE(t01 != t02);
    EXPECT_TRUE(t01 == c01);
    
    // zero
    t01 += c0;
    EXPECT_FALSE(t01.unknown());
    EXPECT_FALSE(t01.is_zero());
    EXPECT_TRUE(t01 == c01);
    
    t02 *= c0;
    EXPECT_FALSE(t02.unknown());
    EXPECT_TRUE(t02.is_zero());
    
    // one
    t01 *= c1;
    EXPECT_TRUE(t01 == c01);

}


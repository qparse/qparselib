//
//  TestMTU.cpp
//  GTests
//
//  Created by Florent Jacquemard on 31/01/2020.
//  Copyright © 2020 Florent Jacquemard. All rights reserved.
//

#include <stdio.h>
#include "gtest/gtest.h"

#include "MTU.hpp"


TEST(TestMTU, TestSimple)
{
    mtu_t t = Rational(8) - Rational(1, 4);
    EXPECT_EQ(MTU::measure(t), 7);
    EXPECT_EQ(MTU::inside(t), Rational(3, 4));
    mtu_t u = MTU::make(7, 3, 4);
    EXPECT_EQ(t, u);
}

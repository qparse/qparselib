//
//  TestValueList.cpp
//  GTests
//
//  Created by Florent Jacquemard on 11/02/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#include <stdio.h>
#include "gtest/gtest.h"

// #include "MTU.hpp"
#include "ValueList.hpp"


TEST(TestValueList, TestConstruction)
{
    ValueList vl = ValueList(MTU::ONE);
    EXPECT_TRUE(vl.empty());
    EXPECT_EQ(vl.length(), Rational(1));
    EXPECT_EQ(vl.cont(), Rational(0));
    EXPECT_EQ(vl.size(), 0);
    vl.add(Rational(1));
    EXPECT_EQ(vl.size(), 1);
}

TEST(TestValueList, TestKind)
{
    ValueList vl0 = ValueList(MTU::ONE);
    vl0.addcont(Rational(1));
    EXPECT_TRUE(vl0.single_continuation());
    EXPECT_FALSE(vl0.single_event());
    EXPECT_FALSE(vl0.event());

    ValueList vl1 = ValueList(MTU::ONE);
    vl1.add(Rational(1));
    EXPECT_FALSE(vl1.single_continuation());
    EXPECT_TRUE(vl1.single_event());
    
    ValueList vl2 = ValueList(MTU::ONE);
    vl2.add(Rational(0));
    vl2.add(Rational(1));
    EXPECT_FALSE(vl2.single_continuation());
    EXPECT_FALSE(vl2.single_event());
    EXPECT_TRUE(vl2.event());
    EXPECT_EQ(vl2.nbgn(), 1);
}

TEST(TestValueList, TestSubList32)
{
    ValueList vl = ValueList(MTU::ONE);
    EXPECT_TRUE(vl.empty());
    vl.add(Rational(1, 3));
    vl.add(Rational(1, 3));
    vl.add(Rational(1, 3));
    EXPECT_FALSE(vl.empty());
    EXPECT_FALSE(vl.event());
    ValueList vl0 = ValueList(vl, Rational(0), Rational(1, 2));
    ValueList vl1 = ValueList(vl, Rational(1, 2), Rational(1, 2));
    EXPECT_EQ(vl0.cont(), Rational(0));
    EXPECT_EQ(vl0.nbevent(), 2);
    std::list<mtu_t>::const_iterator i = vl0.cbegin();
    EXPECT_NE(i, vl0.cend());
    EXPECT_EQ(*i, Rational(1, 3));
    ++i;
    EXPECT_NE(i, vl0.cend());
    EXPECT_EQ(*i, Rational(1, 6));
    ++i;
    EXPECT_EQ(i, vl0.cend());

    EXPECT_EQ(vl1.cont(), Rational(1, 6));
    EXPECT_EQ(vl1.nbevent(), 1);
    i = vl1.cbegin();
    EXPECT_EQ(*i, Rational(1, 3));
    ++i;
    EXPECT_EQ(i, vl1.cend());
}

TEST(TestValueList, TestSubList23)
{
    ValueList vl = ValueList(MTU::ONE);
    EXPECT_TRUE(vl.empty());
    vl.add(Rational(1, 2));
    vl.add(Rational(1, 2));
    EXPECT_FALSE(vl.empty());
    EXPECT_EQ(vl.cont(), Rational(0));
    EXPECT_EQ(vl.nbevent(), 2);
    EXPECT_FALSE(vl.event());

    // constructor  (original ValueList, start date, len)
    ValueList vl0 = ValueList(vl, Rational(0), Rational(1, 3));
    EXPECT_EQ(vl0.cont(), Rational(0));
    EXPECT_EQ(vl0.nbevent(), 1);
    std::list<mtu_t>::const_iterator i = vl0.cbegin();
    EXPECT_NE(i, vl0.cend());
    EXPECT_EQ(*i, Rational(1, 3));
    ++i;
    EXPECT_EQ(i, vl0.cend());

    ValueList vl1 = ValueList(vl, Rational(1, 3), Rational(1, 3));
    EXPECT_EQ(vl1.cont(), Rational(1, 6));
    EXPECT_EQ(vl1.nbevent(), 1);
    i = vl1.cbegin();
    EXPECT_NE(i, vl1.cend());
    EXPECT_EQ(*i, Rational(1, 6));
    ++i;
    EXPECT_EQ(i, vl1.cend());
    
    ValueList vl2 = ValueList(vl, Rational(2, 3), Rational(1, 3));
    EXPECT_EQ(vl2.cont(), Rational(1, 3));
    EXPECT_EQ(vl2.nbevent(), 0);
    i = vl2.cbegin();
    EXPECT_EQ(i, vl2.cend());
}

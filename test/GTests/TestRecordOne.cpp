//
//  TestRecordOne.cpp
//  GTests
//
//  Created by Florent Jacquemard on 07/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include <stdio.h>
#include "gtest/gtest.h"

#include "TropicalWeight.hpp"
#include "SymbLabel.hpp"
#include "KeyS.hpp"
#include "Runey.hpp"
#include "ParsingEnv.hpp"
#include "RecordOne.hpp"

using namespace parsing;

TEST(TestRecordOne, TestEmpty)
{
    RecordOne<KeyS> rec0 = RecordOne<KeyS>(weightMin<KeyS>);
    EXPECT_TRUE(rec0.empty());
    state_t q0 = 0;
    ParsingEnv env = ParsingEnv(); // empty, not used here
    KeyS* k0 = new KeyS(q0, env);
    rec0.initKey(k0);
    EXPECT_TRUE(rec0.key() == k0);
    EXPECT_TRUE(rec0.best(1) == NULL);

    delete k0;
}

// TBR
TEST(TestRecordOne, TestKey)
{
    state_t q0 = 0;
    ParsingEnv env = ParsingEnv(); // empty, not used here
    KeyS* k0 = new KeyS(q0, env);
    RecordOne<KeyS> rec0 = RecordOne<KeyS>(weightMax<KeyS>);
    rec0.initKey(k0);
    EXPECT_TRUE(rec0.empty());
    EXPECT_EQ(rec0.key(), k0);
    EXPECT_TRUE(rec0.best(1) == NULL);

    delete k0;
}


TEST(TestRecordOne, TestSingle)
{
    RecordOne<KeyS> rec0 = RecordOne<KeyS>(weightMax<KeyS>);
    state_t q0 = 0;
    ParsingEnv env = ParsingEnv(); // empty, not used here
    KeyS* k0 = new KeyS(q0, env);
    rec0.initKey(k0);
    EXPECT_EQ(rec0.key(), k0);
    EXPECT_TRUE(rec0.empty());

    // nullary Run
    const label_t a  = SymbLabel::make_note();
    const Weight w1 = TropicalWeight::make_one().make(0.25);
    const Transition t1 = Transition(a, w1);
    parsing::Run<KeyS>* r0 = new parsing::Run<KeyS>(t1, a, w1);
    rec0.add(r0);
    EXPECT_FALSE(rec0.empty());
    EXPECT_EQ(*(rec0.best(1)), *r0);
    
    // delete r0; // deleted with rec0
    delete k0;
}


TEST(TestRecordOne, TestTwo)
{
    RecordOne<KeyS>* rec0 = new RecordOne<KeyS>(weightMax<KeyS>);
    state_t q0 = 0;
    ParsingEnv env = ParsingEnv(); // empty, not used here
    KeyS* k0 = new KeyS(q0, env);
    rec0->initKey(k0);
    
    EXPECT_TRUE(rec0->empty());
    
    // nullary Run
    const label_t a  = SymbLabel::make_note();
    const Weight w1 = TropicalWeight::make_one().make(0.25);
    const Transition t1 = Transition(a, w1);
    const parsing::Run<KeyS>* r01 = new parsing::Run<KeyS>(t1, a, w1);
    
    // put r0 in a record
    state_t q1 = 1;
    KeyS* k1 = new KeyS(q1, env);
    RecordOne<KeyS>* rec1 = new RecordOne<KeyS>(weightMax<KeyS>);
    rec1->initKey(k1);
    rec1->add(r01);

    // put r0 in a second record
    state_t q2 = 2;
    KeyS* k2 = new KeyS(q2, env);
    const parsing::Run<KeyS>* r02 = new parsing::Run<KeyS>(*r01); // copy
    RecordOne<KeyS>* rec2 = new RecordOne<KeyS>(weightMax<KeyS>);
    rec2->initKey(k2);
    rec2->add(r02);

    // constant symbol with specific info = 1 (note)
    // binary symbol with specific info = 9
    const label_t b  = SymbLabel::make_tuple(2);
    const Weight w0 = w1.make(1);
    Weight w2 = w1.make(8);
    const Transition t2 = Transition(b, w2);
    const parsing::Run<KeyS> r20 = parsing::Run<KeyS>(t2, b, w2);

    const parsing::Run<KeyS> r21 = parsing::Run<KeyS>(r20, rec1, r01); // copy/update
    w2 *= r01->weight();
    const parsing::Run<KeyS>* r2 = new parsing::Run<KeyS>(r21, rec2, r02); // 2d copy/update
    w2 *= r02->weight();

    // add binary run
    rec0->add(r2);
    EXPECT_FALSE(rec0->empty());
    EXPECT_TRUE(*(rec0->best(1)) == *r2);

    // add nullary run (better)
    const parsing::Run<KeyS>* r03 = new parsing::Run<KeyS>(*r01); // copy
    rec0->add(r03);
    EXPECT_FALSE(rec0->empty());
    EXPECT_TRUE(*(rec0->best(1)) == *r03);

    // add nullary run (better)
    const parsing::Run<KeyS>* r04 = new parsing::Run<KeyS>(*r02); // copy
    rec0->add(r04); // r04 does NOT replace r03 as best (weights eq.)
    EXPECT_FALSE(rec0->empty());
    EXPECT_TRUE(*(rec0->best(1)) == *r03);

    delete rec0;
    delete rec1;
    delete rec2;
    // delete r01; // deleted with rec1
    // delete r02; // deleted with rec2
    // delete r2;  // deleted with rec0
    delete k0;
    delete k1;
    delete k2;
}

//
//  TestTransition.cpp
//  GTests
//
//  Created by Florent Jacquemard on 28/05/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include <stdio.h>
#include "gtest/gtest.h"

#include "TropicalWeight.hpp"
#include "SymbLabel.hpp"
#include "Transition.hpp"


TEST(TestTransition, TestTerminal)
{
    // constant symbol with specific info = 1 (note)
    const label_t a  = SymbLabel::make_note();
    // transition with unknown weight and empty body.
    Transition t0 = Transition(a);
    EXPECT_FALSE(t0.inner());
    EXPECT_TRUE(t0.terminal());
    EXPECT_EQ(t0.label(), a);
    EXPECT_EQ(t0.size(), 0);
    EXPECT_EQ(t0.arity(), 0);
    
    const Weight w1 = TropicalWeight::make_one();
    Transition t01 = Transition(a, w1);
    EXPECT_FALSE(t01.inner());
    EXPECT_TRUE(t01.terminal());
    EXPECT_EQ(t01.label(), a);
    EXPECT_EQ(t01.size(), 0);
    EXPECT_EQ(t01.arity(), 0);
    EXPECT_FALSE(t01.weight().unknown());
    EXPECT_FALSE(t01.weight().is_zero());
    EXPECT_TRUE(t01.weight().is_one());
    EXPECT_EQ(t01.weight(), w1);
    EXPECT_TRUE(t01.weight() == w1);
}


TEST(TestTransition, TestInner)
{
    // binary symbol with specific info = 1
    const label_t a  = SymbLabel::make_tuple(2);
    const Weight w1 = TropicalWeight::make_one();
    Transition t2 = Transition(a, w1);
    EXPECT_TRUE(t2.inner());
    EXPECT_FALSE(t2.terminal());
    EXPECT_EQ(t2.label(), a);
    EXPECT_FALSE(t2.weight().unknown());
    EXPECT_FALSE(t2.weight().is_zero());
    EXPECT_TRUE(t2.weight().is_one());
    EXPECT_EQ(t2.weight(), w1);
    EXPECT_TRUE(t2.weight() == w1);
    EXPECT_EQ(t2.size(), 0);
    EXPECT_EQ(t2.arity(), 2);
    // add state 1
    t2.push(1);
    EXPECT_EQ(t2.size(), 1);
    EXPECT_EQ(t2.arity(), 2);
    // add state 2
    t2.push(2);
    EXPECT_EQ(t2.size(), 2);
    EXPECT_EQ(t2.arity(), 2);
    EXPECT_TRUE(t2.member(1));
    EXPECT_TRUE(t2.member(2));
    EXPECT_FALSE(t2.member(3));
    EXPECT_EQ(t2.state(0), 1);
    EXPECT_EQ(t2.state(1), 2);
}


//
//  TestRunFilter.cpp
//  GTests
//
//  Created by Florent Jacquemard on 29/03/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include <stdio.h>
#include "gtest/gtest.h"

#include "RunFilter.hpp"

using namespace parsing;

TEST(TestRunFilter, TestTerminal)
{
    OPT_RUN_UNIT = true;
    OPT_RUN_CONT = true;

    label_t c  = SymbLabel::make_tie();
    RunFilter fc = RunFilter(c);
    
    EXPECT_TRUE(fc.complete());
    EXPECT_TRUE(fc.filter());
    EXPECT_FALSE(fc.unit());
    EXPECT_TRUE(fc.continuation());
   
    label_t a  = SymbLabel::make_note();
    RunFilter fa = RunFilter(a);

    EXPECT_TRUE(fa.complete());
    EXPECT_TRUE(fa.filter());
    EXPECT_TRUE(fa.unit());
    EXPECT_FALSE(fa.continuation());
}


TEST(TestRunFilter, TestInsideUnit)
{
    OPT_RUN_UNIT = true;
    OPT_RUN_CONT = true;
    
    label_t a  = SymbLabel::make_note();
    label_t c  = SymbLabel::make_tie();
    label_t g  = SymbLabel::make_tuple(3,0);
    
    RunFilter fa = RunFilter(a);
    RunFilter fc = RunFilter(c);
    RunFilter fg = RunFilter(g);
    
    EXPECT_FALSE(fg.complete());
    
    fg.update(fa);
    
    EXPECT_TRUE(fa.complete());
    EXPECT_TRUE(fc.complete());
    EXPECT_FALSE(fg.complete());
    
    fg.update(fc);

    EXPECT_FALSE(fg.complete());

    fg.update(fc);

    EXPECT_TRUE(fa.complete());
    EXPECT_TRUE(fg.complete());
    EXPECT_FALSE(fg.filter());
    EXPECT_TRUE(fg.unit());
    EXPECT_FALSE(fg.continuation());
}


TEST(TestRunFilter, TestInsideCont)
{
    OPT_RUN_UNIT = true;
    OPT_RUN_CONT = true;
    
    label_t a  = SymbLabel::make_note();
    label_t c  = SymbLabel::make_tie();
    label_t g  = SymbLabel::make_tuple(3, 0);
    
    RunFilter fa = RunFilter(a);
    RunFilter fc = RunFilter(c);
    RunFilter fg = RunFilter(g);
    
    EXPECT_FALSE(fg.complete());
    
    fg.update(fc);
    
    EXPECT_TRUE(fa.complete());
    EXPECT_TRUE(fc.complete());
    EXPECT_FALSE(fg.complete());
    
    fg.update(fc);
    
    EXPECT_FALSE(fg.complete());
    
    fg.update(fc);
    
    EXPECT_TRUE(fa.complete());
    EXPECT_TRUE(fg.complete());
    EXPECT_FALSE(fg.filter());
    EXPECT_FALSE(fg.unit());
    EXPECT_TRUE(fg.continuation());
}


TEST(TestRunFilter, TestInside)
{
    OPT_RUN_UNIT = true;
    OPT_RUN_CONT = true;
    
    label_t a  = SymbLabel::make_note();
    label_t c  = SymbLabel::make_tie();
    label_t g  = SymbLabel::make_tuple(3, 0);

    RunFilter fa = RunFilter(a);
    RunFilter fc = RunFilter(c);
    RunFilter fg = RunFilter(g);
    
    EXPECT_FALSE(fg.complete());
    
    fg.update(fa);
    
    EXPECT_TRUE(fa.complete());
    EXPECT_TRUE(fc.complete());
    EXPECT_FALSE(fg.complete());
    
    fg.update(fc);
    
    EXPECT_FALSE(fg.complete());
    
    fg.update(fa);
    
    EXPECT_TRUE(fa.complete());
    EXPECT_TRUE(fg.complete());
    EXPECT_TRUE(fg.filter());
    EXPECT_FALSE(fg.unit());
    EXPECT_FALSE(fg.continuation());
}


TEST(TestRunFilter, TestOutside)
{
    OPT_RUN_UNIT = true;
    OPT_RUN_CONT = true;
    
    label_t a  = SymbLabel::make_bar(0, 1);
    label_t s  = SymbLabel::make_bar(2, 0);

    RunFilter fa = RunFilter(a);
    RunFilter fs = RunFilter(s);

    
    EXPECT_TRUE(fa.complete());
    EXPECT_TRUE(fs.complete());

    fs.update(fa);

    EXPECT_TRUE(fa.complete());
    EXPECT_TRUE(fs.complete());

    fs.update(fa);

    EXPECT_TRUE(fa.complete());
    EXPECT_TRUE(fs.complete());
    EXPECT_TRUE(fs.filter());
//    EXPECT_FALSE(fs.unit());
//    EXPECT_FALSE(fs.continuation());
}

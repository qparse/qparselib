//
//  TestSegment.cpp
//  GTests
//
//  Created by Florent Jacquemard on 05/06/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include <stdio.h>
#include "gtest/gtest.h"

#include "InputSegment.hpp"


TEST(TestSegment, TestInputSegment)
{
    InputSegment* seg = new InputSegment(0, 2);
    EXPECT_EQ(seg->rbegin(), 0);
    EXPECT_EQ(seg->rend(), 2);
    EXPECT_EQ(seg->Interval::rduration(), 2);
    EXPECT_EQ(seg->size(), 0);
  
    MusEvent* e[7];
    e[0] = NULL;
    e[1] = new NoteEvent(44, 100); //MusEvent::UNDEF_VELOCITY
    e[2] = new NoteEvent(45, 200); //MusEvent::UNDEF_VELOCITY
    e[3] = new NoteEvent(46, 230); //MusEvent::UNDEF_VELOCITY
    e[4] = new NoteEvent(47, 150); //MusEvent::UNDEF_VELOCITY
    e[5] = new NoteEvent(48, 240); //MusEvent::UNDEF_VELOCITY
    e[6] = new NoteEvent(49, 130); //MusEvent::UNDEF_VELOCITY

    seg->add_back(e[1], 0.07, true);  // ONSET, rdur=0?
    seg->add_back(e[2], 0.72, true);
    seg->add_back(e[3], 0.91, true);
    seg->add_back(e[4], 1.05, true);
    seg->add_back(e[5], 1.36, true);
    seg->add_back(e[6], 1.71, true);

    EXPECT_EQ(seg->size(), 6);
    delete seg;
}

//
//  TestTableInstances.cpp
//  GTests
//
//  Created by Florent Jacquemard on 14/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include <stdio.h>
#include "gtest/gtest.h"

#include "TropicalWeight.hpp"
#include "SymbLabel.hpp"
#include "KeyS.hpp"
#include "Runey.hpp"
#include "ParsingEnv.hpp"
#include "RecordOne.hpp"
#include "TableInstances.hpp"

using namespace parsing;

TEST(TestTableInstances, TestEmpty)
{
    TableInstances<KeyS, RecordOne<KeyS>, KeySHasher>* ti0 =
        new TableInstances<KeyS, RecordOne<KeyS>, KeySHasher>();
    
    EXPECT_TRUE(ti0->empty());
    delete ti0;
}



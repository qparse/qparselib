//
//  TestInterval.cpp
//  GTests
//
//  Created by Florent Jacquemard on 06/06/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include <stdio.h>
//#include <assert.h>
#include "gtest/gtest.h"

#include "InputSegment.hpp"
#include "IntervalHeap.hpp"
#include "IntervalTree.hpp"


class TestInterval : public ::testing::Test
{
protected:
    
    // monophonic input segment
    void SetUp() override
    {
        seg = new InputSegment(0, 2);

        e[0] = NULL;
        e[1] = new NoteEvent(44, 100); //MusEvent::UNDEF_VELOCITY
        e[2] = new NoteEvent(45, 200); //MusEvent::UNDEF_VELOCITY
        e[3] = new NoteEvent(46, 230); //MusEvent::UNDEF_VELOCITY
        e[4] = new NoteEvent(47, 150); //MusEvent::UNDEF_VELOCITY
        e[5] = new NoteEvent(48, 240); //MusEvent::UNDEF_VELOCITY
        e[6] = new NoteEvent(49, 130); //MusEvent::UNDEF_VELOCITY
        
        seg->add_back(e[1], 0.07, true);  // ONSET, rdur=0?
        seg->add_back(e[2], 0.72, true);
        seg->add_back(e[3], 0.91, true);
        seg->add_back(e[4], 1.05, true);
        seg->add_back(e[5], 1.36, true);
        seg->add_back(e[6], 1.71, true);
    }
    
    void TearDown() override
    {
        delete seg;
        // events are cleared when segment deleted
        //        for (size_t i = 1; i < 7; ++i)
        //            delete e[i];
    }

    InputSegment* seg;
    MusEvent* e[7];
};


TEST_F(TestInterval, TestBoundedInterval)
{
    EXPECT_EQ(seg->rbegin(), 0);
    EXPECT_EQ(seg->rend(), 2);
    EXPECT_EQ(seg->Interval::rduration(), 2);
    EXPECT_EQ(seg->size(), 6);
    
    Interval* p = new Interval(seg, MTU::make(2));
    EXPECT_TRUE(seg->rbegin() == p->rbegin());
    EXPECT_TRUE(seg->rend() <= p->rend());
    EXPECT_TRUE((p->mend() == MTU::UNBOUNDED) || (p->mbegin() <= p->mend()));
    delete p;
}


TEST_F(TestInterval, TestOpenInterval)
{
    Interval* p = new Interval(seg, MTU::UNBOUNDED);
    EXPECT_TRUE(seg->rbegin() == p->rbegin());
    EXPECT_TRUE(seg->rend() <= p->rend());
    EXPECT_TRUE((p->mend() == MTU::UNBOUNDED) || (p->mbegin() <= p->mend()));
    delete p;
}

TEST_F(TestInterval, TestAlignedInterval)
{
    AlignedInterval* p = new AlignedInterval(seg, MTU::make(2), true);

    EXPECT_TRUE(seg->rbegin() == p->rbegin());
    EXPECT_TRUE(seg->rend() <= p->rend());
    EXPECT_TRUE((p->mend() == MTU::UNBOUNDED) || (p->mbegin() <= p->mend()));
    
    EXPECT_EQ(p->lsize(), 3);
    EXPECT_EQ(p->lfirst(), 0);
    EXPECT_EQ(p->rsize(), 3);
    EXPECT_EQ(p->rfirst(), 3);
    EXPECT_EQ(p->size(), 6); // out-of-bounds
    EXPECT_EQ(p->first(), 0);
    EXPECT_EQ(p->next(), 6); // out-of-bounds
    EXPECT_TRUE(p->inhabited());

    delete p;
}


TEST_F(TestInterval, TestIntervalTreeBounded)
{
    IntervalHeap* ih = new IntervalHeap();
    IntervalTree* p = ih->make(seg, MTU::make(2), 0.35);
    
    // split into first bar and rest
    IntervalTree* p_first = p->split(seg, ih, 1, MTU::ONE, 1);
    assert(p_first);
    p_first->align(seg);
    EXPECT_EQ(p_first->size(), 3);
    EXPECT_EQ(p_first->first(), 0);
    EXPECT_EQ(p_first->next(), 3);
    EXPECT_EQ(p_first->lsize(), 1);
    EXPECT_EQ(p_first->lfirst(), 0);
    EXPECT_EQ(p_first->rsize(), 2);
    EXPECT_EQ(p_first->rfirst(), 1);
    
    IntervalTree* p_rest  = p->split(seg, ih, 1, MTU::ONE, 2);
    assert(p_rest);
    p_rest->align(seg);
    EXPECT_EQ(p_rest->size(), 3);
    EXPECT_EQ(p_rest->first(), 3);
    EXPECT_EQ(p_rest->next(), 6); // out-of-bounds
    EXPECT_EQ(p_rest->lsize(), 2);
    EXPECT_EQ(p_rest->lfirst(), 3);
    EXPECT_EQ(p_rest->rsize(), 1);
    EXPECT_EQ(p_rest->rfirst(), 5);
    
    delete ih;
    // p is deleted with IntervalHeap
    
}

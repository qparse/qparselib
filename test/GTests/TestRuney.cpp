//
//  TestRuney.cpp
//  GTests
//
//  Created by Florent Jacquemard on 06/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include <stdio.h>
#include "gtest/gtest.h"

#include "TropicalWeight.hpp"
#include "SymbLabel.hpp"
#include "KeyS.hpp"
#include "Runey.hpp"
#include "RecordOne.hpp"
#include "ParsingEnv.hpp"

using namespace parsing;

TEST(TestRuney, TestTerminal)
{
    // constant note symbol
    const label_t a  = SymbLabel::make_note();
    const Weight w1 = TropicalWeight::make_one();
    Transition t1 = Transition(a, w1);
    parsing::RunTerm r0 = parsing::RunTerm(t1, a, w1, 1);
    EXPECT_FALSE(r0.inner());
    EXPECT_TRUE(r0.terminal());
    EXPECT_EQ(r0.label(), a);
    EXPECT_EQ(r0.size(), 0);
    EXPECT_EQ(r0.arity(), 0);
    EXPECT_TRUE(r0.complete());
    EXPECT_FALSE(r0.weight().unknown());
    EXPECT_FALSE(r0.weight().is_zero());
    EXPECT_TRUE(r0.weight().is_one());
    EXPECT_EQ(r0.weight(), w1);
    EXPECT_TRUE(r0.weight() == w1);
}

// 72
TEST(TestRuney, TestBinary)
{
    // constant note symbol
    const label_t a  = SymbLabel::make_note();
    Weight w1 = TropicalWeight::make_one().make(0.25);
    Transition t1 = Transition(a, w1);
    const parsing::RunInner<KeyS> r0 =
        parsing::RunInner<KeyS>(t1, a, w1);

    // binary symbol
    const label_t b  = SymbLabel::make_tuple(2);
    Weight w0 = w1.make(1);
    Weight w2 = w1.make(8);
    Transition t2 = Transition(b, w2);
    
    const parsing::RunInner<KeyS> r2 =
        parsing::RunInner<KeyS>(t2, b, w2);

    EXPECT_TRUE(r2.inner());
    EXPECT_FALSE(r2.terminal());
    EXPECT_EQ(r2.label(), b);
    EXPECT_EQ(r2.size(), 0);
    EXPECT_EQ(r2.arity(), 2);
    EXPECT_FALSE(r2.complete());
    EXPECT_FALSE(r2.weight().unknown());
    EXPECT_FALSE(r2.weight().is_zero());
    EXPECT_FALSE(r2.weight().is_one());
    EXPECT_EQ(r2.weight(), w2);
    EXPECT_TRUE(r2.weight() == w2);
    
    ParsingEnv env = ParsingEnv(); // empty

    // put r0 in a record
    state_t q1 = 1;
    KeyS* k1 = new KeyS(q1, env);
    RecordOne<KeyS>* rec1 = new RecordOne<KeyS>(weightMax<KeyS>);
    rec1->initKey(k1);
    rec1->add(r0);
    
    // put r0 in a second record
    state_t q2 = 2;
    KeyS* k2 = new KeyS(q2, env);
    RecordOne<KeyS>* rec2 = new RecordOne<KeyS>(weightMax<KeyS>);
    const parsing::Run<KeyS>* r00 = new parsing::RunInner<KeyS>(r0); // copy
    rec2->initKey(k2);
    rec2->add(r00);
    // copy/update
    const parsing::Run<KeyS> r21 = parsing::Run<KeyS>(r2, rec1, r0);
    EXPECT_EQ(r21.size(), 1);
    EXPECT_EQ(r21.arity(), 2);
    EXPECT_FALSE(r21.complete());
    w2 *= r0->weight();
    EXPECT_EQ(r21.weight(), w2);
    EXPECT_TRUE(r21.weight() == w2);

    // second copy/update
    const parsing::Run<KeyS> r22 = parsing::Run<KeyS>(r21, rec2, r0);
    EXPECT_EQ(r22.size(), 2);
    EXPECT_EQ(r21.arity(), 2);
    EXPECT_TRUE(r22.complete());
    w2 *= r0->weight();
    EXPECT_EQ(r22.weight(), w2);
    EXPECT_TRUE(r22.weight() == w2);

//    delete r0; // deleted with rec1
    delete rec1;
    delete rec2;
    delete k1;
    delete k2;

    // also test filter update ?

}

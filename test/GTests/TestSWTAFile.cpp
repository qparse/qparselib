//
//  TestSWTAFileIn.cpp
//  GTests
//
//  Created by Florent Jacquemard on 19/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include <stdio.h>
#include "gtest/gtest.h"

#include "trace.hpp"
#include "util.hpp" // parse string
#include "Weight.hpp"
// no compile options for WTA weight : load all classes
#include "FloatWeight.hpp"
#include "TropicalWeight.hpp"
#include "ViterbiWeight.hpp"
#include "SWTA.hpp"
//#include "ScoreMeter.hpp"
#include "SchemaFileIn.hpp"
#define private public // dirty trick to test a private member
#include "SWTAFileIn.hpp"
#define private private

TEST(TestSWTA, TestComment)
{
    SWTAFileIn ta = SWTAFileIn(TropicalWeight::make_one());
    size_t r;
    r = ta.read_line("    // les dents, pipi et au lit! ", true);
    EXPECT_EQ(r, 1);
    EXPECT_TRUE(ta.empty());
}


TEST(TestSWTA, TestNullary)
{
    SWTAFileIn ta = SWTAFileIn(TropicalWeight::make_zero());
    size_t r;
    r = ta.read_line(" 0 -> E1 8 ", true);
    EXPECT_EQ(r, 0);
    EXPECT_FALSE(ta.empty());
    EXPECT_EQ(ta.countTransitions(), 1);
    r = ta.read_line(" 0 -> C0  9 ", false);
    EXPECT_EQ(r, 0);
    r = ta.read_line(" 0 -> E3  29 ", false);
    EXPECT_EQ(r, 0);
    r = ta.read_line(" 0 -> E*  1", false);
    EXPECT_EQ(r, 0);
    EXPECT_EQ(ta.countTransitions(), 4);
}


TEST(TestSWTA, TestNullaryError)
{
    SWTAFileIn ta = SWTAFileIn(TropicalWeight::make_zero());
    size_t r;
    r = ta.read_line(" 0 -> C2 8 ", true);
    EXPECT_EQ(r, 2);
    r = ta.read_line(" 0 -> E1E  9 ", false);
    EXPECT_EQ(r, 2);
    r = ta.read_line(" 0 -> E3()  29 ", false);
    EXPECT_EQ(r, 2);
    EXPECT_TRUE(ta.empty());
}

TEST(TestSWTA, TestNullaryComment)
{
    SWTAFileIn ta = SWTAFileIn(TropicalWeight::make_zero());
    size_t r;
    r = ta.read_line(" 0 -> E1 8 // 1 event  ", true);
    EXPECT_EQ(r, 0);
    EXPECT_FALSE(ta.empty());
    EXPECT_EQ(ta.countTransitions(), 1);
    r = ta.read_line(" 1 -> C0  9  // continuation   ", false);
    EXPECT_EQ(r, 0);
    r = ta.read_line(" 4 -> E3  29 // 2 grace notes", false);
    EXPECT_EQ(r, 0);
    r = ta.read_line(" 3 -> E*  1  //undef", false);
    EXPECT_EQ(r, 0);
    EXPECT_EQ(ta.countTransitions(), 4);
    EXPECT_EQ(ta.initial(), 0);
}


TEST(TestSWTA, TestTuple)
{
    SWTAFileIn ta = SWTAFileIn(TropicalWeight::make_zero());
    size_t r;
    r = ta.read_line(" 0 -> T2(1, 2) 8 ", true);
    EXPECT_EQ(r, 0);
    EXPECT_FALSE(ta.empty());
    EXPECT_EQ(ta.countTransitions(), 1);
    r = ta.read_line(" 0 -> U3(3, 2, 4)  9 // unbeamed", false);
    EXPECT_EQ(r, 0);
    EXPECT_EQ(ta.countTransitions(), 2);
}


TEST(TestSWTA, TestBar)
{
    SWTAFileIn ta = SWTAFileIn(TropicalWeight::make_zero());
    size_t r;
    r = ta.read_line(" 1 -> B2(0, 1) 0 ", true);
    EXPECT_EQ(r, 0);
    EXPECT_FALSE(ta.empty());
    EXPECT_EQ(ta.countTransitions(), 1);
    r = ta.read_line(" 1 -> B0  0 // termine bar seq", false);
    EXPECT_EQ(r, 0);
    r = ta.read_line(" 4 -> B2(0, 4) 0 ", false);
    EXPECT_EQ(r, 0);
    EXPECT_FALSE(ta.empty());
    EXPECT_EQ(ta.countTransitions(), 3);
    EXPECT_EQ(ta.initial(), 1);
    EXPECT_EQ(ta.oftarget(1), 2);
    EXPECT_EQ(ta.oftarget(4), 1);
    EXPECT_EQ(ta.oftarget(5), 0);
    for (Tlist<Transition>::const_iterator i = ta.cbegin(1);
         i != ta.cend(1); i++)
    {
        EXPECT_TRUE((*i).weight().is_one());
    }
}


TEST(TestSWTA, TestBarDefaultWeight)
{
    SWTAFileIn ta = SWTAFileIn(TropicalWeight::make_zero());
    size_t r;
    r = ta.read_line(" 1 -> B2(0, 1)", true);
    EXPECT_EQ(r, 0);
    EXPECT_FALSE(ta.empty());
    EXPECT_EQ(ta.countTransitions(), 1);
    r = ta.read_line(" 1 -> B0  // termine bar seq", false);
    EXPECT_EQ(r, 0);
    r = ta.read_line(" 4 -> B2(0, 4) // other ", true);
    EXPECT_EQ(r, 0);
    EXPECT_FALSE(ta.empty());
    EXPECT_EQ(ta.countTransitions(), 3);
    EXPECT_EQ(ta.oftarget(1), 2);
    EXPECT_EQ(ta.oftarget(4), 1);
    EXPECT_EQ(ta.oftarget(5), 0);
    for (Tlist<Transition>::const_iterator i = ta.cbegin(1);
         i != ta.cend(1); i++)
    {
        EXPECT_TRUE((*i).weight().is_one());
    }
}

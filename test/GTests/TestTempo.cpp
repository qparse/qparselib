//
//  TestTempo.cpp
//  GTests
//
//  Created by Florent Jacquemard on 09/08/2021.
//  Copyright © 2021 Florent Jacquemard. All rights reserved.
//

#include <stdio.h>
#include "gtest/gtest.h"

#include "Tempo.hpp"

using namespace tempo;


TEST(TestTempo, TestT44)
{
    Tempo t1 = Tempo(4); // 4 seconds per bar
    ScoreModel::MeterSig ts = ScoreModel::MeterSig(4,4);
    EXPECT_EQ(t1.spb(), 4.0);
    EXPECT_EQ(t1.bps().numerator(), 1);
    EXPECT_EQ(t1.bps().denominator(), 4);
    EXPECT_EQ(t1.bpm(4), 60);
    EXPECT_EQ(t1.bpm(ts), 60);
    Tempo t2 = Tempo(60, ts);
    EXPECT_TRUE(t1 == t2);
}



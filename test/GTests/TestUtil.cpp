//
//  TestUtil.cpp
//  GTests
//
//  Created by Florent Jacquemard on 19/08/2019.
//  Copyright © 2019 Florent Jacquemard. All rights reserved.
//

#include <stdio.h>
#include "gtest/gtest.h"
#include <string>

#include "util.hpp"


TEST(TestUtil, TestSpaces)
{
    std::string s = "   po  ";
    std::string::const_iterator it = s.begin();
    std::string::const_iterator end = s.end();
    EXPECT_TRUE(it != end);
    util::read_spaces(it, it, end);
    EXPECT_TRUE(*it == 'p');
}


TEST(TestUtil, TestInt1)
{
    std::string s = "0123";
    std::string::const_iterator it = s.begin();
    std::string::const_iterator end = s.end();
    EXPECT_TRUE(it != end);
    long i = util::read_int(it, it, end);
    EXPECT_TRUE(i == 123);
    EXPECT_TRUE(it == end);
}


TEST(TestUtil, TestInt2)
{
    std::string s = "125 pipa";
    std::string::const_iterator it = s.begin();
    std::string::const_iterator end = s.end();
    EXPECT_TRUE(it != end);
    long i = util::read_int(it, it, end);
    EXPECT_TRUE(i == 125);
    EXPECT_TRUE(it != end);
    EXPECT_TRUE(*it == ' ');
}


TEST(TestUtil, TestIntSpace)
{
    std::string s = "     987 pipo";
    std::string::const_iterator it = s.begin();
    std::string::const_iterator end = s.end();
    EXPECT_TRUE(it != end);
    util::read_spaces(it, it, end);
    EXPECT_TRUE(*it == '9');
    long i = util::read_int(it, it, end);
    EXPECT_TRUE(i == 987);
    EXPECT_TRUE(it != end);
    EXPECT_TRUE(*it == ' ');
}


TEST(TestUtil, TestFloat)
{
    std::string s = "0121.5";
    std::string::const_iterator it = s.begin();
    std::string::const_iterator end = s.end();
    EXPECT_TRUE(it != end);
    float v = util::read_float(it, it, end);
    EXPECT_TRUE(v == 121.5);
    EXPECT_TRUE(it == end);
}
